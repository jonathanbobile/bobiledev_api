<?php

/**
 * apiFactory short summary.
 *
 * Creates an instance of the apiclass that supports the biz version from the request
 *
 * @version 1.0
 * @author JonathanM
 */
class apiFactory
{
    private $OS;
    private $papTapVersion;
    private $request;

    public function __construct($GET) {
        $this->OS = $GET["OS"];
        $this->papTapVersion = isset($GET["papTapVersion"]) ? $GET["papTapVersion"] : 0;
    }

    public function getAPIInstance(){
        if($this->OS == 'Android'){
            $APIFolder = $this->getAPIVersionForAndroidPaptapVersion();
        }
        else{
            $APIFolder = $this->getAPIVersionForIOSPaptapVersion();
        }

        echo $APIFolder;

        require("../".$APIFolder."/libs/apiClass.php");

        $apiInstance = new apiClass($this->request);

        return $apiInstance;
    }

    private function getAPIFolderForAndroidPaptapVersion(){
        
        switch($this->papTapVersion){
            case "355":
                $folder = "apiV2";
                break;
            default:
                $folder = "apiV2";
                break;
        }

        return $folder;
    }

    private function getAPIVersionForIOSPaptapVersion(){
        
        switch($this->papTapVersion){
            case "14.0":
                $folder = "apiV2";
                break;
            default:
                $folder = "apiV2";
                break;
        }

        return $folder;
    }
}
