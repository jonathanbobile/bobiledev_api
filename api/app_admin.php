<?
// DO NOT CHANGE TO    E_ALL !!!!!!!!!!!
error_reporting(E_ERROR);
ini_set("display_errors", 1);

require("../admin/libs/apiAdminClass.php");

$apiAdminClass = new apiAdminClass($_REQUEST);

//$fh = fopen("debugApiRequest.txt", 'a');
//fwrite($fh, print_r($_REQUEST,true)."\n");
//fclose($fh);

$action = $_REQUEST["action"];
if($action == ""){
    foreach (getallheaders() as $name => $value) {
        if($name == "action") $action = $value;
    }
}

switch ($action)
{
	case "login":       
        $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "1";
        $logout = isset($_REQUEST["logout"]) ? $_REQUEST["logout"] : "0";
        $pAppid = (isset($_REQUEST["appId"]) && $_REQUEST["appId"] != "") ? $_REQUEST["appId"] : "0";
        echo $apiAdminClass->appAdminLogin($_REQUEST["username"],$_REQUEST["password"],$type,$pAppid);        
        break;
        
    case "logout":        
        echo $apiAdminClass->appAdminLogout($_REQUEST["username"]);        
        break;
        
    case "getBizData":  
        echo $apiAdminClass->appAdminData();        
        break;
        
    case "getSettings":        
        echo $apiAdminClass->appAdminSettingsGet();        
        break;
        
    case "setSettings":        
        echo $apiAdminClass->appAdminSettingsSet($_REQUEST);        
        break;
        
    case "storeListing":     
        echo $apiAdminClass->appAdminStoreListing();        
        break;
    
    case "setStoreListing": 
        echo $apiAdminClass->appAdminSetStoreListing($_REQUEST);        
        break;
        
    case "plans":
        echo $apiAdminClass->appAdminPlans();        
        break;
        
    case "categoryList":
        echo $apiAdminClass->appAdminCategoryList();        
        break;
        
    case "setPurchasePlan":       
        echo $apiAdminClass->appAdminSetPurchasePlan($_REQUEST);        
        break;
        
    case "createInvoice":
        echo $apiAdminClass->appAdminCreateInvoice($_REQUEST);        
        break;
        
    case "cancelInvoice":
        echo $apiAdminClass->appAdminCancelInvoice($_REQUEST);        
        break;

    case "setBrand":
        echo $apiAdminClass->appAdminSetBrand($_REQUEST);        
        break;

    case "getThemes":
        echo $apiAdminClass->appAdminGetThemes($_REQUEST);        
        break;

    case "setTheme":
        echo $apiAdminClass->appAdminSetTheme($_REQUEST);        
        break;

    case "setLayout":
        echo $apiAdminClass->appAdminSetLayout($_REQUEST);        
        break;

    case "setContactDetails":
        echo $apiAdminClass->appAdminSetContactDetails($_REQUEST);
        break;

    case "setWorkHours":
        echo $apiAdminClass->appAdminSetWorkHours($_REQUEST);
        break;

    case "setAddress":        
        echo $apiAdminClass->appAdminAddressSet($_REQUEST);        
        break;

    case "createOrignalImage":        
        $apiAdminClass->createOriginImg($_REQUEST);        
        break;

    case "getCustomer":        
        echo $apiAdminClass->getCustomer($_REQUEST);        
        break;
        
    case "setCustomerDetails":        
        echo $apiAdminClass->appAdminSetCustomerDetails($_REQUEST);        
        break;   
        
    case "setCustomerGroups": 
        echo $apiAdminClass->appAdminSetCustomerGroups($_REQUEST);        
        break;   
        
    case "redeemReward": 
        echo $apiAdminClass->redeemReward($_REQUEST);        
        break; 
        
    case "redeemCoupon": 
        echo $apiAdminClass->redeemCoupon($_REQUEST);        
        break; 
        
    case "punchLoyalty": 
        echo $apiAdminClass->punchLoyalty($_REQUEST);        
        break; 
        
    case "setOrderStatus": 
        echo $apiAdminClass->setOrderStatus($_REQUEST);        
        break;

    case "setOrderPaymentStatus": 
        echo $apiAdminClass->setOrderPaymentStatus($_REQUEST);        
        break;
        
    case "getGroupsList": 
        echo $apiAdminClass->getGroupsList($_REQUEST);        
        break;

    case "deleteGroup": 
        echo $apiAdminClass->deleteGroup($_REQUEST);        
        break;

    case "editGroup": 
        echo $apiAdminClass->editGroup($_REQUEST);        
        break;

    case "getGroupCustomers":        
        echo $apiAdminClass->getGroupCustomers($_REQUEST);        
        break;
        
    case "saveGroupCustomers":        
        echo $apiAdminClass->saveGroupCustomers($_REQUEST);        
        break;

    case "employeeStatus": 
        echo $apiAdminClass->employeeStatus($_REQUEST);        
        break;
        
    case "getEmployeesList": 
        echo $apiAdminClass->getEmployeesList($_REQUEST);        
        break;

    case "deleteEmployee": 
        echo $apiAdminClass->deleteEmployee($_REQUEST);        
        break;
        
    case "editEmployee": 
        echo $apiAdminClass->editEmployee($_REQUEST);        
        break;
        
    case "saveEmployeeService":    
  
        echo $apiAdminClass->saveEmployeeService($_REQUEST);        
        break;
        
    case "getServicesList": 
        echo $apiAdminClass->getServicesList($_REQUEST);        
        break;
        
    case "serviceStatus": 
        echo $apiAdminClass->serviceStatus($_REQUEST);        
        break;
        
    case "deleteService": 
        echo $apiAdminClass->deleteService($_REQUEST);        
        break;
        
    case "editService": 
    
        echo $apiAdminClass->editService($_REQUEST);        
        break;
        
    case "saveServiceEmployee":                
        echo $apiAdminClass->saveServiceEmployee($_REQUEST);        
        break;
        
    case "getClassesList":            
        echo $apiAdminClass->getClassesList($_REQUEST);        
        break;

    case "getSingleClass":            
        echo $apiAdminClass->getSingleClass($_REQUEST);        
        break;
        
    case "deleteClass": 
        echo $apiAdminClass->deleteClass($_REQUEST);        
        break;
        
    case "hideClass": 
        echo $apiAdminClass->hideClass($_REQUEST);        
        break;

    case "setClassDetails": 
        echo $apiAdminClass->setClassDetails($_REQUEST);        
        break;
        
    case "addClassDate": 
        echo $apiAdminClass->addClassDate($_REQUEST);        
        break;

    case "toggleDateVisible":
        echo $apiAdminClass->toggleDateVisible($_REQUEST);        
        break;

    case "deleteDate": 
        echo $apiAdminClass->deleteDate($_REQUEST);        
        break;
        
    case "getDateCustomers":        
        echo $apiAdminClass->getDateCustomers($_REQUEST);        
        break;
        
    case "saveDateCustomers":        
        echo $apiAdminClass->saveDateCustomers($_REQUEST);        
        break;
        
    case "getProductsList":        
        echo $apiAdminClass->getProductsList($_REQUEST);        
        break;
        
    case "getOrdersList":        
        echo $apiAdminClass->getOrdersList($_REQUEST);        
        break;

    case "hideProduct": 
        echo $apiAdminClass->hideProduct($_REQUEST);        
        break;
        
    case "setProductFeatured": 
        echo $apiAdminClass->setProductFeatured($_REQUEST);        
        break; 
        
    case "deleteProduct": 
        echo $apiAdminClass->deleteProduct($_REQUEST);        
    break;   
    
    case "saveProductCategory": 
        echo $apiAdminClass->saveProductCategory($_REQUEST);        
        break;

    case "setProductDetails": 
        echo $apiAdminClass->setProductDetails($_REQUEST);        
        break;
        
    case "deleteAttribute": 
        echo $apiAdminClass->deleteAttribute($_REQUEST);        
        break;
        
    case "saveAttributes":  
        echo $apiAdminClass->saveAttributes($_REQUEST);        
        break;
        
    case "getCategoriesList":
        echo $apiAdminClass->getCategoriesList();        
        break;
        
    case "categoryStatus": 
        echo $apiAdminClass->categoryStatus($_REQUEST);        
        break;
        
    case "deleteCategory": 
        echo $apiAdminClass->deleteCategory($_REQUEST);        
        break;
        
    case "editCategory": 

        echo $apiAdminClass->editCategory($_REQUEST);        
        break;
        
    case "getConnectedProducts": 
        echo $apiAdminClass->getConnectedProducts($_REQUEST);        
        break;  
        
    case "saveCategoryProduct": 
        echo $apiAdminClass->saveCategoryProduct($_REQUEST);        
        break; 
        
    case "getCouponsList": 
        echo $apiAdminClass->getCouponsList($_REQUEST);        
        break; 
        
    case "hideCoupon": 
        echo $apiAdminClass->hideCoupon($_REQUEST);        
        break;
        
    case "deleteCoupon": 
        echo $apiAdminClass->deleteCoupon($_REQUEST);        
        break;
        
    case "editCoupon": 
        echo $apiAdminClass->editCoupon($_REQUEST);        
        break;

    case "getRewardsList": 
        echo $apiAdminClass->getRewardsList($_REQUEST);        
        break; 
        
    case "hideReward": 
        echo $apiAdminClass->hideReward($_REQUEST);        
        break;
        
    case "deleteReward": 
        echo $apiAdminClass->deleteReward($_REQUEST);        
        break;
        
    case "setLoyaltyDetails": 
        echo $apiAdminClass->setLoyaltyDetails($_REQUEST);        
        break;  
        
    case "setLoyaltyIOS": 
        echo $apiAdminClass->setLoyaltyDetailsIOS($_REQUEST);        
        break;
        
    case "saveLoyaltyStamps": 
        echo $apiAdminClass->saveLoyaltyStamps($_REQUEST);        
        break; 

    case "saveLoyaltyRewards": 
        echo $apiAdminClass->saveLoyaltyRewards($_REQUEST);        
        break; 
        
    case "getPaymentRequestList": 
        echo $apiAdminClass->getPaymentRequestList($_REQUEST);        
        break;
        
    case "deletePaymentRequest": 
        echo $apiAdminClass->deletePaymentRequest($_REQUEST);        
        break;
        
    case "savePaymentRequest": 
        echo $apiAdminClass->savePaymentRequest($_REQUEST);        
        break;
        
    case "createApp": 
        echo $apiAdminClass->createApp($_REQUEST);        
        break;

    case "createAccount": 
        echo $apiAdminClass->createAccount($_REQUEST);        
        break;

    case "getValidationCode": 
        echo $apiAdminClass->getValidationCode($_REQUEST);        
        break;
        
    case "cancelPlan": 
        echo $apiAdminClass->cancelPlan($_REQUEST);        
        break;
        
    case "getBizLevels": 
        echo $apiAdminClass->getBizLevels($_REQUEST);        
        break;
        
    case "getVideoThumbnail": 
        echo $apiAdminClass->getVideoThumbnail($_REQUEST);        
        break;

    case "saveBizLevels": 
        echo $apiAdminClass->saveBizLevels($_REQUEST);        
        break;

    case "getAppManagerData": 
        echo $apiAdminClass->getAppManagerData($_REQUEST);        
        break;

    case "hideElement": 
        echo $apiAdminClass->hideElement($_REQUEST);        
        break;

    case "deleteElement": 
        echo $apiAdminClass->deleteElement($_REQUEST);        
        break;

    case "deleteMultiElement": 
        echo $apiAdminClass->deleteMultiElement($_REQUEST);        
        break;

    case "editElement": 
        echo $apiAdminClass->editElement($_REQUEST);        
        break;

    case "getBizModules": 
        echo $apiAdminClass->getBizModules($_REQUEST);        
        break;

    case "setModuleVisibility": 
        echo $apiAdminClass->setModuleVisibility($_REQUEST);        
        break;

    case "setModuleDetails": 
        echo $apiAdminClass->setModuleDetails($_REQUEST);        
        break;

    case "setModulesOrder": 
        echo $apiAdminClass->setModulesOrder($_REQUEST);        
        break;

    case "getHomeLayout": 
        echo $apiAdminClass->getHomeLayout($_REQUEST);        
        break;

    case "getLayoutItemsToConnect": 
        echo $apiAdminClass->getLayoutItemsToConnect($_REQUEST);        
        break;

    case "getStructures": 
        echo $apiAdminClass->getStructures();        
        break;
        
    case "getEmployeeServices": 
        echo $apiAdminClass->getEmployeeServices($_REQUEST);        
        break;

    case "setSimpleHomeLayout": 
        echo $apiAdminClass->setSimpleHomeLayout($_REQUEST);        
        break;

    case "setBookingHomeLayout": 
        echo $apiAdminClass->setBookingHomeLayout($_REQUEST);        
        break;

    case "setButtonsHomeLayout": 
        echo $apiAdminClass->setButtonsHomeLayout($_REQUEST);        
        break;

    case "setConectedListLayout": 
        echo $apiAdminClass->setConectedListLayout($_REQUEST);        
        break;

    case "getBizDirectData": 
        echo $apiAdminClass->getBizDirectData($_REQUEST);        
        break;
    case "processAdminPic":
        echo $apiAdminClass->processPicUrl($_REQUEST);        
        break;

}

?>