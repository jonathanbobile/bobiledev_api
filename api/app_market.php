<?
// DO NOT CHANGE TO    E_ALL !!!!!!!!!!!
error_reporting(E_ERROR);
ini_set("display_errors", 1);

require("../admin/libs/apiMarketClass.php");

$apiMarketClass = new apiMarketClass($_REQUEST);
$action = $_REQUEST["action"];

//$fh = fopen("debugApiRequest.txt", 'a');
//fwrite($fh, print_r($_REQUEST,true)."\n");
//fclose($fh);

switch ($action)
{       
    case "getMarketData": 
        echo $apiMarketClass->getMarketData($_REQUEST);        
        break;
        
    case "getCategoryList": 
        echo $apiMarketClass->getCategoryList($_REQUEST);        
        break;

    case "getSubCategoryList": 
        echo $apiMarketClass->getSubCategoryList($_REQUEST);        
        break;
        
    //for old market + bobile and resellers
    case "getFeatured": 
        echo $apiMarketClass->getFeatured($_REQUEST);        
        break;
        
    //for new market
    case "getMarketFeatured": 
        echo $apiMarketClass->getMarketFeatured($_REQUEST);        
        break;
        
    case "checkIp": 
        echo $apiMarketClass->checkIp($_REQUEST);        
        break;
        
    case "bizInfo": 
        echo $apiMarketClass->bizInfo($_REQUEST);        
        break;

    case "bizInfoInner": 
        echo $apiMarketClass->bizInfoInner($_REQUEST);        
        break;
        
    case "search": 
        echo $apiMarketClass->search($_REQUEST);        
        break;
        
    case "nearMe": 
        echo $apiMarketClass->nearMe($_REQUEST);        
        break;
        
    case "getDeals": 
        echo $apiMarketClass->getDeals($_REQUEST);        
        break;
        
    case "getSearchTrend": 
        echo $apiMarketClass->getSearchTrend($_REQUEST);        
        break;
        
    case "setRating": 
        echo $apiMarketClass->setRating($_REQUEST);        
        break;

    case "getRating": 
        echo $apiMarketClass->getRating($_REQUEST);        
        break;

    case "setMarketUser": 
        echo $apiMarketClass->setMarketUser($_REQUEST);        
        break;

    case "getMarketUser": 
        echo $apiMarketClass->getMarketUser($_REQUEST);        
        break;

    case "getNotifications": 
        echo $apiMarketClass->getNotifications($_REQUEST);        
        break;

    case "getWhiteLabelDomain": 
        echo $apiMarketClass->getWhiteLabelDomain($_REQUEST);        
        break;

    case "getWhiteLabelData": 
        echo $apiMarketClass->getWhiteLabelData($_REQUEST);        
        break;

    // case: "checkIfActive":
    //     echo $apiMarketClass->checkIfActive($_REQUEST);
    //     break;
}

?>