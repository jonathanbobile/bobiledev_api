<?PHP
session_start();
require 'connect.php';
include "../class/phmagick/phmagick.php";

$papTapAppID = "476541845701337";
$paptapAppScrete = "472b003fa24744dc0c26e782cf1894d5";

$pageid = $_GET['pageid'];

$graphFBroot ="https://graph.facebook.com" ;
$graphFBUserAbout = $graphFBroot."/".$pageid."?access_token=".$papTapAppID."|".$paptapAppScrete;

$responseData = getGraph($graphFBUserAbout);

//GENERAL INFO ABOUT THE USER
$appName = addslashes($responseData->name);
//echo $appName;

if($appName == "")
{
    echo "0";
    exit;
}

$facebookID = $responseData->id;

$aboutText = "";
$about = $responseData->about;
$company_overview = $responseData->company_overview;
$description = $responseData->description;
$mission = $responseData->mission;
$products = $responseData->products;
$subCatName = strtolower($responseData->category);


if ($responseData->hours != ""){
    $hours = "<br /><div><table style=\"line-height:2;\"><tr><td width=120px;>Monday       </td><td>".$responseData->hours->mon_1_open." - ".$responseData->hours->mon_1_close."</td></tr>";
    $hours .= "<tr><td>Tuesday       </td><td>".$responseData->hours->tue_1_open." - ".$responseData->hours->tue_1_close."</td></tr>";
    $hours .= "<tr><td>Wednesday       </td><td>".$responseData->hours->wed_1_open." - ".$responseData->hours->wed_1_close."</td></tr>";
    $hours .= "<tr><td>Thursday       </td><td>".$responseData->hours->thu_1_open." - ".$responseData->hours->thu_1_close."</td></tr>";
    $hours .= "<tr><td>Friday       </td><td>".$responseData->hours->fri_1_open." - ".$responseData->hours->fri_1_close."</td></tr>";
    $hours .= "<tr><td>Saturday       </td><td>".$responseData->hours->sat_1_open." - ".$responseData->hours->sat_1_close."</td></tr>";
    $hours .= "<tr><td>Sunday       </td><td>".$responseData->hours->sun_1_open." - ".$responseData->hours->sun_1_close."</table></div>";


}

if($about != "")
    $aboutText .= "<div style=\"font-family: arial black,avant garde; font-size: 18px\"><b>About</b></div>$about";

if($company_overview != "")
    $aboutText .= "<br /><br /><div style=\"font-family: arial black,avant garde; font-size: 18px\"><b>Company Overview</b></div>$company_overview";

if($description != "")
    $aboutText .= "<br /><br /><div style=\"font-family: arial black,avant garde; font-size: 18px\"><b>Description</b></div>$description";

if($mission != "")
    $aboutText .= "<br /><br /><div style=\"font-family: arial black,avant garde; font-size: 18px\"><b>Mission</b></div>$mission";

if($products != "")
    $aboutText .= "<br /><br /><div style=\"font-family: arial black,avant garde; font-size: 18px\"><b>Products</b></div>$products";

if($hours != "")
    $aboutText .= "<br /><br /><div style=\"font-family: arial black,avant garde; font-size: 18px\"><b>Opening Hours</b></div>$hours";

$aboutText = "<div id=\"fbcontent\" contenteditable=\"false\">".addslashes( preg_replace('#<a.*?>.*?</a>#i', '',  $aboutText))."</div>";

//LOCATION & CONTACT INFO
$country_Name = addslashes($responseData->location->country);
$state_Name = addslashes($responseData->location->state);
$city_Name = addslashes($responseData->location->city);
$street  = addslashes($responseData->location->street);
$zip  = addslashes($responseData->location->zip);
$phone = addslashes($responseData->phone);
$website = addslashes($responseData->website);
$latitude = $responseData->location->latitude;
$longitude = $responseData->location->longitude;

$geoData = askGoogleGeo($latitude, $longitude);
$geoResult = $geoData->results[0];
$countryPrefix="";
$statePrefix = "";

if($state_Name != "")
{
    $stateID = dbGetVal("select state_id from tbl_states where state_code = '$state_Name'");
}

if($stateID == "")
{
    if(count($geoResult->address_components)>0){
        foreach ($geoResult->address_components as $component){
            if ($component->types[0] == "country"){
                $countryPrefix = $component->short_name;
            }
            if ($countryPrefix == "US"){
                
                foreach ($geoResult->address_components as $state_component){
                    if ($component->types[0] == "administrative_area_level_1"){
                        $statePrefix = $state_component->short_name;
                    }
                }
                
            }
        }
    }
}


$countryID = 0;
if ($countryPrefix != ""){
    $countryID = dbGetVal("select country_id from tbl_countries where country_code = '$countryPrefix'");
}

$stateID = 0;
if ($statePrefix != ""){
    $stateID = dbGetVal("select state_id from tbl_states where state_code = '$statePrefix'");
}

//CATEGORY & SUB CATEGORY SETUP FOR PAPTAP MARKET
$catID = 7;
$subCatID = 41;

$fbCatRow = dbGetRow("SELECT * FROM tbl_fb_categories WHERE LOWER(fb_cat_name) like '$subCatName'");

if ($fbCatRow["pt_cat_id"] != ""){
    $catID = $fbCatRow["pt_cat_id"];
    $subCatID = $fbCatRow["pt_sub_id"];
}


//GET ICON OF THE USER


$iconUrl = "https://graph.facebook.com/$pageid/picture?width=1024&height=1024&access_token=$papTapAppID|$paptapAppScrete";
//https://graph.facebook.com/103735073315/picture?width=1024&height=1024&access_token=476541845701337|472b003fa24744dc0c26e782cf1894d5
$logoUrl = $responseData->cover->source;


$noState = false;
if($countryID == "1" && $stateID=="0") $noState = true;

if($appName == "" || $aboutText == "" || $countryID == "0" || $noState || $logoPng=="")
{
    if($aboutText == "")
    {
        $fbStep="1";
    }
    else
    {
        $fbStep="2";
    }
}
else
{
    $fbStep="3";
}

$bizShortName = $appName; //mb_substr($appName,0,15,'UTF-8');

$i=0;
$responce->rows[$i]['bizShortName']=$bizShortName;
$responce->rows[$i]['aboutText']=$aboutText;
$responce->rows[$i]['stateID']=$stateID;
$responce->rows[$i]['countryID']=$countryID;
$responce->rows[$i]['country_Name']=$country_Name;
$responce->rows[$i]['state_Name']=$state_Name;
$responce->rows[$i]['subCatID']=$subCatID;
$responce->rows[$i]['iconUrl']=$iconUrl;


$json = json_encode($responce);
echo $json;



function resize_image($source_image, $destination_filename, $width = 250, $height = 250, $quality = 100, $crop = true){

        if( ! $image_data = getimagesize( $source_image ) )
        {
                return false;
        }

        switch( $image_data['mime'] )
        {
                case 'image/gif':
                        $get_func = 'imagecreatefromgif';
                       
                break;
                case 'image/jpeg':
				case 'image/jpg':
                        $get_func = 'imagecreatefromjpeg';
                        
                break;
                case 'image/png':
                        $get_func = 'imagecreatefrompng';
                       
                break;
        }

        $img_original = call_user_func( $get_func, $source_image );
        $old_width = $image_data[0];
        $old_height = $image_data[1];
        $new_width = $width;
        $new_height = $height;
        $src_x = 0;
        $src_y = 0;
        $current_ratio = round( $old_width / $old_height, 2 );
        $desired_ratio_after = round( $width / $height, 2 );
        $desired_ratio_before = round( $height / $width, 2 );

        // if( $old_width < $width || $old_height < $height )
        // {
                // /**
                 // * The desired image size is bigger than the original image. 
                 // * Best not to do anything at all really.
                 // */
                // return false;
        // }


        /**
         * If the crop option is left on, it will take an image and best fit it
         * so it will always come out the exact specified size.
         */
        if( $crop )
        {
                /**
                 * create empty image of the specified size
                 */
                $new_image = imagecreatetruecolor( $width, $height );

				switch ($image_data['mime'])
				{
					case "image/png":
						$background = imagecolorallocate($new_image, 255, 255, 255);
						imagecolortransparent($new_image, $background);
						imagealphablending($new_image, false);
						imagesavealpha($new_image, true);
						break;
					case "image/gif":
						$background = imagecolorallocate($new_image, 255, 255, 255);
						imagecolortransparent($new_image, $background);
						break;
				}
	
                /**
                 * Landscape Image
                 */
                if( $current_ratio > $desired_ratio_after )
                {
                        $new_width = $old_width * $height / $old_height;
                }

                /**
                 * Nearly square ratio image.
                 */
                if( $current_ratio > $desired_ratio_before && $current_ratio < $desired_ratio_after )
                {
                        if( $old_width > $old_height )
                        {
                                $new_height = max( $width, $height );
                                $new_width = $old_width * $new_height / $old_height;
                        }
                        else
                        {
                                $new_height = $old_height * $width / $old_width;
                        }
                }

                /**
                 * Portrait sized image
                 */
                if( $current_ratio < $desired_ratio_before  )
                {
                        $new_height = $old_height * $width / $old_width;
                }

                /**
                 * Find out the ratio of the original photo to it's new, thumbnail-based size
                 * for both the width and the height. It's used to find out where to crop.
                 */
                $width_ratio = $old_width / $new_width;
                $height_ratio = $old_height / $new_height;

                /**
                 * Calculate where to crop based on the center of the image
                 */
                $src_x = floor( ( ( $new_width - $width ) / 2 ) * $width_ratio );
                $src_y = round( ( ( $new_height - $height ) / 2 ) * $height_ratio );
        }
        /**
         * Don't crop the image, just resize it proportionally
         */
        else
        {
                if( $old_width > $old_height )
                {
                        $ratio = max( $old_width, $old_height ) / max( $width, $height );
                }else{
                        $ratio = max( $old_width, $old_height ) / min( $width, $height );
                }

                $new_width = $old_width / $ratio;
                $new_height = $old_height / $ratio;

                $new_image = imagecreatetruecolor( $new_width, $new_height );
        }

        /**
         * Where all the real magic happens
         */
        imagecopyresampled( $new_image, $img_original, 0, 0, $src_x, $src_y, $new_width, $new_height, $old_width, $old_height );

        /**
         * Save it as a PNG File with our $destination_filename param.
         */
        imagejpeg( $new_image, $destination_filename, $quality  );

        /**
         * Destroy the evidence!
         */
        imagedestroy( $new_image );
        imagedestroy( $img_original );

        /**
         * Return true because it worked and we're happy. Let the dancing commence!
         */
        return true;
}   

function getGraph($url){
    
    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    $ch = curl_init();
   
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERAGENT, $agent);
curl_setopt($ch, CURLOPT_URL,$url);
$result=curl_exec($ch);
return json_decode($result);
}

function getURL($url){
    
    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    curl_setopt($ch, CURLOPT_URL,$url);
    $result=curl_exec($ch);
    return $result;
}

function getRedirectedUrl($url){
    
    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 100);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_USERAGENT, $agent);
    $result=curl_exec($ch);
    //print_r($result);

    $lastUrl = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    echo "Original: $url <br>Final: $lastUrl";
    exit;
}

function askGoogleGeo($late,$long){
    $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$late.",".$long;
    $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
    $ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERAGENT, $agent);
curl_setopt($ch, CURLOPT_URL,$url);
$result=curl_exec($ch);
return json_decode($result);
}

function startsWith($haystack, $needle){
    return $needle === "" || strpos($haystack, $needle) === 0;
}

function str_contains($haystack, $needle)
{
    if (strpos($haystack,$needle) !== false)
        return true;
    else
        return false;
}

function endsWith($haystack, $needle){
    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
}

function getFacebookURL($ads,$baseURL)
{   
    $regexp = "<a\s[^>]*href=(\"??)([^\" >]*?)\\1[^>]*>(.*)<\/a>";
    if(preg_match_all("/$regexp/siU", $ads, $matches, PREG_SET_ORDER)) {
        foreach($matches as $match) {
            
            if(!startsWith(strtolower($match[2]),'http')) $match[2] = "$baseURL".str_replace("../","",$match[2]);  
            $componentsLink = parse_url($match[2]);

            if(str_contains(strtolower($componentsLink['host']),"facebook.com"))
            {
                $pathParts = explode("/",$componentsLink['path']);
                return $componentsLink['scheme']."://".$componentsLink['host']."/".$pathParts[count($pathParts)-1];
                break;
            }
        }
    }
}

?>