<?php
require 'connect.php';
$rowid = $_REQUEST['rid'];

$sql = "SELECT * FROM tbl_mod_data35 WHERE md_row_id = $rowid";
$html_row = dbGetRow($sql);

$title = '';
$sql = '';
if($html_row['md_external_type'] == 'module'){
    $sql = "SELECT biz_mod_mobile_name FROM tbl_modules
                                        INNER JOIN tbl_biz_mod ON mod_id = biz_mod_mod_id
                                        LEFT JOIN tbl_permission_tree as perm ON pa_mod_id = tbl_modules.mod_id
                                        LEFT JOIN tbl_permission_tree as area ON area.pa_id = perm.pa_parent_id
                                        WHERE biz_mod_mod_id = 35
                                        AND biz_mod_id = {$html_row['md_external_id']}";
}
if($html_row['md_external_type'] == 'function'){
    $sql = "SELECT bfu_label FROM tbl_biz_functions WHERE bfu_id = {$html_row['md_external_id']}";
}
if($sql != ''){
    $title = dbGetVal($sql);
}


$html = $html_row['md_info1'];
$js = $html_row['md_info2'];
$css = $html_row['md_info3'];

?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=0.6">
        <title>
            <?= $title?>
        </title>
        
        <?= $js?>
        <?= $css?>
    </head>
    <body>
        <?= $html?>
    </body>

</html>