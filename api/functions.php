<?php
require_once('class/class.phpmailer.php');


$serverUTC = "3";

function checkEmail($str){

	return preg_match("/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/", $str);
}

function validatePhone($str) {
	if (preg_match('/^[0-9\-]+$/',$str)) {
		if(substr($str, 0, 1) != "0" && substr($str, 0, 1) != "1" && substr($str, 0, 1) != "*")
		{
			return false;
		}
		if(strlen(str_replace( '-' , '' ,$str)) != 10 && strlen(str_replace( '-' , '' ,$str)) != 9 && strlen(str_replace( '-' , '' ,$str)) != 5)
		{
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

function validateCellPhone($str) {
	if (preg_match('/^[0-9\-]+$/',$str)) {
		if(substr($str, 0, 1) != "0")
		{
			return false;
		}
		if(strlen(str_replace( '-' , '' ,$str)) != 10)
		{
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

function validateNumber($str,$maxLen = 999999) {
	if (preg_match('/[0-9]/',$str)) {
		if(strlen($str) > 9)
		{
			return false;
		}
		return true;
	}
	else
	{
		return false;
	}
}

function valid_date($date, $format = 'YYYY-MM-DD'){
    if(strlen($date) >= 8 && strlen($date) <= 10){
        $separator_only = str_replace(array('M','D','Y'),'', $format);
        $separator = $separator_only[0];
        if($separator){
            $regexp = str_replace($separator, "\\" . $separator, $format);
            $regexp = str_replace('MM', '(0[1-9]|1[0-2])', $regexp);
            $regexp = str_replace('M', '(0?[1-9]|1[0-2])', $regexp);
            $regexp = str_replace('DD', '(0[1-9]|[1-2][0-9]|3[0-1])', $regexp);
            $regexp = str_replace('D', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
            $regexp = str_replace('YYYY', '\d{4}', $regexp);
            $regexp = str_replace('YY', '\d{2}', $regexp);
            if($regexp != $date && preg_match('/'.$regexp.'$/', $date)){
                foreach (array_combine(explode($separator,$format), explode($separator,$date)) as $key=>$value) {
                    if ($key == 'YY') $year = '20'.$value;
                    if ($key == 'YYYY') $year = $value;
                    if ($key[0] == 'M') $month = $value;
                    if ($key[0] == 'D') $day = $value;
                }
                if (checkdate($month,$day,$year)) return true;
            }
        }
    }
    return false;
}

function fix_date($date){
	$newDate = $date;
    if(strlen($date) >= 6 && strlen($date) <= 10){
        $vowels = array(".", "\\", "-");
		$newDate = str_replace($vowels, "/", $date);		
		$dateParts = explode("/", $newDate);
	
		if(strlen($dateParts[0])==4)
		{
			if(strlen($dateParts[2])==1) $dateParts[2] = "0".$dateParts[2];
			if(strlen($dateParts[1])==1) $dateParts[1] = "0".$dateParts[1];
			if(strlen($dateParts[0])==2) 
			{
				if($dateParts[0]<20)
				{
					$dateParts[0] = "20".$dateParts[0];
				}
				else
				{
					$dateParts[0] = "19".$dateParts[0];
				}
			}
			$newDate = $dateParts[0].'-'.$dateParts[1].'-'.$dateParts[2];
		}
		else
		{
			if(strlen($dateParts[0])==1) $dateParts[0] = "0".$dateParts[0];
			if(strlen($dateParts[1])==1) $dateParts[1] = "0".$dateParts[1];
			if(strlen($dateParts[2])==2) 
			{
				if($dateParts[2]<20)
				{
					$dateParts[2] = "20".$dateParts[2];
				}
				else
				{
					$dateParts[2] = "19".$dateParts[2];
				}
			}
			$newDate = $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0];
		}
		
    }

    return $newDate;
}

function send_mail_async($params = array())
{
    $url = 'https://app.bobile.com/mailsender.php';
	$post_params = array();

	foreach ($params as $key => &$val) 
	{
		$post_params[] = $key.'='.$val;
	}
	
	$post_string = implode('&', $post_params);

	$parts=parse_url($url);

	$fp = fsockopen($parts['host'],
		isset($parts['port'])?$parts['port']:80,
		$errno, $errstr, 30);

	$out = "POST ".$parts['path']." HTTP/1.1\r\n";
	$out.= "Host: ".$parts['host']."\r\n";
	$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
	$out.= "Content-Length: ".strlen($post_string)."\r\n";
	$out.= "Connection: Close\r\n\r\n";
	if (isset($post_string)) $out.= $post_string;
	
	fwrite($fp, $out);
	fclose($fp);

}

function send_mail($from,$to,$subject,$body,$fromName='',$toName='',$attach='',$fbfolder='',$smtpserver='',$smtpuser='',$smtppass='',$debug=''){

	if ($from == "support@bobile.support"){

		$body = str_ireplace ( 'www.paptap.com','paptap.com' , $body);
		$body = str_ireplace ( 'support@bobile.support','care@paptapsupport.com' , $body);
		$body = str_ireplace ( 'href="http://paptap.com' , 'href="http://paptapsupport.com/useraction.php?u=pt-session' , stripcslashes($body));
		$body = str_ireplace ( 'paptap.com' , 'paptapsupport.com' , $body);

	    send_support_mail("care@paptapsupport.com", 
					$to, 
					$subject, 
					$body, 
					"Bobile App Creator",
					$toName,
					$attach,
					"care@paptapsupport.com");

	    return "1";
	}

	$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch
	
	$mail->SMTPDebug = 1;	
	$mail->IsSMTP();
	$mail->SMTPAuth = true;
	$mail->IsHTML(true);
	$mail->ContentType = "text/html; charset=\"UTF-8\"";
	$mail->CharSet = "UTF-8";
    
    $smtpserver = 'email-smtp.us-west-2.amazonaws.com,465';
    $smtpuser = 'AKIAJFP4MJS32JBRLNOA';
    $smtppass = 'Armoc/eGk0y9vIMFTtTl2edL1BGcm/Q40lqb77uNvNPW';
    $fromInner = $from;
    
    //if($from != "info@paptap.com" && $from != "support@paptap.com" && $from != "contact@paptap.com" && $from != "billing@paptap.com")
    //{
        $smtpserver = 'mail.paptap.com,465';
		$smtpuser = 'support@bobile.support';
		$smtppass = 'pa55w0rd1qaz';
    //}

	
	try {
	  $mail->AddReplyTo($from, "=?UTF-8?B?".base64_encode($fromName)."?=");
	  $mail->AddAddress($to, $toName);
	  $mail->SetFrom($fromInner, "=?UTF-8?B?".base64_encode($fromName)."?=");	  
	  $mail->Subject = '=?UTF-8?B?'.base64_encode(stripcslashes($subject)).'?=';
	  //$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
	  $mail->MsgHTML(stripcslashes($body));
	  
	if($attach != '')
	{
		$arr = explode (";", $attach);
		foreach( $arr as $oneFile) 
		{
			if($oneFile != '')
			{
				$arrName = explode (".", $oneFile);
				$pos = strpos($oneFile, 'graphics/');
				if ($pos === false) 
				{
					$posgib = strpos($oneFile, 'backup/');
					if ($posgib === false) 
					{
						$mail->AddAttachment($fbfolder.'uploads/'.$oneFile,$oneFile);      // attachment
					}
					else
					{
						$mail->AddAttachment($oneFile,str_replace('backup/','',$oneFile));
					}
				}
				else
				{
					$mail->AddAttachment($oneFile,str_replace('graphics/','',$oneFile));
				}
			}
		}
	}

		$mail->CharSet = "UTF-8";
	    
		if($smtpserver != '')
		{
			$arr = explode (",", $smtpserver);
			
			if (count($arr) > 1) {
				$mail->SMTPSecure = 'ssl'; 
				$mail->Host = $arr[0];
				$mail->Port = $arr[1];  
				$mail->Username = $smtpuser;  
				$mail->Password = $smtppass;   
			} else {
				$mail->Host = $smtpserver;
				$mail->Username = $smtpuser;  
				$mail->Password = $smtppass;
			}
		}		
	
		$mail->Send();
		  //echo "Message Sent OK<p></p>\n";
		} catch (phpmailerException $e) {
			if($debug == "1")
			{
				echo $e->errorMessage(); //Pretty error messages from PHPMailer
			}
		} catch (Exception $e) {
			if($debug == "1")
			{
				echo $e->getMessage(); //Boring error messages from anything else!
			}
		}
}

function send_support_mail($from, $to, $subject, $body, $fromName='',$toName='',$attach='',$replyTo){

	//set POST variables
	$url = 'http://paptapsupport.com/send.php';
	$fields = array(
		'sendTo' => $to,
		'sendToName' => $toName,
		'sentFromMail' => $from,
		'sentFromName' => $fromName,
		'replyTo' => $replyTo,
		'subject' => urlencode(stripcslashes($subject)),
		'body' => urlencode(stripcslashes($body)),
		'sg' => "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"
	);

	//url-ify the data for the POST
	foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
	rtrim($fields_string, '&');

	//open connection
	$ch = curl_init();

	//set the url, number of POST vars, POST data
	curl_setopt($ch,CURLOPT_URL, $url);
	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

	//execute post
	curl_exec($ch);
	
	//close connection
	//curl_close($ch);
}

function send_mail_old($from,$to,$subject,$body,$fromName=''){
	$headers = "";
	$headers .= "From: =?UTF-8?B?".base64_encode($fromName)."?= $from\r\n";
	$headers .= "Reply-to: =?UTF-8?B?".base64_encode($fromName)."?= $from\r\n";
	$headers .= "Return-Path: =?UTF-8?B?".base64_encode($fromName)."?= $from\r\n";
	$headers .= "Message-ID: <" . md5(uniqid(time())) . "@" . $_SERVER['SERVER_NAME'] . ">\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=UTF-8\r\n";
	$headers .= "X-Priority: 3\r\n";
	$headers .= "X-Mailer: PHP". phpversion() ."\r\n";
	$headers .= "Date: " . date('r', time()) . "\r\n";
  
	mail($to,'=?UTF-8?B?'.base64_encode($subject).'?=','<html><head><title>mail from site</title></head><body dir="rtl">'.nl2br($body).'</body></html>',$headers);
}

function send_sms($message,$num,$rply,$tosend,$comp){

	$text 	= stripcslashes($message);
	$user 	= 'daniel7';
	$pass 	= 'daniel7kf';
	$timer	= 0;

	if ($timer > 0)
		$timerStr = "<schedule><relative>" .$timer. "</relative></schedule>";
	else 
		$timerStr = "";

	if($tosend == "0")
	{
		$tosend = "9";
	}
	$rply = preg_replace('/972/i', '0', $rply, 1);
	$postdataIn = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>".
								"<sms command=\"submit\" version=\"1.0\">".
									"<account>".
										"<id>".$user."</id>".
										"<password>".$pass."</password>".
									"</account>".
									"<attributes>".
										"<validity type=\"relative\" units=\"d\">1</validity>".
										"<notify type=\"4\">http://shivukclick.co.il/club/smsresponse.php</notify>".
										"<reference>".$tosend.$comp."</reference>".
										"<replyPath>".$rply."</replyPath>".
									"</attributes>".
									$timerStr.
									"<targets>".
										"<cellphone>".$num."</cellphone>".
									"</targets>".
									"<data type=\"text\"><![CDATA[".$text."]]></data>".
								"</sms>";

	$hostIn = "soprano.co.il";
	$portIn = "80";
	$pathIn = "/prodsms/corpsms";
	$postdata = $postdataIn;
		//$myFile = "testFileXML.txt";
		//$fh = fopen($myFile, 'w') or die("can't open file");
		//fwrite($fh, $postdataIn);
		//fclose($fh);		
	doPost($hostIn,$portIn,$pathIn,$postdata);
}

function utf8_urldecode($str) {
    $str = preg_replace("/%u([0-9a-f]{3,4})/i","&#x\\1;",urldecode($str));
    return html_entity_decode($str,null,'UTF-8');
  }
  
function Strip($value){
	if(get_magic_quotes_gpc() != 0)
  	{
    	if(is_array($value))  
			if ( array_is_associative($value) )
			{
				foreach( $value as $k=>$v)
					$tmp_val[$k] = stripslashes($v);
				$value = $tmp_val; 
			}				
			else  
				for($j = 0; $j < sizeof($value); $j++)
        			$value[$j] = stripslashes($value[$j]);
		else
			$value = stripslashes($value);
	}
	return $value;
}

function array_is_associative ($array){
    if ( is_array($array) && ! empty($array) )
    {
        for ( $iterator = count($array) - 1; $iterator; $iterator-- )
        {
            if ( ! array_key_exists($iterator, $array) ) { return true; }
        }
        return ! array_key_exists(0, $array);
    }
    return false;
}

function constructWhere($s){
    $qwery = "";
	//['eq','ne','lt','le','gt','ge','bw','bn','in','ni','ew','en','cn','nc']
    $qopers = array(
				  'eq'=>" = ",
				  'ne'=>" <> ",
				  'lt'=>" < ",
				  'le'=>" <= ",
				  'gt'=>" > ",
				  'ge'=>" >= ",
				  'bw'=>" LIKE ",
				  'bn'=>" NOT LIKE ",
				  'in'=>" IN ",
				  'ni'=>" NOT IN ",
				  'ew'=>" LIKE ",
				  'en'=>" NOT LIKE ",
				  'cn'=>" LIKE " ,
				  'nc'=>" NOT LIKE " );
    if ($s) {
        $jsona = json_decode($s,true);
        if(is_array($jsona)){
			$gopr = $jsona['groupOp'];
			$rules = $jsona['rules'];
            $i =0;
            foreach($rules as $key=>$val) {
                $field = $val['field'];
                $op = $val['op'];
                $v = $val['data'];
				
				if($field == 'ts_apDate' || $field == 'uEndDate')
				{
					$v = dateToMySQLDate($v);
				}
				
				if($v && $op) {
	                $i++;
					// ToSql in this case is absolutley needed
					$v = ToSql($field,$op,$v);
					if ($i == 1) $qwery = " AND ";
					else $qwery .= " " .$gopr." ";
					switch ($op) {
						// in need other thing
					    case 'in' :
					    case 'ni' :
					        $qwery .= $field.$qopers[$op]." (".$v.")";
					        break;
						default:
					        $qwery .= $field.$qopers[$op].$v;
					}
				}
            }
        }
    }
    return $qwery;
}

function ToSql ($field, $oper, $val) {
	// we need here more advanced checking using the type of the field - i.e. integer, string, float
	switch ($field) {
		case 'id':
		case 'position':
			return intval($val);
			break;
		case 'amount':
		case 'tax':
		case 'total':
			return floatval($val);
			break;
		default :
			//mysql_real_escape_string is better
			if($oper=='bw' || $oper=='bn') return "'" . addslashes($val) . "%'";
			else if ($oper=='ew' || $oper=='en') return "'%" . addcslashes($val) . "'";
			else if ($oper=='cn' || $oper=='nc') return "'%" . addslashes($val) . "%'";
			else return "'" . addslashes($val) . "'";
	}
}

function get_web_page( $url, $data='',$get=true,$getheaders=false,$proxy=''){
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => $getheaders,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 10,      // timeout on connect
        CURLOPT_TIMEOUT        => 10,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
		CURLOPT_POSTFIELDS	   => $data,
		CURLOPT_HTTPGET		   => $get,
		CURLOPT_COOKIESESSION  => true,
		CURLOPT_FORBID_REUSE  => true,
		CURLOPT_FRESH_CONNECT  => true,
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
	if($proxy != '')
	{
		curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1); 
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
	}
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $info  = curl_getinfo( $ch );

	if($getheaders)
	{
		$headers = substr($content, 0, $info['header_size']);
		$content = substr($content, $info['header_size']);
	}
	
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
	$header['headers'] = $headers;
    return $header;
}


function CheckHash($Hashnum){
    $CheckByte = 0;
    $Flag = false;

    $HashStr = sprintf('%u', $Hashnum) ;
    $length = strlen($HashStr);
	
    for ($i = $length - 1;  $i >= 0;  $i --) {
        $Re = $HashStr{$i};
        if ($Flag) {              
            $Re += $Re;     
            $Re = (int)($Re / 10) + ($Re % 10);
        }
        $CheckByte += $Re;
        $Flag = !$Flag;	
    }

    $CheckByte %= 10;
    if (0 !== $CheckByte) {
        $CheckByte = 10 - $CheckByte;
        if ($Flag) {
            if (1 === ($CheckByte % 2)) {
                $CheckByte += 9;
            }
            $CheckByte >>= 1;
        }
    }

    return '7'.$CheckByte.$HashStr;
}

function HashURL($String){
    $Check1 = ConvertStrToInt($String, 0x1505, 0x21);
    $Check2 = ConvertStrToInt($String, 0, 0x1003F);

    $Check1 >>= 2; 	
    $Check1 = (($Check1 >> 4) & 0x3FFFFC0 ) | ($Check1 & 0x3F);
    $Check1 = (($Check1 >> 4) & 0x3FFC00 ) | ($Check1 & 0x3FF);
    $Check1 = (($Check1 >> 4) & 0x3C000 ) | ($Check1 & 0x3FFF);	
	
    $T1 = (((($Check1 & 0x3C0) << 4) | ($Check1 & 0x3C)) <<2 ) | ($Check2 & 0xF0F );
    $T2 = (((($Check1 & 0xFFFFC000) << 4) | ($Check1 & 0x3C00)) << 0xA) | ($Check2 & 0xF0F0000 );
	
    return ($T1 | $T2);
}

function ConvertStrToInt($Str, $Check, $Magic){
    $Int32Unit = 4294967296;  // 2^32

    $length = strlen($Str);
    for ($i = 0; $i < $length; $i++) {
        $Check *= $Magic; 	
        if ($Check >= $Int32Unit) {
            $Check = ($Check - $Int32Unit * (int) ($Check / $Int32Unit));
        }
        $Check += ord($Str{$i}); 
    }
    return $Check;
}

function pbldom2array_full($node){

    $result = array();
    if($node->nodeType == XML_TEXT_NODE) 
    {
    	$result = $node->nodeValue;
    } else 
    {
    	if($node->hasAttributes()) 
    	{
    		$attributes = $node->attributes;
    		if((!is_null($attributes))&&(count($attributes))) 
    			foreach ($attributes as $index=>$attr) 
    		  	$result[$attr->name] = $attr->value;
    	}
    	if($node->hasChildNodes())
    	{
    		$children = $node->childNodes;
    		for($i=0;$i<$children->length;$i++) 
    		{
    			$child = $children->item($i);
    			if($child->nodeName != '#text')
    			if(!isset($result[$child->nodeName]))
    				$result[$child->nodeName] = pbldom2array($child);
    			else 
    			{
    				$aux = $result[$child->nodeName];
    				$result[$child->nodeName] = array( $aux );
    				$result[$child->nodeName][] = pbldom2array($child);
    			}
    		}
    	}
    }
    return $result;
} 

function pbldom2array($node){

  $res = array();
  if($node->nodeType == XML_TEXT_NODE)
  {
  	$res = $node->nodeValue;
  } else
  {
  	if($node->hasAttributes())
  	{
  		$attributes = $node->attributes;
  		if(!is_null($attributes))
  		{
  			$res['@attributes'] = array();
  			foreach ($attributes as $index=>$attr) 
  			{
  				$res['@attributes'][$attr->name] = $attr->value;
  			}
  		}
  	}
  	if($node->hasChildNodes())
  	{
  		$children = $node->childNodes;
  		for($i=0;$i<$children->length;$i++)
  		{
  			$child = $children->item($i);
  			$res[$child->nodeName] = pbldom2array($child);
  		}
  		$res['textContent']=$node->textContent;
  	}
  }
  return $res;
}

function pblgetContent($NodeContent="",$nod){  
  
	$NodList=$nod->childNodes;
	for( $j=0 ;  $j < $NodList->length; $j++ )
	{ 
		$nod2=$NodList->item($j);
		$nodemane=$nod2->nodeName;
		$nodevalue=$nod2->nodeValue;
		if($nod2->nodeType == XML_TEXT_NODE)
		    $NodeContent .= $nodevalue;
		else
		{     $NodeContent .= "<$nodemane ";
		   $attAre=$nod2->attributes;
		   foreach ($attAre as $value)
		      $NodeContent .= "{$value->nodeName}='{$value->nodeValue}'" ;
		    $NodeContent .= ">";                    
		    pblgetContent($NodeContent,$nod2);                    
		    $NodeContent .= "</$nodemane>";
		}
	}
   
}

function current_time( $type, $gmt = 0 ) {
	$t =  gmdate( 'Y-m-d H:i:s', ( time() + ( ($gmt - 6) * 3600 ) ) );
	switch ( $type ) {
		case 'mysql':
			return $t;
			break;
		case 'timestamp':
			return strtotime($t);
			break;
	}
}

function dateTotimestamp($eurodate){
	$arr = explode ("/", $eurodate);
	return strtotime($arr[2].'-'.$arr[1].'-'.$arr[0]);
}

function dateToMySQLDate($eurodate){
	$arr = explode ("/", $eurodate);
	return $arr[2].'-'.$arr[1].'-'.$arr[0];
}

function dateUsaToMySQLDate($eurodate){
	$arr = explode ("/", $eurodate);
	return $arr[2].'-'.$arr[0].'-'.$arr[1];
}

function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return dirname($pageURL);
}

function doPost($host,$port,$path,$postdata){

	$fp = fsockopen($host, $port, $errno, $errstr, $timeout = 30); 

	if(!$fp){ 
	 //error tell us 
	 //echo "$errstr ($errno)\n"; 
	 send_mail('admin@shivukclick.co.il','crazyio2005@gmail.com','SMS Error',"$errstr ($errno)",'','','','','mail.shivukclick.co.il','admin+shivukclick.co.il','pa55w0rd');
	   
	}else{ 

	  //send the server request 
	  fputs($fp, "POST $path HTTP/1.1\r\n"); 
	  fputs($fp, "Host: $host\r\n"); 
	  fputs($fp, "Content-Type: text/xml; charset=UTF-8\r\n"); 
	  fputs($fp, "Content-length: ".strlen($postdata)."\r\n"); 
	  fputs($fp, "Connection: close\r\n\r\n"); 
	  fputs($fp, $postdata . "\r\n\r\n"); 

	  //loop through the response from the server 
	  while(!feof($fp)) { 
		$resString .= fgets($fp, 4096); 
	  }
	  //close fp - we are done with it 
	  fclose($fp); 
	}   
}

function updateSubCompData($userID,$compID,$subCompID=0){
	if($subCompID != 0)
	{
		$delSQL = "delete from tz_subcomp_data where sb_subcomp=".$subCompID;
		dbExecute($delSQL);
	}
	
	$compDate = dbGetRow("SELECT co_start_date,co_type FROM tz_compain where co_id=$compID");
	
	if($compDate["co_type"]== "Facebook")
	{
		$usersSQL = "select id,uStartDate from tz_members where id=".$userID;
	}
	else
	{
		$usersSQL = "select id,uStartDate from tz_members where admin=".$userID;
	}
	$usersList = dbGetTable( $usersSQL );
	

	foreach ($usersList as $oneUser) 
	{
		$subCompSQL = "select subco_id,subco_days from tz_subcompain where subco_comp_id=".$compID." order by subco_id";
		$subCompList = dbGetTable( $subCompSQL );
		$nextRun = $oneUser["uStartDate"];
		if(date('Y-m-d',strtotime($nextRun)) < date('Y-m-d',strtotime($compDate["co_start_date"])))
		{
			$nextRun = $compDate["co_start_date"];
		}
		
		if(count($subCompList) > 0)
		{
			foreach ($subCompList as $oneSubComp) 
			{
				$nextRun = date('Y-m-d',strtotime(date("Y-m-d", strtotime($nextRun)) . " +".$oneSubComp["subco_days"]." days"));
				$sqlInsert = "INSERT INTO tz_subcomp_data (sd_user,sb_subcomp,sb_date) VALUES (".$oneUser["id"].",".$oneSubComp["subco_id"].",'".$nextRun."') ON DUPLICATE KEY UPDATE sb_date='$nextRun'";
				dbExecute($sqlInsert);
			}
		}
	}
}

function readExcell($ext,$file_name){
	
	$objPHPExcel = new PHPExcel();
	if($ext == 'xls')
	{
		$objReader = new PHPExcel_Reader_Excel5();
		
	}
	else
	{
		$objReader = new PHPExcel_Reader_Excel2007();
	}
	
	$objReader->setReadDataOnly(true);
	$objPHPExcel = $objReader->load("uploads/".$file_name);
	
	$sheet = $objPHPExcel->getActiveSheet();
	$rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
	
	$sheet = $objPHPExcel->getActiveSheet();
	$array_data = array();
	foreach($rowIterator as $row){
		if($row->getRowIndex () == 1) continue;//skip first row
		$rowIndex = $row->getRowIndex ();
		$array_data[$rowIndex] = array('A'=>'','B'=>'','C'=>'','D'=>'','E'=>'','F'=>'','G'=>'','H'=>'','I'=>'','J'=>'','K'=>'','L'=>'','M'=>'','N'=>'','O'=>'','P'=>'','Q'=>'','R'=>'','S'=>'','T'=>'','U'=>'','V'=>'','W'=>'','X'=>'','Y'=>'','Z'=>'','AA'=>'');
	 
		$cell = $sheet->getCell('A' . $rowIndex);
		$array_data[$rowIndex]['A'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('B' . $rowIndex);
		$array_data[$rowIndex]['B'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('C' . $rowIndex);
		$array_data[$rowIndex]['C'] = trim(PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'YYYY-MM-DD'));
		$cell = $sheet->getCell('D' . $rowIndex);
		$array_data[$rowIndex]['D'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('E' . $rowIndex);
		$array_data[$rowIndex]['E'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('F' . $rowIndex);
		$array_data[$rowIndex]['F'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('G' . $rowIndex);
		$array_data[$rowIndex]['G'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('H' . $rowIndex);
		$array_data[$rowIndex]['H'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('I' . $rowIndex);
		$array_data[$rowIndex]['I'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('J' . $rowIndex);
		$array_data[$rowIndex]['J'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('K' . $rowIndex);
		$array_data[$rowIndex]['K'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('L' . $rowIndex);
		$array_data[$rowIndex]['L'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('M' . $rowIndex);
		$array_data[$rowIndex]['M'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('N' . $rowIndex);
		$array_data[$rowIndex]['N'] = trim(PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'YYYY-MM-DD'));
		$cell = $sheet->getCell('O' . $rowIndex);
		$array_data[$rowIndex]['O'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('P' . $rowIndex);
		$array_data[$rowIndex]['P'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('Q' . $rowIndex);
		$array_data[$rowIndex]['Q'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('R' . $rowIndex);
		$array_data[$rowIndex]['R'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('S' . $rowIndex);
		$array_data[$rowIndex]['S'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('T' . $rowIndex);
		$array_data[$rowIndex]['T'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('U' . $rowIndex);
		$array_data[$rowIndex]['U'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('V' . $rowIndex);
		$array_data[$rowIndex]['V'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('W' . $rowIndex);
		$array_data[$rowIndex]['W'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('X' . $rowIndex);
		$array_data[$rowIndex]['X'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('Y' . $rowIndex);
		$array_data[$rowIndex]['Y'] = trim($cell->getCalculatedValue());
		$cell = $sheet->getCell('Z' . $rowIndex);
		$array_data[$rowIndex]['Z'] = trim(PHPExcel_Style_NumberFormat::toFormattedString($cell->getCalculatedValue(), 'YYYY-MM-DD'));
		$cell = $sheet->getCell('AA' . $rowIndex);
		$array_data[$rowIndex]['AA'] = trim($cell->getCalculatedValue());
	}
	return $array_data;
}

function checkExcellDo($impAdmin,$impCategory,$array_data){
	if($impCategory == '')
	{
		$impCategory = "0";
	}

	$usersSQL = "select * from tz_members where admin=$impAdmin and uCategory=$impCategory";
	$usersList = dbGetTable( $usersSQL );
	
	$citySQL = "select city_name from tz_city";
	$cityList = dbGetTable( $citySQL );
	$ug_private = "0";
	
	$adminRow = dbGetRow("SELECT * FROM tz_members WHERE id = $impAdmin");
	if($adminRow["custType"] == "1")
	{
		$categoryRow = dbGetRow("SELECT * FROM tz_users_group where ug_id=".$impCategory);
		if($categoryRow["ug_private"] != '')
		{
			$ug_private = $categoryRow["ug_private"];
		}
	}
	
	$rowNumber = 2;
	$errorString = '';
	$array_email = array();
	$array_mobile = array();
	$array_tz = array();
	$array_user = array();
	foreach($array_data as $oneUser){
		
		if($oneUser['K'] !='' && substr($oneUser['K'], 0, 1) != '0' && substr($oneUser['K'], 0, 1) != '1' && substr($oneUser['K'], 0, 1) != '*')
		{
			$oneUser['K'] = "0".$oneUser['K'];
		}
		if($oneUser['L'] !='' && substr($oneUser['L'], 0, 1) != '0' && substr($oneUser['L'], 0, 1) != '1' && substr($oneUser['L'], 0, 1) != '*')
		{
			$oneUser['L'] = "0".$oneUser['L'];
		}
		if($oneUser['M'] !='' && substr($oneUser['M'], 0, 1) != '0' && substr($oneUser['M'], 0, 1) != '1' && substr($oneUser['M'], 0, 1) != '*')
		{
			$oneUser['M'] = "0".$oneUser['M'];
		}
		
		if($adminRow["custType"] == "1")
		{
			if($ug_private == "0")
			{
				if($oneUser['A'] == '')
				{
					$errorString .= "שורה מספר ".$rowNumber." שם משתמש ריק."."<br />";
				}
				else if (!preg_match('/^[a-zA-Z0-9]+$/', $oneUser['A']))
				{
					$errorString .= "שורה מספר ".$rowNumber." לא ניתן להשתמש בתוים מיוחדים בשם משתמש"."<br />";
				}
				else
				{
					if (in_array($oneUser['A'], $array_user)) 
					{
						$errorString .= "שורה מספר ".$rowNumber." שם משתמש קיים בשורה נוספת."."<br />";
					}
					else
					{
						array_push($array_user, $oneUser['A']);
					}
				}
				
				if($oneUser['B'] == '')
				{
					$errorString .= "שורה מספר ".$rowNumber." סיסמה ריקה."."<br />";
				}
				
				if($oneUser['C'] == '')
				{
					$errorString .= "שורה מספר ".$rowNumber." תאריך סיום ריק."."<br />";
				}
			}
			
			if($oneUser['D'] == '')
			{
				$errorString .= "שורה מספר ".$rowNumber." שם העסק ריק."."<br />";
			}
		}
		else
		{
			if($oneUser['E'] == '')
			{
				$errorString .= "שורה מספר ".$rowNumber." שם פרטי ריק."."<br />";
			}
		}
		
		if($oneUser['L'] != '')
		{
			if(!validateCellPhone($oneUser['L']))
			{
				$errorString .= "שורה מספר ".$rowNumber." טלפון נייד לא חוקי."."<br />";
			}
			
			if (in_array($oneUser['L'], $array_mobile)) 
			{
				$errorString .= "שורה מספר ".$rowNumber." טלפון נייד קיים בשורה נוספת."."<br />";
			}
			else
			{
				array_push($array_mobile, $oneUser['L']);
			}
		}
			
		if($oneUser['K'] != '' && !validatePhone($oneUser['K']))
		{
			$errorString .= "שורה מספר ".$rowNumber." טלפון עבודה לא חוקי."."<br />";
		}
		
		if($oneUser['M'] != '' && !validatePhone($oneUser['M']))
		{
			$errorString .= "שורה מספר ".$rowNumber." טלפון בית לא חוקי."."<br />";
		}
		
		if($oneUser['P'] != 'אין')
		{
			if($oneUser['P'] == '')
			{
				$errorString .= "שורה מספר ".$rowNumber." דואר אלקטרוני ריק."."<br />";
			}
			else if (!checkEmail($oneUser['P']))
			{
				$errorString .= "שורה מספר ".$rowNumber." דואר אלקטרוני לא חוקי."."<br />";
			}
			else
			{
				if (in_array($oneUser['P'], $array_email)) 
				{
					$errorString .= "שורה מספר ".$rowNumber." דואר אלקטרוני קיים בשורה נוספת."."<br />";
				}
				else
				{
					array_push($array_email, $oneUser['P']);
				}
			}
		}
		
		if (!checkEmail($oneUser['Q']) && $oneUser['Q'] != '')
		{
			$errorString .= "שורה מספר ".$rowNumber." Facebook לא חוקי."."<br />";
		}
		
		if($oneUser['O'] == 'ז')
		{
			$oneUser['O'] = 'זכר';
		}
		if($oneUser['O'] == 'נ')
		{
			$oneUser['O'] = 'נקבה';
		}
		if($oneUser['O'] != '' && $oneUser['O'] != 'זכר' && $oneUser['O'] != 'נקבה')
		{
			$errorString .= "שורה מספר ".$rowNumber." מין יכול להכיל זכר או נקבה."."<br />";
		}

		$oneUser['C'] = fix_date($oneUser['C']);
		if($oneUser['C'] != '' && !valid_date($oneUser['C']))
		{
			$errorString .= "שורה מספר ".$rowNumber." תאריך סיום לא חוקי."."<br />";
		}
		
		$oneUser['N'] = fix_date($oneUser['N']);	
		if($oneUser['N'] != '' && !valid_date($oneUser['N']))
		{
			$errorString .= "שורה מספר ".$rowNumber." תאריך לידה לא חוקי."."<br />";
		}
		
		$oneUser['Z'] = fix_date($oneUser['Z']);
		if($oneUser['Z'] != '' && !valid_date($oneUser['Z']))
		{
			$errorString .= "שורה מספר ".$rowNumber." תאריך נישואין לא חוקי."."<br />";
		}
		
		if($oneUser['AA'] != '')
		{
			if(!validateNumber($oneUser['AA'],9))
			{
				$errorString .= "שורה מספר ".$rowNumber." תעודת זהות לא חוקי, חייב להיות בעל 9 ספרות."."<br />";
			}
			
			if (in_array($oneUser['AA'], $array_tz)) 
			{
				$errorString .= "שורה מספר ".$rowNumber." תעודת זהות קיים בשורה נוספת."."<br />";
			}
			else
			{
				array_push($array_tz, $oneUser['AA']);
			}
		}
		
		if(count($usersList) > 0)
		{
			foreach ($usersList as $dbUser) 
			{
				if($oneUser['P'] != '' && $oneUser['P'] != 'אין' && $oneUser['P'] == $dbUser["email"])
				{
					$errorString .= "שורה מספר ".$rowNumber." דואר אלקטרוני כבר קיים."."<br />";
				}
				
				$vowels = array("-", " ", "+", "/");
				if($oneUser['L'] != '' && str_replace($vowels, "", $oneUser['L']) == str_replace($vowels, "", $dbUser["uPhone2"]))
				{
					$errorString .= "שורה מספר ".$rowNumber." טלפון נייד כבר קיים."."<br />";
				}
				
				if($oneUser['AA'] != '' && $oneUser['AA'] == $dbUser["usr_tz"])
				{
					$errorString .= "שורה מספר ".$rowNumber." תעודת זהות כבר קיימת."."<br />";
				}
			}
		}
		
		if($adminRow["custType"] == "1" && $oneUser['A'] != '')
		{
			if($ug_private == "0")
			{
				$userExist = dbGetVal("SELECT COUNT(ID) FROM tz_members WHERE usr='".$oneUser['A']."'");
				if($userExist > 0 )
				{
					$errorString .= "שורה מספר ".$rowNumber." שם משתמש כבר קיים."."<br />";
				}
			}
		}
	
		$haveCity = "0";
		if($oneUser['I'] != '')
		{
			foreach ($cityList as $dbCity) 
			{
				if($oneUser['I'] == $dbCity["city_name"])
				{
					$haveCity = "1";
					break;
				}
			}
			
			if($haveCity == "0")
			{
				$errorString .= "שורה מספר ".$rowNumber." ישוב לא קיים."."<br />";
			}
		}
		
		$rowNumber ++;
	}
	if($errorString =='')
	{
		echo "כל הנתונים תקינים.";
	}
	else
	{
		echo $errorString;
	}
}

function importExcellDo($impAdmin,$impCategory,$array_data,$impGroup=1){
	if($impCategory == '')
	{
		$impCategory = "0";
	}
	
	$ug_private = "0";
	
	$adminRow = dbGetRow("SELECT * FROM tz_members WHERE id = $impAdmin");
	if($adminRow["custType"] == "1")
	{
		$categoryRow = dbGetRow("SELECT * FROM tz_users_group where ug_id=".$impCategory);
		if($categoryRow["ug_private"] != '')
		{
			$ug_private = $categoryRow["ug_private"];
		}
	}
	
	foreach($array_data as $oneUser){
	
		if($oneUser['K'] !='' && substr($oneUser['K'], 0, 1) != '0' && substr($oneUser['K'], 0, 1) != '1' && substr($oneUser['K'], 0, 1) != '*')
		{
			$oneUser['K'] = "0".$oneUser['K'];
		}
		if($oneUser['L'] !='' && substr($oneUser['L'], 0, 1) != '0' && substr($oneUser['L'], 0, 1) != '1' && substr($oneUser['L'], 0, 1) != '*')
		{
			$oneUser['L'] = "0".$oneUser['L'];
		}
		if($oneUser['M'] !='' && substr($oneUser['M'], 0, 1) != '0' && substr($oneUser['M'], 0, 1) != '1' && substr($oneUser['M'], 0, 1) != '*')
		{
			$oneUser['M'] = "0".$oneUser['M'];
		}
		
		if($adminRow["custType"] == "1")
		{
			if($ug_private == "0")
			{
				$usr = addslashes($oneUser['A']);
				$pass = addslashes($oneUser['B']);
				$enteredEndDate = "'".fix_date($oneUser['C'])."'";
				$usr_left_sms = "10";
			}
			else
			{
				$usr = "_customer_";
				$pass = "_customer_";
				$enteredEndDate = "null";
				$usr_left_sms = "0";
			}
		}
		else
		{
			$usr = "_customer_";
			$pass = "_customer_";
			$enteredEndDate = "null";
			$usr_left_sms = "0";
		}
				
		$uBizName = addslashes($oneUser['D']);
		$uAddress = addslashes($oneUser['G']);
		$uPhone1 = addslashes($oneUser['K']);
		$uPhone2 = addslashes($oneUser['L']);
		$uName = addslashes($oneUser['E']);
		$email = $oneUser['P'];
		$uFacebook = $oneUser['Q'];
		$uURL = addslashes($oneUser['R']);
		$admin = $impAdmin;
		$uCategory = $impCategory;
		
		$mailName = $uName;
		if($mailName == '')
		{
			$mailName = $uBizName;
		}
		
		$isadmin = $adminRow["custType"];
		
		$uLastNane = addslashes($oneUser['F']);
		if($oneUser['O'] == 'ז')
		{
			$oneUser['O'] = 'זכר';
		}
		if($oneUser['O'] == 'נ')
		{
			$oneUser['O'] = 'נקבה';
		}
		$uSex = $oneUser['O'];
		
		if($uBizName == ''){$uBizName = $uName." ".$uLastNane;}
		
		if($oneUser['N'] != '')
		{
			$enteredDateBirth = "'".fix_date($oneUser['N'])."'";
			$age = "DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS($enteredDateBirth)), '%Y')+0";
		}
		else
		{
			$enteredDateBirth = "null";
			$age = "null";
		}
		
		if($oneUser['Z'] != '')
		{
			$enteredNisuinDate = "'".fix_date($oneUser['Z'])."'";
		}
		else
		{
			$enteredNisuinDate = "null";
		}
		$usr_tz = $oneUser['AA'];
		if($usr_tz == '')
		{
			$usr_tz = "null";
		}
		
		$uHomeNumber = addslashes($oneUser['H']);
		$uCity = $oneUser['I'];
		$uZip = addslashes($oneUser['J']);
		$uPhoneHome = addslashes($oneUser['M']);
		$uComments = addslashes($oneUser['Y']);
		$uSendSMS = ($oneUser['L'] != '') ? "1" : "0";
		$uSendMail = ($oneUser['P'] != '' && $oneUser['P'] != 'אין') ? "1" : "0";
		$uGr1 = ($oneUser['S'] == '1') ? "1" : "0";
		$uGr2 = ($oneUser['T'] == '1') ? "1" : "0";
		$uGr3 = ($oneUser['U'] == '1') ? "1" : "0";
		$uGr4 = ($oneUser['V'] == '1') ? "1" : "0";
		$uGr5 = ($oneUser['W'] == '1') ? "1" : "0";
		$uGr6 = ($oneUser['X'] == '1') ? "1" : "0";
		
		$custType="0";
		
		$insert = "insert into tz_members SET uFormGroup=$impGroup,usr_left_sms=$usr_left_sms,custType=$custType,uAge=$age,uLastNane='$uLastNane',uSex='$uSex',usr_tz=$usr_tz,usr_nisuin=$enteredNisuinDate,uDateBirth=$enteredDateBirth,uHomeNumber='$uHomeNumber',uCity='$uCity',uZip='$uZip',uPhoneHome='$uPhoneHome',uComments='$uComments',uSendSMS=$uSendSMS,uSendMail=$uSendMail,uGr1=$uGr1,uGr2=$uGr2,uGr3=$uGr3,uGr4=$uGr4,uGr5=$uGr5,uGr6=$uGr6,uBizName='$uBizName',uAddress='$uAddress',uPhone1='$uPhone1',uPhone2='$uPhone2',uName='$uName',email='$email',uFacebook='$uFacebook',uURL='$uURL',admin=$admin,uCategory=$uCategory,usr='$usr',pass='$pass',uEndDate = $enteredEndDate";
		dbExecute($insert);
		
		$compSQL = "select co_id from tz_compain where co_user=".$admin." and co_type='תקופתי'";
		$compList = dbGetTable( $compSQL );
		if(count($compList) > 0)
		{
			foreach ($compList as $oneComp) 
			{
				updateSubCompData($admin,$oneComp["co_id"]);
			}
		}

		
		$userRow = dbGetRow("select * from tz_members where email='".$email."' order by id desc limit 1");
		$compRow = dbGetRow("SELECT * FROM tz_compain WHERE co_activ = 1 and co_next_run <= CURRENT_DATE() and co_type='הצטרפות' and co_user = ".$adminRow["id"]." and co_category=$uCategory");
		if(count($compRow) > 0)
		{	
			if($compRow["co_mail"] != '' && $email != 'אין')
			{
				$body = $compRow["co_mail"];
	
				if($compRow["co_img"] != '')
				{
					$body = '<img src="'.curPageURL().'/'.$compRow["co_img"].'" /><br/>'.$body;
				}
				
				$emcode = $adminRow["id"].microtime(true);
				$body .= "<img src='http://shivukclick.co.il/club/logo.php?id=$emcode' height='1' width='1' />";
		
				$repTEXT = str_replace ( '#שם#' , $mailName , $body);
				$repTEXT = str_replace ( '#משפחה#' , $uLastNane , $repTEXT);
				$repTEXT = str_replace ( '#עסק#' , $uBizName , $repTEXT);
				$repTEXT = str_replace ( '#סיסמה#' , $pass , $repTEXT);
				$repTEXT = str_replace ( '#משתמש#' , $usr , $repTEXT);
				$repTEXT = str_replace ( '#uid#' , $userRow["id"] , $repTEXT);
				
				$repTEXT = str_replace ( '#שם#' , '' , $repTEXT);
				$repTEXT = str_replace ( '#משפחה#' , '' , $repTEXT);
				$repTEXT = str_replace ( '#עסק#' , '' , $repTEXT);
				$repTEXT = str_replace ( '#סיסמה#' , '' , $repTEXT);
				$repTEXT = str_replace ( '#משתמש#' , '' , $repTEXT);
				$repTEXT = str_replace ( '#uid#' , '' , $repTEXT);
				
				$repSubjectTEXT = str_replace ( '#שם#' , $mailName , $compRow["co_subject"]);
				$repSubjectTEXT = str_replace ( '#משפחה#' , $uLastNane , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#עסק#' , $uBizName , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#סיסמה#' , $pass , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#משתמש#' , $usr , $repSubjectTEXT);
				
				$repSubjectTEXT = str_replace ( '#שם#' , '' , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#משפחה#' , '' , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#עסק#' , '' , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#סיסמה#' , '' , $repSubjectTEXT);
				$repSubjectTEXT = str_replace ( '#משתמש#' , '' , $repSubjectTEXT);
				
				$encMail = base64_encode($userRow["email"].'|'.$userRow["id"]);
				$repTEXT .= '<br /><br /><br />'.$adminRow["usr_signature"];
				$repTEXT .= "<br /><br /><div align='center'><font color='#666666' face='Arial' style='font-size: 11px;'>נשלח באמצעות שיווק בקליק - <a href='http://www.shivukclick.co.il' target='_blank'>מכונת השיווק האוטומטית</a><br />הודעה זו נשלחה אליך כשרות&nbsp; להפצת מידע על מוצרים ושירותים בחינם לגולשים בהתאם לחוק הספאם במדינת ישראל.<br />הנך רשאי, בכל עת, להודיע על סירובך לקבל מאיתנו מידע חינם,</font><font color='#666666' face='Arial' style='font-size: 11px;'>ע&quot;י לחיצה על הקישור להסרה המופיע בתחתית ההודעה.</font><br /><br /><font color='#666666' face='Arial' style='font-size: 11px;'>"."על מנת להסיר את עצמך מרשימת התפוצה לחץ/י <a href='http://shivukclick.co.il/club/remove.php?id=$encMail'>כאן</a></font>"."</div>";
				send_mail(	$adminRow["email"],
								$email,
								$repSubjectTEXT,
								$repTEXT,$adminRow["uBizName"],$mailName,$compRow["co_files"]
								);
				$updateMail = "insert into tz_email_stats set eml_compain=".$compRow["co_id"].",eml_code='".$emcode."'";
				dbExecute($updateMail);
			}
			
			if($compRow["co_sms"] != '' && $adminRow["usr_left_sms"] > 0 != '' && $userRow["uPhone2"] != '')
			{
				$vowels = array("-", " ", "+", "/");
				$sms_num = '972'.substr(str_replace($vowels, "", $userRow["uPhone2"]),1);
				$sms_num_user = '972'.substr(str_replace($vowels, "", $adminRow["uPhone2"]),1);
	
				$repSmsTEXT = str_replace ( '#שם#' , $mailName , $compRow["co_sms"]);
				$repSmsTEXT = str_replace ( '#משפחה#' , $uLastNane , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#עסק#' , $uBizName , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#סיסמה#' , $pass , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#משתמש#' , $usr , $repSmsTEXT);
				
				$repSmsTEXT = str_replace ( '#שם#' , '' , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#משפחה#' , '' , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#עסק#' , '' , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#סיסמה#' , '' , $repSmsTEXT);
				$repSmsTEXT = str_replace ( '#משתמש#' , '' , $repSmsTEXT);
				
				$chargeSMS = "1";
				if(mb_strlen($repSmsTEXT,'UTF-8') > 70 && mb_strlen($repSmsTEXT,'UTF-8') <= 128)
				{
					$chargeSMS = "2";
				}
				if(mb_strlen($repSmsTEXT,'UTF-8') > 128)
				{
					$chargeSMS = "3";
				}
				
				if($adminRow["usr_left_sms"] - $chargeSMS >= 0)
				{
					send_sms($repSmsTEXT,$sms_num,$sms_num_user,1,$compRow["co_id"]);
					$updateSMS = "UPDATE tz_members SET usr_left_sms = usr_left_sms - $chargeSMS WHERE id = ". $adminRow["id"];
					dbExecute($updateSMS);
				}

			}
		}
			
	}
	echo "1";
}

function generatePassword ($length = 8){

    // start with a blank password
    $password = "";

    // define possible characters - any character in this string can be
    // picked for use in the password, so if you want to put vowels back in
    // or add special characters such as exclamation marks, this is where
    // you should do it
    $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

    // we refer to the length of $possible a few times, so let's grab it now
    $maxlength = strlen($possible);
  
    // check for length overflow and truncate if necessary
    if ($length > $maxlength) {
      $length = $maxlength;
    }
	
    // set up a counter for how many characters are in the password so far
    $i = 0; 
    
    // add random characters to $password until $length is reached
    while ($i < $length) { 

      // pick a random character from the possible ones
      $char = substr($possible, mt_rand(0, $maxlength-1), 1);
        
      // have we already used this character in $password?
      if (!strstr($password, $char)) { 
        // no, so it's OK to add it onto the end of whatever we've already got...
        $password .= $char;
        // ... and increase the counter by one
        $i++;
      }

    }

    // done!
    return $password;

  }
  
 function addHistory($bizid, $action) {
 dbExecute("insert into tbl_history set 
						his_biz_id=$bizid,
						his_action='$action'
						");
}

 function addAPI($tbl, $ap_device_id, $ap_device_model,$ap_system_name,$ap_system_version,$ap_device_type,$ap_phone_no,$ap_app_version,$ap_device_orientation,$ap_lon,$ap_lat,$ap_lang,$ap_category,$ap_sub_category,$ap_biz_id,$ap_mod_id,$ap_level_id,$ap_record_id,$ap_in_favorites,$ap_country_id,$ap_state_id,$ap_search_term='',$marketId=-1) {
    
	$params = array(
		"tbl" => $tbl,
		"ap_device_id" => $ap_device_id,
		"ap_device_model" => $ap_device_model,
		"ap_system_name" => $ap_system_name,
		"ap_system_version" => $ap_system_version,
		"ap_device_type" => $ap_device_type,
		"ap_phone_no" => $ap_phone_no,
		"ap_app_version" => $ap_app_version,
		"ap_device_orientation" => $ap_device_orientation,
		"ap_lon" => $ap_lon,
		"ap_lat" => $ap_lat,
		"ap_lang" => $ap_lang,
		"ap_category" => $ap_category,
		"ap_sub_category" => $ap_sub_category,
		"ap_biz_id" => $ap_biz_id,
		"ap_mod_id" => $ap_mod_id,
		"ap_level_id" => $ap_level_id,
		"ap_record_id" => $ap_record_id,
		"ap_in_favorites" => $ap_in_favorites,
		"ap_country_id" => $ap_country_id,
		"ap_state_id" => $ap_state_id,
        "ap_search_term" => $ap_search_term,
        "ap_market_id" => $marketId
	);
	
	$url = "https://app.bobile.com/fillAPI.php";

	foreach ($params as $key => &$val) {
		if (is_array($val)) $val = implode(',', $val);
		$post_params[] = $key.'='.urlencode($val);
	}
	$post_string = implode('&', $post_params);
	
	// Open connection
	$ch = curl_init();

	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_string);

	// Execute post
	curl_exec($ch);

	// Close connection
	curl_close($ch);
	
	
	
 }
 
 function encryptIt( $q ) {
     $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
     $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
     return( $qEncoded );
 }

 function decryptIt( $q ) {
     $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
     $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
     return( $qDecoded );
 }
 
 function startsWith($haystack, $needle)
 {
     return $needle === "" || strpos($haystack, $needle) === 0;
 }
 
 function endsWith($haystack, $needle)
 {
     return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
 }
 
 function async($url, $params = array())
 {
     
     $post_params = array();

     foreach ($params as $key => &$val) 
     {
         $post_params[] = $key.'='.$val;
     }
     
     $post_string = implode('&', $post_params);

     //open connection
     $ch = curl_init();

     //set the url, number of POST vars, POST data
     curl_setopt($ch,CURLOPT_URL, $url);
     curl_setopt($ch,CURLOPT_POST, count($params));
     curl_setopt($ch,CURLOPT_POSTFIELDS, $post_string);

     //execute post
     $result = curl_exec($ch);

     
     //$fh = fopen("debug.txt", 'a');
     //fwrite($fh, "result - ".$result);
     //fclose($fh);
     
     //close connection
     curl_close($ch);
     
     
     
     //$parts=parse_url($url);

     //$fp = fsockopen($parts['host'], isset($parts['port'])?$parts['port']:80,$errno, $errstr, 30);

     //$out = "POST ".$parts['path']." HTTP/1.1\r\n";
     //$out.= "Host: ".$parts['host']."\r\n";
     //$out.= "Content-Type: application/x-www-form-urlencoded\r\n";
     //$out.= "Content-Length: ".strlen($post_string)."\r\n";
     //$out.= "Connection: Close\r\n\r\n";
     //if (isset($post_string)) $out.= $post_string;

     //fwrite($fp, $out);
     //fclose($fp);

 }

 
function setFunctions($bizid){

    dbExecute("DELETE FROM tbl_biz_functions WHERE bfu_biz_id = $bizid");

    $funcData = dbGetRow("SELECT biz_short_name, biz_office_tele, biz_mobile_tele, biz_addr_country_id,biz_addr_town, biz_addr_street, biz_addr_no FROM tbl_biz WHERE biz_id = $bizid");

    dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index) values (3, $bizid, 'Install', 1)");
    dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index,bfu_info) values (6, $bizid, 'Share', 2,'Wanted to share this app with you! Simply download bobile app & search for {$funcData["biz_short_name"]} - Have Fun!')");
    dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index) values (7, $bizid, 'Rate',3)");
    dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index) values (4, $bizid, 'Email', 4)");

    $newIndex=5;
    
    if($funcData["biz_mobile_tele"] != "" || $funcData["biz_office_tele"]) {
        dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index) values (1, $bizid, 'Call', $newIndex)");
        $newIndex++;
    }

     if($funcData["biz_mobile_tele"] != "") {
        dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index) values (2, $bizid, 'SMS', $newIndex)");
        $newIndex++;
    }


    if($funcData["biz_addr_country_id"] != "" && $funcData["biz_addr_town"] != "" && $funcData["biz_addr_street"] !="") {
        dbExecute("insert into tbl_biz_functions (bfu_func_id, bfu_biz_id, bfu_label,bfu_index) values (5, $bizid, 'Location', $newIndex)");
        $newIndex++;
    }
}

function setUpOperationHours($bizid,$hoursData){

    /*
        $hoursCollection["0"]["start"] = $responseData->hours->mon_1_open;
        $hoursCollection["0"]["end"] = $responseData->hours->mon_1_close;
        $hoursCollection["0"]["name"] = "Monday";

    */

    $counter = dbGetVal("SELECT count(bch_id) vol FROM tbl_biz_cal_hours WHERE bch_biz_id = $bizid");

    if ($counter == "0"){
        $i=0;
        foreach ($hoursData as $dayRow) {
            $daySQL =  "INSERT INTO tbl_biz_cal_hours 
                        SET bch_biz_id = $bizid,
                        bch_day_no = $i,
                        bch_day_name = '{$dayRow["name"]}',
                        bch_begin_hour = (SELECT hour_id FROM calendar_hours WHERE hour_start LIKE '{$dayRow["start"]}'),
                        bch_finish_hour = (SELECT hour_id FROM calendar_hours WHERE hour_end LIKE '{$dayRow["end"]}')
                        ";

            //echo "$daySQL <br><br>";

            dbExecute($daySQL);
            $i++;
        }
    }
}

function setUpAboutData($bizid,$data){


    $i=0;
    if (count($data) > 0){
        foreach ($data as $aboutRecord) {

            $required = 0;
            if ($i=="0"){
                $required = 1;
            }

            $header = addslashes($aboutRecord["header"]);
            $info = addslashes($aboutRecord["info"]);
            
            dbExecute("INSERT INTO tbl_mod_data1 SET
                        md_biz_id = $bizid,
                        md_mod_id = 1,
                        md_level_no = 1,
                        md_parent = 0,
                        md_index = $i,
                        md_element = 6,
                        md_editble = 1,
                        md_required = $required,
                        md_dragble = 1,
                        md_content = 0,
                        md_pic = '',
                        md_head = '$header',
                        md_desc = '', 
                        md_info = '$info';"
                            );
            $i++;

        }
    }
}
 
function getYouTubeID($ytURL)
{
    $posg = strrpos(strtoupper($ytURL), 'YOUTU');
    if($posg != false)
    {
        $link = str_replace( '-nocookie.com/v/' , '.com/watch?v=' ,$ytURL);
        $pattern = "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\??\n]+|(?<=youtu.be/)[^&\n]+#";
        preg_match($pattern, $link, $matches);

        if(isset($matches)){
            if($matches[0] == "")
            {
                $pattern = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";
                preg_match($pattern, $link, $matches);
            }
            
            $videoID = $matches[0];
            $posg = strrpos(strtoupper($videoID), 'YOUTU');
            
            if($videoID != '' && $posg == false)
            {
                return $videoID;
            }
        }
    }
    return "";
}

function getVimeoObject($video_url){
    $oembed_endpoint = 'http://vimeo.com/api/oembed';
    // Create the URLs
    $xml_url = $oembed_endpoint . '.xml?url=' . rawurlencode($video_url) . '&width=640';
    // Load in the oEmbed XML
    return simplexml_load_string(vimeoCurl_get($xml_url));
}

// Curl helper function
function vimeoCurl_get($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
}

function strContains($haystack, $needle,$isCaseSensetive = false)
{
    if(!$isCaseSensetive){
        $haystack = strtolower($haystack);
        $needle = strtolower($needle);
    }
    $pos = strrpos($haystack, $needle);
    if($pos !== false) {
        return true;
    }
        
    return false;
}

class SimpleImage {
 
   var $image;
   var $image_type;
 
   function load($filename) {
 
	  $image_info = getimagesize($filename);
	  $this->image_type = $image_info[2];
	  if( $this->image_type == IMAGETYPE_JPEG ) {
 
		 $this->image = imagecreatefromjpeg($filename);
	  } elseif( $this->image_type == IMAGETYPE_GIF ) {
 
		 $this->image = imagecreatefromgif($filename);
	  } elseif( $this->image_type == IMAGETYPE_PNG ) {
 
		 $this->image = imagecreatefrompng($filename);
	  }
   }
   function save($filename, $image_type=IMAGETYPE_JPEG, $compression=75, $permissions=null) {
 
	  if( $image_type == IMAGETYPE_JPEG ) {
		 imagejpeg($this->image,$filename,$compression);
	  } elseif( $image_type == IMAGETYPE_GIF ) {
 
		 imagegif($this->image,$filename);
	  } elseif( $image_type == IMAGETYPE_PNG ) {
 
		 imagepng($this->image,$filename);
	  }
	  if( $permissions != null) {
 
		 chmod($filename,$permissions);
	  }
   }
   function output($image_type=IMAGETYPE_JPEG) {
 
	  if( $image_type == IMAGETYPE_JPEG ) {
		 imagejpeg($this->image);
	  } elseif( $image_type == IMAGETYPE_GIF ) {
 
		 imagegif($this->image);
	  } elseif( $image_type == IMAGETYPE_PNG ) {
 
		 imagepng($this->image);
	  }
   }
   function getWidth() {
 
	  return imagesx($this->image);
   }
   function getHeight() {
 
	  return imagesy($this->image);
   }
	function getDirection() {
 
	  $imgX =  imagesx($this->image);
	  $imgY = imagesy($this->image);
	  
	  if($imgX >= $imgY)
		return 'Horizontal';
	  else
		return 'Vertical';
	  
   }
   function resizeToHeight($height) {
 
	  $ratio = $height / $this->getHeight();
	  $width = $this->getWidth() * $ratio;
	  $this->resize($width,$height);
   }
 
   function resizeToWidth($width) {
	  $ratio = $width / $this->getWidth();
	  $height = $this->getheight() * $ratio;
	  $this->resize($width,$height);
   }
 
   function scale($scale) {
	  $width = $this->getWidth() * $scale/100;
	  $height = $this->getheight() * $scale/100;
	  $this->resize($width,$height);
   }
 
   function resize($width,$height) {
	  $new_image = imagecreatetruecolor($width, $height);
	  imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
	  $this->image = $new_image;
   }  

}

?>