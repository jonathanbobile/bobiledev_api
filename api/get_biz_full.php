<?php
	require 'connect.php';
	require 'functions.php';
	
	foreach ($_GET as $key => $value)
	{
		$par = $par."$key has a value of $value <br />";
	}
	$repTEXT = "get_biz<br />".$par;
	
	
	// start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
	$lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
	$deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
	$bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    // end global parameters
	
	$nib_name = "";
	if($android == "1")
	{
		$nib_name = "_android";
	}
	
	if($iphone == "0")
	{
		$mainwidth = "620";
	}
	else if($iphone == "1")
	{
		$mainwidth = "310";
	}
	else
	{
		if($iphone > 620)
		{
			$mainwidth = "620";
		}
		else
		{
			$mainwidth = "100%";
		}
	}
	
	if($state == '')
	{
		$state = "0";
	}
	
    addAPI("api_get_biz_full",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
	
    if($iphone == "0" || $iphone == "1")
	{
        //addAPI("api_get_biz",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
    }
	$bizSQL = "select * from tbl_biz where biz_id=$bizid";
	$bizRow = dbGetRow( $bizSQL );
    $bizLogo = dbGetVal("select max(md_pic) from tbl_mod_data0 where md_mod_id=0 and md_biz_id=$bizid limit 1");
    
        $mainMenuSQL = "select * from tbl_biz_mod, tbl_mod_struct
                        where biz_mod_biz_id = $bizid
                        and biz_mod_mod_id = 0
                        and biz_mod_stuct = ms_template
                ";
        $mainMenuRow = dbGetRow( $mainMenuSQL );
	
	$modulesCount = dbGetVal("select count(biz_mod_mod_id) biz_mod_mod_id from tbl_biz_mod where biz_mod_active=1 and biz_mod_mod_id <> 0 and biz_mod_biz_id=$bizid");
	
	$aboutUsRow = dbGetRow( "select * 
                                from tbl_biz_mod,tbl_mod_struct
                                where biz_mod_active=1 
                                and biz_mod_biz_id = $bizid 
                                and biz_mod_mod_id = 1
                                and biz_mod_stuct = ms_template" );
	
	$tmplSQL = "select * from tbl_template where tmpl_id = " . $bizRow["biz_theme"];
	$tmplRow = dbGetRow( $tmplSQL );
    $tname = $tmplRow["tmpl_name"];
    $tid = $tmplRow["tmpl_id"];
    $ttransparent = $tmplRow["tmpl_transparent"];
    $tFont = $tmplRow["tmpl_font"];
    $tFontColor = $tmplRow["tmpl_font_color"];
    $tHeadColor = $tmplRow["tmpl_head_color"];
    $templateFeel = $tmplRow["tmpl_img_feel"];
    
	$tmpl_html_out = $aboutUsRow["ms_html"];
	
	if($iphone == "0" || $iphone >= 620)
	{
		$aboutUsRow["ms_img1_w"] = $aboutUsRow["ms_img1_w"] * 2;
		$aboutUsRow["ms_img2_w"] = $aboutUsRow["ms_img2_w"] * 2;
		$aboutUsRow["ms_img3_w"] = $aboutUsRow["ms_img3_w"] * 2;
		$aboutUsRow["ms_img4_w"] = $aboutUsRow["ms_img4_w"] * 2;
		$aboutUsRow["ms_img5_w"] = $aboutUsRow["ms_img5_w"] * 2;
		$aboutUsRow["ms_img6_w"] = $aboutUsRow["ms_img6_w"] * 2;
		$aboutUsRow["ms_img7_w"] = $aboutUsRow["ms_img7_w"] * 2;
		$aboutUsRow["ms_img8_w"] = $aboutUsRow["ms_img8_w"] * 2;
		$aboutUsRow["ms_img9_w"] = $aboutUsRow["ms_img9_w"] * 2;
		$aboutUsRow["ms_img10_w"] = $aboutUsRow["ms_img10_w"] * 2;
	}
	else if ($iphone != "1")
	{
		$aboutUsRow["ms_img1_w"] = floor($iphone * (($aboutUsRow["ms_img1_w"] / 3.2) / 100));
		$aboutUsRow["ms_img2_w"] = floor($iphone * (($aboutUsRow["ms_img2_w"] / 3.2) / 100));
		$aboutUsRow["ms_img3_w"] = floor($iphone * (($aboutUsRow["ms_img3_w"] / 3.2) / 100));
		$aboutUsRow["ms_img4_w"] = floor($iphone * (($aboutUsRow["ms_img4_w"] / 3.2) / 100));
		$aboutUsRow["ms_img5_w"] = floor($iphone * (($aboutUsRow["ms_img5_w"] / 3.2) / 100));
		$aboutUsRow["ms_img6_w"] = floor($iphone * (($aboutUsRow["ms_img6_w"] / 3.2) / 100));
		$aboutUsRow["ms_img7_w"] = floor($iphone * (($aboutUsRow["ms_img7_w"] / 3.2) / 100));
		$aboutUsRow["ms_img8_w"] = floor($iphone * (($aboutUsRow["ms_img8_w"] / 3.2) / 100));
		$aboutUsRow["ms_img9_w"] = floor($iphone * (($aboutUsRow["ms_img9_w"] / 3.2) / 100));
		$aboutUsRow["ms_img10_w"] = floor($iphone * (($aboutUsRow["ms_img10_w"] / 3.2) / 100));
	}
	if($bizRow["biz_id"] != '')
	{
		$i = 0;
		
		$floatDir = "left";
		$marginDir = "margin-left";
		$opMarginDir = "margin-right";
		if($bizRow["biz_dir"] == 'rtl')
		{
			$floatDir = "right";
			$marginDir = "margin-right";
			$opMarginDir = "margin-left";
		}
		
		$tmpl_html_out = str_replace( '##mainwidth##' , $mainwidth ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_name##' , $bizRow["biz_name"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_odot##' , $bizRow["biz_odot"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##dir##' , $bizRow["biz_dir"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##floatDir##' , $floatDir ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##marginDir##' , $marginDir ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##opMarginDir##' , $opMarginDir ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '#BODYCOLOR#' , $tFontColor ,$tmpl_html_out);
        
		// image width
		$tmpl_html_out = str_replace( '##biz_img1_w##' , $aboutUsRow["ms_img1_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img2_w##' , $aboutUsRow["ms_img2_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img3_w##' , $aboutUsRow["ms_img3_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img4_w##' , $aboutUsRow["ms_img4_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img5_w##' , $aboutUsRow["ms_img5_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img6_w##' , $aboutUsRow["ms_img6_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img7_w##' , $aboutUsRow["ms_img7_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img8_w##' , $aboutUsRow["ms_img8_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img9_w##' , $aboutUsRow["ms_img9_w"] ,$tmpl_html_out);
		$tmpl_html_out = str_replace( '##biz_img10_w##' , $aboutUsRow["ms_img10_w"] ,$tmpl_html_out);
		
		$tmpl_html_out = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tmpl_html_out); 
                
                $viewTypeMainMenuToSend = "ms_view_type";
                $nibNameMainMenuToSend = "ms_nib_name";
                if($android == "1"){
                    $viewTypeMainMenuToSend = "ms_view_type_android";
                    $nibNameMainMenuToSend = "ms_nib_name_android";
                }
		
		$responce->rows[$i]['biz_info']=$tmpl_html_out."<br /><br /><br /><br />";
		$responce->rows[$i]['biz_logo']=($bizLogo == "") ? "no_logo.png" : $bizLogo;
		$responce->rows[$i]['biz_office_tele']=($bizRow["biz_office_tele"] == null) ? "" : $bizRow["biz_office_tele"];  
		$responce->rows[$i]['biz_mobile_tele']=($bizRow["biz_mobile_tele"] == null) ? "" : $bizRow["biz_mobile_tele"];  
		$responce->rows[$i]['biz_email']=($bizRow["biz_e_mail"] == null) ? "" : $bizRow["biz_e_mail"];
		$responce->rows[$i]['biz_addr_street']=($bizRow["biz_addr_street"] == null) ? "" : $bizRow["biz_addr_street"];   
		$responce->rows[$i]['biz_addr_no']=($bizRow["biz_addr_no"] == null) ? "" : $bizRow["biz_addr_no"];
		$responce->rows[$i]['biz_addr_town_id']=($bizRow["biz_addr_town"] == null) ? "" : $bizRow["biz_addr_town"];
		$responce->rows[$i]['biz_addr_state_id']=($bizRow["biz_addr_state"] == null) ? "" : $bizRow["biz_addr_state"];
		$responce->rows[$i]['biz_addr_country_id']=($bizRow["biz_addr_country"] == null) ? "" : $bizRow["biz_addr_country"];
		$responce->rows[$i]['biz_id']=$bizRow["biz_id"];
		$responce->rows[$i]['biz_icon']=$bizRow["biz_icon"];
		$responce->rows[$i]['biz_short_name']=$bizRow["biz_short_name"];
		$responce->rows[$i]['biz_first_id']=$bizRow["biz_first_mode_id"];
		$responce->rows[$i]['biz_num_mod']=$modulesCount;
		$responce->rows[$i]['biz_menu_header']=$bizRow["biz_menu_mobile_header"];
		$responce->rows[$i]['biz_theme_id']=$tid;
        $responce->rows[$i]['biz_theme_name']=$tname;
        $responce->rows[$i]['biz_theme_transparent']=$ttransparent; 
        $responce->rows[$i]['biz_theme_font']=$tFont;
        $responce->rows[$i]['biz_theme_font_color']=$tFontColor;
        $responce->rows[$i]['biz_theme_head_color']=$tHeadColor;
        $responce->rows[$i]['biz_theme_feel']=$templateFeel;
        $responce->rows[$i]['biz_sbmt_apl_bundle_suffix']=$bizRow["biz_sbmt_apl_bundle_suffix"];
        $responce->rows[$i]['biz_category_id']=$bizRow["biz_category_id"];
        $responce->rows[$i]['biz_mainmenu_view_type']=$mainMenuRow["$viewTypeMainMenuToSend"];
        $responce->rows[$i]['biz_mainmenu_nib_name']=$mainMenuRow["$nibNameMainMenuToSend"];
        $responce->rows[$i]['biz_lang_code']=$bizRow["biz_default_lng"];
        
		if($bizRow["biz_img1"] != "")
		{
			$responce->rows[$i]['biz_info_img']["1"]=$bizRow["biz_img1"];
		}
		else
		{
			$responce->rows[$i]['biz_info_img']["1"]=$aboutUsRow["ms_img1_default"];
		}
				
		$modSQL = "SELECT biz_mod_mod_id,REPLACE(biz_mod_mobile_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,IFNULL(biz_mod_mod_pic, mod_pic) biz_mod_mod_pic,tmpl_background,biz_mod_stuct,tmpl_font,tmpl_font_color,tmpl_name,mod_reg_type
                    FROM tbl_biz_mod,tbl_modules,tbl_template  
                    WHERE biz_mod_active=1 
                    and biz_mod_mod_id=mod_id 
                    and biz_mod_biz_id=$bizid 
                    and tmpl_id = $tid 
                    and mod_id<>0
                    and mod_type=1
                    order by biz_mod_mod_id";
	
		$modList = dbGetTable( $modSQL );
		if(count($modList) > 0)
		{
			$j = 0;
			foreach ($modList as $oneMod) 
			{
                $combinedImg = $oneMod["biz_mod_mod_pic"];
                if(startsWith($oneMod["biz_mod_mod_pic"], "mod"))
                {
                    if($OS != "Android" || ($OS == "Android" && $papTapVersion != "" && !startsWith($papTapVersion, "Cha Cha Cha")) )
                    {
                        $combinedImg = str_replace(".png", "_".$templateFeel.".png" , $oneMod["biz_mod_mod_pic"]);
                    }
                }
                
				$responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_id']=$oneMod["biz_mod_mod_id"];
				$responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_name']=$oneMod["biz_mod_mod_name"];
				$responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_pic']=$combinedImg;
				$responce->rows[$i]["biz_modules"][$j]['tmpl_background']=$oneMod["tmpl_background"];
                $responce->rows[$i]["biz_modules"][$j]['mod_reg_type']=$oneMod["mod_reg_type"];
				$j++;
			}
			
			$j = 0;
			foreach ($modList as $oneMod) 
			{
				$modStructSQL = "SELECT ms_mod_id,ms_level_no,ms_view_type$nib_name ms_view_type,
                                    ms_nib_name$nib_name ms_nib_name,'".$oneMod["tmpl_font"]."' ms_font,'".$oneMod["tmpl_font_color"]."' ms_font_color,'backelement_$tname.png' ms_btn_image 
                                    from tbl_mod_struct 
                                    WHERE ms_mod_id=".$oneMod["biz_mod_mod_id"]." 
                                    and ms_template=".$oneMod["biz_mod_stuct"]." 
                                    order by ms_mod_id,ms_level_no";
				$modStruct = dbGetTable( $modStructSQL );
				if(count($modStruct) > 0)
				{	
					foreach ($modStruct as $oneModStruct) 
					{
                        if($ttransparent == "1") $oneModStruct["ms_btn_image"] = "";
                        
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_mod_id']=$oneModStruct["ms_mod_id"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_level_no']=$oneModStruct["ms_level_no"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_view_type']=$oneModStruct["ms_view_type"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_nib_name']=$oneModStruct["ms_nib_name"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_font']=$oneModStruct["ms_font"];						
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_font_color']=$oneModStruct["ms_font_color"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_btn_image']=($oneModStruct["ms_btn_image"] == null) ? "" : $oneModStruct["ms_btn_image"];
						$j++;
					}
				}
			}
            
            $j = 0;
            foreach ($modList as $oneMod) 
			{
                $levelsSQL =  "select * from tbl_mod_data{$oneMod["biz_mod_mod_id"]} main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar from tbl_mod_data{$oneMod["biz_mod_mod_id"]}  group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod
					where biz_mod_active=1 and md_biz_id=$bizid  
					and md_biz_id = biz_mod_biz_id and md_mod_id=biz_mod_mod_id 
					and ms_mod_id=md_mod_id and ms_level_no=md_level_no
					and ms_template=biz_mod_stuct 
					order by md_index";
                $levelsList = dbGetTable( $levelsSQL );

                if(count($levelsList) > 0)
                {
                    
                    foreach ($levelsList as $oneElement) 
                    {				
                        $tmpl_html_out = $oneElement["ms_html"];
                        if($iphone == "0" || $iphone >= 620)
                        {
                            $oneElement["ms_img1_w"] = $oneElement["ms_img1_w"] * 2;
                            $oneElement["ms_img2_w"] = $oneElement["ms_img2_w"] * 2;
                            $oneElement["ms_img3_w"] = $oneElement["ms_img3_w"] * 2;
                            $oneElement["ms_img4_w"] = $oneElement["ms_img4_w"] * 2;
                            $oneElement["ms_img5_w"] = $oneElement["ms_img5_w"] * 2;
                            $oneElement["ms_img6_w"] = $oneElement["ms_img6_w"] * 2;
                            $oneElement["ms_img7_w"] = $oneElement["ms_img7_w"] * 2;
                            $oneElement["ms_img8_w"] = $oneElement["ms_img8_w"] * 2;
                            $oneElement["ms_img9_w"] = $oneElement["ms_img9_w"] * 2;
                            $oneElement["ms_img10_w"] = $oneElement["ms_img10_w"] * 2;
                        }
                        else if ($iphone != "1")
                        {
                            $oneElement["ms_img1_w"] = floor($iphone * (($oneElement["ms_img1_w"] / 3.2) / 100));
                            $oneElement["ms_img2_w"] = floor($iphone * (($oneElement["ms_img2_w"] / 3.2) / 100));
                            $oneElement["ms_img3_w"] = floor($iphone * (($oneElement["ms_img3_w"] / 3.2) / 100));
                            $oneElement["ms_img4_w"] = floor($iphone * (($oneElement["ms_img4_w"] / 3.2) / 100));
                            $oneElement["ms_img5_w"] = floor($iphone * (($oneElement["ms_img5_w"] / 3.2) / 100));
                            $oneElement["ms_img6_w"] = floor($iphone * (($oneElement["ms_img6_w"] / 3.2) / 100));
                            $oneElement["ms_img7_w"] = floor($iphone * (($oneElement["ms_img7_w"] / 3.2) / 100));
                            $oneElement["ms_img8_w"] = floor($iphone * (($oneElement["ms_img8_w"] / 3.2) / 100));
                            $oneElement["ms_img9_w"] = floor($iphone * (($oneElement["ms_img9_w"] / 3.2) / 100));
                            $oneElement["ms_img10_w"] = floor($iphone * (($oneElement["ms_img10_w"] / 3.2) / 100));
                        }
                        
                        if($oneElement["md_mod_id"] == "24")
                        {
                            $addField = "<input type=\"hidden\" value=\"@@".$oneElement["md_info1"]."@@\" />";
                            $tmpl_html_out = str_replace( '</body>' , $addField.'</body>' ,$tmpl_html_out);
                        }
                        
                        $odotReplace = $oneElement["md_info"];
                        if($oneElement["md_mod_id"] == "24")
                        {
                            $addField = "<input type=\"hidden\" value=\"@@".$oneElement["md_info1"]."@@\" />";
                            $tmpl_html_out = str_replace( '</body>' , $addField.'</body>' ,$tmpl_html_out);
                            $odotReplace = "http://www.youtube.com/embed/".$oneElement["md_info1"];
                        }
                        
                        $tmpl_html_out = str_replace( '##mainwidth##' , $mainwidth ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_name##' , $bizRow["biz_name"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_odot##' , $odotReplace ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_odot1##' , $oneElement["md_info1"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_odot2##' , $oneElement["md_info2"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_odot3##' , $oneElement["md_info3"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_odot4##' , $oneElement["md_info4"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_odot5##' , $oneElement["md_info5"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##dir##' , $bizRow["biz_dir"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##floatDir##' , $floatDir ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##dir##' , $floatDir ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##marginDir##' , $marginDir ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##opMarginDir##' , $opMarginDir ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '#BODYCOLOR#' , $tFontColor ,$tmpl_html_out);
                        
                        // image width
                        $tmpl_html_out = str_replace( '##biz_img1_w##' , ($oneElement["md_pic"] == null) ? "0" : $oneElement["ms_img1_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img2_w##' , ($oneElement["md_pic1"] == null) ? "0" : $oneElement["ms_img2_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img3_w##' , ($oneElement["md_pic2"] == null) ? "0" : $oneElement["ms_img3_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img4_w##' , ($oneElement["md_pic3"] == null) ? "0" : $oneElement["ms_img4_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img5_w##' , ($oneElement["md_pic4"] == null) ? "0" : $oneElement["ms_img5_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img6_w##' , ($oneElement["md_pic5"] == null) ? "0" : $oneElement["ms_img6_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img7_w##' , $oneElement["ms_img7_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img8_w##' , $oneElement["ms_img8_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img9_w##' , $oneElement["ms_img9_w"] ,$tmpl_html_out);
                        $tmpl_html_out = str_replace( '##biz_img10_w##' , $oneElement["ms_img10_w"] ,$tmpl_html_out);
                        
                        $tmpl_html_out = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tmpl_html_out);
                        
                        $responce->rows[$i]["biz_level_2"][$j]['md_row_id']=$oneElement["md_row_id"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_biz_id']=$oneElement["md_biz_id"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_mod_id']=$oneElement["md_mod_id"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_level_no']=$oneElement["md_level_no"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_parent']=$oneElement["md_parent"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_index']=$oneElement["md_index"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_icon']=($oneElement["md_icon"] == null) ? "no_icon_levels.png" : $oneElement["md_icon"];			
                        $responce->rows[$i]["biz_level_2"][$j]['md_head']=($oneElement["md_head"] == null) ? "" : $oneElement["md_head"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_desc']=($oneElement["md_desc"] == null) ? "" : $oneElement["md_desc"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_loc']=($oneElement["md_loc"] == null) ? "" : $oneElement["md_loc"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_date']=($oneElement["md_date"] == null) ? "" : $oneElement["md_date"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_info']=($tmpl_html_out == null) ? "" : $tmpl_html_out;
                        $responce->rows[$i]["biz_level_2"][$j]['md_score']=($oneElement["md_score"] == null) ? "" : $oneElement["md_score"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_price']=($oneElement["md_price"] == null) ? "" : $oneElement["md_price"];
                        $responce->rows[$i]["biz_level_2"][$j]['cnt']=($oneElement["cnt"] == null) ? "0" : $oneElement["cnt"];
                        
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["1"]=($oneElement["md_pic"] == null) ? "no_img2_html.png" : $oneElement["md_pic"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["2"]=($oneElement["md_pic1"] == null) ? "transparent.png" : $oneElement["md_pic1"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["3"]=($oneElement["md_pic2"] == null) ? "transparent.png" : $oneElement["md_pic2"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["4"]=($oneElement["md_pic3"] == null) ? "transparent.png" : $oneElement["md_pic3"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["5"]=($oneElement["md_pic4"] == null) ? "transparent.png" : $oneElement["md_pic4"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["6"]=($oneElement["md_pic5"] == null) ? "transparent.png" : $oneElement["md_pic5"];
                        $j++;
                    }

                }
                else
                {
                    $responce->rows[$i]["biz_level_2"][0]['md_row_id']="0";
                }
            }
            
            
		}
		
		
	
        $fieldsSQL = "SELECT fi_id cfi_field_id,fi_req cfi_req,fi_order cfi_order,fi_type cfi_type
                    FROM tbl_fields  
                    WHERE fi_id in(11,12,13) 
                    union
                    SELECT cfi_field_id,cfi_req,fi_order cfi_order,fi_type cfi_type
                    FROM tbl_fields,tbl_cust_fields  
                    WHERE
                    fi_id = cfi_field_id
                    and cfi_cust_id=$bizid
                    order by cfi_order";
        //echo $modSQL;
        $fieldsList = dbGetTable( $fieldsSQL );
        if(count($fieldsList) > 0)
        {
            $j = 0;
            foreach ($fieldsList as $oneField) 
            {
                $responce->rows[$i]["biz_fields"][$j]['cfi_field_id']=$oneField["cfi_field_id"];
                $responce->rows[$i]["biz_fields"][$j]['cfi_req']=$oneField["cfi_req"];
                $responce->rows[$i]["biz_fields"][$j]['cfi_order']=$oneField["cfi_order"];
                $responce->rows[$i]["biz_fields"][$j]['cfi_type']=$oneField["cfi_type"];
                $j++;
            }
        }


        $k=0;
    	$functionsSQL = "SELECT fu_id,
                            bfu_label,
                            fu_icon_name, 
                            bfu_index,
                            fu_ios_selector,
                            fu_and_selector
                    FROM tbl_biz_functions, tbl_functions 
                    WHERE bfu_func_id = fu_id
                    AND bfu_biz_id = $bizid
                    ORDER BY bfu_index
                    ";
    	$functionsList = dbGetTable( $functionsSQL );
    	if(count($functionsList) > 0)
    	{
        	$k=0;
        	foreach ($functionsList as $oneFunction) 
        	{
            	$responce->rows[$i]["tabbar"][$k]['fu_id']=$oneFunction["fu_id"];
           	 	$responce->rows[$i]["tabbar"][$k]['fu_label']=$oneFunction["bfu_label"];
            	$responce->rows[$i]["tabbar"][$k]['fu_icon']=$oneFunction["fu_icon_name"];
            	$responce->rows[$i]["tabbar"][$k]['fu_index']=$oneFunction["bfu_index"];
            	$responce->rows[$i]["tabbar"][$k]['fuselector']=($android == "1")? $oneFunction["fu_and_selector"]: $oneFunction["fu_ios_selector"]; 
            	$k++;
        	}

    	}

    	
        
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
?>