<?php
	require 'connect.php';
	require 'functions.php';
	
    //foreach ($_GET as $key => $value)
    //{
    //    $par = $par."$key has a value of $value <br />";
    //}
    //$repTEXT = "get_biz<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
	// start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
	$lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
	$deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
	$bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    // end global parameters
	
	$nib_name = "";
	if($android == "1")
	{
		$nib_name = "_android";
	}
	
	if($state == '')
	{
		$state = "0";
	}
	
    addAPI("api_get_biz_full",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
	
	$bizSQL = "select biz_theme,
            biz_id,
            biz_dir,
            biz_office_tele,
            biz_mobile_tele,
            biz_e_mail,
            biz_addr_street,
            biz_addr_no,
            biz_addr_town,
            biz_addr_state,
            biz_addr_country,
            biz_icon,
            biz_short_name,
            biz_first_mode_id,
            biz_menu_mobile_header biz_menu_header,
            biz_menu,
            biz_sbmt_apl_bundle_suffix,
            biz_category_id,
            biz_default_lng from tbl_biz where biz_id=$bizid";
	$bizRow = dbGetRow( $bizSQL );
        
    $mainMenuSQL = "select * from tbl_biz_mod, tbl_mod_struct
                    where biz_mod_biz_id = $bizid
                    and biz_mod_mod_id = 0
                    and biz_mod_stuct = ms_template
                ";
    $mainMenuRow = dbGetRow( $mainMenuSQL );
	
	$modulesCount = dbGetVal("select count(biz_mod_mod_id) biz_mod_mod_id from tbl_biz_mod where biz_mod_active=1 and biz_mod_mod_id <> 0 and biz_mod_biz_id=$bizid");
	
	$tmplSQL = "select * from tbl_template where tmpl_id = " . $bizRow["biz_theme"];
	$tmplRow = dbGetRow( $tmplSQL );
    $tname = $tmplRow["tmpl_name"];
    $tid = $tmplRow["tmpl_id"];
    $ttransparent = $tmplRow["tmpl_transparent"];
    $tFont = $tmplRow["tmpl_font"];
    $tFontColor = $tmplRow["tmpl_font_color"];
    $tHeadColor = $tmplRow["tmpl_head_color"];
    $templateFeel = $tmplRow["tmpl_img_feel"];
	/* NEW THEME PROPERTIES */
	$gen_bg_type = $tmplRow["tmpl_gen_bg_type"];
	$tabbar_bg_type = $tmplRow["tmpl_tabbar_bg_type"];
	$shade_bg_type = $tmplRow["tmpl_tabbar_shade_type"];
	$navig_bg_type = $tmplRow["tmpl_navbar_bg_type"];
	$side_bg_type = $tmplRow["tmpl_sidebar_bg_type"];
	$magic_bg_type = $tmplRow["tmpl_sidemagic_bg_type"];
	$icon_type = $tmplRow["theme_tab_icon_type"];
	$alpha = $tmplRow["tmpl_shadow_alpha"];

	$color1 = $tmplRow["tmpl_color_1"];
	$color2 = $tmplRow["tmpl_color_2"];
	$color3 = $tmplRow["tmpl_color_3"];
	$color4 = $tmplRow["tmpl_color_4"];
	$color5 = $tmplRow["tmpl_color_5"];
	$color6 = $tmplRow["tmpl_color_6"];
	$color7 = $tmplRow["tmpl_color_7"];
    
	if($bizRow["biz_id"] != '')
	{
		$i = 0;
		
		$floatDir = "left";
		$marginDir = "margin-left";
		$opMarginDir = "margin-right";
		if($bizRow["biz_dir"] == 'rtl')
		{
			$floatDir = "right";
			$marginDir = "margin-right";
			$opMarginDir = "margin-left";
		}
                
        $viewTypeMainMenuToSend = "ms_view_type";
        $nibNameMainMenuToSend = "ms_nib_name";
        if($android == "1"){
            $viewTypeMainMenuToSend = "ms_view_type_android";
            $nibNameMainMenuToSend = "ms_nib_name_android";
        }
		
        $bizLogo = dbGetVal("select max(md_pic) from tbl_mod_data0 where md_mod_id=0 and md_biz_id=$bizid limit 1");
        
		$responce->rows[$i]['biz_info']="";
		$responce->rows[$i]['biz_logo']=($bizLogo == null) ? "no_logo.png" : $bizLogo;
		$responce->rows[$i]['biz_office_tele']=($bizRow["biz_office_tele"] == null) ? "" : $bizRow["biz_office_tele"];  
		$responce->rows[$i]['biz_mobile_tele']=($bizRow["biz_mobile_tele"] == null) ? "" : $bizRow["biz_mobile_tele"];  
		$responce->rows[$i]['biz_email']=($bizRow["biz_e_mail"] == null) ? "" : $bizRow["biz_e_mail"];
		$responce->rows[$i]['biz_addr_street']=($bizRow["biz_addr_street"] == null) ? "" : $bizRow["biz_addr_street"];   
		$responce->rows[$i]['biz_addr_no']=($bizRow["biz_addr_no"] == null) ? "" : $bizRow["biz_addr_no"];
		$responce->rows[$i]['biz_addr_town_id']=($bizRow["biz_addr_town"] == null) ? "" : $bizRow["biz_addr_town"];
		$responce->rows[$i]['biz_addr_state_id']=($bizRow["biz_addr_state"] == null) ? "" : $bizRow["biz_addr_state"];
		$responce->rows[$i]['biz_addr_country_id']=($bizRow["biz_addr_country"] == null) ? "" : $bizRow["biz_addr_country"];
		$responce->rows[$i]['biz_id']=$bizRow["biz_id"];
		$responce->rows[$i]['biz_icon']=$bizRow["biz_icon"];
		$responce->rows[$i]['biz_short_name']=$bizRow["biz_short_name"];
		$responce->rows[$i]['biz_first_id']=$bizRow["biz_first_mode_id"];
		$responce->rows[$i]['biz_num_mod']=$modulesCount;
		$responce->rows[$i]['biz_menu_header']=$bizRow["biz_menu_header"];
		$responce->rows[$i]['biz_theme_id']=$tid;
        $responce->rows[$i]['biz_theme_name']=$tname;
        $responce->rows[$i]['biz_theme_transparent']=$ttransparent; 
        $responce->rows[$i]['biz_theme_font']=$tFont;
        $responce->rows[$i]['biz_theme_font_color']=$tFontColor;
        $responce->rows[$i]['biz_theme_head_color']=$tHeadColor;
        $responce->rows[$i]['biz_theme_feel']=$templateFeel;
        $responce->rows[$i]['biz_theme_bckg_type']=$gen_bg_type;
    	$responce->rows[$i]['biz_theme_tabbar_type']=$tabbar_bg_type;
    	$responce->rows[$i]['biz_theme_shade_type']=$shade_bg_type;
    	$responce->rows[$i]['biz_theme_navbar_type']=$navig_bg_type;
    	$responce->rows[$i]['biz_theme_sidebar_type']=$side_bg_type;
    	$responce->rows[$i]['biz_theme_magic_type']=$magic_bg_type;

    	$responce->rows[$i]['biz_theme_color1']=$color1;
    	$responce->rows[$i]['biz_theme_color2']=$color2;
    	$responce->rows[$i]['biz_theme_color3']=$color3;
    	$responce->rows[$i]['biz_theme_color4']=$color4;
    	$responce->rows[$i]['biz_theme_color5']=$color5;
    	$responce->rows[$i]['biz_theme_color6']=$color6;
    	$responce->rows[$i]['biz_theme_color7']=$color7;

    	$responce->rows[$i]['biz_theme_shadow_alpha']=$alpha;
    	$responce->rows[$i]['biz_theme_icon_type']=$icon_type;

    	$responce->rows[$i]['biz_layout']=$bizRow["biz_menu"];
        $responce->rows[$i]['biz_sbmt_apl_bundle_suffix']=$bizRow["biz_sbmt_apl_bundle_suffix"];
        $responce->rows[$i]['biz_category_id']=$bizRow["biz_category_id"];
        $responce->rows[$i]['biz_mainmenu_view_type']=$mainMenuRow["$viewTypeMainMenuToSend"];
        $responce->rows[$i]['biz_mainmenu_nib_name']=$mainMenuRow["$nibNameMainMenuToSend"];
        $responce->rows[$i]['biz_lang_code']=$bizRow["biz_default_lng"];
        $responce->rows[$i]['biz_info_img']["1"]="";		
				
		$modSQL = "SELECT biz_mod_mod_id,REPLACE(biz_mod_mobile_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,IFNULL(biz_mod_mod_pic, mod_pic) biz_mod_mod_pic,biz_mod_stuct,mod_reg_type
                    FROM tbl_biz_mod,tbl_modules  
                    WHERE biz_mod_active=1 
                    and biz_mod_mod_id=mod_id 
                    and biz_mod_biz_id=$bizid 
                    and mod_id not in (0,16)
                    and mod_type=1
                    order by biz_mod_mod_id";
	
		$modList = dbGetTable( $modSQL );
		if(count($modList) > 0)
		{
			$j = 0;
			foreach ($modList as $oneMod) 
			{
                $combinedImg = $oneMod["biz_mod_mod_pic"];
                if(startsWith($oneMod["biz_mod_mod_pic"], "mod"))
                {
                    if($OS != "Android" || ($OS == "Android" && $papTapVersion != "" && !startsWith($papTapVersion, "Cha Cha Cha")) )
                    {
                        $combinedImg = str_replace(".png", "_".$templateFeel.".png" , $oneMod["biz_mod_mod_pic"]);
                    }
                }
                
				$responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_id']=$oneMod["biz_mod_mod_id"];
				$responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_name']=$oneMod["biz_mod_mod_name"];
				$responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_pic']=$combinedImg;
                $responce->rows[$i]["biz_modules"][$j]['mod_reg_type']=$oneMod["mod_reg_type"];
				$j++;
			}
			
			$j = 0;
			foreach ($modList as $oneMod) //structure
			{
				$modStructSQL = "SELECT ms_mod_id,ms_level_no,ms_view_type$nib_name ms_view_type,
                                    ms_nib_name$nib_name ms_nib_name 
                                    from tbl_mod_struct 
                                    WHERE ms_mod_id=".$oneMod["biz_mod_mod_id"]." 
                                    and ms_template=".$oneMod["biz_mod_stuct"]."
                                    and ms_level_no=1
                                    order by ms_mod_id,ms_level_no";
				$modStruct = dbGetTable( $modStructSQL );
				if(count($modStruct) > 0)
				{	
					foreach ($modStruct as $oneModStruct) 
					{
                        if($ttransparent == "1") $oneModStruct["ms_btn_image"] = "";
                        
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_mod_id']=$oneModStruct["ms_mod_id"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_level_no']=$oneModStruct["ms_level_no"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_view_type']=$oneModStruct["ms_view_type"];
						$responce->rows[$i]["biz_modules_struct"][$j]['ms_nib_name']=$oneModStruct["ms_nib_name"];
						$j++;
					}
				}
			}
            
            $j = 0;
            foreach ($modList as $oneMod) //structure
			{
                $levelsSQL =  "select * from tbl_mod_data{$oneMod["biz_mod_mod_id"]} main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar from tbl_mod_data{$oneMod["biz_mod_mod_id"]}  group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements
					where biz_mod_active=1 and md_biz_id=$bizid and me_id=md_element 
					and md_biz_id = biz_mod_biz_id and md_mod_id=biz_mod_mod_id 
					and ms_mod_id=md_mod_id and ms_level_no=1
					and ms_template=biz_mod_stuct 
					order by md_mod_id, md_index";
                $levelsList = dbGetTable( $levelsSQL );

                if(count($levelsList) > 0)
                {
                    
                    foreach ($levelsList as $oneElement) 
                    {								
                        $odotReplace = $oneElement["md_info"];
                        if($oneElement["md_mod_id"] == "24")
                        {
                            $tmpl_html_out = $oneElement["ms_html"];
                            $addField = "<input type=\"hidden\" value=\"@@".$oneElement["md_info1"]."@@\" />";
                            $tmpl_html_out = str_replace( '</body>' , $addField.'</body>' ,$tmpl_html_out);
                            $odotReplace = "http://www.youtube.com/embed/".$oneElement["md_info1"];
                            $tmpl_html_out = str_replace( '##biz_odot##' , $odotReplace ,$tmpl_html_out);
                            $odotReplace = $tmpl_html_out;
                        }
                        else
                        {
                            if($oneElement["me_id"] == "7") //video
                            {
                                $odotReplace = "<iframe style=\"border: 0px none transparent;\" src=\"$odotReplace\" frameborder=\"0\" scrolling=\"no\" width=\"100%\"></iframe>";
                            }
                            
                            if($oneElement["me_id"] == "8") //audio
                            {
                                $iframe = "<iframe style=\"border: 0px none transparent;\" src=\"#paragraph_description#\" frameborder=\"0\" scrolling=\"no\" width=\"100%\"></iframe>";
                                $html5 = "<audio preload=\"none\" controls=\"controls\" autoplay=\"autoplay\" src=\"#paragraph_description#\" width=\"100%\"></audio>";
                                
                                if(endsWith(strtolower($oneElement["md_info"]), ".mp3"))
                                {
                                    $odotReplace = str_replace ( '#paragraph_description#' , $oneElement["md_info"] , $html5);
                                }
                                else
                                {
                                    $odotReplace = str_replace ( '#paragraph_description#' , $oneElement["md_info"] , $iframe);
                                }
                            }
                        }
                        
                        $responce->rows[$i]["biz_level_2"][$j]['md_row_id']=$oneElement["md_row_id"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_biz_id']=$oneElement["md_biz_id"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_mod_id']=$oneElement["md_mod_id"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_level_no']=$oneElement["md_level_no"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_parent']=$oneElement["md_parent"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_index']=$oneElement["md_index"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_icon']=($oneElement["md_icon"] == null) ? "no_icon_levels.png" : $oneElement["md_icon"];			
                        $responce->rows[$i]["biz_level_2"][$j]['md_head']=($oneElement["md_head"] == null) ? "" : $oneElement["md_head"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_desc']=($oneElement["md_desc"] == null) ? "" : $oneElement["md_desc"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_loc']=($oneElement["md_loc"] == null) ? "" : $oneElement["md_loc"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_date']=($oneElement["md_date"] == null) ? "" : $oneElement["md_date"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_info']=($odotReplace == null) ? "" : $odotReplace;
                        $responce->rows[$i]["biz_level_2"][$j]['md_score']=($oneElement["md_score"] == null) ? "" : $oneElement["md_score"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_price']=($oneElement["md_price"] == null) ? "" : $oneElement["md_price"];
                        $responce->rows[$i]["biz_level_2"][$j]['cnt']=($oneElement["cnt"] == null) ? "0" : $oneElement["cnt"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_type']=($oneElement["me_name"] == null) ? "" : $oneElement["me_name"];
                        $responce->rows[$i]["biz_level_2"][$j]['md_next_view']=($oneElement["md_content"] == "0") ? $oneElement["ms_view_type$nib_name"] : $oneElement["ms_view_end$nib_name"];                       
                        
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["1"]=($oneElement["md_pic"] == null) ? "" : $oneElement["md_pic"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["2"]=($oneElement["md_pic1"] == null) ? "" : $oneElement["md_pic1"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["3"]=($oneElement["md_pic2"] == null) ? "" : $oneElement["md_pic2"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["4"]=($oneElement["md_pic3"] == null) ? "" : $oneElement["md_pic3"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["5"]=($oneElement["md_pic4"] == null) ? "" : $oneElement["md_pic4"];
                        $responce->rows[$i]["biz_level_2"][$j]['biz_level_img']["6"]=($oneElement["md_pic5"] == null) ? "" : $oneElement["md_pic5"];
                        $responce->rows[$i]['biz_level_2'][$j]['biz_level_img']["7"]=($oneElement["md_pic6"] == null) ? "" : $oneElement["md_pic6"];
                        $responce->rows[$i]['biz_level_2'][$j]['biz_level_img']["8"]=($oneElement["md_pic7"] == null) ? "" : $oneElement["md_pic7"];
                        $responce->rows[$i]['biz_level_2'][$j]['biz_level_img']["9"]=($oneElement["md_pic8"] == null) ? "" : $oneElement["md_pic8"];
                        $responce->rows[$i]['biz_level_2'][$j]['biz_level_img']["10"]=($oneElement["md_pic9"] == null) ? "" : $oneElement["md_pic9"];
                        $responce->rows[$i]['biz_level_2'][$j]['biz_level_img']["11"]=($oneElement["md_pic10"] == null) ? "" : $oneElement["md_pic10"];
                        $j++;
                    }

                }
                else
                {
                    $responce->rows[$i]["biz_level_2"][0]['md_row_id']="0";
                }
            }
            
            
		}
		
		
	
        
        // customers
        
        $fieldsSQL = "SELECT fi_id cfi_field_id,fi_req cfi_req,fi_order cfi_order,fi_type cfi_type
                    FROM tbl_fields  
                    WHERE fi_id in(11,12,13) 
                    union
                    SELECT cfi_field_id,cfi_req,fi_order cfi_order,fi_type cfi_type
                    FROM tbl_fields,tbl_cust_fields  
                    WHERE
                    fi_id = cfi_field_id
                    and cfi_cust_id=$bizid
                    order by cfi_order";
        //echo $modSQL;
        $fieldsList = dbGetTable( $fieldsSQL );
        if(count($fieldsList) > 0)
        {
            $j = 0;
            foreach ($fieldsList as $oneField) 
            {
                $responce->rows[$i]["biz_fields"][$j]['cfi_field_id']=$oneField["cfi_field_id"];
                $responce->rows[$i]["biz_fields"][$j]['cfi_req']=$oneField["cfi_req"];
                $responce->rows[$i]["biz_fields"][$j]['cfi_order']=$oneField["cfi_order"];
                $responce->rows[$i]["biz_fields"][$j]['cfi_type']=$oneField["cfi_type"];
                $j++;
            }
        }


        $k=0;
    	$functionsSQL = "SELECT fu_id,
                            bfu_label,
                            fu_icon_name, 
                            bfu_index,
                            fu_ios_selector,
                            fu_and_selector
                    FROM tbl_biz_functions, tbl_functions 
                    WHERE bfu_func_id = fu_id
                    AND bfu_biz_id = $bizid
                    ORDER BY bfu_index
                    ";
    	$functionsList = dbGetTable( $functionsSQL );
    	if(count($functionsList) > 0)
    	{
        	$k=0;
        	foreach ($functionsList as $oneFunction) 
        	{
            	$responce->rows[$i]["tabbar"][$k]['fu_id']=$oneFunction["fu_id"];
           	 	$responce->rows[$i]["tabbar"][$k]['fu_label']=$oneFunction["bfu_label"];
            	$responce->rows[$i]["tabbar"][$k]['fu_icon']=$oneFunction["fu_icon_name"];
            	$responce->rows[$i]["tabbar"][$k]['fu_index']=$oneFunction["bfu_index"];
            	$responce->rows[$i]["tabbar"][$k]['fuselector']=($android == "1")? $oneFunction["fu_and_selector"]: $oneFunction["fu_ios_selector"]; 
            	$k++;
        	}

    	}
        
		$json = json_encode($responce->rows);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
?>