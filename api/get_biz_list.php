<?php
	require 'connect.php';
	require 'functions.php';
	
    //foreach ($_GET as $key => $value)
    //{
    //    $par = $par."$key has a value of $value <br />";
    //}
    //$repTEXT = "get_biz_list<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
	// start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
	$lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
	$deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
	$bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    // end global parameters
	
    $nib_name = "";
    if($android == "1"){
            $nib_name = "_android";
    }

    if($state == ''){
            $state = "0";
    }

    if($country == ''){
            $country = "0";
    }

    $countryNumber = ( !is_int($country) ? (ctype_digit($country)) : true );
    if(!$countryNumber){
            $country = dbGetVal("select country_id from tbl_countries where country_code='$country'");
    }

    $stateNumber = ( !is_int($state) ? (ctype_digit($state)) : true );
    if(!$stateNumber){
            $state = dbGetVal("select state_id from tbl_states where state_code='$state'");
    }
        
  
    addAPI("api_get_biz_list",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
	
    $marketId = isset($_GET["marketId"]) ? $_GET["marketId"] : "0";
    $lang = isset($_GET["displayLang"]) ? $_GET["displayLang"] : $lang;
    if($lang == "iw") $lang = "he";
    
    $allCountry = "";
    if($marketId == "0"){
        $allCountry = " and if(biz_world=1,1=1,biz_addr_country_id=$country) and biz_addr_state_id=$state ";
    }
    
    $bizSQL = "select max(biz_id) biz_id, 
                      max(biz_short_name) biz_short_name, 
                      max(biz_icon) biz_icon, 
                      max(biz_menu) biz_menu,
                      max(biz_first_mode_id) biz_first_id,
                      count(biz_mod_mod_id) biz_num_mod ,
                      max(mrktbiz_rank) rating,
                      biz_category_id cat_id,
                      max(biz_odot) biz_odot,
                      max(biz_e_mail) email,
                      max(biz_mobile_tele) sms,
                      max(biz_office_tele) callno,
                      max(bm_biz_lati) lati,
                      max(bm_biz_long) longti
                from tbl_biz,tbl_biz_mod,tbl_biz_market,tbl_res_market_biz 
                where biz_mod_active=1 
                and biz_mod_mod_id <> 0 
                and biz_id=biz_mod_biz_id 
                and biz_id=mrktbiz_biz_id
                and biz_id = bm_biz_id
                and biz_sub_category_id=$subcategory 
                $allCountry 
                and biz_status=1
                and biz_isdeleted=0 
                and mrktbiz_market_id = $marketId
                group by biz_id";
                          
    //echo $bizSQL;         
	$bizList = dbGetTable( $bizSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{
			$f_id = $oneBiz["biz_first_id"];
                        $theID = $oneBiz["biz_id"];
			
            $structSQL = "SELECT REPLACE(biz_mod_mobile_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,ms_view_type$nib_name ms_view_type 
			FROM tbl_biz_mod,tbl_mod_struct 
			WHERE biz_mod_active=1 
                        and biz_mod_biz_id=$theID
			and biz_mod_mod_id=$f_id 
			and ms_mod_id=biz_mod_mod_id 
			and ms_level_no=1 
			and ms_template=biz_mod_stuct";
			$structRow = dbGetRow( $structSQL );
                        
            $modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $theID and biz_mod_active=1 and mod_id>0 and mod_type=1");
                        $categoryID = $oneBiz["cat_id"];
                        $catName = dbGetVal("select cat_name_$lang cat_name from tbl_categories where cat_id = $categoryID");
                        
			
			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_icon"];
            $responce->rows[$i]['cat_id']=$categoryID;
            $responce->rows[$i]['cat_name']=$catName;
			$responce->rows[$i]['biz_first_id']=$oneBiz["biz_first_id"];
			$responce->rows[$i]['biz_num_mod']=$oneBiz["biz_num_mod"];
			$responce->rows[$i]['biz_mod_mod_name']=$structRow["biz_mod_mod_name"];
			$responce->rows[$i]['ms_view_type']=$structRow["ms_view_type"];
            $responce->rows[$i]['biz_layout']=$oneBiz["biz_menu"];
            $responce->rows[$i]['rating']=$oneBiz["rating"];
            $responce->rows[$i]['biz_market_img']="pach";
           
                        
                        $k = 0;
                        if(count($modList) > 0){
                            foreach ($modList as $oneMod){
                                $responce->rows[$i]["biz_modules"][$k]["mod_name"] = $oneMod["mod_name"];
                                $responce->rows[$i]["biz_modules"][$k]["mod_pic"] = $oneMod["mod_pic"];
                                $k++;
                            }
                        }else{
                            $responce->rows[$i]["biz_modules"]="0";
                        }
                        
                        $newOdot = strip_tags($oneBiz['biz_odot'],'<p></p><b></b><br></br>');
                        $responce->rows[$i]['description']=$newOdot;
                        $responce->rows[$i]['long']=$oneBiz["longti"];
                        $responce->rows[$i]['lati']=$oneBiz["lati"];
                        $responce->rows[$i]['call']=$oneBiz["callno"];
                        $responce->rows[$i]['sms']=$oneBiz["sms"];
                        $responce->rows[$i]['email']=$oneBiz["email"]; 
			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
		//decode the data
		//$data = json_decode($json);
		
		//foreach ($data->rows as $row) {
			//echo $row->biz_id . ',' . $row->biz_name . ',' . $row->biz_icon . '<br />';
		//}
	}
	else
	{
		echo "0";
	}
	
		
?>