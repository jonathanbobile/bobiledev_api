<?php
require_once('../admin/MVC/config.php');
require_once('../admin/MVC/Database.php');
require_once('../admin/libs/languageManager.php');
require_once('../admin/libs/customersManager.php');
require 'connect.php';
require 'functions.php';

$android = $_GET["android"];
$deviceID = $_GET["deviceID"];
$iphone = $_GET["iphone"];
$lang = $_GET["lang"];
$deviceType = $_GET["deviceType"];
$deviceModel = $_GET["deviceModel"];
$deviceOrient = $_GET["deviceOrient"];
$OS = $_GET["OS"];
$OSVersion = $_GET["OSVersion"];
$papTapVersion = $_GET["papTapVersion"];
$long = $_GET["long"];
$lati = $_GET["lati"];
$phoneNumber = $_GET["phoneNumber"];

$bizid = $_GET["bizid"];
$state = $_GET["state"];
$country = $_GET["country"];	
$category = $_GET["category"];
$subcategory = $_GET["subcategory"];
$mod_id = $_GET["mod_id"];
$level_no = $_GET["level_no"];
$parent = $_GET["parent"];
$in_favorites = $_GET["in_favorites"];
$isadmin = $_GET["isadmin"];

if($android == 'null' || $android == '') $android = "0";
if($deviceID == 'null') $deviceID = "";
if($iphone == 'null') $iphone = "0";
if($lang == 'null') $lang = "eng";
if($deviceType == 'null') $deviceType = "";
if($deviceModel == 'null') $deviceModel = "";
if($deviceOrient == 'null') $deviceOrient = "";
if($OS == 'null') $OS = "Android";
if($OSVersion == 'null') $OSVersion = "";
if($papTapVersion == 'null') $papTapVersion = "";
if($long == 'null') $long = ""; 
if($lati == 'null') $lati = ""; 
if($phoneNumber == 'null') $phoneNumber = "";    

if($bizid == '' || $bizid == 'null') $bizid = "0"; 
if($state == '' || $state == 'null') $state = "0"; 
if($country == '' || $country == 'null') $country = "0"; 
if($category == '' || $category == 'null') $category = "0";
if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
if($level_no == '' || $level_no == 'null') $level_no = "0";
if($parent == '' || $parent == 'null') $parent = "0";
if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";

$cust_id = customersManager::getCusIdByDevice($bizid,$deviceID);

// end global parameters

$nib_name = "";
if($android == "1")
{
    $nib_name = "_android";
}

if($state == '')
{
    $state = "0";
}

addAPI("api_get_biz",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);

if($deviceID != ""){
    dbExecute("update tbl_customers set cust_last_seen=now() where cust_id=$cust_id");
}

$bizSQL = "select biz_theme,
            biz_id,
            biz_dir,
            biz_office_tele,
            biz_mobile_tele,
            biz_e_mail,
            biz_addr_street,
            biz_addr_no,
            biz_addr_town,
            biz_addr_state,
            biz_addr_country,
            biz_icon,
            biz_short_name,
            biz_first_mode_id,
            biz_menu_mobile_header biz_menu_header,
            biz_menu,
            biz_sbmt_apl_bundle_suffix,
            biz_category_id,
            biz_zooz_app_key,
            biz_zooz_unique_id,
            owner_zooz_merchant_id,
            biz_default_lng from tbl_biz,tbl_owners where owner_id=biz_owner_id and biz_id=$bizid";


$bizRow = dbGetRow( $bizSQL );

$bizExtraRow = dbGetRow("select * from tbl_mod_data0 where md_biz_id=$bizid order by md_element limit 1");
$bizLogo = $bizExtraRow["md_pic"];
$bizGridTitle = $bizExtraRow["md_head"];

$mainMenuSQL = "select * from tbl_biz_mod, tbl_mod_struct
                        where biz_mod_biz_id = $bizid
                        and biz_mod_mod_id = 0
                        and biz_mod_stuct = ms_template
                ";
$mainMenuRow = dbGetRow( $mainMenuSQL );

$modulesCount = dbGetVal("select count(biz_mod_mod_id) biz_mod_mod_id from tbl_biz_mod where biz_mod_active=1 and biz_mod_mod_id not in(0,16) and biz_mod_biz_id=$bizid");


$tmplSQL = "select * from tbl_template where tmpl_id = " . $bizRow["biz_theme"];
$tmplRow = dbGetRow( $tmplSQL );
$tname = $tmplRow["tmpl_name"];
$tid = $tmplRow["tmpl_id"];
$ttransparent = $tmplRow["tmpl_transparent"];
$tFont = $tmplRow["tmpl_font"];
$tFontColor = $tmplRow["tmpl_font_color"];
$tHeadColor = $tmplRow["tmpl_head_color"];
$templateFeel = $tmplRow["tmpl_img_feel"];

/* NEW THEME PROPERTIES */
$gen_bg_type = $tmplRow["tmpl_gen_bg_type"];
$tabbar_bg_type = $tmplRow["tmpl_tabbar_bg_type"];
$shade_bg_type = $tmplRow["tmpl_tabbar_shade_type"];
$navig_bg_type = $tmplRow["tmpl_navbar_bg_type"];
$side_bg_type = $tmplRow["tmpl_sidebar_bg_type"];
$magic_bg_type = $tmplRow["tmpl_sidemagic_bg_type"];

$color1 = $tmplRow["tmpl_color_1"];
$color2 = $tmplRow["tmpl_color_2"];
$color3 = $tmplRow["tmpl_color_3"];
$color4 = $tmplRow["tmpl_color_4"];
$color5 = $tmplRow["tmpl_color_5"];
$color6 = $tmplRow["tmpl_color_6"];
$color7 = $tmplRow["tmpl_color_7"];

$alpha = $tmplRow["tmpl_shadow_alpha"];



if($bizRow["biz_id"] != '')
{
    $i = 0;
    
    $floatDir = "left";
    $marginDir = "margin-left";
    $opMarginDir = "margin-right";
    if($bizRow["biz_dir"] == 'rtl')
    {
        $floatDir = "right";
        $marginDir = "margin-right";
        $opMarginDir = "margin-left";
    }
    
    $viewTypeMainMenuToSend = "ms_view_type";
    $nibNameMainMenuToSend = "ms_nib_name";
    if($android == "1"){
        $viewTypeMainMenuToSend = "ms_view_type_android";
        $nibNameMainMenuToSend = "ms_nib_name_android";
    }
    
    $responce->rows[$i]['biz_info']="";
    $responce->rows[$i]['biz_logo']=($bizLogo == "") ? "" : $bizLogo;
    $responce->rows[$i]['biz_grid_title']=$bizGridTitle;
    $responce->rows[$i]['biz_office_tele']=($bizRow["biz_office_tele"] == null) ? "" : $bizRow["biz_office_tele"];  
    $responce->rows[$i]['biz_mobile_tele']=($bizRow["biz_mobile_tele"] == null) ? "" : $bizRow["biz_mobile_tele"];  
    $responce->rows[$i]['biz_email']=($bizRow["biz_e_mail"] == null) ? "" : $bizRow["biz_e_mail"];  
    $responce->rows[$i]['biz_addr_street']=($bizRow["biz_addr_street"] == null) ? "" : $bizRow["biz_addr_street"];   
    $responce->rows[$i]['biz_addr_no']=($bizRow["biz_addr_no"] == null) ? "" : $bizRow["biz_addr_no"];
    $responce->rows[$i]['biz_addr_town_id']=($bizRow["biz_addr_town"] == null) ? "" : $bizRow["biz_addr_town"];
    $responce->rows[$i]['biz_addr_state_id']=($bizRow["biz_addr_state"] == null) ? "" : $bizRow["biz_addr_state"];
    $responce->rows[$i]['biz_addr_country_id']=($bizRow["biz_addr_country"] == null) ? "" : $bizRow["biz_addr_country"];
    $responce->rows[$i]['biz_id']=$bizRow["biz_id"];
    $responce->rows[$i]['biz_icon']=$bizRow["biz_icon"];
    $responce->rows[$i]['biz_short_name']=$bizRow["biz_short_name"];
    $responce->rows[$i]['biz_first_id']=$bizRow["biz_first_mode_id"];
    $responce->rows[$i]['biz_num_mod']=$modulesCount;
    $responce->rows[$i]['biz_menu_header']=$mainMenuRow["biz_mod_mobile_name"];    
    $responce->rows[$i]['biz_theme_id']=$tid;
    $responce->rows[$i]['biz_theme_name']=$tname;
    $responce->rows[$i]['biz_theme_transparent']=$ttransparent;   
    $responce->rows[$i]['biz_theme_font']=$tFont;
    $responce->rows[$i]['biz_theme_font_color']=$tFontColor;
    $responce->rows[$i]['biz_theme_head_color']=$tHeadColor;
    $responce->rows[$i]['biz_theme_feel']=$templateFeel;

    $responce->rows[$i]['biz_theme_bckg_type']=$gen_bg_type;
    $responce->rows[$i]['biz_theme_tabbar_type']=$tabbar_bg_type;
    $responce->rows[$i]['biz_theme_shade_type']=$shade_bg_type;
    $responce->rows[$i]['biz_theme_navbar_type']=$navig_bg_type;
    $responce->rows[$i]['biz_theme_sidebar_type']=$side_bg_type;
    $responce->rows[$i]['biz_theme_magic_type']=$magic_bg_type;
    $responce->rows[$i]['biz_theme_shadow_alpha']=$alpha;
    $responce->rows[$i]['biz_theme_icon_type']=$icon_type;

    $responce->rows[$i]['biz_theme_color1']=$color1;
    $responce->rows[$i]['biz_theme_color2']=$color2;
    $responce->rows[$i]['biz_theme_color3']=$color3;
    $responce->rows[$i]['biz_theme_color4']=$color4;
    $responce->rows[$i]['biz_theme_color5']=$color5;
    $responce->rows[$i]['biz_theme_color6']=$color6;
    $responce->rows[$i]['biz_theme_color7']=$color7;


    $responce->rows[$i]['biz_layout']=$bizRow["biz_menu"];
    $responce->rows[$i]['biz_sbmt_apl_bundle_suffix']=$bizRow["biz_sbmt_apl_bundle_suffix"];
    $responce->rows[$i]['biz_category_id']=$bizRow["biz_category_id"];
    $responce->rows[$i]['biz_mainmenu_view_type']=$mainMenuRow["$viewTypeMainMenuToSend"];
    $responce->rows[$i]['biz_mainmenu_nib_name']=$mainMenuRow["$nibNameMainMenuToSend"];
    $responce->rows[$i]['biz_lang_code']=$bizRow["biz_default_lng"];
    $responce->rows[$i]['biz_zooz_app_key']=$bizRow["biz_zooz_app_key"];
    $responce->rows[$i]['biz_zooz_unique_id']=$bizRow["biz_zooz_unique_id"];
    $responce->rows[$i]['owner_zooz_merchant_id']=$bizRow["owner_zooz_merchant_id"];
    
    $responce->rows[$i]['biz_info_img']["1"]="";
    
    $storeRow = dbGetRow("select * from ecommerce_store_settings where ess_biz_id=$bizid");
    $responce->rows[$i]['ess_currency']=$storeRow["ess_currency"];
    $responce->rows[$i]['ess_currency_symbol']=$storeRow["ess_currency_symbol"];
    $responce->rows[$i]['ess_tax']=$storeRow["ess_tax_rate"];
    
    
    $modSQL = "SELECT biz_mod_mod_id,REPLACE(biz_mod_mobile_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,IFNULL(biz_mod_mod_pic, mod_pic) biz_mod_mod_pic,biz_mod_stuct,mod_reg_type
                    FROM tbl_biz_mod,tbl_modules  
                    WHERE biz_mod_active=1 
                    and biz_mod_mod_id=mod_id 
                    and biz_mod_biz_id=$bizid 
                    and mod_id not in (0,16)
                    and (mod_type=1 or (biz_mod_mod_id=9 and (biz_mod_biz_id=43 || biz_mod_biz_id=78594 || biz_mod_biz_id=78633 ) ))
                    order by biz_mod_index";
	//echo $modSQL;
    $modList = dbGetTable( $modSQL );
    if(count($modList) > 0)
    {
        $j = 0;
        foreach ($modList as $oneMod) 
        {
            $combinedImg = $oneMod["biz_mod_mod_pic"];
            //if(startsWith($oneMod["biz_mod_mod_pic"], "mod"))
            //{
            //    if($OS != "Android" || ($OS == "Android" && $papTapVersion != "" && !startsWith($papTapVersion, "Cha Cha Cha")) )
            //    {
            //        $combinedImg = str_replace(".png", "_".$templateFeel.".png" , $oneMod["biz_mod_mod_pic"]);
            //    }
                
            //}
            
            $responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_id']=$oneMod["biz_mod_mod_id"];
            $responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_name']=$oneMod["biz_mod_mod_name"];
            $responce->rows[$i]["biz_modules"][$j]['biz_mod_mod_pic']=$combinedImg;
            $responce->rows[$i]["biz_modules"][$j]['mod_reg_type']=$oneMod["mod_reg_type"];
            $j++;
        }
        
        $j = 0;
        foreach ($modList as $oneMod) 
        {
            $modStructSQL = "SELECT ms_mod_id,ms_level_no,ms_view_type$nib_name ms_view_type,
                                    ms_nib_name$nib_name ms_nib_name 
                                    from tbl_mod_struct 
                                    WHERE ms_mod_id=".$oneMod["biz_mod_mod_id"]." 
                                    and ms_template=".$oneMod["biz_mod_stuct"]."
                                    and ms_level_no=1
                                    order by ms_mod_id,ms_level_no";
            $modStruct = dbGetTable( $modStructSQL );
            if(count($modStruct) > 0)
            {	
                foreach ($modStruct as $oneModStruct) 
                {
                    if($ttransparent == "1") $oneModStruct["ms_btn_image"] = "";
                    
                    $responce->rows[$i]["biz_modules_struct"][$j]['ms_mod_id']=$oneModStruct["ms_mod_id"];
                    $responce->rows[$i]["biz_modules_struct"][$j]['ms_level_no']=$oneModStruct["ms_level_no"];
                    $responce->rows[$i]["biz_modules_struct"][$j]['ms_view_type']=$oneModStruct["ms_view_type"];
                    $responce->rows[$i]["biz_modules_struct"][$j]['ms_nib_name']=$oneModStruct["ms_nib_name"];
                    $j++;
                }
            }
        }
    }
	
    $fieldsSQL = "SELECT fi_id cfi_field_id,fi_req cfi_req,fi_order cfi_order,fi_type cfi_type
                    FROM tbl_fields  
                    WHERE fi_id in(11,12,13) 
                    union
                    SELECT cfi_field_id,cfi_req,fi_order cfi_order,fi_type cfi_type
                    FROM tbl_fields,tbl_cust_fields  
                    WHERE
                    fi_id = cfi_field_id
                    and cfi_cust_id=$bizid
                    order by cfi_order";
	//echo $modSQL;
    $fieldsList = dbGetTable( $fieldsSQL );
    if(count($fieldsList) > 0)
    {
        $j = 0;
        foreach ($fieldsList as $oneField) 
        {
            $responce->rows[$i]["biz_fields"][$j]['cfi_field_id']=$oneField["cfi_field_id"];
            $responce->rows[$i]["biz_fields"][$j]['cfi_req']=$oneField["cfi_req"];
            $responce->rows[$i]["biz_fields"][$j]['cfi_order']=$oneField["cfi_order"];
            $responce->rows[$i]["biz_fields"][$j]['cfi_type']=$oneField["cfi_type"];
            $j++;
        }
    }

    $k=0;
    $functionsSQL = "SELECT fu_id,
                            bfu_label,
                            fu_icon_name, 
                            bfu_index,
                            fu_ios_selector,
                            fu_and_selector,
                            bfu_info,
                            bfu_bool1,
                            bfu_bool2,
                            bfu_bool3,
                            bfu_bool4
                    FROM tbl_biz_functions, tbl_functions 
                    WHERE bfu_func_id = fu_id
                    AND bfu_biz_id = $bizid
                    ORDER BY bfu_index
                    ";
    $functionsList = dbGetTable( $functionsSQL );
    if(count($functionsList) > 0)
    {
        $k=0;
        foreach ($functionsList as $oneFunction) 
        {
            $responce->rows[$i]["tabbar"][$k]['fu_id']=$oneFunction["fu_id"];
            $responce->rows[$i]["tabbar"][$k]['fu_label']=$oneFunction["bfu_label"];
            $responce->rows[$i]["tabbar"][$k]['fu_icon']=$oneFunction["fu_icon_name"];
            $responce->rows[$i]["tabbar"][$k]['fu_index']=$oneFunction["bfu_index"];
            $responce->rows[$i]["tabbar"][$k]['fuselector']=($android == "1")? $oneFunction["fu_and_selector"]: $oneFunction["fu_ios_selector"]; 
            $responce->rows[$i]["tabbar"][$k]['fu_info']=$oneFunction["bfu_info"];
            $responce->rows[$i]["tabbar"][$k]['fu_bool1']=$oneFunction["bfu_bool1"];
            $responce->rows[$i]["tabbar"][$k]['fu_bool2']=$oneFunction["bfu_bool2"];
            $responce->rows[$i]["tabbar"][$k]['fu_bool3']=$oneFunction["bfu_bool3"];
            $responce->rows[$i]["tabbar"][$k]['fu_bool4']=$oneFunction["bfu_bool4"];
            $k++;
        }

    }
    
    
    $navigations = dbGetTable("SELECT * FROM tbl_biz_navigation WHERE bn_biz_id = $bizid");
    
    if (count($navigations)> 0){
        $n = 0;
        foreach ($navigations as $nav) {

            switch ($nav["bn_entity"]) {
                case 'function':
                    $functionData = dbGetRow("SELECT * FROM tbl_functions, tbl_biz_functions
                                                    WHERE bfu_biz_id = $bizid
                                                    AND bfu_func_id = fu_id
                                                    AND bfu_func_id = {$nav["bn_entity_id"]}");

                    $responce->rows[$i]["biz_navigation"][$n]['nav_entity_type']=$nav["bn_entity"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_entity_id']=$nav["bn_entity_id"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_index']=$nav["bn_index"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_label']=(isset($functionData["bfu_label"])) ? $functionData["bfu_label"] : "";
                    $responce->rows[$i]["biz_navigation"][$n]['nav_icon']= $nav["bn_entity_id"].".svg";
                    $responce->rows[$i]["biz_navigation"][$n]['nav_selector']=$functionData["fu_ios_selector"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_selector_android']=$functionData["fu_and_selector"];
                    break;

                case 'module':
                    $modData =dbGetRow("SELECT biz_mod_mobile_name,mod_svg FROM tbl_modules,tbl_biz_mod
                                                WHERE biz_mod_mod_id = mod_id
                                                AND biz_mod_biz_id = $bizid
                                                AND biz_mod_mod_id = {$nav["bn_entity_id"]}");

                    $responce->rows[$i]["biz_navigation"][$n]['nav_entity_type']=$nav["bn_entity"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_entity_id']=$nav["bn_entity_id"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_index']=$nav["bn_index"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_label']=$modData["biz_mod_mobile_name"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_icon']="mod".$nav["bn_entity_id"].".svg";
                    $responce->rows[$i]["biz_navigation"][$n]['nav_selector']="";
                    $responce->rows[$i]["biz_navigation"][$n]['nav_selector_android']="";
                    break;
                
                default:
                    $responce->rows[$i]["biz_navigation"][$n]['nav_entity_type']=$nav["bn_entity"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_entity_id']=$nav["bn_entity_id"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_index']=$nav["bn_index"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_label']=(isset($nav["bn_label"])) ? $nav["bn_label"]: "";
                    $responce->rows[$i]["biz_navigation"][$n]['nav_icon']=(isset($nav["bn_icon"])) ? $nav["bn_icon"]: "";
                    $responce->rows[$i]["biz_navigation"][$n]['nav_selector']=$nav["bn_selector_ios"];
                    $responce->rows[$i]["biz_navigation"][$n]['nav_selector_android']=$nav["bn_selector_and"];
                    break;
            }
        
            $n++;

        }
    }
    

    $engagements = dbGetTable("SELECT * FROM tbl_biz_engagement 
                                WHERE bng_biz_id = $bizid");

    if (count($engagements)> 0){
        $e = 0;
        foreach ($engagements as $eng) {
            switch ($eng["bng_entity_type"]) {
                case 'engage':
                    $engagementData = dbGetRow("SELECT * FROM tbl_engagement WHERE eng_id = {$eng["bng_entity_id"]}");

                    $responce->rows[$i]["engagement_data"][$e]['eng_id']=$eng["bng_id"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_bubble_size']=$eng["bng_bubble_size"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_entity']=$eng["bng_entity_type"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_entity_id']= $engagementData["eng_entity_id"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_type']=$engagementData["eng_type"];
                    $responce->rows[$i]["engagement_data"][$e]['action_selector']=$engagementData["eng_action_selector"];
                    $responce->rows[$i]["engagement_data"][$e]['action_selector_android']=$engagementData["eng_action_selector"];
                    $responce->rows[$i]["engagement_data"][$e]['regular_selector']=$engagementData["eng_regular_selector"];
                    $responce->rows[$i]["engagement_data"][$e]['regular_selector_android']=$engagementData["eng_regular_selector_android"];
                    $responce->rows[$i]["engagement_data"][$e]['symbol']="mod{$engagementData["eng_entity_id"]}.svg";
                    $responce->rows[$i]["engagement_data"][$e]['title']=$eng["bng_title"];
                    $responce->rows[$i]["engagement_data"][$e]['text']=$eng["bng_text"];
                    $responce->rows[$i]["engagement_data"][$e]['regular_text']=$eng["bng_succ_desc"];
                    $responce->rows[$i]["engagement_data"][$e]['regular_title']=$eng["bng_succ_title"];                  

                    
                    $paramsData = dbGetTable("SELECT * FROM tbl_biz_engage_params WHERE bep_bng_id = {$eng["bng_id"]}");
                    if (count($paramsData) > 0){
                        $p = 0;
                        foreach ($paramsData as $param) {
                            $responce->rows[$i]["engagement_data"][$e]['eng_params'][$p]["paramName"] = $param["bep_param_name"];
                            $responce->rows[$i]["engagement_data"][$e]['eng_params'][$p]["paramValue"] = $param["bep_param_value"];
                            $p++;
                        }
                    }
                    
                    break;
                
                case 'module':
                
                    $modData =dbGetRow("SELECT biz_mod_mobile_name FROM tbl_modules,tbl_biz_mod
                                                WHERE biz_mod_mod_id = mod_id
                                                AND biz_mod_biz_id = $bizid
                                                AND biz_mod_mod_id = {$eng["bng_entity_id"]}");

                    $responce->rows[$i]["engagement_data"][$e]['eng_id']="0";
                    $responce->rows[$i]["engagement_data"][$e]['eng_bubble_size']=$eng["bng_bubble_size"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_entity']=$eng["bng_entity_type"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_entity_id']=$eng["bng_entity_id"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_type']="";
                    $responce->rows[$i]["engagement_data"][$e]['action_selector']="";
                    $responce->rows[$i]["engagement_data"][$e]['action_selector_android']="openModule";
                    $responce->rows[$i]["engagement_data"][$e]['regular_selector']="";
                    $responce->rows[$i]["engagement_data"][$e]['regular_selector_android']="";
                    $responce->rows[$i]["engagement_data"][$e]['symbol']="";
                    $responce->rows[$i]["engagement_data"][$e]['title']=$modData["biz_mod_mobile_name"];
                    $responce->rows[$i]["engagement_data"][$e]['text']="";
                    $responce->rows[$i]["engagement_data"][$e]['regular_text']="";
                    $responce->rows[$i]["engagement_data"][$e]['regular_title']="";
                    
                    break;


                default:
                
                    $functionData = dbGetRow("SELECT * FROM tbl_functions, tbl_biz_functions
                                                    WHERE bfu_biz_id = $bizid
                                                    AND bfu_func_id = fu_id
                                                    AND bfu_func_id = {$eng["bng_entity_id"]}");

                    $responce->rows[$i]["engagement_data"][$e]['eng_id']="0";
                    $responce->rows[$i]["engagement_data"][$e]['eng_bubble_size']=$eng["bng_bubble_size"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_entity']=$eng["bng_entity_type"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_entity_id']=$eng["bng_entity_id"];
                    $responce->rows[$i]["engagement_data"][$e]['eng_type']="";
                    $responce->rows[$i]["engagement_data"][$e]['action_selector']=$functionData["fu_ios_selector"];
                    $responce->rows[$i]["engagement_data"][$e]['action_selector_android']=$functionData["fu_and_selector"];
                    $responce->rows[$i]["engagement_data"][$e]['regular_selector']="";
                    $responce->rows[$i]["engagement_data"][$e]['regular_selector_android']="";
                    $responce->rows[$i]["engagement_data"][$e]['symbol']="";
                    $responce->rows[$i]["engagement_data"][$e]['title']=$functionData["bfu_label"];
                    $responce->rows[$i]["engagement_data"][$e]['text']="";
                    $responce->rows[$i]["engagement_data"][$e]['regular_text']="";
                    $responce->rows[$i]["engagement_data"][$e]['regular_title']="";
                    
                    break;

            }

            $e++;
        }
    }


    $j++;
    
    $json = json_encode($responce);
    echo $json;
    
}
else
{
    echo "0";
}
?>