<?php
	require 'connect.php';
	require 'functions.php';

      //$machine = $_REQUEST["machine"];
      //$orderBy = "ORDER BY biz_id ASC";
      //if ($machine == "Mufasa"){
            $orderBy = "ORDER BY biz_id DESC";
      //}
	
      $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              biz_owner_id,
                              biz_push_type,
                              biz_firebase_project,
                              ac_owner_id,
                              ac_biz_id,
                              ac_status,
                              ac_team_id,
                              ac_username,
                              ac_team_name,
                              ac_password,
                              ac_team_apple_status,
                              biz_goog_need_update_list
                              FROM (SELECT *
                        FROM  (SELECT tbl_biz.*,tbl_owners.*, tbl_queue_apk.*, ac_id, ac_wizard_status  FROM tbl_biz,tbl_owners,tbl_account,tbl_queue_apk  
                        WHERE biz_owner_id = owner_id
                        AND owner_account_id = ac_id 
                        AND biz_id = qa_biz_id
                        ) BIZ
                        LEFT JOIN (SELECT ac_owner_id,
                        ac_biz_id,
                        ac_status,
                        ac_team_id,
                        ac_username,
                        ac_password,
                        ac_team_name,
                        ac_team_apple_status
                        FROM tbl_apple_credentials)tbl_apple_credentials ON BIZ.biz_owner_id = tbl_apple_credentials.ac_owner_id AND BIZ.biz_id = tbl_apple_credentials.ac_biz_id
                        ) AS a
                        $orderBy
                        LIMIT 1";

                        //AND biz_id IN (246192)
                        //AND biz_id IN (345129)
     
	$bizList = dbGetTable( $bizToSubmitSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{   
                  $bid = $oneBiz["biz_id"];
                  $appstore = dbGetVal("SELECT count(id) vol FROM tbl_biz_appstores WHERE appstore_id = 1 AND biz_id = $bid");
                  $b_owner_id = $oneBiz["biz_owner_id"];
                  $has_apple_acc = dbGetVal("SELECT count(ac_id) FROM tbl_apple_credentials WHERE ac_status = 'active' AND ac_owner_id = $b_owner_id AND ac_team_apple_status = 'approved'");
                  if ($appstore == "0" || $has_apple_acc == "0"){
                        $responce->rows[$i]['build_only']="1";
                  }else{
                        $responce->rows[$i]['build_only']="0";
                  }


                  

                  
			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
                  $responce->rows[$i]['biz_owner_id']=$oneBiz["biz_owner_id"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
                  $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
                  $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
                  $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
                  $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
                  $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
                  $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
                  $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
                  $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
                  $responce->rows[$i]['lang']=$oneBiz["biz_sbmt_market_lang"];
                  $responce->rows[$i]['category']="";
                  $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
                  $responce->rows[$i]['biz_firebase_project'] = $oneBiz["biz_firebase_project"];
                  $responce->rows[$i]['biz_goog_need_update_list'] = $oneBiz["biz_goog_need_update_list"];
                  $responce->rows[$i]['ac_owner_id']=$oneBiz["ac_owner_id"];
                  $responce->rows[$i]['ac_biz_id']=$oneBiz["ac_biz_id"];
                  $responce->rows[$i]['ac_status']=$oneBiz["ac_status"];
                  $responce->rows[$i]['ac_team_id']=$oneBiz["ac_team_id"];
                  $responce->rows[$i]['ac_username']=$oneBiz["ac_username"];
                  $responce->rows[$i]['ac_password']=$oneBiz["ac_password"];
                  $responce->rows[$i]['ac_team_name']=$oneBiz["ac_team_name"];
                  $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
	/*
		"Tapachula DEV SEP 2015"
                      (e5fbff58-7ff4-4e6f-9702-409f07e9bcb3)
	*/
		
?>