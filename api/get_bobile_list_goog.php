<?php

	require 'connect.php';
    require 'functions.php';

      $machine = $_REQUEST["machine"];

      $table = "tbl_queue_apk";
      $orderBy = "ORDER BY biz_id ASC";
      if ($machine == "Timon"){
            $table = "tbl_queue_apk_update";
            $orderBy = "ORDER BY biz_id DESC";
      }

	
    $bizToSubmitSQL = "SELECT biz_id,
                                    owner_id,
                                    owner_username,
                                    biz_google_pdf_submit_date,
                                    biz_google_submit_step,
                                    biz_short_name,
                                    biz_office_tele,
                                    biz_fb_app_id,
                                    biz_submit_icon,
                                    biz_submit_splash,
                                    biz_submit_desc,
                                    biz_copy_right,
                                    biz_submit_keys,
                                    biz_submit_sprt_url,
                                    biz_submit_mrkt_url,
                                    biz_submit_priv_url,
                                    biz_sbmt_apl_bundle_suffix,
                                    biz_sbmt_market_lang,
                                    biz_push_type,
                                    biz_firebase_project,
                                    biz_sbmt_goo_build_version_num,
                                    '' gc_account_id,
                                    '' gc_account_id_second,
                                    '' gc_googlePlayClientId,
                                    '' gc_googlePlaySecret,
                                    '' gc_googlePlayCode,
                                    '' gc_googlePlayRefreshToken,
                                    '' gc_googlePlayAccessToken,
                                    '' gc_owner_devloper_email ,
                                    biz_goog_need_update_list
                              FROM tbl_biz,$table, tbl_owners
                              WHERE biz_owner_id = owner_id
                              AND biz_id = qa_biz_id
                              $orderBy
                              LIMIT 1";
    
    $bizList = dbGetTable( $bizToSubmitSQL );
              
	if(count($bizList) > 0)
	{
           
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{   $bid = $oneBiz["biz_id"];
                  $appstore = dbGetVal("SELECT count(id) vol FROM tbl_biz_appstores WHERE appstore_id = 2 AND biz_id = $bid");
                  if ($appstore == "0"){
                        $responce->rows[$i]['build_only']="1";
                  }else{
                        $responce->rows[$i]['build_only']="0";
                  }

                  
                  //If this app isn't linked to any google account - link it in tbl_google_credentials
                  if (!isset($oneBiz["gc_account_id"])){

                        $oneBiz["gc_account_id"] = "";
                        $oneBiz["gc_account_id_second"] = "";
                        $oneBiz["gc_googlePlayClientId"] = "";
                        $oneBiz["biz_google_pdf_submit_date"] = "";
                        $oneBiz["gc_googlePlaySecret"] = "";
                        $oneBiz["gc_owner_devloper_email"] = "";
                        $oneBiz["gc_googlePlayCode"] = "";
                        $oneBiz["gc_googlePlayRefreshToken"] = "";
                        $oneBiz["gc_googlePlayAccessToken"] = "";

                  }
                  

			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
                  $responce->rows[$i]['owner_id']=$oneBiz["owner_id"];
                  $responce->rows[$i]['acc_id']=$oneBiz["gc_account_id"];
                  $responce->rows[$i]['acc_id_sec']=$oneBiz["gc_account_id_second"];
                  $responce->rows[$i]['client_id']=$oneBiz["gc_googlePlayClientId"];
                  $responce->rows[$i]['biz_google_pdf_submit_date']=$oneBiz["biz_google_pdf_submit_date"];
                  $responce->rows[$i]['biz_google_submit_step']=$oneBiz["biz_google_submit_step"];
                  $responce->rows[$i]['client_secret']=$oneBiz["gc_googlePlaySecret"];
                  $responce->rows[$i]['gc_owner_devloper_email']=$oneBiz["gc_owner_devloper_email"];
                  $responce->rows[$i]['client_code']=$oneBiz["gc_googlePlayCode"];
                  $responce->rows[$i]['refresh_token']=$oneBiz["gc_googlePlayRefreshToken"];
                  $responce->rows[$i]['access_token']=$oneBiz["gc_googlePlayAccessToken"];
			      $responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
                  $responce->rows[$i]['biz_phone']=($oneBiz["biz_office_tele"] == "") ? "1-800-800-800" : $oneBiz["biz_office_tele"];
                  $responce->rows[$i]['biz_email']=$oneBiz["owner_username"];
			      $responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
                  $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
                  $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
                  $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
                  $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
                  $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
                  $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
                  $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
                  $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
                  $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
                  $responce->rows[$i]['category']=$oneBiz["sub_goog_val"];
                  $responce->rows[$i]['theme']=$oneBiz["tmpl_name"];
                  $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
                  $responce->rows[$i]['biz_firebase_project'] = $oneBiz["biz_firebase_project"];
                  $responce->rows[$i]['biz_sbmt_goo_build_version_num']=$oneBiz["biz_sbmt_goo_build_version_num"];
                  $responce->rows[$i]['biz_goog_need_update_list']=$oneBiz["biz_goog_need_update_list"];
                  $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
 
?>