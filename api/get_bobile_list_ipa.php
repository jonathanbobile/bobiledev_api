<?php
	require 'connect.php';
    require 'functions.php';
    
    $machine = $_REQUEST["machine"];

      $table = "tbl_queue_ipa";
      $orderBy = "ORDER BY biz_id ASC";
      if ($machine == "Mufasa"){
            $table = "tbl_queue_ipa_update";
            $orderBy = "ORDER BY biz_id DESC";
      }

      
    $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              biz_owner_id,
                              biz_push_type,
                              biz_firebase_project,
                              ac_owner_id,
                              ac_status,
                              ac_team_id,
                              ac_username,
                              ac_team_name,
                              ac_password,
                              ac_team_apple_status,
                              owner_username,
                              '' ac_phone,
                              '' ac_name
                        FROM  (
                              SELECT * FROM 
                                          (SELECT * FROM tbl_biz,tbl_owners,$table  
                                          WHERE biz_id = qi_biz_id
                                          AND biz_owner_id = owner_id
                                          ) BIZ 
                                          LEFT JOIN (
                                          SELECT ac_owner_id,
                                                ac_status,
                                                ac_team_id,
                                                ac_username,
                                                ac_password,
                                                ac_team_name,
                                                ac_team_apple_status
                                          FROM tbl_apple_enterprise_credentials
                                          ) tbl_apple_enterprise_credentials 
                                          ON BIZ.biz_owner_id = tbl_apple_enterprise_credentials.ac_owner_id 
                              ) a
                        $orderBy
                        LIMIT 1
                        ";

    $bizList = dbGetTable( $bizToSubmitSQL );
      
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{   

            if ($oneBiz["ac_team_id"] == ""){

                $defaultAccountRow = dbGetRow( "SELECT * FROM tbl_apple_enterprise_credentials WHERE ac_id = 1" );
                $oneBiz["ac_owner_id"] = $oneBiz["biz_owner_id"];
                $oneBiz["ac_biz_id"] = $oneBiz["biz_id"];
                $oneBiz["ac_status"] = $defaultAccountRow["ac_status"];
                $oneBiz["ac_team_id"] = $defaultAccountRow["ac_team_id"];
                $oneBiz["ac_username"] = $defaultAccountRow["ac_username"];
                $oneBiz["ac_password"] = $defaultAccountRow["ac_password"];
                $oneBiz["ac_team_name"] = $defaultAccountRow["ac_team_name"];
                $oneBiz["ac_team_name"] = $defaultAccountRow["ac_team_name"];
                $oneBiz["ac_publicKey"] = $defaultAccountRow["ac_publicKey"];
            }

            $responce->rows[$i]['build_only']="0";
			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
            $responce->rows[$i]['biz_owner_id']=$oneBiz["biz_owner_id"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
            $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
            $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
            $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
            $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
            $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
            $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
            $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
            $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
            $responce->rows[$i]['lang']=$oneBiz["biz_sbmt_market_lang"];
            $responce->rows[$i]['category']='none';
            $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
            $responce->rows[$i]['biz_firebase_project']=$oneBiz["biz_firebase_project"];
            $responce->rows[$i]['ac_owner_id']=$oneBiz["ac_owner_id"];
            $responce->rows[$i]['ac_biz_id']=$oneBiz["biz_id"];
            $responce->rows[$i]['ac_status']=$oneBiz["ac_status"];
            $responce->rows[$i]['ac_team_id']=$oneBiz["ac_team_id"];
            $responce->rows[$i]['ac_username']=$oneBiz["ac_username"];
            $responce->rows[$i]['ac_password']=$oneBiz["ac_password"];
            $responce->rows[$i]['ac_team_name']=$oneBiz["ac_team_name"];
            $responce->rows[$i]['ac_publicKey']=$oneBiz["ac_publicKey"];
            $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];
            $responce->rows[$i]['owner_username']=$oneBiz["owner_username"];
            $responce->rows[$i]['ac_phone']=$oneBiz["ac_phone"];
            $responce->rows[$i]['ac_name']=$oneBiz["ac_name"];
			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
		
?>