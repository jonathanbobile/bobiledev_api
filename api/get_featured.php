<?php
	require 'connect.php';
	require 'functions.php';
	
	foreach ($_GET as $key => $value)
	{
		$par = $par."$key has a value of $value <br />";
	}
	$repTEXT = "get_biz_list<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
	// start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
    $lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
    $deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
    $bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null' || $lang == '') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    // end global parameters
	
    $nib_name = "";
    if($android == "1"){
            $nib_name = "_android";
    }

    if($state == ''){
            $state = "0";
    }

    if($country == ''){
            $country = "0";
    }

    $countryNumber = ( !is_int($country) ? (ctype_digit($country)) : true );
    if(!$countryNumber){
            $country = dbGetVal("select country_id from tbl_countries where country_code='$country'");
    }

    $stateNumber = ( !is_int($state) ? (ctype_digit($state)) : true );
    if(!$stateNumber){
            $state = dbGetVal("select state_id from tbl_states where state_code='$state'");
    }
        
    $allCountry = " and if(biz_world=1,1=1,biz_addr_country_id=$country) and biz_addr_state_id=$state ";


    addAPI("api_get_featured",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
    
    $marketId = isset($_REQUEST["marketId"]) ? $_REQUEST["marketId"] : "0";
    
    $categoriesSQL="select cat_stamp,cat_background,cat_icon,cat_id,cat_name_$lang from tbl_categories order by cat_name_$lang";
    
    $addSql = "";
    if ($category > 0){
        $addSql = " and biz_category_id=$category ";
    }
        
    $editorsBizSQL="select * from (select biz_id, 
			    biz_short_name, 
			    biz_icon, 
			    biz_category_id cat_id,
			    cat_name_$lang cat_name,
			    bm_user_rating rating,
			    biz_apr_publish_date,bm_rating,
			    biz_odot,
			    biz_menu,
			    biz_first_mode_id biz_first_id,
			    biz_mod_stuct,
			    ms_view_type$nib_name ms_view_type
			    from (select bm_user_rating,bm_rating,biz_id,biz_first_mode_id,biz_category_id,biz_short_name,biz_icon,biz_apr_publish_date,biz_odot,biz_menu 
                        from tbl_biz,tbl_biz_market 
                        where biz_status=1 and biz_id=bm_biz_id and bm_editor_choice = 1 $addSql order by biz_id desc limit 10) tbl_biz,tbl_categories,tbl_biz_mod,tbl_mod_struct
			    where biz_mod_biz_id = biz_id
			    and ms_template = biz_mod_stuct
			    and ms_level_no=1
			    and biz_mod_mod_id = biz_first_mode_id
			    and cat_id = biz_category_id			   
			    ) t1
			    order by biz_id desc";
        
        
    $newestSQL="select * from (select biz_id, 
			        biz_short_name, 
			        biz_icon, 
			        biz_category_id cat_id,
			        cat_name_$lang cat_name,
			        bm_user_rating rating,
			        biz_apr_publish_date,bm_rating,
			        biz_odot,
			        biz_menu,
			        biz_first_mode_id biz_first_id,
			        biz_mod_stuct,
			        ms_view_type$nib_name ms_view_type
			        from (select bm_user_rating,bm_rating,biz_id,biz_first_mode_id,biz_category_id,biz_short_name,biz_icon,biz_apr_publish_date,biz_odot,biz_menu 
						    from tbl_biz,tbl_biz_market 
						    where biz_status=1 and biz_id=bm_biz_id and bm_editor_choice = 0 $addSql order by biz_apr_publish_date desc limit 15) tbl_biz,tbl_categories,tbl_biz_mod,tbl_mod_struct
			        where cat_id = biz_category_id
			        and biz_mod_biz_id = biz_id
			        and biz_mod_mod_id = biz_first_mode_id
			        and ms_template = biz_mod_stuct
			        and ms_level_no=1                        
			        ) t1
			        order by biz_apr_publish_date, bm_rating desc";
        
    $topRatedSQL="select * from (select biz_id, 
			        biz_short_name, 
			        biz_icon, 
			        biz_category_id cat_id,
			        cat_name_$lang cat_name,
			        bm_user_rating rating,
			        biz_apr_publish_date,bm_rating,
			        biz_odot,
			        biz_menu,
			        biz_first_mode_id biz_first_id,
			        biz_mod_stuct,
			        ms_view_type$nib_name ms_view_type
			        from (select bm_user_rating,bm_rating,biz_id,biz_first_mode_id,biz_category_id,biz_short_name,biz_icon,biz_apr_publish_date,biz_odot,biz_menu 
					    from tbl_biz,tbl_biz_market 
					    where biz_status=1 and biz_id=bm_biz_id and bm_rating > 6 and bm_editor_choice = 0 $addSql order by biz_apr_publish_date desc limit 15) tbl_biz,tbl_categories,tbl_biz_mod,tbl_mod_struct
			        where cat_id = biz_category_id
			        and ms_template = biz_mod_stuct
			        and ms_level_no=1
			        and biz_mod_biz_id = biz_id
			        and biz_mod_mod_id = biz_first_mode_id
			        ) t1
			        order by biz_apr_publish_date, bm_rating desc";
        
    
   
    
    $catList = dbGetTable( $categoriesSQL );
    $editorList = dbGetTable( $editorsBizSQL );
    $newestList = dbGetTable( $newestSQL );
    $topRatedList = dbGetTable( $topRatedSQL );
    
    if(count($catList) > 0){
        $i = 0;
        foreach ($catList as $oneCat){
                $responce->featured[0]['cat'][$i]['cat_id']=$oneCat["cat_id"];
                $responce->featured[0]['cat'][$i]['cat_name']=$oneCat["cat_name_$lang"];
                $responce->featured[0]['cat'][$i]['cat_background']="c".$oneCat["cat_background"];
                $i++;
        }
    }
   
    
    if(count($editorList) > 0){
        $j = 0;
        foreach ($editorList as $oneApp){
                $theID = $oneApp["biz_id"];
                $modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $theID and mod_id>0 and biz_mod_active=1 and mod_type=1");
                
                $biz_num_mode = dbGetVal("select count(biz_mod_mod_id) from tbl_biz_mod where biz_mod_biz_id={$oneApp["biz_id"]}");
                
                $responce->featured[0]['editors_app'][$j]['biz_id']=$oneApp["biz_id"];
                $responce->featured[0]['editors_app'][$j]['biz_short_name']=$oneApp["biz_short_name"];
                $responce->featured[0]['editors_app'][$j]['biz_icon']=$oneApp["biz_icon"];
                $responce->featured[0]['editors_app'][$j]['cat_id']=$oneApp["cat_id"];
                $responce->featured[0]['editors_app'][$j]['cat_name']=$oneApp["cat_name"];
                $responce->featured[0]['editors_app'][$j]['biz_first_id']=$oneApp["biz_first_id"];
                $responce->featured[0]['editors_app'][$j]['ms_view_type']=$oneApp["ms_view_type"];
                $responce->featured[0]['editors_app'][$j]['biz_num_mod']=$biz_num_mode;
                $responce->featured[0]['editors_app'][$j]['biz_layout']=$oneApp["biz_menu"];
                $responce->featured[0]['editors_app'][$j]['rating']=$oneApp["rating"];
                $responce->featured[0]['editors_app'][$j]['biz_market_img']="pach";
                $responce->featured[0]['editors_app'][$j]['randNumber'] = rand(1, 35);
                $k = 0;
                if(count($modList) > 0){
                    foreach ($modList as $oneMod){
                        $responce->featured[0]['editors_app'][$j]['biz_modules'][$k]["mod_name"] = $oneMod["mod_name"];
                        $responce->featured[0]['editors_app'][$j]['biz_modules'][$k]["mod_pic"] = $oneMod["mod_pic"];
                        $k++;
                    }
                }else {
                    $responce->featured[0]['editors_app'][$j]['biz_modules']="0";
                }
                $newOdot = dbGetVal("select md_info from tbl_mod_data1 where md_biz_id=$theID and md_element=6 and md_required=1");
                $responce->featured[0]['editors_app'][$j]['description']=$newOdot;
                $j++;
        }
    
    }else {
        
        $responce->featured[0]['editors_app'] ="0";
    }
    
    if(count($newestList) > 0){
        $j = 0;
        foreach ($newestList as $oneApp){
                $theID = $oneApp["biz_id"];
                $modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $theID and mod_id>0 and biz_mod_active=1 and mod_type=1");
                
                $biz_num_mode = dbGetVal("select count(biz_mod_mod_id) from tbl_biz_mod where biz_mod_biz_id={$oneApp["biz_id"]}");
                
                $responce->featured[0]['new_app'][$j]['biz_id']=$oneApp["biz_id"];
                $responce->featured[0]['new_app'][$j]['biz_short_name']=$oneApp["biz_short_name"];
                $responce->featured[0]['new_app'][$j]['biz_icon']=$oneApp["biz_icon"];
                $responce->featured[0]['new_app'][$j]['cat_id']=$oneApp["cat_id"];
                $responce->featured[0]['new_app'][$j]['cat_name']=$oneApp["cat_name"];
                $responce->featured[0]['new_app'][$j]['biz_first_id']=$oneApp["biz_first_id"];
                $responce->featured[0]['new_app'][$j]['ms_view_type']=$oneApp["ms_view_type"];
                $responce->featured[0]['new_app'][$j]['biz_num_mod']=$biz_num_mode;
                $responce->featured[0]['new_app'][$j]['biz_layout']=$oneApp["biz_menu"];
                $responce->featured[0]['new_app'][$j]['rating']=$oneApp["rating"];
                $responce->featured[0]['new_app'][$j]['biz_market_img']="pach";
                $responce->featured[0]['new_app'][$j]['randNumber'] = rand(1, 35);
                $k = 0;
                if(count($modList) > 0){
                    foreach ($modList as $oneMod){
                        $responce->featured[0]['new_app'][$j]['biz_modules'][$k]["mod_name"] = $oneMod["mod_name"];
                        $responce->featured[0]['new_app'][$j]['biz_modules'][$k]["mod_pic"] = $oneMod["mod_pic"];
                        $k++;
                    }
                }else {
                    $responce->featured[0]['new_app'][$j]['biz_modules']="0";
                }
                $newOdot = dbGetVal("select md_info from tbl_mod_data1 where md_biz_id=$theID and md_element=6 and md_required=1");
                $responce->featured[0]['new_app'][$j]['description']=$newOdot;
                $j++;
        }
    
    }else {
        
        $responce->featured[0]['new_app'] ="0";
    }
    
    if(count($topRatedList) > 0){
        $j = 0;
        foreach ($topRatedList as $oneApp){
                $theID = $oneApp["biz_id"];
                $modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $theID and mod_id>0 and biz_mod_active=1 and mod_type=1");
                
                $biz_num_mode = dbGetVal("select count(biz_mod_mod_id) from tbl_biz_mod where biz_mod_biz_id={$oneApp["biz_id"]}");
                
                $responce->featured[0]['top_app'][$j]['biz_id']=$oneApp["biz_id"];
                $responce->featured[0]['top_app'][$j]['biz_short_name']=$oneApp["biz_short_name"];
                $responce->featured[0]['top_app'][$j]['biz_icon']=$oneApp["biz_icon"];
                $responce->featured[0]['top_app'][$j]['cat_id']=$oneApp["cat_id"];
                $responce->featured[0]['top_app'][$j]['cat_name']=$oneApp["cat_name"];
                $responce->featured[0]['top_app'][$j]['biz_first_id']=$oneApp["biz_first_id"];
                $responce->featured[0]['top_app'][$j]['ms_view_type']=$oneApp["ms_view_type"];
                $responce->featured[0]['top_app'][$j]['biz_num_mod']=$biz_num_mode;
                $responce->featured[0]['top_app'][$j]['biz_layout']=$oneApp["biz_menu"];
                $responce->featured[0]['top_app'][$j]['rating']=$oneApp["rating"];
                $responce->featured[0]['top_app'][$j]['biz_market_img']="pach";
                $responce->featured[0]['top_app'][$j]['randNumber'] = rand(1, 35);
                $k = 0;
                if(count($modList) > 0){
                    foreach ($modList as $oneMod){
                        $responce->featured[0]['top_app'][$j]['biz_modules'][$k]["mod_name"] = $oneMod["mod_name"];
                        $responce->featured[0]['top_app'][$j]['biz_modules'][$k]["mod_pic"] = $oneMod["mod_pic"];
                        $k++;
                    }
                }else {
                    $responce->featured[0]['top_app'][$j]['biz_modules']="0";
                }
                $newOdot = dbGetVal("select md_info from tbl_mod_data1 where md_biz_id=$theID and md_element=6 and md_required=1");
                $responce->featured[0]['top_app'][$j]['description']=$newOdot;
                $j++;
        }
    
    }else {
        
        $responce->featured[0]['top_app'] ="0";
    }
    
    if ($responce != ""){
        $json = json_encode($responce);
        echo $json;

    }else{
	echo "0";
    }
	
		
?>
