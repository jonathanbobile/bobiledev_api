<?php
	require 'connect.php';
	require 'functions.php';
    
    //foreach ($_GET as $key => $value)
    //{
    //    $par = $par."$key has a value of $value <br />";
    //}
    //$repTEXT = "get_news<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
    // start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
	$lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
	$deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
	$bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    // end global parameters
    
    $lastid = $_GET["lastid"];
    $oldestid = $_GET["oldestid"];
    
    $sqlBizId = " and biz_mod_biz_id=$bizid ";
    if($bizid == "0")
    {
        $sqlBizId = "";
    }
    
    $nib_name = "";
	if($android == "1")
	{
		$nib_name = "_android";
	}
	
    $tmpl_img1_w = "120";
	if($iphone == "0")
	{
		$mainwidth = "620";
        $tmpl_img1_w = "240";
	}
	else if($iphone == "1")
	{
		$mainwidth = "310";
	}
	else
	{
		if($iphone > 620)
		{
			$mainwidth = "620";
            $tmpl_img1_w = "240";
		}
		else
		{
			$mainwidth = "100%";
		}
	}
    
    $countryNumber = ( !is_int($country) ? (ctype_digit($country)) : true );
	if(!$countryNumber)
	{
		$country = dbGetVal("select country_id from tbl_countries where country_code='$country'");
	}
	
	$stateNumber = ( !is_int($state) ? (ctype_digit($state)) : true );
	if(!$stateNumber)
	{
		$state = dbGetVal("select state_id from tbl_states where state_code='$state'");
	}
	
	$existcountry = dbGetVal("select count(country_id) from tbl_countries where country_id=$country");	
	if($existcountry == "0")
	{
		$country = "0";
	}
	
	if($state != "0")
	{
		$existstate = dbGetVal("select count(state_id) from tbl_states where state_id=$state");	
		if($existstate == "0")
		{
			$state = "0";
		}
	}
	
    $sqlNew = "";
    if($lastid != "" && $oldestid == "")
    {
        $sqlNew = " and md_row_id>$lastid ";
    }
    else if($lastid == "" && $oldestid != "")
    {
        $sqlNew = " and md_row_id<$oldestid ";
    }
    else
    {
        addAPI("api_get_news",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
    }
	
	$marketId = isset($_GET["marketId"]) ? $_GET["marketId"] : "0";
    $lang = isset($_GET["displayLang"]) ? $_GET["displayLang"] : $lang;
    if($lang == "iw") $lang = "he";
    
	$newsSQL = "select * from (
                    SELECT 
                    md_row_id_desc,
                    md_row_id,
                    biz_dir,
                    ms_html,
                    cat_name_$lang cat_name,
                    md_icon,
                    md_head,
                    md_info,
                    md_date,
                    biz_first_mode_id,
                    md_biz_id,
                    biz_icon,
                    biz_menu,
                    md_biz_id_real,
                    md_tap_like,
                    biz_short_name,
                    mrktbiz_user_rank rating,
                    biz_odot,
                    biz_category_id
                    FROM (select md_icon,md_biz_id_real,md_head,md_info,md_date,md_biz_id,md_row_id,md_tap_like,md_row_id_desc,md_mod_id 
                            from tbl_mod_data3 
                            where md_level_no=1 and md_mod_id=3 and md_head <> 'News Title' and md_head <> 'Header' $sqlBizId $sqlNew order by md_row_id_desc) tbl_mod_data3,
                    tbl_biz_mod,tbl_biz,tbl_template,tbl_mod_struct,tbl_categories,tbl_res_market_biz
                    WHERE biz_id=mrktbiz_biz_id
                    and mrktbiz_market_id = $marketId
                    and biz_category_id = cat_id
                    and md_mod_id=biz_mod_mod_id
                    and md_biz_id = biz_mod_biz_id
                    and biz_id=md_biz_id
                    and biz_theme=tmpl_id
                    and biz_mod_active=1 
                    and (biz_status = 1 or (biz_status = 0 and biz_id=0))
                    and biz_isdeleted=0
                    and biz_mod_stuct = ms_template
                    ) t1
                    order by md_row_id desc	limit 10";
    //echo $newsSQL;
    //send_mail("dany@paptap.com","crazyio2005@gmail.com","PapTap API debug",$newsSQL,"Dany","Dany");
    
	$newsList = dbGetTable( $newsSQL );
	if(count($newsList) > 0)
	{
		$i = 0;
		foreach ($newsList as $oneNews) 
		{
            
            $parent_id = $oneNews["md_row_id"];
            $subElements = dbGetTable("SELECT md_info FROM tbl_mod_data3 WHERE md_parent = $parent_id AND md_element = 6");
            $newStory = "";
            if (count($subElements)>0){
                foreach ($subElements as $para) {
                    $newStory = $newStory."".$para["md_info"]."\n";
                }
            }
            
            
            $floatDir = "left";
            $marginDir = "margin-left";
            $opMarginDir = "margin-right";
            if($oneNews["biz_dir"] == 'rtl')
            {
                $floatDir = "right";
                $marginDir = "margin-right";
                $opMarginDir = "margin-left";
            }
            
            $imgObject = "<img style='float: left; margin:5px; cursor:pointer;' src='##biz_img1##' height='##biz_img1_w##' />";
            
            $tmpl_html_out = $oneNews["ms_html"];
            
            if($oneNews["md_icon"] != null && $oneNews["md_icon"] != "")
                $tmpl_html_out = str_replace( '##imgobject##' , $imgObject ,$tmpl_html_out);
            else
                $tmpl_html_out = str_replace( '##imgobject##' , "" ,$tmpl_html_out);
            
            $tmpl_html_out = str_replace( '##mainwidth##' , $mainwidth ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '#header#' , $oneNews["md_head"] ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '#md_info#' , ($oneNews["md_info"] == null) ? "" : $oneNews["md_info"] ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '##dir##' , $oneNews["biz_dir"] ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '##floatDir##' , $floatDir ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '##marginDir##' , $marginDir ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '##opMarginDir##' , $opMarginDir ,$tmpl_html_out);
            
            // image height
            $tmpl_html_out = str_replace( '##biz_img1_w##' , $tmpl_img1_w ,$tmpl_html_out);
            
            $tmpl_html_out = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tmpl_html_out);
            
            $to_time = strtotime($oneNews["md_date"]);
            $from_time = strtotime("now");
            
            $mins = round(abs($to_time - $from_time) / 60,0);
            $multyTime = ($mins > 1) ? "s" : "";
            $dtData = $mins. " minute$multyTime ago";
            if($mins >= 60)
            {
                $mins = round($mins / 60,0);
                $multyTime = ($mins > 1) ? "s" : "";
                $dtData = $mins. " hour$multyTime ago";
                if($mins >= 24)
                {
                    $mins = round($mins / 24,0);
                    $multyTime = ($mins > 1) ? "s" : "";
                    $dtData = $mins. " day$multyTime ago";
                }
            }
            $f_id = $oneNews["biz_first_mode_id"];
			if($f_id == "0") $f_id = "1";
            
            $realBizId = $oneNews["md_biz_id"];
            $realIcon = $oneNews["biz_icon"];
            $realFirstId = $oneNews["biz_first_mode_id"];
            if($oneNews["md_biz_id_real"] != "0")
            {
                $realBizId = $oneNews["md_biz_id_real"];
                $realIcon = "icon.png";
                $realFirstId = dbGetVal("SELECT biz_first_mode_id FROM tbl_biz WHERE biz_id = $realBizId");
            }
            
            $structSQL = "SELECT REPLACE(biz_mod_mobile_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,ms_view_type$nib_name ms_view_type 
			FROM tbl_biz_mod,tbl_mod_struct 
			WHERE biz_mod_active=1 and biz_mod_biz_id=$realBizId 
			and biz_mod_mod_id=".$f_id." 
			and ms_mod_id=biz_mod_mod_id 
			and ms_level_no=1 
			and ms_template=biz_mod_stuct";
			$structRow = dbGetRow( $structSQL );
            
            $biz_num_mod = dbGetVal("SELECT COUNT(biz_mod_mod_id) AS biz_num_mod
                            FROM tbl_biz_mod
                            WHERE biz_mod_active = 1  and biz_mod_mod_id <> 0 AND  biz_mod_biz_id = $realBizId");
            
            $modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $realBizId and mod_id>0 and biz_mod_active=1 and mod_type=1");
            
            $striped_links = preg_replace('#<a.*?>(.*?)</a>#i', '\1', stripslashes($oneNews["md_info"]));
            
			$responce->rows[$i]['recordID']=$oneNews["md_row_id"];
			$responce->rows[$i]['bizID']=$realBizId;
			$responce->rows[$i]['bizShortName']=$oneNews["biz_short_name"];
            $responce->rows[$i]['bizIcon']=$realIcon;
            $responce->rows[$i]['postTime']=$dtData;
            $responce->rows[$i]['postTime_date']=trim($oneNews["md_date"]);
            $responce->rows[$i]['headLine']=$oneNews["md_head"];
            $responce->rows[$i]['story']=$newStory;
            $responce->rows[$i]['story_short']=$oneNews["md_head"];
            $responce->rows[$i]['bodyImage']=($oneNews["md_icon"] == null) ? "transparent.png" : $oneNews["md_icon"];
            $responce->rows[$i]['html']=$tmpl_html_out."<br><br>";
            $responce->rows[$i]['biz_first_id']=$realFirstId;
            $responce->rows[$i]['biz_mod_mod_name']=$structRow["biz_mod_mod_name"];
            $responce->rows[$i]['ms_view_type']=$structRow["ms_view_type"];
            $responce->rows[$i]['biz_num_mod']=$biz_num_mod;
            $responce->rows[$i]['cat_id']=$oneNews["biz_category_id"];
            $responce->rows[$i]['cat_name']=$oneNews["cat_name"];
            $responce->rows[$i]['biz_layout']=$oneNews["biz_menu"];
            $responce->rows[$i]['rating']=$oneNews["rating"];
            $responce->rows[$i]['biz_market_img']="pach";
            $responce->rows[$i]['randNumber']= rand(1, 35);
            $responce->rows[$i]['tap_likes']=$oneNews["md_tap_like"];
            $k = 0;
            if(count($modList) > 0){
                foreach ($modList as $oneMod){
                    $responce->rows[$i]['biz_modules'][$k]["mod_name"] = $oneMod["mod_name"];
                    $responce->rows[$i]['biz_modules'][$k]["mod_pic"] = $oneMod["mod_pic"];
                    $k++;
                }
            }else {
                $responce->rows[$i]['biz_modules']="0";
            }
            
            $newOdot = dbGetVal("select md_info from tbl_mod_data1 where md_biz_id=$realBizId and md_element=6 and md_required=1");
            $responce->rows[$i]['description']=$newOdot;
            
			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
	}
	else
	{
		echo "0";
	}
	
		
?>

