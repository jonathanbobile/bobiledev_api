<?php
	require 'connect.php';
	require 'functions.php';
	
	foreach ($_GET as $key => $value)
	{
		$par = $par."$key has a value of $value <br />";
	}
	$repTEXT = "get_levels<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
    // start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
	$lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
	$deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
	$bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];
    
    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    // end global parameters
	

    if($bizid == "0") $bizid = $_GET["biz_id"];    //temporery  for android/iphone 
    
	if($iphone == "0")
	{
		$mainwidth = "620";
	}
	else if($iphone == "1")
	{
		$mainwidth = "310";
	}
	else
	{
		if($iphone > 620)
		{
			$mainwidth = "620";
		}
		else
		{
			$mainwidth = "100%";
		}
	}
	
	if($parent == '') $parent = "0";
	
    addAPI("api_get_levels",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
	
	$bizSQL = "select * from tbl_biz where biz_id=$bizid";
	$bizRow = dbGetRow( $bizSQL );
	
	$levelsSQL =  "select * from tbl_mod_data main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar from tbl_mod_data  group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod 
					where biz_mod_active=1 and md_biz_id=$bizid and md_mod_id=$mod_id and md_level_no=$level_no and md_parent=$parent 
					and md_biz_id = biz_mod_biz_id and md_mod_id=biz_mod_mod_id 
					and ms_mod_id=md_mod_id and ms_level_no=md_level_no
					and ms_template=biz_mod_stuct 
					order by md_index";
				//echo $levelsSQL;
	$levelsList = dbGetTable( $levelsSQL );

	if(count($levelsList) > 0)
	{
        $tmplSQL = "select * from tbl_template where tmpl_id = " . $bizRow["biz_theme"];
        $tmplRow = dbGetRow( $tmplSQL );
        $tFontColor = $tmplRow["tmpl_font_color"];
        
		$i = 0;
		foreach ($levelsList as $oneElement) 
		{
			$tmpl_html_out = $oneElement["ms_html"];
			if($iphone == "0" || $iphone >= 620)
			{
				$oneElement["ms_img1_w"] = $oneElement["ms_img1_w"] * 2;
				$oneElement["ms_img2_w"] = $oneElement["ms_img2_w"] * 2;
				$oneElement["ms_img3_w"] = $oneElement["ms_img3_w"] * 2;
				$oneElement["ms_img4_w"] = $oneElement["ms_img4_w"] * 2;
				$oneElement["ms_img5_w"] = $oneElement["ms_img5_w"] * 2;
				$oneElement["ms_img6_w"] = $oneElement["ms_img6_w"] * 2;
				$oneElement["ms_img7_w"] = $oneElement["ms_img7_w"] * 2;
				$oneElement["ms_img8_w"] = $oneElement["ms_img8_w"] * 2;
				$oneElement["ms_img9_w"] = $oneElement["ms_img9_w"] * 2;
				$oneElement["ms_img10_w"] = $oneElement["ms_img10_w"] * 2;
			}
			else if ($iphone != "1")
			{
				$oneElement["ms_img1_w"] = floor($iphone * (($oneElement["ms_img1_w"] / 3.2) / 100));
				$oneElement["ms_img2_w"] = floor($iphone * (($oneElement["ms_img2_w"] / 3.2) / 100));
				$oneElement["ms_img3_w"] = floor($iphone * (($oneElement["ms_img3_w"] / 3.2) / 100));
				$oneElement["ms_img4_w"] = floor($iphone * (($oneElement["ms_img4_w"] / 3.2) / 100));
				$oneElement["ms_img5_w"] = floor($iphone * (($oneElement["ms_img5_w"] / 3.2) / 100));
				$oneElement["ms_img6_w"] = floor($iphone * (($oneElement["ms_img6_w"] / 3.2) / 100));
				$oneElement["ms_img7_w"] = floor($iphone * (($oneElement["ms_img7_w"] / 3.2) / 100));
				$oneElement["ms_img8_w"] = floor($iphone * (($oneElement["ms_img8_w"] / 3.2) / 100));
				$oneElement["ms_img9_w"] = floor($iphone * (($oneElement["ms_img9_w"] / 3.2) / 100));
				$oneElement["ms_img10_w"] = floor($iphone * (($oneElement["ms_img10_w"] / 3.2) / 100));
			}
			
			$floatDir = "left";
			$marginDir = "margin-left";
			$opMarginDir = "margin-right";
			if($bizRow["biz_dir"] == 'rtl')
			{
				$floatDir = "right";
				$marginDir = "margin-right";
				$opMarginDir = "margin-left";
			}
			
            $odotReplace = $oneElement["md_info"];
			if($oneElement["md_mod_id"] == "24")
			{
				$addField = "<input type=\"hidden\" value=\"@@".$oneElement["md_info1"]."@@\" />";
				$tmpl_html_out = str_replace( '</body>' , $addField.'</body>' ,$tmpl_html_out);
                $odotReplace = "http://www.youtube.com/embed/".$oneElement["md_info1"];
			}			
			
			$tmpl_html_out = str_replace( '##mainwidth##' , $mainwidth ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_name##' , $bizRow["biz_name"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_odot##' , $odotReplace ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_odot1##' , $oneElement["md_info1"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_odot2##' , $oneElement["md_info2"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_odot3##' , $oneElement["md_info3"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_odot4##' , $oneElement["md_info4"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_odot5##' , $oneElement["md_info5"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##dir##' , $bizRow["biz_dir"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##floatDir##' , $floatDir ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##dir##' , $floatDir ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##marginDir##' , $marginDir ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##opMarginDir##' , $opMarginDir ,$tmpl_html_out);
            $tmpl_html_out = str_replace( '#BODYCOLOR#' , $tFontColor ,$tmpl_html_out);
            
			// image width
			$tmpl_html_out = str_replace( '##biz_img1_w##' , ($oneElement["md_pic"] == null) ? "0" : $oneElement["ms_img1_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img2_w##' , ($oneElement["md_pic1"] == null) ? "0" : $oneElement["ms_img2_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img3_w##' , ($oneElement["md_pic2"] == null) ? "0" : $oneElement["ms_img3_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img4_w##' , ($oneElement["md_pic3"] == null) ? "0" : $oneElement["ms_img4_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img5_w##' , ($oneElement["md_pic4"] == null) ? "0" : $oneElement["ms_img5_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img6_w##' , ($oneElement["md_pic5"] == null) ? "0" : $oneElement["ms_img6_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img7_w##' , $oneElement["ms_img7_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img8_w##' , $oneElement["ms_img8_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img9_w##' , $oneElement["ms_img9_w"] ,$tmpl_html_out);
			$tmpl_html_out = str_replace( '##biz_img10_w##' , $oneElement["ms_img10_w"] ,$tmpl_html_out);
			
			$tmpl_html_out = preg_replace('#<a.*?>(.*?)</a>#i', '\1', $tmpl_html_out);
		
			$responce->rows[$i]['md_row_id']=$oneElement["md_row_id"];
			$responce->rows[$i]['md_biz_id']=$oneElement["md_biz_id"];
			$responce->rows[$i]['md_mod_id']=$oneElement["md_mod_id"];
			$responce->rows[$i]['md_level_no']=$oneElement["md_level_no"];
			$responce->rows[$i]['md_parent']=$oneElement["md_parent"];
			$responce->rows[$i]['md_index']=$oneElement["md_index"];
			$responce->rows[$i]['md_icon']=($oneElement["md_icon"] == null) ? "no_icon_levels.png" : $oneElement["md_icon"];			
			$responce->rows[$i]['md_head']=($oneElement["md_head"] == null) ? "" : $oneElement["md_head"];
			$responce->rows[$i]['md_desc']=($oneElement["md_desc"] == null) ? "" : $oneElement["md_desc"];
			$responce->rows[$i]['md_loc']=($oneElement["md_loc"] == null) ? "" : $oneElement["md_loc"];
			$responce->rows[$i]['md_date']=($oneElement["md_date"] == null) ? "" : $oneElement["md_date"];
			$responce->rows[$i]['md_info']=($tmpl_html_out == null) ? "" : $tmpl_html_out;
			$responce->rows[$i]['md_score']=($oneElement["md_score"] == null) ? "" : $oneElement["md_score"];
			$responce->rows[$i]['md_price']=($oneElement["md_price"] == null) ? "" : $oneElement["md_price"];
			$responce->rows[$i]['cnt']=($oneElement["cnt"] == null) ? "0" : $oneElement["cnt"];
			
			$responce->rows[$i]['biz_level_img']["1"]=($oneElement["md_pic"] == null) ? "no_img2_html.png" : $oneElement["md_pic"];
			$responce->rows[$i]['biz_level_img']["2"]=($oneElement["md_pic1"] == null) ? "transparent.png" : $oneElement["md_pic1"];
			$responce->rows[$i]['biz_level_img']["3"]=($oneElement["md_pic2"] == null) ? "transparent.png" : $oneElement["md_pic2"];
			$responce->rows[$i]['biz_level_img']["4"]=($oneElement["md_pic3"] == null) ? "transparent.png" : $oneElement["md_pic3"];
			$responce->rows[$i]['biz_level_img']["5"]=($oneElement["md_pic4"] == null) ? "transparent.png" : $oneElement["md_pic4"];
			$responce->rows[$i]['biz_level_img']["6"]=($oneElement["md_pic5"] == null) ? "transparent.png" : $oneElement["md_pic5"];
			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
	}
	else
	{
		echo "0";
	}
	
		
?>