<?php
    require_once("../admin/MVC/config.php");
	require_once 'connect.php';
	require_once 'functions.php';
    require_once("../admin/MVC/Database.php");
    require_once("../admin/libs/languageManager.php");
    require_once("../admin/MVC/Model.php");
    require_once("../admin/models/ecommerceManager.php");  
    require_once('../admin/libs/customersManager.php');
	
    // start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
	$lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
	$deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
	$bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];
    $prodId = $_GET["prodId"];
    
    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    if($prodId == 'null') $prodId = "";

    if(isset($_REQUEST['marketId'])){
        $marketId = $_REQUEST['marketId'];
    }else{
        $marketId = $_REQUEST['marketId'] = "-1";
    }
    
    $cust_id = customersManager::getCusIdByDevice($bizid,$deviceID);
    // end global parameters
	
    $nib_name = "";
    if($android == "1")
    {
        $nib_name = "_android";
    }

    if($bizid == "0") $bizid = $_GET["biz_id"];    //temporery  for android/iphone 
	
	if($parent == '') $parent = "0";
    
    $ecommerceManager = new ecommerceManager($bizid);
	
    addAPI("api_get_levels",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state,"",$marketId);
		
    $workingOnOneProduct = false;
    
    if($mod_id == "9"){        

        $levelsSQL =  "select * from tbl_mod_data999 main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data999  where md_biz_id=$bizid group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
					where biz_mod_active=1 and md_biz_id=$bizid and md_mod_id=999 and md_parent=$parent and me_id=md_element
					and md_biz_id = biz_mod_biz_id and biz_mod_mod_id=9
					and ms_mod_id=md_mod_id
					and ms_template=biz_mod_stuct 
                    and md_bool1=1
                    and md_visible_element=1
					order by md_index";
        
        //send_mail("info@paptap.com","crazyio2005@gmail.com","PapTap API debug","1 ".$levelsSQL,"Dany","Dany");
        
        $levelsList = dbGetTable( $levelsSQL );
        
        if (count($levelsList) == 0 ){ //not category - must be a product
	   
            if($level_no == 1 && $parent != "-1"){
               echo "0";
               exit;
           }
           else{
               
                $qs = $_REQUEST["qs"];
    
                if($parent == "-1") 
                    $filters['keyword'] = $qs;
                else
                    $filters['category'] = $parent;
 
                if($prodId != ""){
                    $filters['productId'] = $prodId;
                }

               $filters['itemsPerPage'] = -1;
               $levelsList = $ecommerceManager->getProducts($filters,1);
               
               if($deviceID != ""){

                   if($parent !=""){
                       $catName = addslashes(dbGetVal("select md_head from tbl_mod_data999 where md_row_id=$parent"));
                       if($catName != ""){
                           dbExecute("insert into tbl_cust_history SET 
			                    ch_status='category',
                                ch_replace_text='$catName',
                                ch_cust_id=$cust_id");
                       }
                    }
               }
               
               if(count($levelsList) == 0){
                   
                   if ($parent != "-1" && !$ecommerceManager->isEmptyCategory($parent)){
                       
                       if($parent != 0) {
                           $workingOnOneProduct = true;
                       }                   
                  
                       $levelsSQL =  "select * from tbl_mod_data$mod_id main 
					    LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data$mod_id  where md_biz_id=$bizid group by md_parent) intbl
					    on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
					    where biz_mod_active=1 
                        and md_biz_id=$bizid 
                        and md_mod_id=$mod_id 
                        and md_parent=$parent 
                        and me_id=md_element
					    and md_biz_id = biz_mod_biz_id 
                        and md_mod_id=biz_mod_mod_id 
					    and ms_mod_id=md_mod_id 
                        and ms_level_no=1
					    and ms_template=biz_mod_stuct 
                        and md_visible_element=1
					    order by md_index";
                   
                       //send_mail("info@paptap.com","crazyio2005@gmail.com","PapTap API debug","2 ".$levelsSQL,"Dany","Dany");
                       
                       $levelsList = dbGetTable( $levelsSQL );
                       
                   }
                   
                   if($ecommerceManager->isEmptyCategory($parent)){
                       echo "0";
                       exit;
                   }
                   
               }
           }
		}
	}
    else{
        
        $levelsSQL =  "select * from tbl_mod_data$mod_id main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data$mod_id  where md_biz_id=$bizid group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
					where biz_mod_active=1 and md_biz_id=$bizid and md_mod_id=$mod_id and md_parent=$parent and me_id=md_element
					and md_biz_id = biz_mod_biz_id and md_mod_id=biz_mod_mod_id 
					and ms_mod_id=md_mod_id and ms_level_no=1
					and ms_template=biz_mod_stuct 
                    and md_visible_element=1
					order by md_index";
        $levelsList = dbGetTable( $levelsSQL );

    }  

    $variants = $ecommerceManager->getVariationsAPI($parent);
    $price = array();
    
	if(count($levelsList) > 0 || ($mod_id == "9" && $parent != "-1") )
	{        
		$i = 0;
        $attrIndex = 0;
        $itemRow = dbGetRow("SELECT it_id,it_need_shipment FROM tbl_item_type WHERE it_mod_id = $mod_id");
        $storeRow = dbGetRow("select * from ecommerce_store_settings where ess_biz_id=$bizid");
        
        $item_type = $itemRow["it_id"];
        $item_ship = ($itemRow["it_need_shipment"] == null) ? "0" : $itemRow["it_need_shipment"];

        //add product Image
        if($mod_id == "9" && $workingOnOneProduct){
            $productRow = dbGetRow("select * from tbl_mod_data9 where md_row_id=$parent");
                        
            //image
            if($productRow["md_icon"] != ""){
                $responce->rows[$i]['md_row_id']=0;
                $responce->rows[$i]['md_biz_id']=$bizid;
                $responce->rows[$i]['md_mod_id']=9;
                $responce->rows[$i]['md_level_no']=2;
                $responce->rows[$i]['md_parent']=$parent;
                $responce->rows[$i]['md_index']=0;
                $responce->rows[$i]['md_icon']="";			
                $responce->rows[$i]['md_head']="";
                $responce->rows[$i]['md_desc']="";
                $responce->rows[$i]['md_loc']="";
                $responce->rows[$i]['md_date']="";
                $responce->rows[$i]['md_info']="";
                $responce->rows[$i]['md_score']="";
                $responce->rows[$i]['md_price']="0";
                $responce->rows[$i]['md_featured']="0";
                $responce->rows[$i]['md_has_discount']="0";
                $responce->rows[$i]['md_discount_type']="";
                $responce->rows[$i]['md_discount_amount']="0";
                $responce->rows[$i]['md_original_price']="0";
                $responce->rows[$i]['cnt']=0;
                $responce->rows[$i]['md_link']="";
                $responce->rows[$i]['md_type']="product_image";
                $responce->rows[$i]['md_next_view']="";
                $responce->rows[$i]['md_bool1']=0;
                $responce->rows[$i]['md_bool2']=0;
                $responce->rows[$i]['item_type']=0;
                $responce->rows[$i]['item_sku']="";
                $responce->rows[$i]['md_first_img']="";
                $responce->rows[$i]['md_photos']=0;
                $responce->rows[$i]['md_albums']=0;
                $responce->rows[$i]['currency']="";
                $responce->rows[$i]['variations_count']=0;

                $responce->rows[$i]['biz_level_img']["1"]= str_replace("paptap-thumbs","paptap",$productRow["md_icon"]);
                $responce->rows[$i]['biz_level_img']["2"]="";
                $responce->rows[$i]['biz_level_img']["3"]="";
                $responce->rows[$i]['biz_level_img']["4"]="";
                $responce->rows[$i]['biz_level_img']["5"]="";
                $responce->rows[$i]['biz_level_img']["6"]="";
                $responce->rows[$i]['biz_level_img']["7"]="";
                $responce->rows[$i]['biz_level_img']["8"]="";
                $responce->rows[$i]['biz_level_img']["9"]="";
                $responce->rows[$i]['biz_level_img']["10"]="";
                $responce->rows[$i]['biz_level_img']["11"]="";
                
                $i++;
            }
            
            //title
            $responce->rows[$i]['md_row_id']=0;
            $responce->rows[$i]['md_biz_id']=$bizid;
            $responce->rows[$i]['md_mod_id']=9;
            $responce->rows[$i]['md_level_no']=2;
            $responce->rows[$i]['md_parent']=$parent;
            $responce->rows[$i]['md_index']=0;
            $responce->rows[$i]['md_icon']="";			
            $responce->rows[$i]['md_head']="";
            $responce->rows[$i]['md_desc']=stripslashes($productRow["md_head"]);
            $responce->rows[$i]['md_loc']="";
            $responce->rows[$i]['md_date']="";
            $responce->rows[$i]['md_info']="";
            $responce->rows[$i]['md_score']="";
            $responce->rows[$i]['md_price']="0";
            $responce->rows[$i]['md_featured']="0";
            $responce->rows[$i]['md_has_discount']="0";
            $responce->rows[$i]['md_discount_type']="";
            $responce->rows[$i]['md_discount_amount']="0";
            $responce->rows[$i]['md_original_price']="0";
            $responce->rows[$i]['cnt']=0;
            $responce->rows[$i]['md_link']="";
            $responce->rows[$i]['md_type']="product_name";
            $responce->rows[$i]['md_next_view']="";
            $responce->rows[$i]['md_bool1']=0;
            $responce->rows[$i]['md_bool2']=0;
            $responce->rows[$i]['item_type']=0;
            $responce->rows[$i]['item_sku']="";
            $responce->rows[$i]['md_first_img']="";
            $responce->rows[$i]['md_photos']=0;
            $responce->rows[$i]['md_albums']=0;
            $responce->rows[$i]['currency']="";
            $responce->rows[$i]['variations_count']=0;

            $responce->rows[$i]['biz_level_img']["1"]="";
            $responce->rows[$i]['biz_level_img']["2"]="";
            $responce->rows[$i]['biz_level_img']["3"]="";
            $responce->rows[$i]['biz_level_img']["4"]="";
            $responce->rows[$i]['biz_level_img']["5"]="";
            $responce->rows[$i]['biz_level_img']["6"]="";
            $responce->rows[$i]['biz_level_img']["7"]="";
            $responce->rows[$i]['biz_level_img']["8"]="";
            $responce->rows[$i]['biz_level_img']["9"]="";
            $responce->rows[$i]['biz_level_img']["10"]="";
            $responce->rows[$i]['biz_level_img']["11"]="";
            
            $i++;
            
            //price
            $responce->rows[$i]['md_row_id']=0;
            $responce->rows[$i]['md_biz_id']=$bizid;
            $responce->rows[$i]['md_mod_id']=9;
            $responce->rows[$i]['md_level_no']=2;
            $responce->rows[$i]['md_parent']=$parent;
            $responce->rows[$i]['md_index']=0;
            $responce->rows[$i]['md_icon']="";			
            $responce->rows[$i]['md_head']="";
            $responce->rows[$i]['md_desc']="";
            $responce->rows[$i]['md_loc']="";
            $responce->rows[$i]['md_date']="";
            $responce->rows[$i]['md_info']="";
            $responce->rows[$i]['md_score']="";
            $responce->rows[$i]['md_price']=$productRow["md_price"];
            $responce->rows[$i]['md_featured']="0";
            $responce->rows[$i]['md_has_discount']="0";
            $responce->rows[$i]['md_discount_type']="";
            $responce->rows[$i]['md_discount_amount']="0";
            $responce->rows[$i]['md_original_price']=$productRow["md_original_price"];
            $responce->rows[$i]['cnt']=0;
            $responce->rows[$i]['md_link']="";
            $responce->rows[$i]['md_type']="product_price";
            $responce->rows[$i]['md_next_view']="";
            $responce->rows[$i]['md_bool1']=0;
            $responce->rows[$i]['md_bool2']=0;
            $responce->rows[$i]['item_type']=0;
            $responce->rows[$i]['item_sku']="";
            $responce->rows[$i]['md_first_img']="";
            $responce->rows[$i]['md_photos']=0;
            $responce->rows[$i]['md_albums']=0;
            $responce->rows[$i]['currency']=$storeRow["ess_currency"];
            $responce->rows[$i]['variations_count']=0;

            $responce->rows[$i]['biz_level_img']["1"]="";
            $responce->rows[$i]['biz_level_img']["2"]="";
            $responce->rows[$i]['biz_level_img']["3"]="";
            $responce->rows[$i]['biz_level_img']["4"]="";
            $responce->rows[$i]['biz_level_img']["5"]="";
            $responce->rows[$i]['biz_level_img']["6"]="";
            $responce->rows[$i]['biz_level_img']["7"]="";
            $responce->rows[$i]['biz_level_img']["8"]="";
            $responce->rows[$i]['biz_level_img']["9"]="";
            $responce->rows[$i]['biz_level_img']["10"]="";
            $responce->rows[$i]['biz_level_img']["11"]="";
            
            $price["price"] = $productRow["md_price"];
            $price["original_price"] = $productRow["md_original_price"];
            $price["currency"] = $storeRow["ess_currency"];
            $i++;
            
            //Slider
            if($productRow["md_pic1"] != "" || $productRow["md_pic2"] != "" || $productRow["md_pic3"] != "" || $productRow["md_pic4"] != "" || $productRow["md_pic5"] != "" || $productRow["md_pic6"] != "" || $productRow["md_pic7"] != "" || $productRow["md_pic8"] != "" || $productRow["md_pic9"] != "" || $productRow["md_pic10"] != ""){
                $responce->rows[$i]['md_row_id']=0;
                $responce->rows[$i]['md_biz_id']=$bizid;
                $responce->rows[$i]['md_mod_id']=9;
                $responce->rows[$i]['md_level_no']=2;
                $responce->rows[$i]['md_parent']=$parent;
                $responce->rows[$i]['md_index']=0;
                $responce->rows[$i]['md_icon']="";			
                $responce->rows[$i]['md_head']="";
                $responce->rows[$i]['md_desc']="";
                $responce->rows[$i]['md_loc']="";
                $responce->rows[$i]['md_date']="";
                $responce->rows[$i]['md_info']="";
                $responce->rows[$i]['md_score']="";
                $responce->rows[$i]['md_price']="0";
                $responce->rows[$i]['md_featured']="0";
                $responce->rows[$i]['md_has_discount']="0";
                $responce->rows[$i]['md_discount_type']="";
                $responce->rows[$i]['md_discount_amount']="0";
                $responce->rows[$i]['md_original_price']="0";
                $responce->rows[$i]['cnt']=0;
                $responce->rows[$i]['md_link']="";
                $responce->rows[$i]['md_type']="Slider";
                $responce->rows[$i]['md_next_view']="";
                $responce->rows[$i]['md_bool1']=0;
                $responce->rows[$i]['md_bool2']=0;
                $responce->rows[$i]['item_type']=0;
                $responce->rows[$i]['item_sku']="";
                $responce->rows[$i]['md_first_img']="";
                $responce->rows[$i]['md_photos']=0;
                $responce->rows[$i]['md_albums']=0;
                $responce->rows[$i]['currency']="";
                $responce->rows[$i]['variations_count']=0;

                $responce->rows[$i]['biz_level_img']["1"]=($productRow["md_pic"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic"]);
                $responce->rows[$i]['biz_level_img']["2"]=($productRow["md_pic1"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic1"]);
                $responce->rows[$i]['biz_level_img']["3"]=($productRow["md_pic2"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic2"]);
                $responce->rows[$i]['biz_level_img']["4"]=($productRow["md_pic3"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic3"]);
                $responce->rows[$i]['biz_level_img']["5"]=($productRow["md_pic4"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic4"]);
                $responce->rows[$i]['biz_level_img']["6"]=($productRow["md_pic5"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic5"]);
                $responce->rows[$i]['biz_level_img']["7"]=($productRow["md_pic6"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic6"]);
                $responce->rows[$i]['biz_level_img']["8"]=($productRow["md_pic7"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic7"]);
                $responce->rows[$i]['biz_level_img']["9"]=($productRow["md_pic8"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic8"]);
                $responce->rows[$i]['biz_level_img']["10"]=($productRow["md_pic9"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic9"]);
                $responce->rows[$i]['biz_level_img']["11"]=($productRow["md_pic10"] == null) ? "" : str_replace("paptap-thumbs","paptap",$productRow["md_pic10"]);
                
                $i++;
            }

            
            //SKU
            $responce->rows[$i]['md_row_id']=0;
            $responce->rows[$i]['md_biz_id']=$bizid;
            $responce->rows[$i]['md_mod_id']=9;
            $responce->rows[$i]['md_level_no']=2;
            $responce->rows[$i]['md_parent']=$parent;
            $responce->rows[$i]['md_index']=0;
            $responce->rows[$i]['md_icon']="";			
            $responce->rows[$i]['md_head']="";
            $responce->rows[$i]['md_desc']="";
            $responce->rows[$i]['md_loc']="";
            $responce->rows[$i]['md_date']="";
            $responce->rows[$i]['md_info']=stripslashes($productRow["md_info1"]);
            $responce->rows[$i]['md_score']="";
            $responce->rows[$i]['md_price']="0";
            $responce->rows[$i]['md_featured']="0";
            $responce->rows[$i]['md_has_discount']="0";
            $responce->rows[$i]['md_discount_type']="";
            $responce->rows[$i]['md_discount_amount']="0";
            $responce->rows[$i]['md_original_price']="0";
            $responce->rows[$i]['cnt']=0;
            $responce->rows[$i]['md_link']="";
            $responce->rows[$i]['md_type']="product_sku";
            $responce->rows[$i]['md_next_view']="";
            $responce->rows[$i]['md_bool1']=0;
            $responce->rows[$i]['md_bool2']=0;
            $responce->rows[$i]['item_type']=0;
            $responce->rows[$i]['item_sku']="";
            $responce->rows[$i]['md_first_img']="";
            $responce->rows[$i]['md_photos']=0;
            $responce->rows[$i]['md_albums']=0;
            $responce->rows[$i]['currency']="";
            $responce->rows[$i]['variations_count']=0;

            $responce->rows[$i]['biz_level_img']["1"]="";
            $responce->rows[$i]['biz_level_img']["2"]="";
            $responce->rows[$i]['biz_level_img']["3"]="";
            $responce->rows[$i]['biz_level_img']["4"]="";
            $responce->rows[$i]['biz_level_img']["5"]="";
            $responce->rows[$i]['biz_level_img']["6"]="";
            $responce->rows[$i]['biz_level_img']["7"]="";
            $responce->rows[$i]['biz_level_img']["8"]="";
            $responce->rows[$i]['biz_level_img']["9"]="";
            $responce->rows[$i]['biz_level_img']["10"]="";
            $responce->rows[$i]['biz_level_img']["11"]="";
  
            $i++;
        }
        
        if(count($levelsList) > 0){
            
            if($mod_id == "3" && $level_no==2){
                
                $parentSQL =  "select * from tbl_mod_data$mod_id where md_row_id=$parent";
                $parentRow = dbGetRow($parentSQL);
                
                //mainImage
                if($parentRow["md_icon"] != ""){
                    $responce->rows[$i]['md_row_id']=$parentRow["md_row_id"];
                    $responce->rows[$i]['md_biz_id']=$bizid;
                    $responce->rows[$i]['md_mod_id']=3;
                    $responce->rows[$i]['md_level_no']=2;
                    $responce->rows[$i]['md_parent']=$parent;
                    $responce->rows[$i]['md_index']=0;
                    $responce->rows[$i]['md_icon']="";			
                    $responce->rows[$i]['md_head']="";
                    $responce->rows[$i]['md_desc']="";
                    $responce->rows[$i]['md_loc']="";
                    $responce->rows[$i]['md_date']="";
                    $responce->rows[$i]['md_info']="";
                    $responce->rows[$i]['md_score']="";
                    $responce->rows[$i]['md_price']="0";
                    $responce->rows[$i]['md_featured']="0";
                    $responce->rows[$i]['md_has_discount']="0";
                    $responce->rows[$i]['md_discount_type']="";
                    $responce->rows[$i]['md_discount_amount']="0";
                    $responce->rows[$i]['md_original_price']="0";
                    $responce->rows[$i]['cnt']=0;
                    $responce->rows[$i]['md_link']="";
                    $responce->rows[$i]['md_type']="product_image";
                    $responce->rows[$i]['md_next_view']="";
                    $responce->rows[$i]['md_bool1']=0;
                    $responce->rows[$i]['md_bool2']=0;
                    $responce->rows[$i]['item_type']=0;
                    $responce->rows[$i]['item_sku']="";
                    $responce->rows[$i]['md_first_img']="";
                    $responce->rows[$i]['md_photos']=0;
                    $responce->rows[$i]['md_albums']=0;
                    $responce->rows[$i]['currency']="";
                    $responce->rows[$i]['variations_count']=0;

                    $responce->rows[$i]['biz_level_img']["1"]=$parentRow["md_icon"];
                    $responce->rows[$i]['biz_level_img']["2"]="";
                    $responce->rows[$i]['biz_level_img']["3"]="";
                    $responce->rows[$i]['biz_level_img']["4"]="";
                    $responce->rows[$i]['biz_level_img']["5"]="";
                    $responce->rows[$i]['biz_level_img']["6"]="";
                    $responce->rows[$i]['biz_level_img']["7"]="";
                    $responce->rows[$i]['biz_level_img']["8"]="";
                    $responce->rows[$i]['biz_level_img']["9"]="";
                    $responce->rows[$i]['biz_level_img']["10"]="";
                    $responce->rows[$i]['biz_level_img']["11"]="";
                    
                    $i++;
                }
                
                //large text
                $responce->rows[$i]['md_row_id']=$parentRow["md_row_id"];
                $responce->rows[$i]['md_biz_id']=$bizid;
                $responce->rows[$i]['md_mod_id']=3;
                $responce->rows[$i]['md_level_no']=2;
                $responce->rows[$i]['md_parent']=$parent;
                $responce->rows[$i]['md_index']=0;
                $responce->rows[$i]['md_icon']="";			
                $responce->rows[$i]['md_head']="";
                $responce->rows[$i]['md_desc']="";
                $responce->rows[$i]['md_loc']="";
                $responce->rows[$i]['md_date']="";
                $responce->rows[$i]['md_info']=stripslashes($parentRow["md_head"]);
                $responce->rows[$i]['md_score']="";
                $responce->rows[$i]['md_price']="0";
                $responce->rows[$i]['md_featured']="0";
                $responce->rows[$i]['md_has_discount']="0";
                $responce->rows[$i]['md_discount_type']="";
                $responce->rows[$i]['md_discount_amount']="0";
                $responce->rows[$i]['md_original_price']="0";
                $responce->rows[$i]['cnt']=0;
                $responce->rows[$i]['md_link']="";
                $responce->rows[$i]['md_type']="large_text";
                $responce->rows[$i]['md_next_view']="";
                $responce->rows[$i]['md_bool1']=0;
                $responce->rows[$i]['md_bool2']=0;
                $responce->rows[$i]['item_type']=0;
                $responce->rows[$i]['item_sku']="";
                $responce->rows[$i]['md_first_img']="";
                $responce->rows[$i]['md_photos']=0;
                $responce->rows[$i]['md_albums']=0;
                $responce->rows[$i]['currency']="";
                $responce->rows[$i]['variations_count']=0;

                $responce->rows[$i]['biz_level_img']["1"]="";
                $responce->rows[$i]['biz_level_img']["2"]="";
                $responce->rows[$i]['biz_level_img']["3"]="";
                $responce->rows[$i]['biz_level_img']["4"]="";
                $responce->rows[$i]['biz_level_img']["5"]="";
                $responce->rows[$i]['biz_level_img']["6"]="";
                $responce->rows[$i]['biz_level_img']["7"]="";
                $responce->rows[$i]['biz_level_img']["8"]="";
                $responce->rows[$i]['biz_level_img']["9"]="";
                $responce->rows[$i]['biz_level_img']["10"]="";
                $responce->rows[$i]['biz_level_img']["11"]="";
                
                $i++;
                
                
                
            }
            foreach ($levelsList as $oneElement) 
            {			
                $printElement = true;
                if(($oneElement["me_name"] == "color_attr" || $oneElement["me_name"] == "list_attr") && count($variants) == 0 ) $printElement=false;
                
                if($printElement){
                    if($mod_id == "9"){
                        $md_parent = $parent;
                        $variationsCount = dbGetVal("SELECT count(id) FROM ecommerce_variations WHERE attributes_values_ids <> '' and product_id = {$oneElement["md_row_id"]}");
                    }else{
                        $md_parent = $oneElement["md_parent"];
                        
                        if($oneElement["md_element"] == 8 && startsWith($oneElement["md_info"],"audio:")){
                            $oneElement["md_info"] = str_replace("audio:","",$oneElement["md_info"]);
                            $oneElement["md_bool2"] = 1;
                        }
                        
                        if($oneElement["md_element"] == 7){

                            $video_thumb = "https://storage.googleapis.com/paptap/templates/BoutiqueBouquets/android/video_placeholder.png";

                            if(strContains($oneElement["md_info"],"youtu")){
                                $videoID = getYouTubeID($oneElement["md_info"]);
                                
                                if($videoID != ''){
                                    $oneElement["md_pic"] = "https://img.youtube.com/vi/$videoID/hqdefault.jpg";
                                }
                            }

                            if(strContains($oneElement["md_info"],"vimeo")){
                                $vObject = getVimeoObject($oneElement["md_info"]);
                                
                                if($vObject->thumbnail_url != ''){
                                    $oneElement["md_pic"] = stripslashes($vObject->thumbnail_url);
                                }
                            }
                        }
                        
                        if($oneElement["md_element"] == 30){
                            $state = ($oneElement["md_info1"] != "") ? ", ".$oneElement["md_info1"] : "";
                            $oneElement["md_info"] = $oneElement["md_info"].$state.", ".$oneElement["md_info2"].", ".$oneElement["md_info3"]." ".$oneElement["md_info4"].", ".$oneElement["md_info5"];
                        }
                        
                        $item_ship = $oneElement["md_bool2"];
                    }
                    
                    $odotReplace = $oneElement["md_info"];
                    $cnt = ($oneElement["cnt"] == null) ? "0" : $oneElement["cnt"];
                    
                    $responce->rows[$i]['md_row_id']=$oneElement["md_row_id"];
                    $responce->rows[$i]['md_biz_id']=$oneElement["md_biz_id"];
                    $responce->rows[$i]['md_mod_id']=$oneElement["md_mod_id"];
                    $responce->rows[$i]['md_level_no']=$oneElement["md_level_no"];
                    $responce->rows[$i]['md_parent']=$md_parent;
                    $responce->rows[$i]['md_index']=$oneElement["md_index"];
                    
                    if($mod_id == "4" && $oneElement["md_content"] == "1"){
                        $oneElement["md_icon"] = ($oneElement["md_first_img"] == "") ? getGalleryImage($mod_id,$oneElement["md_row_id"]) : $oneElement["md_first_img"];
                    }

                    if($mod_id == "24" && $oneElement["md_icon"] == "" && $oneElement["md_element"] == 10 && $oneElement["md_content"] == "1"){
                        $oneElement["md_icon"] = getVideoImage($mod_id,$oneElement["md_row_id"]);
                    }
                    
                    $responce->rows[$i]['md_icon']=($oneElement["md_icon"] == null) ? "" : $oneElement["md_icon"];			
                    $responce->rows[$i]['md_head']=($oneElement["md_head"] == null) ? "" : $oneElement["md_head"];
                    $responce->rows[$i]['md_desc']=($oneElement["md_desc"] == null) ? "" : $oneElement["md_desc"];
                    $responce->rows[$i]['md_loc']=($oneElement["md_loc"] == null) ? "" : $oneElement["md_loc"];
                    $responce->rows[$i]['md_date']=($oneElement["md_date"] == null) ? "" : $oneElement["md_date"];
                    $responce->rows[$i]['md_info']=($odotReplace == null) ? "" : $odotReplace;
                    $responce->rows[$i]['md_score']=($oneElement["md_score"] == null) ? "" : $oneElement["md_score"];
                    $responce->rows[$i]['md_price']=($oneElement["md_price"] == null) ? "0" : $oneElement["md_price"];
                    $responce->rows[$i]['md_featured']=($oneElement["md_featured"] == null) ? "0" : $oneElement["md_featured"];
                    $responce->rows[$i]['md_has_discount']=($oneElement["md_has_discount"] == null) ? "0" : $oneElement["md_has_discount"];
                    $responce->rows[$i]['md_discount_type']=($oneElement["md_discount_type"] == null) ? "" : $oneElement["md_discount_type"];
                    $responce->rows[$i]['md_discount_amount']=($oneElement["md_discount_amount"] == null) ? "0" : $oneElement["md_discount_amount"];
                    $responce->rows[$i]['md_original_price']=($oneElement["md_original_price"] == null) ? "0" : $oneElement["md_original_price"];
                    $responce->rows[$i]['cnt']=$cnt;
                    $responce->rows[$i]['md_link']=($oneElement["link"] == null) ? "" : $oneElement["link"];
                    $responce->rows[$i]['md_type']=($oneElement["me_name"] == null) ? "" : $oneElement["me_name"];
                    $responce->rows[$i]['md_next_view']=($oneElement["md_content"] == "0") ? $oneElement["ms_view_type$nib_name"] : $oneElement["ms_view_end$nib_name"];
                    $responce->rows[$i]['md_content']=$oneElement["md_content"];
                    $responce->rows[$i]['md_bool1']=$oneElement["md_bool1"];
                    $responce->rows[$i]['md_bool2']=$item_ship;
                    $responce->rows[$i]['item_type']=$item_type;
                    $responce->rows[$i]['item_sku']=$oneElement["md_info1"];

                    $responce->rows[$i]['md_first_img']=($oneElement["md_first_img"] == "") ? "" : $oneElement["md_first_img"];
                    
                    if(($mod_id == "4" || $mod_id == "24") && $oneElement["md_element"] == 10){
                        $oneElement["md_photos"] = getImagesCount($mod_id,$oneElement["md_row_id"]);
                    }
                    $responce->rows[$i]['md_photos']=($oneElement["md_photos"] == 0) ? "0" : $oneElement["md_photos"];
                    
                    if(($mod_id == "4" || $mod_id == "24") && $oneElement["md_element"] == 10){
                        $oneElement["md_albums"] = getAlbumCount($mod_id,$oneElement["md_row_id"]);
                    }
                    $responce->rows[$i]['md_albums']=($oneElement["md_albums"] == 0) ? "0" : $oneElement["md_albums"];
                    
                    $responce->rows[$i]['currency']=$storeRow["ess_currency"];
                    $responce->rows[$i]['variations_count']=($variationsCount == null) ? "0" : $variationsCount;

                    $responce->rows[$i]['biz_level_img']["1"]=($oneElement["md_pic"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic"]);
                    $responce->rows[$i]['biz_level_img']["2"]=($oneElement["md_pic1"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic1"]);
                    $responce->rows[$i]['biz_level_img']["3"]=($oneElement["md_pic2"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic2"]);
                    $responce->rows[$i]['biz_level_img']["4"]=($oneElement["md_pic3"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic3"]);
                    $responce->rows[$i]['biz_level_img']["5"]=($oneElement["md_pic4"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic4"]);
                    $responce->rows[$i]['biz_level_img']["6"]=($oneElement["md_pic5"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic5"]);
                    $responce->rows[$i]['biz_level_img']["7"]=($oneElement["md_pic6"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic6"]);
                    $responce->rows[$i]['biz_level_img']["8"]=($oneElement["md_pic7"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic7"]);
                    $responce->rows[$i]['biz_level_img']["9"]=($oneElement["md_pic8"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic8"]);
                    $responce->rows[$i]['biz_level_img']["10"]=($oneElement["md_pic9"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic9"]);
                    $responce->rows[$i]['biz_level_img']["11"]=($oneElement["md_pic10"] == null) ? "" : str_replace("paptap-thumbs","paptap",$oneElement["md_pic10"]);
                    
                    if($oneElement["me_name"] == "color_attr" || $oneElement["me_name"] == "list_attr"){
                        
                        $atributeValues = $ecommerceManager->getAttributeValues($oneElement["md_row_id"],$attrIndex);
                        
                        if(count($atributeValues) > 0){
                            $responce->rows[$i]['attributeValues'] = $atributeValues; 
                            $attrIndex++;
                        }              
                    }
                    
                    
                    $i++;
                }
            }
        }
        
        if($mod_id == "9" && $workingOnOneProduct){
        //description
        $responce->rows[$i]['md_row_id']=0;
        $responce->rows[$i]['md_biz_id']=$bizid;
        $responce->rows[$i]['md_mod_id']=9;
        $responce->rows[$i]['md_level_no']=2;
        $responce->rows[$i]['md_parent']=$parent;
        $responce->rows[$i]['md_index']=0;
        $responce->rows[$i]['md_icon']="";			
        $responce->rows[$i]['md_head']="";
        $responce->rows[$i]['md_desc']="";
        $responce->rows[$i]['md_loc']="";
        $responce->rows[$i]['md_date']="";
        $responce->rows[$i]['md_info']=stripslashes($productRow["md_desc"]);
        $responce->rows[$i]['md_score']="";
        $responce->rows[$i]['md_price']="0";
        $responce->rows[$i]['md_featured']="0";
        $responce->rows[$i]['md_has_discount']="0";
        $responce->rows[$i]['md_discount_type']="";
        $responce->rows[$i]['md_discount_amount']="0";
        $responce->rows[$i]['md_original_price']="0";
        $responce->rows[$i]['cnt']=0;
        $responce->rows[$i]['md_link']="";
        $responce->rows[$i]['md_type']="product_description";
        $responce->rows[$i]['md_next_view']="";
        $responce->rows[$i]['md_bool1']=0;
        $responce->rows[$i]['md_bool2']=0;
        $responce->rows[$i]['item_type']=0;
        $responce->rows[$i]['item_sku']="";
        $responce->rows[$i]['md_first_img']="";
        $responce->rows[$i]['md_photos']=0;
        $responce->rows[$i]['md_albums']=0;
        $responce->rows[$i]['currency']="";
        $responce->rows[$i]['variations_count']=0;

        $responce->rows[$i]['biz_level_img']["1"]="";
        $responce->rows[$i]['biz_level_img']["2"]="";
        $responce->rows[$i]['biz_level_img']["3"]="";
        $responce->rows[$i]['biz_level_img']["4"]="";
        $responce->rows[$i]['biz_level_img']["5"]="";
        $responce->rows[$i]['biz_level_img']["6"]="";
        $responce->rows[$i]['biz_level_img']["7"]="";
        $responce->rows[$i]['biz_level_img']["8"]="";
        $responce->rows[$i]['biz_level_img']["9"]="";
        $responce->rows[$i]['biz_level_img']["10"]="";
        $responce->rows[$i]['biz_level_img']["11"]="";
        
        $i++;
        
        //add purchase button        
            $responce->rows[$i]['md_row_id']=0;
			$responce->rows[$i]['md_biz_id']=$bizid;
			$responce->rows[$i]['md_mod_id']=9;
			$responce->rows[$i]['md_level_no']=2;
			$responce->rows[$i]['md_parent']=$parent;
			$responce->rows[$i]['md_index']=99;
			$responce->rows[$i]['md_icon']="";			
			$responce->rows[$i]['md_head']="Buy Now";
			$responce->rows[$i]['md_desc']="";
			$responce->rows[$i]['md_loc']="";
			$responce->rows[$i]['md_date']="";
			$responce->rows[$i]['md_info']="";
			$responce->rows[$i]['md_score']="";
			$responce->rows[$i]['md_price']="0";
            $responce->rows[$i]['md_featured']="0";
            $responce->rows[$i]['md_has_discount']="0";
            $responce->rows[$i]['md_discount_type']="";
            $responce->rows[$i]['md_discount_amount']="0";
            $responce->rows[$i]['md_original_price']="0";
			$responce->rows[$i]['cnt']=0;
            $responce->rows[$i]['md_link']="";
			$responce->rows[$i]['md_type']="shop_button";
            $responce->rows[$i]['md_next_view']="";
            $responce->rows[$i]['md_bool1']=0;
            $responce->rows[$i]['md_bool2']=0;
            $responce->rows[$i]['item_type']=0;
            $responce->rows[$i]['item_sku']="";
            $responce->rows[$i]['md_first_img']="";
            $responce->rows[$i]['md_photos']=0;
            $responce->rows[$i]['md_albums']=0;
            $responce->rows[$i]['currency']=$storeRow["ess_currency"];
            $responce->rows[$i]['variations_count']=0;

			$responce->rows[$i]['biz_level_img']["1"]="";
			$responce->rows[$i]['biz_level_img']["2"]="";
			$responce->rows[$i]['biz_level_img']["3"]="";
			$responce->rows[$i]['biz_level_img']["4"]="";
			$responce->rows[$i]['biz_level_img']["5"]="";
			$responce->rows[$i]['biz_level_img']["6"]="";
            $responce->rows[$i]['biz_level_img']["7"]="";
			$responce->rows[$i]['biz_level_img']["8"]="";
			$responce->rows[$i]['biz_level_img']["9"]="";
			$responce->rows[$i]['biz_level_img']["10"]="";
			$responce->rows[$i]['biz_level_img']["11"]="";
        }
        
        $responce->variants = $variants;
		$responce->price = $price;
        
		$json = json_encode($responce);
		echo $json;
	}
	else
	{
		echo "0";
	}
	
    function getGalleryImage($modID,$rowID){        
        return dbGetVal("SELECT md_icon FROM tbl_mod_data$modID WHERE md_parent = $rowID and md_element <> 10 order by md_row_id limit 1");
    }

    function getVideoImage($modID,$rowID){        
        return dbGetVal("SELECT md_pic FROM tbl_mod_data$modID WHERE md_parent = $rowID and md_element <> 10 order by md_row_id limit 1");
    }
    
    function getImagesCount($modID,$rowID){ 
        if($modID == 4){
            return dbGetVal("SELECT count(md_row_id) FROM tbl_mod_data$modID WHERE md_parent = $rowID and md_element <> 10");
        }
        if($modID == 24){
            return dbGetVal("SELECT count(md_row_id) FROM tbl_mod_data$modID WHERE md_parent = $rowID and md_element = 10 and md_content = 1 and md_visible_element = 1");
        }

        return 0;
    }
    function getAlbumCount($modID,$rowID){        
        if($modID == 4){
            return dbGetVal("SELECT count(md_row_id) FROM tbl_mod_data$modID WHERE md_parent = $rowID and md_element = 10");
        }
        if($modID == 24){
            return dbGetVal("SELECT count(md_row_id) FROM tbl_mod_data$modID WHERE md_parent = $rowID and md_element = 10 and md_content = 0 and md_visible_element = 1");
        }

        return 0;
    }
?>