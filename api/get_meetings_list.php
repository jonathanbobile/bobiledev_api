<?php
require 'connect.php';
require 'functions.php';

date_default_timezone_set("UTC");



$android = $_GET["android"];
$deviceID = $_GET["deviceID"];
$iphone = $_GET["iphone"];
$lang = $_GET["lang"];
$deviceType = $_GET["deviceType"];
$deviceModel = $_GET["deviceModel"];
$deviceOrient = $_GET["deviceOrient"];
$OS = $_GET["OS"];
$OSVersion = $_GET["OSVersion"];
$papTapVersion = $_GET["papTapVersion"];
$long = $_GET["long"];
$lati = $_GET["lati"];
$phoneNumber = $_GET["phoneNumber"];

$bizid = $_GET["bizid"];
$emp_id = $_GET["emp_id"];
$meet_type_id = $_GET["meet_type"];
$start_date = $_GET["date_from"];
$offset = $_GET["offset"];
if($offset == "") $offset = 0;

if($emp_id == '') $emp_id = "0";
if($meet_type_id == '') $meet_type_id = "0";

if ($start_date == ''){ 
    $start_date = mktime(0, 0, 0);
    $nowTimeStamp = mktime() + 60*60 + $offset;
    
}else{


    $qure_date = mktime(0, 0, 0);
    
    $day = date('d',$start_date);
    $month = date('m',$start_date);
    $year = date('Y',$start_date);
    $start_date = mktime(0, 0, 0, $month, $day, $year);
    
    if($start_date <= $qure_date) {
        $start_date = $qure_date;
        $nowTimeStamp = mktime() + 60*60 + $offset;
    }else{

        $nowTimeStamp = mktime(0, 0, 0);
    }
}

$state = $_GET["state"];
$country = $_GET["country"];	
$category = $_GET["category"];
$subcategory = $_GET["subcategory"];
$mod_id = $_GET["mod_id"];
$level_no = $_GET["level_no"];
$parent = $_GET["parent"];
$in_favorites = $_GET["in_favorites"];
$isadmin = $_GET["isadmin"];

if($android == 'null' || $android == '') $android = "0";
if($deviceID == 'null') $deviceID = "";
if($iphone == 'null') $iphone = "0";
if($lang == 'null') $lang = "eng";
if($deviceType == 'null') $deviceType = "";
if($deviceModel == 'null') $deviceModel = "";
if($deviceOrient == 'null') $deviceOrient = "";
if($OS == 'null') $OS = "Android";
if($OSVersion == 'null') $OSVersion = "";
if($papTapVersion == 'null') $papTapVersion = "";
if($long == 'null') $long = ""; 
if($lati == 'null') $lati = ""; 
if($phoneNumber == 'null') $phoneNumber = "";    

if($bizid == '' || $bizid == 'null') $bizid = "0"; 
if($state == '' || $state == 'null') $state = "0"; 
if($country == '' || $country == 'null') $country = "0"; 
if($category == '' || $category == 'null') $category = "0";
if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
if($level_no == '' || $level_no == 'null') $level_no = "0";
if($parent == '' || $parent == 'null') $parent = "0";
if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";

$maxRecords = 30;
$responce = new stdClass();
$j=0;
$currentRecordNo = 1;


$nowHour = date('H',$nowTimeStamp);
$nowHour .= ":00";

//send_mail("info@paptap.com","crazyio2005@gmail.com","PapTap API debug","SELECT hour_id FROM calendar_hours WHERE hour_start = '$nowHour'","Dany","Dany");
$startHourId = dbGetVal("SELECT hour_id FROM calendar_hours WHERE hour_start = '$nowHour'");


// end global parameters


if($bizid != '' & $bizid != "0")
{    
    if ($emp_id == "0" && $meet_type_id != "0"){

        getMeetingListOnlyMeetingType($bizid,$meet_type_id,$start_date,$startHourId);

    }else if ($emp_id != "0" && $meet_type_id == "0"){

        getMeetingListOnlyEmplyee($bizid,$emp_id,$start_date,$startHourId);

    }else if ($emp_id != "0" && $meet_type_id != "0"){

        getMeetingListBoth($bizid,$emp_id, $meet_type_id,$start_date,$startHourId);

    }else{

        echo "0";
        //$responce->rows[0] = "0";
        //echo json_encode($responce);
        exit;
    }

    

    if($responce && count($responce)>0)
    {
        $json = json_encode($responce);
        echo $json;
    } 
    else
    {
        //$responce->rows[0] = "0";
        //echo json_encode($responce);
        echo "0";
    }
    
}
else
{
    echo "0";
}

function getMeetingListOnlyMeetingType($bizid,$meet_type_id, $start_date,$starting_hour){
    
    global $maxRecords, $responce,$j,$currentRecordNo;
    
    if ($currentRecordNo >= $maxRecords){
        return;
    }

    $meeting = dbGetRow("SELECT * FROM tbl_biz_cal_meet_type WHERE bmt_id = $meet_type_id");
    $meetSize = $meeting["bmt_size"];
    $meetName = $meeting["bmt_name"];
    $meetDur = $meeting["bmt_duration"];
    $meetPrice = $meeting["bmt_price"];
    
    $employees = dbGetTable("SELECT be_id, be_name,be_pic FROM tbl_employee,tbl_employee_meet_type
                                WHERE be_biz_id = $bizid
                                AND emt_bmt_id = $meet_type_id
                                AND emt_emp_id = be_id
                                AND be_active = 1
                                AND emt_active = 1
                                ORDER BY be_id ASC");

    if (count($employees) > 0){
        
        while ($currentRecordNo <=  $maxRecords){

            
            $end_date = $start_date + 5*$meetSize*60;
            
            foreach ($employees as $oneEmployee){

                $employeeID = $oneEmployee["be_id"];
                $employeeName = $oneEmployee["be_name"];
                $employeePic = ($oneEmployee["be_pic"] == "") ? "https://storage.googleapis.com/paptap/system/no_employee_image.png" : str_replace("paptap","paptap-thumbs",$oneEmployee["be_pic"]) ;

                $startDateDayOfWeek = date('w', $start_date) - 1;
                if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
                $startDate = date( "Y-m-d",$start_date);
                
                $endDate = date( "Y-m-d",$end_date);
                
                $allSlots = getAllSlotsFromSpecificHour($bizid,$startDateDayOfWeek,$meetSize,$starting_hour,$employeeID);
                
                if (count($allSlots) > 0){

                    $takenSlots = getAllTakenBetween($employeeID,$startDate,$endDate);

                    foreach ($allSlots as $oneSlot) {

                        $slotStartHour  = $oneSlot["hour_id"];
                        
                        $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                        $slotEndTime = $slotStartTime + 5*60*$meetSize;
                        
                        $isTaken = 0;
                        $takenEndHourId = 0;
                        if (count($takenSlots) > 0 ){

                            foreach ($takenSlots as $taken) {
                                $takenStartTime = strtotime($taken["em_start"]);
                                $takenEndTime =  strtotime($taken["em_end"]);
                                $takenEndHourId = $taken["em_end_time"];
                                
                                if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                    //It is not overlaping
                                    $isTaken = 0;
                                }else{
                                    $isTaken = 1;
                                    break;
                                }
                            }
                        }
                        
                        if ($currentRecordNo == $maxRecords){
                            return;
                        }else{

                        }
                        
                        if ($isTaken == 0){
                            $responce->rows[$j]["meet_emp_id"] = $employeeID;
                            $responce->rows[$j]["meet_emp_name"] = $employeeName;
                            $responce->rows[$j]["meet_emp_pic"] = $employeePic;
                            $responce->rows[$j]["meet_type_id"] = $meet_type_id;
                            $responce->rows[$j]["meet_type_name"] = $meetName;
                            $responce->rows[$j]["meet_price"] = $meetPrice;
                            $responce->rows[$j]["meet_start_timestamp"] = $slotStartTime;
                            $responce->rows[$j]["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                            $responce->rows[$j]["meet_end_timestamp"] = $slotEndTime;
                            $responce->rows[$j]["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);
                            $j++;
                            $currentRecordNo++;
                        }else{
                            $day = date('d',$takenEndTime);
                            $month = date('m',$takenEndTime);
                            $year = date('Y',$takenEndTime);
                            $goTo = mktime(0, 0, 0, $month, $day, $year);
                            
                            getMeetingListBoth($bizid,$employeeID,$meet_type_id ,$goTo,($takenEndHourId+1));
                            
                        }

                        if ($currentRecordNo == $maxRecords){
                            return;
                        }else{

                        }


                    }


                }
                else{
                    $currentRecordNo++;
                }
            }
            $starting_hour = 0;
            $start_date = $start_date + 24*60*60;
        }
    } else{
        $responce = "0";
    }

    return;
}

function getMeetingListOnlyEmplyee($bizid,$employee_id, $start_date,$starting_hour){

    
    global $maxRecords, $responce,$j,$currentRecordNo;
    
    if ($currentRecordNo >= $maxRecords){
        return;
    }

    $employee = dbGetRow("SELECT * FROM tbl_employee WHERE be_id = $employee_id");
    $employeeID = $employee["be_id"];
    $employeeName = $employee["be_name"];
    $employeePic = ($employee["be_pic"] == "") ? "https://storage.googleapis.com/paptap/system/no_employee_image.png" : str_replace("paptap","paptap-thumbs",$employee["be_pic"]) ;

    $meetingTyps = dbGetTable("SELECT bmt_id, bmt_name, bmt_size,bmt_duration,bmt_price FROM tbl_biz_cal_meet_type,tbl_employee_meet_type
                                WHERE bmt_biz_id = $bizid
                                AND emt_bmt_id = bmt_id
                                AND emt_emp_id = $employeeID
                                AND bmt_active = 1
                                AND bmt_mobile_active = 1
                                AND emt_active = 1
                                ORDER BY bmt_id ASC");
    

    if (count($meetingTyps) > 0){
        
        while ($currentRecordNo <=  $maxRecords){
            
            foreach ($meetingTyps as $oneType){

                $meet_type_id = $oneType["bmt_id"];
                $meetSize = $oneType["bmt_size"];
                $meetName = $oneType["bmt_name"];
                $meetDur = $oneType["bmt_duration"];
                $meetPrice = $oneType["bmt_price"];

                $startDateDayOfWeek = date('w', $start_date) - 1;
                if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
                
                $startDate = date( "Y-m-d",$start_date);
                
                $end_date = $start_date + 5*$meetSize*60;
                $endDate = date( "Y-m-d",$end_date);
                
                

                $allSlots = getAllSlotsFromSpecificHour($bizid,$startDateDayOfWeek,$meetSize,$starting_hour,$employeeID);
                
                
                if (count($allSlots) > 0){
                    
                    $takenSlots = getAllTakenBetween($employeeID,$startDate,$endDate);

                    
                    
                    foreach ($allSlots as $oneSlot) {

                        $slotStartHour  = $oneSlot["hour_id"];
                        
                        $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                        $slotEndTime = $slotStartTime + 5*60*$meetSize;
                        
                        
                        $isTaken = 0;
                        $takenEndHourId = 0;
                        if (count($takenSlots) > 0 ){

                            foreach ($takenSlots as $taken) {
                                $takenStartTime = strtotime($taken["em_start"]);
                                $takenEndTime =  strtotime($taken["em_end"]);
                                $takenEndHourId = $taken["em_end_time"];
                                
                                if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                    //It is not overlaping
                                    $isTaken = 0;
                                }else{
                                    $isTaken = 1;
                                    break;
                                }
                            }
                        }
                        
                        if ($currentRecordNo == $maxRecords){
                            return;
                        }else{

                        }
                        
                        if ($isTaken == 0){
                            $responce->rows[$j]["meet_emp_id"] = $employeeID;
                            $responce->rows[$j]["meet_emp_name"] = $employeeName;
                            $responce->rows[$j]["meet_emp_pic"] = $employeePic;
                            $responce->rows[$j]["meet_type_id"] = $meet_type_id;
                            $responce->rows[$j]["meet_type_name"] = $meetName;
                            $responce->rows[$j]["meet_price"] = $meetPrice;
                            $responce->rows[$j]["meet_start_timestamp"] = $slotStartTime;
                            $responce->rows[$j]["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                            $responce->rows[$j]["meet_end_timestamp"] = $slotEndTime;
                            $responce->rows[$j]["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);
                            $j++;
                            $currentRecordNo++;
                        }else{
                            $day = date('d',$takenEndTime);
                            $month = date('m',$takenEndTime);
                            $year = date('Y',$takenEndTime);
                            $goTo = mktime(0, 0, 0, $month, $day, $year);
                            
                            getMeetingListBoth($bizid,$employeeID,$meet_type_id ,$goTo,($takenEndHourId+1));
                            $takenEndHourId = "bch_begin_hour";
                        }

                        if ($currentRecordNo == $maxRecords){
                            return;
                        }else{

                        }


                    }

                }
                else{
                    
                    $currentRecordNo++;
                }
            }
            $starting_hour = 0;
            $start_date = $start_date + 24*60*60;
            
        }
    }  else{
        $responce = "0";
    } 

    return;
}

function getMeetingListBoth($bizid,$employee_id,$meet_type_id,$start_date,$starting_hour){
    global $maxRecords, $responce,$j,$currentRecordNo;
    
    if ($currentRecordNo >= $maxRecords){
        return;
    }
    
    $meeting = dbGetRow("SELECT * FROM tbl_biz_cal_meet_type WHERE bmt_id = $meet_type_id");
    $meetSize = $meeting["bmt_size"];
    $meetName = $meeting["bmt_name"];
    $meetDur = $meeting["bmt_duration"];
    $meetPrice = $meeting["bmt_price"];

    $employee = dbGetRow("SELECT * FROM tbl_employee WHERE be_id = $employee_id");
    $employeeID = $employee["be_id"];
    $employeeName = $employee["be_name"];
    $employeePic = ($employee["be_pic"] == "") ? "https://storage.googleapis.com/paptap/system/no_employee_image.png" : str_replace("paptap","paptap-thumbs",$employee["be_pic"]);
    $numLoops = 0;
    while ($currentRecordNo <=  $maxRecords && $numLoops < 100){
        
        $startDateDayOfWeek = date('w', $start_date) - 1;
        if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
        
        $startDate = date( "Y-m-d",$start_date);
        
        $end_date = $start_date + 5*$meetSize*60;
        $endDate = date( "Y-m-d",$end_date);

        
        $allSlots = getAllSlotsFromSpecificHour($bizid,$startDateDayOfWeek,$meetSize,$starting_hour,$employeeID);
        
        if (count($allSlots) > 0){
            $takenSlots = getAllTakenBetween($employeeID,$startDate,$endDate); 
            
            foreach ($allSlots as $oneSlot) {

                $slotStartHour  = $oneSlot["hour_id"];
                
                $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                $slotEndTime = $slotStartTime + 5*60*$meetSize;
                
                
                $isTaken = 0;
                $takenEndHourId = 0;
                if (count($takenSlots) > 0 ){


                    foreach ($takenSlots as $taken) {
                        $takenStartTime = strtotime($taken["em_start"]);
                        $takenEndTime =  strtotime($taken["em_end"]);
                        $takenEndHourId = $taken["em_end_time"];
                        
                        if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                            //It is not overlaping
                            $isTaken = 0;
                        }else{
                            $isTaken = 1;
                            break;
                        }
                    }
                }
                
                if ($currentRecordNo == $maxRecords){
                    return;
                }
                
                if ($isTaken == 0){
                    $responce->rows[$j]["meet_emp_id"] = $employeeID;
                    $responce->rows[$j]["meet_emp_name"] = $employeeName;
                    $responce->rows[$j]["meet_emp_pic"] = $employeePic;
                    $responce->rows[$j]["meet_type_id"] = $meet_type_id;
                    $responce->rows[$j]["meet_type_name"] = $meetName;
                    $responce->rows[$j]["meet_price"] = $meetPrice;
                    $responce->rows[$j]["meet_start_timestamp"] = $slotStartTime;
                    $responce->rows[$j]["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                    $responce->rows[$j]["meet_end_timestamp"] = $slotEndTime;
                    $responce->rows[$j]["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);
                    $j++;
                    $currentRecordNo++;
                }else{
                    $day = date('d',$takenEndTime);
                    $month = date('m',$takenEndTime);
                    $year = date('Y',$takenEndTime);
                    $goTo = mktime(0, 0, 0, $month, $day, $year);
                    
                    getMeetingListBoth($bizid,$employeeID,$meet_type_id ,$goTo,($takenEndHourId + 1));
                    
                }

                if ($currentRecordNo == $maxRecords){
                    return;
                }

            }

        }
        else{
            $numLoops = $numLoops + 1;
            //$currentRecordNo++;
        }
        $starting_hour = 0;
        $start_date = $start_date + 24*60*60;

    } 

    return;
}

function getAllSlotsFromSpecificHour($bizid,$day_no,$size,$from,$emp){

    if ($size > 144){
        $size = 36;
    }
    

    $sql = "SELECT biz_cal_biz_hours_only FROM tbl_biz_cal_settings WHERE bcs_biz_id = $bizid";

    $onlyBizHours = dbGetVal($sql);
    //tbl_employee_hours
    
    if($onlyBizHours == 1){
        $retVal = dbGetTable("SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_biz_cal_hours,tbl_employee_hours
                                    WHERE bch_biz_id = $bizid
                                    AND bch_emp_id = $emp
                                    AND tbl_employee_hours.bch_day_no = $day_no
                                    AND tbl_biz_cal_hours.bch_day_no = $day_no
                                    AND tbl_biz_cal_hours.bch_working_day = 1
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id between GREATEST($from,tbl_biz_cal_hours.bch_begin_hour) and tbl_biz_cal_hours.bch_finish_hour
                                    AND hour_id between GREATEST($from,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND (hour_id + $size) between GREATEST($from,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND MOD(hour_id,6) = 1 /*MOD(GREATEST($from,tbl_employee_hours.bch_begin_hour),$size)*/
                                    ORDER BY hour_id");
    }
    else{
        $retVal = dbGetTable("SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_employee_hours
                                    WHERE bch_emp_id = $emp  
                                    AND tbl_employee_hours.bch_day_no = $day_no
                                    AND tbl_employee_hours.bch_working_day = 1                                    
                                    AND hour_id between GREATEST($from,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND (hour_id + $size) between GREATEST($from,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND MOD(hour_id,6) = 1 /*MOD(GREATEST($from,tbl_employee_hours.bch_begin_hour),$size)*/
                                    ORDER BY hour_id");
    }
    
    return $retVal;
}

function getAllTakenBetween($emp,$start,$end){
    
    
    $retVal = dbGetTable("SELECT * FROM tbl_employee_meeting
                                WHERE em_emp_id = $emp
                                AND (
                                    (
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$start','%Y-%m-%d')
                                    AND
                                    DATE_FORMAT(em_end_date , '%Y-%m-%d') >= STR_TO_DATE('$start','%Y-%m-%d')
                                   )
                                   OR 
                                   (
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') >= STR_TO_DATE('$start','%Y-%m-%d')
                                    AND
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$end','%Y-%m-%d')
                                    )
                                )
                                ");
    
    
    return $retVal;
}


?>
