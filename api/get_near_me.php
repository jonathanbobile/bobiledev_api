<?php
	require 'connect.php';
	require 'functions.php';
	
	foreach ($_GET as $key => $value)
	{
		$par = $par."$key has a value of $value <br />";
	}
	$repTEXT = "get_biz_list<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
	// start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
    $lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
    $deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
    $bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];

    $distance = $_GET["distance"];
    $unit = $_GET["unit"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = 0; 
    if($lati == 'null') $lati = 0; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    if($distance == '' || $distance == 'null') $distance = "5";
    if($unit == '' || $unit == 'null') $unit = "km";
    
    // end global parameters
	
    $nib_name = "";
    if($android == "1"){
            $nib_name = "_android";
    }
    
    if($lang == ""){
            $lang = "eng";
    }

    if($state == ''){
            $state = "0";
    }

    if($country == ''){
            $country = "0";
    }
    
    $factor=1;
    switch ($unit) {
    case "mile":
        $factor = 1.6;
            break;
    }
    
    $distance = $factor*$distance;

 
    addAPI("api_get_near_me",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);
    
    $marketId = isset($_GET["marketId"]) ? $_GET["marketId"] : "0";
    $lang = isset($_GET["displayLang"]) ? $_GET["displayLang"] : $lang;
    if($lang == "iw") $lang = "he";
    
    $categorySql = "";
    if ($category > 0){
        $categorySql = " and biz_category_id=$category ";
    }
    
    $categoriesSQL="select cat_stamp,cat_background,cat_icon,cat_id,cat_name_$lang,IFNULL(cbiz,0) biz_count 
				from tbl_categories as cat LEFT JOIN (select count(biz_id) cbiz,biz_category_id from tbl_biz 
				where 1=1
				$allCountry 
				and biz_status=1 group by biz_category_id ) as bizcount ON cat.cat_id=bizcount.biz_category_id 
				order by cat.cat_name_$lang";


    $nearBizSQL="select biz_id, 
                       biz_short_name, 
                       biz_icon, 
                       biz_category_id cat_id,
                       cat_name_$lang cat_name,
                       mrktbiz_user_rank rating,
                       bm_biz_long longti,
                       bm_biz_lati lati,
                       biz_addr_street,biz_addr_no,biz_addr_town,
                       biz_odot,
                       biz_menu,
                       biz_first_mode_id biz_first_id,
                       biz_office_tele callno,
                       biz_mobile_tele sms,
                       biz_e_mail email, 
                       biz_mod_stuct,
                       ms_view_type$nib_name ms_view_type
                       from tbl_biz,tbl_biz_market,tbl_res_market_biz,tbl_categories ,tbl_biz_mod,tbl_mod_struct
                       where biz_id=bm_biz_id 
                       and bm_biz_id=mrktbiz_biz_id
                       and cat_id = biz_category_id
                       and biz_mod_biz_id = biz_id and biz_mod_mod_id = biz_first_mode_id
                       and ms_template = biz_mod_stuct and ms_level_no=1
                       and biz_status=1
                       and biz_isdeleted=0
                       and bm_biz_id in (
                       select bm_biz_id from 
		                    (SELECT bm_biz_id,bm_biz_long,bm_biz_lati,
		                    (6371 * acos( cos( radians($long) ) * cos( radians( bm_biz_long ) ) * cos( radians( bm_biz_lati ) - radians($lati) ) + sin( radians($long) ) * sin(radians(bm_biz_long)) ) ) AS distance 
		                    FROM tbl_biz_market,tbl_res_market_biz
                            where bm_biz_id=mrktbiz_biz_id
                            and mrktbiz_market_id = $marketId
		                    HAVING distance < $distance
		                    limit 50
		                    )t1 where(bm_biz_long <> 0 or bm_biz_lati <> 0)
	                    )
	                    $categorySql";
    
    $nearList = dbGetTable( $nearBizSQL );
    
    
    if(count($nearList) > 0){
        $j = 0;
        foreach ($nearList as $oneApp){
                $theID = $oneApp["biz_id"];
                $modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $theID and biz_mod_active=1 and mod_id>0 and mod_type=1");
                
                $biz_num_mode = dbGetVal("select count(biz_mod_mod_id) from tbl_biz_mod where biz_mod_biz_id={$oneApp["biz_id"]}");
                $bizAddress = $oneApp["biz_addr_street"]." ".$oneApp["biz_addr_no"]." ".$oneApp["biz_addr_town"];
                
                $responce->near_app[$j]['biz_id']=$oneApp["biz_id"];
                $responce->near_app[$j]['biz_short_name']=$oneApp["biz_short_name"];
                $responce->near_app[$j]['biz_icon']=$oneApp["biz_icon"];
                $responce->near_app[$j]['cat_id']=$oneApp["cat_id"];
                $responce->near_app[$j]['cat_name']=$oneApp["cat_name"];
                $responce->near_app[$j]['biz_first_id']=$oneApp["biz_first_id"];
                $responce->near_app[$j]['ms_view_type']=$oneApp["ms_view_type"];
                $responce->near_app[$j]['biz_num_mod']=$biz_num_mode;
                $responce->near_app[$j]['biz_layout']=$oneApp["biz_menu"];
                $responce->near_app[$j]['rating']=$oneApp["rating"];
                $responce->near_app[$j]['long']=$oneApp["longti"];
                $responce->near_app[$j]['lati']=$oneApp["lati"];
                $responce->near_app[$j]['call']=$oneApp["callno"];
                $responce->near_app[$j]['sms']=$oneApp["sms"];
                $responce->near_app[$j]['email']=$oneApp["email"];
                $responce->near_app[$j]['biz_market_img']="pach";
                $responce->near_app[$j]['biz_address']=$bizAddress;
                $k = 0;
                if(count($modList) > 0){
                    foreach ($modList as $oneMod){
                        $responce->near_app[$j]['biz_modules'][$k]["mod_name"] = $oneMod["mod_name"];
                        $responce->near_app[$j]['biz_modules'][$k]["mod_pic"] = $oneMod["mod_pic"];
                        $k++;
                    }
                }else {
                    $responce->near_app[$j]['biz_modules']="0";
                }
                $newOdot = dbGetVal("select md_info from tbl_mod_data1 where md_biz_id=$theID and md_element=6 and md_required=1");
                $responce->near_app[$j]['description']=$newOdot;
                $j++;
        }
    
    }
    
    
    if ($responce != ""){
        $json = json_encode($responce);
        echo $json;

    }else{
	echo "0";
    }
	
		
?>


