<?php
	require 'connect.php';
	require 'functions.php';
	
	foreach ($_GET as $key => $value)
	{
		$par = $par."$key has a value of $value <br />";
	}
	$repTEXT = "get_biz_list<br />".$par;
	//send_mail("dany@ald.co.il","crazyio2005@gmail.com","PapTap API debug",$repTEXT,"Dany","Dany");
	
	// start global parameters
    
    $android = $_GET["android"];
    $deviceID = $_GET["deviceID"];
    $iphone = $_GET["iphone"];
    $lang = $_GET["lang"];
    $deviceType = $_GET["deviceType"];
    $deviceModel = $_GET["deviceModel"];
    $deviceOrient = $_GET["deviceOrient"];
    $OS = $_GET["OS"];
    $OSVersion = $_GET["OSVersion"];
    $papTapVersion = $_GET["papTapVersion"];
    $long = $_GET["long"];
    $lati = $_GET["lati"];
    $phoneNumber = $_GET["phoneNumber"];
    
    $bizid = $_GET["bizid"];
    $state = $_GET["state"];
    $country = $_GET["country"];	
    $category = $_GET["category"];
    $subcategory = $_GET["subcategory"];
    $mod_id = $_GET["mod_id"];
    $level_no = $_GET["level_no"];
    $parent = $_GET["parent"];
    $in_favorites = $_GET["in_favorites"];
    $package_id = $_GET["packid"];

    $distance = $_GET["distance"];
    $unit = $_GET["unit"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = 0; 
    if($lati == 'null') $lati = 0; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 
    if($country == '' || $country == 'null') $country = "0"; 
    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    if($distance == '' || $distance == 'null') $distance = "5";
    if($unit == '' || $unit == 'null') $unit = "km";
    
    // end global parameters
	
    $nib_name = "";
    if($android == "1"){
            $nib_name = "_android";
    }

    if($state == ''){
            $state = "0";
    }

    if($country == ''){
            $country = "0";
    }
    
    $factor=1;
    switch ($unit) {
    case "mile":
        $factor = 1.6;
            break;
    }
    
    $distance = $factor*$distance;

    /* NEED PACKAGE ID? */
    addAPI("api_get_pack_apps",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$package_id,$mod_id,$level_no,$parent,$in_favorites,$country,$state);


    $nearBizSQL="select biz_id, 
                       biz_short_name, 
                       biz_icon, 
                       biz_category_id cat_id,
                       cat_name_$lang cat_name,
                       bm_user_rating rating,
                       bm_rating rating,
                       biz_office_tele callno,
                       biz_mobile_tele sms,
                       biz_e_mail email, 
                       bm_biz_long longti,
                       bm_biz_lati lati,
                       biz_odot,
                       biz_first_mode_id biz_first_id,
                       biz_mod_stuct,
                       biz_menu,
                       ms_view_type$nib_name ms_view_type,
                       (select count(biz_mod_mod_id) from tbl_biz_mod where biz_mod_biz_id = biz_id) biz_num_mod
                       from tbl_biz,tbl_biz_market,tbl_categories ,tbl_biz_mod,tbl_mod_struct,tbl_pack_biz
                       where biz_id=bm_biz_id 
                       and cat_id = biz_category_id
                       and biz_mod_biz_id = biz_id and biz_mod_mod_id = biz_first_mode_id
                       and ms_template = biz_mod_stuct and ms_level_no=1
                       and biz_status=2
                       and biz_id = pb_biz_id
                       and pb_pack_id = $package_id
                       order by biz_short_name";
    
    
    $nearList = dbGetTable( $nearBizSQL );
    
    
    if(count($nearList) > 0){
        $j = 0;
        foreach ($nearList as $oneApp){
                $theID = $oneApp["biz_id"];
                //$modList = dbGetTable("select mod_name, mod_pic from tbl_modules,tbl_biz_mod where mod_id = biz_mod_mod_id and biz_mod_biz_id = $theID and biz_mod_active=1 and mod_id>0");
                
                $responce->rows[$j]['biz_id']=$oneApp["biz_id"];
                $responce->rows[$j]['biz_short_name']=$oneApp["biz_short_name"];
                $responce->rows[$j]['biz_icon']=$oneApp["biz_icon"];
                $responce->rows[$j]['cat_id']=$oneApp["cat_id"];
                $responce->rows[$j]['cat_name']=$oneApp["cat_name"];
                $responce->rows[$j]['biz_first_id']=$oneApp["biz_first_id"];
                $responce->rows[$j]['ms_view_type']=$oneApp["ms_view_type"];
                $responce->rows[$j]['biz_num_mod']=$oneApp["biz_num_mod"];
                $responce->rows[$j]['long']=$oneApp["longti"];
                $responce->rows[$j]['lati']=$oneApp["lati"];
                $responce->rows[$j]['call']=$oneApp["callno"];
                $responce->rows[$j]['sms']=$oneApp["sms"];
                $responce->rows[$j]['email']=$oneApp["email"];
                $responce->rows[$j]['biz_market_img']="pach";
                $responce->rows[$j]['biz_layout']=$oneApp["biz_menu"];
                $j++;
        }
    
    }
    
    
    if ($responce != ""){
        $json = json_encode($responce);
        echo $json;

    }else{
	echo "0";
    }
	
		
?>


