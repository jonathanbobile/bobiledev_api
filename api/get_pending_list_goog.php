<?php
	require 'connect.php';
	require 'functions.php';
	
	$bizToSubmitSQL = "SELECT *
                                            FROM tbl_biz,
                                                tbl_owners,
                                                tbl_google_credentials,
                                                (SELECT biz_id appstore_biz, GROUP_CONCAT(appstore_id SEPARATOR ',') stores
                                                FROM tbl_biz_appstores 
                                                WHERE appstore_id <> 4 
                                                GROUP BY biz_id) appstr
                                            where biz_owner_id = owner_id
                                            AND owner_id = gc_owner_id
                                            and biz_id = appstore_biz
                                            and biz_id in (select biz_id from tbl_biz_appstores where appstore_id <> 4) 
                                            and biz_id not in (43,45,4371,13303,10379) 
                                            and biz_submit_intern_time is not null 
                                            and biz_submit_extern_time is null
                                            AND biz_ok_to_submit = 1
                                            and biz_goog_status=1
                                            order by biz_submit_intern_time asc
                                            ";
                        
//AND biz_submit_completed_treatment = 1
                  
     
	$bizList = dbGetTable( $bizToSubmitSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{   $bid = $oneBiz["biz_id"];
                  $appstore = dbGetVal("SELECT count(id) vol FROM tbl_biz_appstores WHERE appstore_id = 2 AND biz_id = $bid");
                  if ($appstore == "0"){
                        $responce->rows[$i]['build_only']="1";
                  }else{
                        $responce->rows[$i]['build_only']="0";
                  }
			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
            $responce->rows[$i]['owner_id']=$oneBiz["owner_id"];
            $responce->rows[$i]['acc_id']=$oneBiz["gc_account_id"];
            $responce->rows[$i]['acc_id_sec']=$oneBiz["gc_account_id_second"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
            $responce->rows[$i]['biz_phone']=($oneBiz["biz_office_tele"] == "") ? "1-800-800-800" : $oneBiz["biz_office_tele"];
            $responce->rows[$i]['biz_email']=$oneBiz["owner_username"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
            $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
            $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
            $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
            $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
            $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
            $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
            $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
            $responce->rows[$i]['bundle_id']=$oneBiz["biz_sbmt_goo_pack_id"];
            $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
            $responce->rows[$i]['category']=$oneBiz["sub_goog_val"];
            $responce->rows[$i]['theme']=$oneBiz["tmpl_name"];
            $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
	/*
		"Tapachula DEV SEP 2015"
                      (e5fbff58-7ff4-4e6f-9702-409f07e9bcb3)
	*/
		
?>