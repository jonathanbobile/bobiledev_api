<?php
	//require("../admin/libs/apiClass.php");
    
    //must params
    //
    // bizid
    // deviceID
    // lastmessageid - the las message ID that we have in mobile DB
    // lastpushid - the last push id that we have in the mobile DB
    
    $type = $_REQUEST["type"];
    
    $_REQUEST["fromPersonalZone"] = "1";

    $apiFactory = new apiFactory($_REQUEST);
    $apiClass = $apiFactory->getAPIInstance();
    //$apiClass = new apiClass($_REQUEST);    
    //$apiClass->debugMail($_GET,"Get personal zone","crazyio2005@gmail.com");   
    
    $response = array();
    
    if($type == "" || $type == "coupons"){
        $coupons = $apiClass->getCouponsUpdate();
        $response["coupons"] = json_decode($coupons);
    }
    
    if($type == "" || $type == "offers"){
        $offers = $apiClass->getOffers();
        $response["offers"] = ($offers == "") ? $offers : json_decode($offers);
    }
    
    //if($type == "" || $type == "messages"){
    //    $lastmessageid = $_REQUEST["lastmessageid"];
    //    $messages = $apiClass->getMessages($lastmessageid);
    //    $response["messages"] = ($messages == "") ? $messages : json_decode($messages); 
    //}
    
    if($type == "" || $type == "meetings"){
        $meetings = $apiClass->getCustomersMeetings();
        $response["meetings"] = ($meetings == "") ? $meetings : json_decode($meetings);
    }
    
    if($type == "" || $type == "push"){
        $lastpushid = $_REQUEST["lastpushid"];
        $push = $apiClass->getPush($lastpushid);
        $response["push"] = ($push == "") ? $push : json_decode($push);
    }
    
    if($type == "" || $type == "orders"){
        $orders = $apiClass->getOrdersUpdate();
        $response["orders"] = json_decode($orders);
    }
    
    if($type == "" || $type == "benefits"){
        $benefits = $apiClass->getBenefits();
        $response["benefits"] = json_decode($benefits);
        $response["membershipDiscount"] = $apiClass->getMembershipDiscount();
    }

    if($type == "" || $type == "requests"){
        $requests = $apiClass->getPaymentRequests();
        $response["requests"] = json_decode($requests);
    }
    
    if($type == "" || $type == "documents"){
        $documents = $apiClass->getDocuments();
        $response["documents"] = json_decode($documents);
    }

    if($type == "" || $type == "wishlist"){
        $wishlist = $apiClass->wishlist();
        $response["wishlist"] = json_decode($wishlist);
    }

    if($type == "" || $type == "membership"){
        $membership = $apiClass->membership();
        $response["membership"] = json_decode($membership);
    }

    if($type == "" || $type == "friends"){
        $friends = $apiClass->getInviteFriends();
        $response["friends"] = json_decode($friends);
    }

    if($type == "" || $type == "customer"){
        $customer = $apiClass->getCustomer();
        $response["customer"] = json_decode($customer);
    }

    if($type == "" || $type == "subscriptions"){
        $subscriptions = $apiClass->getCustomerSubscriptions();
        $response["subscriptions"] = $subscriptions;
    }

    if($type == "" || $type == "punchpasses"){
        $punch_passes = $apiClass->getCustomerPunchPasses();
        $response["punchpasses"] = $punch_passes;
    }

    if($type == "multiuseitems"){
        $response["multiuse"] = array();
        $punch_passes = $apiClass->getCustomerPunchPasses();
        $subscriptions = $apiClass->getCustomerSubscriptions();
        $response["multiuse"] = array_merge($response["multiuse"],$punch_passes);
        $response["multiuse"] = array_merge($response["multiuse"],$subscriptions);
    }

    
    $json = json_encode($response);
    echo $json;
	
?>