<?
require("../admin/libs/apiClass.php");

$apiClass = new apiClass($_REQUEST);

$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "getPush";

switch ($action)
{
    case "getPush":       
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";
        
        echo $apiClass->getPushHtml($pushID);        
        break;
        
	case "delivered":       
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";
        
        $apiClass->setPushDelivered($pushID);        
        break;  
        
    case "getGreeting":       
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";
        echo $apiClass->getGreeting($pushID);
        break;

    case "setOpen":
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";        
        $apiClass->setPushOpen($pushID);        
        break;

    
}

?>