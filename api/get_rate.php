<?php
	require 'connect.php';

    $bizid = $_GET["bizid"];

    $marketId = isset($_GET["marketId"]) ? $_GET["marketId"] : "0";
    
    $stats = dbGetRow("select sum(rate_score) as sum_score, count(rate_id) as count_score from tbl_rating where rate_biz_id=$bizid and rate_market=$marketId");
    
    $avgScore = "0";
    
    if($stats["count_score"] > 0)
    {
        $avgScore = $stats["sum_score"]/$stats["count_score"];
        $avgScore = round($avgScore, 2);
    }
    echo $avgScore;
		
?>