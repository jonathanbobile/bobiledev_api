<?php
require("../admin/libs/apiClass.php");
require("../admin/libs/languageManager.php");
require("../admin/MVC/Model.php");
require("../admin/models/ecommerceManager.php");
    
    $apiClass = new apiClass($_REQUEST);    
    $ecommerceManager = new ecommerceManager($apiClass->bizid); 

    $catID = $_REQUEST["cat_id"];
    $qs = $_REQUEST["qs"];
    
    if($catID == "-1") 
        $filters['keyword'] = $qs;
    else
        $filters['category'] = $catID;
    
    
    $levelsList = $ecommerceManager->getProducts($filters,1);
    
    if(count($levelsList) > 0)
	{        
		$i = 0;
        $attrIndex = 0;
		foreach ($levelsList as $oneElement) 
		{			      
			$responce->rows[$i]['md_row_id']=$oneElement["md_row_id"];
			$responce->rows[$i]['md_biz_id']=$oneElement["md_biz_id"];
			$responce->rows[$i]['md_mod_id']=$oneElement["md_mod_id"];
			$responce->rows[$i]['md_level_no']=$oneElement["md_level_no"];
			$responce->rows[$i]['md_parent']=$oneElement["md_parent"];
			$responce->rows[$i]['md_index']=$oneElement["md_index"];
			$responce->rows[$i]['md_icon']=($oneElement["md_icon"] == null) ? "" : $oneElement["md_icon"];			
			$responce->rows[$i]['md_head']=($oneElement["md_head"] == null) ? "" : $oneElement["md_head"];
			$responce->rows[$i]['md_desc']=($oneElement["md_desc"] == null) ? "" : $oneElement["md_desc"];
			$responce->rows[$i]['md_loc']=($oneElement["md_loc"] == null) ? "" : $oneElement["md_loc"];
			$responce->rows[$i]['md_date']=($oneElement["md_date"] == null) ? "" : $oneElement["md_date"];
			$responce->rows[$i]['md_info']=($oneElement["md_info"] == null) ? "" : $oneElement["md_info"];
			$responce->rows[$i]['md_score']=($oneElement["md_score"] == null) ? "" : $oneElement["md_score"];
			$responce->rows[$i]['md_price']=($oneElement["md_price"] == null) ? "" : $oneElement["md_price"];
			$responce->rows[$i]['cnt']="0";
			$responce->rows[$i]['md_type']=($oneElement["me_name"] == null) ? "" : $oneElement["me_name"];
            $responce->rows[$i]['md_next_view']="Info";
            $responce->rows[$i]['md_bool1']=$oneElement["md_bool1"];
            $responce->rows[$i]['md_bool2']=$oneElement["md_bool2"];
            
			$responce->rows[$i]['biz_level_img']["1"]=($oneElement["md_pic"] == null) ? "" : $oneElement["md_pic"];
			$responce->rows[$i]['biz_level_img']["2"]=($oneElement["md_pic1"] == null) ? "" : $oneElement["md_pic1"];
			$responce->rows[$i]['biz_level_img']["3"]=($oneElement["md_pic2"] == null) ? "" : $oneElement["md_pic2"];
			$responce->rows[$i]['biz_level_img']["4"]=($oneElement["md_pic3"] == null) ? "" : $oneElement["md_pic3"];
			$responce->rows[$i]['biz_level_img']["5"]=($oneElement["md_pic4"] == null) ? "" : $oneElement["md_pic4"];
			$responce->rows[$i]['biz_level_img']["6"]=($oneElement["md_pic5"] == null) ? "" : $oneElement["md_pic5"];
            $responce->rows[$i]['biz_level_img']["7"]=($oneElement["md_pic6"] == null) ? "" : $oneElement["md_pic6"];
			$responce->rows[$i]['biz_level_img']["8"]=($oneElement["md_pic7"] == null) ? "" : $oneElement["md_pic7"];
			$responce->rows[$i]['biz_level_img']["9"]=($oneElement["md_pic8"] == null) ? "" : $oneElement["md_pic8"];
			$responce->rows[$i]['biz_level_img']["10"]=($oneElement["md_pic9"] == null) ? "" : $oneElement["md_pic9"];
			$responce->rows[$i]['biz_level_img']["11"]=($oneElement["md_pic10"] == null) ? "" : $oneElement["md_pic10"];
            
            if($oneElement["me_name"] == "color_variant" || $oneElement["me_name"] == "type_variant"){
                
                $atribute = $ecommerceManager->getAttributeValues($oneElement["md_row_id"],$attrIndex);
                
                if(count($atribute) > 0){
                    $responce->rows[$i]['attribute'] = $atribute; 
                    $attrIndex++;
                }              
            }
            
            
			$i++;
		}       
		
		$json = json_encode($responce);
		echo $json;
	}
	else
	{
		echo "0";
	}  
	
?>