<?php
	require 'connect.php';
	require 'functions.php';

      
	
      $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_submit_appl_phone,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              sub_app_val,
                              biz_owner_id,
                              biz_push_type,
                              biz_firebase_project,
                              ac_owner_id,
                              ac_biz_id,
                              ac_status,
                              ac_team_id,
                              ac_username,
                              ac_team_name,
                              ac_password,
                              ac_team_apple_status,
                              owner_username,
                              ac_phone,
                              ac_name
                              FROM (SELECT *
                        FROM  (SELECT tbl_biz.*,tbl_owners.*, ac_id, ac_wizard_status,ac_phone,ac_name  FROM tbl_biz,tbl_owners,tbl_account  
                        WHERE biz_owner_id = owner_id
                        AND owner_account_id = ac_id 
                        AND biz_submit_intern_time IS NOT NULL
                        AND biz_submit_extern_time IS NULL
                        AND biz_paymentMethodToken <> ''
                        AND biz_submit_completed_treatment = 0
                        AND biz_appl_status IN (0,-1)
                        AND biz_ok_to_submit = 1
                        AND biz_id in (select biz_id from tbl_biz_appstores where appstore_id <> 4) 
                        AND biz_id not in (43,45,4371,13303,10379,4058) 
                        AND biz_owner_id NOT IN (14,185187)
                        AND biz_status = 1
                        AND ac_wizard_status IN ('first','finished')
                        ) BIZ
                        LEFT JOIN (SELECT ac_owner_id,
                        ac_biz_id,
                        ac_status,
                        ac_team_id,
                        ac_username,
                        ac_password,
                        ac_team_name,
                        ac_team_apple_status
                        FROM tbl_apple_credentials) tbl_apple_credentials ON BIZ.biz_owner_id = tbl_apple_credentials.ac_owner_id AND BIZ.biz_id = tbl_apple_credentials.ac_biz_id
                        ) AS a,tbl_sub_categories
                        WHERE biz_sub_category_id = sub_sub_id
                        ORDER BY biz_id
                        ";

                        //AND biz_id IN (246192)
                        //AND biz_id IN (345129)


/*	$bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              sub_app_val
                        FROM tbl_biz,tbl_sub_categories
                        WHERE biz_submit_intern_time IS NOT NULL
                        AND biz_submit_extern_time IS NULL
                        AND biz_appl_status = 0
                        AND biz_ok_to_submit = 1
                        AND biz_id in (select biz_id from tbl_biz_appstores where appstore_id <> 4) 
                        AND biz_id not in (43,45,4371,13303,10379,4058) 
                        AND biz_sub_category_id = sub_sub_id
                        AND biz_status = 1
                        ORDER BY biz_id ASC";
*/
/*
      $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              sub_app_val
                        FROM tbl_biz,tbl_sub_categories
                        WHERE biz_id = 18267
                        AND biz_sub_category_id = sub_sub_id
                        ORDER BY biz_id ASC";
                        */
                        
                        

                  
     
	$bizList = dbGetTable( $bizToSubmitSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{   
                  $bid = $oneBiz["biz_id"];
                  $appstore = dbGetVal("SELECT count(id) vol FROM tbl_biz_appstores WHERE appstore_id = 1 AND biz_id = $bid");
                  $b_owner_id = $oneBiz["biz_owner_id"];
                  $has_apple_acc = dbGetVal("SELECT count(ac_id) FROM tbl_apple_credentials WHERE ac_status = 'active' AND ac_owner_id = $b_owner_id AND ac_team_apple_status = 'approved'");
                  if ($appstore == "0" || $has_apple_acc == "0"){
                        $responce->rows[$i]['build_only']="1";
                  }else{
                        $responce->rows[$i]['build_only']="0";
                  }

			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
            $responce->rows[$i]['biz_owner_id']=$oneBiz["biz_owner_id"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
            $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
            $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
            $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
            $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
            $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
            $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
            $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
            $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
            $responce->rows[$i]['lang']=$oneBiz["biz_sbmt_market_lang"];
            $responce->rows[$i]['category']=$oneBiz["sub_app_val"];
            $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
            $responce->rows[$i]['biz_firebase_project']=$oneBiz["biz_firebase_project"];
            $responce->rows[$i]['ac_owner_id']=$oneBiz["ac_owner_id"];
            $responce->rows[$i]['ac_biz_id']=$oneBiz["ac_biz_id"];
            $responce->rows[$i]['ac_status']=$oneBiz["ac_status"];
            $responce->rows[$i]['ac_team_id']=$oneBiz["ac_team_id"];
            $responce->rows[$i]['ac_username']=$oneBiz["ac_username"];
            $responce->rows[$i]['ac_password']=$oneBiz["ac_password"];
            $responce->rows[$i]['ac_team_name']=$oneBiz["ac_team_name"];
            $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];
            $responce->rows[$i]['owner_username']=$oneBiz["owner_username"];
            $responce->rows[$i]['ac_phone']= ($oneBiz["biz_submit_appl_phone"] == "") ? $oneBiz["ac_phone"] : $oneBiz["biz_submit_appl_phone"];
            $responce->rows[$i]['ac_name']=$oneBiz["ac_name"];
			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
	/*
		"Tapachula DEV SEP 2015"
                      (e5fbff58-7ff4-4e6f-9702-409f07e9bcb3)
	*/
		
?>