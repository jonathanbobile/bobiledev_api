<?php
      require 'connect.php';
      require 'functions.php';
      
      $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_office_tele,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              owner_username,
                              sub_goog_val,
                              tmpl_name
                        FROM tbl_biz,tbl_owners,tbl_sub_categories,tbl_template
                        WHERE biz_submit_intern_time IS NOT NULL
                        AND biz_owner_id = owner_id
                        AND tmpl_id = biz_theme
                        AND biz_submit_extern_time IS NULL
                        AND biz_paymentMethodToken <> ''
                        AND biz_amaz_status = 0
                        AND biz_ok_to_submit = 1
                        and biz_id in (select biz_id from tbl_biz_appstores where appstore_id = 3) 
                        and biz_id not in (43,45,4371,13303,10379,4058,120236) 
                        AND biz_sub_category_id = sub_sub_id
                        AND biz_id not in (43,21832)
                        ORDER BY biz_id ASC
                        LIMIT 25";
                        
                        

                  
     
      $bizList = dbGetTable( $bizToSubmitSQL );
        
      if(count($bizList) > 0)
      {
            $i = 0;
            foreach ($bizList as $oneBiz) 
            {
                  $responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
                  $responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
                  $responce->rows[$i]['biz_phone']=($oneBiz["biz_office_tele"] == "") ? "1-800-800-800" : $oneBiz["biz_office_tele"];
                  $responce->rows[$i]['biz_email']=$oneBiz["owner_username"];
                  $responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
                  $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
                  $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
                  $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
                  $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
                  $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
                  $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
                  $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
                  $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
                  $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
                  $responce->rows[$i]['category']=$oneBiz["sub_goog_val"];
                  $responce->rows[$i]['theme']=$oneBiz["tmpl_name"];
                  $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

                  $i++;
            }
            
            $json = json_encode($responce);
            echo $json;
            
      }
      else
      {
            echo "0";
      }
      /*
            "Tapachula DEV SEP 2015"
                      (e5fbff58-7ff4-4e6f-9702-409f07e9bcb3)
      */
            
?>