<?php

	require 'connect.php';
	require 'functions.php';

  $operationMode = $_REQUEST["mode"];

$limit = "LIMIT 1";
$pdfDate = "AND biz_google_pdf_submit_date IS NOT NULL";
if ($operationMode == "pdf"){
    $limit = "LIMIT 50";
    $pdfDate = "AND biz_google_pdf_submit_date IS NULL AND biz_goog_status = 0";
}

      
      

	$bizToSubmitSQL = "SELECT K.*, 
                              gc_account_id,
                              gc_account_id_second,
                              gc_googlePlayClientId,
                              gc_googlePlaySecret,
                              gc_googlePlayCode,
                              gc_googlePlayRefreshToken,
                              gc_googlePlayAccessToken,
                              gc_owner_devloper_email 
                        FROM (
                              (SELECT biz_id,
                                    owner_id,
                                    biz_google_pdf_submit_date,
                                    biz_google_submit_step,
                                    biz_short_name,
                                    biz_office_tele,
                                    biz_fb_app_id,
                                    biz_submit_icon,
                                    biz_submit_splash,
                                    biz_submit_desc,
                                    biz_copy_right,
                                    biz_submit_keys,
                                    biz_submit_sprt_url,
                                    biz_submit_mrkt_url,
                                    biz_submit_priv_url,
                                    biz_sbmt_apl_bundle_suffix,
                                    biz_sbmt_market_lang,
                                    biz_push_type,
                                    biz_firebase_project,
                                    owner_username,
                                    sub_goog_val,
                                    sub_business_type,
                                    biz_sbmt_goo_build_version_num,
                                    tmpl_name 
                              FROM tbl_biz,tbl_owners,tbl_account,tbl_sub_categories,tbl_template
                              WHERE biz_submit_intern_time IS NOT NULL
                              AND biz_owner_id = owner_id
                              AND owner_account_id = ac_id
                              AND tmpl_id = biz_theme
                              AND biz_status = 1
                              AND biz_submit_extern_time IS NULL
                              AND biz_goog_status = 0
                              AND biz_ok_to_submit = 1
                              AND ac_wizard_status IN ('first','finished')
                              AND biz_submit_completed_treatment = 0 $pdfDate
                              AND biz_id IN (select biz_id from tbl_biz_appstores where appstore_id in (2,3)) 
                              AND biz_id not in (43,45,4371,13303,10379,4058,120236)
                              AND biz_sub_category_id = sub_sub_id
                              ) K
                              LEFT JOIN tbl_google_credentials
                              ON owner_id = gc_owner_id 
                              AND gc_account_id IS NOT NULL
                              )
                        ORDER BY biz_id ASC
                        $limit";
                        
                        
                  //WHERE gc_account_id NOT LIKE '04087249462404171544'
     
	$bizList = dbGetTable( $bizToSubmitSQL );

      
        
        
	if(count($bizList) > 0)
	{
           
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{   $bid = $oneBiz["biz_id"];
                  $appstore = dbGetVal("SELECT count(id) vol FROM tbl_biz_appstores WHERE appstore_id = 2 AND biz_id = $bid");
                  if ($appstore == "0"){
                        $responce->rows[$i]['build_only']="1";
                  }else{
                        $responce->rows[$i]['build_only']="0";
                  }

                  
                  //If this app isn't linked to any google account - link it in tbl_google_credentials
                  if (!isset($oneBiz["gc_account_id"])){


                  //     switch ($oneBiz["sub_business_type"]) {
                  //             case 'product':
                  //                   $theNewRecordId = duplicateSQLRecord ("tbl_google_credentials", "gc_id", 1733); //bobile.shops
                  //                   dbExecute("UPDATE tbl_google_credentials SET gc_owner_id = {$oneBiz["owner_id"]} WHERE gc_id = $theNewRecordId");
                  //                   break;
                  //             case 'service':
                  //                   if (($oneBiz["biz_id"] % 2) == 0){
                  //                     $theNewRecordId = duplicateSQLRecord ("tbl_google_credentials", "gc_id", 1731); //bobile.service
                  //                     dbExecute("UPDATE tbl_google_credentials SET gc_owner_id = {$oneBiz["owner_id"]} WHERE gc_id = $theNewRecordId");
                  //                   }else{
                  //                     //$theNewRecordId = duplicateSQLRecord ("tbl_google_credentials", "gc_id", 2321); //ira@bobile.com
                  //                     $theNewRecordId = duplicateSQLRecord ("tbl_google_credentials", "gc_id", 1732); // bobile.tech
                  //                     dbExecute("UPDATE tbl_google_credentials SET gc_owner_id = {$oneBiz["owner_id"]} WHERE gc_id = $theNewRecordId");
                  //                   }
                  //                   break;
                  //             default:
                  //                   $theNewRecordId = duplicateSQLRecord ("tbl_google_credentials", "gc_id", 1730); //bobile.dining
                  //                   dbExecute("UPDATE tbl_google_credentials SET gc_owner_id = {$oneBiz["owner_id"]} WHERE gc_id = $theNewRecordId");
                  //                   break;
                  //       }

                  //       $accountData = dbGetRow("SELECT * FROM tbl_google_credentials WHERE gc_id = $theNewRecordId");
                  //       $oneBiz["gc_account_id"] = $accountData["gc_account_id"];
                  //       $oneBiz["gc_account_id_second"] = $accountData["gc_account_id_second"];
                  //       $oneBiz["gc_googlePlayClientId"] = $accountData["gc_googlePlayClientId"];
                  //       $oneBiz["biz_google_pdf_submit_date"] = $accountData["biz_google_pdf_submit_date"];
                  //       $oneBiz["gc_googlePlaySecret"] = $accountData["gc_googlePlaySecret"];
                  //       $oneBiz["gc_owner_devloper_email"] = $accountData["gc_owner_devloper_email"];
                  //       $oneBiz["gc_googlePlayCode"] = $accountData["gc_googlePlayCode"];
                  //       $oneBiz["gc_googlePlayRefreshToken"] = $accountData["gc_googlePlayRefreshToken"];
                  //       $oneBiz["gc_googlePlayAccessToken"] = $accountData["gc_googlePlayAccessToken"];

                
                  $oneBiz["gc_account_id"] = "";
                  $oneBiz["gc_account_id_second"] = "";
                  $oneBiz["gc_googlePlayClientId"] = "";
                  $oneBiz["biz_google_pdf_submit_date"] = "";
                  $oneBiz["gc_googlePlaySecret"] = "";
                  $oneBiz["gc_owner_devloper_email"] = "";
                  $oneBiz["gc_googlePlayCode"] = "";
                  $oneBiz["gc_googlePlayRefreshToken"] = "";
                  $oneBiz["gc_googlePlayAccessToken"] = "";
                        

                  }

                  
                  

			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
                  $responce->rows[$i]['owner_id']=$oneBiz["owner_id"];
                  $responce->rows[$i]['acc_id']=$oneBiz["gc_account_id"];
                  $responce->rows[$i]['acc_id_sec']=$oneBiz["gc_account_id_second"];
                  $responce->rows[$i]['client_id']=$oneBiz["gc_googlePlayClientId"];
                  $responce->rows[$i]['biz_google_pdf_submit_date']=$oneBiz["biz_google_pdf_submit_date"];
                  $responce->rows[$i]['biz_google_submit_step']=$oneBiz["biz_google_submit_step"];
                  $responce->rows[$i]['client_secret']=$oneBiz["gc_googlePlaySecret"];
                  $responce->rows[$i]['gc_owner_devloper_email']=$oneBiz["gc_owner_devloper_email"];
                  $responce->rows[$i]['client_code']=$oneBiz["gc_googlePlayCode"];
                  $responce->rows[$i]['refresh_token']=$oneBiz["gc_googlePlayRefreshToken"];
                  $responce->rows[$i]['access_token']=$oneBiz["gc_googlePlayAccessToken"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
                  $responce->rows[$i]['biz_phone']=($oneBiz["biz_office_tele"] == "") ? "1-800-800-800" : $oneBiz["biz_office_tele"];
                  $responce->rows[$i]['biz_email']=$oneBiz["owner_username"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
                  $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
                  $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
                  $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
                  $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
                  $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
                  $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
                  $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
                  $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
                  $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
                  $responce->rows[$i]['category']=$oneBiz["sub_goog_val"];
                  $responce->rows[$i]['theme']=$oneBiz["tmpl_name"];
                  $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
                  $responce->rows[$i]['biz_firebase_project']=$oneBiz["biz_firebase_project"];
                  $responce->rows[$i]['biz_sbmt_goo_build_version_num']=$oneBiz["biz_sbmt_goo_build_version_num"];
                  $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}

      
      function duplicateSQLRecord ($table, $id_field ,$id) {
        // load the original record into an array
            
        $resultsql = "SELECT * FROM $table WHERE $id_field=$id";
        $original_record = dbGetRow($resultsql);



        // insert the new record and get the new auto_increment id
        $newid = dbExecute("INSERT INTO $table ($id_field) VALUES (NULL)");

        //echo "INSERT INTO $table ($id_field) VALUES (NULL)";

        // generate the query to update the new record with the previous values
        $query = "UPDATE $table SET ";
        foreach ($original_record as $key => $value) {
            
            if ($key != $id_field && $value != "") {
                $value = addslashes(stripslashes($value));
                
                //check if date other case add apostrophe
                //$value = ($value == "now()") ? $value : "'$value'";
                if ($key != "gc_owner_id"){
                  $query .= "$key='$value', ";
                }
            }
        } 
        $query = substr($query,0,strlen($query)-2); // lop off the extra trailing comma
        $query .= " WHERE {$id_field}={$newid}";
        
        $response = dbExecute($query);

        if($response != -1) {
            return $newid;
        }
        else{
            if($newid > 0 && $id_field != $newid){
                dbExecute("delete from $table where $id_field=$newid");
            }
            return $response;
        }
        
    }
    
		
?>