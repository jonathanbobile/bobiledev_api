<?php
	require 'connect.php';
	require 'functions.php';


      $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_whats_new,
                              biz_sbmt_apl_sku,
                              biz_sbmt_market_lang,
                              biz_sbmt_apl_bundle_id,
                              sub_app_val,
                              biz_owner_id,
                              biz_push_type,
                              biz_firebase_project,
                              ac_owner_id,
                              ac_biz_id,
                              ac_status,
                              ac_team_id,
                              ac_username,
                              ac_team_name,
                              ac_password,
                              owner_username,
                              ac_phone,
                              ac_name
                              FROM (SELECT tbl_biz.*,tbl_apple_credentials.*,owner_username,ac_phone,ac_name
                                    FROM tbl_owners,tbl_account,tbl_biz
                                    LEFT JOIN tbl_apple_credentials ON tbl_biz.biz_owner_id = tbl_apple_credentials.ac_owner_id AND tbl_biz.biz_id = tbl_apple_credentials.ac_biz_id
                                    WHERE biz_owner_id = owner_id
                                    AND owner_account_id = tbl_account.ac_id 
                                    AND biz_submit_intern_time IS NOT NULL
                                    AND biz_ok_to_submit = 1
                                    AND biz_id in (select biz_id from tbl_biz_appstores where appstore_id = 1) 
                                    AND biz_id not in (4371,13303,10379,4058) 
                                    AND biz_owner_id NOT IN (14,15,1,11131)
                                    AND biz_id NOT IN (404353,398681,395953,394642,390946,387906,380669,298670)
                                    /*AND biz_id IN (267991,247658,209923,297045,274930,203512)*/
                                    AND biz_id IN (203512)
                                    AND biz_appl_need_update = 1
                                    AND biz_appl_status >= 1
                                    ORDER BY biz_id DESC) AS a,tbl_sub_categories
                        WHERE biz_sub_category_id = sub_sub_id
                        ORDER BY biz_id DESC
                        LIMIT 1"; 

     
                        //285768 //216556
                  
     
	$bizList = dbGetTable( $bizToSubmitSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{

        if (!isset($oneBiz["ac_team_id"])){

            $theNewRecordId = duplicateSQLRecord ("tbl_apple_credentials", "ac_id", 4);
            dbExecute("UPDATE tbl_apple_credentials SET ac_owner_id = {$oneBiz["biz_owner_id"]},
                                                        ac_biz_id = {$oneBiz["biz_id"]},
                                                        ac_status = 'active',
                                                        ac_team_name = 'Bobile LTD',
                                                        ac_team_id = '2LLS7MGTY6',
                                                        ac_username = 'ira@paptap.com',
                                                        ac_password = '4rfvPa55w0rd' WHERE ac_id = $theNewRecordId");

            $accountData = dbGetRow("SELECT * FROM tbl_apple_credentials WHERE ac_id = $theNewRecordId");

            $oneBiz["ac_owner_id"] = $accountData["ac_owner_id"];
            $oneBiz["ac_biz_id"] = $accountData["ac_biz_id"];
            $oneBiz["ac_status"] = $accountData["ac_status"];
            $oneBiz["ac_team_id"] = $accountData["ac_team_id"];
            $oneBiz["ac_username"] = $accountData["ac_username"];
            $oneBiz["ac_team_name"] = $accountData["ac_team_name"];
            $oneBiz["ac_password"] = $accountData["ac_password"];

      }

			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
      $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
      $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
      $responce->rows[$i]['biz_owner_id']=$oneBiz["biz_owner_id"];
      $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
      $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
      $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
      $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
      $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
      $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
      $responce->rows[$i]['whats_new']=$oneBiz["biz_sbmt_whats_new"];
      $responce->rows[$i]['sku']=$oneBiz["biz_sbmt_apl_sku"];
      $responce->rows[$i]['bundle_id']=$oneBiz["biz_sbmt_apl_bundle_id"];
      $responce->rows[$i]['category']=$oneBiz["sub_app_val"];
      $responce->rows[$i]['ac_owner_id']=$oneBiz["ac_owner_id"];
      $responce->rows[$i]['ac_status']=$oneBiz["ac_status"];
      $responce->rows[$i]['ac_team_id']=$oneBiz["ac_team_id"];
      $responce->rows[$i]['ac_username']=$oneBiz["ac_username"];
      $responce->rows[$i]['ac_password']=$oneBiz["ac_password"];
      $responce->rows[$i]['ac_team_name']=$oneBiz["ac_team_name"];
      $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
      $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
      $responce->rows[$i]['biz_firebase_project'] = $oneBiz["biz_firebase_project"];
      $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];
      $responce->rows[$i]['owner_username']=$oneBiz["owner_username"];
      $responce->rows[$i]['ac_phone']=$oneBiz["ac_phone"];
      $responce->rows[$i]['ac_name']=$oneBiz["ac_name"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}


  function duplicateSQLRecord ($table, $id_field ,$id) {
      // load the original record into an array
          
      $resultsql = "SELECT * FROM $table WHERE $id_field=$id";
      $original_record = dbGetRow($resultsql);



      // insert the new record and get the new auto_increment id
      $newid = dbExecute("INSERT INTO $table ($id_field) VALUES (NULL)");

      //echo "INSERT INTO $table ($id_field) VALUES (NULL)";

      // generate the query to update the new record with the previous values
      $query = "UPDATE $table SET ";
      foreach ($original_record as $key => $value) {
          
          if ($key != $id_field && $value != "") {
              $value = addslashes(stripslashes($value));
              
              //check if date other case add apostrophe
              //$value = ($value == "now()") ? $value : "'$value'";
              if ($key != "gc_owner_id"){
                $query .= "$key='$value', ";
              }
          }
      } 
      $query = substr($query,0,strlen($query)-2); // lop off the extra trailing comma
      $query .= " WHERE {$id_field}={$newid}";
      
      $response = dbExecute($query);

      if($response != -1) {
          return $newid;
      }
      else{
          if($newid > 0 && $id_field != $newid){
              dbExecute("delete from $table where $id_field=$newid");
          }
          return $response;
      }
        
  }
		
?>