<?php
	require 'connect.php';
	require 'functions.php';

    $bizToSubmitSQL = "SELECT biz_id,
                              biz_short_name,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_apl_bundle_id,
                              biz_sbmt_apl_sku,
                              biz_sbmt_apl_version,
                              biz_sbmt_apl_bundle_suffix,
                              biz_sbmt_market_lang,
                              biz_appl_need_update_list,
                              sub_app_val,
                              biz_owner_id,
                              biz_appl_status,
                              biz_push_type,
                              ac_owner_id,
                              ac_biz_id,
                              ac_status,
                              ac_team_id,
                              ac_username,
                              ac_team_name,
                              ac_password
                              FROM (SELECT *
                        FROM tbl_biz
                        LEFT JOIN tbl_apple_credentials ON tbl_biz.biz_owner_id = tbl_apple_credentials.ac_owner_id AND tbl_biz.biz_id = tbl_apple_credentials.ac_biz_id
                        WHERE biz_submit_intern_time IS NOT NULL
                        AND biz_submit_extern_time IS NOT NULL
                        AND biz_ok_to_submit = 1
                        and biz_id in (select biz_id from tbl_biz_appstores where appstore_id IN (1,2)) 
                        and biz_id not in (43,45,4371,13303,10379,4058) 
                        AND biz_status = 1
                        AND (biz_goog_need_update_list = 1)
                        AND biz_assets_refreshed = 0
                        AND (biz_appl_status = 2 OR biz_goog_status = 2)
                        ) AS a,tbl_sub_categories
                        WHERE biz_sub_category_id = sub_sub_id
                        AND biz_id IN (411512,190509,113603)
                        ORDER BY biz_id DESC
                        LIMIT 5";    

                  
   //OR biz_amaz_need_update_list = 1
                        //biz_appl_need_update_list = 1 OR 

                  
     
	$bizList = dbGetTable( $bizToSubmitSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{
                  if ($oneBiz["biz_appl_status"] > 0 && $oneBiz["ac_owner_id"] == ""){

                        
                        $insert = "INSERT INTO tbl_apple_credentials SET ac_owner_id = {$oneBiz["biz_owner_id"]},
                                                                         ac_team_name = 'Bobile LTD',
                                                                         ac_team_id = '2LLS7MGTY6',
                                                                         ac_publicKey = '',
                                                                         ac_username = 'ira@paptap.com',
                                                                         ac_password = '4rfvPa55w0rd',
                                                                         ac_status = 'active'";

                        //echo $insert;
                        $newAccountId = dbExecute($insert);

                        $oneBiz["ac_owner_id"] = $newAccountId;
                        $oneBiz["ac_status"] = "active";
                        $oneBiz["ac_team_id"] = "2LLS7MGTY6";
                        $oneBiz["ac_username"] = "ira@paptap.com";
                        $oneBiz["ac_password"] = "4rfvPa55w0rd";
                        $oneBiz["ac_team_name"] = "Bobile LTD";
                        


                  }else if ($oneBiz["ac_owner_id"] == ""){

                        $oneBiz["ac_owner_id"] = "";
                        $oneBiz["ac_status"] = "";
                        $oneBiz["ac_team_id"] = "";
                        $oneBiz["ac_username"] = "";
                        $oneBiz["ac_password"] = "";
                        $oneBiz["ac_team_name"] = "";
                  }
                  if ($oneBiz["biz_appl_need_update_list"] == "0"){
                        $responce->rows[$i]['build_only']="1";
                  }else{
                        $responce->rows[$i]['build_only']="0";
                  }

      	         $responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
      	         $responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
      	         $responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
                  $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
                  $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
                  $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
                  $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
                  $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
                  $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
                  $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
                  $responce->rows[$i]['suffix']=$oneBiz["biz_sbmt_apl_bundle_suffix"];
                  $responce->rows[$i]['whats_new']=$oneBiz["biz_sbmt_whats_new"];
                  $responce->rows[$i]['sku']=$oneBiz["biz_sbmt_apl_sku"];
                  $responce->rows[$i]['bundle_id']=$oneBiz["biz_sbmt_apl_bundle_id"];
                  $responce->rows[$i]['category']=$oneBiz["sub_app_val"];
                  $responce->rows[$i]['sbmt_apl_version']=$oneBiz["biz_sbmt_apl_version"];
                  $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
                  $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
                  $responce->rows[$i]['ac_owner_id']=$oneBiz["ac_owner_id"];
                  $responce->rows[$i]['ac_biz_id']=$oneBiz["ac_biz_id"];
                  $responce->rows[$i]['ac_status']=$oneBiz["ac_status"];
                  $responce->rows[$i]['ac_team_id']=$oneBiz["ac_team_id"];
                  $responce->rows[$i]['ac_username']=$oneBiz["ac_username"];
                  $responce->rows[$i]['ac_password']=$oneBiz["ac_password"];
                  $responce->rows[$i]['ac_team_name']=$oneBiz["ac_team_name"];
                  $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
		
?>