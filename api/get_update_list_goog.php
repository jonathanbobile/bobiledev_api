<?php
	require 'connect.php';
	require 'functions.php';

    $badIds = "194954,246605,345129,408651,416917,420680,425036,429235,429923,430576,432675,301181,192774,201386,203512,203805,238469,275715,301427,307131,345445,301485,301627";

    $bizToSubmitSQL = "SELECT biz_id,
                              owner_id,
                              biz_google_submit_step,
                              gc_account_id,
                              gc_account_id_second,
                              gc_googlePlayClientId,
                              gc_googlePlaySecret,
                              gc_googlePlayCode,
                              gc_googlePlayRefreshToken,
                              gc_googlePlayAccessToken,
                              biz_short_name,
                              biz_office_tele,
                              biz_fb_app_id,
                              biz_submit_icon,
                              biz_submit_splash,
                              biz_submit_desc,
                              biz_copy_right,
                              biz_submit_keys,
                              biz_submit_sprt_url,
                              biz_submit_mrkt_url,
                              biz_submit_priv_url,
                              biz_sbmt_goo_pack_id,
                              biz_sbmt_whats_new,
                              biz_sbmt_market_lang,
                              biz_push_type,
                              biz_firebase_project,
                              owner_username,
                              sub_goog_val,
                              biz_sbmt_apl_sku,
                              tmpl_name,
                              biz_sbmt_goo_build_version_num,
                              biz_sbmt_apl_bundle_id
                        FROM tbl_biz,tbl_owners,tbl_sub_categories,tbl_template,tbl_google_credentials,tbl_queue_google_update
                        WHERE biz_owner_id = owner_id
                        AND owner_id = gc_owner_id
                        AND tmpl_id = biz_theme
                        AND biz_id = qa_biz_id
                        AND biz_id NOT IN ($badIds)
                        AND biz_sub_category_id = sub_sub_id
                        ORDER BY qa_biz_id DESC 
                        LIMIT 1";       

                  
     
	$bizList = dbGetTable( $bizToSubmitSQL );
        
	if(count($bizList) > 0)
	{
		$i = 0;
		foreach ($bizList as $oneBiz) 
		{
			$responce->rows[$i]['biz_id']=$oneBiz["biz_id"];
                  $responce->rows[$i]['owner_id']=$oneBiz["owner_id"];
                  $responce->rows[$i]['biz_google_submit_step']=$oneBiz["biz_google_submit_step"];
                  $responce->rows[$i]['acc_id']=$oneBiz["gc_account_id"];
                  $responce->rows[$i]['acc_id_sec']=$oneBiz["gc_account_id_second"];
                  $responce->rows[$i]['client_id']=$oneBiz["gc_googlePlayClientId"];
                  $responce->rows[$i]['client_secret']=$oneBiz["gc_googlePlaySecret"];
                  $responce->rows[$i]['client_code']=$oneBiz["gc_googlePlayCode"];
                  $responce->rows[$i]['refresh_token']=$oneBiz["gc_googlePlayRefreshToken"];
                  $responce->rows[$i]['access_token']=$oneBiz["gc_googlePlayAccessToken"];
			$responce->rows[$i]['biz_short_name']=$oneBiz["biz_short_name"];
                  $responce->rows[$i]['biz_phone']=($oneBiz["biz_office_tele"] == "") ? "1-800-800-800" : $oneBiz["biz_office_tele"];
                  $responce->rows[$i]['biz_email']=$oneBiz["owner_username"];
			$responce->rows[$i]['biz_icon']=$oneBiz["biz_submit_icon"];
                  $responce->rows[$i]['biz_launch']=$oneBiz["biz_submit_splash"];
                  $responce->rows[$i]['biz_desc']=$oneBiz["biz_submit_desc"];
                  $responce->rows[$i]['biz_copy_right']=$oneBiz["biz_copy_right"];
                  $responce->rows[$i]['biz_keywords']=$oneBiz["biz_submit_keys"];
                  $responce->rows[$i]['biz_support_url']=$oneBiz["biz_submit_sprt_url"];
                  $responce->rows[$i]['biz_marketing_url']=$oneBiz["biz_submit_mrkt_url"];
                  $responce->rows[$i]['biz_privacy_url']=$oneBiz["biz_submit_priv_url"];
                  $responce->rows[$i]['biz_google_pack_id']= isset($oneBiz["biz_sbmt_goo_pack_id"]) && $oneBiz["biz_sbmt_goo_pack_id"] != '' ? $oneBiz["biz_sbmt_goo_pack_id"] : $oneBiz["biz_sbmt_apl_bundle_id"];
                  $responce->rows[$i]['whats_new']=$oneBiz["biz_sbmt_whats_new"];
                  $responce->rows[$i]['category']=$oneBiz["sub_goog_val"];
                  $responce->rows[$i]['theme']=$oneBiz["tmpl_name"];
                  $responce->rows[$i]['biz_push_type']=$oneBiz["biz_push_type"];
                  $responce->rows[$i]['biz_firebase_project'] = $oneBiz["biz_firebase_project"];
                  $responce->rows[$i]['lang']=($oneBiz["biz_sbmt_market_lang"] == "english")? "English":$oneBiz["biz_sbmt_market_lang"];
                  $responce->rows[$i]['biz_sbmt_goo_build_version_num']=$oneBiz["biz_sbmt_goo_build_version_num"];
                  $responce->rows[$i]['biz_fb_app_id']= ($oneBiz["biz_fb_app_id"] == "") ? "fb476541845701337" : $oneBiz["biz_fb_app_id"];

			$i++;
		}
		
		$json = json_encode($responce);
		echo $json;
		
	}
	else
	{
		echo "0";
	}
		
?>