<?php
$base = siteURL();


function siteURL()
{
    return "https://".$_SERVER['HTTP_HOST'];
}

?>

<html lang="en-US" class=" js flexbox canvas canvastext webgl no-touch geolocation postmessage websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" style="">
	<head>
		<!-- <script type="text/javascript" id="www-widgetapi-script" src="https://s.ytimg.com/yts/jsbin/www-widgetapi-vflO1YZr2/www-widgetapi.js" async=""></script>-->
		<!-- META DATA -->
		<meta http-equiv="content-type" content="text/html;charset=UTF-8">
		<meta http-equiv="cache-control" content="max-age=604800" />
		<meta http-equiv="last-modified" content="Sat, 1 Oct 2016 10:10:10 GMT" />
		<meta charset="UTF-8">
		<meta name="author" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="google-site-verification" content="QW-BXvoqwbNfufRYLVYOmrZmH4negeYsjnIKU-koups">
		<title>Mobile App Builder | Create A Your Own Native Mobile App | PapTap.com - Creating your own native mobile app has never been easier. No coding, no hassle, it only takes minutes and it's free.</title>
		<meta name="description" content="The Future of Mobile is Native. Native mobile apps provide a rich content display your clients will love consuming. Give your clients the easiest access to your business by installing your app.You don't need to be a designer or a developer to get your own stunning mobile app." style="position: relative;">
        <meta name="keywords" content="paptap, create, app, business, tools, native, native app, free app builder, app builder, friendly app builder, no coding, app template, apps for business, mobile schedule, mobile chat, mobile ecommerce, mobile shop, mobile payments, create app, app creator" style="position: relative;">
        <script>
        	if (window.location.protocol == "http:") {
            	window.location.href = window.location.href.replace("http", "https");
        	}
    	</script>


    	<script type="text/javascript">
    		window.onload = function() {

    			
    			setTimeout(loadStyles(), 10);
    			setTimeout(loadJQuery(), 10);
			  	setTimeout(loadBootStrap(), 1500);
			  	setTimeout(function(){
			  		loadOurScript();
			  	}, 1500);
			  	setTimeout(function(){
			  		loadFancyBox();
			  	}, 3500);
			  	setTimeout(loadYouTubeVideo(), 250);
			  	setTimeout(loadIntercom(),4500);
			  	setTimeout(function(){
			  		document.getElementById("body-content").className = "loaded";
			  		document.getElementById("loader").className = "loaded";
			  	},10);
			  

			};

			var tv;
			var playerDefaults = {autoplay: 1, loop: 1,  playlist:'Pj0k0_GQiR4', autohide: 1, modestbranding: 0, rel: 0, showinfo: 0, controls: 0, disablekb: 1, enablejsapi: 1, iv_load_policy: 3};
			var vid;


			function loadJQuery(){


				var tag2 = document.createElement('script');
					tag2.src = '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js';
					tag2.id = 'jQueryUIScript';
					tag2.type = "text/javascript";
				var firstScriptTag0 = document.getElementsByTagName('script')[0];
					firstScriptTag0.parentNode.insertBefore(tag2, firstScriptTag0);

				var tag = document.createElement('script');
					tag.src = '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js';
					tag.id = 'jQueryScript';
					tag.type = "text/javascript";
				var firstScriptTag = document.getElementsByTagName('script')[0];
					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


			}



			function loadBootStrap(){
				var tag = document.createElement('script');
					tag.src = '<?=$base?>/wp/wp-content/themes/Alpine/assets/js/bootstrap.min.js';
					tag.id = 'bootStrapScript';
				var jQueryScriptTag = document.getElementsByTagName('script')[1];
					jQueryScriptTag.parentNode.insertBefore(tag, jQueryScriptTag);

				var tagModal = document.createElement('script');
					tagModal.src = '<?=$base?>/wp/wp-content/themes/Alpine/assets/js/bootstrap-modal.min.js';
					tagModal.id = 'bootStrapModalScript';
				var bsScriptTag = document.getElementsByTagName('script')[3];
					bsScriptTag.parentNode.insertBefore(tagModal, bsScriptTag);
				
			}

			function loadOurScript(){
				var head = document.getElementsByTagName('head')[0];
				var tag = document.createElement('script');
					tag.src = '<?=$base?>/wp/wp-content/themes/Alpine/assets/js/external.min.js';
					tag.id = 'bobileScript';
					tag.type = "text/javascript";
				head.appendChild(tag);

				if (typeof initPlugins == "undefined"){
					setTimeout(function (){initPlugins();},2000);
				}
			}

			function loadFancyBox(){
				var head = document.getElementsByTagName('head')[0];
				var tag = document.createElement('script');
					tag.src = '<?=$base?>/admin/scripts/fancybox/jquery.fancybox-1.3.4.pack.min.js';
					tag.id = 'fancyBoxScript';
					tag.type = "text/javascript";
				head.appendChild(tag);
			}

			function loadStyles(){
				var head = document.getElementsByTagName('head')[0];
				var bsCssTag = document.createElement('link');
					bsCssTag.rel = "stylesheet";
					bsCssTag.id = "bootStrapStyle";
					bsCssTag.href = "<?=$base?>/wp/wp-content/themes/Alpine/assets/css/bootstrap.min.css";
					bsCssTag.type = "text/css";
					bsCssTag.media = "all";
				head.appendChild(bsCssTag);

				var fancyStyleTag = document.createElement('link');
					fancyStyleTag.rel = "stylesheet";
					fancyStyleTag.id = "fancyBoxStyle";
					fancyStyleTag.href = "<?=$base?>/admin/scripts/fancybox/jquery.fancybox-1.3.4.css";
					fancyStyleTag.type = "text/css";
					fancyStyleTag.media = "all";
				head.appendChild(fancyStyleTag);

				var railwayFontTag = document.createElement('link');
					railwayFontTag.rel = "stylesheet";
					railwayFontTag.id = "fancyBoxStyle";
					railwayFontTag.href = "https://fonts.googleapis.com/css?family=Raleway%3A400%2C700";
					railwayFontTag.type = "text/css";
					railwayFontTag.media = "all";
				head.appendChild(railwayFontTag);


				var ourStyleTag = document.createElement('link');
					ourStyleTag.rel = "stylesheet";
					ourStyleTag.id = "fancyBoxStyle";
					ourStyleTag.href = "<?=$base?>/wp/wp-content/themes/Alpine/external.min.css";
					ourStyleTag.type = "text/css";
					ourStyleTag.media = "all";
				head.appendChild(ourStyleTag);
			}

			
			function loadYouTubeVideo(){

				var tag = document.createElement('script');
						tag.src = '//youtube.com/player_api';
				var firstScriptTag = document.getElementsByTagName('script')[0];
						firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
				
			}


			function onYouTubePlayerAPIReady(){
			  tv = new YT.Player('tv', {
			  	events: {'onReady': onPlayerReady, 'onStateChange': onPlayerStateChange}, 
			  	playerVars: playerDefaults,
			    videoId: 'Pj0k0_GQiR4',
			    origin: 'https://bobile.com'});
			}

			function onPlayerReady(){
			  tv.mute();
			}

			function onPlayerStateChange(e) {
			
				var tvElement = document.getElementById("tv");
			  	if (e.data === 1){
			    	//tvElement.className = "screen,mute,active,loaded"; 
			    	tvElement.className = "loaded";
			  	} 
			  	else if (e.data === 2){
			    	//tvElement.className = "screen,mute,loaded";
			  	}
			  
			}

			function vidRescale(){

				var tvElement = document.getElementById("tv");	
			  	var w = window.innerWidth + 200;
			    var h = window.innerHeight + 200;

			  	if (w/h > 16/9){
			  		if (typeof tv != "undefined"){
			    		tv.setSize(w, w/16*9);
			    	}
			    	tvElement.style.left = "-50px";
			    	tvElement.style.marginLeft = "-10px";
			    	//tvElement.className = "loaded";
			  	} else {
			  		if (typeof tv != "undefined"){
			    		tv.setSize(h/9*16, h);
			    	}
			    	var calc = -(tvElement.offsetHeight - w)/2;
			    	tvElement.style.left = calc+"px";
			    	tvElement.style.marginLeft = "-10px";
			    	//tvElement.className = "loaded";
			  
			  	}

			  	if (typeof tv == "undefined"){ //wait for youTube to load
			  		//wait(500);
			  		setTimeout(function (){vidRescale();},500);
			  	}
			  
			}

			function loadIntercom(){

				window.intercomSettings = {
			        app_id: "ceyraqtj"
			    };

			   
		    	var w = window; 
		    	var ic = w.Intercom; 
		    	if (typeof ic === "function") { 
		    		ic('reattach_activator'); 
		    		ic('update', intercomSettings); 
		    	} else { 
		    		var d = document; 
		    		var i = function () { 
		    			i.c(arguments) 
		    		}; 
		    		i.q = []; 
		    		i.c = function (args) { 
		    			i.q.push(args) 
		    		}; 
		    		w.Intercom = i; 


		    		function l() { 
		    			var s = d.createElement('script'); 
		    			s.type = 'text/javascript'; 
		    			s.async = true; s.src = 'https://widget.intercom.io/widget/ceyraqtj'; 
		    			var x = d.getElementsByTagName('script')[0]; 
		    			x.parentNode.insertBefore(s, x); 
		    		} 

		    		l();

		    		/*
		    		if (w.attachEvent) { 
		    			w.attachEvent('onload', l); 
		    		} else { 
		    			w.addEventListener('load', l, false); 
		    		} 
		    		*/
		    	} 

	
			    
			}

			 if (typeof tv == "undefined"){
			 	setTimeout(function (){vidRescale();},1000);
			 }

    	</script>

    	<!--

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <script type="text/javascript" src="<?=$base?>/wp/wp-content/themes/Alpine/assets/js/bootstrap.min.js?ver=1.0.0"></script>
		<script type="text/javascript" src="<?=$base?>/wp/wp-content/themes/Alpine/assets/js/bootstrap-modal.js?ver=1.0.0"></script>


    	<script type="text/javascript" async="" src="//staticw2.yotpo.com/fq5QE0Xf02fKqS5gOwDaFas7BYLvs9Hu1NpA7KUW/widget.js"></script>

    	<script src="<?=$base?>/admin/scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>

	 	<script type="text/javascript" src="<?=$base?>/wp/wp-content/themes/Alpine/assets/js/external.js"></script>

	 	


		
	 	<link rel="stylesheet" id="bootstrap-css" href="https://bobile.com/wp/wp-content/themes/Alpine/assets/css/bootstrap.min.css?ver=4.4.5" type="text/css" media="all">

		<link rel="stylesheet" type="text/css" href="<?=$base?>/admin/scripts/fancybox/jquery.fancybox-1.3.4.css" />
		<link rel="stylesheet" id="Raleway-css" href="https://fonts.googleapis.com/css?family=Raleway%3A400%2C700&amp;ver=4.4.5" type="text/css" media="all">

		<link rel="stylesheet" type="text/css" href="https://bobile.com/wp/wp-content/themes/Alpine/external.css"  media="all">

		-->


		
		<!--
		<link rel="stylesheet" id="jquery.bxslider-css" href="https://bobile.com/wp/wp-content/themes/Alpine/assets/css/jquery.bxslider.css?ver=4.4.5" type="text/css" media="all">
		<link rel="stylesheet" id="owl.carousel-css" href="https://bobile.com/wp/wp-content/themes/Alpine/assets/css/owl.carousel.css?ver=4.4.5" type="text/css" media="all">
		<link rel="stylesheet" id="owl.theme-css" href="https://bobile.com/wp/wp-content/themes/Alpine/assets/css/owl.theme.css?ver=4.4.5" type="text/css" media="all">
		
		<link rel="stylesheet" id="style-css" href="https://bobile.com/wp/wp-content/themes/Alpine/style.css?ver=4.4.5" type="text/css" media="all">
		<link rel="stylesheet" id="style-responsive-css" href="https://bobile.com/wp/wp-content/themes/Alpine/style-responsive.css?ver=4.4.5" type="text/css" media="all">
		
		<link rel="stylesheet" id="ot-dynamic-dynamic_css-css" href="https://bobile.com/wp/wp-content/themes/Alpine/dynamic.css?ver=2.1.4" type="text/css" media="all">
		-->


		<script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-40263393-1']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script> 
        
        

		 <!-- YotPos Widget 
		<script type="text/javascript">
		(function e(){var e=document.createElement("script");e.type="text/javascript",e.async=true,e.src="//staticw2.yotpo.com/fq5QE0Xf02fKqS5gOwDaFas7BYLvs9Hu1NpA7KUW/widget.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})();
		</script>
		-->

		<style type="text/css">

			#body-content {

				opacity: 0;

				-webkit-transition: opacity 1s ease-in .8s;
			       -moz-transition: opacity 1s ease-in .8s;
			        -ms-transition: opacity 1s ease-in .8s;
			         -o-transition: opacity 1s ease-in .8s;
			            transition: opacity 1s ease-in .8s;
			        
			}

			#loader{
				opacity: 1;

				-webkit-transition: opacity 2.5s ease-in 2s;
			       -moz-transition: opacity 2.5s ease-in 2s;
			        -ms-transition: opacity 2.5s ease-in 2s;
			         -o-transition: opacity 2.5s ease-in 2s;
			            transition: opacity 2.5s ease-in 2s;
			}



			#tv {

				opacity: 0;

				-webkit-transition: opacity 2s ease-in 2s;
			       -moz-transition: opacity 2s ease-in 2s;
			        -ms-transition: opacity 2s ease-in 2s;
			         -o-transition: opacity 2s ease-in 2s;
			            transition: opacity 2s ease-in 2s;
			        
			}

			#body-content.loaded,
			#tv.loaded{
				opacity:1;
			}

			#loader.loaded{
				opacity: 0;
			}

			

			.tv {
				  position: absolute;
				  top: 0;
				  left: 0;
				  z-index: 1;

				  width: 100%;
				  height: 100%;

				  overflow: hidden;
				  
				  .screen {
				    position: absolute;
				    top: 0;
				    right: 0;
				    bottom: 0;
				    left: 0;
				    z-index: 1;

				    margin: auto;

				  }
				}

				#fancybox-close{
					right: -55px !important;
				}
		</style>
					
</head>

<body>
<div id="loader" style="position:absolute; background-color:#2b2b2b; width:100%; height:100%; left:0px; top:0px;"><img style="position:absolute; left: 49%; top: 49%;" src="<?=$base?>/wp/wp-content/themes/Alpine/assets/images/loading.gif"></div>
<div id="body-content">    
    <!-- HOME Section Start -->
    <section id="home" style="margin-top:0px !important;">
      	<div class="intro-video">
      		<div class="tv">
		  		<div class="screen mute" id="tv"></div>
			</div>
			<a class="top-login topLogin"></a>
	        <div class="intro-text-slider" style="background: url('<?=$base?>/wp/wp-content/themes/Alpine/assets/images/pattern.png');">
	          	<div class="text-slider">
	            	<div class="intro-item">
	              		<div class="section-title text-center">
	              			<ul class="textbxslider" style="width: auto; position: relative;">
	              				<li>
	              					<h1>Create your own<i>mobile app </i></h1>
	              				</li>
	              				<li style="display:none;">
	              					<h1>It's Super<i>Easy &amp; Fast</i></h1>
	              				</li>
	              				<li style="display:none;">
	              					<h1 >Grow your business<i>Starting today!</i></h1>
	              				</li>
	              			</ul>

			              	<div class="hidden-xs">
			              		<span class="line"></span>
			              		<span>bobile - a mobile app maker tailor-made for small businesses</span>
			              		<span class="line"></span>
			              	</div>

	              		</div>
	              		<div class="mybutton ultra main-button">
	              			<a class="start-button" href=""> 
	              				<span data-hover="Create App">Create App</span> 
	              			</a>
	              		</div>
	             	</div>
	          	</div>
	        </div>
      	</div>
    </section>
    <!-- HOME Section End -->

    <!-- Navigation Bar Start -->   
	<div id="navigation-sticky-wrapper" class="sticky-wrapper" style="height: 60px; position: relative;">
		<div id="navigation" class="navbar navbar-default navbar-fixed-top" role="navigation">
		  	<div class="navbar-inner">
			    <div class="navbar-header">
			        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			        	<span class="sr-only">Toggle navigation</span>
			        	<i class="fa fa-bars fa-2x"></i>
			        </button>
			        <a id="brand" class="navbar-brand" href="https://bobile.com/wp">
			            <img alt="Logo" title="logo" src="https://bobile.com/wp/wp-content/uploads/2016/08/logo_bobile.png" class="img-responsive">                  
			        </a>
			    </div>
			    <div class="navbar-collapse collapse">
			        <div class="menu-main-menu-container">
				      	<ul id="menu-main-menu" class="navbar-nav navbar-right">
				      		<li id="menu-item-404" class="menu-item menu-item-type-post_type menu-item-object-onepage menu-item-404"><a href="https://bobile.com/wp/#youre-covered">TOOLS</a></li>
							<li id="menu-item-407" class="menu-item menu-item-type-post_type menu-item-object-onepage menu-item-407"><a href="https://bobile.com/wp/#key-features">FEATURES</a></li>
							<li id="menu-item-411" class="menu-item menu-item-type-post_type menu-item-object-onepage menu-item-411"><a href="https://bobile.com/wp/#pricing">PLANS</a></li>
							<li id="menu-item-1153" class="menu-item menu-item-type-post_type menu-item-object-onepage menu-item-1153"><a href="https://bobile.com/wp/#reviews">Reviews</a></li>
							<li id="menu-item-418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-418"><a>Login</a></li>
						</ul>
					</div>    
				</div>
		    </div>
		</div>
	</div>  
	<!-- Navigation Bar End -->  
	
	<!-- Why Bobile Section -->										
    <section id="youre-covered" class="section-content slide-menu ">
        <div class="container">
		    <div class="row">
		        <div class="container">
		            <div class="row">
		                <div class="section-title text-center">
		                    <div class="col-md-12">
		                        <h1 class="item_right">YOU'RE COVERED</h1>
								<div>
									<span class="line"></span>
									<span>Creating a mobile app for your business has never been easier</span>
									<span class="line"></span>
								</div>                              
							</div>
		            	</div>
		            </div>
		        </div>
		        <p></p>
		        <div class="col-md-3 column">
		        	<div class="item_top">
			      		<div class="element-line">
			        		<div class="service-items text-center ">
			          			<a>
			            			<div class="no-boundaries-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb1.png);" onmouseover="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb1h.png)';" onmouseout="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb1.png)';"></div>
			            			<h3>EDITOR</h3>
			          			</a>
			          			<p>We devoted the last year to develop a state of the art EDITOR to fit your needs.&nbsp;The Bobile Editor is where you’ll be able to easily edit your mobile app and tailor it to your business. Start now and fall in love with app creation.</p>
			        		</div>
			      		</div>
		      		</div>
		      	</div>
		      	<div class="col-md-3 column">
		      		<div class="item_bottom">
				      	<div class="element-line">
				        	<div class="service-items text-center ">
					          	<a>
					            	<div class="no-boundaries-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb3.png);" onmouseover="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb3h.png)';" onmouseout="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb3.png)';"></div>
					            	<h3>SUBMITTER</h3>
					          	</a>
					          	<p>Sit back &amp; relax as we’ll do the heavy lifting. Thanks to our automatic app submission, unleashing your app to the world was never so easy.&nbsp;We’ll take care of submitting to the app stores for you.&nbsp;Once we’re done, everyone will be able to find &amp; experience your business on their mobile.</p>
				        	</div>
				      	</div>
				    </div>
				</div>
		      	<div class="col-md-3 column">
		      		<div class="item_top">
		      			<div class="element-line">
		        			<div class="service-items text-center ">
		          				<a>
		            				<div class="no-boundaries-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb4.png);" onmouseover="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb4h.png)';" onmouseout="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb4.png)';"></div>
		            				<h3>RETENTION</h3>
		          				</a>
		          				<p>We understand that generating more sales is one of your main goals.&nbsp;Inside your personal WorkSpace you’ll find the most advanced tools to keep your customers returning to your app.</p>
		        			</div>
		      			</div>
		      		</div>
		      	</div>
		      	<div class="col-md-3 column">
		      		<div class="item_bottom">
			      		<div class="element-line">
			        		<div class="service-items text-center ">
			          			<a>
			            			<div class="no-boundaries-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb2.png);" onmouseover="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb2h.png)';" onmouseout="this.style.background='url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/nb2.png)';"></div>
			            			<h3>ASSISTANT</h3>
			          			</a>
			          			<p>Taking you from scratch to success.&nbsp;We will guide you, hand in hand, through all steps from creating the mobile app to generating mobile sales.</p>
			        		</div>
			      		</div>
		      		</div>
		      	</div>
		      	<p></p>
        	</div>
    	</div>
    </section>
				
	<!-- Feature Section Start -->			
  	<div id="key-features" class="slide-menu parallax parallax-55" style=" background-image:url(https://storage.googleapis.com/paptap/wp/features_back2.jpg);background-attachment:fixed;">
    	<div class="parallax-overlay"></div>
    	<div class="section-content">
            <div class="container">
        		<div class="row">
                	<div class="text-center">
                        <div class="col-md-12">
              				<div class="item_left">
                				<h1>OUR FEATURES - YOUR BUSINESS SUCCESS</h1>
                            </div>
            			</div>
            			<!-- Features Slider Start -->
                    	<div class="parallax-content">
              				<div class="col-md-8 col-md-offset-2">
            					<div class="element-line">
              						<div class="owl-single owl-carousel text-center owl-theme">

              						<!--
              							<div class="owl-wrapper-outer autoHeight">
              								<div class="owl-wrapper">

              								-->

              									<div class="owl-item">
              										<div class="item">
              											<span class="feature-fact">“Mobile now accounts for 50.3% of all e-commerce traffic” <br>
              												<span>(Shopify Aug 26, 2014)</span>
              											</span>
              											<img width="314" height="284" src="https://bobile.com/wp/wp-content/uploads/2014/03/mobile_shop.png" class="img-center img-responsive wp-post-image" alt="mobile_shop" srcset="https://bobile.com/wp/wp-content/uploads/2014/03/mobile_shop-300x271.png 300w, https://bobile.com/wp/wp-content/uploads/2014/03/mobile_shop.png 314w" sizes="(max-width: 314px) 100vw, 314px">
              											<h2>Mobile Shop</h2>
              											<p>Open your own M-Shop and start selling your products directly from the mobile <br>
															the world's leading shopping platform</p>
              											<p class="lead">Gain More Sales</p>
            										</div>
            									</div>
            									<div class="owl-item">
            										<div class="item">
              											<span class="feature-fact">“The most apparent impact of “no-shows” appointments is lost of revenue”<br>
              												<span>Appointment-Plus - Reducing the ‘No-Show’ Rate at Your Organization</span>
              											</span>
              											<img width="314" height="284" src="https://bobile.com/wp/wp-content/uploads/2014/03/calendar.png" class="img-center img-responsive wp-post-image" alt="calendar" srcset="https://bobile.com/wp/wp-content/uploads/2014/03/calendar-300x271.png 300w, https://bobile.com/wp/wp-content/uploads/2014/03/calendar.png 314w" sizes="(max-width: 314px) 100vw, 314px">
              											<h2>Mobile Booking</h2>
              											<p>Let your customers easily schedule appointments and be automatically reminded<br>directly from your app</p>
              											<p class="lead">Decrease no-shows</p>
            										</div>
            									</div>
            									<div class="owl-item">
            										<div class="item">
										              	<span class="feature-fact">“40% Of U.S. smartphone users will redeem a mobile coupon ”<br>
										              		<span>TrueShip March 24th, 2015</span>
										              	</span>
										              	<img width="314" height="284" src="https://bobile.com/wp/wp-content/uploads/2016/06/coupons.png" class="img-center img-responsive wp-post-image" alt="coupons" srcset="https://bobile.com/wp/wp-content/uploads/2016/06/coupons-300x271.png 300w, https://bobile.com/wp/wp-content/uploads/2016/06/coupons.png 314w" sizes="(max-width: 314px) 100vw, 314px">
										              	<h2>Coupons</h2>
										              	<p>Increase sales by offering your customers personalized mobile deals <br> to be redeemed &amp; paid straight from their mobile</p>
										              	<p class="lead">Offer top deals</p>
										            </div>
										        </div>
										        <div class="owl-item">
										        	<div class="item">
										              	<span class="feature-fact">“44% of consumers say that they would like brands to deliver deals and coupons to their mobile devices”<br>
										              		<span>Millward Brown</span>
										              	</span>
										              	<img width="314" height="284" src="https://bobile.com/wp/wp-content/uploads/2016/06/push.png" class="img-center img-responsive wp-post-image" alt="push" srcset="https://bobile.com/wp/wp-content/uploads/2016/06/push-300x271.png 300w, https://bobile.com/wp/wp-content/uploads/2016/06/push.png 314w" sizes="(max-width: 314px) 100vw, 314px">
										              	<h2>Push Messages</h2>
										              	<p>Send direct messages and interact with your customers<br>everywhere and at anytime</p>
										              	<p class="lead">Improves Customer Engagement</p>
										            </div>
										        </div>
										        <div class="owl-item">
										        	<div class="item">
										              	<span class="feature-fact">“Americans are spending nearly five hours a day on smartphones”<br>
										              		<span>Mobile Intelligence, 2015</span>
										              	</span>
										              	<img width="314" height="284" src="https://bobile.com/wp/wp-content/uploads/2014/03/chat.png" class="img-center img-responsive wp-post-image" alt="chat" srcset="https://bobile.com/wp/wp-content/uploads/2014/03/chat-300x271.png 300w, https://bobile.com/wp/wp-content/uploads/2014/03/chat.png 314w" sizes="(max-width: 314px) 100vw, 314px">
										              	<h2>Live Chat</h2>
										              	<p>Be there for your clients when they need you the most<br>Use your app to communicate with them at real time</p>
										              	<p class="lead">Personal customer service</p>
										            </div>
										        </div>
										        <!--
										    </div>
										</div>  

										-->
										<!--
										<div class="owl-controls clickable">
											<div class="owl-pagination">
												<div class="owl-page">
													<span class=""></span>
												</div>
												<div class="owl-page active">
													<span class=""></span>
												</div>
												<div class="owl-page">
													<span class=""></span>
												</div>
												<div class="owl-page">
													<span class=""></span>
												</div>
												<div class="owl-page">
													<span class=""></span>
												</div>
											</div>
										</div>

										-->
									</div>
								</div>
							</div>
						</div>
						<!-- Features Slider End -->
          			</div>
              	</div>
      		</div>
        </div>
  	</div>
  	<!-- Features Section End -->

	<!-- Plans Section  Start -->						
  	<section id="pricing" class="section-content slide-menu ">
        <div class="container">
      		<div class="row">
            	<div class="container">
          			<div class="row">
            			<div class="section-title text-center">
              				<div class="col-md-12">
                                <h1 class="item_left">Plans</h1>
								
                <div><span class="line"></span><span>Pick a plan that suits your business</span><span class="line"></span></div>                              </div>
            </div>
          </div>
        </div>
        	<div class="col-md-12 column text-center"><div class="pricing-box">  <div class="item_bottom">    <div class="element-line">    <div class="pricing-popular"></div>      <ul>        <li class="title-row">          <h4>Personal Plan</h4>          <h5>Test your app</h5>        </li>        <li class="price-row">          <span><sup>USD</sup>10<sub>/month</sub></span>        </li> <li> White Label Native App&nbsp;</li> <li> No Ads&nbsp;</li> <li> Google Play Submission </li> <li> —&nbsp;</li>       </ul>    </div>  </div></div><div class="pricing-box main">  <div class="item_bottom">    <div class="element-line">    <div class="pricing-popular"></div>      <ul class="pricing-featured">        <li class="title-row">          <h4>Small Business Plan</h4>          <h5>Expose your business</h5>        </li>        <li class="price-row">          <span><sup>USD</sup>20<sub>/month</sub></span>        </li> <li> White Label Native App </li> <li> No Ads&nbsp;</li> <li> App Stores Submission </li> <li> — </li>       </ul>    </div>  </div></div><div class="pricing-box">  <div class="item_bottom">    <div class="element-line">    <div class="pricing-popular"></div>      <ul>        <li class="title-row">          <h4>Mobile Commerce</h4>          <h5>Engage your customers</h5>        </li>        <li class="price-row">          <span><sup>USD</sup>30<sub>/month</sub></span>        </li> <li> White Label Native App </li> <li> No Ads&nbsp;</li> <li> App Stores Submission </li> <li> All business features&nbsp;</li>       </ul>    </div>  </div></div></div>
          	</div>
    	</div>
    </section>
    <!-- Plans Section  End -->
	
	<!-- Counters Section Start -->			
  	<div id="counter-number" class="slide-menu parallax parallax-52" style=" background-image:url(https://storage.googleapis.com/paptap/wp/counter_back3.jpg);background-attachment:fixed;">
    	<div class="parallax-overlay"></div>
    	<div class="section-content">
            <div class="container">
        		<div class="row">
                	<div class="text-center">
                        <div class="col-md-12">
              				<div class="item_right">
                				<h1>Trusted by thousands of small businesses globally</h1>
                            </div>
            			</div>
                        <div class="parallax-content">
              				<p></p>
              				<div class="col-md-4 column"> 
              					<div class="number-counters text-center">
               						<div class="counters-item element-line">
                 						<div class="counters-item-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/counter1.png) bottom center no-repeat;"></div>
                 						<strong data-to="+202000">+202000</strong>
                 						<p class="lead">Mobile apps build with Bobile</p>
               						</div>
             					</div>
             				</div>
	             			<div class="col-md-4 column"> 
	             				<div class="number-counters text-center">
	               					<div class="counters-item element-line">
	                 					<div class="counters-item-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/counter2.png) bottom center no-repeat;"></div>
	                 					<strong data-to="237">237</strong>
	                 					<p class="lead">Countries our clients operate in</p>
	               					</div>
	             				</div>
	             			</div>
             				<div class="col-md-4 column"> 
             					<div class="number-counters text-center">
               						<div class="counters-item element-line">
                 						<div class="counters-item-img" style="background:url(https://bobile.com/wp/wp-content/themes/Alpine/assets/images/counter3.png) bottom center no-repeat;"></div>
                 						<strong data-to="+1600">+1600</strong>
                 						<p class="lead">App Engagements a day</p>
               						</div>
             					</div>
             				</div>
             				<p></p>
            			</div>
          			</div>
              	</div>
      		</div>
        </div>
  	</div>
  	<!-- Counters Section End -->		
			
	<!-- Reviews Section Start -->			
  	<section id="reviews" class="section-content slide-menu ">
        <div class="container">
      		<div class="row">
            	<div class="container">
          			<div class="row">
            			<div class="section-title text-center">
              				<div class="col-md-12">
                                <h1 class="item_left">Reviews</h1>		
	                			<div>
	                				<span class="line"></span>
	                				<span>See what our loyal customers think of us</span>
	                				<span class="line"></span>
	                			</div>                              
                			</div>
            			</div>
          			</div>
        		</div>
        		<p><!--YotPo Reviews Carousel --></p>

        		<!--YotPo Reviews Carousel -->


	            <style type="text/css">
	            .standard-layout.single-carousel.review{
	            	max-width: 330px;
					overflow: hidden;
	            }

	            .carousel-review-title,
	            .carousel-review-body{
	            	max-width: 240px;
	            }

	            .standard-layout.single-carousel.review .big-version{
	            	overflow-wrap: break-word;
	            }
	            /*
	                .scroller{width: 920px !important;}
	                .yotpo.yotpo-carousel.y-slider-container.y-slide-left-animations{
	                    left:0px !important;
	                }
	                .standard-layout.single-carousel.review{
	                    margin-right:10px !important; 
	                }

	                @media (min-width: 1200px) and (max-width: 1500px){
	                    .scroller{width: 720px !important;}
	                }

	                @media (min-width: 900px) and (max-width: 1200px){
	                    .scroller{width: 660px !important;}
	                }
	                */

	            </style>
				<div class="row">
					<div class="col-md-12">
						<div class="yotpo yotpo-reviews-carousel" 
                        data-mode="top_rated" 
                        data-type="site" 
                        data-count="3" 
                        data-autoplay-enabled="0" 
                        data-autoplay-speed="3000" 
                        data-header-customisation-text="Trusted by hunderds of thousands" 
                        data-header-customisation-font-size="18" 
                        data-header-customisation-color="#414141" 
                        data-header-customisation-alignment="center" 
                        data-background-color="transparent">&nbsp;</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    <!-- Reviews Section End -->
					
	<!-- FOOTER -->
	<footer id="footer" style="position: relative;">
      	<div class="container">
			<div class="row">
	            <div class="col-md-2 col-sm-6">
	                <div class="footer-links">
	                    <a class="footer-link" href="https://bobile.com/wp/#home">Home</a>
	                    <a class="footer-link" href="https://bobile.com/wp/about/" target="_blank">About Us</a>
	                    <a class="footer-link" href="https://bobile.com/wp/why-bobile/" target="_blank">Why Bobile</a>
	                    <a class="footer-link" href="https://bobile.com/wp/plans/" target="_blank">Pricing</a>
	                    <a class="footer-link" href="https://bobile.com/wp/portfolio/">Get Inspired</a>
	                    <a class="footer-link footerLogin" href="#">Login</a>
	                </div>
	            </div>
	            <div class="col-md-2 col-sm-6">
	                <div class="footer-links">
	                    <a class="footer-link" href="https://bobile.com/wp/helpcenter" target="_blank">HelpCenter</a>
	                    <a class="footer-link" href="https://bobile.com/wp/terms" target="_blank">Terms of Use</a>
	                    <a class="footer-link" href="https://bobile.com/wp/privacy" target="_blank">Privacy Policy</a>
	                    <a class="footer-link" href="https://bobile.com/wp/bobile-story/" target="_blank">Our Story</a>
	                    <!--<a class="footer-link" href="https://affiliate.bobile.com" target="_blank">Make Money</a>-->
	                    <div class="clear"></div>
	                </div>
	            </div>
            	<div class="col-md-4 hidden-xs hidden-sm"></div>
	            <div class="col-md-4 hidden-xs hidden-sm">
	                <div class="footer-logo"></div>
	                <p>Promote your business, showcase your products/services, set up a Mobile Shop and engage your customers. Bobile app builder has everything you need to create a fully pesonalized, high-quality app for your business.</p>
	                <p>ⓒ 2012-2016 bobile.com Inc.</p>
	                <div class="social-icon">
	                  <a href="https://www.facebook.com/paptapapp" target="_blank"><i class="fa fa-facebook fa-2x"></i></a><a href="https://twitter.com/PapTap" target="_blank"><i class="fa fa-twitter fa-2x"></i></a><a href="https://www.linkedin.com/company/paptap" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a><a href="https://plus.google.com/+Paptapapp/posts" target="_blank"><i class="fa fa-google-plus fa-2x"></i></a><a href="https://www.youtube.com/user/Paptapapp" target="_blank"><i class="fa fa-youtube fa-2x"></i></a>                </div>
	            </div>
        	</div>
      	</div>
    </footer>

	<a href="#" class="register"></a>
		
		<!--
	<script type="text/javascript">
	$(window).load(function () {
		$(".home_parallax").parallax("50%", "0.4");
		$(".parallax-55").parallax("50%", "0.3");
		$(".parallax-52").parallax("50%", "0.3");
	});

	</script>

	-->

    <!-- YotPos Widget -->
    <script type="text/javascript">
    	(function e(){var e=document.createElement("script");e.type="text/javascript",e.async=true,e.src="//staticw2.yotpo.com/fq5QE0Xf02fKqS5gOwDaFas7BYLvs9Hu1NpA7KUW/widget.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})();
    </script>


   
	<!-- Intercom -->
	<!--
	<script>
	    window.intercomSettings = {
	        app_id: "ceyraqtj"
	    };
	</script>
	<script>
	    (function () { var w = window; var ic = w.Intercom; if (typeof ic === "function") { ic('reattach_activator'); ic('update', intercomSettings); } else { var d = document; var i = function () { i.c(arguments) }; i.q = []; i.c = function (args) { i.q.push(args) }; w.Intercom = i; function l() { var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/ceyraqtj'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); } if (w.attachEvent) { w.attachEvent('onload', l); } else { w.addEventListener('load', l, false); } } })()
	</script>
	-->
</div>
</body>
</html>