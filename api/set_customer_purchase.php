<?
    require_once('../admin/MVC/config.php');
    require_once('../admin/MVC/Database.php');
    require_once('../admin/libs/languageManager.php');
    require_once('../admin/libs/customersManager.php');
	require 'connect.php';
	require 'functions.php';

	// start global parameters
    
    $android = $_REQUEST["android"];
    $deviceID = $_REQUEST["deviceID"];
    $customerID = $_REQUEST["customerID"];
    $iphone = $_REQUEST["iphone"];
	$lang = $_REQUEST["lang"];
    $deviceType = $_REQUEST["deviceType"];
	$deviceModel = $_REQUEST["deviceModel"];
    $deviceOrient = $_REQUEST["deviceOrient"];
    $OS = $_REQUEST["OS"];
    $OSVersion = $_REQUEST["OSVersion"];
    $papTapVersion = $_REQUEST["papTapVersion"];
    $long = $_REQUEST["long"];
    $lati = $_REQUEST["lati"];
    $phoneNumber = $_REQUEST["phoneNumber"];
    
	$bizid = $_REQUEST["bizid"];
    $state = $_REQUEST["state"];
    $country = $_REQUEST["country"];	
    $category = $_REQUEST["category"];
    $subcategory = $_REQUEST["subcategory"];
    $mod_id = $_REQUEST["mod_id"];
    $level_no = $_REQUEST["level_no"];
    $parent = $_REQUEST["parent"];
    $in_favorites = $_REQUEST["in_favorites"];
    $orderId = $_REQUEST["orderId"];

    if($android == 'null' || $android == '') $android = "0";
    if($deviceID == 'null') $deviceID = "";
    if($iphone == 'null') $iphone = "0";
    if($lang == 'null') $lang = "eng";
    if($deviceType == 'null') $deviceType = "";
    if($deviceModel == 'null') $deviceModel = "";
    if($deviceOrient == 'null') $deviceOrient = "";
    if($OS == 'null') $OS = "Android";
    if($OSVersion == 'null') $OSVersion = "";
    if($papTapVersion == 'null') $papTapVersion = "";
    if($long == 'null') $long = ""; 
    if($lati == 'null') $lati = ""; 
    if($phoneNumber == 'null') $phoneNumber = "";    
    if($orderId == 'null') $orderId = "";
    
    if($bizid == '' || $bizid == 'null') $bizid = "0"; 
    if($state == '' || $state == 'null') $state = "0"; 

    if($category == '' || $category == 'null') $category = "0";
    if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
    if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
    if($level_no == '' || $level_no == 'null') $level_no = "0";
    if($parent == '' || $parent == 'null') $parent = "0";
    if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";
    
    $cust_id = customersManager::getCusIdByDevice($bizid,$deviceID);

    // end global parameters

    if($deviceID == "") $deviceID = $_REQUEST["user"];    //temporery  for iphone     
    
	$countryNumber = ( !is_int($country) ? (ctype_digit($country)) : true );
	if(!$countryNumber)
	{
		$country = dbGetVal("select country_id from tbl_countries where country_code='$country'");
	}
    if($country == '' || $country == 'null') $country = "0"; 
	
	
    //purchase Fields
    $processorReferenceId = addslashes(urldecode($_REQUEST["processorReferenceId"]));
    $captureCode = addslashes(urldecode($_REQUEST["captureCode"]));
    $reconciliationId = addslashes(urldecode($_REQUEST["reconciliationId"]));
    $amount = addslashes(urldecode($_REQUEST["amount"]));
    $currency = addslashes(urldecode($_REQUEST["currency"]));
    $paymentMethodToken = addslashes(urldecode($_REQUEST["paymentMethodToken"]));
    $paymentMethodType = addslashes(urldecode($_REQUEST["paymentMethodType"]));
    $cvvNumber = addslashes(urldecode($_REQUEST["cvvNumber"]));
    $ipAddress = addslashes(urldecode($_REQUEST["ipAddress"]));
    $phone = addslashes(urldecode($_REQUEST["phone"]));
    $items = $_REQUEST["items"];   
    
    if($orderId == ""){
        $order_id = dbExecute("insert into tbl_cust_transaction_order set cto_amount=$amount,cto_currency='$currency',cto_device_id='$deviceID',cto_cust_id=$cust_id,cto_biz_id=$bizid, cto_cust_id=$cust_id"); 
    }else{
        $order_id = $orderId;
    }
    
    $orderTime = dbGetVal("select cto_order_tyme from tbl_cust_transaction_order where cto_id=$order_id");
    $longOrderId = substr(strtotime($orderTime), -8) . $order_id;

    $transNumber = dbExecute("insert into tbl_cust_transaction set
                           tr_biz_id = $bizid,
                           tr_order_id=$order_id,
                           tr_reference_Id = '$processorReferenceId',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = 'CHARGE',
                           tr_amount=$amount,
                           tr_currency='$currency',
                           tr_paymentMethodToken='$paymentMethodToken',
                           tr_paymentMethodType='$paymentMethodType',
                           tr_cvv = '$cvvNumber',
                           tr_ip = '$ipAddress',
                           tr_country = '$country',
                           tr_phone = '$phone',
                           tr_device_id='$deviceID',
                           tr_cust_id=$cust_id
                          ");
    
      
    
    if($orderId == ""){
        $jarray = json_decode($items);
        
        foreach($jarray as $oneObject){
            
            $name = $oneObject->name;
            $quantity = $oneObject->quantity;
            $price = $oneObject->price;
            $item_id = $oneObject->item_id;
            $redeemCode = $oneObject->redeemCode;
            $item_type = $oneObject->item_type;
            $couponID = $oneObject->couponID;
            
            $itemRow = dbGetRow("select * from tbl_items where ite_id=$item_id"); 
            $withoutVat = $itemRow["ite_sell_price"];
            $vat = $itemRow["ite_vat"];
            $totalWithoutVat = $totalWithoutVat + $withoutVat;
            $totalVat = $totalVat + $vat;
            
            $insert = "insert into tbl_cust_transaction_items 
                    set tri_order_id=$order_id, 
                    tri_name='$name', 
                    tri_quantity='$quantity', 
                    tri_price=$price, 
                    tri_without_vat=$withoutVat,
                    tri_vat=$vat,
                    tri_item_id=$item_id,
                    tri_item_type=$item_type,
                    tri_itm_row_id=$couponID, 
                    tri_redeem_code='$redeemCode'";
            dbExecute($insert);

        }
        dbExecute("update tbl_cust_transaction_order set cto_without_vat=$totalWithoutVat, cto_vat=$totalVat, cto_long_order_id='$longOrderId' where cto_id=$order_id");
    }
   
    echo "ok";
		
?>