<?php
date_default_timezone_set("UTC");
require("../admin/libs/apiClass.php");

$apiClass = new apiClass($_REQUEST);
header('Content-Type: application/json');
$result = $apiClass->setMeeting($_REQUEST);

echo json_encode($result);


?>
