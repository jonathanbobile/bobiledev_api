<?
require 'connect.php';
require 'functions.php';

$android = $_GET["android"];
$deviceID = $_GET["deviceID"];
$iphone = $_GET["iphone"];
$lang = $_GET["lang"];
$deviceType = $_GET["deviceType"];
$deviceModel = $_GET["deviceModel"];
$deviceOrient = $_GET["deviceOrient"];
$OS = $_GET["OS"];
$OSVersion = $_GET["OSVersion"];
$papTapVersion = $_GET["papTapVersion"];
$long = $_GET["long"];
$lati = $_GET["lati"];
$phoneNumber = $_GET["phoneNumber"];

$bizid = $_GET["bizid"];
$state = $_GET["state"];
$country = $_GET["country"];	
$category = $_GET["category"];
$subcategory = $_GET["subcategory"];
$mod_id = $_GET["mod_id"];
$level_no = $_GET["level_no"];
$parent = $_GET["parent"];
$in_favorites = $_GET["in_favorites"];
$isadmin = $_GET["isadmin"];

if($android == 'null' || $android == '') $android = "0";
if($deviceID == 'null') $deviceID = "";
if($iphone == 'null') $iphone = "0";
if($lang == 'null') $lang = "eng";
if($deviceType == 'null') $deviceType = "";
if($deviceModel == 'null') $deviceModel = "";
if($deviceOrient == 'null') $deviceOrient = "";
if($OS == 'null') $OS = "Android";
if($OSVersion == 'null') $OSVersion = "";
if($papTapVersion == 'null') $papTapVersion = "";
if($long == 'null') $long = ""; 
if($lati == 'null') $lati = ""; 
if($phoneNumber == 'null') $phoneNumber = "";    

if($bizid == '' || $bizid == 'null') $bizid = "0"; 
if($state == '' || $state == 'null') $state = "0"; 
if($country == '' || $country == 'null') $country = "0"; 
if($category == '' || $category == 'null') $category = "0";
if($subcategory == '' || $subcategory == 'null') $subcategory = "0";
if($mod_id == '' || $mod_id == 'null') $mod_id = "0";
if($level_no == '' || $level_no == 'null') $level_no = "0";
if($parent == '' || $parent == 'null') $parent = "0";
if($in_favorites == '' || $in_favorites == 'null') $in_favorites = "0";

// end global parameters

$nib_name = "";
if($android == "1")
{
    $nib_name = "_android";
}

if($iphone == "0")
{
    $mainwidth = "620";
}
else if($iphone == "1")
{
    $mainwidth = "310";
}
else
{
    if($iphone > 620)
    {
        $mainwidth = "620";
    }
    else
    {
        $mainwidth = "100%";
    }
}

if($state == '')
{
    $state = "0";
}

addAPI("api_get_biz",$deviceID,$deviceModel,$OS,$OSVersion,$deviceType,$phoneNumber,$papTapVersion,$deviceOrient,$long,$lati,$lang,$category,$subcategory,$bizid,$mod_id,$level_no,$parent,$in_favorites,$country,$state);


echo "1";

?>