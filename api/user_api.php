<?
require("../admin/libs/apiClass.php");

$apiClass = new apiClass($_REQUEST);

$action = $_REQUEST["action"];

switch ($action)
{
    case "initialBizCall":
        echo $apiClass->initialBizCall($_REQUEST);
        break;

    case "setCustomerHistory":         
        $apiClass->setCustomerHistory($_REQUEST);        
        break;

    case "setUser":         
        $apiClass->setUserExecute($_REQUEST);        
        break;

    case "setWishList":         
        $apiClass->setWishList($_REQUEST);        
        break;

    case "getPointsShop":         
        echo $apiClass->getPointsShop($_REQUEST);        
        break;

    case "claimFromPointsShop":         
        echo $apiClass->claimFromPointsShop($_REQUEST);        
        break;

    case "unclaimFromPointsShop":         
        echo $apiClass->unclaimFromPointsShop($_REQUEST);        
        break;

    case "getInstagram":         
        echo $apiClass->getInstagram($_REQUEST);        
        break;

    case "claimScratchReward":
        echo $apiClass->claimScratchReward($_REQUEST); 
        break;

    case "getCustomerScratchCard":
        echo $apiClass->getCustomerScratchCard($_REQUEST); 
        break;

    case "justRegistered":
        $apiClass->justRegistered($_REQUEST); 
        break;

    case "getBiz":
    
        $apiClass->getBiz($_REQUEST); 
        break;

    case "getBizFull":
        $apiClass->getBizFull($_REQUEST); 
        break;

    case "getLevel":
        $json = json_encode($apiClass->getLevel($_REQUEST));
        echo $json;
        break;

    case "setReview":
        $apiClass->setReview($_REQUEST); 
        break;

    case "getReviews":
        $apiClass->getReviews($_REQUEST); 
        break;

    case "getSingleReview":
        $apiClass->getSingleReview($_REQUEST); 
        break;

    case "getPointsHistory":
        $apiClass->getPointsHistory($_REQUEST); 
        break;

    case "zeroCustomerBadge":
        $apiClass->zeroCustomerBadge($_REQUEST); 
        break;

    case "subtractBadge":
        $apiClass->subtractBadgeCounter($_REQUEST);
        break;

    case "cancelSubscription":
        
        echo $apiClass->customerCancelSubscription($_REQUEST);
        
        break;

    case "setPrimarySubscription":
        echo $apiClass->setPrimaryCustomerSubscription($_REQUEST);
        break;

    case "setPrimaryPunchpass":
        echo $apiClass->setPrimaryCustomerPunchpass($_REQUEST);
        break;

    case "getCustomerPrimaryMultiuse":
        echo json_encode($apiClass->getWidgetCustomerPrimaryMultiuseItem($_REQUEST));
        break;

    case "getSubscriptionToUseForService":
        echo json_encode($apiClass->getSubscriptionToUseForService($_REQUEST));
        break;
    case "getSubscriptionToUseForClass":
        echo json_encode($apiClass->getSubscriptionUseForClass($_REQUEST));
        break;
    case "getSubscriptionToUseForProduct":
        echo json_encode($apiClass->getSubscriptionUseForProduct($_REQUEST));
        break;
    case "useCustomerSubscriptionOnProduct":
        echo json_encode($apiClass->useCustomerSubscriptionForProduct($_REQUEST));
        break;
    case "useCustomerPunchPassOnProduct":
        echo json_encode($apiClass->useCustomerPunchPassForProduct($_REQUEST));
        break;
    case "checkItemInCustomerMultiuse":
        echo json_encode($apiClass->chekIteminCustomerMultiuse($_REQUEST));
        break;
    case "validateMultiuseInCheckout":
        
        echo json_encode($apiClass->validateMultiuseInCartCheckout($_REQUEST));
        break;
    case "usedGeoSubscription":
        echo json_encode($apiClass->usedGeoSubscription($_REQUEST));
        break;
    case "usedGeoPunchPass":
        echo json_encode($apiClass->usedGeoPunchPass($_REQUEST));
        break;
}

?>