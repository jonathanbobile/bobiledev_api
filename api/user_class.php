<?
require("../admin/libs/apiClass.php");

$apiClass = new apiClass($_REQUEST);

$action = isset($_REQUEST["action"]) ? $_REQUEST["action"] : "getClasses";

switch ($action)
{
    case "getClasses":         
        echo $apiClass->getClasses();        
        break;
        
	case "getClassesDates":       
        $classId = $_REQUEST["classId"];        
        echo $apiClass->getClassDates($classId);        
        break;   

    case "setClassesDates":         
        echo $apiClass->setClassDates($_REQUEST);        
        break;

    case "checkClassDates":       
        echo $apiClass->checkClassDates($_REQUEST);        
        break;

}

?>