<?php

class Api extends Controller{

    function __construct(){
        parent::__construct();
        set_time_limit(0);
    }
    
    function index(){

    }

    function register_apns(){
        require_once('../'.MVC_NAME.'/libs/APNS/apns.php');
        new Apns($_GET);
    }

    function get_invite_short_url($bizId,$customerId){
        $firstGroup = mt_rand(100000, 999999);
        $secondGroup = mt_rand(1000, 9999);
        $thirdGroup = mt_rand(10000, 99999);
        $parameter = "$firstGroup-$bizId-$secondGroup-$customerId-$thirdGroup";
        $domain = utilityManager::getWhiteLabelURLForBiz($bizId);
        $longUrl = $domain."/client/invite_landing_page.php?SESS=$parameter";
        echo utilityManager::getShortOneForLong($longUrl);
    }

    function get_share_short_url($bizId){
        $firstGroup = mt_rand(100000, 999999);
        $secondGroup = mt_rand(10000, 99999);
        $parameter = "$firstGroup-$bizId-$secondGroup";
        $domain = utilityManager::getWhiteLabelURLForBiz($bizId);
        $longUrl = $domain."/qr/?SESS=$parameter";  
        echo utilityManager::getShortOneForLong($longUrl);
    }

    function get_google_storage_keys(){
        
        $response = array();

        $response["apiKey"] = "AIzaSyALisWLTLwuZFRyZSaoAk5Z9OvYU7f78OQ";
        $response["googleClientId"] = "434865682562-ntlgp59dqll58bo604daesapqsso0q67.apps.googleusercontent.com";
        $response["googleClientSecret"] = "18y_Wp8W3-rE61UWYcpH0Rew";
        $response["googleCode"] = "4/TWiyhMlNL77eaQ51ZFSAvfeHiIaViHgOg7Vj1MCQku8";
        $response["googleAccessToken"] = "ya29.GltGBZhkBNJ95EAcQueTFBL_XlXFAbkO6H7mgKSo1cO0wUxPSCtStn-KVewLI13cy9uTA_v2dqwyP9NXosWmjaEA98XmQQcgda21UazxGklTXXg9Yemg9exAhYjU";
        $response["googleRefreshToken"] = "1/mG0rovMvm5bVUF-EyCYVMvNenRw9YWOoXqi1j63JacQ";

        echo json_encode($response);
    }
  
}
