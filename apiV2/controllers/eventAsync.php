<?php

/**
 * eventAsync short summary.
 *
 * eventAsync description.
 *
 * @version 1.0
 * @author JonathanM
 */

//require ROOT_PATH.'libs/Ratchet/vendor/autoload.php';
//use Ratchet\Server\IoServer;
//use Ratchet\Http\HttpServer;
//use Ratchet\WebSocket\WsServer;
//require_once 'Chat.php';



class eventAsync extends Controller
{
    function __construct()
    {
        parent::__construct(true);
        
    }

    public function triggerAction($cust_action_id){
       
        $eventManager = new eventManager();

        $eventManager->actionTriggered($cust_action_id);

    }

    public function triggeringAction(){
        $eventManager = new eventManager();
        
        $actionTriggerObject = actionTriggerObject::withData($_REQUEST);

        
        
        $eventManager->actionTriggering($actionTriggerObject);
    }

    public function triggerCustomerStageCheck($cust_id){

        $customersModel = new customerModel();
        $customersModel->checkCustomerStage($cust_id);
    }

    public function grantInviterPoints(){
        $friendID = $_REQUEST['friend_id'];
        $amount = $_REQUEST['amount'];
       
        $customerMod = new customerModel();

        $customerMod->grantInviterPointsExecute($friendID,$amount);
        
    } 
    
    public function sendInstantMessage(){
       
        $message = json_decode(urldecode($_POST['message']));
       
        $target = $_POST['receiver'];
        $type = $_POST['message_type'];
        $postFallback = isset($_POST['fallback']) ? json_decode($_POST['fallback']) : null;

        if(isset($postFallback)){
            $fallback = new pushParamsObject();
            foreach($postFallback as $property => $value) { 
                $fallback->$property = $value; 
            } 
        }
        else{
            $fallback = null;
        }

        wsocketManager::sendIM($message,$target,$type,$fallback);
    }

    public function sendInstantClientChat(){
        $chatID = $_POST['chatID'];
        $customerID = $_POST['customerID'];
        $deviceID = $_POST['deviceID'];
        $customerModel = new customerModel();

        $customerResult = $customerModel->getCustomerWithID($customerID);

        if($customerResult->code != 1){
            return;
        }
        $customer = $customerResult->data;

        $chatManager = new chatManager();

        $chatResult = $chatManager->getChatMessageByID($chatID);
        if($chatResult->code != 1){
            return;
        }

        $chat = $chatResult->data;

        $socketMessage = new stdClass();
        $socketMessage->chat_message = $chat->ch_message;
        $receiver = $chat->ch_user."_".$chat->ch_customer."_chat_listener";
        $wsendResult = wsocketManager::sendIM($socketMessage,$receiver,enumSocketType::client_chat_message);

        if($wsendResult != 1){//user did not get message through socket, so send notifications
            $extraParams["chat_id"] = $chatID;
            $extraParams["customer_id"] = $customerID;
            $extraParams["customer_device_id"] = $deviceID;
            pushManager::sendPushAdmin($customer->cust_biz_id,enumPushType::admin_chat,$chat->ch_message,$extraParams);

            $notifications = new notificationsManager();
            $params["customer_id"] = $customerID;
            $notifications->addNotification($customer->cust_biz_id,enumActions::newCatMessage,false,$params);

            $params["messID"] = $chatID;
            utilityManager::asyncTask(PUSH_SERVER."/admin/autojobs/sendpushChat",$params);

            $customer->cust_unread_chats = $customer->cust_unread_chats + 1;
            $customerModel->updateCustomer($customer);
        }

    }

    public function logSocketConnection(){
        $identity = $_REQUEST['identity'];
        $connection_id = $_REQUEST['connection_id'];

        $wsManager = new wsocketManager();

        $log_id = $wsManager->logConnection($identity,$connection_id);

        header('Content-Type: application/json');
        utilityManager::echoJavaScriptResponseArray(1,'',$log_id);
    }

    public function deleteSocketConnection($socket_conn_id){
        

        $wsManager = new wsocketManager();

        $wsManager->deleteConnection($socket_conn_id);
    }

    public function updateCustomerAddressByLocation(){

        $geoData = utilityManager::getAddresByLongAndLati($_REQUEST["lati"],$_REQUEST["long"]);
        if(isset($geoData["country_id"]) || isset($geoData["state_id"]) || isset($geoData["city"])){
            $customerModel = new customerModel();
            $resultCustomer = $customerModel->getCustomerWithID($_REQUEST["custId"]);

            if($resultCustomer->code == 1){
                if(isset($geoData["country_id"])){
                    $resultCustomer->data->cust_country = $geoData["country_id"];
                }
                if(isset($geoData["state_id"])){
                    $resultCustomer->data->cust_state = $geoData["state_id"];
                }
                if(isset($geoData["city"])){
                    $resultCustomer->data->cust_city = $geoData["city"];
                }
                $customerModel->updateCustomer($resultCustomer->data);
            }
        }

    }

    public function updateCustomerAddressByIP(){

        $geoData = utilityManager::getUsersAddress($_REQUEST['ip']);
        
        if($geoData['statusCode'] == 'OK'){
            $customerModel = new customerModel();
            $resultCustomer = $customerModel->getCustomerWithID($_REQUEST["custID"]);
            
            if($resultCustomer->code == 1){
                $customer = $resultCustomer->data;
                $customer->cust_city = $geoData['cityName'];
                $customer->cust_country = utilityManager::getCountryId($geoData['countryCode']);
                $customer->cust_state = utilityManager::getStateIdFromName($geoData['regionName']);
                $customerModel->updateCustomer($customer);
            }
        }

    }

    public function sendAsyncPersonalZoneData(){
        $type = $_REQUEST['type'];
        $custID = $_REQUEST['cust_id'];
       
        $this->customersModel = new customerModel();
        $customerResult = $this->customersModel->getCustomerWithID($custID);
        if($customerResult->code != 1){
            return;
        }
        $customer = $customerResult->data;

        $devices = customerManager::getAllCustomerDeviceIDs($custID);

        foreach ($devices as $device)
        {
            $deviceID = $device['device_id'];

            $this->mobileRequestObject = new mobileRequestObject();
            $this->mobileRequestObject->bizid = $customer->cust_biz_id;
            $this->mobileRequestObject->deviceID = $device['device_id'];
            $this->mobileRequestObject->server_customer_id = $custID;
            $this->mobileRequestObject->android = $device['type'] == 'Android' ? 1 : 0;

            $dataToSend = array();
            
            $socketType = "";
            switch($type){
                case "meetings":
                    if(bizManager::hasPersonalZoneElement('appointments',$this->mobileRequestObject->bizid)){
                        $socketType = 'Booking';
                        $meetingsListResult = $this->customersModel->getAllCustomersUpcomingAppointments($customer);
                        
                        $dataToSend['featured'] = array();
                        if($meetingsListResult->code == 1){
                            foreach ($meetingsListResult->data as $meeting)
                            {
                                $entry = $meeting->PersonalZoneAPIArray();
                                $dataToSend['featured'][] = $entry;
                            }                
                        }
                    }

                    break;
                case "push":
                    if(bizManager::hasPersonalZoneElement('notifications',$this->mobileRequestObject->bizid)){
                        $socketType = 'Push';
                        $dataToSend['featured'] = array();
                        $lastpushid = isset($_REQUEST["lastpushid"]) ? $_REQUEST["lastpushid"] : 0;
                        $lastreminderid = isset($_REQUEST["lastreminderid"]) ? $_REQUEST["lastreminderid"] : 0;
                        $pushesResult = $this->customersModel->getCustomerPushes($customer,$lastpushid,$this->mobileRequestObject->deviceID,$lastreminderid);
                        
                        if($pushesResult->code == 1){
                            foreach ($pushesResult->data as $push)
                            {
                                $dataToSend['featured'][] = $push->PersonalZoneAPIArray();
                            }
                            
                        }
                    }
                    break;
                case "orders":
                    if(bizManager::hasPersonalZoneElement('orders',$this->mobileRequestObject->bizid)){
                        $socketType = 'Orders';
                        $ordersResult = customerOrderManager::getCustomerPersonalZoneOrders($customer,'eCommerce');
                        

                        if($ordersResult->code == 1){
                            foreach ($ordersResult->data as $orderObj)
                            {
                                $order = $orderObj->PersonalZoneAPIArray();
                                $dataToSend[] = $order;
                            }
                        }
                    }
                    break;
                case "benefits":
                    
                    if(bizManager::hasPersonalZoneElement('benefits',$this->mobileRequestObject->bizid)){
                        $socketType = 'Benefits';
                        $benefitsResult = $this->customersModel->getCustomerBenefitsForPersonalZone($customer);
                        if($benefitsResult->code == 1){
                            if($benefitsResult->code == 1){
                                foreach ($benefitsResult->data as $benefit)
                                {                                    
                                    $dataToSend[] = $benefit->PersonalZoneAPIArray($this->mobileRequestObject);
                                }
                            }
                        }
                    }
                    break;
                case "requests":
                    if(bizManager::hasPersonalZoneElement('requests',$this->mobileRequestObject->bizid)){
                        $socketType = 'Requests';
                        $ordersResult = customerOrderManager::getCustomerPersonalZoneOrders($customer,'paymentRequest');
                        

                        if($ordersResult->code == 1){
                            foreach ($ordersResult->data as $orderObj)
                            {
                                $order = $orderObj->PersonalZoneAPIArray();
                                $dataToSend[] = $order;
                            }
                        }
                    }
                    break;
                case "documents":
                    if(bizManager::hasPersonalZoneElement('documents',$this->mobileRequestObject->bizid)){
                        $socketType = 'Documents';
                        $documentsResult = $this->customersModel->getCustomerDocuments($customer);
                        

                        if($documentsResult->code == 1){
                            foreach ($documentsResult->data as $doc)
                            {
                                $dataToSend[] = $doc->PersonalZoneAPIArray($this->mobileRequestObject);
                            }
                            
                        }
                    }
                    break;
                case "wishlist":
                    $wishlistResult = $this->customersModel->getCustomerWishlist($customer);
                    $socketType = 'Wishlist';
                    if($wishlistResult->code == 1){
                        foreach ($wishlistResult->data as $wish)
                        {
                            $dataToSend[] = $wish->PersonalZoneAPIArray();
                        }
                    }
                    break;
                case "friends":
                    if(bizManager::hasPersonalZoneElement('invitefriends',$this->mobileRequestObject->bizid)){
                        $socketType = 'Friends';
                        $friendsResult = $this->customersModel->getCustomerInvitedFriends($customer);
                        
                        if($friendsResult->code == 1){
                            foreach ($friendsResult->data as $friend)
                            {
                                $dataToSend[] = $friend->getArrayForFriendList();
                            }
                            
                        }
                    }
                    break;
                case "subscriptions":
                    if(bizManager::hasPersonalZoneElement('subscriptions',$this->mobileRequestObject->bizid)){
                        $socketType = 'Subscriptions';
                        $subscriptionsResult = $this->customersModel->getCustomerSubscriptions($customer);
                        
                        if($subscriptionsResult->code == 1){
                            $dataToSend = $subscriptionsResult->data;
                        }
                    }
                    break;
                case "punchpasses":
                    if(bizManager::hasPersonalZoneElement('punchpass',$this->mobileRequestObject->bizid)){
                        $socketType = 'Punchpasses';
                        $punch_passesResult = $this->customersModel->getCustomerPunchpasses($customer);
                        
                        if($punch_passesResult->code == 1){
                            $dataToSend = $punch_passesResult->data;
                        }
                    }
                    break;
                case "multiuseitems":
                    $punch_passesResult = $this->customersModel->getCustomerPunchpasses($customer);
                    $socketType = 'Multiuse';
                    if($punch_passesResult->code == 1){
                        $response["multiuse"] = array_merge($dataToSend,$punch_passesResult->data);
                    }
                    $subscriptionsResult = $this->customersModel->getCustomerSubscriptions($customer);
                    if($subscriptionsResult->code == 1){
                        $response["multiuse"] = array_merge($dataToSend,$subscriptionsResult->data);
                    }
                    break;
                case "points":
                    $socketType = 'PointsHistory';
                    $dataToSend = array();                   

                    $entriesResult = $this->customersModel->getPointsHistoryForCustomer($customer,0,0);

                    if($entriesResult->code == 1){
                        foreach ($entriesResult->data as $entry)
                        {
                            $dataToSend[] = $entry->getAPIFormattedArray();
                        }                
                    }
                    break;
                case "membership":
                    $socketType = 'Membership';
                    $dataToSend = $customer->membership;
                    break;
                case "badges":
                    $socketType = 'Badges';
                    $dataToSend = array();
                    $dataToSend['badges'] = $this->customersModel->getCustomerBadges($customer);
                    break;
            }
            if(count($dataToSend) == 0 || $socketType == ''){
                return;
            }        

            $sendType = 'client'.$socketType.'Data';
            
            if($device['type'] == 'web'){
                $target = $deviceID;
            }
            else{
                $target = $custID.'_'.$deviceID;
            }
            
            wsocketManager::sendIM($dataToSend,$target,$sendType);
        }
        return;
    }

    public function processOrder(){
        

        customerOrderManager::processCustOrder($_REQUEST['order_id']);
    }
}
