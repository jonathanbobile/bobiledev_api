<?php

/**
 * accountManager short summary.
 *
 * accountManager description.
 *
 * @version 1.0
 * @author Dany
 */
class accountManager extends Manager
{
    function __construct()
    {
        parent::__construct();        
    }

    /**
     * Get account object by ID
     * * Return Data = account object
     * @param int $accountId 
     * @return resultObject
     */
    public function getAccountById($accountId){

        try{
            $accountData = $this->getAccountByIdDB($accountId);
            $accountObject = accountObject::withData($accountData);
            return resultObject::withData(1,"Ok",$accountObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Update account in DB
     * @param accountObject $accountObject 
     * @return resultObject
     */
    public function updateAccount(accountObject $accountObject){
        
        try{
            $this->updateAccountDB($accountObject);
            return resultObject::withData(1,"Ok");
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }


    /**
     * Get account object by ResellerID and Email
     * * Return Data = account object
     * @param int $resellerId 
     * @param string $email 
     * @return resultObject
     */
    public function getAccountByPartnerIdAndEmail($resellerId,$email){

        try{
            $accountData = $this->getAccountByPartnerIdAndEmailDB($resellerId,$email);
            if(is_array($accountData)){
                $accountObject = accountObject::withData($accountData);
                return resultObject::withData(1,"Ok",$accountObject);
            }else{
                return resultObject::withData(0,"No Data");
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Get account object by Client Token
     * * Return Data = account object
     * @param int $clientToken 
     * @return resultObject
     */
    public function getAccountByClientToken($clientToken){

        try{
            $accountData = $this->getAccountByClientTokenDB($clientToken);
            $accountObject = accountObject::withData($accountData);
            return resultObject::withData(1,"Ok",$accountObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /*------------------------- PRIVATE FUNCTIONS --------------------------*/

    private function getAccountByIdDB($accountId){

        if (!is_numeric($accountId) || $accountId <= 0){
            throw new Exception("Illegal value accountId");             
        }

        return $this->db->getRow("SELECT * FROM tbl_account WHERE ac_id = $accountId");
    }

    private function updateAccountDB(accountObject $accountObject){
        
        if (!isset($accountObject->ac_id) || !is_numeric($accountObject->ac_id) || $accountObject->ac_id <= 0){
            throw new Exception("accountObject value must be provided");             
        }

        $sql = "
                UPDATE tbl_biz SET
                    ac_type = '{$accountObject->ac_type}',
                    ac_username = '".addslashes($accountObject->ac_username)."',
                    ac_reseller_id = {$accountObject->ac_reseller_id},
                    ac_reseller_client_token = '".addslashes($accountObject->ac_reseller_client_token)."',
                    ac_name = '".addslashes($accountObject->ac_name)."',
                    ac_password = '".addslashes($accountObject->ac_password)."',
                    ac_isverified = {$accountObject->ac_isverified},
                    ac_isactive = {$accountObject->ac_isactive},
                    ac_lang = '".addslashes($accountObject->ac_lang)."',
                    ac_country = {$accountObject->ac_country},
                    ac_phone = '".addslashes($accountObject->ac_phone)."',
                    ac_phone_valid = {$accountObject->ac_phone_valid},
                    ac_facebook_id = '".addslashes($accountObject->ac_facebook_id)."',
                    ac_facebook_token = '".addslashes($accountObject->ac_facebook_token)."',
                    ac_conf_code = '".addslashes($accountObject->ac_conf_code)."',
                    ac_unsub = {$accountObject->ac_unsub},
                    ac_last_bizid = {$accountObject->ac_last_bizid},
                    ac_environment = '{$accountObject->ac_environment}',
                    ac_wizard_status = '{$accountObject->ac_wizard_status}',
                    ac_need_change_pass = {$accountObject->ac_need_change_pass}
                    WHERE ac_id = {$accountObject->ac_id}
        ";

        $this->db->execute($sql);
    }

    private function getAccountByPartnerIdAndEmailDB($resellerId,$email){

        if (!is_numeric($resellerId) || $resellerId <= 0){
            throw new Exception("Illegal value for reseller ID $resellerId");             
        }

        if (!isset($email) || $email == ""){
            throw new Exception("Illegal value for Email $email");             
        }

        return $this->db->getRow("SELECT * FROM tbl_account WHERE ac_username = '$email' AND ac_reseller_id = $resellerId LIMIT 1");
    }

    private function getAccountByClientTokenDB($clientToken){

        if (!isset($clientToken)){
            throw new Exception("Illegal value clientToken");             
        }

        return $this->db->getRow("SELECT * FROM tbl_account WHERE ac_reseller_client_token = '$clientToken'");
    }

    /************************************* */
    /*   BASIC GUEST - PUBLIC           */
    /************************************* */

    /**
    * Insert new guest to DB
    * Return Data = new guest ID
    * @param guest $guestObj 
    * @return resultObject
    */
    public function addGuest(guest $guestObj){       
            try{
                $newId = $this->addGuestDB($guestObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($guestObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get guest from DB for provided ID
    * * Return Data = guest
    * @param int $guestId 
    * @return resultObject
    */
    public function getGuestByID($guestId){
        
            try {
                $guestData = $this->loadGuestFromDB($guestId);
            
                $guestObj = guest::withData($guestData);
                $result = resultObject::withData(1,'',$guestObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$guestId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update guest in DB
    * @param guest $guestObj 
    * @return resultObject
    */
    public function updateGuest(guest $guestObj){        
            try{
                $this->upateGuestDB($guestObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($guestObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete guest from DB
    * @param int $guestID 
    * @return resultObject
    */
    public function deleteGuestById($guestID){
            try{
                $this->deleteGuestByIdDB($guestID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$guestID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC GUEST - DB METHODS           */
    /************************************* */

    private function addGuestDB(guest $obj){

    if (!isset($obj)){
                throw new Exception("guest value must be provided");             
            }

    $newId = $this->db->execute("INSERT INTO tbl_guests SET 
                                    gu_account_id = {$obj->gu_account_id},
                                    gu_name = '".addslashes($obj->gu_name)."',
                                    gu_biz_id = {$obj->gu_biz_id},
                                    gu_isactive = {$obj->gu_isactive},
                                    gu_invite_token = '".addslashes($obj->gu_invite_token)."',
                                    gu_verify_code = '".addslashes($obj->gu_verify_code)."',
                                    gu_isrevoked = {$obj->gu_isrevoked}
                                             ");
             return $newId;
        }

    private function loadGuestFromDB($guestID){

    if (!is_numeric($guestID) || $guestID <= 0){
                throw new Exception("Illegal value guestID");             
            }

            return $this->db->getRow("SELECT * FROM tbl_guests WHERE gu_id = $guestID");
        }

    private function upateGuestDB(guest $obj){

    if (!isset($obj->gu_id) || !is_numeric($obj->gu_id) || $obj->gu_id <= 0){
                throw new Exception("guest value must be provided");             
            }

    $gu_biz_invite_dateDate = isset($obj->gu_biz_invite_date) ? "'".$obj->gu_biz_invite_date."'" : "null";

    $this->db->execute("UPDATE tbl_guests SET 
                            gu_account_id = {$obj->gu_account_id},
                            gu_name = '".addslashes($obj->gu_name)."',
                            gu_biz_id = {$obj->gu_biz_id},
                            gu_isactive = {$obj->gu_isactive},
                            gu_biz_invite_date = $gu_biz_invite_dateDate,
                            gu_invite_token = '".addslashes($obj->gu_invite_token)."',
                            gu_verify_code = '".addslashes($obj->gu_verify_code)."',
                            gu_isrevoked = {$obj->gu_isrevoked}
                            WHERE gu_id = {$obj->gu_id} 
                                     ");
        }

    private function deleteGuestByIdDB($guestID){

            if (!is_numeric($guestID) || $guestID <= 0){
                throw new Exception("Illegal value guestID");             
            }

            $this->db->execute("DELETE FROM tbl_guests WHERE gu_id = $guestID");
        }
}
