<?php

/**
 * bizManager short summary.
 *
 * bizManager description.
 *
 * @version 1.0
 * @author Dany
 */
class bizManager extends Manager
{

    function __construct()
    {
        parent::__construct(); 
    }

    public static function hasPremiumFeature($featureId,$bizId = ""){

        if($featureId == 0) return true;
        if($bizId == "") return false;

        $instance = new self();

        $purchasedAsSingleFeature = $instance->db->getVal("SELECT COUNT(tf_id) volume
                                                            FROM tbl_transaction_feature WHERE tf_biz_id = $bizId
                                                            AND tf_entity = 'module'
                                                            AND tf_entity_id in ($featureId)
                                                            AND tf_end_time IS NULL");

        $purchasedInBundle = $instance->db->getVal("SELECT COUNT(tf_id) volume FROM tbl_transaction_feature, tbl_plans , tbl_bundle_features
                                                            WHERE tf_biz_id = $bizId
                                                            AND tf_entity = 'plan'
                                                            AND tf_end_time IS NULL
                                                            AND tf_entity_id = pl_id
                                                            AND pl_entity = 'bundle'
                                                            AND pl_entity_id = fg_bundle_id
                                                            AND fg_entity = 'module'
                                                            AND fg_entity_id in ($featureId)");

        $conectedModulus = $instance->db->getVal("SELECT count(biz_mod_id) FROM tbl_biz_mod WHERE biz_mod_biz_id = $bizId and biz_mod_mod_id in ($featureId)");

        if(($purchasedAsSingleFeature + $purchasedInBundle + $conectedModulus) > 0){
            return true;
        }

        return false;
    }

    public static function getAppOwnerLang($biz_id){
        $instance = new self();
        $sql = "SELECT * FROM tbl_biz,tbl_account,tbl_owners
                LEFT JOIN tbl_resellers ON reseller_id = owner_reseller_id
                WHERE owner_id = biz_owner_id
                AND ac_id = owner_account_id
                AND biz_id = $biz_id";

        $bizRow = $instance->db->getRow($sql);

        if($bizRow['owner_reseller_id'] == 0){
            return $bizRow['ac_lang'];
        }

        $sql = "SELECT * FROM tbl_client_biz,tbl_reseller_clients,tbl_account
                WHERE client_biz_client_id = reseller_client_id
                AND reseller_client_account_id = ac_id
                AND client_biz_biz_id = $biz_id";

        $clientRow =  $instance->db->getRow($sql);

        if(isset($clientRow['ac_lang'])){
            return $clientRow['ac_lang'];
        }

        return $bizRow['ac_lang'];
    }
    
    public static function getLanguageCodeFromPrefix($prefix){
        $instance= new self();

        $sql = "SELECT LNG_CODE FROM tbl_languges WHERE LNG_PREFIX = '$prefix'";

        return $instance->db->getVal($sql);
    }

    public static function getOwnerIdByBizId($biz_id){
        $instance = new self();
        $sql = "SELECT owner_id FROM tbl_owners,tbl_biz
                WHERE biz_owner_id = owner_id
                AND biz_id = $biz_id";

        return $instance->db->getVal($sql);
    }

    public static function getAppOwnerCountry($biz_id){
        $instance = new self();

        $bizRow = self::getOwnerOrResellerFromBizID($biz_id);

        if($bizRow['owner_reseller_id'] == 0){
            return $bizRow['owner_country'];
        }

        $sql = "SELECT * FROM tbl_client_biz,tbl_reseller_clients
                WHERE client_biz_client_id = reseller_client_id
                AND client_biz_biz_id = $biz_id";

        $clientRow =  $instance->db->getRow($sql);

        if(isset($clientRow['reseller_client_country'])){
            return $clientRow['reseller_client_country'];
        }

        return $bizRow['owner_country'];
    }

    public static function getBizCountryID($biz_id){
        $instance = new self();

        $sql = "SELECT biz_addr_country_id FROM tbl_biz WHERE biz_id = $biz_id";

        return $instance->db->getVal($sql);
    }

    public static function checkIfBizHasStripe($biz_id){ 
        require_once('../'.MVC_NAME.'/libs/firebase.php');
        require_once('../'.MVC_NAME.'/libs/stripeConnectManager.php');

        $fbase = new firebase();
        $stripeData = $fbase->getDocument("businesses",$biz_id);
        if($stripeData['code'] != 0 && $stripeData["data"]["stripe_account_id"] != ""){

            if(isset($stripeData["data"]["ENVIRONMENT"]) && $stripeData["data"]["ENVIRONMENT"] == "SANDBOX"){
                $stripeAccount = stripeConnectManager::getConnectAccountSandbox($stripeData["data"]["stripe_account_id"]);
            }else{
                $stripeAccount = stripeConnectManager::getConnectAccount($stripeData["data"]["stripe_account_id"]);
            }

            return $stripeAccount["charges_enabled"];
        }

        return false;
    }

    public static function getAndroidVersionDataForBiz($biz_id){ 
        $instance = new self();

        $bizRow = $instance->db->getRow("SELECT * FROM tbl_biz WHERE biz_id=$biz_id");
        $versionRow = array();

        $response['bizApkUpdateType'] = enumApkUpdateType::no_update_needed;
        $response['bizApkUpdateLocation'] = "";
        $response['bizApkUpdateUrl'] = "";
        $response['bizApkVersion'] = 0;
        $response['hasGooglePlay'] = 0;

        $haveGooglePlay = ($bizRow["biz_goog_status"] == 1 || $bizRow["biz_goog_status"] == 2);

        if($haveGooglePlay){
            $bizVersionNumber = $bizRow["biz_sbmt_goo_version"] == "" ? 0 : $bizRow["biz_sbmt_goo_version"];
            if($bizVersionNumber != 0){
                $versionRow = $instance->db->getRow("select * from tbl_release_version where rv_last_android_version_play=$bizVersionNumber");
            }

            $response['bizApkVersion'] = $bizVersionNumber;
            $response['bizApkUpdateLocation'] = enumApkUpdateLocation::google_play;
            $response['hasGooglePlay'] = 1;
        }else{
            $bizVersionNumber = $bizRow["biz_sbmt_goo_build_version_num"] == "" ? 0 : $bizRow["biz_sbmt_goo_build_version_num"];
            if($bizVersionNumber != 0){
                $versionRow = $instance->db->getRow("select * from tbl_release_version where rv_last_android_version=$bizVersionNumber");
            }

            $response['bizApkVersion'] = $bizVersionNumber;
            $response['bizApkUpdateLocation'] = enumApkUpdateLocation::bobile_market;
            $folder = 14000000 + $bizRow["biz_id"];
            $response['bizApkUpdateUrl'] = "https://storage.googleapis.com/bobile/download/apk/{$folder}/approved_signed_version_{$bizVersionNumber}.apk";
        }

        if(isset($versionRow["rv_android_must_update"]) && $versionRow["rv_android_must_update"] == "1"){
            $response['bizApkUpdateType'] = enumApkUpdateType::must_update;
        }else{
            $response['bizApkUpdateType'] = enumApkUpdateType::have_update;
        }
        
        return $response;
    }

    public static function getIosVersionDataForBiz($biz_id,$bundleId){ 
        $instance = new self();

        $response['bizIpaUpdateType'] = enumApkUpdateType::no_update_needed;
        $response['bizIpaUpdateLocation'] = "";
        $response['bizIpaUpdateUrl'] = "";
        $response['bizIpaVersion'] = 0;

        if($bundleId == ""){
            return $response;
        }

        $bizRow = $instance->db->getRow("SELECT * FROM tbl_biz WHERE biz_id=$biz_id");
        $versionRow = array();

        $haveAppleStore = !utilityManager::contains($bundleId,"com.bb.bb");

        if($haveAppleStore){
            $bizVersionNumber = $bizRow["biz_sbmt_apl_version"] == "" ? 0 : $bizRow["biz_sbmt_apl_version"];
            if($bizVersionNumber != 0){
                $versionRow = $instance->db->getRow("select * from tbl_release_version where rv_last_ios_version_store=$bizVersionNumber");
            }

            $response['bizIpaVersion'] = $bizVersionNumber;
            $response['bizIpaUpdateLocation'] = enumApkUpdateLocation::apple_store;
            $response['bizIpaUpdateType'] = enumApkUpdateType::no_update_needed;

        }else{
            $bizVersionNumber = $bizRow["biz_sbmt_apl_build_version_num"] == "" ? 0 : $bizRow["biz_sbmt_apl_build_version_num"];
            if($bizVersionNumber != 0){
                $versionRow = $instance->db->getRow("select * from tbl_release_version where rv_last_ios_version_store=$bizVersionNumber");
                $folder = "/$bizVersionNumber";
            }else{
                $folder = "";
            }

            $response['bizIpaVersion'] = $bizVersionNumber;
            $response['bizIpaUpdateLocation'] = enumApkUpdateLocation::bobile_market;

            $ipaId = $biz_id + 14000000;
            $response['bizIpaUpdateUrl'] = "itms-services://?action=download-manifest&url=itms-services://?action=download-manifest&url=https://storage.googleapis.com/bobile/download/ipa/".$ipaId.$folder."/manifest.plist";

            if(isset($versionRow["rv_ios_must_update"]) && $versionRow["rv_ios_must_update"] == "1"){
                $response['bizIpaUpdateType'] = enumApkUpdateType::must_update;
            }else{
                $response['bizIpaUpdateType'] = enumApkUpdateType::have_update;
            }
        }

        return $response;
    }

    public static function checkBizHasAnyPaymentGateway($biz_id){
        $hasProcessors = self::hasAnyPaymentProcessors($biz_id);
        if(!$hasProcessors){
            $hasProcessors = self::checkIfBizHasStripe($biz_id);
        }

        return $hasProcessors;
    }

    public static function hasAnyPaymentProcessors($biz_id){
        $instance = new self();

        $sql = "SELECT COUNT(*) FROM tbl_biz_processors 
                    WHERE bp_biz_id = $biz_id                     
                    AND bp_active = 1";

        $procCount = $instance->db->getVal($sql);

        $fbase = new firebase();
        $result = $fbase->getDocument("businesses",$biz_id);
        if($result['code'] != 0){
            if($result["data"]["stripe_account_id"] != ""){
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    $stripeAccount = stripeConnectManager::getConnectAccountSandbox($result["data"]["stripe_account_id"]);
                }else{
                    $stripeAccount = stripeConnectManager::getConnectAccount($result["data"]["stripe_account_id"]);
                }
                if($stripeAccount["payouts_enabled"]){
                    $procCount++;
                }
            }
        }

        return $procCount > 0;
    }

    public static function isCountryAvailbleOnStripeByCountryId($countryId){

        $countryCode = self::getCountryCodeByCountryId($countryId);

        $instance = new self();
        return $instance->db->getVal("SELECT country_support_by_stripe FROM tbl_countries WHERE country_code='$countryCode'") == "1";
    }

    public static function getCountryCodeByCountryId($countryId){
        $instance = new self();
        return $instance->db->getVal("SELECT country_code FROM tbl_countries WHERE country_id = '$countryId'");
    }

    public static function modulesAreActive($mod_ids,$bizId = 0){
        $bizId = $bizId != 0 ? $bizId : $_SESSION["appData"]["biz_id"];
        $instance = new self();
        $sql = "SELECT COUNT(*) FROM tbl_biz_mod
                WHERE biz_mod_biz_id = $bizId
                AND biz_mod_mod_id IN ($mod_ids)
                AND biz_mod_active = 1";

        $count = $instance->db->getVal($sql);

        return $count > 0;
    }

    public static function getBizSmsHash($biz_id){
        $instance = new self();
        return $instance->db->getVal("SELECT biz_smshash FROM tbl_biz WHERE biz_id = $biz_id");
    }

    public static function getOwnerCurrency($biz_id)
    {
        $instance = new self();

        $response = $instance->getOwnerOrResellerFromBizID($biz_id);

        if($response["reseller_id"] == ""){
            return $instance->db->getVal("SELECT owner_currency_code FROM tbl_biz,tbl_owners WHERE biz_owner_id=owner_id and biz_id = $biz_id");
        }else{
            return $instance->getResellerCurrency($response["reseller_id"]);
        }
        
    }

    public static function getOwnerOrResellerFromBizID($biz_id){
        $instance = new self();
        $sql = "SELECT * FROM tbl_biz,tbl_owners LEFT JOIN tbl_resellers ON owner_reseller_id = reseller_id
                WHERE owner_id = biz_owner_id
                AND biz_id = $biz_id";

        return $instance->db->getRow($sql);
    }

    public static function getResellerIDForBiz($biz_id){
        $instance = new self();
        $sql = "SELECT owner_reseller_id FROM tbl_biz,tbl_owners
                WHERE owner_id = biz_owner_id
                AND biz_id = $biz_id";

        return $instance->db->getVal($sql);
    }

    public static function getResellerCurrency($reseller_id)
    {
        $instance = new self();
        $rCurrency = $instance->db->getVal("SELECT owner_currency_code FROM tbl_owners WHERE owner_reseller_id = $reseller_id");

        $fbase = new firebase();
        $result = $fbase->getDocument("partners",$reseller_id);
        if($result['code'] != 0){
            if($result["data"]["stripe_account_id"] != ""){
                $stripeAccount = stripeConnectManager::getConnectAccount($result["data"]["stripe_account_id"]);
                $accounts = $stripeAccount["external_accounts"]["data"];
                $defaultBankCurrency = stripeConnectManager::getDefaulBankAccountCurrency($accounts);
                if($defaultBankCurrency != ""){
                    $rCurrency = $defaultBankCurrency;
                }
            }
        }

        return $rCurrency;
    }

    public static function needUpdateApp($bizID)
    {        
        if($bizID != 0){
            $instance = new self();
            $instance->db->execute("update tbl_users_biz set usrbiz_need_update=1 where usrbiz_biz_id=$bizID");
        }
    }

    public static function isUserExistForBiz(userObject $userObject,$bizID)
    {        
        $instance = new self();
        return $instance->db->getVal("SELECT COUNT(usrbiz_user_id) FROM tbl_users_biz WHERE usrbiz_user_id={$userObject->usr_id} AND usrbiz_biz_id=$bizID") > 0;
    }

    public static function isUserNeedUpdate(userObject $userObject,$bizID)
    {        
        $instance = new self();
        return $instance->db->getVal("SELECT usrbiz_need_update FROM tbl_users_biz WHERE usrbiz_user_id={$userObject->usr_id} AND usrbiz_biz_id=$bizID");
    }

    public static function setUserUpdated(userObject $userObject,$bizID)
    {        
        $instance = new self();
        $instance->db->execute("UPDATE tbl_users_biz SET usrbiz_need_update=0 WHERE usrbiz_user_id={$userObject->usr_id} AND usrbiz_biz_id=$bizID");
    }

    public static function clearNeedUpdateAppForUser(userObject $userObject,$bizID)
    {        
        $instance = new self();
        $instance->db->execute("UPDATE tbl_users_biz SET usrbiz_need_update=0 WHERE usrbiz_user_id={$userObject->usr_id} AND usrbiz_biz_id=$bizID");
    }

    public static function addUser(userObject $userObject,$bizID)
    {        
        $instance = new self();
        $insert = "INSERT INTO tbl_users_biz SET usrbiz_user_id={$userObject->usr_id},
                                                    usrbiz_biz_id=$bizID,
                                                    usrbiz_resellerid={$userObject->usr_resellerid},
                                                    userbiz_marketid={$userObject->usr_marketid}";
        $instance->db->execute($insert);
    }

    public static function updateRowReview(customerReviewObject $review){
        $instance = new self();        

        $cr_biz_id = $review->cr_biz_id;
        $cr_mod_id = $review->cr_mod_id;
        $cr_row_id = $review->cr_row_id;
        $cr_item_type = $review->cr_item_type;
        $cr_item_id = $review->cr_item_id;

        if($cr_item_type == "" || $cr_item_type == "0"){
            $totalReviews = $instance->db->getVal("select count(cr_id) FROM tbl_customer_reviews 
                                            WHERE cr_biz_id = $cr_biz_id
                                            AND cr_mod_id = $cr_mod_id
                                            AND cr_item_type = '$cr_item_type'
                                            AND cr_row_id = $cr_row_id");

            $sumReviews = $instance->db->getVal("select sum(cr_rating) FROM tbl_customer_reviews 
                                            WHERE cr_biz_id = $cr_biz_id
                                            AND cr_mod_id = $cr_mod_id
                                            AND cr_item_type = '$cr_item_type'
                                            AND cr_row_id = $cr_row_id");

            $avarage = $totalReviews == 0 ? 0 : round($sumReviews / $totalReviews, 2);

            $instance->db->execute("update tbl_mod_data$cr_mod_id SET 
                                            md_price = $avarage,
                                            md_int1 = $totalReviews
                                            WHERE md_row_id = $cr_row_id");
        }else{
            $totalReviews = $instance->db->getVal("select count(cr_id) FROM tbl_customer_reviews 
                                            WHERE cr_item_id = $cr_item_id 
                                            AND cr_biz_id = $cr_biz_id
                                            AND cr_item_type = '$cr_item_type'");

            $sumReviews = $instance->db->getVal("select sum(cr_rating) FROM tbl_customer_reviews 
                                            WHERE cr_item_id = $cr_item_id 
                                            AND cr_biz_id = $cr_biz_id
                                            AND cr_item_type = '$cr_item_type'");

            $avarage = $totalReviews == 0 ? 0 : round($sumReviews / $totalReviews, 2);

            $sql = "SELECT * FROM tbl_modules WHERE mod_can_review = 1";

            $reviewedMods = $instance->db->getTable($sql);

            foreach ($reviewedMods as $mod){
            	$instance->db->execute("update tbl_mod_data{$mod['mod_id']} SET md_price = $avarage,md_int1 = $totalReviews WHERE md_biz_id = $cr_biz_id AND md_external_id = $cr_item_id AND md_external_type = '$cr_item_type'");
            }                      
        }

        $response["code"] = 1;
        $response["average"] = $avarage;
        $response["total"] = $totalReviews;

        return $response;
    }

    public static function deleteUser(userObject $userObject,$bizID)
    {        
        $instance = new self();
        $insert = "DELETE FROM tbl_users_biz WHERE usrbiz_user_id={$userObject->usr_id} AND usrbiz_biz_id=$bizID";
        $instance->db->execute($insert);
    }

    public static function getInstagramData($userId,$maxId){

        $url = "https://www.instagram.com/$userId/?__a=1";
        
        $json = file_get_contents($url);
        $userData = json_decode($json);
        if(!isset($userData->graphql->user->id)){
            return resultObject::withData(0,"no_data");
        }
        $user_id = $userData->graphql->user->id;
        $struct = array();

        if($maxId != ""){
            $url = "https://instagram.com/graphql/query/?query_id=17888483320059182&id=$user_id&first=12&after=$maxId";
            $json = file_get_contents($url);
            $struct = json_decode($json);
        }else{
            $struct = $userData;
        }

        if($maxId != ""){
            $data = $struct->data->user->edge_owner_to_timeline_media;
        }else{
            $data = $struct->graphql->user->edge_owner_to_timeline_media;
        }

        $result = array();

        $result['has_next_page'] = $data->page_info->has_next_page;
        $result['end_cursor'] = $data->page_info->has_next_page ? $data->page_info->end_cursor : '';

        $result['data'] = array();
        if(count($data->edges)>0){
            foreach ($data->edges as $edge)
            {
                $node = array();
                $node['is_video'] = $edge->node->is_video;
                $node['display_url'] = $edge->node->display_url;
                $node['status_type'] = $edge->node->is_video ? 'added_video' : '';
                $node['type'] = $edge->node->is_video ? 'video' : 'photo';
                $node['date'] = $edge->node->taken_at_timestamp;
                $node['created_time'] = $edge->node->taken_at_timestamp;
                $node['link'] = $edge->node->display_url;
                $node['full_name'] = $userData->graphql->user->full_name;
                $node['id'] = $userData->graphql->user->id;
                $node['profile_pic_url'] = $userData->graphql->user->profile_pic_url;
                if($maxId != ""){
                    $node['count'] = $edge->node->edge_media_preview_like->count;
                }else{
                    $node['count'] = $edge->node->edge_liked_by->count;
                }
                
                $node['object_id'] = $edge->node->id;
                $node['caption'] = isset($edge->node->edge_media_to_caption->edges[0]) ? $edge->node->edge_media_to_caption->edges[0]->node->text : "";
                $node['thumbnail_src'] = $edge->node->thumbnail_src;
                if($edge->node->is_video){
                    $code = $edge->node->shortcode;
                    $vidUrl = "https://www.instagram.com/p/$code/?__a=1";
                    $vidData = json_decode(file_get_contents($vidUrl));
                    $node['videoSource'] = $vidData->graphql->shortcode_media->video_url;
                }
                
                array_push($result['data'],$node);
            }
        }

        return resultObject::withData(1,"ok",$result);
    }

    public static function hasPersonalZoneElement($elementType,$bizId){

        $instance = new self();

        $personalZoneRow = $instance->db->getRow("SELECT tbpz_id,tbpz_mod_id FROM tbl_biz_personal WHERE tbpz_biz_id = $bizId AND tbpz_type = '$elementType' and tbpz_is_visible=1");
        if($personalZoneRow["tbpz_id"] == "" || !self::hasPremiumFeature($personalZoneRow['tbpz_mod_id'],$bizId)){
            return false;
        }
        return true;
    }

    public static function getViewTypeFromStructID($structID,$nib_name){
        $instance = new self();

        return $instance->db->getVal("select ms_view_type$nib_name from tbl_mod_struct where ms_mod_id=$structID");
    }

    public static function getBizThemeByBizID($bizID){
        
        try {
            $instance = new self();
            $bizThemeData = $instance->db->getRow("SELECT tbl_template.* FROM tbl_biz,tbl_template 
                                    WHERE tmpl_id = biz_theme
                                    AND biz_id = $bizID");
            
            $bizThemeObj = bizThemeObject::withData($bizThemeData);
            $result = resultObject::withData(1,'',$bizThemeObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function updateGcmDeviceLocation($gcmId){

        $instance = new self();
        $ip = utilityManager::get_real_IP();

        $locations = utilityManager::getUsersAddress($ip);

        if($locations["countryCode"] != ""){
            $countryCode = $locations["countryCode"];
            $countryName = addslashes($locations["countryName"]);
            $regionName = addslashes($locations["regionName"]);
            $cityName = addslashes($locations["cityName"]);
            $zipCode = addslashes($locations["zipCode"]);
            $latitude = $locations["latitude"];
            $longitude = $locations["longitude"];
            $timeZone = $locations["timeZone"];
            
            $instance->db->execute("UPDATE gsm_devices SET 
                                gsm_city='$cityName',
                                gsm_country_code='$countryCode',
                                gsm_country='$countryName',
                                gsm_zip='$zipCode',
                                gsm_long='$longitude',
                                gsm_lati='$latitude',
                                gsm_region='$regionName',
                                gsm_time_zone='$timeZone'
                                WHERE gsm_id=$gcmId");
        }
    }

    public static function deleteGcmDevice(mobileRequestObject $mobileRequestObject){

        $instance = new self();
        $sql = "DELETE FROM gsm_devices 
                    WHERE gsm_device_id='{$mobileRequestObject->deviceID}' 
                    AND gsm_appid={$mobileRequestObject->appid} 
                    AND gsm_reseller_id={$mobileRequestObject->resellerId} 
                    AND gsm_market_id={$mobileRequestObject->marketId}";

        $instance->db->execute($sql);
    }

    public static function addGcmDevice($token,mobileRequestObject $mobileRequestObject){

        $instance = new self();
        $sql = "INSERT INTO gsm_devices 
                        SET gsm_device_id='{$mobileRequestObject->deviceID}',
                        gsm_token='$token',
                        gsm_appid={$mobileRequestObject->appid},
                        gsm_reseller_id={$mobileRequestObject->resellerId},
                        gsm_market_id={$mobileRequestObject->marketId}";

        return $instance->db->execute($sql);
    }

    public static function getBizListForClient($clientId){

        $instance = new self();

        $sql = "SELECT * FROM tbl_biz,tbl_client_biz
                    WHERE biz_id = client_biz_biz_id
                    AND client_biz_client_id=$clientId";
   
        return $instance->db->getTable($sql);
    
    }

    public static function getBizWelcomeGrant($bizID){
        try{
            $instance = new self();

            $sql = "SELECT biz_welcome_grant FROM tbl_biz WHERE biz_id = $bizID";
            
            return $instance->db->getVal($sql);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            return 0;
        }
    }

    /************************************* */
    /*   BASIC BIZCALENDARSETTINGS - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizCalendarSettingsObject to DB
     * Return Data = new bizCalendarSettingsObject ID
     * @param bizCalendarSettingsObject $bizCalendarSettingsObj 
     * @return resultObject
     */
    public static function addBizCalendarSettings(bizCalendarSettingsObject $bizCalendarSettingsObj){       
        try{
            $instance = new self();
            $newId = $instance->addBizCalendarSettingsDB($bizCalendarSettingsObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizCalendarSettingsObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizCalendarSettings from DB for provided ID
     * * Return Data = bizCalendarSettingsObject
     * @param int $bizCalendarSettingsId 
     * @return resultObject
     */
    public static function getBizCalendarSettingsByID($bizCalendarSettingsId){
        
        try {
            $instance = new self();
            $bizCalendarSettingsData = $instance->loadBizCalendarSettingsFromDB($bizCalendarSettingsId);
            
            $bizCalendarSettingsObj = bizCalendarSettingsObject::withData($bizCalendarSettingsData);
            $result = resultObject::withData(1,'',$bizCalendarSettingsObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizCalendarSettingsId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizCalendarSettings in DB
     * @param bizCalendarSettingsObject $bizCalendarSettingsObj 
     * @return resultObject
     */
    public static function updateBizCalendarSettings(bizCalendarSettingsObject $bizCalendarSettingsObj){        
        try{
            $instance = new self();
            $instance->upateBizCalendarSettingsDB($bizCalendarSettingsObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizCalendarSettingsObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizCalendarSettings from DB
     * @param int $bizCalendarSettingsID 
     * @return resultObject
     */
    public static function deleteBizCalendarSettingsById($bizCalendarSettingsID){
        try{
            $instance = new self();
            $instance->deleteBizCalendarSettingsByIdDB($bizCalendarSettingsID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizCalendarSettingsID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Summary of getBizCalendarSettingsByBizID
     * @param mixed $bizID 
     * @return bizCalendarSettingsObject|null
     */
    public static function getBizCalendarSettingsByBizID($bizID){
        try{
            
            $instance = new self();

            $bizCalSetRow = $instance->loadBizCalendarSettingsFromDBByBizID($bizID);

           
            return bizCalendarSettingsObject::withData($bizCalSetRow);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            return null;
        }
    }

    public static function getAppToken($biz_id,$redirectUrl,$planIdForTrial = 0)
    {
        $instance = new self();
        $data = $instance->db->getRow("SELECT ac_username,ac_password 
                                       FROM tbl_account,tbl_owners,tbl_biz 
                                       WHERE biz_owner_id=owner_id
                                       AND owner_account_id = ac_id
                                       AND biz_id = $biz_id");

        $generated = $data['ac_username']."@|@".$data['ac_password']."@|@".$biz_id."@|@".$redirectUrl."@|@".$planIdForTrial;
        return utilityManager::encryptIt($generated,true);
    }

    /************************************* */
    /*   BASIC BIZCALENDARSETTINGS - DB METHODS           */
    /************************************* */

    private function addBizCalendarSettingsDB(bizCalendarSettingsObject $obj){

        if (!isset($obj)){
            throw new Exception("bizCalendarSettingsObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_cal_settings SET 
                                        bcs_biz_id = {$obj->bcs_biz_id},
                                        biz_cal_24h = {$obj->biz_cal_24h},
                                        biz_cal_display_start = {$obj->biz_cal_display_start},
                                        biz_cal_display_end = {$obj->biz_cal_display_end},
                                        biz_cal_default_color = {$obj->biz_cal_default_color},
                                        biz_cal_last_emp_id = {$obj->biz_cal_last_emp_id},
                                        biz_cal_last_emp_name = '".addslashes($obj->biz_cal_last_emp_name)."',
                                        biz_cal_def_employee = {$obj->biz_cal_def_employee},
                                        biz_cal_def_meeting = {$obj->biz_cal_def_meeting},
                                        biz_cal_def_reminder_hour = {$obj->biz_cal_def_reminder_hour},
                                        biz_cal_def_reminder_day = {$obj->biz_cal_def_reminder_day},
                                        biz_cal_biz_hours_only = {$obj->biz_cal_biz_hours_only}");
        return $newId;
    }

    private function loadBizCalendarSettingsFromDB($bizCalendarSettingsID){

        if (!is_numeric($bizCalendarSettingsID) || $bizCalendarSettingsID <= 0){
            throw new Exception("Illegal value bizCalendarSettingsID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_cal_settings WHERE bcs_id = $bizCalendarSettingsID");
    }

    private function loadBizCalendarSettingsFromDBByBizID($bizID){
        
        if (!is_numeric($bizID) || $bizID <= 0){
            throw new Exception("Illegal value bizID");             
        }
        
        return $this->db->getRow("SELECT * FROM tbl_biz_cal_settings WHERE bcs_biz_id = $bizID");
    }

    private function upateBizCalendarSettingsDB(bizCalendarSettingsObject $obj){

        if (!isset($obj->bcs_id) || !is_numeric($obj->bcs_id) || $obj->bcs_id <= 0){
            throw new Exception("bizCalendarSettingsObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_biz_cal_settings SET 
                                bcs_biz_id = {$obj->bcs_biz_id},
                                biz_cal_24h = {$obj->biz_cal_24h},
                                biz_cal_display_start = {$obj->biz_cal_display_start},
                                biz_cal_display_end = {$obj->biz_cal_display_end},
                                biz_cal_default_color = {$obj->biz_cal_default_color},
                                biz_cal_last_emp_id = {$obj->biz_cal_last_emp_id},
                                biz_cal_last_emp_name = '".addslashes($obj->biz_cal_last_emp_name)."',
                                biz_cal_def_employee = {$obj->biz_cal_def_employee},
                                biz_cal_def_meeting = {$obj->biz_cal_def_meeting},
                                biz_cal_def_reminder_hour = {$obj->biz_cal_def_reminder_hour},
                                biz_cal_def_reminder_day = {$obj->biz_cal_def_reminder_day},
                                biz_cal_biz_hours_only = {$obj->biz_cal_biz_hours_only}
                                WHERE bcs_id = {$obj->bcs_id}");
    }

    private function deleteBizCalendarSettingsByIdDB($bizCalendarSettingsID){

        if (!is_numeric($bizCalendarSettingsID) || $bizCalendarSettingsID <= 0){
            throw new Exception("Illegal value bizCalendarSettingsID");             
        }

        $this->db->execute("DELETE FROM tbl_biz_cal_settings WHERE bcs_id = $bizCalendarSettingsID");
    }
}
