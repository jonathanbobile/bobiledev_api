<?php
/**
 * Methods for Utility Functions
 *
 * @version 1.0
 * @author PapTap
 */

class bookingManager extends Manager
{ 
    private $currentRecordNo = 0;
    private $maxRecords = 30;
    private $index = 0;
    private $slotsList = array();

    function __construct(){
        parent::__construct();
    }

    /************************************* */
    /*   ADVANCED USAGE - PUBLIC           */
    /************************************* */

    public static function getAvailableClassesForBiz($biz_id){
        try{
            $instance = new self();
            $rowsList = $instance->getClassesForBiz_DB($biz_id);

            $classes = array();
            foreach ($rowsList as $entry)
            {
            	$classObj = classObject::withData($entry);

                $employeeObj = employeeObject::withData($entry);
                $classObj->setEmployee($employeeObj);

                if(isset($entry['bmt_id']) && $entry['bmt_id'] > 0){
                    $serviceObj = serviceObject::withData($entry);
                    $classObj->setService($serviceObj);
                }
                
                $classes[] = $classObj;
            }
            
            return resultObject::withData(1,'',$classes);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$biz_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeeMeetingsForCustomer(customerObject $customer,$start_date){
        try{
            $instance = new self();
            $meetings = array();
            $meetingsRows = $instance->getEmployeeMeetingForCustomerDB($customer,$start_date);

            foreach ($meetingsRows as $meetingRow)
            {
            	$meeting = employeeMeetingObject::withData($meetingRow);
                $meetings[] = $meeting;
            }

            return resultObject::withData(1,'',$meetings);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['start_date'] = $start_date;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerClassSignUps(customerObject $customer,$start_date){
        try{
            $instance = new self();
            $classesRows = $instance->getCustomerClassSignUpsDB($customer,$start_date);

            $classDatesList = array();
            foreach ($classesRows as $classRow)
            {           	

                $classDate = classDateObject::withData($classRow);


                $classDatesList[] = $classDate;
            }
            
            return resultObject::withData(1,'',$classDatesList);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['start_date'] = $start_date;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getClassDatesForClassByClassID($classID){
        try{
            $instance = new self();
            $datesRows = $instance->getClassDatesForClassByClassIDDB($classID);
            $dates = array();
            foreach ($datesRows as $dateRow)
            {
                
            	$date = classDateObject::withData($dateRow);
               
                $dates[] = $date;
            }
            return resultObject::withData(1,'',$dates);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getMeetingsList($empID,$meetTypeID,$startDate,$offset){
        try{
            $instance = new self();
          
            $startHourID = $instance->getStartHourIDFromDate($startDate,$offset);

            
            if($empID != 0){
                $employeeRow = $instance->loadEmployeeFromDB($empID);
                $employee = employeeObject::withData($employeeRow);
            }
            else{
                $employee = new employeeObject();
            }

            if($meetTypeID != 0){
                $meetTypeRow = $instance->loadServiceFromDB($meetTypeID);
                $meetType = serviceObject::withData($meetTypeRow);
            }
            else{
                $meetType = new serviceObject();
            }

            if($empID != 0 && $meetTypeID != 0){
               
                $instance->getMeetingListBoth($employee,$meetType,$startDate,$startHourID);
            }
            else if($empID == 0 && $meetTypeID != 0){
               
                $instance->getMeetingListOnlyMeetingType($meetType,$startDate,$startHourID);
            }
            else if($empID != 0 && $meetTypeID == 0){
               
                $instance->getMeetingListOnlyEmployee($employee,$startDate,$startHourID);
            }
            else{
                
                return resultObject::withData(0);
            }

            $meetings = array();
            foreach ($instance->slotsList as $slotData)
            {
            	$meetings[] = meetingSlotObject::withData($slotData);
            }

            return resultObject::withData(1,'',$meetings);
        }
        catch(Exception $e){
            $data = array();
            $data['empID'] = $empID;
            $data['meetType'] = $meetTypeID;
            $data['startDate'] = $startDate;
            $data['offset'] = $offset;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getServicesListForBizID($bizID){
        try{
            $instance = new self();

            $servicesRow = $instance->getServicesListForBizIDDB($bizID);

            $services = array();

            foreach ($servicesRow as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }
            
            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAvailableServicesListForBizID($bizID){
        try{
            $instance = new self();

            $servicesRow = $instance->getAvailableServicesListForBizIDDB($bizID);

            $services = array();

            foreach ($servicesRow as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }
            
            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getServicesListForEmployee(employeeObject $employee){
        try{
            $instance = new self();

            $servicesRow = $instance->getServicesForEmployee($employee);

            $services = array();

            foreach ($servicesRow as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }
            
            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employee));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeesForBizID($bizID){
        try{
            $instance = new self();

            $employeeRows = $instance->getEmployeeRowsForBizID($bizID);

            $employees = array();

            foreach ($employeeRows as $employeeRow)
            {
            	$employees[] = employeeObject::withData($employeeRow);
            }
            

            return resultObject::withData(1,'',$employees);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeesForService(serviceObject $service){
        try{
            $instance = new self();

            $employeeRows = $instance->getEmployeesRowsForService($service);

            $employees = array();

            foreach ($employeeRows as $employeeRow)
            {
            	$employees[] = employeeObject::withData($employeeRow);
            }            

            return resultObject::withData(1,'',$employees);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($service));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function setMeeting(employeeMeetingObject $meeting,customerObject $customer,$start_time_stamp,$end_time_stamp){
        try{
            $instance = new self();
            $meetingStartDate = date("Y-m-d",$start_time_stamp);
            $meetingEndDate = date("Y-m-d",$end_time_stamp);
            $meetingStartHour = date("H:i",$start_time_stamp);
            $meetingEndHour = date("H:i",$end_time_stamp);

            

            $startHourID = $instance->getHourIDFromStartHour($meetingStartHour);
            $endHourID = $instance->getHourIDFromEndHour($meetingEndHour);

            $start = $meetingStartDate." ".addslashes($meetingStartHour).":00";
            $end = $meetingEndDate." ".addslashes($meetingEndHour).":00";

            $isFuture = $instance->isFutureDate($start_time_stamp);

            if(!$isFuture){
                $responce = new stdClass();
                $responce->result["code"] = "9";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "222";
                $responce->result["internal_desc"] = "Past Dates Received";
                return resultObject::withData(1,'',$responce);
            }

            $isSlotAvailable = $instance->verifyAvailability($meeting->em_emp_id,$meetingStartDate,$meetingEndDate,$startHourID,$endHourID);

            if(!$isSlotAvailable){
                $responce = new stdClass();
                $responce->result["code"] = "8";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "221";
                $responce->result["internal_desc"] = "Slot is Taken";
                return resultObject::withData(1,'',$responce);
            }

            $serviceRow = $instance->loadServiceFromDB($meeting->em_meet_type);
            $service = serviceObject::withData($serviceRow);

            
            if($meeting->em_emp_id != 0){
                $employeeRow = $instance->loadEmployeeFromDB($meeting->em_emp_id);
                $employee = employeeObject::withData($employeeRow);
            }
            else{
                $employee = new employeeObject();
            }

            $meeting->setEmployee($employee);

            $country = bizManager::getBizCountryID($customer->cust_biz_id);

            $meetStart = ($country=="1") ? date( 'm/d/Y h:i A', $start_time_stamp ) : date( 'd/m/Y H:i', $start_time_stamp );
            $meetEnd = ($country=="1") ? date( 'm/d/Y h:i A', $end_time_stamp ) : date( 'd/m/Y H:i', $end_time_stamp );

            if($employee->be_cal_type == "IOS" || $employee->be_cal_type == "Outlook"){

                if($employee->be_cal_type == "Outlook"){
                    $meeting->em_cust_request = str_replace("\n","<br>",$meeting->em_cust_request);
                }

                $addSpace = ($employee->be_cal_type == "Outlook") ? "<br>" : "\n";

                if($customer->cust_first_name != ""){                
                    $meeting->em_cust_request = "Client: {$customer->cust_first_name}".$addSpace.$meeting->em_cust_request;
                }

                $meeting->em_cust_request = "Employee: ".$employee->be_name.$addSpace.$meeting->em_cust_request;
            }

            if($employee->be_cal_type == "IOS"){
                $meeting->em_cust_request = str_replace("\r\n", "\n", $meeting->em_cust_request);
                $meeting->em_cust_request = str_replace("\\n", "\n", $meeting->em_cust_request);
                $meeting->em_cust_request = str_replace("\n", "\\n", $meeting->em_cust_request);
            }

            $meeting->em_start_date = $meetingStartDate;
            $meeting->em_start_time = $startHourID;
            $meeting->em_end_date = $meetingEndDate;
            $meeting->em_end_time = $endHourID;

            $meeting->em_color = $service->bmt_color;
            $meeting->em_cust_name = $customer->cust_first_name;

            $meeting->em_start = date('Y-m-d H:i:s',$start_time_stamp);
            $meeting->em_end = date('Y-m-d H:i:s',$end_time_stamp);

            $meetID = $instance->addEmployeeMeetingDB($meeting);

            if($meetID <= 0){
                return resultObject::withData(0,'could not add');
            }

            $meeting->em_id = $meetID;

            $meeting->setCustomer($customer);

            if($meeting->em_payment_source == 'order'){
                customerOrderManager::updateOrderitemsExternalTypeAndID($meeting->em_payment_source_id,'meeting',$meetID);
            }

            if($meeting->em_payment_source == "subscription"){
                $usage = new customerSubscriptionUsageObject();
                $usage->csuse_biz_id = $meeting->em_biz_id;
                $usage->csuse_cust_id = $customer->cust_id;
                $usage->csuse_csu_id = $meeting->em_payment_source_id;
                $usage->csuse_item_type = 'service';
                $usage->csuse_item_id = $meetID;
                $usage->csuse_source = 'auto';
                $usage->csuse_type = 'usage';
                customerSubscriptionManager::useCustSubscription($usage);
            }

            if($meeting->em_payment_source == "punch_pass"){
                $usage = new customerPunchUsageObject();
                $usage->cpuse_biz_id = $meeting->em_biz_id;
                $usage->cpuse_cust_id = $customer->cust_id;
                $usage->cpuse_cpp_id = $meeting->em_payment_source_id;
                $usage->cpuse_item_type = 'service';
                $usage->cpuse_item_id = $meetID;
                $usage->cpuse_source = 'auto';
                $usage->cpuse_type = 'usage';
                customerSubscriptionManager::useCustPunchPass($usage);

                
            }

            eventManager::actionTrigger(enumCustomerActions::bookedAppointment,$customer->cust_id, "appointment",'',$meeting->em_cust_device_id,$meeting->em_id);

            $instance->syncMeetingCalendars($meeting);

            $instance->setMeetingsRemindersMobile($meeting->em_biz_id,$meeting->em_cust_id,$meeting->em_name,$start_time_stamp,$meeting->em_id);

            $instance->sendMeetingEmails($meeting);

            $response = new stdClass();
            $response->result["code"] = 0;
            $response->result["meeting_id"] = $meeting->em_id;
            $response->result["localized_no"] = "225";
            $response->result["internal_desc"] = "Meeting is set";

            customerManager::addCustomerLoyaltyStampToCustomerFromMeeting($customer,$meeting->em_meet_type,$meeting->em_id);

            return resultObject::withData(1,'',$response);
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            $data['start'] = $start_time_stamp;
            $data['end'] = $end_time_stamp;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function signUpCustomerToClassdate(customerObject $customer,classDateCustomerObject $customerDate){
        try{
            $instance = new self();

            $custDateID = $instance->addClassDateCustomerDB($customerDate);

            if($custDateID <= 0){
                return resultObject::withData(0,'add_failed');
            }

            $customerDate->cdc_id = $custDateID;

            if($customerDate->cdc_payment_source == 'order'){
                customerOrderManager::updateOrderitemsExternalTypeAndID($customerDate->cdc_payment_source_id,'class',$custDateID);
            }

            if($customerDate->cdc_payment_source == "subscription"){
                $usage = new customerSubscriptionUsageObject();
                $usage->csuse_biz_id = $customer->cust_biz_id;
                $usage->csuse_cust_id = $customer->cust_id;
                $usage->csuse_csu_id = $customerDate->cdc_payment_source_id;
                $usage->csuse_item_type = 'class';
                $usage->csuse_item_id = $custDateID;
                $usage->csuse_source = 'auto';
                $usage->csuse_type = 'usage';
                customerSubscriptionManager::useCustSubscription($usage);
            }

            if($customerDate->cdc_payment_source == "punch_pass"){
                $usage = new customerPunchUsageObject();
                $usage->cpuse_biz_id = $customer->cust_biz_id;
                $usage->cpuse_cust_id = $customer->cust_id;
                $usage->cpuse_cpp_id = $customerDate->cdc_payment_source_id;
                $usage->cpuse_item_type = 'class';
                $usage->cpuse_item_id = $custDateID;
                $usage->cpuse_source = 'auto';
                $usage->cpuse_type = 'usage';
                customerSubscriptionManager::useCustPunchPass($usage);
            }

            $instance->sendCustomerClassDateEmails($customer,$customerDate);

            $params = array();
            $params['class_date_id'] = $customerDate->cdc_id;
            utilityManager::asyncTask(URL . 'autojobs/asyncUpdateClassAttendees',$params);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['date'] = $customerDate;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function searchServicesInBiz($bizID,$skip = 0,$take = 5){
        try{
            $instance = new self();
            $serviceRows = $instance->searchServicesInBiz_DB($bizID,$skip,$take);

            $services = array();

            foreach ($serviceRows as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }
            
            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   ADVANCED USAGE - PRIVATE          */
    /************************************* */


    private function getCustomerClassSignUpsDB(customerObject $customer,$start_date){
        $sql = "select * from tbl_calendar_class_dates, 
                    (select * from calendar_hours) startTable,
                    (select * from calendar_hours) endTable,
					tbl_class_date_customers
                    where ccd_class_id=cdc_class_id									
					and cdc_class_date_id = ccd_id
					and cdc_customer_id = {$customer->cust_id}
                    and startTable.hour_id = ccd_start_time
                    and endTable.hour_id = ccd_end_time
                    and concat(ccd_start_date,' ',startTable.hour_start) >= STR_TO_DATE('$start_date','%d/%m/%Y')";

        return $this->db->getTable($sql);
    }

    private function syncMeetingCalendars(employeeMeetingObject $meeting){
        $calendarSincModel = new calendarSincManager();

        $employee = $meeting->getEmployee();

        if($employee->be_cal_type == "Google" || $employee->be_cal_type == "IOS" || $employee->be_cal_type == "Outlook"){
            $client = array();
            if($meeting->be_cal_type == "Google"){                
                $client = $calendarSincModel->getClient($employee->be_id);                
            }
            
            $externalID="";
            $startTime = strtotime( $meeting->em_start_date." ".$meeting->em_start_time.":00");
            $startdate = date( 'Y-m-d\TH:i:s', $startTime );

            $endTime = strtotime(  $meeting->em_end_date." ".$meeting->em_end_time.":00" );
            $enddate = date( 'Y-m-d\TH:i:s', $endTime );

            $sincVals = array();
            $sincVals["calendarId"] = $employee->be_cal_id;
            $sincVals["eventName"] = $meeting->em_name;
            $sincVals["notes"] = $meeting->em_cust_request;
            $sincVals["startDate"] = $startdate;
            $sincVals["endDate"] = $enddate;
            $sincVals["client"] = $client;
            $attendees = array();

            $employeeArray = array();
            $employeeArray["id"] = $employee->be_id;
            $employeeArray["email"] = $employee->be_mail;
            $employeeArray["displayName"] = $employee->be_name;
            $employeeArray["organizer"] = true;

            array_push($attendees,$employeeArray);
            
            if($meeting->em_cust_id != 0){
                
                $custEmail = $this->db->getVal($meeting->customer->cust_email);

                $employeeArray = array();
                $employeeArray["id"] = $meeting->em_cust_id;
                
                $employeeArray["email"] = $custEmail;
                
                $employeeArray["displayName"] = $meeting->em_cust_name;
                $employeeArray["organizer"] = false;
                if($custEmail != ""){
                    array_push($attendees,$employeeArray);
                }
            }
            
            $sincVals["attendees"] = $attendees;

            if($employee->be_cal_type == "Google"){
                if(count($client) != 0){
                    $externalID = $calendarSincModel->addEventFromArray($sincVals);
                }
            }
            if($employee->be_cal_type == "IOS"){

                $appleUsername = utilityManager::decryptIt($employee->be_apple_user);
                $applePassword = utilityManager::decryptIt($employee->be_apple_pass);

                $externalID = $calendarSincModel->addEventToIOS($appleUsername,$applePassword,$employee->be_cal_id,$meeting->em_id,$meeting->em_name,$meeting->em_cust_request,$startdate, $enddate,$employee->be_event_color);
            }
            if($employee->be_cal_type == "Outlook"){
                $externalID = $calendarSincModel->addEventOutlook($employee->be_id,$meeting->em_name,$meeting->em_cust_request,$startdate,$enddate,$client);
            }

            $this->db->execute("update tbl_employee_meeting SET
                    em_external_id='$externalID'
                    where em_id={$meeting->em_id}");
        }



    }

    private function setMeetingsRemindersMobile($biz_id,$cust_id,$meet_name,$time,$meeting_id){
        $reminders = $this->checkCalendarReminders($biz_id);

        $sql = "SELECT biz_owner_id,biz_time_zone FROM tbl_biz WHERE biz_id = $biz_id";

        $bizRow = $this->db->getRow($sql);

        $owner_id = $bizRow['biz_owner_id'];
        $biz_time = $bizRow['biz_time_zone'];
        if($reminders['biz_cal_def_reminder_hour'] == 1 && $cust_id != 0){
            $sql = "SELECT CASE
                            WHEN ntpo_push_text IS NULL THEN ntp_push_text
                            WHEN ntpo_push_text = '' THEN ntp_push_text
                            ELSE ntpo_push_text
                           END as text
                    FROM tbl_biz_notif_params LEFT JOIN tbl_biz_notif_params_owner ON ntpo_notification_id = ntp_id AND ntpo_owner_id = $owner_id 
                    WHERE ntp_id = 14";

            $text = $this->db->getVal($sql);

            $text = str_replace('#meeting_name#',$meet_name,$text);
            $correctTime = 1 + $biz_time;
            $sql = "INSERT INTO tbl_reminders SET
                                rem_biz_id = {$biz_id},
                                pu_cust_id = $cust_id,
                                rem_repeat = 'Once',
                                rem_text = '$text',
                                rem_time = TIME(DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR)),
                                rem_next_send = DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR),
                                rem_entity_type = 'meeting',
                                rem_entity_id = $meeting_id,
                                rem_active = 1";
            
            $this->db->execute($sql);
        }

        if($reminders['biz_cal_def_reminder_day'] == 1 && $cust_id != 0){
            $meetTime = DateTime::createFromFormat('U',$time);

            $dayAheadTime = new DateTime('+1 day');            
            
            if($meetTime > $dayAheadTime){// check that the meeting is set for more than a day in the future - otherwise don't add day reminder
                $sql = "SELECT CASE
                                WHEN ntpo_push_text IS NULL THEN ntp_push_text
                                WHEN ntpo_push_text = '' THEN ntp_push_text
                                ELSE ntpo_push_text
                               END as text
                        FROM tbl_biz_notif_params LEFT JOIN tbl_biz_notif_params_owner ON ntpo_notification_id = ntp_id AND ntpo_owner_id = $owner_id 
                        WHERE ntp_id = 15";

                $text = $this->db->getVal($sql);

                $text = str_replace('#meeting_name#',$meet_name,$text);
                $correctTime = 24 + $biz_time;
                $sql = "INSERT INTO tbl_reminders SET
                                    rem_biz_id = {$biz_id},
                                    pu_cust_id = $cust_id,
                                    rem_repeat = 'Once',
                                    rem_text = '$text',
                                    rem_time = TIME(DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR)),
                                    rem_next_send = DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR),
                                    rem_entity_type = 'meeting',
                                    rem_entity_id = $meeting_id,
                                    rem_active = 1";

                $this->db->execute($sql);
            }
        }

        return 1;
    }

    /**
     * Check if biz sends reminders before meetings
     * @param mixed $biz_id 
     * @return mixed
     */
    private function checkCalendarReminders($biz_id){
        $sql = "SELECT biz_cal_def_reminder_hour,biz_cal_def_reminder_day FROM tbl_biz_cal_settings WHERE bcs_biz_id = $biz_id";

        $result = $this->db->getRow($sql);

        return $result;
    }

    private function sendMeetingEmails(employeeMeetingObject $meeting){
        $extraParams = array();
        $extraParams["cust_name"] = $meeting->em_cust_name;
        $extraParams["meet_type"] = $meeting->em_name;
        $extraParams["meet_start"] = strtotime($meeting->em_start_date." ".$meeting->em_start_time.":00");
        $extraParams["meet_end"] = strtotime($meeting->em_start_date." ".$meeting->em_start_time.":00");
        $extraParams["employee_name"] = $meeting->employee->be_name;

        //Email to Admin
        emailManager::sendSystemMailApp($meeting->em_biz_id,146,enumEmailType::SystemMailSGCare,$extraParams);

        //Email to Employee  
        if($meeting->employee->be_mail != ""){
            $extraParams["to_name"] = $meeting->employee->be_name;
            $extraParams["to_email"] = $meeting->employee->be_mail;
            emailManager::sendSystemMailApp($meeting->em_biz_id,210,enumEmailType::SystemMailSGCare,$extraParams); 
        }

        //Push to Admin
        $message = pushManager::getSystemAlertPushMessageText(20,$meeting->em_biz_id,$extraParams);
        pushManager::sendPushAdmin($meeting->em_biz_id,enumPushType::admin_newMeeting,$message);
        
        if($meeting->customer->cust_email != ""){
            $extraParams["to_name"] = $meeting->customer->cust_first_name;
            $extraParams["to_email"] = $meeting->customer->cust_email;
            //Email to customer 
            emailManager::sendSystemMailApp($meeting->em_biz_id,150,enumEmailType::SystemMailSGCare,$extraParams);
        }

        $notifications = new notificationsManager();
        $params["cust_first_name"] = $meeting->em_cust_name;
        $params["customer_id"] = $meeting->em_cust_id;
        $notifications->addNotification($meeting->em_biz_id,enumActions::newMeeting,false,$params);

        //Push to Customer
        $message = pushManager::getSystemAlertPushMessageText(21,$meeting->em_biz_id,$extraParams);
        pushManager::sendPushBiz($meeting->em_biz_id,$meeting->em_cust_id,enumPushType::biz_personalBooking,$message);

        return;
    }

    private function sendCustomerClassDateEmails(customerObject $customer,classDateCustomerObject $customerDate){
        $classRow = $this->loadClassFromDB($customerDate->cdc_class_id);
        $class = classObject::withData($classRow);

        

        $dateRow = $this->loadClassDateFromDB($customerDate->cdc_class_date_id);
        $date = classDateObject::withData($dateRow);

        $custName = $customer->cust_first_name;
        $custEmail = $customer->cust_email;

        if($custEmail != "" && $custEmail != "N/A"){
            $extraParams = array();
            if(isset($classRow['ce_employee_id']) && $classRow['ce_employee_id'] > 0){
                $employeeRow = $this->loadEmployeeFromDB($classRow['ce_employee_id']);

                $employeeName = $employeeRow["be_name"];
                $employeeEmail = $employeeRow["be_mail"];                
            }
            else{
                $employeeName = "";
                $employeeEmail = "";
            }
            $extraParams["employee_name"] = $employeeName;

            $hour = $this->getHourDataFromHourID($date->ccd_start_time);

            $extraParams["cust_name"] = stripslashes($custName);
            $extraParams["class_name"] = stripslashes($class->calc_name);
            $extraParams["class_date"] = bizManager::getBizCountryID($class->calc_biz_id) ? date( 'm/d/Y', strtotime(($date->ccd_start_date))) : date( 'd/m/Y', strtotime(($date->ccd_start_date)));
            $extraParams["class_time"] = $hour['hour_start'];
            emailManager::sendSystemMailApp($class->calc_biz_id,230,enumEmailType::SystemMailSGCare,$extraParams);

            if($employeeEmail != ""){
                $extraParams["to_name"] = $employeeName;
                $extraParams["to_email"] = $employeeEmail;
                emailManager::sendSystemMailApp($class->calc_biz_id,229,enumEmailType::SystemMailSGCare,$extraParams);
            }

            $extraParams["to_name"] = $custName;
            $extraParams["to_email"] = $custEmail;
            emailManager::sendSystemMailApp($class->calc_biz_id,229,enumEmailType::SystemMailSGCare,$extraParams);
        }
    }
    
    /************************************* */
    /*   BASIC SERVICE - PUBLIC           */
    /************************************* */

    /**
     * Insert new serviceObject to DB
     * Return Data = new serviceObject ID
     * @param serviceObject $serviceObj 
     * @return resultObject
     */
    public static function addService(serviceObject $serviceObj){       
        try{
            $instance = new self();
            $newId = $instance->addServiceDB($serviceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($serviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get service from DB for provided ID
     * * Return Data = serviceObject
     * @param int $serviceId 
     * @return resultObject
     */
    public static function getServiceByID($serviceId){
        
        try {
            $instance = new self();
            $serviceData = $instance->loadServiceFromDB($serviceId);
            
            $serviceObj = serviceObject::withData($serviceData);
            $result = resultObject::withData(1,'',$serviceObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$serviceId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update service in DB
     * @param serviceObject $serviceObj 
     * @return resultObject
     */
    public static function updateService(serviceObject $serviceObj){        
        try{
            $instance = new self();
            $instance->upateServiceDB($serviceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($serviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete service from DB
     * @param int $serviceID 
     * @return resultObject
     */
    public static function deleteServiceById($serviceID){
        try{
            $instance = new self();
            $instance->deleteServiceByIdDB($serviceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$serviceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC SERVICE - DB METHODS           */
    /************************************* */

    private function addServiceDB(serviceObject $obj){

        if (!isset($obj)){
            throw new Exception("serviceObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_cal_meet_type SET 
                                        bmt_biz_id = {$obj->bmt_biz_id},
                                        bmt_name = '".addslashes($obj->bmt_name)."',
                                        bmt_desc = '".addslashes($obj->bmt_desc)."',
                                        bmt_size = {$obj->bmt_size},
                                        bmt_duration = '".addslashes($obj->bmt_duration)."',
                                        bmt_active = {$obj->bmt_active},
                                        bmt_mobile_active = {$obj->bmt_mobile_active},
                                        bmt_capacity = {$obj->bmt_capacity},
                                        bmt_color = '".addslashes($obj->bmt_color)."',
                                        bmt_price = {$obj->bmt_price},
                                        bmt_pic = '".addslashes($obj->bmt_pic)."'
                                                 ");
        return $newId;
    }

    private function loadServiceFromDB($serviceID){

        if (!is_numeric($serviceID) || $serviceID <= 0){
            throw new Exception("Illegal value serviceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_cal_meet_type WHERE bmt_id = $serviceID");
    }

    private function upateServiceDB(serviceObject $obj){

        if (!isset($obj->bmt_id) || !is_numeric($obj->bmt_id) || $obj->bmt_id <= 0){
            throw new Exception("serviceObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_biz_cal_meet_type SET 
                                bmt_biz_id = {$obj->bmt_biz_id},
                                bmt_name = '".addslashes($obj->bmt_name)."',
                                bmt_desc = '".addslashes($obj->bmt_desc)."',
                                bmt_size = {$obj->bmt_size},
                                bmt_duration = '".addslashes($obj->bmt_duration)."',
                                bmt_active = {$obj->bmt_active},
                                bmt_mobile_active = {$obj->bmt_mobile_active},
                                bmt_capacity = {$obj->bmt_capacity},
                                bmt_color = '".addslashes($obj->bmt_color)."',
                                bmt_price = {$obj->bmt_price},
                                bmt_pic = '".addslashes($obj->bmt_pic)."'
                                WHERE bmt_id = {$obj->bmt_id} 
                                         ");
    }

    private function deleteServiceByIdDB($serviceID){

        if (!is_numeric($serviceID) || $serviceID <= 0){
            throw new Exception("Illegal value serviceID");             
        }

        $this->db->execute("DELETE FROM tbl_biz_cal_meet_type WHERE bmt_id = $serviceID");
    }

    private function getServicesForEmployee(employeeObject $employee){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type,tbl_employee_meet_type
                                WHERE bmt_biz_id = {$employee->be_biz_id}
                                AND emt_bmt_id = bmt_id
                                AND emt_emp_id = {$employee->be_id}
                                AND bmt_active = 1
                                AND bmt_mobile_active = 1
                                AND emt_active = 1
                                ORDER BY bmt_id ASC";

        return $this->db->getTable($sql);
    }

    private function getServicesListForBizIDDB($bizID){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type 
                WHERE bmt_biz_id = $bizID
                AND bmt_active = 1
                AND bmt_mobile_active = 1 ";

        return $this->db->getTable($sql);
    }

    private function getAvailableServicesListForBizIDDB($bizID){
        $sql = "SELECT * FROM tbl_employee_meet_type,tbl_employee,tbl_biz_cal_meet_type 
                WHERE bmt_biz_id = $bizID
                AND be_id=emt_emp_id
                AND emt_bmt_id = bmt_id
                AND be_active = 1
                AND emt_active = 1
                AND bmt_active = 1
                AND bmt_mobile_active = 1
                GROUP BY bmt_id";

        return $this->db->getTable($sql);
    }

    private function searchServicesInBiz_DB($bizID,$skip = 0,$take = 5){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type 
                WHERE bmt_biz_id = $bizID
                AND bmt_active = 1
                AND bmt_mobile_active = 1
                AND ";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CLASS - PUBLIC           */
    /************************************* */

    /**
     * Insert new classObject to DB
     * Return Data = new classObject ID
     * @param classObject $classObj 
     * @return resultObject
     */
    public static function addClass(classObject $classObj){       
        try{
            $instance = new self();
            $newId = $instance->addClassDB($classObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get class from DB for provided ID
     * * Return Data = classObject
     * @param int $classId 
     * @return resultObject
     */
    public static function getClassByID($classId){
        
        try {
            $instance = new self();
            $classData = $instance->loadClassFromDB($classId);
            
            $classObj = classObject::withData($classData);

            if(isset($classData['ce_employee_id']) && $classData['ce_employee_id'] > 0){
                $employeeData = $instance->loadEmployeeFromDB($classData['ce_employee_id']);
                $classObj->setEmployee(employeeObject::withData($employeeData));
            }

            if($classObj->calc_service > 0){
                $serviceData = $instance->loadServiceFromDB($classObj->calc_service);
                $classObj->setService(serviceObject::withData($serviceData));
            }

            $result = resultObject::withData(1,'',$classObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update class in DB
     * @param classObject $classObj 
     * @return resultObject
     */
    public static function updateClass(classObject $classObj){        
        try{
            $instance = new self();
            $instance->upateClassDB($classObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete class from DB
     * @param int $classID 
     * @return resultObject
     */
    public static function deleteClassById($classID){
        try{
            $instance = new self();
            $instance->deleteClassByIdDB($classID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CLASS - DB METHODS           */
    /************************************* */

    private function addClassDB(classObject $obj){

        if (!isset($obj)){
            throw new Exception("classObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_calendar_classes SET 
                                        calc_biz_id = {$obj->calc_biz_id},
                                        calc_name = '".addslashes($obj->calc_name)."',
                                        calc_desc = '".addslashes($obj->calc_desc)."',
                                        calc_max_cust = {$obj->calc_max_cust},
                                        calc_min_cust = {$obj->calc_min_cust},
                                        calc_duration_by_service = {$obj->calc_duration_by_service},
                                        calc_service = {$obj->calc_service},
                                        calc_price = {$obj->calc_price},
                                        calc_duration = '".addslashes($obj->calc_duration)."',
                                        calc_duration_size = {$obj->calc_duration_size},
                                        calc_isvisible = {$obj->calc_isvisible},
                                        calc_isdeleted = {$obj->calc_isdeleted}
                                                 ");
        return $newId;
    }

    private function loadClassFromDB($classID){

        if (!is_numeric($classID) || $classID <= 0){
            throw new Exception("Illegal value classID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_calendar_classes
                                    LEFT JOIN tbl_calendar_class_employee ON calc_id = ce_class_id
                                    WHERE calc_id = $classID");
    }

    private function upateClassDB(classObject $obj){

        if (!isset($obj->calc_id) || !is_numeric($obj->calc_id) || $obj->calc_id <= 0){
            throw new Exception("classObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_calendar_classes SET 
                                calc_biz_id = {$obj->calc_biz_id},
                                calc_name = '".addslashes($obj->calc_name)."',
                                calc_desc = '".addslashes($obj->calc_desc)."',
                                calc_max_cust = {$obj->calc_max_cust},
                                calc_min_cust = {$obj->calc_min_cust},
                                calc_duration_by_service = {$obj->calc_duration_by_service},
                                calc_service = {$obj->calc_service},
                                calc_price = {$obj->calc_price},
                                calc_duration = '".addslashes($obj->calc_duration)."',
                                calc_duration_size = {$obj->calc_duration_size},
                                calc_isvisible = {$obj->calc_isvisible},
                                calc_isdeleted = {$obj->calc_isdeleted}
                                WHERE calc_id = {$obj->calc_id} 
                                         ");
    }

    private function deleteClassByIdDB($classID){

        if (!is_numeric($classID) || $classID <= 0){
            throw new Exception("Illegal value classID");             
        }

        $this->db->execute("DELETE FROM tbl_calendar_classes WHERE calc_id = $classID");
    }

    private function getClassesForBiz_DB($biz_id){
        if (!is_numeric($biz_id) || $biz_id <= 0){
            throw new Exception("Illegal value bizID");             
        }

        $sql = "select *
                from tbl_calendar_class_employee,tbl_employee,
                (SELECT ccd_class_id,min(concat(ccd_start_date,' ',hour_start)) as myDate 
                FROM tbl_calendar_class_dates,calendar_hours 
                where ccd_start_time=hour_id 
                and concat(ccd_start_date,' ',hour_start) > now()
                and ccd_biz_id=$biz_id
                group by ccd_class_id) t1,tbl_calendar_classes
                LEFT JOIN tbl_biz_cal_meet_type ON calc_service = bmt_id
                WHERE ccd_class_id=calc_id 
                and calc_id=ce_class_id
                and be_id=ce_employee_id
                and calc_isvisible = 1
                and calc_isdeleted = 0";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CLASSDATE - PUBLIC           */
    /************************************* */

    /**
     * Insert new classDateObject to DB
     * Return Data = new classDateObject ID
     * @param classDateObject $classDateObj 
     * @return resultObject
     */
    public static function addClassDate(classDateObject $classDateObj){       
        try{
            $instance = new self();
            $newId = $instance->addClassDateDB($classDateObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get classDate from DB for provided ID
     * * Return Data = classDateObject
     * @param int $classDateId 
     * @return resultObject
     */
    public static function getClassDateByID($classDateId){
        
        try {
            $instance = new self();
            $classDateData = $instance->loadClassDateFromDB($classDateId);
            
            $classDateObj = classDateObject::withData($classDateData);
            $result = resultObject::withData(1,'',$classDateObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update classDate in DB
     * @param classDateObject $classDateObj 
     * @return resultObject
     */
    public static function updateClassDate(classDateObject $classDateObj){        
        try{
            $instance = new self();
            $instance->upateClassDateDB($classDateObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete classDate from DB
     * @param int $classDateID 
     * @return resultObject
     */
    public static function deleteClassDateById($classDateID){
        try{
            $instance = new self();
            $instance->deleteClassDateByIdDB($classDateID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getClassDateBookedCount($classDateID){
        $instance = new self();

        return $instance->getClassDateBookedCountDB($classDateID);
    }

    public static function isCustomerInClassDate($cust_id,classDateObject $classDate){
        $instance = new self();
        return $instance->isCustomerInClassDateDB($cust_id,$classDate);
    }

    /************************************* */
    /*   BASIC CLASSDATE - DB METHODS           */
    /************************************* */

    private function addClassDateDB(classDateObject $obj){

        if (!isset($obj)){
            throw new Exception("classDateObject value must be provided");             
        }

        $ccd_start_dateDate = isset($obj->ccd_start_date) ? "'".$obj->ccd_start_date."'" : "null";

        $ccd_end_dateDate = isset($obj->ccd_end_date) ? "'".$obj->ccd_end_date."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_calendar_class_dates SET 
                                        ccd_biz_id = {$obj->ccd_biz_id},
                                        ccd_class_id = {$obj->ccd_class_id},
                                        ccd_start_date = $ccd_start_dateDate,
                                        ccd_start_time = {$obj->ccd_start_time},
                                        ccd_end_date = $ccd_end_dateDate,
                                        ccd_end_time = {$obj->ccd_end_time},
                                        ccd_iscanceled = {$obj->ccd_iscanceled},
                                        ccd_isvisible = {$obj->ccd_isvisible}
                                                 ");
        return $newId;
    }

    private function loadClassDateFromDB($classDateID){

        if (!is_numeric($classDateID) || $classDateID <= 0){
            throw new Exception("Illegal value classDateID");             
        }

        return $this->db->getRow("SELECT tbl_calendar_class_dates.*,
                                         startTable.hour_start as start_hour,
                                         endTable.hour_end as end_hour 
                                    FROM tbl_calendar_class_dates, 
                                    (select * from calendar_hours) startTable,
                                    (select * from calendar_hours) endTable
                                    WHERE ccd_id = $classDateID
                                    AND startTable.hour_id = ccd_start_time
                                    AND endTable.hour_id = ccd_end_time");
    }

    private function upateClassDateDB(classDateObject $obj){

        if (!isset($obj->ccd_id) || !is_numeric($obj->ccd_id) || $obj->ccd_id <= 0){
            throw new Exception("classDateObject value must be provided");             
        }

        $ccd_start_dateDate = isset($obj->ccd_start_date) ? "'".$obj->ccd_start_date."'" : "null";

        $ccd_end_dateDate = isset($obj->ccd_end_date) ? "'".$obj->ccd_end_date."'" : "null";

        $this->db->execute("UPDATE tbl_calendar_class_dates SET 
                                ccd_biz_id = {$obj->ccd_biz_id},
                                ccd_class_id = {$obj->ccd_class_id},
                                ccd_start_date = $ccd_start_dateDate,
                                ccd_start_time = {$obj->ccd_start_time},
                                ccd_end_date = $ccd_end_dateDate,
                                ccd_end_time = {$obj->ccd_end_time},
                                ccd_iscanceled = {$obj->ccd_iscanceled},
                                ccd_isvisible = {$obj->ccd_isvisible}
                                WHERE ccd_id = {$obj->ccd_id} 
                                         ");
    }

    private function deleteClassDateByIdDB($classDateID){

        if (!is_numeric($classDateID) || $classDateID <= 0){
            throw new Exception("Illegal value classDateID");             
        }

        $this->db->execute("DELETE FROM tbl_calendar_class_dates WHERE ccd_id = $classDateID");
    }

    private function getClassDatesForClassByClassIDDB($classID){
        $sql = "select tbl_calendar_class_dates.*, startTable.hour_start start_hour,endTable.hour_end end_hour
                                        from tbl_calendar_class_dates, 
                                        (select * from calendar_hours) startTable,
                                        (select * from calendar_hours) endTable
                                        where ccd_class_id=$classID
                                        and startTable.hour_id = ccd_start_time
                                        and endTable.hour_id = ccd_end_time
                                        and concat(ccd_start_date,' ',startTable.hour_start) > now()
                                        and ccd_iscanceled = 0
                                        and ccd_isvisible = 1
                                        order by ccd_start_date,ccd_start_time";

        return $this->db->getTable($sql);
    }

    private function getClassDateBookedCountDB($classDateID){
        $sql = "select count(cdc_id) from tbl_class_date_customers where cdc_class_date_id = $classDateID";

        return $this->db->getVal($sql);
    }

    private function isCustomerInClassDateDB($custID,classDateObject $classDate){
        $sql = "SELECT count(cdc_id) FROM tbl_class_date_customers 
                WHERE cdc_biz_id={$classDate->ccd_biz_id}
                AND cdc_class_id={$classDate->ccd_class_id} 
                AND cdc_class_date_id={$classDate->ccd_id}
                AND cdc_customer_id=$custID";

        return $this->db->getVal($sql) > 0;
    }

    /************************************* */
    /*   BASIC CLASSDATECUSTOMER - PUBLIC           */
    /************************************* */

    /**
     * Insert new classDateCustomerObject to DB
     * Return Data = new classDateCustomerObject ID
     * @param classDateCustomerObject $classDateCustomerObj 
     * @return resultObject
     */
    public static function addClassDateCustomer(classDateCustomerObject $classDateCustomerObj){       
        try{
            $instance = new self();
            $newId = $instance->addClassDateCustomerDB($classDateCustomerObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateCustomerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get classDateCustomer from DB for provided ID
     * * Return Data = classDateCustomerObject
     * @param int $classDateCustomerId 
     * @return resultObject
     */
    public static function getClassDateCustomerByID($classDateCustomerId){
        
        try {
            $instance = new self();
            $classDateCustomerData = $instance->loadClassDateCustomerFromDB($classDateCustomerId);
            
            $classDateCustomerObj = classDateCustomerObject::withData($classDateCustomerData);
            $result = resultObject::withData(1,'',$classDateCustomerObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateCustomerId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update classDateCustomer in DB
     * @param classDateCustomerObject $classDateCustomerObj 
     * @return resultObject
     */
    public static function updateClassDateCustomer(classDateCustomerObject $classDateCustomerObj){        
        try{
            $instance = new self();
            $instance->upateClassDateCustomerDB($classDateCustomerObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateCustomerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete classDateCustomer from DB
     * @param int $classDateCustomerID 
     * @return resultObject
     */
    public static function deleteClassDateCustomerById($classDateCustomerID){
        try{
            $instance = new self();
            $instance->deleteClassDateCustomerByIdDB($classDateCustomerID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateCustomerID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CLASSDATECUSTOMER - DB METHODS           */
    /************************************* */

    private function addClassDateCustomerDB(classDateCustomerObject $obj){

        if (!isset($obj)){
            throw new Exception("classDateCustomerObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_class_date_customers SET 
                                        cdc_biz_id = {$obj->cdc_biz_id},
                                        cdc_class_id = {$obj->cdc_class_id},
                                        cdc_class_date_id = {$obj->cdc_class_date_id},
                                        cdc_customer_id = {$obj->cdc_customer_id},
                                        cdc_comments = '".addslashes($obj->cdc_comments)."',
                                        cdc_payment_source = '{$obj->cdc_payment_source}',
                                        cdc_payment_source_id = {$obj->cdc_payment_source_id}
                                                 ");
        return $newId;
    }

    private function loadClassDateCustomerFromDB($classDateCustomerID){

        if (!is_numeric($classDateCustomerID) || $classDateCustomerID <= 0){
            throw new Exception("Illegal value classDateCustomerID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_class_date_customers WHERE cdc_id = $classDateCustomerID");
    }

    private function upateClassDateCustomerDB(classDateCustomerObject $obj){

        if (!isset($obj->cdc_id) || !is_numeric($obj->cdc_id) || $obj->cdc_id <= 0){
            throw new Exception("classDateCustomerObject value must be provided");             
        }

        $cdc_signupDate = isset($obj->cdc_signup) ? "'".$obj->cdc_signup."'" : "null";

        $this->db->execute("UPDATE tbl_class_date_customers SET 
                                cdc_biz_id = {$obj->cdc_biz_id},
                                cdc_class_id = {$obj->cdc_class_id},
                                cdc_class_date_id = {$obj->cdc_class_date_id},
                                cdc_customer_id = {$obj->cdc_customer_id},
                                cdc_signup = $cdc_signupDate,
                                cdc_comments = '".addslashes($obj->cdc_comments)."',
                                cdc_payment_source = '{$obj->cdc_payment_source}',
                                cdc_payment_source_id = {$obj->cdc_payment_source_id}
                                WHERE cdc_id = {$obj->cdc_id} 
                                         ");
    }

    private function deleteClassDateCustomerByIdDB($classDateCustomerID){

        if (!is_numeric($classDateCustomerID) || $classDateCustomerID <= 0){
            throw new Exception("Illegal value classDateCustomerID");             
        }

        $this->db->execute("DELETE FROM tbl_class_date_customers WHERE cdc_id = $classDateCustomerID");
    }

    /************************************* */
    /*   BASIC EMPLOYEEMEETING - PUBLIC           */
    /************************************* */

    /**
    * Insert new employeeMeetingObject to DB
    * Return Data = new employeeMeetingObject ID
    * @param employeeMeetingObject $employeeMeetingObj 
    * @return resultObject
    */
    public static function addEmployeeMeeting(employeeMeetingObject $employeeMeetingObj){       
            try{
                $instance = new self();
                $newId = $instance->addEmployeeMeetingDB($employeeMeetingObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeMeetingObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get employeeMeeting from DB for provided ID
    * * Return Data = employeeMeetingObject
    * @param int $employeeMeetingId 
    * @return resultObject
    */
    public static function getEmployeeMeetingByID($employeeMeetingId){
        
            try {
                $instance = new self();
                $employeeMeetingData = $instance->loadEmployeeMeetingFromDB($employeeMeetingId);
            
                $employeeMeetingObj = employeeMeetingObject::withData($employeeMeetingData);
                $result = resultObject::withData(1,'',$employeeMeetingObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeMeetingId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update a1History in DB
    * @param employeeMeetingObject $employeeMeetingObj 
    * @return resultObject
    */
    public static function updateEmployeeMeeting(employeeMeetingObject $employeeMeetingObj){        
            try{
                $instance = new self();
                $instance->upateEmployeeMeetingDB($employeeMeetingObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeMeetingObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete employeeMeeting from DB
    * @param int $employeeMeetingID 
    * @return resultObject
    */
    public static function deleteEmployeeMeetingById($employeeMeetingID){
            try{
                $instance = new self();
                $instance->deleteEmployeeMeetingByIdDB($employeeMeetingID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeMeetingID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
     * Get the count of customers scheduled meetings for the last X days
     * Return Data = count of the meetings
     * @param mixed $cust_id 
     * @param mixed $period 
     * @return resultObject
     */
    public static function getCustMeetingCountForLastXDays($cust_id,$days){
        
        try {
            $instance = new self();
            $cnt = $instance->getCustMeetingCountForLastXDays_DB($cust_id,$days);
            
            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get the count of customers scheduled meetings
     * Return Data = count of the meetings
     * @param mixed $cust_id 
     * @param mixed $period 
     * @return resultObject
     */
    public static function getCustMeetingCount($cust_id){
        
        try {
            $instance = new self();
            $cnt = $instance->getCustMeetingCount_DB($cust_id);
            
            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }



    /************************************* */
    /*   BASIC EMPLOYEEMEETING - DB METHODS           */
    /************************************* */

    private function addEmployeeMeetingDB(employeeMeetingObject $obj){

        if (!isset($obj)){
            throw new Exception("employeeMeetingObject value must be provided");             
        }

        $em_start_dateDate = isset($obj->em_start_date) ? "'".$obj->em_start_date."'" : "null";

        $em_end_dateDate = isset($obj->em_end_date) ? "'".$obj->em_end_date."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_employee_meeting SET 
                                        em_emp_id = {$obj->em_emp_id},
                                        em_biz_id = {$obj->em_biz_id},
                                        em_name = '".addslashes($obj->em_name)."',
                                        em_class_id = {$obj->em_class_id},
                                        em_class_date_id = {$obj->em_class_date_id},
                                        em_start_date = $em_start_dateDate,
                                        em_start_time = {$obj->em_start_time},
                                        em_start = '{$obj->em_start}',
                                        em_end_date = $em_end_dateDate,
                                        em_end_time = {$obj->em_end_time},
                                        em_end = '{$obj->em_end}',
                                        em_color = '".addslashes($obj->em_color)."',
                                        em_from_mobile = {$obj->em_from_mobile},
                                        em_cust_device_id = '".addslashes($obj->em_cust_device_id)."',
                                        em_cust_id = {$obj->em_cust_id},
                                        em_cust_name = '".addslashes($obj->em_cust_name)."',
                                        em_cust_cancelled = {$obj->em_cust_cancelled},
                                        em_meet_type = {$obj->em_meet_type},
                                        em_cust_request = '".addslashes($obj->em_cust_request)."',
                                        em_is_recurring = '{$obj->em_is_recurring}',
                                        em_recurring_group = {$obj->em_recurring_group},
                                        em_external_id = '".addslashes($obj->em_external_id)."',
                                        em_source = '{$obj->em_source}',
                                        em_all_day_event = {$obj->em_all_day_event},
                                        em_payment_source = '{$obj->em_payment_source}',
                                        em_payment_source_id = {$obj->em_payment_source_id}
                                                 ");
         return $newId;
    }

    private function loadEmployeeMeetingFromDB($employeeMeetingID){

        if (!is_numeric($employeeMeetingID) || $employeeMeetingID <= 0){
            throw new Exception("Illegal value employeeMeetingID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_employee_meeting WHERE em_id = $employeeMeetingID");
    }

    private function upateEmployeeMeetingDB(employeeMeetingObject $obj){

        if (!isset($obj->em_id) || !is_numeric($obj->em_id) || $obj->em_id <= 0){
            throw new Exception("employeeMeetingObject value must be provided");             
        }

        $em_scheduled_onDate = isset($obj->em_scheduled_on) ? "'".$obj->em_scheduled_on."'" : "null";

        $em_start_dateDate = isset($obj->em_start_date) ? "'".$obj->em_start_date."'" : "null";

        $em_end_dateDate = isset($obj->em_end_date) ? "'".$obj->em_end_date."'" : "null";

        $this->db->execute("UPDATE tbl_employee_meeting SET 
        em_emp_id = {$obj->em_emp_id},
        em_biz_id = {$obj->em_biz_id},
        em_name = '".addslashes($obj->em_name)."',
        em_scheduled_on = $em_scheduled_onDate,
        em_class_id = {$obj->em_class_id},
        em_class_date_id = {$obj->em_class_date_id},
        em_start_date = $em_start_dateDate,
        em_start_time = {$obj->em_start_time},
        em_start = '{$obj->em_start}',
        em_end_date = $em_end_dateDate,
        em_end_time = {$obj->em_end_time},
        em_end = '{$obj->em_end}',
        em_color = '".addslashes($obj->em_color)."',
        em_from_mobile = {$obj->em_from_mobile},
        em_cust_device_id = '".addslashes($obj->em_cust_device_id)."',
        em_cust_id = {$obj->em_cust_id},
        em_cust_name = '".addslashes($obj->em_cust_name)."',
        em_cust_cancelled = {$obj->em_cust_cancelled},
        em_meet_type = {$obj->em_meet_type},
        em_cust_request = '".addslashes($obj->em_cust_request)."',
        em_is_recurring = '{$obj->em_is_recurring}',
        em_recurring_group = {$obj->em_recurring_group},
        em_external_id = '".addslashes($obj->em_external_id)."',
        em_source = '{$obj->em_source}',
        em_all_day_event = {$obj->em_all_day_event},
        em_payment_source = '{$obj->em_payment_source}',
        em_payment_source_id = {$obj->em_payment_source_id}
        WHERE em_id = {$obj->em_id} 
                 ");
    }

    private function deleteEmployeeMeetingByIdDB($employeeMeetingID){

        if (!is_numeric($employeeMeetingID) || $employeeMeetingID <= 0){
            throw new Exception("Illegal value employeeMeetingID");             
        }

        $this->db->execute("DELETE FROM tbl_employee_meeting WHERE em_id = $employeeMeetingID");
    }

    private function getCustMeetingCountForLastXDays_DB($cust_id,$days = 0){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");             
        }

        return $this->db->getVal("SELECT COUNT(*) FROM tbl_employee_meeting
                WHERE em_cust_id = $cust_id
                AND em_scheduled_on >= (NOW() - INTERVAL $days DAY)");
    }

    private function getCustMeetingCount_DB($cust_id){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");             
        }

        return $this->db->getVal("SELECT COUNT(*) 
                                    FROM tbl_employee_meeting
                                    WHERE em_cust_id = $cust_id");
    }

    private function getEmployeeMeetingForCustomerDB(customerObject $customer,$start_date){
        $sql = "SELECT * FROM tbl_employee_meeting,tbl_employee
                WHERE em_end_date >= STR_TO_DATE('$start_date','%d/%m/%Y')
                AND em_cust_id = {$customer->cust_id}
                AND em_biz_id = {$customer->cust_biz_id}
                AND em_emp_id = be_id ";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC EMPLOYEE - PUBLIC           */
    /************************************* */

    /**
     * Insert new employeeObject to DB
     * Return Data = new employeeObject ID
     * @param employeeObject $employeeObj 
     * @return resultObject
     */
    public static function addEmployee(employeeObject $employeeObj){       
        try{
            $instance = new self();
            $newId = $instance->addEmployeeDB($employeeObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get employee from DB for provided ID
     * * Return Data = employeeObject
     * @param int $employeeId 
     * @return resultObject
     */
    public static function getEmployeeByID($employeeId){
        
        try {
            $instance = new self();
            $employeeData = $instance->loadEmployeeFromDB($employeeId);
            
            $employeeObj = employeeObject::withData($employeeData);
            $result = resultObject::withData(1,'',$employeeObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update employeeObject in DB
     * @param employeeObject $employeeObj 
     * @return resultObject
     */
    public static function updateEmployee(employeeObject $employeeObj){        
        try{
            $instance = new self();
            $instance->upateEmployeeDB($employeeObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete employee from DB
     * @param int $employeeID 
     * @return resultObject
     */
    public static function deleteEmployeeById($employeeID){
        try{
            $instance = new self();
            $instance->deleteEmployeeByIdDB($employeeID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeePic(employeeObject $employee){
        return isset($employee->be_pic) && $employee->be_pic == "" ? "https://storage.googleapis.com/paptap/system/no_employee_image.png" : utilityManager::getImageThumb($employee->be_pic);
    }    

    /************************************* */
    /*   BASIC EMPLOYEE - DB METHODS           */
    /************************************* */

    private function addEmployeeDB(employeeObject $obj){

        if (!isset($obj)){
            throw new Exception("employeeObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_employee SET 
                                    be_biz_id = {$obj->be_biz_id},
                                    be_account_id = {$obj->be_account_id},
                                    be_name = '".addslashes($obj->be_name)."',
                                    be_mail = '".addslashes($obj->be_mail)."',
                                    be_pass = '".addslashes($obj->be_pass)."',
                                    be_job = '".addslashes($obj->be_job)."',
                                    be_active = {$obj->be_active},
                                    be_pic = '".addslashes($obj->be_pic)."',
                                    be_default_color = {$obj->be_default_color},
                                    be_phone = '".addslashes($obj->be_phone)."',
                                    be_cal_type = '{$obj->be_cal_type}',
                                    be_cal_id = '".addslashes($obj->be_cal_id)."',
                                    be_cal_name = '".addslashes($obj->be_cal_name)."',
                                    be_token = '".addslashes($obj->be_token)."',
                                    be_event_color = '".addslashes($obj->be_event_color)."',
                                    be_reminder_time = {$obj->be_reminder_time},
                                    be_sync_token = '".addslashes($obj->be_sync_token)."',
                                    be_token_expiration = '{$obj->be_token_expiration}',
                                    be_ipn_push_id = '".addslashes($obj->be_ipn_push_id)."',
                                    be_ipn_expiration = '{$obj->be_ipn_expiration}',
                                    be_apple_user = '".addslashes($obj->be_apple_user)."',
                                    be_apple_pass = '".addslashes($obj->be_apple_pass)."',
                                    be_outlook_user = '".addslashes($obj->be_outlook_user)."'
                                             ");
        return $newId;
    }

    private function loadEmployeeFromDB($employeeID){

        if (!is_numeric($employeeID) || $employeeID <= 0){
            throw new Exception("Illegal value employeeID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_employee WHERE be_id = $employeeID");
    }

    private function upateEmployeeDB(employeeObject $obj){

        if (!isset($obj->be_id) || !is_numeric($obj->be_id) || $obj->be_id <= 0){
            throw new Exception("employeeObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_employee SET 
                            be_biz_id = {$obj->be_biz_id},
                            be_account_id = {$obj->be_account_id},
                            be_name = '".addslashes($obj->be_name)."',
                            be_mail = '".addslashes($obj->be_mail)."',
                            be_pass = '".addslashes($obj->be_pass)."',
                            be_job = '".addslashes($obj->be_job)."',
                            be_active = {$obj->be_active},
                            be_pic = '".addslashes($obj->be_pic)."',
                            be_default_color = {$obj->be_default_color},
                            be_phone = '".addslashes($obj->be_phone)."',
                            be_cal_type = '{$obj->be_cal_type}',
                            be_cal_id = '".addslashes($obj->be_cal_id)."',
                            be_cal_name = '".addslashes($obj->be_cal_name)."',
                            be_token = '".addslashes($obj->be_token)."',
                            be_event_color = '".addslashes($obj->be_event_color)."',
                            be_reminder_time = {$obj->be_reminder_time},
                            be_sync_token = '".addslashes($obj->be_sync_token)."',
                            be_token_expiration = '{$obj->be_token_expiration}',
                            be_ipn_push_id = '".addslashes($obj->be_ipn_push_id)."',
                            be_ipn_expiration = '{$obj->be_ipn_expiration}',
                            be_apple_user = '".addslashes($obj->be_apple_user)."',
                            be_apple_pass = '".addslashes($obj->be_apple_pass)."',
                            be_outlook_user = '".addslashes($obj->be_outlook_user)."'
                            WHERE be_id = {$obj->be_id} 
                                     ");
    }

    private function deleteEmployeeByIdDB($employeeID){

        if (!is_numeric($employeeID) || $employeeID <= 0){
            throw new Exception("Illegal value employeeID");             
        }

        $this->db->execute("DELETE FROM tbl_employee WHERE be_id = $employeeID");
    }

    private function getEmployeesRowsForService(serviceObject $service){
        $sql = "SELECT * FROM tbl_employee_meet_type,tbl_employee
                WHERE be_biz_id = {$service->bmt_biz_id}
                AND emt_bmt_id = {$service->bmt_id}
                AND emt_emp_id = be_id
                AND be_active = 1
                AND emt_active = 1
                ORDER BY be_id ASC";

        return $this->db->getTable($sql);
    }

    private function getEmployeeRowsForBizID($bizID){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type,tbl_employee_meet_type,tbl_employee
                    WHERE be_biz_id = $bizID
		            AND emt_emp_id = be_id
                    AND emt_bmt_id = bmt_id
                    AND emt_active = 1 
                    AND bmt_active = 1
                    AND be_active = 1
                    AND bmt_mobile_active = 1
                    GROUP BY be_id";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   Booking utilities                 */
    /************************************* */

    private function getStartHourIDFromDate($start_date,$offset){
        if ($start_date == ''){ 
            $start_date = mktime(0, 0, 0);
            $nowTimeStamp = time() + 60*60 + $offset;
    
        }else{


            $qure_date = mktime(0, 0, 0);
    
            $day = date('d',$start_date);
            $month = date('m',$start_date);
            $year = date('Y',$start_date);
            $start_date = mktime(0, 0, 0, $month, $day, $year);
    
            if($start_date <= $qure_date) {
                $start_date = $qure_date;
                $nowTimeStamp = time() + 60*60 + $offset;
            }else{

                $nowTimeStamp = mktime(0, 0, 0);
            }
        }

        $nowHour = date('H',$nowTimeStamp);
        $nowHour .= ":00";

        return $this->db->getVal("SELECT hour_id FROM calendar_hours WHERE hour_start = '$nowHour'");
    }

    private function getMeetingListBoth(employeeObject $employee,serviceObject $service,$start_date,$startHourID){
        $bizID = $service->bmt_biz_id;
        $meetSize = $service->bmt_size;
        if ($this->currentRecordNo >= $this->maxRecords){
            return;
        }       

        $numLoops = 0;
        while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){
            $startDateDayOfWeek = date('w', $start_date) - 1;
            if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
            
            $startDate = date( "Y-m-d",$start_date);
            
            $end_date = $start_date + 5*$meetSize*60;
            $endDate = date( "Y-m-d",$end_date);
            
            $allSlots = $this->getAllSlotsFromSpecificHour($bizID,$service,$employee,$startDateDayOfWeek,$startHourID);

            if (count($allSlots) > 0){
                $takenSlots = $this->getAllTakenBetween($employee,$startDate,$endDate);

                foreach ($allSlots as $oneSlot) {
                    $slotStartHour  = $oneSlot["hour_id"];
                    
                    $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                    $slotEndTime = $slotStartTime + 5*60*$meetSize;
                    
                    
                    $isTaken = 0;
                    $takenEndHourId = 0;
                    if (count($takenSlots) > 0 ){


                        foreach ($takenSlots as $taken) {
                            $takenStartTime = strtotime($taken["em_start"]);
                            $takenEndTime =  strtotime($taken["em_end"]);
                            $takenEndHourId = $taken["em_end_time"];
                            
                            if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                //It is not overlaping
                                $isTaken = 0;
                            }else{
                                $isTaken = 1;
                                break;
                            }
                        }
                    }

                    if ($this->currentRecordNo == $this->maxRecords){
                        return;
                    }

                    if ($isTaken == 0){
                        $slotData = array();
                        $slotData["meet_emp_id"] = $employee->be_id;
                        $slotData["meet_emp_name"] = $employee->be_name;
                        $slotData["meet_emp_pic"] = self::getEmployeePic($employee);
                        $slotData["meet_type_id"] = $service->bmt_id;
                        $slotData["meet_type_name"] = $service->bmt_name;
                        $slotData["meet_price"] = $service->bmt_price;
                        $slotData["meet_start_timestamp"] = $slotStartTime;
                        $slotData["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                        $slotData["meet_end_timestamp"] = $slotEndTime;
                        $slotData["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);

                        $this->slotsList[$this->index] = $slotData;
                        $this->index++;
                        $this->currentRecordNo++;
                    }else{
                        $day = date('d',$takenEndTime);
                        $month = date('m',$takenEndTime);
                        $year = date('Y',$takenEndTime);
                        $goTo = mktime(0, 0, 0, $month, $day, $year);
                        
                        $this->getMeetingListBoth($employee,$service,$goTo,($takenEndHourId + 1));
                        
                    }

                    if ($this->currentRecordNo == $this->maxRecords){
                        return;
                    }
                }
            }
            else{
                $numLoops = $numLoops + 1;
            }
            $startHourID = 0;
            $start_date = $start_date + 24*60*60;
        }

        return;
    }

    private function getMeetingListOnlyMeetingType(serviceObject $service,$start_date,$startHourID){
        $bizID = $service->bmt_biz_id;
        $meetSize = $service->bmt_size;
        if ($this->currentRecordNo >= $this->maxRecords){
            return;
        }  

        $employees = $this->getEmployeesRowsForService($service);

        if (count($employees) > 0){
            $numLoops = 0;
            while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){

                $end_date = $start_date + 5*$meetSize*60;

                foreach ($employees as $employeeRow)
                {
                	$employee = employeeObject::withData($employeeRow);

                    $startDateDayOfWeek = date('w', $start_date) - 1;
                    if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
                    $startDate = date( "Y-m-d",$start_date);
                    
                    $endDate = date( "Y-m-d",$end_date);

                    $allSlots = $this->getAllSlotsFromSpecificHour($bizID,$service,$employee,$startDateDayOfWeek,$startHourID);

                    if (count($allSlots) > 0){
                        $takenSlots = $this->getAllTakenBetween($employee,$startDate,$endDate);

                        foreach ($allSlots as $oneSlot) {
                            $slotStartHour  = $oneSlot["hour_id"];
                            
                            $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                            $slotEndTime = $slotStartTime + 5*60*$meetSize;
                            
                            
                            $isTaken = 0;
                            $takenEndHourId = 0;
                            if (count($takenSlots) > 0 ){


                                foreach ($takenSlots as $taken) {
                                    $takenStartTime = strtotime($taken["em_start"]);
                                    $takenEndTime =  strtotime($taken["em_end"]);
                                    $takenEndHourId = $taken["em_end_time"];
                                    
                                    if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                        //It is not overlaping
                                        $isTaken = 0;
                                    }else{
                                        $isTaken = 1;
                                        break;
                                    }
                                }
                            }

                            if ($this->currentRecordNo == $this->maxRecords){
                                return;
                            }

                            if ($isTaken == 0){
                                $slotData = array();
                                $slotData["meet_emp_id"] = $employee->be_id;
                                $slotData["meet_emp_name"] = $employee->be_name;
                                $slotData["meet_emp_pic"] = self::getEmployeePic($employee);
                                $slotData["meet_type_id"] = $service->bmt_id;
                                $slotData["meet_type_name"] = $service->bmt_name;
                                $slotData["meet_price"] = $service->bmt_price;
                                $slotData["meet_start_timestamp"] = $slotStartTime;
                                $slotData["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                                $slotData["meet_end_timestamp"] = $slotEndTime;
                                $slotData["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);

                                $this->slotsList[$this->index] = $slotData;
                                $this->index++;
                                $this->currentRecordNo++;
                            }else{
                                $day = date('d',$takenEndTime);
                                $month = date('m',$takenEndTime);
                                $year = date('Y',$takenEndTime);
                                $goTo = mktime(0, 0, 0, $month, $day, $year);
                                
                                $this->getMeetingListBoth($employee,$service,$goTo,($takenEndHourId + 1));
                                
                            }

                            if ($this->currentRecordNo == $this->maxRecords){
                                return;
                            }
                        }
                    }
                    else{
                        $numLoops = $numLoops + 1;
                    }
                    $startHourID = 0;
                    $start_date = $start_date + 24*60*60;
                }
                
            }
        }
    }

    private function getMeetingListOnlyEmployee(employeeObject $employee,$start_date,$startHourID){
        $bizID = $employee->be_biz_id;

        if ($this->currentRecordNo >= $this->maxRecords){
            return;
        }       

        $numLoops = 0;
        while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){
            $services = $this->getServicesForEmployee($employee);

            if(count($services) > 0){
                $numLoops = 0;
                while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){
                    foreach ($services as $serviceRow)
                    {
                    	$service = serviceObject::withData($serviceRow);

                        $meetSize = $service->bmt_size;

                        $startDateDayOfWeek = date('w', $start_date) - 1;
                        if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
                        
                        $startDate = date( "Y-m-d",$start_date);
                        
                        $end_date = $start_date + 5*$meetSize*60;
                        $endDate = date( "Y-m-d",$end_date);

                        $allSlots = $this->getAllSlotsFromSpecificHour($bizID,$service,$employee,$startDateDayOfWeek,$startHourID);
                        
                        if (count($allSlots) > 0){
                            $takenSlots = $this->getAllTakenBetween($employee,$startDate,$endDate);

                            foreach ($allSlots as $oneSlot) {
                                $slotStartHour  = $oneSlot["hour_id"];
                                
                                $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                                $slotEndTime = $slotStartTime + 5*60*$meetSize;
                                
                                
                                $isTaken = 0;
                                $takenEndHourId = 0;
                                if (count($takenSlots) > 0 ){


                                    foreach ($takenSlots as $taken) {
                                        $takenStartTime = strtotime($taken["em_start"]);
                                        $takenEndTime =  strtotime($taken["em_end"]);
                                        $takenEndHourId = $taken["em_end_time"];
                                        
                                        if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                            //It is not overlaping
                                            $isTaken = 0;
                                        }else{
                                            $isTaken = 1;
                                            break;
                                        }
                                    }
                                }

                                if ($this->currentRecordNo == $this->maxRecords){
                                    return;
                                }

                                if ($isTaken == 0){
                                    $slotData = array();
                                    $slotData["meet_emp_id"] = $employee->be_id;
                                    $slotData["meet_emp_name"] = $employee->be_name;
                                    $slotData["meet_emp_pic"] = self::getEmployeePic($employee);
                                    $slotData["meet_type_id"] = $service->bmt_id;
                                    $slotData["meet_type_name"] = $service->bmt_name;
                                    $slotData["meet_price"] = $service->bmt_price;
                                    $slotData["meet_start_timestamp"] = $slotStartTime;
                                    $slotData["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                                    $slotData["meet_end_timestamp"] = $slotEndTime;
                                    $slotData["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);

                                    $this->slotsList[$this->index] = $slotData;
                                    $this->index++;
                                    $this->currentRecordNo++;
                                }else{
                                    $day = date('d',$takenEndTime);
                                    $month = date('m',$takenEndTime);
                                    $year = date('Y',$takenEndTime);
                                    $goTo = mktime(0, 0, 0, $month, $day, $year);
                                    
                                    $this->getMeetingListBoth($employee,$service,$goTo,($takenEndHourId + 1));
                                    
                                }

                                if ($this->currentRecordNo == $this->maxRecords){
                                    return;
                                }
                            }
                        }
                        else{
                            $numLoops = $numLoops + 1;
                        }
                        $startHourID = 0;
                        $start_date = $start_date + 24*60*60;
                    }
                    
                }
            }
        }
    }

    private function getAllSlotsFromSpecificHour($bizID,serviceObject $service,employeeObject $employee,$startDateDayOfWeek,$startHourID){
        
       
        
        $size = $service->bmt_size;
        if ($size > 144){
            $size = 36;
        }

        $calSettings = bizManager::getBizCalendarSettingsByBizID($bizID);

       

        if(!isset($calSettings)){
            throw new Exception("bizCalendarSettingsObject is null");
        }

        

        $onlyBizHours = $calSettings->biz_cal_biz_hours_only;
        $bizSlotsGap = $calSettings->biz_cal_slot_gap;
        $canOverTime = $calSettings->biz_cal_allow_overtime == 1;

        if($bizSlotsGap < 144){
            $modulo = "AND MOD(hour_id - tbl_employee_hours.bch_begin_hour, $bizSlotsGap) = 0 ";
        }
        else{
            $modulo = " AND MOD(hour_id,$bizSlotsGap) = tbl_employee_hours.bch_begin_hour";
        }

        if($onlyBizHours == 1){
            if($canOverTime){
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_biz_cal_hours,tbl_employee_hours
                                    WHERE bch_biz_id = $bizID
                                    AND bch_emp_id = {$employee->be_id}
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_working_day = 1
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id between GREATEST($startHourID,tbl_biz_cal_hours.bch_begin_hour) and tbl_biz_cal_hours.bch_finish_hour
                                    AND hour_id >= GREATEST($startHourID,tbl_employee_hours.bch_begin_hour)
                                    $modulo
                                    ORDER BY hour_id";
            }
            else{
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_biz_cal_hours,tbl_employee_hours
                                    WHERE bch_biz_id = $bizID
                                    AND bch_emp_id = {$employee->be_id}
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_working_day = 1
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id between GREATEST($startHourID,tbl_biz_cal_hours.bch_begin_hour) and tbl_biz_cal_hours.bch_finish_hour
                                    AND hour_id between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND (hour_id + $size) between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and (tbl_employee_hours.bch_finish_hour + 1)
                                    $modulo
                                    ORDER BY hour_id";
            }
        }
        else{
            if($canOverTime){
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_employee_hours
                                    WHERE bch_emp_id = {$employee->be_id}  
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_employee_hours.bch_working_day = 1                                    
                                    AND hour_id >= GREATEST($startHourID,tbl_employee_hours.bch_begin_hour)
                                    $modulo
                                    ORDER BY hour_id";
            }
            else{
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_employee_hours
                                    WHERE bch_emp_id = {$employee->be_id}  
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_employee_hours.bch_working_day = 1                                    
                                    AND hour_id between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND (hour_id + $size) between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and (tbl_employee_hours.bch_finish_hour + 1)
                                    $modulo
                                    ORDER BY hour_id";
            }
        }


        return $this->db->getTable($sql);
    }

    private function getAllTakenBetween(employeeObject $employee,$start,$end){
        
        
        return $this->db->getTable("SELECT * FROM tbl_employee_meeting
                                WHERE em_emp_id = {$employee->be_id}
                                AND (
                                    (
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$start','%Y-%m-%d')
                                    AND
                                    DATE_FORMAT(em_end_date , '%Y-%m-%d') >= STR_TO_DATE('$start','%Y-%m-%d')
                                   )
                                   OR 
                                   (
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') >= STR_TO_DATE('$start','%Y-%m-%d')
                                    AND
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$end','%Y-%m-%d')
                                    )
                                )
                                ");
        
    }

    private function getHourIDFromStartHour($startHour){
        return $this->db->getVal("SELECT hour_id FROM calendar_hours WHERE hour_start like '$startHour'");
    }

    private function getHourIDFromEndHour($endHour){
        return $this->db->getVal("SELECT hour_id FROM calendar_hours WHERE hour_end like '$endHour'");
    }

    private function getHourDataFromHourID($hourID){
        return $this->db->getRow("SELECT * FROM calendar_hours WHERE hour_id = $hourID");
    }

    private function isFutureDate($date){

        $currentTime = time();
        
        if ($date < $currentTime + 1*60*60){
            return false;
        }

        return true;
    }

    private function verifyAvailability($empID,$starting_date,$ending_date,$starting_time,$ending_time){       
        
        $takenSlots = $this->db->getTable("SELECT * FROM tbl_employee_meeting
                                    WHERE em_emp_id = $empID
                                    AND (
                                            (
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_start_time <= $starting_time
                                                AND
                                                DATE_FORMAT(em_end_date , '%Y-%m-%d') >= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_end_time >= $starting_time
                                            )
                                            OR 
                                            (
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') >= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_start_time >= $starting_time
                                                AND
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$ending_date','%Y-%m-%d')
                                                AND em_start_time <= $ending_time
                                            )
                                        )
                                    ");
        
        if (count($takenSlots) > 0){
            return 0;
        }

        return 1;
        
    }
}