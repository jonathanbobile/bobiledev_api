<?php

/**
 * captainOrders short summary.
 *
 * captainOrders description.
 *
 * @version 1.0
 * @author JonathanM
 */
class captainOrders
{
    protected $db;    

    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }       
    }

    /* Order Management & Handling  - Should this be in E-Commerce Manager??? */

    public function initiateEmptyOrderForCustomer($custID,$orderObject){
        if(!$orderObject->_isValid()){
            $result['code'] = -1;
            return $result;
        }

        $result = array();
        $biz_id = customerManager::getCustomerBizID($custID);
        $uniq_id = $this->generateUniuqueCustOrderId($custID,$biz_id);
        $sql = "INSERT INTO tbl_cust_transaction_order SET 
                                        cto_amount=$orderObject->amount,
                                        cto_without_vat=$orderObject->subTotal,
                                        cto_vat=$orderObject->tax,
                                        cto_currency='$orderObject->currency',
                                        cto_device_id='$orderObject->deviceID',
                                        cto_biz_id=$biz_id,
                                        cto_status=1,
                                        cto_addr_country='$orderObject->shipCountry',
                                        cto_addr_state='$orderObject->shipState',
                                        cto_addr_city='$orderObject->shipCity',
                                        cto_addr_line1='$orderObject->shipLine1',
                                        cto_addr_line2='$orderObject->shipLine2',
                                        cto_addr_postcode='$orderObject->shipZip',
                                        cto_addr_name='$orderObject->shipName',
                                        cto_addr_phone='$orderObject->shipPhone',
                                        cto_buyer_email='$orderObject->buyEmail',
                                        cto_sipping=$orderObject->shipMethodPrice,
                                        cto_membership_discount=$orderObject->membershipDiscount,
                                        cto_benefit_id = $orderObject->cartBenefitId,
                                        cto_benefit_value = $orderObject->cartBenefitValue,
                                        cto_shipping_type_id=$orderObject->shipMethodId,
                                        cto_note='$orderObject->comments',
                                        cto_unique_id = '$uniq_id',
                                        cto_cust_id=$custID";

        $orderID = $this->db->execute($sql);

        $long_order_id = substr((string)time(), -8) . $orderID;

        $sql = "UPDATE tbl_cust_transaction_order SET cto_long_order_id = '$long_order_id' WHERE cto_id = $orderID";

        $this->db->execute($sql);

        $result['code'] = 1;
        $result['order_id'] = $orderID;
        $result['long_order_id'] = $long_order_id;
        return $result;
    }

    public function generateUniuqueCustOrderId($custID,$bizID){
        $timeLast5Digits = time() % 10000;
        $toScramble = $bizID.$custID.$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 15){
            $scrambled .= rand();
        }
        
        $output = 'bbord_'.substr($scrambled,0,15);

        return $output; 
    }
    
    public function deleteCustOrder($orderID){

    }


    public function addCustItemToOrder($itemData,$orderID){
        if(!$itemData->_isValid()){
            $result['code'] = -1;
            return $result;
        }

        $result = array();
        //1. Check if order invoice is finalized - if finlized return.
        if($this->isOrderConnectedCustInvoiceFinalized($orderID)){
            $result['code'] = 0;
            return $result;
        }

        $uniq_id = $this->generateUniqueCustItemId($orderID);

        //2. create Item in DB with OrderID.
        $insert = "INSERT INTO tbl_cust_transaction_items 
                    set tri_order_id=$orderID, 
                    tri_name='$itemData->name', 
                    tri_quantity='$itemData->quantity', 
                    tri_price=$itemData->price, 
                    tri_orig_price = $itemData->origPrice,
                    tri_item_id=$itemData->item_id,
                    tri_item_type=$itemData->item_type,
                    tri_itm_row_id=$itemData->rowId, 
                    tri_redeem_code='$itemData->redeemCode',
                    tri_variation_values='$itemData->variationValues',
                    tri_item_sku='$itemData->sku',
                    tri_reward_redeem_code='$itemData->rewardRedeemCode',
                    tri_reward_Id=$itemData->rewardId,
                    tri_reward_type='$itemData->rewardType',
                    tri_reward_value='$itemData->rewardValue',
                    tri_unique_id = '$uniq_id',
                    tri_multiuse_src_type = '$itemData->multiuse_src_type',
                    tri_multiuse_src_usage_id = $itemData->multiuse_src_usage_id";
        $triID = $this->db->execute($insert);    
        

        $result['code'] = 1;
        $result['tri_id'] = $triID;
        return $result;
    }

    public function generateUniqueCustItemId($orderID){

        $order = self::getOrderDataFromOrderID($orderID);
        
        $timeLast5Digits = time() % 10000;
        $toScramble = $order['cto_biz_id'].$order['cto_cust_id'].$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 15){
            $scrambled .= rand();
        }
        
        $output = 'bbitem_'.substr($scrambled,0,15);

        return $output; 
    }

    public function removeCustItem($itemID){

        //1. Check if the invoice related to this order is finalized - if finalized return.
        //2. Recalc Order
        //3. Recalc Related Invoice
    }

    public function isOrderConnectedCustInvoiceFinalized($orderID){
        $invoices = $this->getConnectedCustInvoiceForCustOrderID($orderID);

        foreach ($invoices as $invoice)
        {
            if($invoice['cin_finalized'] == 1){
                return true;   
            }
        }        

        return false;
    }

    public function getConnectedCustInvoiceForCustOrderID($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_invoice
                WHERE cini_invoice_id = cin_id
                AND cini_order_id = $orderID";

        return $this->db->getTable($sql);
    }

    public function getConnectedOrderItemsByOrderID($orderID){
        $sql = "SELECT * FROM tbl_cust_transaction_items
                WHERE tri_order_id = $orderID";

        return $this->db->getTable($sql);
    }

    public function getOrderDataFromOrderIDInner($orderID){
        $sql = "SELECT * FROM tbl_cust_transaction_order WHERE cto_id = $orderID";

        return $this->db->getRow($sql);
    }

    public function processCustOrder($orderID){

        $orderRow = self::getOrderDataFromOrderID($orderID);

        $customerManager = new customersManager();

        $customer = $customerManager->getCustomerDetails($orderRow['cto_cust_id']);

        if($orderRow['cto_benefit_id'] != 0){//cart level discount was used - setting the reward to redeem
            $cartBenefitResult = $customerManager->redeemBenefitByID($orderRow['cto_benefit_id']);

            if($cartBenefitResult['code'] == 1){
                $orderRow['benefit_label'] = $cartBenefitResult['label'];
                eventManager::actionTrigger(23,$orderRow['cto_cust_id'],'coupon_redeemed',$orderRow['benefit_label']);
            }
        }


        $orderItems = $this->getConnectedOrderItemsByOrderID($orderID);

        
        $isNonDelevarbleItem = false;

        foreach ($orderItems as $key =>$orderItem)
        {
            if($orderItem['tri_reward_redeem_code'] != ''){//a Reward was used for this item - setting the reward as redeemd.
                $orderItems[$key]['benefit_data'] = $customerManager->enterCustRedeemCodeAPI($orderItem['tri_reward_redeem_code']);
                if(isset($orderItems[$key]['benefit_data']['label'])){
                    eventManager::actionTrigger(23,$orderRow['cto_cust_id'],'coupon_redeemed',$orderItems[$key]['benefit_data']['label']);
                }
            }

            if(isset($orderItem['tri_multiuse_src_type']) && $orderItem['tri_multiuse_src_type'] != ''){//a Reward was used for this item - setting the reward as redeemd.
                $subM = new subscriptionsManager();

                if($orderItem['tri_multiuse_src_type'] == 'subscription'){
                    
                    
                    $orderItem['multiuse_label'] = $subM->getSubscriptionNameFromUsageID($orderItem['tri_multiuse_src_usage_id']);
                }

                if($orderItem['tri_multiuse_src_type'] == 'punch_pass'){
                    
                    $orderItem['multiuse_label'] = $subM->getPunchPassNameFromUsageID($orderItem['tri_multiuse_src_usage_id']);
                }
                
            }


            if($orderItem['tri_item_type'] == 1){//a coupon was purchased
                $benefit_id = $customerManager->addCouponBenefitToCustomer($orderRow['cto_cust_id'],$orderItem['tri_id'],$orderItem['tri_redeem_code']);
                $this->setItemExtTypeAndID($orderItem['tri_id'],'coupon',$benefit_id);
                $isNonDelevarbleItem = true;
            }


            if ($orderItem['tri_item_type'] == 2 || $orderItem['tri_item_type'] == 3){//Retail Product or Paid Booking Service - nothing to handle
                continue;
            }


            if($orderItem['tri_item_type'] == 4){//a subscription was purchased
                $subM = new subscriptionsManager();
                $subResult = $subM->addSubscriptionToCustomer($orderItem['tri_item_id'],$orderRow['cto_cust_id'],$orderRow['cto_biz_id']);
                $isNonDelevarbleItem = true;
                $this->setItemExtTypeAndID($orderItem['tri_id'],'subscription',$subResult['data']);
                eventManager::actionTrigger(26,$orderRow['cto_cust_id'],'purchased_subscription',$subResult['label']);
            }


            if($orderItem['tri_item_type'] == 5){//a punchpass was purchased
                $subM = new subscriptionsManager();
                $passResult = $subM->addPunchPassToCustomer($orderItem['tri_item_id'],$orderRow['cto_cust_id'],$orderRow['cto_biz_id']);
                $isNonDelevarbleItem = true;  
                $this->setItemExtTypeAndID($orderItem['tri_id'],'punch_pass',$passResult['data']);
                eventManager::actionTrigger(27,$orderRow['cto_cust_id'],'purchased_punch_pass',$passResult['label']);
            }


        }

       
        if($isNonDelevarbleItem){
            $this->updateOrderStatus($orderID,enumOrderStatus::delivered);
        }
        else{
            $this->updateOrderStatus($orderID,enumOrderStatus::received);
        }

        //Update badges and notifiactions + push to admin 
        customersManager::addOneUnattendedOrderForCust($orderRow['cto_cust_id']);
        customersManager::addBadgeToCustomer($orderRow['cto_cust_id'],'orders'); 
        
        utilityManager::notifyBizOfAction($orderRow['cto_biz_id'],enumActions::newOrder);

        //Grant inviter points
        $inviterPoints = $orderRow['cto_without_vat'] / 2;
        customersManager::grantInviterPoints($orderRow['cto_cust_id'],$inviterPoints);

        //Send emails
        $this->sendOrderEmails($orderRow,$orderItems,$customer);               
        
    }

    public function setItemExtTypeAndID($item_id,$ext_type,$ext_id){
        $sql = "UPDATE tbl_cust_transaction_items SET
                                tri_item_ext_type = '$ext_type',
                                tri_item_ext_id = $ext_id
                            WHERE tri_id = $item_id";

        $this->db->execute($sql);
    }

    public function updateOrderStatus($orderID,$status){
        
        $this->db->execute("INSERT INTO tbl_cust_transaction_order_history SET 
                                        ctoh_order_id=$orderID,
                                        ctoh_status_id=$status"); 

        $this->db->execute("UPDATE tbl_cust_transaction_order SET 
                                cto_status = $status
                            WHERE cto_id=$orderID"); 

        return;
    }

    public function sendOrderEmails($order,$items,$customer){
        

        $itemsToMail = $this->createOrderMailBody($order,$items);
        $bizData = appManager::getBizData($order['cto_biz_id']);
        
        $orderDate = utilityManager::getFormattedTimeByCountryID($bizData['biz_addr_country_id'],$order['cto_order_tyme']);

        $extraParams["cust_name"] = $customer['cust_first_name'];
        $extraParams["order_id"] = $order["cto_long_order_id"];;
        $extraParams["tax"] = $order["cto_vat"];
        $extraParams["currency"] = $order["cto_currency"];
        $extraParams["total"] = $order["cto_amount"];
        $extraParams["order_date"] = $orderDate;
        $extraParams["items"] = $itemsToMail;
        $extraParams["ship_country"] = $order["cto_addr_country"];
        $extraParams["ship_state"] = $order["cto_addr_state"];
        $extraParams["ship_city"] = $order["cto_addr_city"];
        $extraParams["ship_address1"] = $order["cto_addr_line1"];
        $extraParams["ship_address2"] = $order["cto_addr_line2"];
        $extraParams["ship_zip"] = $order["cto_addr_postcode"];
        $extraParams["ship_name"] = $order["cto_addr_name"];
        $extraParams["ship_phone"] = $order["cto_addr_phone"];
        $extraParams["cust_email"] = $customer['cust_email'];
        $extraParams["cust_phone"] = $customer['cust_phone2'];

        //biz owner
        emailManager::sendSystemMailApp($bizData['biz_id'],147,enumEmailType::SystemMailSGCare,$extraParams);

        //customer
        $extraParams["to_name"] = $customer['cust_first_name'];
        $extraParams["to_email"] = $customer['cust_email'];
        emailManager::sendSystemMailApp($bizData['biz_id'],151,enumEmailType::SystemMailSGCare,$extraParams);

        return;
    }

    public function createOrderMailBody($order,$items){       

        $orderMailBody = "<table width=\"100%\" cellspacing=\"0\">
                            <tr>
                                <td width=\"45%\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Item</td>
                                <td width=\"15%\" style=\"text-align:center; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Qty</td>
                                <td width=\"20%\" style=\"text-align:center; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Price <span style=\"font-size:9pt;\">per unit</span></td>
                                <td width=\"20%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Total</td>
                            </tr>";

        $orderTotal = 0;

        foreach ($items as $item)
        {           

            $totalRow = $item['tri_quantity'] * $item['tri_price'];
            $orderTotal += $totalRow;
        	$totalLine = "$totalRow <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            $paddingBottom = "padding-bottom:5px;";

            $priceLine = $item['tri_price'];
            if($item['tri_orig_price'] != $item['tri_price']){
                
                $priceLine = $item['tri_orig_price'];
            }

            $rewardRow = '';
            if(isset($item['benefit_data']['label'])){

                $rewardLine = '';
                if($item['tri_reward_type'] == 'discount'){
                    $rewardLine = "({$item['tri_reward_value']}% discount)";
                }
                if($item['tri_reward_type'] == 'item'){
                    $rewardLine = "({$item['tri_reward_value']} free)";
                }

                $rewardRow = "<br><span style=\"font-size: 9pt; color: #555555;\"><span style=\"font-weight:600;\">Reward used</span>: {$item['benefit_data']['label']} $rewardLine</span>";
                $paddingBottom = "padding-bottom:0px;";
                $totalLine = "<span style=\"text-decoration:line-through;\">{$item['tri_orig_price']}</span> <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span><br>{$item['tri_price']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            }

            if(isset($item['multiuse_label'])){

                $rewardLine = "Used {$item['multiuse_label']}";
                

                $rewardRow = "<br><span style=\"font-size: 9pt; color: #555555;\"><span style=\"font-weight:600;\">Reward used</span>: {$item['benefit_data']['label']} $rewardLine</span>";
                $paddingBottom = "padding-bottom:0px;";
                $totalLine = "<span style=\"text-decoration:line-through;\">{$item['tri_orig_price']}</span> <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span><br>{$item['tri_price']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            }
            

            $orderMailBody .= " <tr>
                                <td width=\"45%\" height=\"60px\" style=\"text-align:left; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; vertical-align:top; padding-top:20px; $paddingBottom \">{$item['tri_name']}<br><span style=\"font-size: 9pt; color: #555555;\">SKU: {$item['tri_item_sku']}</span><br><span style=\"font-size: 9pt; color: #555555; line-height:10pt\">{$item['tri_variation_values']}</span>$rewardRow</td>
                                <td width=\"15%\" height=\"60px\" style=\"text-align:center; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif;  font-size:16px; vertical-align:top; padding-top:20px;\">{$item['tri_quantity']}</td>
                                <td width=\"20%\" height=\"60px\" style=\"text-align:center; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; vertical-align:top; padding-top:20px;\">$priceLine <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
                                <td width=\"20%\" height=\"60px\" style=\"text-align:right; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif;  font-size:16px; vertical-align:top; padding-top:20px; padding-right:5px;\">$totalLine</td>
                            
                            </tr>
                            ";
        }

        $subtotal = $orderTotal;
        $orderTotal = $orderTotal + $order['cto_vat'] + $order['cto_sipping'] - $order['cto_membership_discount'] - $order['cto_benefit_value'];

        $orderMailBody .= "
            <tr>
                <td colspan=\"4\" width=\"100%\" height=\"15px\"></td>
            </tr>
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px; padding-top:15px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px; padding-top:15px;\">Subtotal</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px;padding-right:5px; padding-top:15px;\">$subtotal <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>    ";
        
        if($order['cto_membership_discount'] > 0){
            $orderMailBody .= "
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; line-height:18px;\">Membership Discount</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">-{$order['cto_membership_discount']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>";
        }

        
        if($order['cto_benefit_value'] > 0 && $order['cto_benefit_id'] > 0){
            $orderMailBody .= "
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; line-height:18px;\">{$order['benefit_label']}</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">-{$order['cto_benefit_value']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>";
        }

        $orderMailBody .= "<tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\">Shipping</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">{$order['cto_sipping']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\">Tax</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">{$order['cto_vat']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; padding-top:10px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #1875d2; font-size:18px; font-weight:600; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; padding-top:10px;\">Order Total</td>
                <td width=\"25%\" style=\"text-align:right; color: #1875d2; font-size:18px; font-weight:600; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px; padding-top:10px;\">$orderTotal <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>
   
        </table>";

        return $orderMailBody;

    }

    /* External utility functions */

    public function getOrdersConnectedToInvoice($invoice_id){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_transaction_order
                WHERE cini_order_id = cto_id
                AND cini_invoice_id = $invoice_id";

        return $this->db->getTable($sql);
    }

    public static function getOrderDataFromOrderID($orderID){
        $instance = new self();

        return $instance->db->getRow("SELECT * FROM tbl_cust_transaction_order WHERE cto_id = $orderID");
        
    }
}
