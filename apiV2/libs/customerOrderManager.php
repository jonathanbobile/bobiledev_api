<?php

/**
 * customerOrderManager short summary.
 *
 * customerOrderManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerOrderManager extends Manager
{
    function __construct()
    {
        parent::__construct();        
    }    

    /************************************* */
    /*   ORDER FLOWS - PUBLIC              */
    /************************************* */

    public static function initiateEmptyOrderForCustomer($custID,customerOrderObject $orderObject){
        try{
            $instance = new self();
            $orderObject->cto_biz_id = customerManager::getCustomerBizID($custID);
            $orderObject->cto_unique_id = $instance->generateUniuqueCustOrderId($custID,$orderObject->cto_biz_id);

            $orderObject->cto_id = $instance->addCustomerOrderDB($orderObject);

            $long_order_id = substr((string)time(), -8) . $orderObject->cto_id;

            $orderObject->cto_long_order_id = $long_order_id;

            $instance->upateCustomerOrderDB($orderObject);

            return resultObject::withData(1,'',$orderObject);
            
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($orderObject));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }
    
    public static function addCustItemsToOrder($orderID,$items){
        try{
            $instance = new self();
            foreach ($items as $item)
            {
            	$item->tri_order_id = $orderID;
                $item->tri_unique_id = $instance->generateUniqueCustItemId($orderID);
                $instance->addCustomerOrderItemDB($item);
                
            }
            
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function isOrderConnectedCustInvoiceFinalized($orderID){
        $instance = new self();
        $invoices = $instance->getConnectedCustInvoiceForCustOrderID($orderID);

        foreach ($invoices as $invoice)
        {
            if($invoice['cin_finalized'] == 1){
                return true;   
            }
        }        

        return false;
    }

    public static function processCustOrder($orderID){
        try{
            $instance = new self();
            $orderRow = $instance->loadCustomerOrderFromDB($orderID);

            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($orderRow['cto_cust_id']);

            $customer = $customerResult->data;

            if($orderRow['cto_benefit_id'] != 0){//cart level discount was used - setting the reward to redeem
                
                $cartBenefitResult = customerManager::getCustomerBenefit($orderRow['cto_benefit_id']);
                if($cartBenefitResult->code == 1){
                    $cartRedeemBenefitResult = customerManager::redeemBenefitByID($orderRow['cto_benefit_id']);
                    $orderRow['benefit_label'] = customerManager::getBenefitLabel($orderRow['cto_benefit_id']);
                    eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderRow['benefit_label'],$orderRow['cto_device_id'],$cartBenefitResult->data->cb_external_id);
                }
            }


            $orderItems = $instance->getOrderItemsForOrderByOrderIDDB($orderID);

            
            $isNonDelevarbleItem = false;

            foreach ($orderItems as $key =>$orderItem)
            {
                if($orderItem['tri_reward_redeem_code'] != ''){//a Reward was used for this item - setting the reward as redeemd.
                    
                    $redeemCodeResult = customerManager::getBenefitByCode($orderItem['tri_reward_redeem_code']);
                    if($redeemCodeResult->code == 1){
                        $itemRedeemResult = customerManager::redeemBenefitWithCode($orderItem['tri_reward_redeem_code']);
                        $orderItems[$key]['benefit_label'] = $itemRedeemResult->data;

                        eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderItems[$key]['benefit_label'],$orderRow['cto_device_id'],$redeemCodeResult->data->cb_external_id);
                    }
                }

                if(isset($orderItem['tri_multiuse_src_type']) && $orderItem['tri_multiuse_src_type'] != ''){//a subscription or punch pass was used for this item
                    

                    if($orderItem['tri_multiuse_src_type'] == 'subscription'){
                        $subscriptionResult  = customerSubscriptionManager::getCustomerSubscriptionByUsageID($orderItem['tri_multiuse_src_usage_id']); 
                        if($subscriptionResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $subUsed = $subscriptionResult->data;
                            $orderItem['multiuse_label'] = $subUsed->subscription->md_head;
                        }
                    }

                    if($orderItem['tri_multiuse_src_type'] == 'punch_pass'){
                        $punchpassResult = customerSubscriptionManager::getCustomerPunchpassFromUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($punchpassResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $ppassUsed = $punchpassResult->data;
                            $orderItem['multiuse_label'] = $ppassUsed->punchpass->md_head;
                        }
                    }
                    
                }


                if($orderItem['tri_item_type'] == 1){//a coupon was purchased
                    $redeemCode = utilityManager::generateRedeemCode($orderRow['cto_cust_id']."1");

                    $orderItemObj = customerOrderItemObject::withData($orderItem);

                    $orderItemObj->tri_redeem_code = $redeemCode;

                    $instance->upateCustomerOrderItemDB($orderItemObj);

                    $benefitResult = customerManager::addRewardToCustomer($orderRow['cto_cust_id'],'coupon',$redeemCode,$orderItem['tri_id']);
                    
                    if($benefitResult->code == 1){ 
                        customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::benefits);                       
                        $instance->setItemExtTypeAndID($orderItem['tri_id'],'coupon',$benefitResult->data);                        
                    }
                    $isNonDelevarbleItem = true;
                }

                if($orderItem['tri_item_type'] == 2){//Retail Product
                    customerManager::addCustomerLoyaltyStampToCustomerFromProductPurchase($customer,$orderItem['tri_itm_row_id'],$orderItem['tri_quantity'],$orderID);
                }


                if ( $orderItem['tri_item_type'] == 3 || $orderItem['tri_item_type'] == 0){//Paid Booking Service OR 'other' from payment request
                    continue;
                }


                if($orderItem['tri_item_type'] == 4){//a subscription was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $subscriptionDataResult = $bizM->getSubscriptionBySubscriptionID($orderItem['tri_itm_row_id']);
                    if($subscriptionDataResult->code == 1){
                        $custSubResult = customerSubscriptionManager::addSubscriptionToCustomerByCustomerID($orderRow['cto_cust_id'],$subscriptionDataResult->data);
                        if($custSubResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'subscription',$custSubResult->data->csu_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedSubscription,$orderRow['cto_cust_id'],'purchased_subscription',$subscriptionDataResult->data->md_head,$orderRow['cto_device_id'],$custSubResult->data->csu_id);
                        }
                    }
                    $isNonDelevarbleItem = true; 
                }

                if($orderItem['tri_item_type'] == 5){//a punchpass was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $punchpassDataResult = $bizM->getPunchpassByPunchpassID($orderItem['tri_itm_row_id']);
                    if($punchpassDataResult->code == 1){
                        $custPunchResult = customerSubscriptionManager::addPunchpassToCustomerByCustomerID($orderRow['cto_cust_id'],$punchpassDataResult->data);
                        
                        if($custPunchResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'punch_pass',$custPunchResult->data->cpp_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedPunchPass,$orderRow['cto_cust_id'],'purchased_punch_pass',$punchpassDataResult->data->md_head,$orderRow['cto_device_id'],$custPunchResult->data->cpp_id);
                        }
                    }                    
                    $isNonDelevarbleItem = true;  
                }


            }

            
            if($isNonDelevarbleItem){
                $instance->updateOrderStatus($orderID,enumOrderStatus::delivered);
            }
            else{
                $instance->updateOrderStatus($orderID,enumOrderStatus::received);
            }

            customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$orderRow['cto_cust_id']);
            customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$orderRow['cto_cust_id']);
            

            //Update badges and notifiactions + push to admin 
            customerManager::addOneUnattendedOrderForCust($orderRow['cto_cust_id']);
            customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::orders); 
            
            utilityManager::notifyBizOfAction($orderRow['cto_biz_id'],enumActions::newOrder);

            //Grant inviter points
            $inviterPoints = $orderRow['cto_without_vat'] / 2;
            customerManager::grantInviterPoints($orderRow['cto_cust_id'],$inviterPoints);

            //Send emails
            $instance->sendOrderEmails($orderRow,$orderItems,$customer); 
            
            return resultObject::withData(1,'',$orderID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
        
    }

    public static function updateOrderitemsExternalTypeAndID($order_id,$ext_type,$ext_id){
        try{
            $instance = new self();

            $instance->setOrderItemExtTypeAndIDDB($order_id,$ext_type,$ext_id);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['orderID'] = $order_id;
            $data['extType'] = $ext_type;
            $data['extID'] = $ext_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   ORDER FLOWS - PRIVATE              */
    /************************************* */

    private function generateUniuqueCustOrderId($custID,$bizID){
        $timeLast5Digits = time() % 10000;
        $toScramble = $bizID.$custID.$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 15){
            $scrambled .= rand();
        }
        
        $output = 'bbord_'.substr($scrambled,0,15);

        return $output; 
    }

    private function generateUniqueCustItemId($orderID){

        $order = self::loadCustomerOrderFromDB($orderID);
        
        $timeLast5Digits = time() % 10000;
        $toScramble = $order['cto_biz_id'].$order['cto_cust_id'].$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 15){
            $scrambled .= rand();
        }
        
        $output = 'bbitem_'.substr($scrambled,0,15);

        return $output; 
    }

    private function getConnectedCustInvoiceForCustOrderID($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_invoice
                WHERE cini_invoice_id = cin_id
                AND cini_order_id = $orderID";

        return $this->db->getTable($sql);
    }

    private function setItemExtTypeAndID($item_id,$ext_type,$ext_id){
        $sql = "UPDATE tbl_cust_transaction_items SET
                                tri_item_ext_type = '$ext_type',
                                tri_item_ext_id = $ext_id
                            WHERE tri_id = $item_id";

        $this->db->execute($sql);
    }

    private function setOrderItemExtTypeAndIDDB($order_id,$ext_type,$ext_id){
        $sql = "UPDATE tbl_cust_transaction_items SET
                                tri_item_ext_type = '$ext_type',
                                tri_item_ext_id = $ext_id
                            WHERE tri_order_id = $order_id";

        $this->db->execute($sql);
    }

    private function updateOrderStatus($orderID,$status){
        
        $this->db->execute("INSERT INTO tbl_cust_transaction_order_history SET 
                                        ctoh_order_id=$orderID,
                                        ctoh_status_id=$status"); 

        $this->db->execute("UPDATE tbl_cust_transaction_order SET 
                                cto_status = $status
                            WHERE cto_id=$orderID"); 

        return;
    }

    private function sendOrderEmails($order,$items,$customer){
        

        $itemsToMail = $this->createOrderMailBody($order,$items);
        $bizCountryID = bizManager::getBizCountryID($order['cto_biz_id']);
        
        $orderDate = utilityManager::getFormattedTimeByCountryID($bizCountryID,$order['cto_order_tyme']);

        $extraParams["cust_name"] = $customer['cust_first_name'];
        $extraParams["order_id"] = $order["cto_long_order_id"];;
        $extraParams["tax"] = $order["cto_vat"];
        $extraParams["currency"] = $order["cto_currency"];
        $extraParams["total"] = $order["cto_amount"];
        $extraParams["order_date"] = $orderDate;
        $extraParams["items"] = $itemsToMail;
        $extraParams["ship_country"] = $order["cto_addr_country"];
        $extraParams["ship_state"] = $order["cto_addr_state"];
        $extraParams["ship_city"] = $order["cto_addr_city"];
        $extraParams["ship_address1"] = $order["cto_addr_line1"];
        $extraParams["ship_address2"] = $order["cto_addr_line2"];
        $extraParams["ship_zip"] = $order["cto_addr_postcode"];
        $extraParams["ship_name"] = $order["cto_addr_name"];
        $extraParams["ship_phone"] = $order["cto_addr_phone"];
        $extraParams["cust_email"] = $customer['cust_email'];
        $extraParams["cust_phone"] = $customer['cust_phone2'];

        //biz owner
        emailManager::sendSystemMailApp($order['cto_biz_id'],147,enumEmailType::SystemMailSGCare,$extraParams);

        //customer
        $extraParams["to_name"] = $customer['cust_first_name'];
        $extraParams["to_email"] = $customer['cust_email'];
        emailManager::sendSystemMailApp($order['cto_biz_id'],151,enumEmailType::SystemMailSGCare,$extraParams);

        return;
    }

    private function createOrderMailBody($order,$items){       

        $orderMailBody = "<table width=\"100%\" cellspacing=\"0\">
                            <tr>
                                <td width=\"45%\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Item</td>
                                <td width=\"15%\" style=\"text-align:center; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Qty</td>
                                <td width=\"20%\" style=\"text-align:center; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Price <span style=\"font-size:9pt;\">per unit</span></td>
                                <td width=\"20%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Total</td>
                            </tr>";

        $orderTotal = 0;

        foreach ($items as $item)
        {           

            $totalRow = $item['tri_quantity'] * $item['tri_price'];
            $orderTotal += $totalRow;
        	$totalLine = "$totalRow <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            $paddingBottom = "padding-bottom:5px;";

            $priceLine = $item['tri_price'];
            if($item['tri_orig_price'] != $item['tri_price']){
                
                $priceLine = $item['tri_orig_price'];
            }

            $rewardRow = '';
            if(isset($item['benefit_label'])){

                $rewardLine = '';
                if($item['tri_reward_type'] == 'discount'){
                    $rewardLine = "({$item['tri_reward_value']}% discount)";
                }
                if($item['tri_reward_type'] == 'item'){
                    $rewardLine = "({$item['tri_reward_value']} free)";
                }

                $rewardRow = "<br><span style=\"font-size: 9pt; color: #555555;\"><span style=\"font-weight:600;\">Reward used</span>: {$item['benefit_label']} $rewardLine</span>";
                $paddingBottom = "padding-bottom:0px;";
                $totalLine = "<span style=\"text-decoration:line-through;\">{$item['tri_orig_price']}</span> <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span><br>{$item['tri_price']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            }

            if(isset($item['multiuse_label'])){

                $rewardLine = "Used {$item['multiuse_label']}";
                

                $rewardRow = "<br><span style=\"font-size: 9pt; color: #555555;\"><span style=\"font-weight:600;\">Reward used</span>: {$item['benefit_label']} $rewardLine</span>";
                $paddingBottom = "padding-bottom:0px;";
                $totalLine = "<span style=\"text-decoration:line-through;\">{$item['tri_orig_price']}</span> <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span><br>{$item['tri_price']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            }
            

            $orderMailBody .= " <tr>
                                <td width=\"45%\" height=\"60px\" style=\"text-align:left; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; vertical-align:top; padding-top:20px; $paddingBottom \">{$item['tri_name']}<br><span style=\"font-size: 9pt; color: #555555;\">SKU: {$item['tri_item_sku']}</span><br><span style=\"font-size: 9pt; color: #555555; line-height:10pt\">{$item['tri_variation_values']}</span>$rewardRow</td>
                                <td width=\"15%\" height=\"60px\" style=\"text-align:center; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif;  font-size:16px; vertical-align:top; padding-top:20px;\">{$item['tri_quantity']}</td>
                                <td width=\"20%\" height=\"60px\" style=\"text-align:center; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; vertical-align:top; padding-top:20px;\">$priceLine <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
                                <td width=\"20%\" height=\"60px\" style=\"text-align:right; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif;  font-size:16px; vertical-align:top; padding-top:20px; padding-right:5px;\">$totalLine</td>
                            
                            </tr>
                            ";
        }

        $subtotal = $orderTotal;
        $orderTotal = $orderTotal + $order['cto_vat'] + $order['cto_sipping'] - $order['cto_membership_discount'] - $order['cto_benefit_value'];

        $orderMailBody .= "
            <tr>
                <td colspan=\"4\" width=\"100%\" height=\"15px\"></td>
            </tr>
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px; padding-top:15px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px; padding-top:15px;\">Subtotal</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px;padding-right:5px; padding-top:15px;\">$subtotal <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>    ";
        
        if($order['cto_membership_discount'] > 0){
            $orderMailBody .= "
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; line-height:18px;\">Membership Discount</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">-{$order['cto_membership_discount']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>";
        }

        
        if($order['cto_benefit_value'] > 0 && $order['cto_benefit_id'] > 0){
            $orderMailBody .= "
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; line-height:18px;\">{$order['benefit_label']}</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">-{$order['cto_benefit_value']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>";
        }

        $orderMailBody .= "<tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\">Shipping</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">{$order['cto_sipping']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\">Tax</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">{$order['cto_vat']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; padding-top:10px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #1875d2; font-size:18px; font-weight:600; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; padding-top:10px;\">Order Total</td>
                <td width=\"25%\" style=\"text-align:right; color: #1875d2; font-size:18px; font-weight:600; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px; padding-top:10px;\">$orderTotal <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>
   
        </table>";

        return $orderMailBody;

    }    

    /************************************* */
    /*   BASIC CUSTOMERORDERITEM - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerOrderItemObject to DB
    * Return Data = new customerOrderItemObject ID
    * @param customerOrderItemObject $customerOrderItemObj 
    * @return resultObject
    */
    public static function addCustomerOrderItem(customerOrderItemObject $customerOrderItemObj){       
            try{
                $instance = new self();
                $newId = $instance->addCustomerOrderItemDB($customerOrderItemObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderItemObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerOrderItem from DB for provided ID
    * * Return Data = customerOrderItemObject
    * @param int $customerOrderItemId 
    * @return resultObject
    */
    public static function getCustomerOrderItemByID($customerOrderItemId){
        
            try {
                $instance = new self();
                $customerOrderItemData = $instance->loadCustomerOrderItemFromDB($customerOrderItemId);
            
                $customerOrderItemObj = customerOrderItemObject::withData($customerOrderItemData);
                $result = resultObject::withData(1,'',$customerOrderItemObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderItemId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update customerOrderItem in DB
    * @param customerOrderItemObject $customerOrderItemObj 
    * @return resultObject
    */
    public static function updateCustomerOrderItem(customerOrderItemObject $customerOrderItemObj){        
            try{
                $instance = new self();
                $instance->upateCustomerOrderItemDB($customerOrderItemObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderItemObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerOrderItem from DB
    * @param int $customerOrderItemID 
    * @return resultObject
    */
    public static function deleteCustomerOrderItemById($customerOrderItemID){
            try{
                $instance = new self();
                $instance->deleteCustomerOrderItemByIdDB($customerOrderItemID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderItemID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC CUSTOMERORDERITEM - DB METHODS           */
    /************************************* */

    private function addCustomerOrderItemDB(customerOrderItemObject $obj){

    if (!isset($obj)){
                throw new Exception("customerOrderItemObject value must be provided");             
            }

    $tri_redeem_dateDate = isset($obj->tri_redeem_date) ? "'".$obj->tri_redeem_date."'" : "null";

    $newId = $this->db->execute("INSERT INTO tbl_cust_transaction_items SET 
                                    tri_order_id = {$obj->tri_order_id},
                                    tri_name = '".addslashes($obj->tri_name)."',
                                    tri_unique_id = '".addslashes($obj->tri_unique_id)."',
                                    tri_attribute_values = '".addslashes($obj->tri_attribute_values)."',
                                    tri_quantity = '".addslashes($obj->tri_quantity)."',
                                    tri_price = {$obj->tri_price},
                                    tri_orig_price = {$obj->tri_orig_price},
                                    tri_without_vat = {$obj->tri_without_vat},
                                    tri_vat = {$obj->tri_vat},
                                    tri_itm_row_id = {$obj->tri_itm_row_id},
                                    tri_item_id = {$obj->tri_item_id},
                                    tri_item_type = {$obj->tri_item_type},
                                    tri_redeem_code = '".addslashes($obj->tri_redeem_code)."',
                                    tri_redeem_date = $tri_redeem_dateDate,
                                    tri_variation_values = '".addslashes($obj->tri_variation_values)."',
                                    tri_item_sku = '".addslashes($obj->tri_item_sku)."',
                                    tri_reward_redeem_code = '".addslashes($obj->tri_reward_redeem_code)."',
                                    tri_reward_Id = {$obj->tri_reward_Id},
                                    tri_reward_type = '".addslashes($obj->tri_reward_type)."',
                                    tri_reward_value = '".addslashes($obj->tri_reward_value)."',
                                    tri_item_ext_type = '{$obj->tri_item_ext_type}',
                                    tri_item_ext_id = {$obj->tri_item_ext_id},
                                    tri_multiuse_src_type = '{$obj->tri_multiuse_src_type}',
                                    tri_multiuse_src_usage_id = {$obj->tri_multiuse_src_usage_id}
                                             ");
             return $newId;
        }

    private function loadCustomerOrderItemFromDB($customerOrderItemID){

        if (!is_numeric($customerOrderItemID) || $customerOrderItemID <= 0){
            throw new Exception("Illegal value customerOrderItemID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction_items WHERE tri_id = $customerOrderItemID");
    }

    private function upateCustomerOrderItemDB(customerOrderItemObject $obj){

    if (!isset($obj->tri_id) || !is_numeric($obj->tri_id) || $obj->tri_id <= 0){
                throw new Exception("customerOrderItemObject value must be provided");             
            }

    $tri_redeem_dateDate = isset($obj->tri_redeem_date) ? "'".$obj->tri_redeem_date."'" : "null";

    $this->db->execute("UPDATE tbl_cust_transaction_items SET 
                            tri_order_id = {$obj->tri_order_id},
                            tri_name = '".addslashes($obj->tri_name)."',
                            tri_unique_id = '".addslashes($obj->tri_unique_id)."',
                            tri_attribute_values = '".addslashes($obj->tri_attribute_values)."',
                            tri_quantity = '".addslashes($obj->tri_quantity)."',
                            tri_price = {$obj->tri_price},
                            tri_orig_price = {$obj->tri_orig_price},
                            tri_without_vat = {$obj->tri_without_vat},
                            tri_vat = {$obj->tri_vat},
                            tri_itm_row_id = {$obj->tri_itm_row_id},
                            tri_item_id = {$obj->tri_item_id},
                            tri_item_type = {$obj->tri_item_type},
                            tri_redeem_code = '".addslashes($obj->tri_redeem_code)."',
                            tri_redeem_date = $tri_redeem_dateDate,
                            tri_variation_values = '".addslashes($obj->tri_variation_values)."',
                            tri_item_sku = '".addslashes($obj->tri_item_sku)."',
                            tri_reward_redeem_code = '".addslashes($obj->tri_reward_redeem_code)."',
                            tri_reward_Id = {$obj->tri_reward_Id},
                            tri_reward_type = '".addslashes($obj->tri_reward_type)."',
                            tri_reward_value = '".addslashes($obj->tri_reward_value)."',
                            tri_item_ext_type = '{$obj->tri_item_ext_type}',
                            tri_item_ext_id = {$obj->tri_item_ext_id},
                            tri_multiuse_src_type = '{$obj->tri_multiuse_src_type}',
                            tri_multiuse_src_usage_id = {$obj->tri_multiuse_src_usage_id}
                            WHERE tri_id = {$obj->tri_id} 
                                     ");
        }

    private function deleteCustomerOrderItemByIdDB($customerOrderItemID){

            if (!is_numeric($customerOrderItemID) || $customerOrderItemID <= 0){
                throw new Exception("Illegal value customerOrderItemID");             
            }

            $this->db->execute("DELETE FROM tbl_cust_transaction_items WHERE tri_id = $customerOrderItemID");
        }

    private function getOrderItemsForOrderByOrderIDDB($orderID){
        if (!is_numeric($orderID) || $orderID <= 0){
            throw new Exception("Illegal value orderID");             
        }

        return $this->db->getTable("SELECT * FROM tbl_cust_transaction_items WHERE tri_order_id = $orderID");
    }

    /************************************* */
    /*   BASIC CUSTOMERORDER - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerOrderObject to DB
    * Return Data = new customerOrderObject ID
    * @param customerOrderObject $customerOrderObj 
    * @return resultObject
    */
    public static function addCustomerOrder(customerOrderObject $customerOrderObj){       
            try{
                $instance = new self();
                $newId = $instance->addCustomerOrderDB($customerOrderObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerOrder from DB for provided ID
    * * Return Data = customerOrderObject
    * @param int $customerOrderId 
    * @return resultObject
    */
    public static function getCustomerOrderByID($customerOrderId){
        
            try {
                $instance = new self();
                $customerOrderData = $instance->loadCustomerOrderFromDB($customerOrderId);
            
                $customerOrderObj = customerOrderObject::withData($customerOrderData);

                $itemRows = $instance->getOrderItemsForOrderByOrderIDDB($customerOrderId);

                $itemsList = array();

                foreach ($itemRows as $itemData)
                {
                	$item = customerOrderItemObject::withData($itemData);
                    $itemsList[] = $item;
                }
                
                $customerOrderObj->setItems($itemsList);

                $result = resultObject::withData(1,'',$customerOrderObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update customerOrder in DB
    * @param customerOrderObject $customerOrderObj 
    * @return resultObject
    */
    public static function updateCustomerOrder(customerOrderObject $customerOrderObj){        
            try{
                $instance = new self();
                $instance->upateCustomerOrderDB($customerOrderObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerOrder from DB
    * @param int $customerOrderID 
    * @return resultObject
    */
    public static function deleteCustomerOrderById($customerOrderID){
            try{
                $instance = new self();
                $instance->deleteCustomerOrderByIdDB($customerOrderID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
     * Get the count of customers orders for the last X days
     * Return Data = count of the orders
     * @param mixed $cust_id 
     * @param mixed $period 
     * @return resultObject
     */
    public static function getCustTransactionsOrderCountForLastXDays($cust_id,$days){
        
        try {
            $instance = new self();
            $cnt = $instance->getCustTransactionsOrderCountForLastXDays_DB($cust_id,$days);
            
            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get the count of customers orders
     * Return Data = count of the orders
     * @param mixed $cust_id 
     * @param mixed $period 
     * @return resultObject
     */
    public static function getCustTransactionsOrderCount($cust_id){
        
        try {
            $instance = new self();
            $cnt = $instance->getCustTransactionsOrderCount_DB($cust_id);
            
            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerPersonalZoneOrders(customerObject $customer,$filterType = '',$skip = 0,$take = 0){
        try{
            $instance = new self();
            $orderRows = $instance->getCustomerOrdersDB($customer,$filterType,$skip,$take);
            $ordersList = array();

            $bizModel = new bizModel($customer->cust_biz_id);

            $bizObjResult = $bizModel->getBiz();

            if($bizObjResult->code == 0){
                return resultObject::withData(0,'no_biz');
            }
            $bizobj = $bizObjResult->data;
            $storeSettingsResult = $bizModel->getBizStoreSettings();
            $storeSettings = $storeSettingsResult->data;

            foreach ($orderRows as $orderRow)
            {
            	$order = customerOrderObject::withData($orderRow);

                if($filterType != "paymentRequest" && $order->cto_due_date == ""){
                    $orderTime = new DateTime($order->cto_order_tyme);
                    $addHours = ($storeSettings->ess_shipping_time * 24) + $bizobj->biz_time_zone;
                    $orderTime->add(new DateInterval("PT{$addHours}H"));
                    $order->cto_due_date = $orderTime->format('Y-m-d H:i:s');
                }
                $itemsRows = $instance->getOrderItemsForOrderByOrderIDDB($order->cto_id);

                $order->items = array();
                foreach ($itemsRows as $itemRow)
                {
                	$item = customerOrderItemObject::withData($itemRow);
                    $order->items[] = $item;
                }
                

                $ordersList[] = $order;
            }

            return resultObject::withData(1,'',$ordersList);
            
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERORDER - DB METHODS           */
    /************************************* */

    private function addCustomerOrderDB(customerOrderObject $obj){

    if (!isset($obj)){
                throw new Exception("customerOrderObject value must be provided");             
            }

    $cto_due_dateDate = isset($obj->cto_due_date) ? "'".$obj->cto_due_date."'" : "null";

    $cto_next_reminderDate = isset($obj->cto_next_reminder) ? "'".$obj->cto_next_reminder."'" : "null";

    $cto_seen_onDate = isset($obj->cto_seen_on) ? "'".$obj->cto_seen_on."'" : "null";

    $newId = $this->db->execute("INSERT INTO tbl_cust_transaction_order SET 
                                    cto_amount = {$obj->cto_amount},
                                    cto_without_vat = {$obj->cto_without_vat},
                                    cto_vat = {$obj->cto_vat},
                                    cto_sipping = {$obj->cto_sipping},
                                    cto_membership_discount = {$obj->cto_membership_discount},
                                    cto_benefit_id = {$obj->cto_benefit_id},
                                    cto_benefit_value = {$obj->cto_benefit_value},
                                    cto_shipping_type_id = {$obj->cto_shipping_type_id},
                                    cto_currency = '".addslashes($obj->cto_currency)."',
                                    cto_device_id = '".addslashes($obj->cto_device_id)."',
                                    cto_cust_id = {$obj->cto_cust_id},
                                    cto_biz_id = {$obj->cto_biz_id},
                                    cto_cancelled = {$obj->cto_cancelled},
                                    cto_status = {$obj->cto_status},
                                    cto_note = '".addslashes($obj->cto_note)."',
                                    cto_addr_name = '".addslashes($obj->cto_addr_name)."',
                                    cto_addr_country = '".addslashes($obj->cto_addr_country)."',
                                    cto_addr_state = '".addslashes($obj->cto_addr_state)."',
                                    cto_addr_city = '".addslashes($obj->cto_addr_city)."',
                                    cto_addr_line1 = '".addslashes($obj->cto_addr_line1)."',
                                    cto_addr_line2 = '".addslashes($obj->cto_addr_line2)."',
                                    cto_addr_pob = {$obj->cto_addr_pob},
                                    cto_addr_postcode = '".addslashes($obj->cto_addr_postcode)."',
                                    cto_addr_phone = '".addslashes($obj->cto_addr_phone)."',
                                    cto_buyer_email = '".addslashes($obj->cto_buyer_email)."',
                                    cto_long_order_id = '".addslashes($obj->cto_long_order_id)."',
                                    cto_paid_manual = {$obj->cto_paid_manual},
                                    cto_order_type = '{$obj->cto_order_type}',
                                    cto_due_date = $cto_due_dateDate,
                                    cto_title = '".addslashes($obj->cto_title)."',
                                    cto_unique_id = '".addslashes($obj->cto_unique_id)."',
                                    cto_reminder_type = '{$obj->cto_reminder_type}',
                                    cto_reminder_time = '{$obj->cto_reminder_time}',
                                    cto_next_reminder = $cto_next_reminderDate,
                                    cto_reminder_text = '".addslashes($obj->cto_reminder_text)."',
                                    cto_discount = {$obj->cto_discount},
                                    cto_seen_on = $cto_seen_onDate
                                             ");
             return $newId;
        }

    private function loadCustomerOrderFromDB($customerOrderID){

        if (!is_numeric($customerOrderID) || $customerOrderID <= 0){
            throw new Exception("Illegal value customerOrderID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                                    LEFT JOIN tbl_ship_method ON cto_shipping_type_id = ship_method_id
                                    WHERE cto_status=ors_id
                                    AND cto_id = $customerOrderID");
    }

    private function upateCustomerOrderDB(customerOrderObject $obj){

    if (!isset($obj->cto_id) || !is_numeric($obj->cto_id) || $obj->cto_id <= 0){
                throw new Exception("customerOrderObject value must be provided");             
            }

    $cto_order_tymeDate = isset($obj->cto_order_tyme) ? "'".$obj->cto_order_tyme."'" : "null";

    $cto_status_dateDate = isset($obj->cto_status_date) ? "'".$obj->cto_status_date."'" : "null";

    $cto_due_dateDate = isset($obj->cto_due_date) ? "'".$obj->cto_due_date."'" : "null";

    $cto_next_reminderDate = isset($obj->cto_next_reminder) ? "'".$obj->cto_next_reminder."'" : "null";

    $cto_seen_onDate = isset($obj->cto_seen_on) ? "'".$obj->cto_seen_on."'" : "null";

    $this->db->execute("UPDATE tbl_cust_transaction_order SET 
                            cto_amount = {$obj->cto_amount},
                            cto_without_vat = {$obj->cto_without_vat},
                            cto_original_amount = {$obj->cto_original_amount},
                            cto_vat = {$obj->cto_vat},
                            cto_sipping = {$obj->cto_sipping},
                            cto_membership_discount = {$obj->cto_membership_discount},
                            cto_benefit_id = {$obj->cto_benefit_id},
                            cto_benefit_value = {$obj->cto_benefit_value},
                            cto_shipping_type_id = {$obj->cto_shipping_type_id},
                            cto_currency = '".addslashes($obj->cto_currency)."',
                            cto_order_tyme = $cto_order_tymeDate,
                            cto_device_id = '".addslashes($obj->cto_device_id)."',
                            cto_cust_id = {$obj->cto_cust_id},
                            cto_biz_id = {$obj->cto_biz_id},
                            cto_cancelled = {$obj->cto_cancelled},
                            cto_status = {$obj->cto_status},
                            cto_note = '".addslashes($obj->cto_note)."',
                            cto_status_date = $cto_status_dateDate,
                            cto_addr_name = '".addslashes($obj->cto_addr_name)."',
                            cto_addr_country = '".addslashes($obj->cto_addr_country)."',
                            cto_addr_state = '".addslashes($obj->cto_addr_state)."',
                            cto_addr_city = '".addslashes($obj->cto_addr_city)."',
                            cto_addr_line1 = '".addslashes($obj->cto_addr_line1)."',
                            cto_addr_line2 = '".addslashes($obj->cto_addr_line2)."',
                            cto_addr_pob = {$obj->cto_addr_pob},
                            cto_addr_postcode = '".addslashes($obj->cto_addr_postcode)."',
                            cto_addr_phone = '".addslashes($obj->cto_addr_phone)."',
                            cto_buyer_email = '".addslashes($obj->cto_buyer_email)."',
                            cto_long_order_id = '".addslashes($obj->cto_long_order_id)."',
                            cto_paid_manual = {$obj->cto_paid_manual},
                            cto_order_type = '{$obj->cto_order_type}',
                            cto_due_date = $cto_due_dateDate,
                            cto_title = '".addslashes($obj->cto_title)."',
                            cto_unique_id = '".addslashes($obj->cto_unique_id)."',
                            cto_reminder_type = '{$obj->cto_reminder_type}',
                            cto_reminder_time = '{$obj->cto_reminder_time}',
                            cto_next_reminder = $cto_next_reminderDate,
                            cto_reminder_text = '".addslashes($obj->cto_reminder_text)."',
                            cto_discount = {$obj->cto_discount},
                            cto_seen_on = $cto_seen_onDate
                            WHERE cto_id = {$obj->cto_id} 
                                     ");
        }

    private function deleteCustomerOrderByIdDB($customerOrderID){

            if (!is_numeric($customerOrderID) || $customerOrderID <= 0){
                throw new Exception("Illegal value customerOrderID");             
            }

            $this->db->execute("DELETE FROM tbl_cust_transaction_order WHERE cto_id = $customerOrderID");
        }

    private function getCustTransactionsOrderCountForLastXDays_DB($cust_id,$days = 0){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");             
        }

        return $this->db->getVal("SELECT COUNT(*) 
                                    FROM tbl_cust_transaction_order
                                    WHERE cto_cust_id = $cust_id
                                    AND cto_order_tyme >= (NOW() - INTERVAL $days DAY)");
    }

    private function getCustTransactionsOrderCount_DB($cust_id){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");             
        }

        return $this->db->getVal("SELECT COUNT(*) 
                                    FROM tbl_cust_transaction_order
                                    WHERE cto_cust_id = $cust_id");
    }

    private function getCustomerOrdersDB(customerObject $customer,$filterType = '',$skip = 0,$take = 0){
        $filter = "";
        if($filterType != ""){
            $filter = " AND cto_order_type='$filterType' ";
        }

        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_status=ors_id
                AND cto_biz_id={$customer->cust_biz_id}
                AND cto_cust_id={$customer->cust_id}
                $filter
                ORDER BY cto_order_tyme DESC
                $limit";
        
        return $this->db->getTable($sql);
    }

    /************************************* */
    /*     HELPERS CUSTOMERORDER           */
    /************************************* */

    public static function getClaimedCouponsByItem($itemId){

        $instance = new self();

        return $instance->db->getVal("SELECT count(tri_id) 
                                        FROM tbl_cust_transaction_items 
                                        WHERE tri_item_type=1 
                                        AND tri_itm_row_id=$itemId");
    }

    public static function getOrderAndItemByCouponAndRedeemCode($custId,$bizId,$couponId,$redeemCode){

        $instance = new self();

        return $instance->db->getRow("SELECT * FROM tbl_cust_transaction_order,tbl_cust_transaction_items 
                                        WHERE tri_order_id=cto_id
                                        AND cto_cust_id=$custId 
                                        AND cto_biz_id=$bizId
                                        AND tri_itm_row_id = $couponId
                                        AND tri_redeem_code = '$redeemCode'");

    }
}
