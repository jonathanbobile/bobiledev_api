<?php

/**
 * Enum for email types
 *
 * @version 1.0
 * @author PapTap
 */

class enumBadges extends Enum {
    
    const appointments = "appointments"; 

    
    const benefits = "benefits";       

    
    const orders = "orders";      

    
    const notifications = "notifications"; 
    
    
    const requests = "requests";

   
    const documents = "documents";
    
}
?>