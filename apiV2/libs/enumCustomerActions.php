<?php

/**
 * Enum for customer actions
 *
 * @version 1.0
 * @author PapTap
 */

class enumCustomerActions extends Enum {
    const enteredApplication = 1;
    const completedRegistration = 2;
    const skippedRegistration = 3;
    const lateRegistration = 4;
    const welcomeBack = 5;
    const updateProfile = 6;
    const bookedAppointment = 7;
    const completedPurchase = 8;
    const punchedCard = 9;
    const claimedFreeCoupon = 10;
    const purchasedCoupon = 11;
    const completedPunchCard = 12;
    const sentFile = 13;
    const paidPayment = 14;
    const likeOnSocial = 15;
    const addedToFavorites = 16;
    const openedPush = 17;
    const upgradedMembership = 18;
    const filledForm = 19;
    const DayWithApp = 20;
    const monthWithApp = 21;
    const invitedAFriend = 22;
    const redeemedCoupon = 23;
    const usedPunchPass = 24;
    const usedSubscription = 25;
    const purchasedSubscription = 26;
    const purchasedPunchPass = 27;
    const installed = 28;
    const classsignup = 29;
    const sentChat = 30;
    const webMemberValidate = 31;
    const webMemberClaimPoints = 32;
    const completedOnboarding = 33;
}
?>