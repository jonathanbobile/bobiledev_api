<?php

/**
 * Enum for email types
 *
 * @version 1.0
 * @author PapTap
 */

class enumEmailType extends Enum {
    
    /**
     * support@bobile.com via SMTP
     */
    const SystemMailSMTP = 1;

    /**
     * support@paptap-apps.com via Amazon
     */
    const SystemMailAmazon = 2;     

    /**
     * support@bobile.support via SendGrid
     */
    const SystemMailSGCare = 3;     

    /**
     * noreply@paptapspecials.com via SendGrid
     */
    //const SystemMailSGSpecial = 4;     

    /**
     * noreply@paptapbilling.com via SendGrid
     */
    //const SystemMailSGBilling = 5; 
    
    /**
     * care@paptapsupport.com via SendGrid
     */
    //const SystemMailSgOld = 6;
    
    /**
     * support@bobile.com via SendGrid
     */
    //const BobileSystemMailSGCare = 7;

    /**
     * support@bobile.net via SendGrid
     */
    const BobileMailSGCampaign = 8;
    
    const SystemMailSGBeeZee = 9;
    
}

?>
