<?php

/**
 * enumOrderStatus - the order's statuses
 * 
 *
 * @version 1.0
 * @author JonathanM
 */
class enumOrderStatus extends Enum{

    const received = 1;
   
    const in_process = 2;

    const packaging = 3;

    const in_transit = 4;

    const delivered = 5;
    
}

    

