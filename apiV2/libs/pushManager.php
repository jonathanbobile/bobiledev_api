<?php
/**
 * Methods for Send Push Notifications
 *
 * @version 1.0
 * @author PapTap
 */

class pushManager {

    protected $db;
    private $googleApiKey = "AIzaSyDU2uHOD7DHPRAXcTWVGrf1nFjLk6ioYVU";
    private $googleUrl = 'https://android.googleapis.com/gcm/send';
    private $googleFireBaseUrl = 'https://fcm.googleapis.com/fcm/send';

    //IOS params
    private $http2_server_production = 'https://api.push.apple.com';
    private $http2_server_development = 'https://api.development.push.apple.com';

    public function __construct(){
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }
    }

    public function getSavedCampaigns(){
        $pushTable = $this->db->getTable("select * from tbl_push
                                            where pu_biz_id={$_SESSION["appData"]["biz_id"]}
                                            and pu_send_on is null
                                            and pu_sent is null
                                            and pu_based_location = 0
                                            and pu_type<>7 and pu_type <> 6
                                            and pu_async_sent <> 1
                                            order by pu_created desc");

        if(count($pushTable) > 0){

            $pushTable = $this->addGroupNames($pushTable);
        }

        return $pushTable;
    }

    public function getScheduledCampaigns(){
        $pushTable = $this->db->getTable("select * from tbl_push
                                            where pu_biz_id={$_SESSION["appData"]["biz_id"]}
                                            and pu_send_on is not null
                                            and pu_sent is null
                                            and pu_based_location = 0
                                            and pu_type <> 6
                                            order by pu_send_on");

        if(count($pushTable) > 0){

            $pushTable = $this->addGroupNames($pushTable);
        }

        return $pushTable;
    }

    public function getGreetingsCampaigns(){
        $pushTable = $this->db->getTable("select * from tbl_push LEFT JOIN tbl_push_events
                                            on pu_event_type = pue_id
                                            where pu_biz_id={$_SESSION["appData"]["biz_id"]}
                                            and (pu_type=7 or pu_type = 6)
                                            and pu_based_location = 0
                                            order by pu_created desc");

        if(count($pushTable) > 0){

            $pushTable = $this->addGroupNames($pushTable);
        }

        return $pushTable;
    }

    public function getReminders(){
        $pushTable = $this->db->getTable("select * from tbl_reminders
                                            where rem_biz_id={$_SESSION["appData"]["biz_id"]}
                                            AND rem_entity_type = 'reminder'
                                            order by rem_created desc");

        if(count($pushTable) > 0){

            $pushTable = $this->addGroupNames($pushTable);
        }

        return $pushTable;
    }

    public function getHistory($filters = array(),$skip = 0, $take = 25){
        $limit = 'Limit '. $skip .','. $take;

        $orderBy = 'puh_sent';
        $sortBy = isset($filters['sort']) ? $filters['sort'] : "";

        if($sortBy == 'created'){
            $orderBy = 'puh_created';
        }

        $format = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? 'm/d/Y' : 'd/m/Y';
        $filter = '';
        if(isset($filters['start']) && $filters['start'] != ''){
            $filter .= " AND $orderBy >= STR_TO_DATE('" . date_format(DateTime::createFromFormat($format,$filters['start']),'Y-m-d') . " 00:00:00','%Y-%m-%d %H:%i:%s')";
        }
        if(isset($filters['end']) && $filters['end'] != ''){
            $filter .= " AND $orderBy <= STR_TO_DATE('" . date_format(DateTime::createFromFormat($format,$filters['end']),'Y-m-d') . " 23:59:00','%Y-%m-%d %H:%i:%s')";
        }
        if(isset($filters['text']) && $filters['text'] != ''){
            $filter .= " AND (LOWER(puh_short_mess) LIKE '%" . strtolower(addslashes($filters['text'])) . "%' or LOWER(puh_type) LIKE '%" . strtolower(addslashes($filters['text'])) . "%' or LOWER(cust_first_name) LIKE '%" . strtolower(addslashes($filters['text'])) . "%')";
        }
        if($sortBy == 'type'){
            $orderBy = 'puh_type';
        }

        $sql = "select * from tbl_push_history
                            LEFT JOIN tbl_customers ON puh_cust_id = cust_id AND cust_biz_id = {$_SESSION["appData"]["biz_id"]}
                                            where puh_biz_id={$_SESSION["appData"]["biz_id"]}
                                            $filter
                                            order by $orderBy desc $limit";

       
        $historyTable = $this->db->getTable($sql);

        if(count($historyTable) > 0){

            $historyTable = $this->addGroupNames($historyTable);
        }

        return $historyTable;
    }

    public function createGeoPush(){
        $pid = $this->db->getVal("select pu_id from tbl_push where pu_biz_id={$_SESSION["appData"]["biz_id"]} and pu_based_location=1");

        if($pid == ""){
            $sql = "insert into tbl_push set
                pu_biz_id={$_SESSION["appData"]["biz_id"]},
                pu_based_location=1";

            $pid = $this->db->execute($sql);
        }
        return $pid;
    }

    private function addGroupNames($pushTable){
        $i=0;
        foreach ($pushTable as $pushRow)
        {
            if(isset($pushRow['puh_cust_id']) && $pushRow['puh_cust_id'] != 0 && isset($pushRow['cust_first_name']) && $pushRow['cust_first_name'] != ''){
                $pushTable[$i]["groupsText"] = $pushRow['cust_first_name'];
            }
            else if($pushRow["pu_groups"] != ""){
                $namesArray = array();
                $gropsTable = $this->db->getTable("select cg_name from tbl_biz_groups where cg_id in ({$pushRow["pu_groups"]})");
                if(count($gropsTable)>0){
                    foreach ($gropsTable as $oneName)
                    {
                    	array_push($namesArray,$oneName["cg_name"]);
                    }

                }
                $groupNames = implode(", ",$namesArray);
                $pushTable[$i]["groupsText"] = $groupNames;
            }else{
                
                $pushTable[$i]["groupsText"] = "All";
               
            }
            $i++;
        }
        return $pushTable;
    }

    public function deleteCampaign($campaign_id){
        $push = $this->db->getRow("SELECT * FROM tbl_push where pu_id = $campaign_id and pu_biz_id = {$_SESSION["appData"]["biz_id"]}");
        $sql = "delete from tbl_mod_data29 where md_parent = $campaign_id and md_biz_id = {$_SESSION["appData"]["biz_id"]}";
        $this->db->execute($sql);
        $sql = "delete from tbl_push where pu_id = $campaign_id and pu_biz_id = {$_SESSION["appData"]["biz_id"]}";
        $this->db->execute($sql);
        $result = array();
        if(isset($push['pu_send_on']) && !isset($push['pu_sent'])){
            $this->db->execute("update tbl_biz set biz_push_left = biz_push_left + 1 WHERE biz_id = {$_SESSION["appData"]["biz_id"]}");
            $_SESSION['appData']['biz_push_left'] = $_SESSION['appData']['biz_push_left'] + 1;
           
        }
        $result['credit'] = $_SESSION['appData']['biz_push_left'];
        $result['status'] = 1;
        return $result;
    }

    public function toggleReminderStatus($reminderid,$tostate){
        $sql = "UPDATE tbl_reminders SET rem_active = $tostate WHERE rem_id = $reminderid";
        $this->db->execute($sql);
        $response = array(
            'code' => 1,
            'message' => ''
            );

        return $response;
    }

    public function deleteReminder($rem_id){
        $sql = "delete from tbl_reminders where rem_id = $rem_id";
        $this->db->execute($sql);
        return 1;
    }

    public function getPushRow($push_id){
        return $this->db->getRow("SELECT * FROM tbl_push WHERE pu_id = $push_id");
    }

    public function getPushAnalyticsRow($history_id){
        $analiticsRow = $this->db->getRow("SELECT * FROM tbl_push_history,tbl_push WHERE pu_id=puh_push_id and puh_id = $history_id");
        if($analiticsRow["pu_groups"] != ""){
            $namesArray = array();
            $gropsTable = $this->db->getTable("select cg_name from tbl_biz_groups where cg_id in ({$analiticsRow["pu_groups"]})");
            if(count($gropsTable)>0){
                foreach ($gropsTable as $oneName)
                {
                    array_push($namesArray,$oneName["cg_name"]);
                }
            }
            $groupNames = implode(", ",$namesArray);
            $analiticsRow["groupsText"] = $groupNames;
        }else{
            $analiticsRow["groupsText"] = "All";
        }

        return $analiticsRow;
    }

    public function getAnalyticsDelivery($pushID){

        $valAndroid = $this->db->getVal("select count(cup_id) from tbl_cust_push where cup_push_id=$pushID and cup_delivery_time is not null and cup_device_type='Android'");
        $valIOS = $this->db->getVal("select count(cup_id) from tbl_cust_push where cup_push_id=$pushID and cup_delivery_time is not null and cup_device_type='IOS'");

        $data["Android"] = $valAndroid;
        $data["IOS"] = $valIOS;
        return $data;
    }

    public function getAnalyticsOpened($pushID,$deliveredAndroid,$deliveredIOS){

        $valAndroid = $this->db->getVal("select count(cup_id) from tbl_cust_push where cup_push_id=$pushID and cup_open_time is not null and cup_device_type='Android'");
        $valIOS = $this->db->getVal("select count(cup_id) from tbl_cust_push where cup_push_id=$pushID and cup_open_time is not null and cup_device_type='IOS'");

        $data["Android_Opened"] = $valAndroid;
        $data["IOS_Opened"] = $valIOS;
        $data["Android_Percent"] = $deliveredAndroid != 0 ? number_format($valAndroid / ($deliveredAndroid / 100),2) : 0;
        $data["IOS_Percent"] = $deliveredIOS != 0 ? number_format($valIOS / ($deliveredIOS / 100),2) : 0;
        return $data;
    }

    public function getAnalytics24Open($pushID,$pushSentDate){

        $data = array();
        $lasHour = 0;
        for($i=3; $i<=24; $i = $i+3){
            $valAndroid = $this->db->getVal("select count(cup_id) from tbl_cust_push
                                                where cup_push_id=$pushID
                                                and cup_open_time is not null
                                                and cup_open_time > DATE_ADD('$pushSentDate',INTERVAL $lasHour HOUR)
                                                and cup_open_time <= DATE_ADD('$pushSentDate',INTERVAL $i HOUR)
                                                and cup_device_type='Android'");

            $valIOS = $this->db->getVal("select count(cup_id) from tbl_cust_push
                                                where cup_push_id=$pushID
                                                and cup_open_time is not null
                                                and cup_open_time > DATE_ADD('$pushSentDate',INTERVAL $lasHour HOUR)
                                                and cup_open_time <= DATE_ADD('$pushSentDate',INTERVAL $i HOUR)
                                                and cup_device_type='IOS'");

            $stats["hr"] = $i;
            $stats["Android"] = $valAndroid;
            $stats["IOS"] = $valIOS;
            array_push($data,$stats);

            $lasHour = $lasHour + $i;
        }

        return $data;
    }

    public function getAnalyticsGeo($pushID){

        return $this->db->getTable("SELECT cust_city, IFNULL(android, 0) android,IFNULL(ios, 0) ios FROM (select cust_city from tbl_cust_push,tbl_customers
                                    where cust_id = cup_customer_id
                                    and cup_push_id=$pushID
                                    and cup_delivery_time is not null
                                    and cust_city <> ''
                                    and cust_city is not null
                                    group by cust_city)cty
                                    LEFT JOIN
                                    (SELECT COUNT(cup_id) android, cust_city andr_city from tbl_cust_push,tbl_customers
                                    where cust_id = cup_customer_id
                                    and cup_push_id=$pushID
                                    and cup_delivery_time is not null
                                    and cust_city <> ''
                                    and cust_city is not null
                                    and cup_device_type = 'Android'
                                    group by andr_city)andr
                                    ON cust_city = andr_city
                                    LEFT JOIN
                                    (SELECT COUNT(cup_id) ios, cust_city ios_city from tbl_cust_push,tbl_customers
                                    where cust_id = cup_customer_id
                                    and cup_push_id=$pushID
                                    and cup_delivery_time is not null
                                    and cust_city <> ''
                                    and cust_city is not null
                                    and cup_device_type = 'IOS'
                                    group by ios_city)ios
                                    ON cust_city = ios_city");
    }

    public function getReminderhRow($rem_id){
        return $this->db->getRow("SELECT * FROM tbl_reminders WHERE rem_id = $rem_id");
    }

    public function getSentCampaigns($filters,$sortBy,$take,$skip){

        $limit = 'Limit '. $skip .','. $take;

        $orderBy = 'pu_created';
        if($sortBy == 'scheduled'){
            $orderBy = 'pu_send_on';
        }
        if($sortBy == 'sent'){
            $orderBy = 'pu_sent';
        }
        $format = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? 'm/d/Y' : 'd/m/Y';
        $filter = '';
        if(isset($filters['start']) && $filters['start'] != ''){
            $filter .= " AND $orderBy >= STR_TO_DATE('" . date_format(DateTime::createFromFormat($format,$filters['start']),'Y-m-d H:i:s') . "','%Y-%m-%d %H:%i:%s')";
        }
        if(isset($filters['end']) && $filters['end'] != ''){
            $filter .= " AND $orderBy <= STR_TO_DATE('" . date_format(DateTime::createFromFormat($format,$filters['end']),'Y-m-d H:i:s') . "','%Y-%m-%d %H:%i:%s')";
        }
        if(isset($filters['text']) && $filters['text'] != ''){
            $filter .= " AND LOWER(pu_short_mess) LIKE '%" . strtolower(addslashes($filters['text'])) . "%'";
        }
        $sql = "select * from tbl_push
                                        where pu_biz_id={$_SESSION["appData"]["biz_id"]} and pu_sent is not null
                                        $filter
                                        order by $orderBy desc
                                        $limit";

        return $this->db->getTable($sql);
    }

    public function getCampaignModules($campaign_id, $is_copy = false){
        $biz_id = $campaign_id != 0 ? "and md_biz_id={$_SESSION["appData"]["biz_id"]}" : "and md_element <> 9";
        $result = $this->db->getTable("SELECT * from tbl_mod_data29,tbl_mod_elements
                                    where me_id=md_element
                                    $biz_id
                                    and md_parent = $campaign_id
                                    and md_editble=1
                                    order by md_index");
        if($is_copy){
            for ($i = 0; $i < count($result); $i++)
            {

                $result[$i]['md_row_id'] = 0;
            }
        }

        return $result;
    }

    public function getGroupsForPushes(){
        return $this->db->getTable("SELECT *,count(gc_group_id) as cust_count FROM tbl_biz_groups tbg
                                        inner JOIN tbl_biz_mod tbm ON (tbm.biz_mod_biz_id = tbg.cg_biz_id AND tbm.biz_mod_mod_id = 30)
                                        left join tbl_group_cust ON gc_group_id=cg_id
                                        WHERE tbg.cg_biz_id = {$_SESSION["appData"]["biz_id"]}
										group by cg_id
                                        ORDER BY tbg.cg_name");
    }

    public function getPushTypes($selectedID=0,$filter = "'Push'"){
        $typeTable = $this->db->getTable("SELECT tbl_push_type.*,perm.*,area.pa_system_name as area_name FROM tbl_push_type
                                            left join tbl_permission_tree as perm ON pa_mod_id = put_req_mod
                                            left join tbl_permission_tree as area on area.pa_id = perm.pa_parent_id 
                                            WHERE put_type in ($filter) and put_active=1 ORDER BY put_index ASC");

        if($selectedID != 0){
            $i=0;
            foreach ($typeTable as $type)
            {
            	$selected = 0;
                if($type["put_id"] == $selectedID){
                    $selected = 1;
                }
                $typeTable[$i]["selected"] = $selected;
                $i++;
            }

        }

        return $typeTable;
    }

    public function getEventsTypes($selectedID=0,$filter = ""){
        $typeTable = $this->db->getTable("SELECT * from tbl_push_events $filter");

        if($selectedID != 0){
            $i=0;
            foreach ($typeTable as $type)
            {
            	$selected = 0;
                if($type["pue_id"] == $selectedID){
                    $selected = 1;
                }
                $typeTable[$i]["selected"] = $selected;
                $i++;
            }

        }

        return $typeTable;
    }

    public function getEventsStructList($eventId){
        $returnHTML = "";
        $structTable = $this->getEventsStruct(0,$eventId);
        if(count($structTable)>0){
            foreach ($structTable as $struct)
            {
            	//generate UI
                //$returnHTML .= "";
            }
        }
        return $returnHTML;
    }

    public function getEventsStruct($selectedID=0,$eventId){
        $typeTable = $this->db->getTable("SELECT * from tbl_push_event_struct where pbg_event_id = $eventId");

        $i = 0;
        foreach ($typeTable as $type)
        {
            $selected = 0;
            if($type["pbg_id"] == $selectedID || ($selectedID == 0 && $i == 0)){
                $selected = 1;
            }
            $typeTable[$i]["selected"] = $selected;
            $i++;
        }


        return $typeTable;
    }

    public function getStruct($structid){
        if(!isset($structid) || $structid == 'undefined'){
            $structid = 1;
        }
        $result = $this->db->getRow("SELECT * FROM tbl_push_event_struct where pbg_id = $structid");
        return $result;
    }

    public function getEventsTextList($eventId){
        $returnHTML = "";
        $textTable = $this->getEventsText($eventId);
        if(count($textTable)>0){
            foreach ($textTable as $txt)
            {
            	//generate UI
                //$returnHTML .= "";
            }
        }
        return $returnHTML;
    }

    public function getEventsText($eventId){
        $typeTable = $this->db->getRow("SELECT * from tbl_push_text where ptx_event_id = $eventId");

        return $typeTable;
    }

    public function checkPushesLeft($bizID=0){
        
        $bizID = ($bizID == 0) ? $_SESSION["appData"]["biz_id"] : $bizID;
        return $this->db->getVal("select biz_push_left from tbl_biz where biz_id=$bizID");
    }

    public function editCampaign($push,$modules){
        $sql = '';
        $mod_ids = array();
        $result = array();
        $dFormat = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? '%m/%d/%Y %h:%i%p' : '%d/%m/%Y %H:%i';
        $tFormat = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? '%h:%i%p' : '%H:%i';
        $addToTime =  $_SESSION["appData"]["biz_time_zone"] + date("I");
        $send_to_date = $push['send_to_date'];
        if($send_to_date != '')
        {
            $sql_send_to_date = "DATE_ADD(STR_TO_DATE('$send_to_date', '$dFormat'), INTERVAL -($addToTime) HOUR)";
        }
        else
        {
            $sql_send_to_date = "null";
        }

        $pu_coupon_id = isset($push['pu_coupon_id']) ? $push['pu_coupon_id'] : "0";
        $pu_product_id = isset($push['pu_product_id']) ? $push['pu_product_id'] : "0";
        $pu_loyalty_id = isset($push['pu_loyalty_id']) ? $push['pu_loyalty_id'] : "0";
        $pu_scard_id = isset($push['pu_scard_id']) ? $push['pu_scard_id'] : "0";
        $pu_event_type = isset($push['pu_event_type']) ? $push['pu_event_type'] : "0";
        $pu_event_struct = isset($push['pu_event_struct']) ? $push['pu_event_struct'] : "0";
        $pu_event_greeting = isset($push['pu_event_greeting']) ? addslashes($push['pu_event_greeting']) : "";
        $pu_event_main_text = isset($push['pu_event_main_text']) ? addslashes($push['pu_event_main_text']) : "";
        $pu_event_signature = isset($push['pu_event_signature']) ? addslashes($push['pu_event_signature']) : "";
        $pu_event_send_time = isset($push['pu_event_send_time']) ? addslashes($push['pu_event_send_time']) : "";

        if($pu_event_send_time != '')
        {
            $pu_event_send_time = "DATE_ADD(STR_TO_DATE('$pu_event_send_time', '$tFormat'), INTERVAL -($addToTime) HOUR)";
        }
        else
        {
            $pu_event_send_time = "'00:00:00'";
        }
         
        
        
        if($push['id'] == 0){ // add new push
            $sql = "insert into tbl_push set
                pu_send_on = $sql_send_to_date,
                pu_groups='{$push['groups']}',
                pu_target='{$push['target']}',
                pu_biz_id={$_SESSION["appData"]["biz_id"]},
                pu_status='Draft',
                pu_short_mess='".addslashes($push['push_mess'])."',
                pu_type = {$push['campaign_type']},
                pu_coupon_id=$pu_coupon_id,
                pu_product_id=$pu_product_id,
                pu_loyalty_id=$pu_loyalty_id,
                pu_scard_id=$pu_scard_id,
                pu_event_type=$pu_event_type,
                pu_event_struct=$pu_event_struct,
                pu_event_greeting='$pu_event_greeting',
                pu_event_main_text='$pu_event_main_text',
                pu_event_signature='$pu_event_signature',
                pu_event_send_time=$pu_event_send_time,
                pu_isnew = 1";

            $pid = $this->db->execute($sql);
        }
        else{ // edit existing push
            $sql = "update tbl_push set
                pu_send_on = $sql_send_to_date,
                pu_groups='{$push['groups']}',
                pu_target='{$push['target']}',
                pu_biz_id={$_SESSION["appData"]["biz_id"]},
                pu_status='Draft',
                pu_short_mess='".addslashes($push['push_mess'])."',
                pu_type = {$push['campaign_type']},
                pu_coupon_id=$pu_coupon_id,
                pu_product_id=$pu_product_id,
                pu_loyalty_id=$pu_loyalty_id,
                pu_scard_id=$pu_scard_id,
                pu_event_type=$pu_event_type,
                pu_event_struct=$pu_event_struct,
                pu_event_greeting='$pu_event_greeting',
                pu_event_main_text='$pu_event_main_text',
                pu_event_signature='$pu_event_signature',
                pu_event_send_time=$pu_event_send_time,
                pu_isnew = 1
                where pu_id = {$push['id']}";
            
            $pid = $push['id'];
            $this->db->execute($sql);
            
            if($push['campaign_type'] == 10){
                $sql = "DELETE FROM tbl_mod_data29 WHERE md_parent = $pid AND md_biz_id = {$_SESSION["appData"]["biz_id"]}";
                $this->db->execute($sql);
            }
            

           
        }
        
        if(count($modules)>0){
            $index = 0;
            $innerSql = '';
            foreach ($modules as $module)
            {
                $index++;
                $innerData = "";
                foreach ($module['data'] as $key => $value)
                {
                    $value = addslashes($value);
                    $innerData .= ", $key = '$value'";
                }

                if($module['id'] == 0){
                    $innerSql = "insert into tbl_mod_data29 set
                            md_required=0,
                            md_biz_id = {$_SESSION["appData"]["biz_id"]},
                            md_level_no = 1,
                            md_parent = $pid,
                            md_index = $index,
                            md_element = {$module['type']}
                            $innerData";

                    $mod_ids[$module['order']] = $this->db->execute($innerSql);

                }
                else{
                    
                    $innerSql = "update tbl_mod_data29 set
                        md_biz_id = {$_SESSION["appData"]["biz_id"]},
                        md_level_no = 1,
                        md_parent = $pid,
                        md_index = $index,
                        md_element = {$module['type']}
                        $innerData
                        where md_row_id = {$module['id']}";

                    $this->db->execute($innerSql);
                    $mod_ids[$module['order']] = $module['id'];
                    
                }

            }
        }

        if($push['push_type'] == 'sendNow' || ($push['send_to_date'] != '' && $push['campaign_type'] != 7)){
            $this->db->execute("update tbl_biz set biz_push_left = biz_push_left - 1 WHERE biz_id = {$_SESSION["appData"]["biz_id"]}");
            $_SESSION['appData']['biz_push_left'] = $_SESSION['appData']['biz_push_left'] - 1;
        }

        $this->updateModuleDataForPushCampaign($pid);

        $result['push_id'] = $pid;
        $result['mods'] = $mod_ids;
        return $result;
    }

    public function deleteOrphanElements($biz_id){
        $sql = "DELETE FROM tbl_mod_data29 WHERE md_parent = 0 AND md_biz_id = $biz_id";

        $this->db->execute($sql);

        return 1;
    }

    public function pushAsyncSent($push_id){
        $sql = "UPDATE tbl_push SET pu_async_sent = 1 WHERE pu_id = $push_id";

        $this->db->execute($sql);

        return;
    }

    public function saveGeoCampaign($push,$modules){
        $sql = '';
        $mod_ids = array();
        $result = array();

        $q = "UPDATE tbl_biz SET
                            biz_addr_country = '" . addslashes($push['biz_addr_country']) . "',
                            biz_addr_state = '" . addslashes($push['biz_addr_state']) . "',
                            biz_addr_town = '" . addslashes($push['biz_addr_town']) . "',
                            biz_addr_street = '" . addslashes($push['biz_addr_street']) . "',
                            biz_addr_no = '" . addslashes($push['biz_addr_no']) . "',
                            biz_addr_state_id = {$push['biz_addr_state_id']},
                            biz_addr_country_id = {$push['biz_addr_country_id']}
                   WHERE biz_id = {$_SESSION["appData"]["biz_id"]}";

        $this->db->execute($q);

        $_SESSION["appData"]["biz_addr_state_id"] = $push['biz_addr_state_id'];
        $_SESSION["appData"]["biz_addr_country_id"] = $push['biz_addr_country_id'];

        $sql = "update tbl_push set
                pu_short_mess='".addslashes($push['push_mess'])."',
                pu_active = {$push['pu_active']},
                pu_distance = {$push['pu_distance']},
                pu_longi = '{$push['pu_longi']}',
                pu_lati = '{$push['pu_lati']}'
                where pu_id = {$push['id']}";


        $pid = $push['id'];
        $this->db->execute($sql);
        $innerSql = '';

        if(count($modules)>0){
            foreach ($modules as $module)
            {
                $innerData = "";
                foreach ($module['data'] as $key => $value)
                {
                    $value = addslashes($value);
                    $innerData .= ", $key = '$value'";
                }

                if($module['id'] == 0){
                    $innerSql = "insert into tbl_mod_data29 set
                            md_required=0,
                            md_biz_id = {$_SESSION["appData"]["biz_id"]},
                            md_level_no = 1,
                            md_parent = {$push['id']},
                            md_element = {$module['type']}
                            $innerData";

                    $mod_ids[$module['order']] = $this->db->execute($innerSql);

                }
                else{
                    $innerSql = "update tbl_mod_data29 set
                            md_biz_id = {$_SESSION["appData"]["biz_id"]},
                            md_level_no = 1,
                            md_parent = {$push['id']},
                            md_element = {$module['type']}
                            $innerData
                            where md_row_id = {$module['id']}";

                    $this->db->execute($innerSql);
                    $mod_ids[$module['order']] = $module['id'];
                }

            }
        }

        $result['push_id'] = $pid;
        $result['mods'] = $mod_ids;
        return $result;
    }

    public function saveReminder($reminder){
        $sql = '';
        $result = array();

        $dFormat = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? '%h:%i%p' : '%H:%i';
        $addToTime =  $_SESSION["appData"]["biz_time_zone"] + date("I");
        $send_on_time = $reminder['rem_time'];
        $sql_send_on_time = "DATE_ADD(STR_TO_DATE('$send_on_time', '$dFormat'), INTERVAL -($addToTime) HOUR)";
        $sql_next_date = addslashes($reminder['rem_next_send']);
        $dFormat = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? '%m/%d/%Y %h:%i%p' : '%d/%m/%Y %H:%i';
        $sql_next = "DATE_ADD(STR_TO_DATE('$sql_next_date', '$dFormat'), INTERVAL -($addToTime) HOUR)";

        if($reminder['id'] == 0){ // add new reminder

            $sql = "insert into tbl_reminders set
                rem_biz_id = {$_SESSION["appData"]["biz_id"]},
                rem_text='".addslashes($reminder['rem_text'])."',
                rem_active = {$reminder['rem_active']},
                rem_repeat = '{$reminder['rem_repeat']}',
                pu_groups = '{$reminder['pu_groups']}',
                rem_time = $sql_send_on_time,
                pu_target='{$reminder['pu_target']}',
                rem_next_send = $sql_next";

            $reminder['id'] = $this->db->execute($sql);
        }else{

            $sql = "update tbl_reminders set
                rem_text='".addslashes($reminder['rem_text'])."',
                rem_active = {$reminder['rem_active']},
                rem_repeat = '{$reminder['rem_repeat']}',
                pu_groups = '{$reminder['pu_groups']}',
                rem_time = $sql_send_on_time,
                pu_target='{$reminder['pu_target']}',
                rem_next_send = $sql_next
                where rem_id = {$reminder['id']}";

            $this->db->execute($sql);
        }

        //$fh = fopen("debugreminder.txt", 'a');
        // fwrite($fh, $sql."\n");
        // fclose($fh);

        $result['rem_id'] = $reminder['id'];
        return $result;
    }
    
    public function getPushBalance($biz_id){
        
        $sql = "SELECT biz_push_left from tbl_biz WHERE biz_id = $biz_id";
        return $this->db->getVal($sql);
        
    }

    public function getDeviceIdOfCustomerByCustId($custId){
        return $this->db->getVal("select cust_mobile_serial from tbl_customers where cust_id = $custId");
    }

    public function updateModuleDataForPushCampaign($campaignID){

        $moduleData = $this->getModuleDataForCampaign($campaignID);
        
        $this->db->execute("UPDATE tbl_push SET
                                pu_external_id={$moduleData["externalId"]},
                                pu_level={$moduleData["level"]},
                                pu_mod_id={$moduleData["modId"]},
                                pu_view='{$moduleData["iosView"]}',
                                pu_view_android='{$moduleData["androidView"]}'
                                WHERE pu_id=$campaignID");
    }

    public function getModuleDataForCampaign($campaignID){

        $campaignRow = $this->getPushRow($campaignID);
        $bizId = $campaignRow["pu_biz_id"];
        $pushType = $this->getPushTypeForCampaign($campaignRow["pu_type"]);
        $externalId = $this->getExternalIdForCampaign($campaignID);

        $response = array();
        $response["externalId"] = $externalId;
        

        switch($pushType){
            case enumPushType::biz_landingPage:                
                $response["modId"] = 29;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_promoteCoupon:                
                $response["modId"] = 26;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_promoteProduct:                
                $response["modId"] = 9;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $parentLevel = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                $response["level"] = $parentLevel + 1;
                break;
            case enumPushType::biz_promotePunchCard:                
                $response["modId"] = 6;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_greetingsCard:                
                $response["modId"] = 29;  
                $response["iosView"] = "Greeting";
                $response["androidView"] = "Greeting";
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            default: //fall back to main screen of the app
                $mainScreenData = $this->getModuleLastStructureByBizModId($bizId,0);
                $response["modId"] = 0;
                $response["level"] = 1;
                $response["iosView"] = $mainScreenData["ms_view_end"];
                $response["androidView"] = $mainScreenData["ms_view_end_android"];
                break;
        }

        return $response;
    }


    //********************************* refactor ***********************//

    /* ************************************** */
    /*   Cron Triggered Runners               */
    /* ************************************** */

    public static function runLocationPush(){

        $instance = new self();

        $pushList = $instance->getLocationBasedCampaignsToSend();

        if(count($pushList)>0){

            //protect multi sending of a push
            foreach ($pushList as $push){
                $instance->markGeoPushCampaignAsSent($push["pu_id"]);
            }
            
            foreach ($pushList as $pushData){

                $customers = $instance->getCustomersInRangeForLocationPushCampaign($pushData);

                if(count($customers)>0){
                    foreach ($customers as $customer){
                        $extraParams["push_id"] = $pushData["pu_id"];
                        $countPushesSent = $instance->sendPushBiz($customer["cust_biz_id"],$customer["cust_id"],enumPushType::biz_geoLocation,$pushData['pu_short_mess'],$extraParams);
                        if($countPushesSent > 0){
                            $instance->addPushHistory($pushData["pu_id"],enumPushType::biz_geoLocation,$customer["cust_mobile_serial"],$customer["cust_id"]);
                        }
                    }
                }
            }
        }else{
            //if all campaigns are sent, mark all campains to run again
            $instance->markGeoPushCampaignsAsNotSent();
        }
    }

    public static function runSubscriptionPush(){
        $instance = new self();

        $subM = new subscriptionsManager();

        $subs = $subM->getLocationActiveSubscriptions();

        foreach ($subs as $locationsub)
        {
        	$sub_customers = $instance->getActiveCustomersForSubscriptionInRange($locationsub);
            
            if(count($sub_customers)>0){
                foreach ($sub_customers as $customer){
                    $extraParams['csub_id'] = $customer['csu_id'];
                    $extraParams['sub_name'] = $locationsub['md_head'];
                    $message = pushManager::getSystemAlertPushMessageText(33,$customer["cust_biz_id"]);
                    $countPushesSent = $instance->sendPushBiz($customer["cust_biz_id"],$customer["cust_id"],enumPushType::geo_subscription_trigger,$message,$extraParams);
                    if($countPushesSent > 0){
                        $instance->addPushHistory(0,enumPushType::geo_subscription_trigger,$customer["cust_mobile_serial"],$customer["cust_id"],$locationsub['md_row_id']);
                    }
                }
            }
        }
        
    }

    public static function runPunchPassPush(){
        $instance = new self();

        $subM = new subscriptionsManager();

        $subs = $subM->getLocationActivePunchPasses();

        foreach ($subs as $locationppass)
        {
        	$sub_customers = $instance->getActiveCustomersForPunchPassInRange($locationppass);
            
            if(count($sub_customers)>0){
                foreach ($sub_customers as $customer){
                    $extraParams['cpass_id'] = $customer['cpp_id'];
                    $message = pushManager::getSystemAlertPushMessageText(33,$customer["cust_biz_id"]);
                    $countPushesSent = $instance->sendPushBiz($customer["cust_biz_id"],$customer["cust_id"],enumPushType::geo_punchpass_trigger,$message,$extraParams);
                    if($countPushesSent > 0){
                        $instance->addPushHistory(0,enumPushType::geo_punchpass_trigger,$customer["cust_mobile_serial"],$customer["cust_id"],$locationppass['md_row_id']);
                    }
                }
            }
        }
        
    }

    public static function runReminders(){

        $instance = new self();

        $pushList = $instance->getReminderCampaignsToSend();

        if(count($pushList)>0){

            //first update the next date / not active to prevent duplicate run
            foreach ($pushList as $push){
                if($push["rem_repeat"] != 'Once'){
                    $instance->updateReminderToNextDate($push["rem_repeat"], $push["rem_id"]);
                }else{
                    $instance->markReminderAsNotActive($push["rem_id"]);
                }
            }

            foreach ($pushList as $push){

                //Personal Reminder
                if($push["pu_cust_id"] > 0){
                    $extraParams["push_id"] = $push["rem_id"];
                    $extraParams["reminder"] = "1";
                    $countPushesSent = $instance->sendPushBiz($push["pu_biz_id"],$push["pu_cust_id"],enumPushType::biz_messageOnly,$push["pu_short_mess"],$extraParams);
                    if($countPushesSent > 0){
                        $instance->addReminderHistory($push["rem_id"],enumPushType::biz_messageOnly,"",$push["pu_cust_id"]);
                    }
                    return;
                }

                //Reminder - by target. We don't have campaignID so we run over the list directly and not call the campaign function
                $customersList = array();
                switch($push["pu_target"]){
                    case "all":
                        $customersList = $instance->getAllCustomersForBiz($push["pu_biz_id"]);
                        break;
                    case "guests":
                        $customersList = $instance->getGuestsForBiz($push["pu_biz_id"]);
                        break;
                    case "members":
                        $customersList = $instance->getClubMembersForBiz($push["pu_biz_id"]);
                        break;
                    case "groups":
                        $groupsArray = explode(",",$push["pu_groups"]);
                        $customersList = $instance->getDistinctCustomersListForGroupsList($groupsArray,$push["pu_biz_id"]);
                        break;
                }

                if(count($customersList) > 0){            
                    foreach ($customersList as $customer){
                        $extraParams["push_id"] = $push["rem_id"];
                        $extraParams["reminder"] = "1";
                        $countPushesSent = $instance->sendPushBiz($push["pu_biz_id"],$customer["cust_id"],enumPushType::biz_messageOnly,$push["pu_short_mess"],$extraParams);
                        if($countPushesSent > 0){
                            $instance->addReminderHistory($push["rem_id"],enumPushType::biz_messageOnly,$customer["cust_mobile_serial"],$customer["cust_id"]);
                        }
                    }
                }

            }
        }
    }

    public static function runPaymentRequestReminders(){

        $instance = new self();

        $pushList = $instance->getPaymentRequestRemindersList();

        if(count($pushList)>0){
            foreach ($pushList as $push)
            {                
                $instance->updatePaymentRequestReminderToNextDate($push["cto_reminder_type"], $push["cto_id"]);

                $message =$instance->getSystemAlertPushMessageText(29,$push["cto_biz_id"]);
                $extraParams["push_id"] = $push["cto_id"];
                $extraParams["reminder"] = "1";
                $instance->sendPushBiz($push["cto_biz_id"],$push["cto_cust_id"],enumPushType::biz_personalPRequest,$message,$extraParams);    
                $instance->addPRequestReminderHistory($push["cto_biz_id"],enumPushType::biz_personalPRequest,$push["cto_cust_id"],$message);
            }
        }
    }

    public static function runPreScheduledPush(){

        $instance = new self();

        $campaigns = $instance->getPreScheduledCampaignsToSend();

        if(count($campaigns) > 0){
            foreach ($campaigns as $oneCampaign)
            {
                switch($oneCampaign["pu_target"]){
                    case "all":
                        $instance->sendPushCampaignToAllCustomers($oneCampaign["pu_id"]);
                        break;
                    case "guests":
                        $instance->sendPushCampaignToAllGuests($oneCampaign["pu_id"]);
                        break;
                    case "members":
                        $instance->sendPushCampaignToAllMembers($oneCampaign["pu_id"]);
                            break;
                    case "groups":
                        $instance->sendPushCampaignToGroups($oneCampaign["pu_id"]);
                            break;
                }
            }
        }
    }

    public static function runGreetingsPush(){

        $instance = new self();

        $campaigns = $instance->getGreetingsCampaignsToSend();

        if(count($campaigns) > 0){

            foreach ($campaigns as $oneCampaign){

                $customersList = array ();
                switch ($oneCampaign["pu_target"]){

                    case "members":
                        if ($oneCampaign["pu_event_type"] == "3"){
                            $customersList = $instance->getCampaignMatchingCustomersByBirthDate($oneCampaign["pu_id"]);
                            $instance->sendPushCampaignToCustomizedListOfCustomers($oneCampaign["pu_id"],$customersList);
                        }else if ($oneCampaign["pu_event_type"] == "4"){
                            $customersList = $instance->getCampaignMatchingCustomersByWeddingDate($oneCampaign["pu_id"]);
                            $instance->sendPushCampaignToCustomizedListOfCustomers($oneCampaign["pu_id"],$customersList);
                        }
                    break;
                    case "groups":
                        $groupsList = explode(",",$oneCampaign["pu_groups"]);
                        if ($oneCampaign["pu_event_type"] == "3"){
                            $customersList = $instance->getCampaignMatchingCustomersByBirthDateInGroups($groupsList,$oneCampaign["pu_id"]);
                            $instance->sendPushCampaignToCustomizedListOfCustomers($oneCampaign["pu_id"],$customersList);
                        }else if ($oneCampaign["pu_event_type"] == "4"){
                            $customersList = $instance-> getCampaignMatchingCustomersByWeddingDateInGroups($groupsList,$oneCampaign["pu_id"]);
                            $instance->sendPushCampaignToCustomizedListOfCustomers($oneCampaign["pu_id"],$customersList);
                        }
                    break;
                }
            }
        }
    }

    public static function runHolidaysPush(){

        $instance = new self();

        $campaigns = $instance->getHolidaysCampaignsToSend();

        if(count($campaigns) > 0){

            //first update the next date to prevent duplicate run
            foreach ($campaigns as $oneCampaign){
                $instance->updateHolidayNextSendDate($oneCampaign["pu_id"]);
            }

            foreach ($campaigns as $oneCampaign)
            {
                switch($oneCampaign["pu_target"]){
                    case "all":
                        $instance->sendPushCampaignToAllCustomers($oneCampaign["pu_id"]);
                        break;
                    case "guests":
                        $instance->sendPushCampaignToAllGuests($oneCampaign["pu_id"]);
                        break;
                    case "members":
                        $instance->sendPushCampaignToAllMembers($oneCampaign["pu_id"]);
                        break;
                    case "groups":
                        $instance->sendPushCampaignToGroups($oneCampaign["pu_id"]);
                        break;
                }                
            }
        }
    }

    
    /* **************************** */
    /*   Campaign Functions         */
    /* **************************** */
    
    public static function sendPushCampaignToAllCustomers($campaignID){
        $instance = new self();

        $pushData = $instance->getPushRow($campaignID);
        $customers = $instance->getAllCustomersForBiz($pushData["pu_biz_id"]);
        $pushType = $instance->getPushTypeForCampaign($pushData["pu_type"]);
        $externalId = $instance->getExternalIdForCampaign($campaignID);
        $sentPushes = 0;

        $instance->addPushHistory($campaignID,$pushType);
        $instance->updateCampaignSent($campaignID,$sentPushes,$pushType);

        if (count($customers) > 0) {
            foreach ($customers as $customer) {
                $extraParams["push_id"] = $campaignID;
                $extraParams["external_id"] = $externalId;
                $instance->sendPushBiz($pushData["pu_biz_id"],$customer["cust_id"],$pushType,$pushData['pu_short_mess'],$extraParams);
                $sentPushes++;
            }
        }        
    }

    public static function sendPushCampaignToAllMembers($campaignID){
        $instance = new self();

        $pushData = $instance->getPushRow($campaignID);
        $members = $instance->getClubMembersForBiz($pushData["pu_biz_id"]);
        $pushType = $instance->getPushTypeForCampaign($pushData["pu_type"]);
        $externalId = $instance->getExternalIdForCampaign($campaignID);
        $sentPushes = 0;

        $instance->addPushHistory($campaignID,$pushType);
        $instance->updateCampaignSent($campaignID,$sentPushes,$pushType);

        if (count($members) > 0) {
            foreach ($members as $customer) {
                $extraParams["push_id"] = $campaignID;
                $extraParams["external_id"] = $externalId;
                $instance->sendPushBiz($pushData["pu_biz_id"],$customer["cust_id"],$pushType,$pushData['pu_short_mess'],$extraParams);
                $sentPushes++;
            }
        }        
    }

    public static function sendPushCampaignToAllGuests($campaignID){

        $instance = new self();

        $pushData = $instance->getPushRow($campaignID);
        $guests = $instance->getGuestsForBiz($pushData["pu_biz_id"]);
        $pushType = $instance->getPushTypeForCampaign($pushData["pu_type"]);
        $externalId = $instance->getExternalIdForCampaign($campaignID);
        $sentPushes = 0;

        $instance->addPushHistory($campaignID,$pushType);
        $instance->updateCampaignSent($campaignID,$sentPushes,$pushType);

        if (count($guests) > 0) {
            foreach ($guests as $customer) {
                $extraParams["push_id"] = $campaignID;
                $extraParams["external_id"] = $externalId;
                $instance->sendPushBiz($pushData["pu_biz_id"],$customer["cust_id"],$pushType,$pushData['pu_short_mess'],$extraParams);
                $sentPushes++;
            }
        }        
    }

    public static function sendPushCampaignToGroups($campaignID){
        $instance = new self();

        $pushData = $instance->getPushRow($campaignID);
        $groupsList = explode(",",$pushData["pu_groups"]);

        //No groups - no push.
        if (count($groupsList) == 0){
            return;
        }

        $customersList = $instance->getDistinctCustomersListForGroupsList($groupsList,$pushData["pu_biz_id"]);
        $pushType = $instance->getPushTypeForCampaign($pushData["pu_type"]);
        $externalId = $instance->getExternalIdForCampaign($campaignID);
        $sentPushes = 0;

        $instance->addPushHistory($campaignID,$pushType);
        $instance->updateCampaignSent($campaignID,$sentPushes,$pushType);

        if(count($customersList) > 0){
            foreach ($customersList as $customer){
                $extraParams["push_id"] = $campaignID;
                $extraParams["external_id"] = $externalId;
                $instance->sendPushBiz($pushData["pu_biz_id"],$customer["cust_id"],$pushType,$pushData['pu_short_mess'],$extraParams); 
                $sentPushes++;
            }
        }         
    }

    public static function sendPushCampaignToCustomizedListOfCustomers($campaignID,$customersList){
        $instance = new self();

        $pushData = $instance->getPushRow($campaignID);

        //No list - no push.
        if (count($customersList) == 0){
            return;
        }
        
        $pushType = $instance->getPushTypeForCampaign($pushData["pu_type"]);
        $externalId = $instance->getExternalIdForCampaign($campaignID);
        $sentPushes = 0;

        $instance->updateCampaignSent($campaignID,$sentPushes,$pushType);

        if(count($customersList) > 0){
            foreach ($customersList as $customer){
                $extraParams["push_id"] = $campaignID;
                $extraParams["external_id"] = $externalId;
                $instance->sendPushBiz($pushData["pu_biz_id"],$customer["cust_id"],$pushType,$pushData['pu_short_mess'],$extraParams);
                $instance->addPushHistory($campaignID,$pushType,"",$customer["cust_id"]);
                $sentPushes++;
            }
        }                
    }

    
    /* **************************** */
    /*   Enqueue Functions          */
    /* **************************** */

    /**
     * Summary of sendPushBiz
     * 
     * For extraParams use this default keys if needed:
     *      push_id
     *      external_id
     *      chat_id
     *      received_points
     *      
     * For keys of type String: surround with  addslashes()
     * 
     * @param mixed $bizId 
     * @param mixed $custID 
     * @param mixed $pushType 
     * @param mixed $messageText 
     * @param mixed $campaignID 
     * @param mixed $externalID 
     * @param array $extraParams
     * @return integer
     */
    public static function sendPushBiz($bizId,$custID,$pushType,$messageText,$extraParams = array()){

        $instance = new self();

        $bizRow = $instance->getBizData($bizId);
        $bizMenuTypeRow = $instance->getBizMainMenuViewType($bizId);
        $devices = $instance->getDeviceRowsByCustId($custID);
        
        $messageText = addslashes(stripslashes($messageText));
        $campaignID = isset($extraParams["push_id"]) ? $extraParams["push_id"] : 0;
        $externalID = isset($extraParams["external_id"]) ? $extraParams["external_id"] : 0;
        $chatMessageId = isset($extraParams["chat_id"]) ? $extraParams["chat_id"] : 0;

        $moduleData = array();
        if($campaignID != 0 && !isset($extraParams["reminder"])){
            $moduleData = $instance->getStructureDataByCampaign($campaignID);
        }else{
            $moduleData = $instance->getStructureDataByBizAndPushType($bizId,$pushType,$externalID);
        }

        $extraParams["mod_id"] = $moduleData["modId"];
        $extraParams["level"] = $moduleData["level"];
        $extraParams["biz_id"] = $bizId;
       
        if(count($devices)>0){
            foreach ($devices as $device)
            {
                $bizName = addslashes(stripslashes($bizRow["biz_short_name"]));
                if($device["deviceType"] == "Android"){
                    $layout = $bizMenuTypeRow["ms_view_type_android"];
                    $iOSTeamId = "";
                    $extraParams["fragment_to_open"] = $moduleData["androidView"];
                }else{
                    $layout = $bizMenuTypeRow["ms_view_type"];
                    $iOSTeamId = $instance->getIOSTeamId($device["deviceRowId"]);
                    $extraParams["openView"] = $moduleData["iosView"];
                }
                
                $extraParamsJsonString = json_encode($extraParams);

            	$sql = "insert into tbl_queue_push set
                            qup_biz_id=$bizId,
                            qup_app_id={$device["appId"]},
                            qup_biz_name='$bizName',
                            qup_icon='{$bizRow["biz_icon"]}',
                            qup_modules_count=1,
                            qup_biz_firs_mod={$bizRow["biz_first_mode_id"]},
                            qup_entity_id=$campaignID,
                            qup_device_id='{$device["deviceId"]}',
                            qup_message_id=$chatMessageId,
                            qup_biz_layout='$layout',
                            qup_menu_id={$bizRow["biz_menu"]},
                            qup_token='{$device["token"]}',
                            qup_os='{$device["deviceType"]}',
                            qup_message='$messageText',
                            qup_apns_id = {$device["deviceRowId"]},
                            qup_type=$pushType,
                            qup_external_id=$externalID,
                            qup_mod_id={$moduleData["modId"]},
                            qup_level={$moduleData["level"]},
                            qup_view='{$moduleData["iosView"]}',
                            qup_view_android='{$moduleData["androidView"]}',
                            qup_ios_team_id = '$iOSTeamId',
                            qup_cust_id = $custID,
                            qup_extra_params = '$extraParamsJsonString'
                            ";

               
                $instance->db->execute($sql);

                if($campaignID != 0){
                    $instance->addCustPushMessages($campaignID,$custID,$device["deviceId"],$device["deviceType"]);
                }else{
                    $instance->addGeneralPushHistory($bizId,$pushType,$custID,$messageText);
                }


                $instance->addBadgeToCustomerIfNeeded($pushType,$custID);
            }          
        }
        return count($devices);
    }

    /**
     * Summary of sendPushBiz
     * 
     * For extraParams use this default keys if needed:
     *      push_id
     *      external_id
     *      chat_id
     *      
     * For keys of type String: surround with  addslashes()
     * 
     * @param mixed $bizId 
     * @param mixed $pushType 
     * @param mixed $messageText 
     * @param mixed $extraParams 
     */
    public static function sendPushAdmin($bizId,$pushType,$messageText,$extraParams = array()){

        $instance = new self();
        $bizRow = $instance->getBizData($bizId);
        $modCount = $instance->getBizModuleCount($bizId);
        $bizMenuTypeRow = $instance->getBizMainMenuViewType($bizId);
        $devices = $instance->getAdminDeviceRowsByBizId($bizId);
        $messageText = addslashes(stripslashes($messageText));

        $externalID = isset($extraParams["external_id"]) ? $extraParams["external_id"] : 0;
        $campaignID = isset($extraParams["push_id"]) ? $extraParams["push_id"] : 0;
        $chatMessageId = isset($extraParams["chat_id"]) ? $extraParams["chat_id"] : 0;
        $extraParams["biz_id"] = $bizId;

        $extraParamsJsonString = json_encode($extraParams);

        if(count($devices)>0){
            foreach ($devices as $device)
            {
                
                if($instance->verifyAdminPermissionsByPushType($bizId,$device["accountId"],$pushType)){
                    $layout = $device["deviceType"] == "Android" ? $bizMenuTypeRow["ms_view_type_android"] : $bizMenuTypeRow["ms_view_type"];
                    $iOSTeamId = $device["deviceType"] == "IOS" ? $instance->getIOSTeamId($device["deviceRowId"]) : "";
                    $bizName = addslashes(stripslashes($bizRow["biz_short_name"]));

                    $sql = "insert into tbl_queue_push set
                                qup_biz_id=$bizId,
                                qup_app_id={$device["appId"]},
                                qup_biz_name='$bizName',
                                qup_icon='{$bizRow["biz_icon"]}',
                                qup_modules_count=$modCount,
                                qup_biz_firs_mod={$bizRow["biz_first_mode_id"]},
                                qup_entity_id=$campaignID,
                                qup_device_id='{$device["deviceId"]}',
                                qup_message_id=$chatMessageId,
                                qup_biz_layout='$layout',
                                qup_menu_id={$bizRow["biz_menu"]},
                                qup_token='{$device["token"]}',
                                qup_os='{$device["deviceType"]}',
                                qup_message='$messageText',
                                qup_apns_id = {$device["deviceRowId"]},
                                qup_type=$pushType,
                                qup_external_id=$externalID,
                                qup_ios_team_id = '$iOSTeamId',
                                qup_extra_params = '$extraParamsJsonString'
                                ";
                    $instance->db->execute($sql);
                }
    
            }          
        }
    }

   
    /* **************************** */
    /*   Dequeue Functions          */
    /* **************************** */

    public function getQueueMessages($os,$messID){

        $data = $this->db->getTable("select *
			                        from tbl_queue_push
			                        where qup_os='$os'
                                    and qup_message_id=$messID
                                    order by qup_ios_team_id
                                    limit 200");
        if(count($data)>0){
            foreach ($data as $row)
            {
                $this->db->execute("delete from tbl_queue_push where qup_id={$row["qup_id"]}");
            }
        }

        return $data;
    }

	public function deleteQueueMessages($id){
        return $this->db->execute("delete
			                        from tbl_queue_push
			                        where qup_id=$id");
    }

    public function dequeueAndroidPush($messageID=0){

        $androidPushes = $this->getQueueMessages('Android',$messageID);
        $arr = array();

        if(count($androidPushes)>0){
            foreach ($androidPushes as $onePush)
            {
                if ($onePush["qup_entity_id"] != 0 && !in_array($onePush["qup_entity_id"], $arr)) {
                    array_push ($arr, $onePush["qup_entity_id"]);
                }

                $headers = array();
                $fields = array();
                $registrationIDs="";

                $bizName = stripcslashes($onePush["qup_biz_name"]);
                $bizID = $onePush["qup_biz_id"];
                $message = stripcslashes($onePush["qup_message"]);
                $entity = $onePush["qup_entity_id"];
                $bizIcon = $onePush["qup_icon"];
                $deviceId = $onePush["qup_device_id"];
                $messageID = $onePush["qup_message_id"];
                $bizLayoutID = $onePush["qup_menu_id"];
                $mainScreenStructure = $onePush["qup_biz_layout"];
                $pushType = $onePush["qup_type"];
                $externalID = $onePush["qup_external_id"];
                $level = $onePush["qup_level"];
                $modID = $onePush["qup_mod_id"];
                $openView = $onePush["qup_view_android"];
                $registrationIDs[] = $onePush["qup_token"];
                $appId = $onePush["qup_app_id"];
                $gsm_id = $onePush["qup_apns_id"];
                $deviceRow = $this->db->getRow("select * from gsm_devices where gsm_id=$gsm_id");
                $bizRow = $this->db->getRow("select biz_push_type,biz_sbmt_goo_build_version_num,biz_firebase_project from tbl_biz where biz_id={$onePush["qup_biz_id"]}");
                $sendingType = $bizRow["biz_push_type"];
                $deviceToken = $onePush["qup_token"];
                $extraParams = $onePush["qup_extra_params"];

                if($appId == 2){
                    if($deviceRow["gsm_reseller_id"] > 0){
                        $sendingType = $this->db->getVal("select raa_android_push_type from tbl_reseller_app_admin where raa_rid={$deviceRow["gsm_reseller_id"]}");
                    }else{
                        $sendingType = 'fcm';
                    }
                }

                $this->addCustPushMessages($onePush["qup_entity_id"],$onePush["qup_cust_id"],$deviceId,"Android");

                $msg = array(                     
		                 "biz_name" => $bizName,
                         "biz_id" => $bizID,
                         "biz_icon" => $bizIcon,
                         "biz_layout_id" => $bizLayoutID,
                         "biz_mainscreen_structure" => $mainScreenStructure,
                         "customer_device" => $deviceId,
                         "push_type" => $pushType,
                         "push_message" => $message,
                         "extra_params" => $extraParams
                    );

                if($sendingType == 'gcm'){

                    $message = "$bizName@@$bizID@@$message@@$entity@@$bizIcon@@$messageID@@$mainScreenStructure@@$bizLayoutID@@$deviceId@@$pushType@@$externalID@@$modID@@$level@@$openView";

                    $headers = array(
                    'Authorization: key=' . $this->googleApiKey,
                    'Content-Type: application/json'
                    );

                    $fields = array(
                    'registration_ids' => $registrationIDs,
                    'data' => array( "message" => $message ),
                    );

                    $result = utilityManager::curl($this->googleUrl,$headers,$fields);
                }else{

                    if($appId == 2){
                        if($deviceRow["gsm_reseller_id"] > 0){
                            $googleFireBaseApiKey = $this->db->getVal("select fp_key from tbl_firebase_projects where fp_project_id in (select raa_firebase_project from tbl_reseller_app_admin where raa_rid={$deviceRow["gsm_reseller_id"]})");
                            if($googleFireBaseApiKey == ""){
                                $googleFireBaseApiKey = $this->db->getVal("select fp_key from tbl_firebase_projects where fp_project_id='641610969699'");
                            }
                        }else{
                            $googleFireBaseApiKey = $this->db->getVal("select fp_key from tbl_firebase_projects where fp_project_id='641610969699'");
                        }
                    }else{
                        $googleFireBaseApiKey = $this->db->getVal("select fp_key from tbl_firebase_projects where fp_project_id='{$bizRow["biz_firebase_project"]}'");
                    }

                    $headers = array(
                        'Authorization: key=' . $googleFireBaseApiKey,
                        'Content-Type: application/json'
                    );

                    $fields = array(
                        'to' => $onePush["qup_token"],
                        'data' => $msg,
                    );

                    $result = utilityManager::curl($this->googleFireBaseUrl,$headers,$fields);
                }
                
                $jResponse = json_decode($result);
                if($jResponse->success == "1"){
                    $status = "delivered";
                    $fail_reason = "";
                }else{
                    $status = "failed";
                    $fail_reason = isset($jResponse->results[0]->error) ? addslashes($jResponse->results[0]->error) : $result;

                    if($fail_reason == 'NotRegistered'){
                        $this->db->execute("update gsm_devices set status='uninstalled' where gsm_id=$gsm_id");
                    }
                }

                $dbMessage = addslashes($message);
                $gsm_market_id = $deviceRow["gsm_market_id"] == "" ? "-1" : $deviceRow["gsm_market_id"];
                $gsm_reseller_id = $deviceRow["gsm_reseller_id"] == "" ? "-1" : $deviceRow["gsm_reseller_id"];
                
                $sql = "INSERT INTO gsm_messages set clientid='$deviceId',
						fk_device=$gsm_id,
						message='$dbMessage',
						delivery=NOW(),
						status='$status',
                        biz_id=$bizID,
                        app_id=$appId,
                        market_id=$gsm_market_id,
                        reseller_id=$gsm_reseller_id,
                        token='$deviceToken',
                        fail_reason='$fail_reason'";
                $this->db->execute($sql);
                
                $this->deleteQueueMessages($onePush["qup_id"]);
            }

        }
    }

    public function dequeueIOSPush($messageID=0){
        
        $messageID = $messageID == "" ? 0 : $messageID;

        $iosPushes = $this->getQueueMessages('IOS',$messageID);
        $arr = array();
        $lastTeamId = "";
        $encodedTokenForUser = "";
        $http2ch = null;
        
        if(count($iosPushes)>0){          

            if (!defined('CURL_HTTP_VERSION_2_0')) {
                define('CURL_HTTP_VERSION_2_0', 3);
            }           

            foreach ($iosPushes as $onePush)
            {
                if ($onePush["qup_apns_id"] != 0 && !in_array($onePush["qup_apns_id"], $arr)) {
                    array_push ($arr, $onePush["qup_apns_id"]);


                    $bizName = stripcslashes($onePush["qup_biz_name"]);
                    $bizID = $onePush["qup_biz_id"];
                    $message = stripcslashes($onePush["qup_message"]);
                    $pid = $onePush["qup_entity_id"];
                    $deviceID = $onePush["qup_device_id"];
                    $messageID = $onePush["qup_message_id"];
                    $appId = $onePush["qup_app_id"];
                    $apns_id = $onePush["qup_apns_id"];
                    $deviceRow = $this->db->getRow("select * from apns_devices where pid=$apns_id");
                    $deviceToken = $deviceRow["devicetoken"];
                    $http2_server = ($deviceRow["development"] == "production") ? $this->http2_server_production : $this->http2_server_development;
                    $pushType = $onePush["qup_type"];
                    $externalID = $onePush["qup_external_id"];
                    $level = $onePush["qup_level"];
                    $modID = $onePush["qup_mod_id"];
                    $openView = $onePush["qup_view"];
                    $extraParams = $onePush["qup_extra_params"];

                    $app_bundle_id = pushManager::getBundleId($deviceRow,$appId);

                    if($onePush["qup_ios_team_id"] != $lastTeamId){
                        
                        if($http2ch != null){
                            curl_close($http2ch);
                        }
                        
                        $http2ch = curl_init();
                        curl_setopt($http2ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
                        $lastTeamId = $onePush["qup_ios_team_id"];
                        if(utilityManager::contains($app_bundle_id,"com.bb.bb")){
                            $encodedTokenForUser =  file_get_contents(PUSH_SERVER."/admin/libs/refreshIOSTokenEnterprise.php?apnsid=$apns_id");
                        }else{
                            $encodedTokenForUser =  file_get_contents(PUSH_SERVER."/admin/libs/refreshIOSToken.php?apnsid=$apns_id");
                        }
                    }
                    
                    if($app_bundle_id != ""){

                        $payload = array();
                        $messageArray = array();

                        $messageArray["alert"] = $message;
                        $messageArray["sound"] = 'bingbong.aiff';
                        if($deviceRow["appversion"] >= 12.0){
                            $messageArray["badge"] = 1;
                        }
                        $payload['aps'] = $messageArray;

                        //FOR_DEV_ONLY - check if version supporting new push structure - can be removed after 1/11/2018
                        if($deviceRow["appversion"] >= 12.0){

                            $payload["biz_name"] = $bizName;
                            $payload["biz_id"] = $bizID;
                            $payload["customer_device"] = $deviceID;
                            $payload["push_type"] = $pushType;
                            $payload["extra_params"] = $extraParams;

                        }else{
                            $pushType = $this->getOldPushTypeByNewType($pushType);

                            $payload["acme1"] = $bizName;
                            $payload["acme2"] = $bizID;
                            $payload["acme3"] = 1;
                            $payload["acme4"] = 0;
                            $payload["acme5"] = $pid;
                            $payload["acme6"] = $deviceID;
                            $payload["acme7"] = $messageID;
                            $payload["acme8"] = $pushType;
                            $payload["acme9"] = $externalID;
                            $payload["acme10"] = $level;
                            $payload["acme11"] = $modID;
                            $payload["acme12"] = $openView;
                        }

                        $payloadJSN = json_encode($payload);

                        $dbMessage = addslashes($payloadJSN);
                        $sql = "INSERT INTO apns_messages set clientid='{$deviceRow["deviceuid"]}',
						            fk_device=$apns_id,
						            message='$dbMessage',
						            delivery=NOW(),
						            status='queued',
						            created=NOW(),
						            modified=NOW(),
                                    biz_id=$bizID,
                                    app_id=$appId,
                                    market_id={$deviceRow["market_id"]},
                                    reseller_id={$deviceRow["reseller_id"]},
                                    team_id='$lastTeamId',
                                    token='$deviceToken'";

                        $internalMessId = $this->db->execute($sql);

                        if($pid != 0) $this->addCustPushMessages($pid,$onePush["qup_cust_id"],$deviceID,"IOS");                      
                        
                        $response = $this->sendHTTP2Push($http2ch, $http2_server, $encodedTokenForUser, $app_bundle_id, $payloadJSN, $deviceToken);
                        
                        if($response["code"] == "200"){
                            $this->db->execute("update apns_messages set status='delivered', modified=NOW() where pid=$internalMessId");
                            $this->custPushDelivered($pid,$deviceID);
                        }
                        else if($response["code"] == "-1"){
                            $this->db->execute("update apns_messages set status='failed', modified=NOW(), fail_reason='{$response["data"]}' where pid=$internalMessId");
                        }
                        else{
                            $responseData = addslashes($response["data"]);
                            $this->db->execute("update apns_messages set status='failed', modified=NOW(), fail_reason='$responseData' where pid=$internalMessId");
                            if(utilityManager::contains($responseData,"BadDeviceToken")){
                                $sql = "UPDATE apns_devices
				                        SET status='uninstalled'
				                        WHERE devicetoken='$deviceToken'";
                                $this->db->execute($sql);
                            }
                        }
                    }
                    $this->deleteQueueMessages($onePush["qup_id"]);
                }
            }
            if($http2ch != null){
                curl_close($http2ch);
            }
        }
    }

    private function sendHTTP2Push($http2ch, $http2_server, $providerTokenJWT, $app_bundle_id, $message, $deviceToken) {


        $url = "{$http2_server}/3/device/{$deviceToken}";
        $expires = time() + 86400;

        // headers
        $headers = array(
            "apns-topic: {$app_bundle_id}",
            "User-Agent: My Sender",
            "Authorization: bearer {$providerTokenJWT}",
            "apns-expiration = {$expires}",
            "apns-priority = 10"

        );

        // other curl options
        curl_setopt_array($http2ch, array(
            CURLOPT_URL => $url,
            CURLOPT_PORT => 443,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POST => TRUE,
            CURLOPT_POSTFIELDS => $message,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HEADER => 1
        ));


        $result = curl_exec($http2ch);

        $response = array();

        if ($result === FALSE) {
            $response["code"] = "-1";
            $response["data"] = "False Result";
        }else{
            $response["code"] = curl_getinfo($http2ch, CURLINFO_HTTP_CODE);
            $response["data"] = $result;
        }

        return $response;
    }


    /* **************************** */
    /*   Public Helpers             */
    /* **************************** */

    public static function getSystemAlertPushMessageText($alertId,$bizId,$extraParams = array()){

        $eManager = new emailManager();
        return $eManager->getNotificationMessageFromBiz($alertId,$bizId,$extraParams);
    }

    public static function markPushForSending($campaignId){

        $instance = new self();

        $instance->db->execute("UPDATE tbl_push SET pu_send_on=NOW() WHERE pu_id=$campaignId");
    }

    public static function getAppleCredentials($apns_id){
        
        if($apns_id <= ""){
            return "";
        }

        $instance = new self();
        
        $apnsRow = $instance->db->getRow("SELECT appid, market_id, reseller_id FROM apns_devices WHERE pid = $apns_id");
        $appId = $apnsRow["appid"];
        $marketId = $apnsRow["market_id"];
        $resellerId = $apnsRow["reseller_id"];
        
        $credentialsRow = array();

        //MARKETS
        if ($appId == 0){ 
            $credentialsRow = $instance->db->getRow("SELECT * 
                                                 FROM tbl_apple_credentials 
                                                 WHERE ac_owner_id = (
                                                        SELECT owner_id FROM tbl_markets,tbl_owners,tbl_resellers 
                                                        WHERE mrkt_id = $marketId
                                                        AND mrkt_reseller_id = reseller_id
                                                        AND reseller_id = owner_reseller_id)
                                                ");
        //APP ADMINS                                        
        }else if ($appId == 2){
            //Backwards Compatibility
            if ($resellerId == -1){
                $resellerId = 1;   
            }
            $credentialsRow = $instance->db->getRow("SELECT * 
                                                FROM tbl_apple_credentials
                                                WHERE ac_owner_id = (
                                                    SELECT owner_id 
                                                    FROM tbl_owners,tbl_resellers 
                                                    WHERE reseller_id = $resellerId
                                                    AND reseller_id = owner_reseller_id)");
        //BIZ                            
        }else{
            $ownerId = $instance->db->getVal("select biz_owner_id from tbl_biz where biz_id = $appId");
            $credentialsRow = $instance->db->getRow("SELECT * 
                                                 FROM tbl_apple_credentials 
                                                 WHERE ac_owner_id = $ownerId AND ac_biz_id = $appId
                                                 ");
            if($credentialsRow["ac_id"] == ""){
                $credentialsRow = $instance->db->getRow("SELECT * 
                                                 FROM tbl_apple_credentials 
                                                 WHERE ac_owner_id = $ownerId
                                                 ");
            }
        }

        
        return $credentialsRow;
    
    }

    public static function getAppleCredentialsEnterprise($apns_id){
        
        $credentialsRow = array();

        if($apns_id <= ""){
            return $credentialsRow;
        }

        $instance = new self();
        
        $apnsRow = $instance->db->getRow("SELECT appid, market_id, reseller_id FROM apns_devices WHERE pid = $apns_id");
        $appId = $apnsRow["appid"];
        $marketId = $apnsRow["market_id"];
        $resellerId = $apnsRow["reseller_id"];
        

        //MARKETS
        if ($appId == 0){ 
            $credentialsRow = $instance->db->getRow("SELECT * 
                                                 FROM tbl_apple_enterprise_credentials
                                                 WHERE ac_owner_id = (
                                                        SELECT owner_id FROM tbl_markets,tbl_owners,tbl_resellers 
                                                        WHERE mrkt_id = $marketId
                                                        AND mrkt_reseller_id = reseller_id
                                                        AND reseller_id = owner_reseller_id)
                                                ");
            //APP ADMINS                                        
        }else if ($appId == 2){
            //Backwards Compatibility
            if ($resellerId == -1){
                $resellerId = 1;   
            }
            $credentialsRow = $instance->db->getRow("SELECT * 
                                                FROM tbl_apple_enterprise_credentials
                                                WHERE ac_owner_id = (
                                                    SELECT owner_id 
                                                    FROM tbl_owners,tbl_resellers 
                                                    WHERE reseller_id = $resellerId
                                                    AND reseller_id = owner_reseller_id)");
            //BIZ                            
        }else{
            $ownerId = $instance->db->getVal("select biz_owner_id from tbl_biz where biz_id = $appId");
            $credentialsRow = $instance->db->getRow("SELECT * 
                                                 FROM tbl_apple_enterprise_credentials
                                                 WHERE ac_owner_id = $ownerId
                                                 ");
        }

        
        return $credentialsRow;
        
    }

    public static function getBundleId($deviceRow,$appId){

        $instance = new self();
        $app_bundle_id = $deviceRow["bundle_id"];

        if($app_bundle_id == ""){
            if($appId == "0")
            {
                $app_bundle_id = $instance->db->getVal("select mrkt_skin_ur_ios from tbl_markets where mrkt_id={$deviceRow["market_id"]}"); //"com.paptap";
            }
            else if($appId == "2")
            {
                $app_bundle_id = $instance->db->getVal("select raa_ios_bundle_id from tbl_reseller_app_admin where raa_rid={$deviceRow["reseller_id"]}");
                if($app_bundle_id == ""){
                    $app_bundle_id = "com.paptap.pt2";
                }
            }
            else {
                $app_bundle_id = $instance->db->getVal("select biz_sbmt_apl_bundle_id from tbl_biz where biz_id=$appId");
            }
        }
        return $app_bundle_id;
    }

    /* **************************** */
    /*   Private Helpers            */
    /* **************************** */

    
    private function getBizData($bizId){
        return $this->db->getRow("select * from tbl_biz where biz_id=$bizId");
    }

    private function getBizModuleCount($bizID=""){

        if($bizID == "") $bizID = $_SESSION["appData"]["biz_id"];
        

        if ($bizID == ""){
            return 0;
        }

        $result = $this->db->getVal("select count(biz_mod_mod_id) biz_mod_mod_id
                                    from tbl_biz_mod
                                    where biz_mod_biz_id=$bizID
                                    and biz_mod_mod_id not in(0,16)
                                    and biz_mod_active=1
                                    ");
        
        return $result;
    }

    private function getBizMainMenuViewType($bizID=""){

        if($bizID == "") $bizID = $_SESSION["appData"]["biz_id"];

        if ($bizID == ""){
            return array();
        }

        $result = $this->db->getRow("SELECT ms_view_type,ms_view_type_android
                                    FROM tbl_biz_mod,tbl_mod_struct
                                    WHERE ms_template=biz_mod_stuct
                                    AND biz_mod_biz_id = $bizID
                                    AND biz_mod_mod_id = 0");

        return $result;
    }

    private function getAdminDeviceRowsByBizId($bizID){
        $deviceCollection = array();
        $duplicatesArray = array();

        $resellerRow = $this->db->getRow("SELECT owner_reseller_id,IFNULL(raa_id,0) reseller_has_manager 
                                            FROM tbl_biz, tbl_owners LEFT JOIN tbl_resellers 
                                            ON owner_reseller_id=reseller_id 
                                            LEFT JOIN tbl_reseller_app_admin
                                            ON owner_reseller_id = raa_rid
                                            WHERE biz_owner_id=owner_id 
                                            AND biz_id=$bizID");

        $resellerId = $resellerRow["owner_reseller_id"];
        if($resellerId == 0 || $resellerRow["reseller_has_manager"] == 0){ 
            $resellerId = "0,-1";
        }

        //get admin list for android
        $data = $this->db->getTable("SELECT gsm_token token ,gsm_appid appId,gsm_device_id deviceId,gsm_id deviceRowId, 'Android' deviceType, ba_account_id accountId
                                        FROM gsm_devices,tbl_biz_admin
                                        WHERE ba_biz_id=$bizID
                                        AND gsm_device_id = ba_device_id
                                        AND gsm_appid=ba_app_id
                                        AND gsm_reseller_id IN ($resellerId)
                                    ");
        if(count($data) > 0){
            foreach ($data as $deviceRow)
            {
                if (!in_array($deviceRow["token"], $duplicatesArray)) {
                    array_push($deviceCollection,$deviceRow);
                    array_push($duplicatesArray,$deviceRow["token"]);
                }
            }            
        }

        //get admin list for IOS
        $data = $this->db->getTable("SELECT devicetoken token ,appid appId,deviceuid deviceId,pid deviceRowId, 'IOS' deviceType, ba_account_id accountId
			                                FROM apns_devices,tbl_biz_admin
			                                WHERE ba_biz_id=$bizID
                                            AND deviceuid = ba_device_id
			                                AND status='active'
                                            AND appid=ba_app_id
                                            AND reseller_id IN ($resellerId)
                                    ");
        if(count($data) > 0){
            foreach ($data as $deviceRow)
            {
                if (!in_array($deviceRow["token"], $duplicatesArray)) {
                    array_push($deviceCollection,$deviceRow);
                    array_push($duplicatesArray,$deviceRow["token"]);
                }
            }            
        }

        return $deviceCollection;
    }

    private function getDeviceRowsByCustId($custId){

        $deviceCollection = array();
        $duplicatesArray = array();

        $custRow = $this->db->getRow("SELECT * FROM tbl_customers WHERE cust_id=$custId");

        //if customer exist
        if($custRow["cust_id"] != ""){

            //get customer for android from tbl_customers
            $sql = "SELECT gsm_token token ,gsm_appid appId,gsm_device_id deviceId,gsm_id deviceRowId, 'Android' deviceType
                FROM gsm_devices
                WHERE gsm_appid = {$custRow["cust_appid"]}
                AND gsm_market_id = {$custRow["cust_market_id"]}
                AND gsm_reseller_id = {$custRow["cust_reseller_id"]}
                AND gsm_device_id = '{$custRow["cust_mobile_serial"]}'
                ";
            $data = $this->db->getTable($sql);

            if(count($data) > 0){
                foreach ($data as $deviceRow)
                {
                    if (!in_array($deviceRow["token"], $duplicatesArray)) {
                        array_push($deviceCollection,$deviceRow);
                        array_push($duplicatesArray,$deviceRow["token"]);
                    }
                }            
            }

            //get customer for android from tbl_cust_device
            $sql = "SELECT gsm_token token ,gsm_appid appId,gsm_device_id deviceId,gsm_id deviceRowId, 'Android' deviceType
                FROM gsm_devices,tbl_cust_device
                WHERE cd_cust_id = $custId
                AND cd_app_id = {$custRow["cust_appid"]}
                AND gsm_appid = cd_app_id
                AND gsm_market_id = {$custRow["cust_market_id"]}
                AND gsm_reseller_id = {$custRow["cust_reseller_id"]}
                AND gsm_device_id = cd_device
                        ";
            $data = $this->db->getTable($sql);

            if(count($data) > 0){
                foreach ($data as $deviceRow)
                {
                    if (!in_array($deviceRow["token"], $duplicatesArray)) {
                        array_push($deviceCollection,$deviceRow);
                        array_push($duplicatesArray,$deviceRow["token"]);
                    }
                }            
            }


            //get customer for IOS from tbl_customers
            $sql = "SELECT devicetoken token ,appid appId,deviceuid deviceId,pid deviceRowId, 'IOS' deviceType
                FROM apns_devices
                WHERE appid = {$custRow["cust_appid"]}
                AND market_id = {$custRow["cust_market_id"]}
                AND reseller_id = {$custRow["cust_reseller_id"]}
                AND deviceuid = '{$custRow["cust_mobile_serial"]}'
                ";
            $data = $this->db->getTable($sql);

            if(count($data) > 0){
                foreach ($data as $deviceRow)
                {
                    if (!in_array($deviceRow["token"], $duplicatesArray)) {
                        array_push($deviceCollection,$deviceRow);
                        array_push($duplicatesArray,$deviceRow["token"]);
                    }
                }            
            }


            //get customer for IOS from tbl_cust_device
            $sql = "SELECT devicetoken token ,appid appId,deviceuid deviceId,pid deviceRowId, 'IOS' deviceType
                FROM apns_devices,tbl_cust_device
                WHERE cd_cust_id = $custId
                AND cd_app_id = {$custRow["cust_appid"]}
                AND appid = cd_app_id
                AND market_id = {$custRow["cust_market_id"]}
                AND reseller_id = {$custRow["cust_reseller_id"]}
                AND deviceuid = cd_device
                ";
            $data = $this->db->getTable($sql);

            if(count($data) > 0){
                foreach ($data as $deviceRow)
                {
                    if (!in_array($deviceRow["token"], $duplicatesArray)) {
                        array_push($deviceCollection,$deviceRow);
                        array_push($duplicatesArray,$deviceRow["token"]);
                    }
                }            
            }
        }

        return $deviceCollection;
    }

    private function getIOSTeamId($apns_id){
        
        if($apns_id <= ""){
            return "";
        }

        $crednetialsRow = pushManager::getAppleCredentials($apns_id);

        if(!isset($crednetialsRow["ac_team_id"]) || $crednetialsRow["ac_team_id"] == ""){
            return "2LLS7MGTY6";
        }

        return $crednetialsRow["ac_team_id"];
        
    }
    
    //Structure for push which is NOT a campaign
    private function getStructureDataByBizAndPushType($bizId,$pushType,$externalId){

        $mainScreenData = $this->getModuleLastStructureByBizModId($bizId,0);

        $response = array();
        $response["modId"] = 0;
        $response["level"] = 1;
        $response["iosView"] = $mainScreenData["ms_view_end"];
        $response["androidView"] = $mainScreenData["ms_view_end_android"];

        switch($pushType){
            case enumPushType::biz_landingPage:                
                $response["modId"] = 29;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_promoteCoupon:                
                $response["modId"] = 26;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_promoteProduct:                
                $response["modId"] = 9;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $parentLevel = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                $response["level"] = $parentLevel + 1;
                break;
            case enumPushType::biz_promotePunchCard:                
                $response["modId"] = 6;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_greetingsCard:                
                $response["modId"] = 29;  
                $sructRow["ms_view_end"] = "Greeting";
                $sructRow["ms_view_end_android"] = "Greeting";
                $response["level"] = $this->getLevelNumberByExternalId($response["modId"],$externalId);
                break;
            case enumPushType::biz_chat:                
                $response["modId"] = 7;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                break;
            case enumPushType::biz_personalBooking:
            case enumPushType::biz_personalFriends:
            case enumPushType::biz_personalRewards:
            case enumPushType::biz_personalOrders:
            case enumPushType::biz_personalPRequest:
            case enumPushType::biz_personalDocuments:
                $response["modId"] = 31;  
                $structureData = $this->getModuleLastStructureByBizModId($bizId,$response["modId"]);
                $response["iosView"] = $structureData["ms_view_end"];
                $response["androidView"] = $structureData["ms_view_end_android"];
                break;
        }

        return $response;
    }
    
    //Structure for push which is a campaign
    private function getStructureDataByCampaign($campaignID){

        $cmapaignData = $this->getPushRow($campaignID);

        $response = array();
        $response["modId"] = $cmapaignData["pu_mod_id"];
        $response["level"] = $cmapaignData["pu_level"];
        $response["iosView"] = $cmapaignData["pu_view"];
        $response["androidView"] = $cmapaignData["pu_view_android"];

        return $response;
        
    }


    //This function returns the LAST/END structure for a specific module at biz
    private function getModuleLastStructureByBizModId($bizID,$modId){
        return $this->db->getRow("SELECT ms_view_end,ms_view_end_android 
                                  FROM tbl_mod_struct,tbl_biz_mod
                                  WHERE biz_mod_stuct=ms_template
                                  AND biz_mod_biz_id=$bizID
                                  AND biz_mod_mod_id = ms_mod_id
                                  AND ms_mod_id=$modId
                                  ORDER by ms_level_no DESC
                                  LIMIT 1");
    }

    private function getLevelNumberByExternalId($modId,$externalId){
        if($externalId != 0){
            return $this->db->getVal("select md_level_no from tbl_mod_data$modId where md_row_id=$externalId");
        }else{
            return 1;
        }
    }

    private function verifyAdminPermissionsByPushType($bizId,$accountId,$pushType){

        //If we don't know this account - we assume that push is allowed (backwards compatibility)
        if($accountId == '' || $accountId == '0'){
            return true;
        }

        $permission = new permissionsManager();
        $hasSystemPushPermission = $permission->accountHasPermission($accountId,$bizId,'system_push');

        $permissionArea = "";
        switch ($pushType)
        {
            case enumPushType::admin_newMeeting:
                $permissionArea = "schedule";
                break;
            case enumPushType::admin_newFormFilled:
            case enumPushType::admin_newClubMember:
            case enumPushType::admin_newDocument:
                $permissionArea = "vip_club";
                break;
            case enumPushType::admin_newOrder:
                $permissionArea = "orders";
                break;
            case enumPushType::admin_chat:
                $permissionArea = "live_chat";
                break;
            case enumPushType::admin_couponClaim:
                $permissionArea = "coupons";
                break;
            case enumPushType::admin_unattendedOrders:
                $permissionArea = "orders";
                break;  
            case enumPushType::admin_messageOnly:
                $permissionArea = "owner";
                break;
        }
                
        if($permissionArea == "owner"){
            $hasPushPermission = $permission->accountIsOwner($accountId,$bizId); 
        }else{
            $hasPushPermission = $permission->accountHasPermission($accountId,$bizId,$permissionArea);                   
        }

        return $hasPushPermission && $hasSystemPushPermission;
    }

    private function getReminderData($reminderID){
        return $this->db->getRow("SELECT  rem_id,
                                            0 as pu_id,
                                            pu_groups,
                                            pu_cust_id,
                                            rem_text as pu_short_mess,
                                            rem_biz_id as pu_biz_id,
                                            rem_created as pu_created,
                                            rem_repeat,
                                            0 as pu_based_location,
                                            pu_target
                                            FROM tbl_reminders 
                                            WHERE rem_id = $reminderID");
    }

    //Device ID is for legacy
    private function addPushHistory($campaignID, $pushType, $deviceID="", $customerId=0,$external_id = 0){

        $pushRow = $this->getPushRow($campaignID);

        $entityName = $this->getDBStringPushTypeByNewType($pushType);
        
        if(!isset($pushRow["pu_biz_id"])){
            $custData = customersManager::getCustomerDetailsStatic($customerId);
            $pushRow["pu_biz_id"] = $custData['cust_biz_id'];
        }

        $createdDate = (isset($pushRow["pu_created"])) ? "'{$pushRow["pu_created"]}'" : "null";        
        $mess = addslashes($pushRow["pu_short_mess"]);
        
        $this->db->execute("INSERT IGNORE INTO tbl_push_history SET
                                puh_biz_id = {$pushRow["pu_biz_id"]},
                                puh_cust_id=$customerId,
                                puh_push_id = $campaignID,
                                puh_device_id='$deviceID',
                                puh_short_mess = '$mess',
                                puh_type = '$entityName',
                                pu_groups = '{$pushRow["pu_groups"]}',
                                puh_created = $createdDate,
                                puh_external_id = $external_id
                                ");
    }

    private function addReminderHistory($reminderId,$pushType,$deviceID="", $customerId=0){

        $pushRow = $this->getReminderData($reminderId);

        $entityName = $this->getDBStringPushTypeByNewType($pushType);
        

        $createdDate = (isset($pushRow["pu_created"])) ? "'{$pushRow["pu_created"]}'" : "null";        
        $mess = addslashes($pushRow["pu_short_mess"]);
        
        $this->db->execute("INSERT IGNORE INTO tbl_push_history SET
                                puh_biz_id = {$pushRow["pu_biz_id"]},
                                puh_cust_id=$customerId,
                                puh_push_id = 0,
                                puh_device_id='$deviceID',
                                puh_short_mess = '$mess',
                                puh_type = '$entityName',
                                pu_groups = '{$pushRow["pu_groups"]}',
                                puh_created = $createdDate
                                ");

    }

    private function addPRequestReminderHistory($bizId,$pushType, $customerId=0,$message=""){


        $entityName = $this->getDBStringPushTypeByNewType($pushType);
              
        $mess = addslashes($message);
        
        $this->db->execute("INSERT IGNORE INTO tbl_push_history SET
                                puh_biz_id = $bizId,
                                puh_cust_id=$customerId,
                                puh_push_id = 0,
                                puh_short_mess = '$mess',
                                puh_type = '$entityName'
                                ");

    }

    private function addOrderReminderHistory($bizId,$pushType, $customerId=0,$message=""){


        $entityName = $this->getDBStringPushTypeByNewType($pushType);
              
        $mess = addslashes($message);
        
        $this->db->execute("INSERT IGNORE INTO tbl_push_history SET
                                puh_biz_id = $bizId,
                                puh_cust_id=$customerId,
                                puh_push_id = 0,
                                puh_short_mess = '$mess',
                                puh_type = '$entityName'
                                ");

    }

    private function addGeneralPushHistory($bizId,$pushType, $customerId=0,$message=""){


        $entityName = $this->getDBStringPushTypeByNewType($pushType);
        
        $mess = addslashes($message);
        
        $this->db->execute("INSERT IGNORE INTO tbl_push_history SET
                                puh_biz_id = $bizId,
                                puh_cust_id=$customerId,
                                puh_push_id = 0,
                                puh_short_mess = '$mess',
                                puh_type = '$entityName'
                                ");

    }

    private function addCustPushMessages($campaignID,$cust_id,$deviceId,$deviceType){

        $this->db->execute("INSERT IGNORE INTO tbl_cust_push
                                SET cup_cust_id='{$deviceId}',
                                cup_customer_id=$cust_id,
                                cup_push_id=$campaignID,
                                cup_device_type='$deviceType'");
    }

    private function getPushTypeForCampaign($campaignType){

        $pushType = enumPushType::biz_messageOnly;
        switch($campaignType){
            case 3:
                $pushType = enumPushType::biz_promoteProduct;
                break;
            case 2:
                $pushType = enumPushType::biz_promoteCoupon;
                break;
            case 4:
                $pushType = enumPushType::biz_promotePunchCard;
                break;
            case 1:
                $pushType = enumPushType::biz_landingPage;
                break;
            case 6:
            case 7:
                $pushType = enumPushType::biz_greetingsCard;
                break;
        }
        return $pushType;
    }

    private function getExternalIdForCampaign($campaignID){

        $campaignData = $this->getPushRow($campaignID);

        $externalId = 0;
        switch($campaignData["pu_type"]){
            case 3:
                $externalId = $campaignData["pu_product_id"];
                break;
            case 2:
                $externalId = $campaignData["pu_coupon_id"];
                break;
            case 4:
                $externalId = $campaignData["pu_loyalty_id"];
                break;
            default:
                $externalId = $campaignData["pu_id"];
        }
        return $externalId;
    }

    private function markGeoPushCampaignAsSent($campaignId){
        $this->db->execute("UPDATE tbl_push SET pu_async_sent=1 WHERE pu_id=$campaignId");
    }

    private function markGeoPushCampaignsAsNotSent(){
        $this->db->execute("UPDATE tbl_push SET pu_async_sent=0 
                                    WHERE pu_based_location=1 
                                    AND pu_active=1 
                                    AND pu_async_sent=1");
    }

    private function markReminderAsNotActive($reminderId){

        $this->db->execute("update tbl_reminders set rem_active = 0 where rem_id=$reminderId");
    }

    private function updateReminderToNextDate($repeetCycle, $reminderId){
        $newDate = "";
        switch($repeetCycle){
            case 'Daily':
                $newDate = "DATE_ADD(rem_next_send,INTERVAL 1 day)";
                break;
            case 'Weekly':
                $newDate = "DATE_ADD(rem_next_send,INTERVAL 1 week)";
                break;
            case 'Monthly':
                $newDate = "DATE_ADD(rem_next_send,INTERVAL 1 month)";
                break;
            case 'Yearly':
                $newDate = "DATE_ADD(rem_next_send,INTERVAL 1 year)";
                break;
        }

        $this->db->execute("update tbl_reminders set rem_next_send = $newDate where rem_id=$reminderId");
    }

    private function getPaymentRequestRemindersList(){
        
        return $this->db->getTable("SELECT tbl_cust_transaction_order.*,tbl_cust_transaction.*
                                        FROM tbl_biz,tbl_cust_transaction_order 
                                        LEFT JOIN tbl_cust_transaction ON cto_id=tr_order_id
                                        WHERE biz_id=cto_biz_id
                                        AND biz_status=1
                                        AND cto_next_reminder <= NOW() 
                                        AND cto_paid_manual=0
                                        AND tr_id is null
                                        AND cto_order_type='paymentRequest'
                                        AND cto_reminder_type <> ''");
    }

    private function updatePaymentRequestReminderToNextDate($repeetCycle, $transactionOrderId){
        $newDate = "";
        switch($repeetCycle){
            case 'Daily':
                $newDate = "DATE_ADD(cto_next_reminder,INTERVAL 1 day)";
                break;
            case 'Weekly':
                $newDate = "DATE_ADD(cto_next_reminder,INTERVAL 1 week)";
                break;
            case 'Monthly':
                $newDate = "DATE_ADD(cto_next_reminder,INTERVAL 1 month)";
                break;
            case 'Yearly':
                $newDate = "DATE_ADD(cto_next_reminder,INTERVAL 1 year)";
                break;
        }

        $this->db->execute("update tbl_cust_transaction_order set cto_next_reminder = $newDate where cto_id=$transactionOrderId");
    }

    private function addBadgeToCustomerIfNeeded($pushType,$customerId){
       
        //don't execute for this type of campaigns
        switch($pushType){
            case enumPushType::biz_landingPage:
            case enumPushType::biz_promoteCoupon:
            case enumPushType::biz_promoteProduct:
            case enumPushType::biz_promotePunchCard:            
            case enumPushType::biz_greetingsCard:
            case enumPushType::biz_geoLocation:
            case enumPushType::biz_messageOnly:
                break;
            default:
                return;
        }

        customersManager::addBadgeToCustomer($customerId,'notifications');
    }

    private function updateCampaignSent($campaignId,$sentPushes,$pushType){

        //don't execute for this type of campaigns
        switch($pushType){
            case enumPushType::biz_geoLocation:
            case enumPushType::biz_greetingsCard:
            case enumPushType::biz_bobileXPoints:
            case enumPushType::biz_bobileXScratch:
            case enumPushType::biz_personalRewards:
                return;
        }

        $this->db->execute("UPDATE tbl_push 
                            SET pu_num_sent=$sentPushes,
                            pu_status='Sent',
                            pu_sent=NOW(),
                            pu_async_sent = 0 
                            WHERE pu_id=$campaignId");
    }

    private function updateHolidayNextSendDate($campaignId){

        $this->db->execute("UPDATE tbl_push 
                            SET pu_send_on = DATE_ADD(pu_send_on, INTERVAL 1 YEAR)
                            WHERE pu_id=$campaignId");
    }

    private function getOldPushTypeByNewType($pushId){

        switch($pushId){

            case enumPushType::biz_landingPage:
                return 1;
            case enumPushType::biz_promoteCoupon:
                return 2;
            case enumPushType::biz_promoteProduct:
                return 3;
            case enumPushType::biz_promotePunchCard:
                return 4;
            case enumPushType::biz_greetingsCard:
                return 6;
            case enumPushType::biz_greetingsCard:
                return 7;
            case enumPushType::biz_personalRewards:
                return 8;
            case enumPushType::biz_personalDocuments:
                return 9;
            case enumPushType::biz_bobileXPoints:
                return 11;
            case enumPushType::biz_bobileXScratch:
                return 12;
            case enumPushType::biz_bobileXMembership:
                return 13;
            case enumPushType::biz_personalBooking:
                return 14;
            default:
                return 10;
        }
    }

    private function getDBStringPushTypeByNewType($pushType){

        switch($pushType){

            case enumPushType::biz_landingPage:
                return "Campaign";
            case enumPushType::biz_promoteCoupon:
                return "Campaign";
            case enumPushType::biz_promoteProduct:
                return "Campaign";
            case enumPushType::biz_promotePunchCard:
                return "Campaign";
            case enumPushType::biz_greetingsCard:
                return "Greeting";
            case enumPushType::biz_greetingsCard:
                return "Greeting";
            case enumPushType::biz_personalRewards:
                return "Benefit";
            case enumPushType::biz_personalDocuments:
                return "Reminder";
            case enumPushType::biz_messageOnly:
                return "Reminder";
            case enumPushType::biz_bobileXPoints:
                return "Benefit";
            case enumPushType::biz_bobileXScratch:
                return "Benefit";
            case enumPushType::biz_bobileXMembership:
                return "Benefit";
            case enumPushType::biz_personalBooking:
                return "Reminder";
            case enumPushType::biz_personalPRequest:
                return "Reminder";
            case enumPushType::biz_personalOrders:
                return "Reminder";
            case enumPushType::biz_geoLocation:
                return "Geo";
            case enumPushType::geo_subscription_trigger:
                return "Subscription";
            case enumPushType::geo_punchpass_trigger:
                return "Punch pass";
            default:
                return "Reminder";
        }
    }

    /*   GETTING Different Campaigns Lists     */

    private function getReminderCampaignsToSend(){
        return $this->db->getTable("SELECT  rem_id,
                                            0 as pu_id,
                                            pu_groups,
                                            pu_cust_id,
                                            rem_text as pu_short_mess,
                                            rem_biz_id as pu_biz_id,
                                            rem_created as pu_created,
                                            rem_repeat,
                                            0 as pu_based_location,
                                            pu_target
                                            FROM tbl_reminders, tbl_biz 
                                            WHERE biz_id=rem_biz_id
                                            AND rem_next_send <= now() 
                                            AND rem_active=1
                                            AND biz_status=1");
    }

    private function getLocationBasedCampaignsToSend(){
        return $this->db->getTable("SELECT tbl_push.* FROM tbl_push,tbl_biz
                                    WHERE biz_id=pu_biz_id
                                    AND biz_status=1
                                    AND pu_based_location=1 
                                    AND pu_active=1 
                                    AND pu_async_sent=0 
                                    LIMIT 1000");
    }

    private function getGreetingsCampaignsToSend(){
        return $this->db->getTable("SELECT tbl_push.* 
                                        FROM tbl_push,tbl_biz 
                                        WHERE biz_id=pu_biz_id
                                        AND biz_status=1
                                        AND pu_type=7");
    }

    private function getHolidaysCampaignsToSend(){
        return $this->db->getTable("SELECT tbl_push.* FROM tbl_push,tbl_biz 
                                    WHERE biz_id=pu_biz_id
                                    AND biz_status=1
                                    AND pu_type=6 
                                    AND (pu_sent IS NULL OR pu_sent <= DATE_SUB(NOW(),INTERVAL 1 DAY))
                                    AND pu_send_on <= NOW()
                                    AND pu_async_sent = 0");
    }

    private function getPreScheduledCampaignsToSend(){
        return $this->db->getTable("SELECT tbl_push.* FROM tbl_push,tbl_biz 
                                    WHERE biz_id=pu_biz_id
                                    AND biz_status=1
                                    AND pu_sent IS NULL 
                                    AND (pu_send_on IS NOT NULL && pu_send_on <= NOW()) 
                                    AND pu_async_sent = 0
                                    AND pu_type NOT IN (6,7)");
    }



    /*   GETTING Different Customers Lists     */

    private function getCustomersInRangeForLocationPushCampaign($pushData){
        return $this->db->getTable("SELECT *,
                                          ( 6371 * acos( cos( radians({$pushData["pu_longi"]}) ) * cos( radians( cust_longt ) )* cos( radians( cust_lati ) - radians({$pushData["pu_lati"]}) ) + sin( radians({$pushData["pu_longi"]}) ) * sin(radians(cust_longt)) ) ) AS distance
                                    FROM tbl_customers
                                    WHERE cust_biz_id={$pushData["pu_biz_id"]}
                                    AND cust_location_updated IS NOT NULL
                                    AND cust_location_updated > DATE_SUB(NOW(), INTERVAL 1 HOUR)
                                    AND cust_id NOT IN (SELECT puh_cust_id FROM (
											                        SELECT puh_cust_id ,DATE_ADD(MAX(puh_sent), INTERVAL 1 DAY) run_date
											                        FROM tbl_push_history 
											                        WHERE puh_type = 'Geo'
											                        AND puh_cust_id > 0
											                        AND puh_push_id = {$pushData["pu_id"]}
											                        GROUP BY puh_cust_id
										                        ) t1
										                        WHERE run_date > NOW()
									                        )
                                    HAVING distance < {$pushData["pu_distance"]}");
    }

    private function getActiveCustomersForSubscriptionInRange($locationSub){
        $range = $locationSub['sum_geo_range'] / 1000;
        return $this->db->getTable("SELECT *,
                                          ( 6371 * acos( cos( radians({$locationSub['sum_geo_long']}) ) * cos( radians( cust_longt ) )* cos( radians( cust_lati ) - radians({$locationSub['sum_geo_lat']}) ) + sin( radians({$locationSub['sum_geo_long']}) ) * sin(radians(cust_longt)) ) ) AS distance
                                    FROM tbl_customers,tbl_customer_subscription
                                    WHERE csu_sub_id = {$locationSub['md_row_id']}
                                    AND cust_id = csu_cust_id
                                    AND csu_status = 'active'
                                    AND cust_location_updated IS NOT NULL
                                    AND cust_location_updated > DATE_SUB(NOW(), INTERVAL 1 HOUR)
                                    AND cust_id NOT IN (SELECT puh_cust_id FROM (
											                        SELECT puh_cust_id ,DATE_ADD(MAX(puh_sent), INTERVAL 1 DAY) run_date
											                        FROM tbl_push_history 
											                        WHERE puh_type = 'Subscription'
											                        AND puh_cust_id > 0
											                        AND puh_external_id = {$locationSub['md_row_id']}
											                        GROUP BY puh_cust_id
										                        ) t1
										                        WHERE run_date > NOW()
                                    )
                                    HAVING distance < {$range}");

    }

    private function getActiveCustomersForPunchPassInRange($locationppass){
        $range = $locationppass['pum_geo_range'] / 1000;
        return $this->db->getTable("SELECT *,
                                          ( 6371 * acos( cos( radians({$locationppass['pum_geo_long']}) ) * cos( radians( cust_longt ) )* cos( radians( cust_lati ) - radians({$locationppass['pum_geo_lat']}) ) + sin( radians({$locationppass['pum_geo_long']}) ) * sin(radians(cust_longt)) ) ) AS distance
                                    FROM tbl_customers,tbl_customer_subscription
                                    WHERE csu_sub_id = {$locationppass['md_row_id']}
                                    AND cust_id = csu_cust_id
                                    AND csu_status = 'active'
                                    AND cust_location_updated IS NOT NULL
                                    AND cust_location_updated > DATE_SUB(NOW(), INTERVAL 1 HOUR)
                                     AND cust_id NOT IN (SELECT puh_cust_id FROM (
											                        SELECT puh_cust_id ,DATE_ADD(MAX(puh_sent), INTERVAL 1 DAY) run_date
											                        FROM tbl_push_history 
											                        WHERE puh_type = 'Punch pass'
											                        AND puh_cust_id > 0
											                        AND puh_external_id = {$locationppass['md_row_id']}
											                        GROUP BY puh_cust_id
										                        ) t1
										                        WHERE run_date > NOW()
                                    )
                                    HAVING distance < {$range}");
    }

    private function getDistinctCustomersListForGroupsList($groupsList,$bizId){

        $custCollection = array();
        $duplicatesArray = array();

        foreach ($groupsList as $group) {

            $customersList = $this->db->getTable("SELECT * FROM tbl_customers,tbl_group_cust 
                                    WHERE gc_cust_id = cust_id
                                    AND gc_group_id = $group
                                    AND cust_mobile_serial <> ''");

            if(count($customersList) > 0){                
                foreach ($customersList as $customer){
                    if (!in_array($customer["cust_id"], $duplicatesArray)) {
                        array_push($custCollection,$customer);
                        array_push($duplicatesArray,$customer["cust_id"]);
                    }
                }
            }
        }

        return $custCollection;
    }

    private function getClubMembersForBiz($bizID){
        return $this->db->getTable("SELECT * FROM tbl_customers
                                    WHERE cust_biz_id = $bizID
                                    AND cust_appid IN ($bizID,0)
                                    AND cust_status = 'member'");
    }

    private function getGuestsForBiz($bizID){
        return $this->db->getTable("SELECT * FROM tbl_customers
                                    WHERE cust_biz_id = $bizID
                                    AND cust_appid IN ($bizID,0)
                                    AND cust_status = 'guest'");
    }

    private function getAllCustomersForBiz($bizID){
        return $this->db->getTable("SELECT * FROM tbl_customers WHERE cust_biz_id = $bizID AND cust_appid IN ($bizID,0) AND cust_status != 'invited'");
    }

    private function getCampaignMatchingCustomersByBirthDate($campaignID){

        $campaignRow = $this->getPushRow($campaignID);

        return $this->db->getTable("SELECT cust_id  
                                                FROM tbl_customers LEFT JOIN tbl_push_history ON cust_id = puh_cust_id AND puh_push_id=$campaignID AND year(puh_sent) = year(NOW())
                                                WHERE cust_biz_id={$campaignRow["pu_biz_id"]} 
                                                AND cust_status = 'member' 
                                                AND cust_birth_date is not null 
                                                AND DATE_FORMAT(cust_birth_date,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d') 
                                                AND time('{$campaignRow["pu_event_send_time"]}') <= time(now())
                                                AND puh_id is null");
    }

    private function getCampaignMatchingCustomersByBirthDateInGroups($groupsList,$campaignID){

        $campaignRow = $this->getPushRow($campaignID);

        $custCollection = array();
        $duplicatesArray = array();

        foreach ($groupsList as $group) {

            $customersList = $this->db->getTable("SELECT cust_id  
                                                FROM tbl_group_cust,tbl_customers LEFT JOIN tbl_push_history ON cust_id = puh_cust_id AND puh_push_id=$campaignID AND year(puh_sent) = year(NOW())
                                                WHERE gc_cust_id=cust_id
                                                AND gc_group_id = $group
                                                AND cust_biz_id={$campaignRow["pu_biz_id"]} 
                                                AND cust_status = 'member' 
                                                AND cust_birth_date is not null 
                                                AND DATE_FORMAT(cust_birth_date,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d') 
                                                AND time('{$campaignRow["pu_event_send_time"]}') <= time(now())
                                                AND puh_id is null");

            if(count($customersList) > 0){                
                foreach ($customersList as $customer){
                    if (!in_array($customer["cust_id"], $duplicatesArray)) {
                        array_push($custCollection,$customer);
                        array_push($duplicatesArray,$customer["cust_id"]);
                    }
                }
            }
        }

        return $custCollection;
    }

    private function getCampaignMatchingCustomersByWeddingDate($campaignID){

        $campaignRow = $this->getPushRow($campaignID);

        return $this->db->getTable("SELECT cust_id  
                                                FROM tbl_customers LEFT JOIN tbl_push_history ON cust_id = puh_cust_id AND puh_push_id=$campaignID AND year(puh_sent) = year(NOW())
                                                WHERE cust_biz_id={$campaignRow["pu_biz_id"]} 
                                                AND cust_status = 'member' 
                                                AND cust_weding_date is not null 
                                                AND DATE_FORMAT(cust_weding_date,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d') 
                                                AND time('{$campaignRow["pu_event_send_time"]}') <= time(now())
                                                AND puh_id is null");
    }

    private function getCampaignMatchingCustomersByWeddingDateInGroups($groupsList,$campaignID){

        $campaignRow = $this->getPushRow($campaignID);

        $custCollection = array();
        $duplicatesArray = array();

        foreach ($groupsList as $group) {

            $customersList = $this->db->getTable("SELECT cust_id  
                                                FROM tbl_group_cust,tbl_customers LEFT JOIN tbl_push_history ON cust_id = puh_cust_id AND puh_push_id=$campaignID AND year(puh_sent) = year(NOW())
                                                WHERE gc_cust_id=cust_id
                                                AND gc_group_id = $group
                                                AND cust_biz_id={$campaignRow["pu_biz_id"]} 
                                                AND cust_status = 'member' 
                                                AND cust_weding_date is not null 
                                                AND DATE_FORMAT(cust_weding_date,'%m-%d') = DATE_FORMAT(NOW(),'%m-%d') 
                                                AND time('{$campaignRow["pu_event_send_time"]}') <= time(now())
                                                AND puh_id is null");

            if(count($customersList) > 0){                
                foreach ($customersList as $customer){
                    if (!in_array($customer["cust_id"], $duplicatesArray)) {
                        array_push($custCollection,$customer);
                        array_push($duplicatesArray,$customer["cust_id"]);
                    }
                }
            }
        }

        return $custCollection;

    } 


    /* **************************** */
    /*       apiVX FUNCTIONS        */
    /* **************************** */



    /***********************************************************/
    /*   BASIC CUSTOMERPUSHCAMPAIGNACTIVITY - PUBLIC           */
    /***********************************************************/

    /**
     * Insert new customerPushCampaignActivityObject to DB
     * Return Data = new customerPushCampaignActivityObject ID
     * @param customerPushCampaignActivityObject $customerPushCampaignActivityObj 
     * @return resultObject
     */
    public function addCustomerPushCampaignActivity(customerPushCampaignActivityObject $customerPushCampaignActivityObj){       
        try{
            $newId = $this->addCustomerPushCampaignActivityDB($customerPushCampaignActivityObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPushCampaignActivityObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerPushCampaignActivity from DB for provided ID
     * * Return Data = customerPushCampaignActivityObject
     * @param int $customerPushCampaignActivityId 
     * @return resultObject
     */
    public function getCustomerPushCampaignActivityByID($customerPushCampaignActivityId){
        
        try {
            $customerPushCampaignActivityData = $this->loadCustomerPushCampaignActivityFromDB($customerPushCampaignActivityId);
            
            $customerPushCampaignActivityObj = customerPushCampaignActivityObject::withData($customerPushCampaignActivityData);
            $result = resultObject::withData(1,'',$customerPushCampaignActivityObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPushCampaignActivityId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerPushCampaignActivity in DB
     * @param customerPushCampaignActivityObject $customerPushCampaignActivityObj 
     * @return resultObject
     */
    public function updateCustomerPushCampaignActivity(customerPushCampaignActivityObject $customerPushCampaignActivityObj){        
        try{
            $this->upateCustomerPushCampaignActivityDB($customerPushCampaignActivityObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPushCampaignActivityObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerPushCampaignActivity from DB
     * @param int $customerPushCampaignActivityID 
     * @return resultObject
     */
    public function deleteCustomerPushCampaignActivityById($customerPushCampaignActivityID){
        try{
            $this->deleteCustomerPushCampaignActivityByIdDB($customerPushCampaignActivityID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPushCampaignActivityID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /***************************************************************/
    /*   BASIC CUSTOMERPUSHCAMPAIGNACTIVITY - DB METHODS           */
    /**************************************************************/

    private function addCustomerPushCampaignActivityDB(customerPushCampaignActivityObject $obj){

        if (!isset($obj)){
            throw new Exception("customerPushCampaignActivityObject value must be provided");             
        }

        $cup_open_timeDate = isset($obj->cup_open_time) ? "'".$obj->cup_open_time."'" : "null";

        $cup_delivery_timeDate = isset($obj->cup_delivery_time) ? "'".$obj->cup_delivery_time."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_cust_push SET 
                                    cup_customer_id = {$obj->cup_customer_id},
                                    cup_cust_id = '".addslashes($obj->cup_cust_id)."',
                                    cup_push_id = {$obj->cup_push_id},
                                    cup_open_time = $cup_open_timeDate,
                                    cup_delivery_time = $cup_delivery_timeDate,
                                    cup_device_type = '{$obj->cup_device_type}'
                                             ");
        return $newId;
    }

    private function loadCustomerPushCampaignActivityFromDB($customerPushCampaignActivityID){

        if (!is_numeric($customerPushCampaignActivityID) || $customerPushCampaignActivityID <= 0){
            throw new Exception("Illegal value customerPushCampaignActivityID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_push WHERE cup_id = $customerPushCampaignActivityID");
    }

    private function upateCustomerPushCampaignActivityDB(customerPushCampaignActivityObject $obj){

        if (!isset($obj->cup_id) || !is_numeric($obj->cup_id) || $obj->cup_id <= 0){
            throw new Exception("customerPushCampaignActivityObject value must be provided");             
        }

        $cup_create_timeDate = isset($obj->cup_create_time) ? "'".$obj->cup_create_time."'" : "null";

        $cup_open_timeDate = isset($obj->cup_open_time) ? "'".$obj->cup_open_time."'" : "null";

        $cup_delivery_timeDate = isset($obj->cup_delivery_time) ? "'".$obj->cup_delivery_time."'" : "null";

        $this->db->execute("UPDATE tbl_cust_push SET 
                            cup_customer_id = {$obj->cup_customer_id},
                            cup_cust_id = '".addslashes($obj->cup_cust_id)."',
                            cup_push_id = {$obj->cup_push_id},
                            cup_create_time = $cup_create_timeDate,
                            cup_open_time = $cup_open_timeDate,
                            cup_delivery_time = $cup_delivery_timeDate,
                            cup_device_type = '{$obj->cup_device_type}'
                            WHERE cup_id = {$obj->cup_id} 
                                     ");
    }

    private function deleteCustomerPushCampaignActivityByIdDB($customerPushCampaignActivityID){

        if (!is_numeric($customerPushCampaignActivityID) || $customerPushCampaignActivityID <= 0){
            throw new Exception("Illegal value customerPushCampaignActivityID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_push WHERE cup_id = $customerPushCampaignActivityID");
    }

    private function loadCustomerPushData_DB($pushId,$custID){

        if (!is_numeric($custID) || $custID <= 0){
            throw new Exception("Illegal value customerPushCampaignActivityID");             
        }

        return $this->db->getRow("SELECT cup_open_time FROM tbl_cust_push 
                                    WHERE cup_customer_id=$custID
                                    AND cup_push_id=$pushId");
    }

    /************************************* */
    /*   BASIC PUSH - PUBLIC           */
    /************************************* */

    /**
     * Insert new pushObject to DB
     * Return Data = new pushObject ID
     * @param pushObject $pushObj 
     * @return resultObject
     */
    public function addPush(pushObject $pushObj){       
        try{
            $newId = $this->addPushDB($pushObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($pushObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get push from DB for provided ID
     * * Return Data = pushObject
     * @param int $pushId 
     * @return resultObject
     */
    public function getPushByID($pushId){
        
        try {
            $pushData = $this->loadPushFromDB($pushId);
            
            $pushObj = pushObject::withData($pushData);
            $result = resultObject::withData(1,'',$pushObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$pushId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param pushObject $pushObj 
     * @return resultObject
     */
    public function updatePush(pushObject $pushObj){        
        try{
            $this->upatePushDB($pushObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($pushObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete push from DB
     * @param int $pushID 
     * @return resultObject
     */
    public function deletePushById($pushID){
        try{
            $this->deletePushByIdDB($pushID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$pushID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC PUSH - DB METHODS           */
    /************************************* */

    private function addPushDB(pushObject $obj){

        if (!isset($obj)){
            throw new Exception("pushObject value must be provided");             
        }

        $pu_sentDate = isset($obj->pu_sent) ? "'".$obj->pu_sent."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_push SET 
                                        pu_biz_id = {$obj->pu_biz_id},
                                        pu_status = '".addslashes($obj->pu_status)."',
                                        pu_short_mess = '".addslashes($obj->pu_short_mess)."',
                                        pu_sent = $pu_sentDate,
                                        pu_num_sent = {$obj->pu_num_sent},
                                        pu_num_recived = {$obj->pu_num_recived},
                                        pu_html = '".addslashes($obj->pu_html)."',
                                        pu_header = '".addslashes($obj->pu_header)."',
                                        pu_img = '".addslashes($obj->pu_img)."',
                                        pu_target = '{$obj->pu_target}',
                                        pu_groups = '".addslashes($obj->pu_groups)."',
                                        pu_send_on = '{$obj->pu_send_on}',
                                        pu_in_server = {$obj->pu_in_server},
                                        pu_isnew = {$obj->pu_isnew},
                                        pu_based_location = {$obj->pu_based_location},
                                        pu_active = {$obj->pu_active},
                                        pu_longi = '".addslashes($obj->pu_longi)."',
                                        pu_lati = '".addslashes($obj->pu_lati)."',
                                        pu_distance = {$obj->pu_distance},
                                        pu_type = {$obj->pu_type},
                                        pu_coupon_id = {$obj->pu_coupon_id},
                                        pu_product_id = {$obj->pu_product_id},
                                        pu_loyalty_id = {$obj->pu_loyalty_id},
                                        pu_scard_id = {$obj->pu_scard_id},
                                        pu_event_type = {$obj->pu_event_type},
                                        pu_event_send_time = '{$obj->pu_event_send_time}',
                                        pu_event_struct = {$obj->pu_event_struct},
                                        pu_event_greeting = '".addslashes($obj->pu_event_greeting)."',
                                        pu_event_main_text = '".addslashes($obj->pu_event_main_text)."',
                                        pu_event_signature = '".addslashes($obj->pu_event_signature)."',
                                        pu_external_id = {$obj->pu_external_id},
                                        pu_level = {$obj->pu_level},
                                        pu_mod_id = {$obj->pu_mod_id},
                                        pu_view = '".addslashes($obj->pu_view)."',
                                        pu_view_android = '".addslashes($obj->pu_view_android)."',
                                        pu_async_sent = {$obj->pu_async_sent}
         ");
        return $newId;
    }

    private function loadPushFromDB($pushID){

        if (!is_numeric($pushID) || $pushID <= 0){
            throw new Exception("Illegal value $pushID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_push WHERE pu_id = $pushID");
    }

    private function upatePushDB(pushObject $obj){

        if (!isset($obj->pu_id) || !is_numeric($obj->pu_id) || $obj->pu_id <= 0){
            throw new Exception("pushObject value must be provided");             
        }

        $pu_createdDate = isset($obj->pu_created) ? "'".$obj->pu_created."'" : "null";

        $pu_sentDate = isset($obj->pu_sent) ? "'".$obj->pu_sent."'" : "null";

        $this->db->execute("UPDATE tbl_push SET 
                                pu_biz_id = {$obj->pu_biz_id},
                                pu_created = $pu_createdDate,
                                pu_status = '".addslashes($obj->pu_status)."',
                                pu_short_mess = '".addslashes($obj->pu_short_mess)."',
                                pu_sent = $pu_sentDate,
                                pu_num_sent = {$obj->pu_num_sent},
                                pu_num_recived = {$obj->pu_num_recived},
                                pu_html = '".addslashes($obj->pu_html)."',
                                pu_header = '".addslashes($obj->pu_header)."',
                                pu_img = '".addslashes($obj->pu_img)."',
                                pu_target = '{$obj->pu_target}',
                                pu_groups = '".addslashes($obj->pu_groups)."',
                                pu_send_on = '{$obj->pu_send_on}',
                                pu_in_server = {$obj->pu_in_server},
                                pu_isnew = {$obj->pu_isnew},
                                pu_based_location = {$obj->pu_based_location},
                                pu_active = {$obj->pu_active},
                                pu_longi = '".addslashes($obj->pu_longi)."',
                                pu_lati = '".addslashes($obj->pu_lati)."',
                                pu_distance = {$obj->pu_distance},
                                pu_type = {$obj->pu_type},
                                pu_coupon_id = {$obj->pu_coupon_id},
                                pu_product_id = {$obj->pu_product_id},
                                pu_loyalty_id = {$obj->pu_loyalty_id},
                                pu_scard_id = {$obj->pu_scard_id},
                                pu_event_type = {$obj->pu_event_type},
                                pu_event_send_time = '{$obj->pu_event_send_time}',
                                pu_event_struct = {$obj->pu_event_struct},
                                pu_event_greeting = '".addslashes($obj->pu_event_greeting)."',
                                pu_event_main_text = '".addslashes($obj->pu_event_main_text)."',
                                pu_event_signature = '".addslashes($obj->pu_event_signature)."',
                                pu_external_id = {$obj->pu_external_id},
                                pu_level = {$obj->pu_level},
                                pu_mod_id = {$obj->pu_mod_id},
                                pu_view = '".addslashes($obj->pu_view)."',
                                pu_view_android = '".addslashes($obj->pu_view_android)."',
                                pu_async_sent = {$obj->pu_async_sent}
                                WHERE pu_id = {$obj->pu_id} 
         ");
    }

    private function deletePushByIdDB($pushID){

        if (!is_numeric($pushID) || $pushID <= 0){
            throw new Exception("Illegal value $pushID");             
        }

        $this->db->execute("DELETE FROM tbl_push WHERE pu_id = $pushID");
    }

    /************************************* */
    /*   BASIC PUSHEVENTSTRUCTURE - PUBLIC           */
    /************************************* */

    /**
     * Insert new pushEventStructureObject to DB
     * Return Data = new pushEventStructureObject ID
     * @param pushEventStructureObject $pushEventStructureObj 
     * @return resultObject
     */
    public function addPushEventStructure(pushEventStructureObject $pushEventStructureObj){       
        try{
            $newId = $this->addPushEventStructureDB($pushEventStructureObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($pushEventStructureObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get pushEventStructure from DB for provided ID
     * * Return Data = pushEventStructureObject
     * @param int $pushEventStructureId 
     * @return resultObject
     */
    public function getPushEventStructureByID($pushEventStructureId){
        
        try {
            $pushEventStructureData = $this->loadPushEventStructureFromDB($pushEventStructureId);
            
            $pushEventStructureObj = pushEventStructureObject::withData($pushEventStructureData);
            $result = resultObject::withData(1,'',$pushEventStructureObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$pushEventStructureId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param pushEventStructureObject $pushEventStructureObj 
     * @return resultObject
     */
    public function updatePushEventStructure(pushEventStructureObject $pushEventStructureObj){        
        try{
            $this->upatePushEventStructureDB($pushEventStructureObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($pushEventStructureObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete pushEventStructure from DB
     * @param int $pushEventStructureID 
     * @return resultObject
     */
    public function deletePushEventStructureById($pushEventStructureID){
        try{
            $this->deletePushEventStructureByIdDB($pushEventStructureID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$pushEventStructureID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC PUSHEVENTSTRUCTURE - DB METHODS           */
    /************************************* */

    private function addPushEventStructureDB(pushEventStructureObject $obj){

        if (!isset($obj)){
            throw new Exception("pushEventStructureObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_push_event_struct SET 
                                        pbg_event_id = {$obj->pbg_event_id},
                                        pbg_name = '".addslashes($obj->pbg_name)."',
                                        pbg_img = '".addslashes($obj->pbg_img)."',
                                        pbg_img_blank = '".addslashes($obj->pbg_img_blank)."',
                                        pbg_font_1 = '".addslashes($obj->pbg_font_1)."',
                                        pbg_font_2 = '".addslashes($obj->pbg_font_2)."',
                                        pbg_font_3 = '".addslashes($obj->pbg_font_3)."',
                                        pbg_font_1_ios = '".addslashes($obj->pbg_font_1_ios)."',
                                        pbg_font_2_ios = '".addslashes($obj->pbg_font_2_ios)."',
                                        pbg_font_3_ios = '".addslashes($obj->pbg_font_3_ios)."',
                                        pbg_color_1 = '".addslashes($obj->pbg_color_1)."',
                                        pbg_color_2 = '".addslashes($obj->pbg_color_2)."',
                                        pbg_color_3 = '".addslashes($obj->pbg_color_3)."',
                                        pbg_view = '".addslashes($obj->pbg_view)."',
                                        pbg_view_android = '".addslashes($obj->pbg_view_android)."',
                                        pbg_font_name_1 = '".addslashes($obj->pbg_font_name_1)."',
                                        pbg_font_name_2 = '".addslashes($obj->pbg_font_name_2)."',
                                        pbg_font_name_3 = '".addslashes($obj->pbg_font_name_3)."'
         ");
        return $newId;
    }

    private function loadPushEventStructureFromDB($pushEventStructureID){

        if (!is_numeric($pushEventStructureID) || $pushEventStructureID <= 0){
            throw new Exception("Illegal value $pushEventStructureID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_push_event_struct WHERE pbg_id = $pushEventStructureID");
    }

    private function upatePushEventStructureDB(pushEventStructureObject $obj){

        if (!isset($obj->pbg_id) || !is_numeric($obj->pbg_id) || $obj->pbg_id <= 0){
            throw new Exception("pushEventStructureObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_push_event_struct SET 
                                pbg_event_id = {$obj->pbg_event_id},
                                pbg_name = '".addslashes($obj->pbg_name)."',
                                pbg_img = '".addslashes($obj->pbg_img)."',
                                pbg_img_blank = '".addslashes($obj->pbg_img_blank)."',
                                pbg_font_1 = '".addslashes($obj->pbg_font_1)."',
                                pbg_font_2 = '".addslashes($obj->pbg_font_2)."',
                                pbg_font_3 = '".addslashes($obj->pbg_font_3)."',
                                pbg_font_1_ios = '".addslashes($obj->pbg_font_1_ios)."',
                                pbg_font_2_ios = '".addslashes($obj->pbg_font_2_ios)."',
                                pbg_font_3_ios = '".addslashes($obj->pbg_font_3_ios)."',
                                pbg_color_1 = '".addslashes($obj->pbg_color_1)."',
                                pbg_color_2 = '".addslashes($obj->pbg_color_2)."',
                                pbg_color_3 = '".addslashes($obj->pbg_color_3)."',
                                pbg_view = '".addslashes($obj->pbg_view)."',
                                pbg_view_android = '".addslashes($obj->pbg_view_android)."',
                                pbg_font_name_1 = '".addslashes($obj->pbg_font_name_1)."',
                                pbg_font_name_2 = '".addslashes($obj->pbg_font_name_2)."',
                                pbg_font_name_3 = '".addslashes($obj->pbg_font_name_3)."'
                                WHERE pbg_id = {$obj->pbg_id} 
         ");
    }

    private function deletePushEventStructureByIdDB($pushEventStructureID){

        if (!is_numeric($pushEventStructureID) || $pushEventStructureID <= 0){
            throw new Exception("Illegal value $pushEventStructureID");             
        }

        $this->db->execute("DELETE FROM tbl_push_event_struct WHERE pbg_id = $pushEventStructureID");
    }
    /***************************************************************/
    /*                       GENERAL FUNCTIONS                    */
    /**************************************************************/

    public function custPushOpen($pushId,$custID){

        $customerPushData = $this->loadCustomerPushData_DB($pushId,$custID);
        if(is_array($customerPushData)){
            $customerPushCampaignActivityObject = customerPushCampaignActivityObject::withData($customerPushData);

            if($customerPushCampaignActivityObject->cup_open_time == ""){

                $customerPushCampaignActivityObject->cup_open_time = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                $this->updateCustomerPushCampaignActivity($customerPushCampaignActivityObject);

                $pusResult = $this->getPushByID($pushId);
                if($pusResult->code == 1){
                    $pusResult->data->pu_num_recived = $pusResult->data->pu_num_recived + 1;
                    $pusResult->data->pu_num_sent = $pusResult->data->pu_num_sent - 1;
                    $this->updatePush($pusResult->data);
                }
            }
        }
    }

    public function custPushDelivered($pushId,$deviceId){

         $this->db->execute("UPDATE tbl_cust_push 
                                SET cup_delivery_time=now() 
                                WHERE cup_cust_id='{$deviceId}'
                                AND cup_push_id=$pushId");
    }

    public function getGreetingPush($pushId){

        $pusResult = $this->getPushByID($pushId);
        if($pusResult->code == 1){
            $pushRow = $pusResult->data;
            if($pushRow->pu_event_struct != 0){
                $eventResult = $this->getPushEventStructureByID($pushRow->pu_event_struct);
                if($eventResult->code == 1){
                    $eventRow = $eventResult->data;

                    $response["greeting"] = $pushRow->pu_event_greeting;
                    $response["main_text"] = $pushRow->pu_event_main_text;
                    $response["signature"] = $pushRow->pu_event_signature;
                    $response["background"] = $eventRow->pbg_img_blank;
                    $response["font1"] = $eventRow->pbg_font_1;
                    $response["font2"] = $eventRow->pbg_font_2;
                    $response["font3"] = $eventRow->pbg_font_3;
                    $response["font1_ios"] = $eventRow->pbg_font_1_ios;
                    $response["font2_ios"] = $eventRow->pbg_font_2_ios;
                    $response["font3_ios"] = $eventRow->pbg_font_3_ios;
                    $response["view_ios"] = $eventRow->pbg_view;
                    $response["view_android"] = $eventRow->pbg_view_android;
                    $response["color1"] = $eventRow->pbg_color_1;
                    $response["color2"] = $eventRow->pbg_color_2;
                    $response["color3"] = $eventRow->pbg_color_3;
                    $response["view_ios"] = $eventRow->pbg_view;
                    $response["view_android"] = $eventRow->pbg_view_android;

                    return resultObject::withData(1,'ok',$response);
                }else{
                    return $eventResult;
                }
            }else{
                return resultObject::withData(0,'no_greeting_push');
            }
        }else{
            return $pusResult;
        }
    }

    public static function getPushHTML(pushObject $pushRow,mobileRequestObject $requestObj){
        $levelDataManager = new levelDataManager(29);

        $pushData = $levelDataManager->getElementByParentAndElementType($pushRow->pu_id,5);
        $levelDataObject = levelData29Object::withData($pushData);
        $pushImg = $levelDataObject->md_pic;

        $pushData = $levelDataManager->getElementByParentAndElementType($pushRow->pu_id,6);
        $levelDataObject = levelData29Object::withData($pushData);
        $pushText = $levelDataObject->md_info;

        if($pushImg == "" && $pushText == "")
        {
            return resultObject::withData(1,'ok');

        }else{
            if($requestObj->iphone == "0")
            {
                $mainwidth = "620";
            }
            else if($requestObj->iphone == "1")
            {
                $mainwidth = "310";
            }
            else
            {
                if($requestObj->iphone > 620)
                {
                    $mainwidth = "620";
                }
                else
                {
                    $mainwidth = "100%";
                }
            }
            
            $pushImage = "";
            if($pushImg != "")
            {
                $pushImage = '<img id="img1" style="margin:5px; width:100%;" src="'.$pushImg.'" />';
            }
            
            $htmlText = $pushText;
            if($htmlText == "")
            {
                $htmlText = $pushRow->pu_short_mess;
            }
            
            $tmpl_html_out = '
                                <center>
                                <table height="100%" width="##mainwidth##" dir="ltr">
	                                <tr>
		                                <td height="50" valign="midle" align="center">
		                                <div style="color:##font_color##;">##biz_name##</div>
		                                </td>
	                                </tr>
	                                <tr>
		                                <td valign="top">
			                                '.$pushImage.'
			                                <div width="100%" style="left:5px; color:##font_color##;">##biz_odot##</div>
		                                </td>
	                                </tr>
                                </table>
                                </center>
                                ';

            $responseTheme = bizManager::getBizThemeByBizID($requestObj->bizid);
            if($responseTheme->code == 1){
                $tmplRow = $responseTheme->data;

                $tFontColor = $tmplRow->tmpl_color2_freestyle;
                
                $tmpl_html_out = str_replace( '##mainwidth##' , $mainwidth ,$tmpl_html_out);
                $tmpl_html_out = str_replace( '##biz_name##' , $pushRow->pu_header ,$tmpl_html_out);
                $tmpl_html_out = str_replace( '##biz_img1##' , $pushRow->pu_img ,$tmpl_html_out);
                $tmpl_html_out = str_replace( '##biz_odot##' , $htmlText ,$tmpl_html_out);
                $tmpl_html_out = str_replace( '##font_color##' , $tFontColor ,$tmpl_html_out);
                $tmpl_html_out = str_replace( '##bizid##' , $requestObj->bizid ,$tmpl_html_out);
                
                return resultObject::withData(1,'ok',$tmpl_html_out);

            }else{
                return $responseTheme;
            } 
        }
    }

    public static function getModuleLastStructureForPush(pushObject $push){
        $instance = new self();

        return $instance->getModuleLastStructureByBizModId($push->pu_biz_id,0);
    }

    public static function sendBizPushWithParams(pushParamsObject $pushParams){
       
        self::sendPushBiz($pushParams->biz_id,$pushParams->cust_id,$pushParams->push_type,$pushParams->message,$pushParams->extra_params);
    }
}   


?>
