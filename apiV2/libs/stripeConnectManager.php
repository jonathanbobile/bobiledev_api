<?php
/**
 * Methods for stripe connect
 *
 * @version 1.0
 * @author PapTap
 */

require_once(dirname(__FILE__) . '/google-api-client/src/Google/autoload.php');
require_once ROOT_PATH.'libs/Composer/vendor/autoload.php';
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\FieldValue;

class stripeConnectManager {  
    
    protected $db;    
    private $language;

    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }  

        $system_lang = (isset($_SESSION['system_lang'])) ? $_SESSION['system_lang'] : "en";
        $this->language = new languageManager( $system_lang );
        $this->language->load('builder');
    }
    

    public static function getCountrySpecFields($countryCode,$entityType){

        $instance = new self();
        $fieldsStructure = $instance->getKYCFields();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $response = \Stripe\CountrySpec::retrieve($countryCode);
            $verificationFields = $response["verification_fields"][$entityType]["minimum"];
        }
        catch(Exception $e){
            $verificationFields = array();
        } 

        $data = array();
        foreach ($fieldsStructure as $field){
        	
            if($field["kyc_type"] != "label" && $field["kyc_type"] != "space"){
                if(in_array($field["kyc_field_id"],$verificationFields)){
                    $field["visible"] = 1;
                }else{
                    $field["visible"] = 0;
                }
                array_push($data,$field);
            }else{
                $field["visible"] = 1;
                array_push($data,$field);
            }
        }
        
        //remove empty labels
        $i=0;
        foreach ($data as $field){
        	
            if($field["kyc_type"] == "label"){

                $display = false;

                foreach ($data as $innerField){                    
                    if($innerField["kyc_parent"] == $field["kyc_field_id"] && $innerField["visible"] == 1){
                        $display = true;
                    }
                }
                if(!$display){
                    $data[$i]["visible"] = 0;
                }
            }

            $i++;
        }

        //remove empty space
        $i=0;
        foreach ($data as $field){
        	
            if($field["kyc_type"] == "space"){

                $display = false;

                foreach ($data as $innerField){                    
                    if($innerField["kyc_parent"]."_space" == $field["kyc_field_id"] && $innerField["visible"] == 1){
                        $display = true;
                    }
                }
                if(!$display){
                    $data[$i]["visible"] = 0;
                }
            }

            $i++;
        }

        return $data;
    }

    public static function getCountrySpecFieldsFull($countryCode,$entityType){

        $instance = new self();
        $fieldsStructure = $instance->getKYCFields();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $response = \Stripe\CountrySpec::retrieve($countryCode);
            $minimum = $response["verification_fields"][$entityType]["minimum"];
            $additional = $response["verification_fields"][$entityType]["additional"];
        }
        catch(Exception $e){
            $minimum = array();
            $additional = array();
        } 

        $verificationFields = array_merge($minimum,$additional);

        $data = array();
        foreach ($fieldsStructure as $field){
        	
            if($field["kyc_type"] != "label" && $field["kyc_type"] != "space"){
                if(in_array($field["kyc_field_id"],$verificationFields)){
                    $field["visible"] = 1;
                }else{
                    $field["visible"] = 0;
                }
                array_push($data,$field);
            }else{
                $field["visible"] = 1;
                array_push($data,$field);
            }
        }
        
        //remove empty labels
        $i=0;
        foreach ($data as $field){
        	
            if($field["kyc_type"] == "label"){

                $display = false;

                foreach ($data as $innerField){                    
                    if($innerField["kyc_parent"] == $field["kyc_field_id"] && $innerField["visible"] == 1){
                        $display = true;
                    }
                }
                if(!$display){
                    $data[$i]["visible"] = 0;
                }
            }

            $i++;
        }

        //remove empty space
        $i=0;
        foreach ($data as $field){
        	
            if($field["kyc_type"] == "space"){

                $display = false;

                foreach ($data as $innerField){                    
                    if($innerField["kyc_parent"]."_space" == $field["kyc_field_id"] && $innerField["visible"] == 1){
                        $display = true;
                    }
                }
                if(!$display){
                    $data[$i]["visible"] = 0;
                }
            }

            $i++;
        }

        return $data;
    }

    public static function getCountryBankFields($countryCode){

        $instance = new self();
        return $instance->getBankFields($countryCode);
    }
    public static function updateCountryBankFields($fieldsStructure,$sid){

        $instance = new self();
        $instance->updateBankFields($fieldsStructure,$sid);

    }

    public static function getBankSupportCurrency($countryCode){

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $response = \Stripe\CountrySpec::retrieve($countryCode);
        }catch(Exception $e){
            $response = array();
        }

        $supportedCurrency = $response["supported_bank_account_currencies"];
        $currencyData = json_decode(json_encode($supportedCurrency),true);
        $currencyArray = array();
        foreach ($currencyData as $key => $value){

            foreach ($value as $country)
            {
            	if($country == $countryCode){
                    array_push($currencyArray,$key);
                }
            }
        }

        $result["currency"] = $currencyArray;
        $result["default_currency"] = $response["default_currency"];

        return $result;
    }

    public static function getStripeAvailbleCountries(){

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $response = \Stripe\CountrySpec::all(["limit" => 100]);
        }catch(Exception $e){
            $response["data"] = array();
        }
        
        $data = array();
        foreach ($response["data"] as $oneCountry)
        {
            array_push($data,$oneCountry["id"]);
        }
        
        return $data;
    }

    public static function isCountryAvailble($countryCode){

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $response = \Stripe\CountrySpec::all(["limit" => 100]);
        }catch(Exception $e){
            $response["data"] = array();
        }
        
        $exist = false;
        foreach ($response["data"] as $oneCountry)
        {
        	if($oneCountry["id"] == $countryCode){
                $exist = true;
            }
        }
        
        return $exist;
    }

    public static function isCountryAvailbleByCountryId($countryId){

        $countryCode = appManager::getCountryCodeByCountryId($countryId);

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $response = \Stripe\CountrySpec::all(["limit" => 100]);
        }catch(Exception $e){
            $response["data"] = array();
        }
        
        $exist = false;
        foreach ($response["data"] as $oneCountry)
        {
        	if($oneCountry["id"] == $countryCode){
                $exist = true;
            }
        }
        
        return $exist;
    }

    public static function createConnectAccountForBiz($data){
        $instance = new self();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        $fields = $instance->generateStripeConnectAccountFields($data);

        $response["code"] = 1;
        $response["message"] = "";
        try {
            $acc = \Stripe\Account::create($fields);
            $response["message"] = $acc["id"];
        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function updateConnectAccount($data,$stripeAccountId){
 
        $instance = new self();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $response["code"] = 1;
        $response["message"] = "";
        try {

            $account = $instance->getConnectAccount($stripeAccountId);
            $fields = $instance->generateStripeConnectAccountFields($data,$account->legal_entity->verification->status == "verified");

            if($fields["business_name"] != ""){
                $account->business_name = $fields["business_name"];
            }
            $account->debit_negative_balances = $fields["debit_negative_balances"];
            $account->legal_entity = $fields["legal_entity"];
            if(isset($fields["external_account"])){
                $account->external_account = $fields["external_account"];
            }
            $account->tos_acceptance = $fields["tos_acceptance"];
            $account->payout_schedule = $fields["payout_schedule"];
            $account->save();

            $response["message"] = "";
        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function addConnectBankAccount($data,$stripeAccountId){
        
        $instance = new self();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $fields = $instance->generateStripeBankAccountFields($data);
        
        $response["code"] = 1;
        $response["message"] = "";
        try {

            if(isset($fields["external_account"])){

                $account = $instance->getConnectAccount($stripeAccountId);
                $account->external_accounts->create(["external_account" => $fields["external_account"]]);
            }

            $response["message"] = "";
        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function updateKYCFields($data,$entityId,$collection = "builder"){
        $fbase = new firebase();
        $instance = new self();
        $bizDocumentData = $fbase->getDocument($collection,$entityId);

        $fields = array();

        if(count($bizDocumentData["data"]["kyc_fields"]) == 0){
            
            foreach ($data as $row)
            {
                if($row["visible"] == 1){
                    $field["index"] = (int)$row["kyc_index"];
                    $field["type"] = $row["kyc_type"];
                    $field["field_id"] = $row["kyc_field_id"];
                    $field["label"] = $instance->language->get($row["kyc_label_key"]);
                    $field["value"] = "";
                    $field["visible"] = $row["visible"];
                    $field["required"] = $row["kyc_required"];
                    $field["place_holder"] = $row["kyc_place_holder"];

                    if($row["kyc_type"] == "date"){
                        $field["dayValue"] = "";
                        $field["monthValue"] = "";
                        $field["yearValue"] = "";
                    }
                    if($row["kyc_type"] == "array"){
                        $field["value"] = [];
                    }
                    array_push($fields,$field);
                }
            }

        }else{
            $fields = $bizDocumentData["data"]["kyc_fields"];

            $existingIds = array();
            foreach ($bizDocumentData["data"]["kyc_fields"] as $existingField)
            {
                $existingIds[] = $existingField["field_id"];
            }

            foreach ($data as $row)
            {
                if($row["visible"] == 1 && !in_array($row["kyc_field_id"],$existingIds)){
                    $field["index"] = (int)$row["kyc_index"];
                    $field["type"] = $row["kyc_type"];
                    $field["field_id"] = $row["kyc_field_id"];
                    $field["label"] = $instance->language->get($row["kyc_label_key"]);
                    $field["value"] = "";
                    $field["visible"] = $row["visible"];
                    $field["required"] = $row["kyc_required"];
                    $field["place_holder"] = $row["kyc_place_holder"];

                    if($row["kyc_type"] == "date"){
                        $field["dayValue"] = "";
                        $field["monthValue"] = "";
                        $field["yearValue"] = "";
                    }
                    if($row["kyc_type"] == "array"){
                        $field["value"] = [];
                    }
                    array_push($fields,$field);
                }
            }
        }

        $newData = [
                       ['path' => 'kyc_fields', 'value' => $fields]
                   ];

        $fbase->updateBaseRecord($newData,$entityId,$collection);
    }

    public static function getConnectAccount($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        try{
            return \Stripe\Account::retrieve($stripeAccountId);
        }
        catch(Exception $e){
            return array();
        }
    }
    public static function getConnectAccountSandbox($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
        try{
            return \Stripe\Account::retrieve($stripeAccountId);
        }
        catch(Exception $e){
            return array();
        }
    }
    public static function getBalance($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        try{
            return \Stripe\Balance::retrieve(["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            return array();
        }
    }

    public static function getTotalBalance($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        try{
            $balance = \Stripe\Balance::retrieve(["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            $balance["available"] = array();
            $balance["pending"] = array();
        }

        $total=0;

        if(count($balance["available"]) > 0){
            foreach ($balance["available"] as $availble)
            {
                $total = $total + $availble["amount"];
            }
        }

        if(count($balance["pending"]) > 0){
            foreach ($balance["pending"] as $pending)
            {
                $total = $total + $pending["amount"];
            }
        }

        return $total / 100;
    }

    public static function getPayouts($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $data = array();
        try{
            return \Stripe\Payout::all($data, ["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            return array();
        }
    }

    public static function getTotalPayouts($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $total = 0;

        $data = array();
        try{
            $payouts = \Stripe\Payout::all($data, ["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            $payouts["data"] = array();
        }

        if(count($payouts["data"])>0){
            foreach ($payouts["data"] as $payout)
            {
            	$total = $total + $payout["amount"];
            }
        }

        return $total / 100;
    }

    public static function getBalanceTransactions($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $data = array();
        try{
            return \Stripe\BalanceTransaction::all($data, ["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            return array();
        }
    }

    public static function getTotals($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $data = array();
        try{
            $transactions = \Stripe\BalanceTransaction::all($data, ["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            $transactions["data"] = array();
        }

        $totalBalance = 0;
        $totalNet = 0;

        if(count($transactions["data"]) > 0){
            foreach ($transactions->autoPagingIterator() as $transaction) {
                switch ($transaction["type"])
                {
                    case "charge":
                        $totalBalance = $totalBalance + $transaction["amount"];
                        $totalNet = $totalNet + $transaction["net"];
                        break;
                }
                
            }
        }

        $response["net"] = $totalNet / 100;
        $response["balance"] = $totalBalance / 100;

        return $response;
    }

    public static function setConnectBankAccountDefault($bankId,$stripeAccountId){
        
        $instance = new self();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $response["code"] = 1;
        $response["message"] = "";
        try {

            $account = $instance->getConnectAccount($stripeAccountId);
            $bank_account = $account->external_accounts->retrieve($bankId);
            $bank_account->default_for_currency = true;
            $bank_account->save();

        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function deleeConnectBankAccount($bankId,$stripeAccountId){
        
        $instance = new self();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $response["code"] = 1;
        $response["message"] = "";
        try {

            $account = $instance->getConnectAccount($stripeAccountId);
            $account->external_accounts->retrieve($bankId)->delete();
        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function setPayoutsSchedule($stripeAccountId,$data){
        
        $instance = new self();

        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));

        $payout_schedule = array();
        $payout_schedule["interval"] = $data["interval"];
        $payout_schedule["delay_days"] = "minimum";  
        
        switch ($data["interval"])
        {
            case "weekly":
                $payout_schedule["weekly_anchor"] = $data["daysOfWeek"];
                break;
            case "monthly":
                $payout_schedule["monthly_anchor"] = $data["days"];
                break;
        	default:
        }
        
        

        $response["code"] = 1;
        $response["message"] = "";
        try {

            $account = $instance->getConnectAccount($stripeAccountId);
            $account->payout_schedule = $payout_schedule;
            $account->save();

            $response["message"] = "";
        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function uploadFile($stripeAccountId,$data){
        
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        
        $response["code"] = 1;
        $response["message"] = "";
        try {

            $fp = fopen($data["path"], 'r');
            $resp = \Stripe\File::create([
                'file' => $fp,
                'purpose' => $data["id"],
            ],["stripe_account" => $stripeAccountId]);

            $response["file_id"] = $resp["id"];
            $response["message"] = "";
        }
        catch (\Stripe\Error\Base $e) {
            $response["code"] = -1;
            $response["message"] =$e->getMessage();
        }
        catch (Exception $e) {
            $response["code"] = -2;
            $response["message"] =$e->getMessage();
        }

        return $response;
    }

    public static function getInvoices($stripeAccountId){
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        try{
            return \Stripe\Invoice::all(["limit" => 100],["stripe_account" => $stripeAccountId]);
        }
        catch(Exception $e){
            return array();
        }
    }

    public static function getDefaulBankAccountCurrency($accounts){
        
        $defaultAccountCurrency = "";
        if(count($accounts) > 0){
            foreach ($accounts as $bankAccount)
            {
                if($bankAccount["default_for_currency"]){
                    $defaultAccountCurrency = strtoupper($bankAccount["currency"]);
                }
            }
        }
        return $defaultAccountCurrency;
    }

    public static function getCurrencyListForAccount($accounts){
        
        $currencyList = array();

        if(count($accounts) > 0){
            foreach ($accounts as $bankAccount)
            {
                $currencyList[] = "'".strtoupper($bankAccount["currency"])."'";
            }
        }
        return implode(",",$currencyList);
    }


    //*********** private functions ****************

    private static function getKYCFields(){
        $instance = new self();
        
        $data = $instance->db->getTable("SELECT * FROM tbl_kyc_fields
                                        ORDER BY kyc_index, kyc_id");
        return $data;
    }

    private static function getBankFields($countryCode){
        $instance = new self();
        
        $data = $instance->db->getTable("SELECT *, 1 as visible FROM tbl_banks_fields
                                        WHERE kyc_country = '$countryCode'
                                        ORDER BY kyc_index");
        return $data;
    }

    private static function updateBankFields($data,$sid){
        $fbase = new firebase();
        $instance = new self();
        $bizDocumentData = $fbase->getDocument("builder",$sid);

        if(count($bizDocumentData["data"]["bank_fields"]) == 0){
            $fields = array();
            foreach ($data as $row)
            {
                $field["index"] = (int)$row["kyc_index"];
                $field["type"] = $row["kyc_type"];
                $field["field_id"] = $row["kyc_field_id"];
                $field["label"] = $instance->language->get($row["kyc_label_key"]);
                $field["value"] = "";
                $field["visible"] = $row["visible"];
                $field["required"] = $row["kyc_required"];
                $field["place_holder"] = $row["kyc_place_holder"];

                array_push($fields,$field);
            }

            $newData = [
                          ['path' => 'bank_fields', 'value' => $fields]
                       ];

            $fbase->updateBaseRecord($newData,$sid,'builder');
        }

    }

    private static function generateStripeConnectAccountFields($data,$accountActive=false){
        $legalArray = array();
        $legalArray["type"] = $data["biz_entity"];

        foreach ($data["kyc_fields"] as $field)
        {
            if($accountActive && ($field["field_id"] == "legal_entity.business_name" || $field["type"] == "file")){
                continue;
            }
        	if($field["type"] != "label" && $field["type"] != "space" && $field["type"] != "date" && $field["type"] != "array"){
                
                $structureArray = explode(".",$field["field_id"]);
                for($i=0; $i<count($structureArray); $i++){
                    if($structureArray[$i] != "legal_entity" && trim($field["value"]) != ""){
                        if($i == (count($structureArray) - 1)){
                            if($i == 2){
                                $legalArray[$structureArray[$i-1]][$structureArray[$i]] = $field["value"];
                            }else{
                                $legalArray[$structureArray[$i]] = $field["value"];
                            }
                        }
                    }
                }
            }
            if($field["type"] == "date"){
                $structureArray = explode(".",$field["field_id"]);
                $dateArray = array();
                $dateArray["day"] = $field["dayValue"];
                $dateArray["month"] = $field["monthValue"];
                $dateArray["year"] = $field["yearValue"];
                if($dateArray["day"] != 0){
                    $legalArray[$structureArray[1]] = $dateArray;
                }
            }

            if($field["type"] == "array"){
                $structureArray = explode(".",$field["field_id"]);
                if(!isset($field["value"]) || $field["value"] == "" || count($field["value"]) == 0){
                    $legalArray[$structureArray[1]] = null;
                }
            }
        }

        $bankArray = array();

        if(isset($data["bank_fields"]) && count($data["bank_fields"])>0){
            
            $workingCurrency = "";
            
            foreach ($data["bank_fields"] as $field){
                if($field["field_id"] == "currency"){
                    $workingCurrency = $field["value"];
                }
            }

            if($workingCurrency != ""){

                $bankArray["object"] = "bank_account";
                $bankArray["country"] = $data["country_code"];

                if($data["country_code"] == "GB"){

                    //find the currency
                    foreach ($data["bank_fields"] as $field){
                        if($field["field_id"] == "currency"){
                            $workingCurrency = $field["value"];
                            $bankArray[$field["field_id"]] = $field["value"];
                        }
                    }

                    foreach ($data["bank_fields"] as $field){
                        if($field["field_id"] != "currency"){
                            $structureArray = explode("-",$field["field_id"]);
                            if($workingCurrency == "eur"){
                                if($structureArray[0] == "account_number" && $structureArray[1] == "2"){
                                    $bankArray[$structureArray[0]] = $field["value"];
                                }
                            }else{
                                if($structureArray[0] == "account_number"){
                                    if($structureArray[1] == "1"){
                                        $bankArray[$structureArray[0]] = $field["value"];
                                    }
                                }else{
                                    $bankArray[$structureArray[0]] = $field["value"];
                                }
                            }
                        }
                    }
                }else{
                    $key = "";
                    $value = "";
                    if(isset($data["bank_fields"]) && count($data["bank_fields"])>0){
                        foreach ($data["bank_fields"] as $field){
                            $structureArray = explode("-",$field["field_id"]);
                            if(count($structureArray) > 1){
                                $key =  $structureArray[0];
                                if($data["country_code"] != "JP"){
                                    $seperator = $value == "" ? "-" : "";
                                }else{
                                    $seperator = "";
                                }
                                if($structureArray[1] == "1"){
                                    $value = $field["value"].$seperator.$value;
                                }else{
                                    $value = $value.$seperator.$field["value"];
                                }
                            }else{
                                $bankArray[$structureArray[0]] = $field["value"];
                            }
                        }
                    }

                    if($key != ""){
                        $bankArray[$key] = $value;
                    }
                }
            }
        }

        $dataArray = array();
        $tos_acceptance = array();
        $payout_schedule = array();

        $tos_acceptance["ip"] = $data["ip"];
        $tos_acceptance["date"] = time ();

        $payout_schedule["delay_days"] = "minimum";
        $payout_schedule["interval"] = "weekly";
        $payout_schedule["weekly_anchor"] = "monday";

        $dataArray["country"] = $data["country_code"];
        $dataArray["legal_entity"]["personal_address"]["country"] = $data["country_code"];
        $dataArray["legal_entity"]["address"]["country"] = $data["country_code"];
        $dataArray["business_name"] = $data["biz_name"];
        $dataArray["debit_negative_balances"] = true;
        $dataArray["type"] = "custom";
        $dataArray["legal_entity"] = $legalArray;
        if(count($bankArray)>0){
            $dataArray["external_account"] = $bankArray;
        }
        $dataArray["tos_acceptance"] = $tos_acceptance;
        $dataArray["payout_schedule"] = $payout_schedule;

        return $dataArray;
    }

    private static function generateStripeBankAccountFields($data){
       
        $bankArray = array();
        if(isset($data["bank_fields"]) && count($data["bank_fields"])>0){
            $bankArray["object"] = "bank_account";
            $bankArray["country"] = $data["country_code"];
            $workingCurrency = "";
            
            if($data["country_code"] == "GB"){

                //find the currency
                foreach ($data["bank_fields"] as $field){
                    if($field["field_id"] == "currency"){
                        $workingCurrency = $field["value"];
                        $bankArray[$field["field_id"]] = $field["value"];
                    }
                }

                foreach ($data["bank_fields"] as $field){
                    if($field["field_id"] != "currency"){
                        if(isset($field["field_id"])){
                            $structureArray = explode("-",$field["field_id"]);
                            if($workingCurrency == "eur"){
                                if($structureArray[0] == "account_number" && $structureArray[1] == "2"){
                                    $bankArray[$structureArray[0]] = $field["value"];
                                }
                            }else{
                                if($structureArray[0] == "account_number"){
                                    if($structureArray[1] == "1"){
                                        $bankArray[$structureArray[0]] = $field["value"];
                                    }
                                }else{
                                    $bankArray[$structureArray[0]] = $field["value"];
                                }
                            }
                        }
                    }
                }
            }else{
                $key = "";
                $value = "";
                if(isset($data["bank_fields"]) && count($data["bank_fields"])>0){
                    foreach ($data["bank_fields"] as $field){
                        if(isset($field["field_id"])){
                            $structureArray = explode("-",$field["field_id"]);
                            if(count($structureArray) > 1){
                                $key =  $structureArray[0];
                                if($data["country_code"] != "JP"){
                                    $seperator = $value == "" ? "-" : "";
                                }else{
                                    $seperator = "";
                                }
                                if($structureArray[1] == "1"){
                                    $value = $field["value"].$seperator.$value;
                                }else{
                                    $value = $value.$seperator.$field["value"];
                                }
                            }else{
                                $bankArray[$structureArray[0]] = $field["value"];
                            }
                        }
                    }
                }

                if($key != ""){
                    $bankArray[$key] = $value;
                }
            }
        }

        $dataArray = array();
        if(count($bankArray)>0){
            $dataArray["external_account"] = $bankArray;
        }

        return $dataArray;
    }
}


?>
