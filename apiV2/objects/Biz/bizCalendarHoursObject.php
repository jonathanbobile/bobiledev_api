<?php 

class bizCalendarHoursObject extends bobileObject { 

    public $bch_id = "0";
    public $bch_biz_id = "0";
    public $bch_day_no = "0";
    public $bch_day_name;
    public $bch_begin_hour = "109";
    public $bch_finish_hour = "105";
    public $bch_working_day = "1";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bch_id"])){
              throw new Exception("bizCalendarHoursObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bch_id = $data["bch_id"];
        $instance->bch_biz_id = $data["bch_biz_id"];
        $instance->bch_day_no = $data["bch_day_no"];
        $instance->bch_day_name = $data["bch_day_name"];
        $instance->bch_begin_hour = $data["bch_begin_hour"];
        $instance->bch_finish_hour = $data["bch_finish_hour"];
        $instance->bch_working_day = $data["bch_working_day"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


