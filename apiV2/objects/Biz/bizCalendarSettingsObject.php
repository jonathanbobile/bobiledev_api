<?php 

class bizCalendarSettingsObject extends bobileObject { 

    public $bcs_id = "0";
    public $bcs_biz_id = "0";
    public $biz_cal_24h = "1";
    public $biz_cal_display_start = "109";
    public $biz_cal_display_end = "205";
    public $biz_cal_default_color = "1";
    public $biz_cal_last_emp_id = "0";
    public $biz_cal_last_emp_name;
    public $biz_cal_def_employee = "0";
    public $biz_cal_def_meeting = "0";
    public $biz_cal_def_reminder_hour = "0";
    public $biz_cal_def_reminder_day = "0";
    public $biz_cal_biz_hours_only = "1";
    public $biz_cal_slot_gap = 0;
    public $biz_cal_allow_overtime = 0;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bcs_id"])){
              throw new Exception("bizCalendarSettingsObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bcs_id = $data["bcs_id"];
        $instance->bcs_biz_id = $data["bcs_biz_id"];
        $instance->biz_cal_24h = $data["biz_cal_24h"];
        $instance->biz_cal_display_start = $data["biz_cal_display_start"];
        $instance->biz_cal_display_end = $data["biz_cal_display_end"];
        $instance->biz_cal_default_color = $data["biz_cal_default_color"];
        $instance->biz_cal_last_emp_id = $data["biz_cal_last_emp_id"];
        $instance->biz_cal_last_emp_name = $data["biz_cal_last_emp_name"];
        $instance->biz_cal_def_employee = $data["biz_cal_def_employee"];
        $instance->biz_cal_def_meeting = $data["biz_cal_def_meeting"];
        $instance->biz_cal_def_reminder_hour = $data["biz_cal_def_reminder_hour"];
        $instance->biz_cal_def_reminder_day = $data["biz_cal_def_reminder_day"];
        $instance->biz_cal_biz_hours_only = $data["biz_cal_biz_hours_only"];
        $instance->biz_cal_slot_gap = $data["biz_cal_slot_gap"];
        $instance->biz_cal_allow_overtime = $data["biz_cal_allow_overtime"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


