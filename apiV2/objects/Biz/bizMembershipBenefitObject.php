<?php

/**
 * bizMembershipBenefitObject short summary.
 *
 * bizMembershipBenefitObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class bizMembershipBenefitObject extends bobileObject
{
    /* Identifiers */
    public $mben_id = "0";
    public $mben_mb_id = "0";
    public $mben_biz_id = "0";

    /* Details */
    public $mben_text;
    public $mben_type;//enum('text','discount','product','service','other')
    public $mben_entity_id = "0";
    public $mben_image;
    public $mben_title;
    public $mben_amount = "0";

    public static function withData($membershipBenefitData){
        if(!isset($membershipBenefitData["mben_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->mben_id = isset($membershipBenefitData["mben_id"]) ? $membershipBenefitData["mben_id"] : 0;
        $instance->mben_mb_id = isset($membershipBenefitData["mben_mb_id"]) ? $membershipBenefitData["mben_mb_id"] : 0;
        $instance->mben_biz_id = isset($membershipBenefitData["mben_biz_id"]) ? $membershipBenefitData["mben_biz_id"] : 0;

        /* Details */
        $instance->mben_text = $membershipBenefitData["mben_text"];
        $instance->mben_type = $membershipBenefitData["mben_type"];
        $instance->mben_entity_id = $membershipBenefitData["mben_entity_id"];
        $instance->mben_image = $membershipBenefitData["mben_image"];
        $instance->mben_title = $membershipBenefitData["mben_title"];
        $instance->mben_amount = $membershipBenefitData["mben_amount"];

        return $instance;
    }

}
