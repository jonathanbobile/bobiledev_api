<?php 

class bizPaymentProcessorObject extends bobileObject { 

    public $bp_biz_id = "0";
    public $bp_proc_id = "0";
    public $bp_type;
    public $bp_wallet_id;
    public $bp_active = "0";
    public $bp_processor_id;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bp_biz_id"])){
              throw new Exception("bizPaymentProcessorObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bp_biz_id = $data["bp_biz_id"];
        $instance->bp_proc_id = $data["bp_proc_id"];
        $instance->bp_type = $data["bp_type"];
        $instance->bp_wallet_id = $data["bp_wallet_id"];
        $instance->bp_active = $data["bp_active"];
        $instance->bp_processor_id = $data["bp_processor_id"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


