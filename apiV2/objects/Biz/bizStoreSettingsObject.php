<?php 

class bizStoreSettingsObject extends bobileObject { 

public $ess_id = "0";
public $ess_biz_id = "0";
public $ess_currency = "USD";
public $ess_currency_symbol = "$";
public $ess_weight_units;
public $ess_tax_rate = "0";
public $ess_shipping_time = "5";
public $ess_shipping_price = "0";
public $ess_shipping_policy_on = "0";
public $ess_refund_policy_on = "1";
public $ess_custom_policy_on = "0";
public $ess_shipping_policy;
public $ess_refund_policy;
public $ess_custom_policy;
public $ess_business_name;
public $ess_phone;
public $ess_email;
public $ess_include_tax = "0";
public $ess_require_shipping = "1";
public $ess_checkout_comment_title;
public $ess_set_cash_as_paid;

 function __construct(){} 

public static function withData($data){

if (!isset($data["ess_id"])){
      throw new Exception("bizStoreSettingsObject constructor requies data array provided!");
}

$instance = new self();

$instance->ess_id = $data["ess_id"];
$instance->ess_biz_id = $data["ess_biz_id"];
$instance->ess_currency = $data["ess_currency"];
$instance->ess_currency_symbol = $data["ess_currency_symbol"];
$instance->ess_weight_units = $data["ess_weight_units"];
$instance->ess_tax_rate = $data["ess_tax_rate"];
$instance->ess_shipping_time = $data["ess_shipping_time"];
$instance->ess_shipping_price = $data["ess_shipping_price"];
$instance->ess_shipping_policy_on = $data["ess_shipping_policy_on"];
$instance->ess_refund_policy_on = $data["ess_refund_policy_on"];
$instance->ess_custom_policy_on = $data["ess_custom_policy_on"];
$instance->ess_shipping_policy = $data["ess_shipping_policy"];
$instance->ess_refund_policy = $data["ess_refund_policy"];
$instance->ess_custom_policy = $data["ess_custom_policy"];
$instance->ess_business_name = $data["ess_business_name"];
$instance->ess_phone = $data["ess_phone"];
$instance->ess_email = $data["ess_email"];
$instance->ess_include_tax = $data["ess_include_tax"];
$instance->ess_require_shipping = $data["ess_require_shipping"];
$instance->ess_checkout_comment_title = $data["ess_checkout_comment_title"];
$instance->ess_set_cash_as_paid = $data["ess_set_cash_as_paid"];

return $instance;
}

function _isValid(){
      return true;
}
}
?>


