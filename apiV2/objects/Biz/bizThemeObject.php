<?php 

class bizThemeObject extends bobileObject { 

    public $tmpl_id = "0";
    public $tmpl_name;
    public $tmpl_html;
    public $tmpl_html_out;
    public $tmpl_img1_default = "no_icon_levels.png";
    public $tmpl_background = "background.png";
    public $tmpl_module = "0";
    public $tmpl_category = "0";
    public $tmpl_unique = "0";
    public $tmpl_font = "arial";
    public $tmpl_font_bold = "arialBold";
    public $tmpl_font_color = "#000000";
    public $tmpl_transparent = "0";
    public $tmpl_head_color = "#000000";
    public $tmpl_img_feel = "light";
    public $tmpl_active = "0";
    public $tmpl_have_phone = "0";
    public $tmpl_gen_bg_type = "2";
    public $tmpl_tabbar_bg_type = "1";
    public $tmpl_tabbar_shade_type = "1";
    public $tmpl_navbar_bg_type = "1";
    public $tmpl_sidebar_bg_type = "1";
    public $tmpl_sidemagic_bg_type = "2";
    public $tmpl_shadow_alpha = "0.5";
    public $theme_tab_icon_type = "1";
    public $tmpl_color_1 = "#000000";
    public $tmpl_color_2 = "#000000";
    public $tmpl_color_3 = "#000000";
    public $tmpl_color_4 = "#000000";
    public $tmpl_color_5 = "#000000";
    public $tmpl_color_6 = "#000000";
    public $tmpl_color_7 = "#000000";
    public $tmpl_family = "1";
    public $tmpl_color1_freestyle;
    public $tmpl_color2_freestyle;
    public $tmpl_color3_freestyle;
    public $tmpl_color4_freestyle;
    public $tmpl_color5_freestyle;
    public $tmpl_live_background = "0";
    public $tmpl_live_magic = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["tmpl_id"])){
              throw new Exception("bizThemeObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->tmpl_id = $data["tmpl_id"];
        $instance->tmpl_name = $data["tmpl_name"];
        $instance->tmpl_html = $data["tmpl_html"];
        $instance->tmpl_html_out = $data["tmpl_html_out"];
        $instance->tmpl_img1_default = $data["tmpl_img1_default"];
        $instance->tmpl_background = $data["tmpl_background"];
        $instance->tmpl_module = $data["tmpl_module"];
        $instance->tmpl_category = $data["tmpl_category"];
        $instance->tmpl_unique = $data["tmpl_unique"];
        $instance->tmpl_font = $data["tmpl_font"];
        $instance->tmpl_font_bold = $data["tmpl_font_bold"];
        $instance->tmpl_font_color = $data["tmpl_font_color"];
        $instance->tmpl_transparent = $data["tmpl_transparent"];
        $instance->tmpl_head_color = $data["tmpl_head_color"];
        $instance->tmpl_img_feel = $data["tmpl_img_feel"];
        $instance->tmpl_active = $data["tmpl_active"];
        $instance->tmpl_have_phone = $data["tmpl_have_phone"];
        $instance->tmpl_gen_bg_type = $data["tmpl_gen_bg_type"];
        $instance->tmpl_tabbar_bg_type = $data["tmpl_tabbar_bg_type"];
        $instance->tmpl_tabbar_shade_type = $data["tmpl_tabbar_shade_type"];
        $instance->tmpl_navbar_bg_type = $data["tmpl_navbar_bg_type"];
        $instance->tmpl_sidebar_bg_type = $data["tmpl_sidebar_bg_type"];
        $instance->tmpl_sidemagic_bg_type = $data["tmpl_sidemagic_bg_type"];
        $instance->tmpl_shadow_alpha = $data["tmpl_shadow_alpha"];
        $instance->theme_tab_icon_type = $data["theme_tab_icon_type"];
        $instance->tmpl_color_1 = $data["tmpl_color_1"];
        $instance->tmpl_color_2 = $data["tmpl_color_2"];
        $instance->tmpl_color_3 = $data["tmpl_color_3"];
        $instance->tmpl_color_4 = $data["tmpl_color_4"];
        $instance->tmpl_color_5 = $data["tmpl_color_5"];
        $instance->tmpl_color_6 = $data["tmpl_color_6"];
        $instance->tmpl_color_7 = $data["tmpl_color_7"];
        $instance->tmpl_family = $data["tmpl_family"];
        $instance->tmpl_color1_freestyle = $data["tmpl_color1_freestyle"];
        $instance->tmpl_color2_freestyle = $data["tmpl_color2_freestyle"];
        $instance->tmpl_color3_freestyle = $data["tmpl_color3_freestyle"];
        $instance->tmpl_color4_freestyle = $data["tmpl_color4_freestyle"];
        $instance->tmpl_color5_freestyle = $data["tmpl_color5_freestyle"];
        $instance->tmpl_live_background = $data["tmpl_live_background"];
        $instance->tmpl_live_magic = $data["tmpl_live_magic"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


