<?php 

class levelData10Object extends levelDataObject { 

    public $md_mod_id = "10";
    public $md_billing_type;//enum('once','auto_renew')
    public $md_billing_period_unit;//enum('month','year','')
    public $md_billing_paymnet_count = "0";
    public $md_usage_period;//enum('unlimited','limited','period','once')
    public $md_usage_period_unit;//enum('day','week','month','year','')
    public $md_usage_period_number = "0";
    public $md_usage_period_start = "";
    public $md_usage_period_end;
    public $md_usage_rule_type;//enum('unlimited','n_times','n_times_period')
    public $md_usage_rule_capacity = "0";
    public $md_usage_rule_period_unit;//enum('day','week','month','year','')
    public $md_color;
    public $md_can_cancel = "0";

    public $extra_data = array();

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData10Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        $instance->md_billing_type = $data["md_billing_type"];
        $instance->md_billing_period_unit = $data["md_billing_period_unit"];
        $instance->md_billing_paymnet_count = $data["md_billing_paymnet_count"];
        $instance->md_usage_period = $data["md_usage_period"];
        $instance->md_usage_period_unit = $data["md_usage_period_unit"];
        $instance->md_usage_period_number = $data["md_usage_period_number"];
        $instance->md_usage_period_start = $data["md_usage_period_start"];
        $instance->md_usage_period_end = $data["md_usage_period_end"];
        $instance->md_usage_rule_type = $data["md_usage_rule_type"];
        $instance->md_usage_rule_capacity = $data["md_usage_rule_capacity"];
        $instance->md_usage_rule_period_unit = $data["md_usage_rule_period_unit"];
        $instance->md_color = $data["md_color"];
        $instance->md_can_cancel = $data["md_can_cancel"];

        $instance->extra_data = levelDataManager::getSubscriptionExtraData($instance);
        $instance->extra_data["connected_items"] = levelDataManager::getSubscriptionConnectedItems($instance);
        return $instance;
    }

}
?>


