<?php

/**
 * benefitPZObject short summary.
 *
 * benefitPZObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class benefitPZObject extends customerPersonalObject
{
    public $card_id;
    public $benefit_type;
    public $bizID;
    public $image;
    public $header;
    public $description;
    public $program_type;
    public $stamp_type;
    public $stamp_number;
    public $reward_type;
    public $reward_entity = 0;
    public $reward_number;
    public $valid_from;
    public $valid_to;
    public $disclaimer;
    public $customer_card_redeemed;
    public $customer_card_redeemed_date;
    public $customer_card_code;
    public $customer_card_id;
    public $stamps;
    public $recordID;
    public $stamp_items;
    public $reward_items;   
    public $coupon_get = 0;
    public $benefit_date;

    public $coupon_data = array();
    public $rewards_list = array();
    public $level_data = array();

    public static function withData($data){
        $instance = new self();
        
        $instance->card_id = $data['card_id'];
        $instance->benefit_type = $data['benefit_type'];
        $instance->bizID = $data['bizID'];
        $instance->image = $data['image'];
        $instance->header = $data['header'];
        $instance->description = $data['description'];
        $instance->program_type = $data['program_type'];
        $instance->stamp_type = $data['stamp_type'];
        $instance->stamp_number = $data['stamp_number'];
        $instance->reward_type = $data['reward_type'];
        $instance->reward_entity = utilityManager::getItemTypeIntFromItemTypeString($data['reward_type']);
        $instance->reward_number = $data['reward_number'];
        $instance->valid_from = $data['valid_from'];
        $instance->valid_to = $data['valid_to'];
        $instance->disclaimer = $data['disclaimer'];
        $instance->customer_card_redeemed = $data['customer_card_redeemed'];
        $instance->customer_card_redeemed_date = $data['customer_card_redeemed_date'];
        $instance->customer_card_code = $data['customer_card_code'];
        $instance->customer_card_id = isset($data['customer_card_id']) ? $data['customer_card_id'] : '';
        $instance->stamps = $data['stamps'];
        $instance->recordID = $data['recordID'];
        $instance->stamp_items = isset($data['stamp_items']) ? $data['stamp_items'] : '';
        $instance->reward_items = isset($data['reward_items']) ? $data['reward_items'] : '';
        $instance->benefit_date = $data['benefit_date'];
        $instance->rewards_list = array();

        if(isset($data['rewards_list'])){
            foreach ($data['rewards_list'] as $reward)
            {
            	$instance->rewards_list[] = rewardPZObject::withData($reward);

            }
            
        }

        if(isset($data['coupon_data'])){
            $instance->coupon_data = $data['coupon_data'];
        }

        if(isset($data['level_data'])){
            $instance->level_data = $data['level_data'];
        }

        return $instance;
    }

    public function PersonalZoneAPIArray(mobileRequestObject $request = null){
        $result = array();

        $result['recordID'] = $this->recordID;
        $result['rewardID'] = $this->card_id;
        $result['benefit_type'] = $this->benefit_type;
        $result['benefit_date'] = $this->benefit_date;
        $result['benefit_bizID'] = $this->bizID;

        if($this->benefit_type == "coupon"){
            $levelDataManager = new levelDataManager(26,$request);
            $this->level_data = $levelDataManager->getSingleCouponLevelsData($this->recordID,2)->data;
        }

        if($this->benefit_type == "loyalty"){
            $levelDataManager = new levelDataManager(6,$request);
            $this->level_data = $levelDataManager->getSingleLoyaltyLevelsData($this->recordID,2)->data;
        }

        $result['data'] = $this;

        return $result;
    }
}