<?php 

class customerDocumentObject extends bobileObject { 

    public $do_id = "0";
    public $do_biz_id = "0";
    public $do_cust_id = "0";
    public $do_title;
    public $do_url;
    public $do_external_id = "0";
    public $do_form_id = "0";
    public $do_create_date = "";
    public $do_send_date;
    public $do_receive_date;
    public $do_uploaded_by;//enum('admin','user')
    public $do_type;//enum('exl','pdf','doc','img','form','info')

    public $md_next_view = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["do_id"])){
              throw new Exception("customerDocumentObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->do_id = $data["do_id"];
        $instance->do_biz_id = $data["do_biz_id"];
        $instance->do_cust_id = $data["do_cust_id"];
        $instance->do_title = $data["do_title"];
        $instance->do_url = $data["do_url"];
        $instance->do_external_id = $data["do_external_id"];
        $instance->do_form_id = $data["do_form_id"];
        $instance->do_create_date = $data["do_create_date"];
        $instance->do_send_date = $data["do_send_date"];
        $instance->do_receive_date = $data["do_receive_date"];
        $instance->do_uploaded_by = $data["do_uploaded_by"];
        $instance->do_type = $data["do_type"];

        return $instance;
    }

    public function PersonalZoneAPIArray(mobileRequestObject $requestObject){
        $result = array();

        $result['recordID']=$this->do_id;
        $result['title']=$this->do_title;
        $result['url']=$this->do_url;
        $result['add_date']=$this->do_create_date;
        $result['send_date']=($this->do_send_date == null) ? "" :$this->do_send_date;
        $result['receive_date']=($this->do_receive_date == null) ? "" :$this->do_receive_date;
        $result['uploaded_by']=$this->do_uploaded_by;
        $result['type']=$this->do_type;
        $result['external_id']=$this->do_external_id;
        $result['form_id']=$this->do_form_id;
        $result['add_date_timestamp']=strtotime($this->do_create_date);
        $result['send_date_timestamp']=($this->do_send_date == null) ? "" :strtotime($this->do_send_date);
        $result['receive_date_timestamp']=($this->do_receive_date == null) ? "" :strtotime($this->do_receive_date);
        
        $view = "";
        if($this->do_type == "info"){
            $android = $requestObject->android;
            if($android == 'null' || $android == '') $android = "0";
            $nib_name = "";
            if($android == "1")
            {
                $nib_name = "_android";
            }
            
            $view = bizManager::getViewTypeFromStructID(12,$nib_name);
        }
        if($this->do_type == "form"){
            $android = $requestObject->android;
            if($android == 'null' || $android == '') $android = "0";
            $nib_name = "";
            if($android == "1")
            {
                $nib_name = "_android";
            }
            
            $view = bizManager::getViewTypeFromStructID(32,$nib_name);
        }
        
        $result['md_next_view'] = $view; 

        return $result;
    }

    function _isValid(){
          return true;
    }
}
?>


