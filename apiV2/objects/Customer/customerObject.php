<?php

/**
 * custObject short summary.
 *
 * custObject description.
 *
 * @version 1.0
 * @author JonathanM & IraN
 */
class customerObject extends bobileObject
{    
    /* Identifiers */
    public $cust_id = 0;
    public $cust_biz_id = 0;
    public $cust_reseller_id = -1;
    public $cust_market_id = -1;
    public $cust_status = 'guest'; //enum('invited', 'guest', 'member', 'tester', 'preview')	
    public $cust_appid = 0;
    public $cust_mobile_serial;
    public $cust_inviter = 0;
    public $cust_inviter_points = 0;
    public $cust_stripe_cust_id = '';

    /* Customer Details */
    public $cust_ip = '';
    public $cust_reg_date;
    public $cust_first_name = '';
    public $cust_pic;
    public $cust_big_pic = '';
    public $cust_email = '';
    public $cust_birth_date;
    public $cust_weding_date;
    public $cust_address;
    public $cust_city = '';
    public $cust_country = 0;
    public $cust_state = 0;
    public $cust_zip;
    public $cust_comments;
    public $cust_gender;
    public $cust_phone1 = '';
    public $cust_phone2;
    public $cust_last_seen;
    public $cust_device = ''; //enum('', 'Android', 'IOS', 'Amazon')
    public $cust_valid_code = 0;
    public $cust_valid_phone = '';
    public $cust_validation_date;
    public $cust_longt = 0.0;
    public $cust_lati = 0.0;
    public $cust_location_updated;
    public $cust_privacy_time;


    /* Counters | Actions | Gained | Achieved */
    public $cust_current_points = 0;
    public $cust_total_points = 0;
    public $cust_membership_id = 0;
    public $cust_membership_level = 0;
    public $cust_membership_since;
    public $cust_current_stage = 1; //Relationship funnel
    public $cust_unread_chats = 0;
    public $cust_unattended_orders = 0;
    public $cust_unseen_files = 0;
    public $cust_paymentreq_paid = 0;

    /* Idicators - Boolians */
    public $cust_returning = 0;
    public $cust_online = 0;
    public $cust_send_mail = 1;
    public $cust_need_photo_update = 0;
    public $cust_islead = 0;
    public $cust_is_admin = 0;


    /* Arrays */
    public $custDevices = array(); 
    public $membership;
    public $pointsData;
    public $badges = array();
    public $unredeemedRewardsCount = 0;
    public $default_payment_source;
    public $cust_default_sub ='';


    public static function withData($custData){       
        if(!isset($custData["cust_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->cust_id = (isset($custData["cust_id"]) && $custData["cust_id"] > 0 ) ? $custData["cust_id"] : 0;
        $instance->cust_biz_id = ($custData["cust_biz_id"] && $custData["cust_biz_id"] > 0 ) ? $custData["cust_biz_id"] : 0;
        $instance->cust_reseller_id = (isset($custData["cust_reseller_id"])) ? $custData["cust_reseller_id"] : 0;
        $instance->cust_market_id = (isset($custData["cust_market_id"])) ? $custData["cust_market_id"] : 0;
        $instance->cust_status = (isset($custData["cust_status"])  && $custData["cust_market_id"] != "") ? $custData["cust_status"] : "guest"; //enum('invited', 'guest', 'member', 'tester', 'preview')	
        $instance->cust_ip = $custData["cust_ip"];
        $instance->cust_reg_date = $custData["cust_reg_date"];
        $instance->cust_appid = ($custData["cust_appid"] && $custData["cust_appid"] > 0 ) ? $custData["cust_appid"] : 0;
        $instance->cust_mobile_serial = $custData["cust_mobile_serial"];
        $instance->cust_inviter = $custData["cust_inviter"];
        $instance->cust_inviter_points = $custData["cust_inviter_points"];
        $instance->cust_stripe_cust_id = $custData["cust_stripe_cust_id"];

        /* Customer Details */
        $instance->cust_first_name = $custData["cust_first_name"];
        if(isset($custData["cust_pic"]) && $custData["cust_pic"] != ""){
            $custPic = $custData["cust_pic"];
        }
        else{
            $avatarIndex = ($instance->cust_id % 28) + 1;
            $custPic = "https://storage.googleapis.com/bbassets/avatars/avatar_$avatarIndex.png";
        }

        $instance->cust_pic = $custPic;
        $instance->cust_big_pic = $custData["cust_big_pic"];
        $instance->cust_email = $custData["cust_email"];
        $instance->cust_birth_date = $custData["cust_birth_date"];
        $instance->cust_weding_date = $custData["cust_weding_date"];
        $instance->cust_address = $custData["cust_address"];
        $instance->cust_city = $custData["cust_city"];
        $instance->cust_country = isset($custData["cust_country"]) ? $custData["cust_country"] : 0;
        $instance->cust_state = isset($custData["cust_state"]) ? $custData["cust_state"] : 0;
        $instance->cust_zip = $custData["cust_zip"];
        $instance->cust_comments = $custData["cust_comments"];
        $instance->cust_gender = $custData["cust_gender"];
        $instance->cust_phone1 = $custData["cust_phone1"];
        $instance->cust_phone2 = $custData["cust_phone2"];
        $instance->cust_last_seen = $custData["cust_last_seen"];
        $instance->cust_device = $custData["cust_device"]; //enum('', 'Android', 'IOS', 'Amazon')
        $instance->cust_valid_code = $custData["cust_valid_code"];
        $instance->cust_valid_phone = $custData["cust_valid_phone"];
        $instance->cust_validation_date = $custData["cust_validation_date"];
        $instance->cust_longt = $custData["cust_longt"];
        $instance->cust_lati = $custData["cust_lati"];
        $instance->cust_location_updated = $custData["cust_location_updated"];
        $instance->cust_privacy_time = $custData["cust_privacy_time"];


        /* Counters | Actions | Gained | Achieved */
        $instance->cust_current_points = $custData["cust_current_points"];
        $instance->cust_total_points = $custData["cust_total_points"];
        $instance->cust_membership_id = $custData["cust_membership_id"];
        $instance->cust_membership_level = $custData["cust_membership_level"];
        $instance->cust_membership_since = $custData["cust_membership_since"];
        $instance->cust_current_stage = $custData["cust_current_stage"]; //Relationship funnel
        $instance->cust_unread_chats = $custData["cust_unread_chats"];
        $instance->cust_unattended_orders = $custData["cust_unattended_orders"];
        $instance->cust_unseen_files = $custData["cust_unseen_files"];
        $instance->cust_paymentreq_paid = $custData["cust_paymentreq_paid"];

        /* Idicators - Boolians */
        $instance->cust_returning = $custData["cust_returning"];
        $instance->cust_online = $custData["cust_online"];
        $instance->cust_send_mail = $custData["cust_send_mail"];
        $instance->cust_need_photo_update = $custData["cust_need_photo_update"];
        $instance->cust_islead = $custData["cust_islead"];
        $instance->cust_is_admin = $custData["cust_is_admin"];

        /* membership */
        
        $custMembershipResult = membershipsManager::getCustomerMembershipByLevelAndBizID($instance->cust_biz_id,$instance->cust_membership_level);
        if($custMembershipResult->code == 1){
            $custMembership = $custMembershipResult->data;
            $req_points = $custMembership->next_membership_req_points - $instance->cust_total_points;
            $custMembership->setPointsToNextlevel($req_points);
            
        }
        else{
            $custMembership = new customerMembershipObject();
            $custMembership->bm_text_color = "#000000";
            $custMembership->bm_back_color = "#D9D9D9";
            $custMembership->membership_color = "#D9D9D9";
            $custMembership->membership_text_color = "#000000";
            $nextMembershipResult = membershipsManager::getBizMembershipByLevelAndBizID($instance->cust_biz_id,1);
            if($nextMembershipResult->code == 1){
                $custMembership->setNextMembershipLevelData($nextMembershipResult->data);
                $req_points = $nextMembershipResult->data->bm_min_points - $instance->cust_total_points;
                $custMembership->setPointsToNextlevel($req_points);
            }
        }
        

        /* Default payment */

        $customerBilling = new customerBillingManager();

        $defaultPaymentResult = $customerBilling->getCustomerDefaultPaymentSource($instance->cust_id);

        if($defaultPaymentResult->code == 1){
            $instance->default_payment_source = $defaultPaymentResult->data;
        }

        
        $custMembership->current_points = $instance->cust_current_points;
        $custMembership->total_points = $instance->cust_total_points;

        $instance->membership = $custMembership;

        /* Points */        
        $instance->pointsData['current_points'] = $instance->cust_current_points;
        $instance->pointsData['total_points'] = $instance->cust_total_points;

        /* default sub */
        $primarySubEntryResult = customerSubscriptionManager::getCustomerPrimaryMultiuseEntry($instance->cust_id);
        if($primarySubEntryResult->code == 1){
            $instance->cust_default_sub = $primarySubEntryResult->data;
        }



        return $instance;

    }

    public function getArrayForFriendList(){
        $result = array();

        $result['fr_id']=$this->cust_id;
        $result['fr_name']=$this->cust_first_name;
        $result['fr_registration_date']=$this->cust_reg_date;
        $result['fr_earned_points']=$this->cust_inviter_points;

        return $result;
    }

    function _isValid(){
        return isset($this->cust_id) && $this->cust_id > 0;
    }
    
    public function getSocketIdentifier(){
        return $this->cust_id."_".$this->cust_mobile_serial;
    }
}
