<?php 

class customerPunchUsageObject extends bobileObject { 

    public $cpuse_id = "0";
    public $cpuse_cust_id = "0";
    public $cpuse_biz_id = "0";
    public $cpuse_cpp_id = "0";
    public $cpuse_type;//enum('usage','usage_renew','freeze','resume','cancel','start','ended')
    public $cpuse_time = "";
    public $cpuse_item_type;//enum('product','service','class','order','other')
    public $cpuse_item_id = "0";
    public $cpuse_source;//enum('auto','manual','scan','geolocation')
    public $cpuse_source_id = "0";
    public $cpuse_note;
    public $cpuse_status = "active";//enum('active','revoked')
    public $cpuse_status_change_time;
    public $cpuse_status_changer_id = "0";
    public $cpuse_status_note;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cpuse_id"])){
              throw new Exception("customerPunchUsgaeObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cpuse_id = $data["cpuse_id"];
        $instance->cpuse_cust_id = $data["cpuse_cust_id"];
        $instance->cpuse_biz_id = $data["cpuse_biz_id"];
        $instance->cpuse_cpp_id = $data["cpuse_cpp_id"];
        $instance->cpuse_type = $data["cpuse_type"];
        $instance->cpuse_time = $data["cpuse_time"];
        $instance->cpuse_item_type = $data["cpuse_item_type"];
        $instance->cpuse_item_id = $data["cpuse_item_id"];
        $instance->cpuse_source = $data["cpuse_source"];
        $instance->cpuse_source_id = $data["cpuse_source_id"];
        $instance->cpuse_note = $data["cpuse_note"];
        $instance->cpuse_status = $data["cpuse_status"];
        $instance->cpuse_status_change_time = $data["cpuse_status_change_time"];
        $instance->cpuse_status_changer_id = $data["cpuse_status_changer_id"];
        $instance->cpuse_status_note = $data["cpuse_status_note"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


