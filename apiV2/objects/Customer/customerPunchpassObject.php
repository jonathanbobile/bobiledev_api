<?php

/**
 * customerPunchpassObject short summary.
 *
 * customerPunchpassObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerPunchpassObject extends bobileObject
{
    /* Identifiers */
    public $cpp_id = "0";
    public $cpp_cust_id = "0";
    public $cpp_pass_id = "0";
    public $cpp_biz_id = "0";

    /* Settings */
    public $cpp_price = "0";
    public $cpp_status;//enum('active','frozen','cancelled','ended')
    public $cpp_is_primary = "0";
    public $cpp_purchase_time;
    public $cpp_usage_start_time;
    public $cpp_usage_end_time;
    public $cpp_uses_left = "0";
    public $cpp_usage_capacity = "0";
    public $cpp_usage_rule = "unlimited";//enum('unlimited','n_times_period')
    public $cpp_usage_rule_period;//enum('hour','day','week','month','year')
    public $cpp_usage_rule_capacity = "0";
    public $cpp_usage_rule_uses_left = "0";
    public $cpp_usage_rule_next_renew;
    public $cpp_qr_url = "";

    /* for IOS */
    public $cpp_usage_rule_next_renew_unix = 0;
    public $cpp_usage_end_time_unix = 0;

    /* punch pass */
    public $punchpass;
    public $usages = array();

    public static function withData($custPunchpassData,$withInnerPunchPass = true){
        $instance = new self();

        /* Identifiers */
        $instance->cpp_id = $custPunchpassData["cpp_id"];
        $instance->cpp_cust_id = $custPunchpassData["cpp_cust_id"];
        $instance->cpp_pass_id = $custPunchpassData["cpp_pass_id"];
        $instance->cpp_biz_id = $custPunchpassData["cpp_biz_id"];

        /* Settings */
        $instance->cpp_price = $custPunchpassData["cpp_price"];
        $instance->cpp_status = $custPunchpassData["cpp_status"];
        $instance->cpp_is_primary = $custPunchpassData["cpp_is_primary"];
        $instance->cpp_purchase_time = $custPunchpassData["cpp_purchase_time"];
        $instance->cpp_usage_start_time = $custPunchpassData["cpp_usage_start_time"];
        $instance->cpp_usage_end_time = isset($custPunchpassData["cpp_usage_end_time"]) ? $custPunchpassData["cpp_usage_end_time"] : '';
        $instance->cpp_uses_left = $custPunchpassData["cpp_uses_left"];
        $instance->cpp_usage_capacity = $custPunchpassData["cpp_usage_capacity"];
        $instance->cpp_usage_rule = $custPunchpassData["cpp_usage_rule"];
        $instance->cpp_usage_rule_period = $custPunchpassData["cpp_usage_rule_period"];
        $instance->cpp_usage_rule_capacity = $custPunchpassData["cpp_usage_rule_capacity"];
        $instance->cpp_usage_rule_uses_left = $custPunchpassData["cpp_usage_rule_uses_left"];
        $instance->cpp_usage_rule_next_renew = isset($custPunchpassData["cpp_usage_rule_next_renew"]) ? $custPunchpassData["cpp_usage_rule_next_renew"] : '';

        /* For IOS */
        $instance->cpp_usage_end_time_unix = isset($custPunchpassData["cpp_usage_end_time"]) ? strtotime($custPunchpassData["cpp_usage_end_time"]) : '';
        $instance->cpp_usage_rule_next_renew_unix = isset($custPunchpassData["cpp_usage_rule_next_renew"]) ? strtotime($custPunchpassData["cpp_usage_rule_next_renew"]) : '';
        
        if($withInnerPunchPass && isset($custPunchpassData['md_row_id'])){
            $instance->punchpass = levelData11Object::withData($custPunchpassData);
        }

        if(isset($instance->cpp_id) && $instance->cpp_id != '' && customerSubscriptionManager::isPunchpassMechAllowed($instance->cpp_id,'scan')){
            $instance->cpp_qr_url = API_SERVER.'/admin/eventAsync/punchpassUsage?uid='.utilityManager::encryptIt($instance->cpp_id,true);
        }

        return $instance;
    }

    public function setPunchpass(levelData11Object $punchpass){
        $this->punchpass = $punchpass;
    }

    public function getPunchpass(){
        return $this->punchpass;
    }
}
