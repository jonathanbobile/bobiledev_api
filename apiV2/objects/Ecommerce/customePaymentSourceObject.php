<?php

/**
 * customerPaymentSourceObject short summary.
 *
 * customerPaymentSourceObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerPaymentSourceObject extends bobileObject
{
    /* Identifiers */
    public $cps_id = "0";
    public $cps_cust_id = "0";

    /* Details */
    public $cps_type;
    public $cps_created;
    public $cps_ip;
    public $cps_token;
    public $cps_stipe_object_id;
    public $cps_card_brand;
    public $cps_country;
    public $cps_exp_month = "0";
    public $cps_exp_year = "0";
    public $cps_funding;
    public $cps_last4 = "0000";
    public $cps_name;
    public $cps_phone;
    public $cps_valid = "1";
    public $cps_default = "0";

    /* Settings */
    public $cps_valid;
    public $cps_default;

    public static function withData($custPaySourceData){
        if(!isset($custPaySourceData["cps_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->cps_id = isset($custPaySourceData["cps_id"]) ? $custPaySourceData["cps_id"] : 0;
        $instance->cps_cust_id = isset($custPaySourceData["cps_cust_id"]) ? $custPaySourceData["cps_cust_id"] : 0;

        /* Details */
        $instance->cps_type = $custPaySourceData["cps_type"];
        $instance->cps_created = $custPaySourceData["cps_created"];
        $instance->cps_ip = $custPaySourceData["cps_ip"];
        $instance->cps_token = $custPaySourceData["cps_token"];
        $instance->cps_stipe_object_id = $custPaySourceData["cps_stipe_object_id"];
        $instance->cps_card_brand = $custPaySourceData["cps_card_brand"];
        $instance->cps_country = $custPaySourceData["cps_country"];
        $instance->cps_exp_month = $custPaySourceData["cps_exp_month"];
        $instance->cps_exp_year = $custPaySourceData["cps_exp_year"];
        $instance->cps_funding = $custPaySourceData["cps_funding"];
        $instance->cps_last4 = $custPaySourceData["cps_last4"];
        $instance->cps_name = $custPaySourceData["cps_name"];
        $instance->cps_phone = $custPaySourceData["cps_phone"];

        /* Settings */
        $instance->cps_valid = $custPaySourceData["cps_valid"];
        $instance->cps_default = $custPaySourceData["cps_default"];

        return $instance;
    }

    public function _isValid(){
        return $this->cps_id > 0;
    }
}


