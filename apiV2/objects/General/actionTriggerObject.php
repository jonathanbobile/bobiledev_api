<?php 

class actionTriggerObject extends bobileObject { 

    public $customerId = "0";
    public $actionId = "0";
    public $status = "";
    public $deviceId = "";
    public $replace = "";
    public $action_type = "";
    public $external_id = 0;
    public $data = "";

    function __construct(){} 

    public static function withData($data){
        
        
        if (!isset($data["act_id"])){
            throw new Exception("actionTrigger constructor requies data array provided!");
        }

        $instance = new self();

        $instance->customerId = $data["cust_id"];
        $instance->actionId = $data["act_id"];
        $instance->status = isset($data['status']) ? $data['status'] : '';
        $instance->deviceId = isset($data['device_id']) ? $data['device_id'] : '';
        $instance->replace = isset($data['replace']) ? $data['replace'] : '';

        $action_type = "";
        switch($data['status']){
            case 'order':
            case 'paid_payment':
                $action_type = 'order';
                break;
            case 'appointment':
                $action_type = 'meeting';
                break;
            case 'used_subscription':
            case 'purchased_subscription':
                $action_type = 'subscription';
                break;
            case 'used_punch_pass':
            case 'purchased_punch_pass':
                $action_type = 'punchpass';
                break;
            case 'coupon_redeemed':
            case 'coupon':
                $action_type = 'coupon';
                break;
            case 'class':
                $action_type = 'class_date';
                break;
        }
        $instance->action_type = $action_type;

        $instance->external_id = isset($data['external_id']) ? $data['external_id'] : 0;
        $instance->data = isset($data['data']) ? $data['data'] : '';
        
        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


