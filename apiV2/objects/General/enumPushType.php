<?php

/**
 * Enum for push types
 *
 * @version 1.0
 * @author bobile
 */

class enumPushType extends Enum {
    
    const biz_landingPage = 1;

    const biz_promoteCoupon = 2;     

    const biz_promoteProduct = 3;     

    const biz_promotePunchCard = 4;
    
    const biz_greetingsCard = 5;

    const biz_messageOnly = 6;

    const biz_chat = 7;

    const biz_bobileXPoints = 8;

    const biz_bobileXScratch = 9;

    const biz_bobileXMembership = 10;

    const biz_personalBooking = 11;

    const biz_personalFriends = 12;

    const biz_personalRewards = 13;

    const biz_personalOrders = 14;

    const biz_personalPRequest = 15;

    const biz_personalDocuments = 16;

    const admin_newMeeting = 17;

    const admin_newFormFilled = 18;

    const admin_newClubMember = 19;

    const admin_newDocument = 20;

    const admin_newOrder = 21;

    const admin_chat = 22;

    const admin_couponClaim = 23;

    const admin_unattendedOrders = 24;

    const admin_unreadChat = 25;

    const admin_updateMeeting = 26;

    const admin_cencelMeeting = 27;

    const biz_geoLocation = 28;

    const admin_messageOnly = 29;

    const biz_bobileXScratchempty = 31;

    const biz_pointsFromFriend = 32;

    const geo_subscription_trigger = 33;

    const geo_punchpass_trigger = 34;
}

?>
