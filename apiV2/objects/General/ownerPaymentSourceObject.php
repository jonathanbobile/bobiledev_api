<?php 

class ownerPaymentSourceObject extends bobileObject { 

    public $ops_id = "0";
    public $ops_owner_id = "0";
    public $ops_type;
    public $ops_created = "";
    public $ops_ip;
    public $ops_token;
    public $ops_stipe_object_id;
    public $ops_card_brand;
    public $ops_country;
    public $ops_exp_month = "0";
    public $ops_exp_year = "0";
    public $ops_funding;
    public $ops_last4 = "0000";
    public $ops_name;
    public $ops_phone;
    public $ops_valid = "1";
    public $ops_default = "0";
    public $ops_suspect = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["ops_id"])){
              throw new Exception("ownerPaymentSourceObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->ops_id = $data["ops_id"];
        $instance->ops_owner_id = $data["ops_owner_id"];
        $instance->ops_type = $data["ops_type"];
        $instance->ops_created = $data["ops_created"];
        $instance->ops_ip = $data["ops_ip"];
        $instance->ops_token = $data["ops_token"];
        $instance->ops_stipe_object_id = $data["ops_stipe_object_id"];
        $instance->ops_card_brand = $data["ops_card_brand"];
        $instance->ops_country = $data["ops_country"];
        $instance->ops_exp_month = $data["ops_exp_month"];
        $instance->ops_exp_year = $data["ops_exp_year"];
        $instance->ops_funding = $data["ops_funding"];
        $instance->ops_last4 = $data["ops_last4"];
        $instance->ops_name = $data["ops_name"];
        $instance->ops_phone = $data["ops_phone"];
        $instance->ops_valid = $data["ops_valid"];
        $instance->ops_default = $data["ops_default"];
        $instance->ops_suspect = $data["ops_suspect"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


