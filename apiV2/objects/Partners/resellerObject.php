<?php

class resellerObject extends bobileObject {

    public $reseller_id = "0";
    public $reseller_company_name;
    public $reseller_job_title;
    public $reseller_full_name;
    public $reseller_phone;
    public $reseller_type = "Company";
    public $reseller_employee_number = "1";
    public $reseller_info;
    public $resller_website;
    public $reseller_country_id = "0";
    public $reseller_city;
    public $reseller_zipcode;
    public $reseller_state_id = "0";
    public $reseller_street;
    public $reseller_street_number = "0";
    public $reseller_token;
    public $reseller_paymentMethodToken;
    public $reseller_paymentMethodType;
    public $reseller_cvv;
    public $reseller_lastFourDigits;
    public $reseller_name_on_ccard;
    public $reseller_card_subtype;
    public $reseller_card_country_code;
    public $reseller_card_vendor;
    public $reseller_card_type;
    public $reseller_card_issuer;
    public $reseller_card_level;
    public $reseller_card_bin;
    public $reseller_billed = "0";
    public $reseller_billed_price = "0";
    public $reseller_plan = "30";
    public $reseller_is_demo = "1";
    public $reseller_is_wl = "0";
    public $reseller_wl_brand_name;
    public $reseller_wl_brand_logo;
    public $reseller_wl_client_back;
    public $reseller_wl_favicon;
    public $reseller_wl_protocol = "http";
    public $reseller_wl_domain_main;
    public $reseller_wl_domain;
    public $reseller_wl_market_domain;
    public $reseller_wl_lang = "en";
    public $reseller_wl_url;
    public $reseller_wl_color_1 = "#1875d2";
    public $reseller_wl_color_2 = "#2196F3";
    public $reseller_wl_color_3 = "#e8f4fe";
    public $reseller_wl_color_4 = "#62bb5c";
    public $reseller_wl_color_5 = "#FFFFFF";
    public $reseller_wl_color_6 = "#000000";
    public $reseller_wl_support_email;
    public $reseller_wl_smtp_server;
    public $reseller_wl_smtp_user;
    public $reseller_wl_smtp_pass;
    public $reseller_wl_include_editor = "1";
    public $reseller_redirect_country;
    public $reseller_wl_email_type = "0";
    public $reseller_wl_apikey;
    public $reseller_agreed = "0";
    public $reseller_agreed_on;
    public $reseller_tier = "0";
    public $reseller_program = "0";
    public $reseller_app_price = "0";
    public $reseller_pcas_price = "0";
    public $reseller_wl_set = "0";
    public $reseller_deactivated = "0";
    public $reseller_deactivate_date;
    public $reseller_internal = "0";
    public $reseller_has_manager = "0";
    public $reseller_has_sso = "0";
    public $reseller_app_ios_skin;
    public $reseller_app_android_skin;
    public $reseller_app_ios_sufix;
    public $reseller_app_name;
    public $reseller_app_color;
    public $reseller_app_icon;
    public $reseller_app_splash;
    public $reseller_app_desc;
    public $reseller_app_support_url;
    public $reseller_app_marketing_url;
    public $reseller_app_privacy_url;
    public $reseller_app_keywords;
    public $reseller_app_logo;
    public $reseller_web_hook;
    public $reseller_app_welcome_text;
    public $reseller_show_preview = "0";
    public $reseller_group = "standard";
    public $reseller_display_tooltips = "0";
    public $reseller_billing_model;
    public $reseller_billing_trial_days = "14";

    public $resellers_account;
    public $resellers_owner;

    function __construct(){
        $this->resellers_account = new accountObject();
        $this->resellers_owner = new ownerObject();
    }

    public static function withData($data){

        if (!isset($data["reseller_id"])){
            throw new Exception("resellerObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->reseller_id = $data["reseller_id"];
        $instance->reseller_company_name = $data["reseller_company_name"];
        $instance->reseller_job_title = $data["reseller_job_title"];
        $instance->reseller_full_name = $data["reseller_full_name"];
        $instance->reseller_phone = $data["reseller_phone"];
        $instance->reseller_type = $data["reseller_type"];
        $instance->reseller_employee_number = $data["reseller_employee_number"];
        $instance->reseller_info = $data["reseller_info"];
        $instance->resller_website = $data["resller_website"];
        $instance->reseller_country_id = $data["reseller_country_id"];
        $instance->reseller_city = $data["reseller_city"];
        $instance->reseller_zipcode = $data["reseller_zipcode"];
        $instance->reseller_state_id = $data["reseller_state_id"];
        $instance->reseller_street = $data["reseller_street"];
        $instance->reseller_street_number = $data["reseller_street_number"];
        $instance->reseller_token = $data["reseller_token"];
        $instance->reseller_paymentMethodToken = $data["reseller_paymentMethodToken"];
        $instance->reseller_paymentMethodType = $data["reseller_paymentMethodType"];
        $instance->reseller_cvv = $data["reseller_cvv"];
        $instance->reseller_lastFourDigits = $data["reseller_lastFourDigits"];
        $instance->reseller_name_on_ccard = $data["reseller_name_on_ccard"];
        $instance->reseller_card_subtype = $data["reseller_card_subtype"];
        $instance->reseller_card_country_code = $data["reseller_card_country_code"];
        $instance->reseller_card_vendor = $data["reseller_card_vendor"];
        $instance->reseller_card_type = $data["reseller_card_type"];
        $instance->reseller_card_issuer = $data["reseller_card_issuer"];
        $instance->reseller_card_level = $data["reseller_card_level"];
        $instance->reseller_card_bin = $data["reseller_card_bin"];
        $instance->reseller_billed = $data["reseller_billed"];
        $instance->reseller_billed_price = $data["reseller_billed_price"];
        $instance->reseller_plan = $data["reseller_plan"];
        $instance->reseller_is_demo = $data["reseller_is_demo"];
        $instance->reseller_is_wl = $data["reseller_is_wl"];
        $instance->reseller_wl_brand_name = $data["reseller_wl_brand_name"];
        $instance->reseller_wl_brand_logo = $data["reseller_wl_brand_logo"];
        $instance->reseller_wl_client_back = $data["reseller_wl_client_back"];
        $instance->reseller_wl_favicon = $data["reseller_wl_favicon"];
        $instance->reseller_wl_protocol = $data["reseller_wl_protocol"];
        $instance->reseller_wl_domain_main = $data["reseller_wl_domain_main"];
        $instance->reseller_wl_domain = $data["reseller_wl_domain"];
        $instance->reseller_wl_market_domain = $data["reseller_wl_market_domain"];
        $instance->reseller_wl_lang = $data["reseller_wl_lang"];
        $instance->reseller_wl_url = $data["reseller_wl_url"];
        $instance->reseller_wl_color_1 = $data["reseller_wl_color_1"];
        $instance->reseller_wl_color_2 = $data["reseller_wl_color_2"];
        $instance->reseller_wl_color_3 = $data["reseller_wl_color_3"];
        $instance->reseller_wl_color_4 = $data["reseller_wl_color_4"];
        $instance->reseller_wl_color_5 = $data["reseller_wl_color_5"];
        $instance->reseller_wl_color_6 = $data["reseller_wl_color_6"];
        $instance->reseller_wl_support_email = $data["reseller_wl_support_email"];
        $instance->reseller_wl_smtp_server = $data["reseller_wl_smtp_server"];
        $instance->reseller_wl_smtp_user = $data["reseller_wl_smtp_user"];
        $instance->reseller_wl_smtp_pass = $data["reseller_wl_smtp_pass"];
        $instance->reseller_wl_include_editor = $data["reseller_wl_include_editor"];
        $instance->reseller_redirect_country = $data["reseller_redirect_country"];
        $instance->reseller_wl_email_type = $data["reseller_wl_email_type"];
        $instance->reseller_wl_apikey = $data["reseller_wl_apikey"];
        $instance->reseller_agreed = $data["reseller_agreed"];
        $instance->reseller_agreed_on = $data["reseller_agreed_on"];
        $instance->reseller_tier = $data["reseller_tier"];
        $instance->reseller_program = $data["reseller_program"];
        $instance->reseller_app_price = $data["reseller_app_price"];
        $instance->reseller_pcas_price = $data["reseller_pcas_price"];
        $instance->reseller_wl_set = $data["reseller_wl_set"];
        $instance->reseller_deactivated = $data["reseller_deactivated"];
        $instance->reseller_deactivate_date = $data["reseller_deactivate_date"];
        $instance->reseller_internal = $data["reseller_internal"];
        $instance->reseller_has_manager = $data["reseller_has_manager"];
        $instance->reseller_has_sso = $data["reseller_has_sso"];
        $instance->reseller_app_ios_skin = $data["reseller_app_ios_skin"];
        $instance->reseller_app_android_skin = $data["reseller_app_android_skin"];
        $instance->reseller_app_ios_sufix = $data["reseller_app_ios_sufix"];
        $instance->reseller_app_name = $data["reseller_app_name"];
        $instance->reseller_app_color = $data["reseller_app_color"];
        $instance->reseller_app_icon = $data["reseller_app_icon"];
        $instance->reseller_app_splash = $data["reseller_app_splash"];
        $instance->reseller_app_desc = $data["reseller_app_desc"];
        $instance->reseller_app_support_url = $data["reseller_app_support_url"];
        $instance->reseller_app_marketing_url = $data["reseller_app_marketing_url"];
        $instance->reseller_app_privacy_url = $data["reseller_app_privacy_url"];
        $instance->reseller_app_keywords = $data["reseller_app_keywords"];
        $instance->reseller_app_logo = $data["reseller_app_logo"];
        $instance->reseller_web_hook = $data["reseller_web_hook"];
        $instance->reseller_app_welcome_text = $data["reseller_app_welcome_text"];
        $instance->reseller_show_preview = $data["reseller_show_preview"];
        $instance->reseller_group = $data["reseller_group"];
        $instance->reseller_display_tooltips = $data["reseller_display_tooltips"];
        $instance->reseller_billing_model = $data["reseller_billing_model"];
        $instance->reseller_billing_trial_days = $data["reseller_billing_trial_days"];
      
        $instance->resellers_account = accountObject::withData($data);
        $instance->resellers_owner = ownerObject::withData($data);

        return $instance;
    }

    function _isValid(){       
        return true;
    }
}
?>