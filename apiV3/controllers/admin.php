<?php

class Admin extends Controller{

    public $mobileRequestObject = array();

    public function __construct(){
        parent::__construct();
        set_time_limit(0);
        $this->mobileRequestObject = mobileRequestObject::withData($_REQUEST);
    }
    
    function login(){
        $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "1";
        $pAppid = (isset($_REQUEST["appId"]) && $_REQUEST["appId"] != "") ? $_REQUEST["appId"] : "0";
        $username = $_REQUEST["username"];
        $password = $_REQUEST["password"];

        $resellerID = $this->mobileRequestObject->resellerId > 0 ? $this->mobileRequestObject->resellerId : 0;

        if($resellerID > 0){
            $partnersModel = new partnerModel();
            $resellerResult = $partnersModel->getResellerByID($resellerID);

            if($resellerResult->code == 1){
                $reseller = $resellerResult->data;
                if($reseller->reseller_deactivated == 1){
                    return $this->returnAnswer(resultObject::withData(0,'reseller_deactivated'));
                }
            }
            else{
                return $this->returnAnswer(resultObject::withData(0,'no_reseller_found'));
            }
        }

        return $this->returnAnswer(resultObject::withData(0,'nothing_found'));
    }

    function getPreviewDownloadLink($bizID){
        try{
            $encoded_bizid = utilityManager::encryptIt($bizID,true);
            $link = "https://market.bobile.com/mobile/app/listing/$encoded_bizid";
            if(bizManager::isAppFromReseller($bizID)){
                $marketDomain = bizManager::getWhiteLabelMarketDomainForBiz($bizID);
                if($marketDomain != ''){
                    $link = "$marketDomain/mobile/app/listing/$encoded_bizid";
                }
            }

            return $this->returnAnswer(resultObject::withData(1,'',$link));
        }
        catch(Exception $e){
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }
  
}
