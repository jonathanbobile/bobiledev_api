<?php

/**
 * couponsManager short summary.
 *
 * couponsManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class couponsManager extends Manager
{
    public function getCouponByCouponID($couponID){
        try{
            $levelManager = new levelDataManager(26);
            $couponDataResult = $levelManager->getLevelDataDirectByID($couponID);
            
            $coupon = levelData26Object::withData($couponDataResult->data);
            $couponItemsResult = $this->getCouponConnectedItemsForCouponObject($coupon);
            if($couponItemsResult->code == 1){
                $coupon->setConnectedItems($couponItemsResult->data);
            }
            return resultObject::withData(1,'',$coupon);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$couponID);
            return resultObject::withData(0,$e->getMessage());
        }
    }
    
    public function getCouponConnectedItemsForCouponObject(levelData26Object $coupon){
        try{
            $itemRows = $this->getCouponConnectedItemRows($coupon);

            $items = array();

            foreach ($itemRows as $entry)
            {
            	$item = array();
                $item['coupon_item_id'] = $entry['ci_item_id'];
                $item['coupon_item_type'] = $entry['ci_type'];
                $item['coupon_item_note'] = $entry['ci_item_notes'];

                $items[] = $item;
            }


            return resultObject::withData(1,'',$items);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($coupon));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function setCouponConnectedItemsForCouponByCouponID($couponID,$items){
        try{          
            $this->deleteCouponItemsRowsByCouponID($couponID);

            $this->setCouponConnectedItemRowsByCouponID($couponID,$items);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['coupon_id'] = $couponID;
            $data['items'] = $items;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function createCoupon(levelData26Object $coupon){
        try{
             $levelManager = new levelDataManager(26);
             $result = $levelManager->addLevelData($coupon);
             //To do - add coupon items
             
             return $result;

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updateCouponFromCouponObject(levelData26Object $coupon){
        try{
            $levelManager = new levelDataManager(26);
             $result = $levelManager->updateLevelData($coupon);
             //To do - add coupon items
             
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteCouponByCouponID($couponID){
        try{
            $levelManager = new levelDataManager(26);
            $levelManager->deleteLevelDataById($couponID);

            $this->deleteCouponRowByCouponID($couponID);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$couponID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteCouponItemsByCouponID($couponID){
        try{
            $this->deleteCouponItemsRowsByCouponID($couponID);
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$couponID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

   

    public function getCouponConnectedItemRows(levelData26Object $coupon){
  
        if(method_exists($this,'getCouponConnected_'.$coupon->md_item_type)){
            $result = $this->{'getCouponConnected_'.$coupon->md_item_type}($coupon->md_row_id);
        }else{
            
            $result = $this->db->getTable("SELECT * FROM tbl_coupon_items
                WHERE ci_coupon_id = {$coupon->md_row_id}
                AND ci_type = '{$coupon->md_item_type}'");
        }
        return $result;
    }

    private function getCouponConnected_product($coupon_id){
        $sql = "SELECT * FROM tbl_coupon_items, tbl_mod_data9
                WHERE ci_item_id = md_row_id
                AND ci_coupon_id = $coupon_id
                AND ci_type = 'product'";

        return $this->db->getTable($sql);
    }

    private function getCouponConnected_service($coupon_id){
        $sql = "SELECT * FROM tbl_coupon_items, tbl_biz_cal_meet_type
                WHERE ci_item_id = bmt_id
                AND ci_coupon_id = $coupon_id
                AND ci_type = 'service'";

        return $this->db->getTable($sql);
    }

    private function getCouponConnected_other($coupon_id){
        $sql = "SELECT * FROM tbl_coupon_items
                WHERE ci_coupon_id = $coupon_id
                AND ci_type = 'other'";

        return $this->db->getTable($sql);
    }

    private function setCouponConnectedItemRowsByCouponID($couponID,$items){
        if(!is_array($items)){
            throw new Exception('Items not array');
        }

        foreach ($items as $item)
        {
        	$sql = "INSERT INTO tbl_coupon_items SET
                        ci_coupon_id = $couponID,
                        ci_type = '{$item['coupon_item_type']}',
                        ci_item_id = {$item['coupon_item_type']},
                        ci_item_notes = '".addslashes($item['coupon_item_note'])."'
                    ON DUPLICATE KEY UPDATE
                        ci_item_notes = '".addslashes($item['coupon_item_note'])."'";

            $this->db->execute($sql);
        }
        

    }

    private function deleteCouponItemsRowsByCouponID($couponID){
        $sql = "DELETE FROM tbl_coupon_items
                WHERE ci_coupon_id = $couponID";

        $this->db->execute($sql);
    }

    /************************************* */
    /*     HELPERS CUSTOMERORDER           */
    /************************************* */

    public static function getClaimedCouponsByItem($itemId){

        $instance = new self();

        return $instance->db->getVal("SELECT count(tri_id) 
                                        FROM tbl_cust_transaction_items 
                                        WHERE tri_item_type=1 
                                        AND tri_itm_row_id=$itemId");
    }

    public static function getClaimedCoupoonsForCustomerByCoupon($couponID,$customerID){
        $instance = new self();

        return $instance->db->getVal("SELECT count(*) 
                                        FROM tbl_cust_transaction_items, tbl_cust_transaction_order
                                        WHERE tri_order_id = cto_id 
                                        AND tri_item_type=1 
                                        AND tri_itm_row_id=$couponID
                                        AND tri_redeem_code <> ''
                                        AND cto_cust_id = $customerID");
    }

    public static function getCouponLimitations($couponID){
        if(!isset($couponID) || $couponID == ""){
            return 0;
        }

        $instance = new self();

        $limit = $instance->db->getRow("SELECT * FROM tbl_coupon_limits WHERE cl_coupon_id = $couponID");

        return isset($limit['cl_limit']) ? $limit['cl_limit'] : 0;

    }

}
