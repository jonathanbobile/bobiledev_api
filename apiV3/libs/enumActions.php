<?php

/**
 * Enum for email types
 *
 * @version 1.0
 * @author PapTap
 */

class enumActions extends Enum {
    /**
     * API - new install from google play or paptap market
     */
    const newInstallation = 1; 

    /**
     * ask to confirm email from owner
     */
    const confirmMail = 2;       

    /**
     * API - new customer registration
     */
    const newCustomer = 3;      

    /**
     * API - new mobile shop order
     */
    const newOrder = 4; 
    
    /**
     * New chat message
     */
    const newCatMessage = 5;

    /**
     * Someone claim a coupon
     */
    const claimCoupon = 6;
    
    /**
     * Shedule a meeting
     */
    const newMeeting = 7;
    
    /**
     * is there any overdue orders
     */
    const overdueOrder = 8;
    
    /**
     * recuring purchase was made
     */
    const accountBilled = 9;
    
    /**
     * recurring order was NOT biled
     */
    const wrongPaymentDetails = 10;
    
    /**
     * User didn't install Bobile Admin
     */
    const noAppPreview = 11;
    
    /**
     * API - new custom form
     */
    const newCustomForm = 12;
    
    /**
     * API - new document
     */
    const newDocument = 13;
}
?>