<?php

/**
 * An adapter class for google storage
 *
 *
 * @version 1.0
 * @author Alex
 */

require_once 'imageStorageInterface.php';

class ImageStorageGoogle implements ImageStorageInterface {

    private $_login;
    private $_key;
    private $_scope;
    private $_project;
    private $_storage;
    private $_api;

    function __construct()
    {
        $this->_login = 'paptap@tough-study-125310.iam.gserviceaccount.com';
        $this->_key = ROOT_PATH.'libs/google-api-client/PapTap Master-55c82cab809a.p12';
        $this->_scope = 'https://www.googleapis.com/auth/devstorage.read_write';
        $this->_project = 'tough-study-125310';
        $this->_storage = array(
                    'url' => 'https://storage.googleapis.com/',
                    'prefix' => '');

        require_once 'google-api-client/src/Google/autoload.php';

        $credentials = new Google_Auth_AssertionCredentials($this->_login, $this->_scope, utilityManager::file_get_contents_pt($this->_key));

        $client = new Google_Client();
        $client->setAssertionCredentials($credentials);
        if ($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion();
        }

        # Starting Webmaster Tools Service
        $this->_api = new Google_Service_Storage($client);
    }

    public function uploadFile($file_path, $storage_path, $thumb_width = 122, $thumb_height = 91,$crop_width = 0, $crop_height = 0)
    {
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);
        return $this->uploadFileData($file_content, $storage_path, $thumb_width, $thumb_height, $file_path,$crop_width,$crop_height);
    }

    public function directUpload($file_path, $storage_path,$bucket = BUCKET){
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);

        return $this->uploadFileContent($file_content, $storage_path,$bucket);
    }

    public function uploadFileContent($file_content, $storage_path,$bucket = BUCKET){
        
        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        if(substr(strstr($storage_path, '.'), 1) == 'pdf'){
            $response = $this->_api->objects->insert($bucket,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'application/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));

        }
        else{
            $mimeType = substr(strstr($storage_path, '.'), 1);
            if($mimeType == 'jpg'){
                $mimeType = 'jpeg';
            }
            $response = $this->_api->objects->insert($bucket,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'image/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));

           
        }

        return "https://storage.googleapis.com/$bucket/".$response["name"];
    }

    public function uploadFileData($file_content, $storage_path, $thumb_width = 122, $thumb_height = 91, $file_path = '',$crop_width = 0, $crop_height = 0)
    {
        if($crop_width == 0 || $crop_height == 0){
            $crop_width = $thumb_width;
            $crop_height = $thumb_height;
        }
        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        if($crop_width != 1 && $crop_height != 1 && substr(strstr($storage_path, '.'), 1) != 'pdf'){
            //Crop image before upload
            $extension = substr(strstr($storage_path, '.'), 1);

            $fileData = getimagesizefromstring($file_content);
            $fileMimeType = $fileData['mime'];
            if($fileMimeType == 'image/png'){
                $extension = 'png';
            }
            else if($fileMimeType == 'image/jpg' || $fileMimeType == 'image/jpeg'){
                $extension = 'jpg';
            }

            $file_data = @imagecreatefromstring($file_content);
            $cropped = imageManager::createThumb($file_data, $extension, $crop_width, $crop_height);
            ob_start();
            imagepng($cropped);
            $file_content =  ob_get_contents();
            ob_end_clean();
            //end crop
        }



        if(substr(strstr($storage_path, '.'), 1) == 'pdf'){
            $response = $this->_api->objects->insert(BUCKET,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'application/' . substr(strstr($storage_path, '.'), 1),
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));

        }
        else{
            $mimeType = substr(strstr($storage_path, '.'), 1);
            if($mimeType == 'jpg'){
                $mimeType = 'jpeg';
            }
            $response = $this->_api->objects->insert(BUCKET,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'image/' . $mimeType,
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));
        }

        if (substr($file_path, strrpos($file_path, '/')) != '/empty_file') {

            $file_data = @imagecreatefromstring($file_content);
            if($file_data !== false){
                $this->createAndUploadThumb($file_data, $storage_path, $thumb_width, $thumb_height);
            }
        }

        return "https://storage.googleapis.com/".BUCKET."/".$response["name"];
    }

    public function createDirectThumbImage($file_path,$storage_path, $thumb_width, $thumb_height){
        $file_path = (!utilityManager::startsWith($file_path,"http") && !utilityManager::startsWith($file_path,"/")) ? "http://$file_path" : $file_path;
        $file_content = utilityManager::file_get_contents_pt($file_path);
        $file_data = @imagecreatefromstring($file_content);

        $result = $this->createAndUploadThumb($file_data,$storage_path, $thumb_width, $thumb_height); 
        
        return "https://storage.googleapis.com/".BUCKET_THUMBS."/".$result['name'];
    }

    public function createAndUploadThumb($file_data, $storage_path, $thumb_width, $thumb_height)
    {
        $extension = substr(strstr($storage_path, '.'), 1);

        $thumb = imageManager::createThumb($file_data, $extension, $thumb_width, $thumb_height);
        ob_start();
        imagepng($thumb);
        $file_content =  ob_get_contents();
        ob_end_clean();

        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        $result = $this->_api->objects->insert(BUCKET_THUMBS,
                                               $object,
                                               array('uploadType' => 'media',
                                                     'mimeType' => 'image/' . substr(strstr($storage_path, '.'), 1),
                                                     'data' => $file_content,
                                                     'predefinedAcl' => 'publicRead'));
        return $result;
    }

    public function deleteFiles($path)
    {
        $res = $this->_api->objects->listObjects(BUCKET, array('prefix' => $path));

        if (isset($res['items'])) {
            foreach($res['items'] as $item) {
                $this->deleteFile($item['name']);
            }
        }
    }

    public function deleteFile($path)
    {
        try{
            $this->_api->objects->delete(BUCKET, $path);
        }
        catch(Exception $e){}

        try{
            $this->_api->objects->delete(BUCKET_THUMBS, $path);
        }
        catch(Exception $e){}
    }

    public function createFolder($folder,$bucket = BUCKET)
    {
        $folder = rtrim($folder, '/') . '/'; // adding trailing slash if not exists. If object has a trailing slash it is added as folder
        if (!$this->exists($folder,$bucket)) {
            $object = new Google_Service_Storage_StorageObject();
            $object->setName($folder);
            $this->_api->objects->insert($bucket,
                                               $object,
                                               array('uploadType' => 'media',
                                                     'data' => '',
                                                     'predefinedAcl' => 'publicRead'));
        } else {
            echo 'Folder exists';
        }
        return '';
    }

    public function getFilesInFolder($biz_id, $folder = '', $pdfOnly = false)
    {

        $prefix = $biz_id . '/' . $folder;

        
        $objs = $this->_api->objects->listObjects(BUCKET, array('maxResults' => 10000, 'prefix' => $prefix));
       
        $files = array();
        $folders = array();
        if(!isset($biz_id) || (is_int($biz_id) && $biz_id <= 0)){
           
            return $files;
        }
        if ($folder) {
            $folders[] = array('file' => '..', 'date' => '', 'size' => '', 'extension' => 'dir');
        }
       
        
       
       
        foreach($objs['items'] as $obj) {
            
            
            if ($obj['name'] == $prefix) { continue; }
            $filename = str_replace($prefix, '', $obj['name']);
            
            if (strpos($filename, '/') !== false) {
                $path_parts = explode('/', $filename);
                $folders[$path_parts[0]] = array('file' => $path_parts[0], 'date' => $obj['updated'], 'size' =>  $obj['size'], 'extension' => 'dir');
            } else {
                $file_parts = explode('.', $filename);

                if((!$pdfOnly && $file_parts[1] != 'pdf') || ($pdfOnly && $file_parts[1] == 'pdf')){

                    $files[] = array('file' => 'https://storage.googleapis.com/'.BUCKET.'/' . $obj['name'],
                        'date' => $obj['updated'],
                        'size' => $obj['size'],
                        'extension' => $file_parts[1],
                        'name' => $filename);
                }
            }
        }
        $files = array_merge(array_values($folders), $files);
        return $files;
    }

    public function getFullBizFileTree($biz_id,$folder,$bucket = BUCKET_THUMBS){
        //echo "called: ".$folder."\n";
        $prefix = $biz_id . '/' . $folder;
                
        $objs = $this->_api->objects->listObjects($bucket, array('prefix' => $prefix));

        $tree = array();
        $files = array();
        $folders = array();
        foreach($objs['items'] as $obj) {
            if ($obj['name'] == $prefix) { continue; }
            $filename = str_replace($prefix, '', $obj['name']);

            if (strpos($filename, '/') !== false) {
                $path_parts = explode('/', $filename);
                $folders[$path_parts[0]] = array('name' => $path_parts[0], 'date' => $obj['updated'], 'size' =>  $obj['size'], 'extension' => 'dir','full_text' => $filename);

                
                
            } else {
                $file_parts = explode('.', $filename);

                if(strtolower($file_parts[1]) != 'pdf'){

                    $files[] = array('url' => 'https://storage.googleapis.com/'.BUCKET.'/' . $obj['name'],
                        'thumb_url' => 'https://storage.googleapis.com/'.BUCKET_THUMBS.'/' . $obj['name'],
                        'created' => $obj['updated'],
                        'extension' => $file_parts[1],
                        'image_name' => $file_parts[0],
                        'file_name' => $folder.$filename,
                        'group' => '');
                }
            }
        }

        foreach ($folders as $key => $value)
        {
            $branch = $this->getFullBizFileTree($biz_id,$folder.$key.'/');

            foreach ($branch['files'] as $pos => $entry)
            {
                $branch['files'][$pos]['group'] = $key;
            }
            
            $files = array_merge($branch['files'],$files);
        }

        $tree['folders'] = $folders;
        $tree['files'] = $files;

        return $tree;
    }

    public function getLibraryFiles($folder = '')
    {
        $fh = fopen("debugLibraryTime.txt", 'w');
        $startTime = time();
        fwrite($fh, "Start: ".$startTime."\n");
        
        $objs = $this->_api->objects->listObjects(BUCKET, array('maxResults' => 10000, 'prefix' => $folder));
        
        $files = array();
        $folders = array();

        if ($folder) {
            $folders[] = array('file' => '..', 'date' => '', 'size' => '', 'extension' => 'dir');
        }
               
        foreach($objs['items'] as $obj) {

            if ($obj['name'] == $folder) { continue; }
            $filename = str_replace($folder, '', $obj['name']);
            $file_parts = explode('.', $filename);
          
            $files[] = array('file' => "https://storage.googleapis.com/".BUCKET."/" . $obj['name'], 'date' => $obj['updated'], 'size' => $obj['size'], 'extension' => $file_parts[1], 'name' => $filename);
        }
        $endTime = time();
        fwrite($fh, "End: ".$endTime."\n");
        $delta = $endTime - $startTime;
        fwrite($fh, "Delta: ".$delta."\n");
        fwrite($fh, "Files: \n");
        fwrite($fh, print_r($files,true)."\n");
        fclose($fh);
        return $files;
    }
    
    public function getFolderContents($folder,$bucket = BUCKET,$showDirs = false){


        $objs = $this->_api->objects->listObjects($bucket, array('maxResults' => 100000000, 'prefix' => $folder));

        $folders = array();
        $files = array();

        foreach($objs['items'] as $obj) {


            if ($obj['name'] == $folder) { continue; }

            $filename = str_replace($folder.'/', '', $obj['name']);

            if (strpos($filename, '/') !== false) {
                if($showDirs){
                    $path_parts = explode('/', $filename);
                    $folders[] = array('file' => $path_parts[0], 'date' => $obj['updated'], 'size' =>  $obj['size'], 'extension' => 'dir');
                }
            } else {
                $file_parts = explode('.', $filename);
                if(isset($file_parts[1])){
                    $files[] = array('file' => 'https://storage.googleapis.com/'.$bucket.'/' . $obj['name'], 'date' => $obj['updated'], 'size' => $obj['size'], 'extension' => $file_parts[1], 'name' => $filename);
                }

            }
        }

        $files = array_merge($folders,$files);
        return $files;
    }

    public function getFileCount($folder,$bucket = BUCKET){
        
        $objs = $this->_api->objects->listObjects($bucket, array('maxResults' => 1000000, 'prefix' => $folder));

        $result = array();

        $result['root'] = 0;

        foreach($objs['items'] as $obj) {


            if ($obj['name'] == $folder) { continue; }

            $filename = str_replace($folder.'/', '', $obj['name']);

            if (strpos($filename, '/') !== false) {
                $path_parts = explode('/', $filename);
                if(!isset($result[$path_parts[0]])){
                    $result[$path_parts[0]] = 0;
                }

                $result[$path_parts[0]] += 1;

            } else {

                $result['root'] += 1;
            }
        }

        return $result;
    }

    public function copyFiles($source_path, $dest_path)
    {
        // Checking if this function is used for renaming or moving files
        $rename = false;
        if ((strpos($source_path, '.') !== false && strpos($dest_path, '.') !== false) || (strpos($source_path, '.') === false && !$this->exists($dest_path))) {
            if ($this->exists($dest_path)) {
                die('File exists');
            }
            $rename = true;
        }

        $res = $this->_api->objects->listObjects(BUCKET, array('prefix' => $source_path));
        $parent_folders = str_replace(strrchr(trim($source_path, '/'), '/'), '', $source_path);

        if (isset($res['items'])) {
            foreach($res['items'] as $item) {
                if ($rename) {
                    $dest_name = str_replace($source_path, $dest_path, $item['name']);
                } else {
                    $dest_name = str_replace('//' , '/', $dest_path . str_replace($parent_folders, '', $item['name']));
                }
                $this->copyFile($item['name'], $dest_name);
            }
        }
        return 1;
    }

    public function copyFile($source_path, $dest_path)
    {
        if ($source_path == $dest_path) {
            die('Illegal copy');
        }

        $object = new Google_Service_Storage_StorageObject();
        try
        {

            $this->_api->objects->copy(BUCKET, $source_path, BUCKET, $dest_path, $object, array('destinationPredefinedAcl' => 'publicRead'));
            $this->_api->objects->copy(BUCKET_THUMBS, $source_path, BUCKET_THUMBS, $dest_path, $object,array('destinationPredefinedAcl' => 'publicRead'));
        }
        catch(Google_Service_Exception $e) {
            // some error occured, but we proceed with other files

        }
        return $dest_path;
    }

    public function moveFiles($source_path, $dest_path)
    {
        $this->copyFiles($source_path, $dest_path);
        if (strpos($source_path, '.') !== false) {
            $this->deleteFiles($source_path);
        } else {
            $this->deleteFiles($source_path);
        }
        return 1;
    }

    public function exists($path,$bucket = BUCKET)
    {
        
        try
        {
            $prefix = rtrim($path, '/');
            $res = $this->_api->objects->listObjects($bucket, array('maxResults' => 10000, 'prefix' => $prefix));
           
            return count($res['modelData']) > 0;

        }
        catch(Google_Service_Exception $e) {
            if (strpos($e->getMessage(), 'Forbidden') === false) { // if the error is 'forbidden' it means the object exists, otherwise it is 'not found'
                return false;
            } else {
                return true;
            }
        }

    }

    public function uploadDocument($file_content, $storage_path,$mimeType, $bucket = BUCKET)
    {
        
        $object = new Google_Service_Storage_StorageObject();
        $object->setName($storage_path);

        $response = $this->_api->objects->insert($bucket,
                                         $object,
                                         array('uploadType' => 'media',
                                               'mimeType' => 'application/'.$mimeType,
                                               'data' => $file_content,
                                               'predefinedAcl' => 'publicRead'));
      

        return IMAGE_STORAGE_PATH.$response["name"];
    }

    public function uploadFiles($files,$folder,$bucket = BUCKET){
       
        $result = array();
        $result['data'] = array();
        foreach ($files['files']['name'] as $key => $value)
        {
            $file_content = utilityManager::file_get_contents_pt($files['files']['tmp_name'][$key]);
        	$object = new Google_Service_Storage_StorageObject();
            $storage_path = $folder."/".$value;
            $mimeType = $files['files']['type'][$key];
            $object->setName($storage_path);

            $response = $this->_api->objects->insert($bucket,
                                             $object,
                                             array('uploadType' => 'media',
                                                   'mimeType' => $mimeType,
                                                   'data' => $file_content,
                                                   'predefinedAcl' => 'publicRead'));
            $fileResult = array();

            $fileResult['file_name'] = $value;
            $fileResult['url'] = "https://storage.googleapis.com/$bucket/".$response["name"];
            $fileResult['type'] = explode('/',$mimeType)[1];
            $result['data'][] = $fileResult;
        }
        $result['code'] = 1;
        return $result;
    }
}

?>