<?php
/**
 * Methods for Send SMS Notifications
 *
 * @version 1.0
 * @author PapTap
 */

class smsManager {  
    
    protected $db;  

    private $AUTH_ID = 'MAOWYYOGFINZAWY2I5OG';
    private $AUTH_TOKEN = 'ZWYzMjU5ZjU1MzQ5NjI2N2RiZDI3M2VlZWJlMDc4';
    private $src = '+18889943205'; 
    
    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }  
    }
 
    public static function sendMessage($to,$message,$location="")
    {
       

        $instance = new self();
        $url = 'https://api.plivo.com/v1/Account/'.$instance->AUTH_ID.'/Message/';
        $data = array("src" => $instance->src, "dst" => "$to", "text" => "$message");
        $data_string = json_encode($data);
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_USERPWD, $instance->AUTH_ID . ":" . $instance->AUTH_TOKEN);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec( $ch );
        curl_close($ch);
        return $response;
    }

    public static function receiveSMS($data){
        $instance = new self();

        $text = addslashes($data["Text"]);
        $textArray = explode(":",$data["Text"]);
        $code = trim($textArray[1]);
        $instance->db->execute("
                                INSERT INTO tbl_apple_credentials_sms SET
                                acs_text = '$text',
                                acs_code = '$code',
                                acs_from = '{$data["From"]}',
                                acs_uuid = '{$data["MessageUUID"]}'
                                ");
    }

    public static function getAllNewSMS(){
        $instance = new self();

        $smsTable = $instance->db->getTable("
                                SELECT * FROM tbl_apple_credentials_sms
                                WHERE acs_new = 1
                                ");

        $response = array();
        if(count($smsTable) > 0){
            $response = $smsTable;           
        }
        return $response;
    }
    
    public static function setSmsAsRead($smsId){
        $instance = new self();

        $instance->db->execute("
                                UPDATE tbl_apple_credentials_sms SET
                                acs_new = 0
                                WHERE acs_id=$smsId
                                ");

        $response["code"] = 1;
        return $response;
    }

    public static function canSendCodeSms($phoneNumber,$customerId){

        $instance = new self();

        $validByCountry = $instance->db->getVal("SELECT count(cust_id) FROM tbl_biz,tbl_customers
                                                WHERE cust_id=$customerId
                                                AND biz_id=cust_biz_id   
                                                AND (biz_id not in (370652)
                                                OR (biz_world=1 OR cust_country=biz_addr_country_id OR cust_country=0 OR cust_country IS NULL))") > 0;

        $validByAmmount = $instance->db->getVal("
                                SELECT count(vc_id) FROM  tbl_valid_code_sms_phones
                                WHERE vc_phone_number='$phoneNumber'
                                AND vc_tume > DATE_ADD(NOW(), INTERVAL -1 DAY)
                                ") < 3;

        return $validByCountry && $validByAmmount;
    }

    public static function addSendCodeSms($phoneNumber){

        $instance = new self();

        if(!utilityManager::startsWith($phoneNumber,"+")){
            $phoneNumber = "+".$phoneNumber;
        }

        $instance->db->execute("
                                INSERT INTO tbl_valid_code_sms_phones
                                SET vc_phone_number='$phoneNumber'
                                ");
    }

    public static function deleteSendCodeSms($phoneNumber){

        $instance = new self();

        $instance->db->execute("
                                DELETE FROM tbl_valid_code_sms_phones
                                WHERE vc_phone_number='$phoneNumber'
                                ");
    }
}   

?>
