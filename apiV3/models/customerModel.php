<?php

class customerModel extends Model
{
    

    function __construct()
    {
        parent::__construct();

    }
    
    /************************************* */
    /*   BASIC CUSTOMER - PUBLIC           */
    /************************************* */

    /**
     * Summary of getCustomerWithID
     * @param mixed $custID 
     * @return resultObject
     */
    public function getCustomerWithID($custID){
        
        try {
            
            $custData = $this->loadCustomerFromDB($custID);
           
            if(isset($custData['cust_id']) && $custData['cust_id'] > 0){
                $custObj = customerObject::withData($custData);
                $custObj->badges = $this->getCustomerBadges($custObj);
                $custObj->unredeemedRewardsCount = $this->getCustomerUnredeemedCount($custObj->cust_id);
                
                $result = resultObject::withData(1,'',$custObj);
            }
            else{
                $result = resultObject::withData(0,'no_customer');
            }
            return $result;
          }
          //catch exception
          catch(Exception $e) {
              errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
              $result = resultObject::withData(0,$e->getMessage());
              return $result;
          }
    }

    public function updateCustomer(customerObject $custObj){        
        try{
            $this->upateCustomerDBRow($custObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function deleteCustomerByCustId($custID){
        try{
            $this->deleteCustomerRowByCustID($custID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }
        
    public function addNewCustomer(customerObject $custObj){       
        try{
            $new_cust_id = $this->addNewCustomerToDB($custObj);           
            $result = resultObject::withData(1,'',$new_cust_id);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function addWebCustomer(mobileRequestObject $requestObj,$requestData,$ip){
        try{
            $customer = new customerObject();
            $time = time() % 1000000;
            $customer->cust_mobile_serial = 'web_'.$time;
            $customer->cust_biz_id = $requestObj->bizid;
            $customer->cust_appid = $requestObj->appid;
            $customer->cust_reseller_id = $requestObj->resellerId;
            $customer->cust_ip = $ip;
            $customer->cust_phone1 = "+".utilityManager::getValidPhoneNumber(trim($requestData['phone']));
            $customer->cust_first_name = $requestData['name'];
            $customer->cust_email = $requestData['email'];
            $customer->cust_status = 'guest';

            $new_cust_id = $this->addNewCustomerToDB($customer);           

            customerManager::updateCustomerLocationByIP($new_cust_id,$ip);

            $result = resultObject::withData(1,'',$new_cust_id);
            return $result;
        }
        catch(Exception $e){
            $data = array();
            $data['requestObj'] = $requestObj;
            $data['request'] = $requestData;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }
    
    public function createNewCustomerFromRequest(mobileRequestObject $requestObj,$requestData){       
        try{
            $client = new customerObject();

            
            
            $isMarket = $requestObj->marketId > 0;

            $type = $requestObj->OS == 'Android' ? "Android" : "IOS";

            if(isset($requestData['cust_name']) && $requestData['cust_name'] != ""){
                $custName = $requestData['cust_name'];
            }
            else{
                $custNameResult = $this->getGeneratedCustomerName();   

                $custName = $custNameResult->message;
            }
            
            $custStatus = "guest";
            if($requestObj->appid == "2" || $requestObj->is_appetize == 1){
                $custStatus = "preview";
            }else{
                if(!$this->shouldAddNewCustomer( $requestObj->bizid,$type,$isMarket) && $requestObj->appid > 2){
                    $custName = "Tester";
                    $custStatus = "tester";
                }
            }

            $deviceIP = customerManager::getDeviceIP($requestObj->deviceID,$requestObj->bizid);

            $inviterID = 0;
            if(isset($requestData['invite_clipboard_code']) && $requestData['invite_clipboard_code'] != ""){
                $inviterID = $this->getInviterIDFromCode($requestData['invite_clipboard_code']);
            }
            else{
                $inviterID = $this->getInviterIDFromIP($requestObj->bizid,$deviceIP);                
            }

            $client->cust_biz_id = $requestObj->bizid;            
            $client->cust_device = $type;
            $client->cust_mobile_serial = $requestObj->deviceID;
            $client->cust_market_id = $requestObj->marketId;
            $client->cust_appid = $requestObj->appid;
            $client->cust_reseller_id = $requestObj->resellerId;
            $client->cust_status = $custStatus;
            $client->cust_first_name = $custName;
            $client->cust_reg_date = null;
            $client->cust_inviter = $inviterID;
            $client->cust_ip = $deviceIP;

            if(isset($requestData['cust_initial_points']) && $requestData['cust_initial_points'] != ""){
                $client->cust_current_points = $requestData['cust_initial_points'];
                $client->cust_total_points = $requestData['cust_initial_points'];
            }

            $new_cust_id = $this->addNewCustomerToDB($client);           

            if($inviterID > 0){
                eventManager::actionTrigger(enumCustomerActions::invitedAFriend,$inviterID,'invited_friend',$custName,'',$new_cust_id);
            }

            customerManager::updateCustomerLocationByIP($new_cust_id,$deviceIP);

            $result = resultObject::withData(1,'',$new_cust_id);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($requestObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }
    
    public function getCustomerMasterArray(){

        $custData = array();
        
         /* Identifiers */
         $custData["cust_id"] = 0;
         $custData["cust_biz_id"]  = 0;
         $custData["cust_reseller_id"]  = 0;
         $custData["cust_market_id"]  = 0;
         $custData["cust_status"]   = 0;
         $custData["cust_ip"] = "";
         $custData["cust_reg_date"] = "";
         $custData["cust_appid"]  = 0;
         $custData["cust_mobile_serial"] = "";
         $custData["cust_inviter"] = 0;
         $custData["cust_inviter_points"] = 0;
         $custData["cust_stripe_cust_id"] = "";

         /* Customer Details */
         $custData["cust_first_name"] = "";
         $custData["cust_pic"] = "";
         $custData["cust_big_pic"] = "";
         $custData["cust_email"] = "";
         $custData["cust_birth_date"];
         $custData["cust_weding_date"] = "";
         $custData["cust_address"] = "";
         $custData["cust_city"] = "";
         $custData["cust_country"] = "";
         $custData["cust_state"] = "";
         $custData["cust_zip"] = "";
         $custData["cust_comments"] = "";
         $custData["cust_gender"] = "";
         $custData["cust_phone1"] = "";
         $custData["cust_phone2"] = "";
         $custData["cust_last_seen"] = "";
         $custData["cust_device"] = ""; //enum('', 'Android', 'IOS', 'Amazon')
         $custData["cust_valid_code"] = "";
         $custData["cust_valid_phone"] = "";
         $custData["cust_validation_date"] = "";
         $custData["cust_longt"] = "";
         $custData["cust_lati"] = "";
         $custData["cust_location_updated"] = "";
         $custData["cust_privacy_time"] = "";


         /* Counters | Actions | Gained | Achieved */
         $custData["cust_current_points"] = 0;
         $custData["cust_total_points"] = 0;
         $custData["cust_membership_id"] = 0;
         $custData["cust_membership_level"] = 0;
         $custData["cust_membership_since"] = "";
         $custData["cust_current_stage"] = 0; //Relationship funnel
         $custData["cust_unread_chats"] = 0;
         $custData["cust_unattended_orders"] = 0;
         $custData["cust_unseen_files"] = 0;
         $custData["cust_paymentreq_paid"] = 0;

         /* Idicators - Boolians */
         $custData["cust_returning"] = 0;
         $custData["cust_online"] = 0;
         $custData["cust_send_mail"] = 1;
         $custData["cust_need_photo_update"] = 0;
         $custData["cust_islead"] = 0;
         $custData["cust_is_admin"] = 0;

         return $custData;

    }

    public function validateWebCustomer($custID){
        try{
            $customerRow = $this->loadCustomerFromDB($custID);
            $customer = customerObject::withData($customerRow);

            if(utilityManager::contains($customer->cust_mobile_serial,'web_') && !isset($customer->cust_validation_date)){
                $customer->cust_valid_phone = $customer->cust_phone1;
                $now = new DateTime();
                $customer->cust_validation_date = $now->format('Y-m-d H:i:s');
                $customer->cust_status = 'member';
                $this->upateCustomerDBRow($customer);
                smsManager::deleteSendCodeSms($customer->cust_valid_phone);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Searches for a customer record by the device ID and environment the mobile device is in.
     * @param mobileRequestObject $request
     * @return resultObject
     */
    public function getCusIdByDevice(mobileRequestObject $request,$filter_status = ""){


            try{
                if($request->server_customer_id > 0){
                    //if we received a valid customer id from the device - this is the value to return.
                    if ($this->customerExists($request->server_customer_id)){
                        return resultObject::withData(1,'',$request->server_customer_id);
                    }
                }

                //From here we support legacy apps that don't pass server_customer_id + Frist time entry to the app//
                $candidateRecords = $this->getAllCustRecordsForDevice($request->bizid, $request->deviceID, $request->appid,$filter_status);

                //We found only one record with this device for the app & biz -> Guest that hasn't registered yet//
                if (count($candidateRecords) == 1){ 
                    return resultObject::withData(1,'',$candidateRecords[0]["cust_id"]);
                }

                //We found a customer with one full phone record & one unverified record (empty phone)//
                //If we have more than one record it means that one of them has to be with a phone//.
                if (count($candidateRecords) > 1){
                    $custIdFromMulti = $this->getVerifiedRecordForDevice($request->bizid, $request->deviceID, $request->appid,$request->marketId);
                    return resultObject::withData(1,'',$custIdFromMulti); 
                }

                //We didn't find this device for this biz_id & appid in tbl_customers//
                //Here we try & check if it is a slave device in the multi device//
                $masterCusId = 0;
                if (count($candidateRecords) == 0){
                    $masterCusId = $this->getRecordsFromMultiForDevice($request->bizid, $request->deviceID, $request->appid,$request->marketId);
                }

                return resultObject::withData(1,'',$masterCusId); 


            }catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($request));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
     * Generates custom name to customer
     * * Return Data = String - generated name
     * @return resultObject
     */
    public function getGeneratedCustomerName(){

        try{
            $adjective = $this->db->getTable("SELECT * FROM tbl_guest_names_helper WHERE gnh_type='adjective'");
            $noun = $this->db->getTable("SELECT * FROM tbl_guest_names_helper WHERE gnh_type='noun'");

            $adjPosition = rand(0 , count($adjective) - 1 );
            $nounPosition = rand(0 , count($noun) - 1 );

            $adjSelected = $adjective[$adjPosition]["gnh_value"];
            $nounSelected = $noun[$nounPosition]["gnh_value"];

            return resultObject::withData(1,"$adjSelected $nounSelected");;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($request));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Checks if the app is already on google play or apple store
     * * Return Data = boolean 
     * * true - if the device is a tester (apple google testers)
     * * false - if real customers
     * @param mixed $biz_id 
     * @param mixed $device 
     * @return resultObject
     */
    public function isAppleOrGoogleTester($biz_id, $device){
        
        $isTester = true; 
        
        $bizModel = new bizModel($biz_id);
        $resultBiz = $bizModel->getBizByBizId();

        if($resultBiz->code == 1){
            $bizObject = $resultBiz->data;

            if($device == "Android"){  
                if($bizObject->biz_sbmt_goo_pack_id == ""){
                    if($bizObject->biz_bobile_status == "2"){
                        $isTester = false;
                    }
                }else{
                    if($bizObject->biz_goog_status == "2"){
                        $isTester = false;
                    }
                }
            }else{
                if($bizObject->biz_sbmt_apl_bundle_suffix == ""){
                    if($bizObject->biz_bobile_status == "2"){
                        $isTester = false;
                    }
                }else{
                    if($bizObject->biz_appl_status == "2"){
                        $isTester = false;
                    }
                }
            }
            
        }

        return resultObject::withData(1,$isTester);
    }

    public function shouldAddNewCustomer($biz_id, $device, $isMarket){
        
        
        if($isMarket){
            return true;
        }
        
        $canAdd = false;        
        $bizRow = $this->db->getRow("select biz_bobile_status,biz_goog_status,biz_appl_status,biz_sbmt_apl_bundle_suffix,biz_sbmt_goo_pack_id from tbl_biz where biz_id=$biz_id");

        if($device == "Android"){  
            if($bizRow["biz_sbmt_goo_pack_id"] == ""){
                if($bizRow["biz_bobile_status"] == "2"){
                    $canAdd = true;
                }
            }else{
                if($bizRow["biz_goog_status"] == "2"){
                    $canAdd = true;
                }
            }
        }else{
            if($bizRow["biz_sbmt_apl_bundle_suffix"] == ""){
                if($bizRow["biz_bobile_status"] == "2"){
                    $canAdd = true;
                }
            }else{
                if($bizRow["biz_appl_status"] == "2"){
                    $canAdd = true;
                }
            }
        }
        return $canAdd;
    }

    /**
     * Returns a key/value array of badge counters. 
     * In the array, the key is the badge type, and the value is the count
     * @param mixed $cust_id 
     * @return array
     */
    public function getCustomerBadges(customerObject $customer){
        $result = array();
        if($customer->cust_id == 0 || $customer->cust_id == ""){
            return $result;
        }
        
        
        $result = array();
        $badges = array();
        $sql = "SELECT tbpz_type as type,IF(unreadCount IS NULL,0,unreadCount) as count FROM tbl_biz_personal
                    LEFT JOIN (SELECT cbt_type,COUNT(cbt_id) as unreadCount FROM tbl_customer_badge_tracker
                                WHERE cbt_cust_id = {$customer->cust_id}
                                GROUP BY cbt_type) as badges ON badges.cbt_type = tbpz_type
                    WHERE tbpz_biz_id = {$customer->cust_biz_id}";

        $badges = $this->db->getTable($sql);
        

        foreach ($badges as $entry)
        {
            $result[$entry['type']] = $entry['count']; 
        }

        $result['chat'] = $this->getUnreadCustomerChatBadge($customer->cust_id);

        $result['subscriptions'] = customerSubscriptionManager::getCustomerActiveSubscriptionsCount($customer->cust_id);
        $result['punchpass'] = customerSubscriptionManager::getCustomerActivePunchpassCount($customer->cust_id);
        
        return $result;
    }

    public function getUnreadCustomerChatBadge($cust_id){
        $sql = "SELECT COUNT(ch_id) FROM tbl_chat
                WHERE ch_customer = $cust_id
                AND ch_dir = 1
                AND ch_new = 1";

        return $this->db->getVal($sql);
    }

    /**
     * Updates the name and phone number for customer by cust_id. Also generates a validation code
     * @param mixed $cust_id 
     * @param mixed $phone 
     * @param mixed $name 
     * @throws Exception 
     * @return resultObject - validation code
     */
    public function setCustomerValidationCodeAndPhoneAndName($cust_id,$phone,$name = ""){
        try{
           
            $customerRow = $this->loadCustomerFromDB($cust_id);
                                   
            $customer = customerObject::withData($customerRow);
            
            $validCode = mt_rand(100000, 999999);
            
            $customer->cust_valid_phone = $phone;
            $customer->cust_valid_code = $validCode;

            if($name != ""){
                $customer->cust_first_name = $name;
            }
            
            $this->upateCustomerDBRow($customer);
            
            return resultObject::withData(1,'',$validCode);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    private function getInviterIDFromCode($code){
        $codeParts = explode("-",$code);
        return $codeParts[3]; 
    }

    private function getInviterIDFromIP($bizID,$ip){
        $inviterId = 0;
        $friendSql = "SELECT * FROM tbl_friends WHERE fr_biz_id=$bizID AND fr_ip = '$ip' order by fr_id DESC limit 1";
        $friend = $this->db->getRow($friendSql);
        if(isset($friend["fr_id"])){
            $inviterId = $friend["fr_inviter_id"];
            $this->db->execute("DELETE FROM tbl_friends WHERE fr_biz_id=$bizID AND fr_ip = '$ip'");
        }

        return $inviterId;
    }


    /* on boarding validation */

    /**
     * Return customer id by phone number
     * * Return Data = (string) customer ID
     * @param mixed $phone 
     * @param mobileRequestObject $request 
     * @return resultObject
     */
    public function getCustIDByValidPhoneNumber($phone,mobileRequestObject $request){
        try{
            $existingCustID = $this->getCustIDRecordByPhoneNumber_DB($phone,$request->bizid, $request->appid,$request->marketId);

            return resultObject::withData(1,'',$existingCustID);
            
        }
        catch(Exception $e){
            $errdata = array();
            $errdata['phone'] = $phone;
            $errdata['request'] = $request;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($errdata));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customer row (if exist) by customer ID and validation code
     * * Return Data = customerObject
     * @param mixed $custServerId 
     * @param mixed $validationCode 
     * @return resultObject
     */
    public function getCustByCodeAndId($custServerId, $validationCode){
        try{
            $customerRow = $this->getCustByCodeAndId_DB($custServerId, $validationCode);
            if(is_array($customerRow)){
                return resultObject::withData(1,'',customerObject::withData($customerRow));
            }else{
                return resultObject::withData(1,'', new customerObject());
            }
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Get EXISTING customer row (if exist) by Phone,BizId,AppId,MarketId
     * Return row if the cus_id different from current mobile customer
     * * Return Data = customerObject
     * @param customerObject $currentCustomer 
     * @param mixed $bizId 
     * @param mixed $appId 
     * @param mixed $marketId 
     * @return resultObject
     */
    public function getExistingCustByPhoneAppMarket(customerObject $currentCustomer,mobileRequestObject $request){
        
        try{
            $customerRow = $this->getExistingCustByPhoneAppMarket_DB($currentCustomer,$request);
            if(is_array($customerRow)){
                return resultObject::withData(1,'',customerObject::withData($customerRow));
            }else{
                return resultObject::withData(1,'', new customerObject());
            }
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Delete customers for provided biz,app and deviceId
     * @param customerObject $existingCustomer 
     * @param mobileRequestObject $request 
     * @return resultObject
     */
    public function deleteMultiCustomerRecordsFromTblCust(customerObject $existingCustomer,mobileRequestObject $request){
        try{
            $this->deleteMultiCustomerRecordsFromTblCust_DB($existingCustomer,$request);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update Current user with ID of the existing user
     * The current user will be deleted after this action
     * @param customerObject $existingCustomer 
     * @param customerObject $currentCustomer 
     * @return resultObject
     */
    public function updateExistingCustomerWithDataFromCurrent(customerObject $existingCustomer,customerObject $currentCustomer){
        try{
            $this->updateExistingCustomerWithDataFromCurrentCustomer_DB($existingCustomer,$currentCustomer);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /*  public general function */

    public function getAllCustomersUpcomingAppointments(customerObject $customer){
        try{
            $appointments = $this->getCustomerUpcomingAppointmentsDB($customer);

            $outList = array();

            foreach ($appointments as $appointment)
            {
            	if($appointment['type'] == 'meeting'){
                    $meetingResult = bookingManager::getEmployeeMeetingByID($appointment['appoint_id']);
                    if($meetingResult->code == 1){
                        $outList[] = $meetingResult->data;
                    }
                    
                }
                else if($appointment['type'] == 'class'){
                    $classCustomerDateResult = bookingManager::getClassDateCustomerByID($appointment['appoint_id']);
                    if($classCustomerDateResult->code == 1){
                        $outList[] = $classCustomerDateResult->data;
                    }
                }
            }
            
            return resultObject::withData(1,'',$outList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getPersonalDataForcustomerFromType(customerObject $customer,$type){
        
        $outData = array();
        switch($type){
            case "meetings":
                if(bizManager::hasPersonalZoneElement('appointments',$customer->cust_biz_id)){
                    
                    $meetingsListResult = $this->getAllCustomersUpcomingAppointments($customer);
                    if($meetingsListResult->code == 1){
                        $outData = $meetingsListResult->data;
                    }
                }

                break;
            case "push":
                if(bizManager::hasPersonalZoneElement('notifications',$customer->cust_biz_id)){
                    
                    $dataToSend['featured'] = array();
                    $lastpushid = isset($_REQUEST["lastpushid"]) ? $_REQUEST["lastpushid"] : 0;
                    $lastreminderid = isset($_REQUEST["lastreminderid"]) ? $_REQUEST["lastreminderid"] : 0;
                    $pushesResult = $this->getCustomerPushes($customer,$lastpushid,$customer->cust_mobile_serial,$lastreminderid);
                    
                    if($pushesResult->code == 1){
                        $outData = $pushesResult->data;
                        
                    }
                }
                break;
            case "orders":
                if(bizManager::hasPersonalZoneElement('orders',$customer->cust_biz_id)){
                    
                    $ordersResult = customerOrderManager::getCustomerPersonalZoneOrders($customer,'eCommerce');
                    

                    if($ordersResult->code == 1){
                        $outData = $ordersResult->data;
                    }
                }
                break;
            case "benefits":
                
                if(bizManager::hasPersonalZoneElement('benefits',$customer->cust_biz_id)){
                    
                    $benefitsResult = $this->getCustomerBenefitsForPersonalZone($customer);
                    if($benefitsResult->code == 1){
                        if($benefitsResult->code == 1){
                            $outData = $benefitsResult->data;
                        }
                    }
                }
                break;
            case "requests":
                if(bizManager::hasPersonalZoneElement('requests',$customer->cust_biz_id)){
                    
                    $ordersResult = customerOrderManager::getCustomerPersonalZoneOrders($customer,'paymentRequest');
                    

                    if($ordersResult->code == 1){
                        $outData = $ordersResult->data;
                    }
                }
                break;
            case "documents":
                if(bizManager::hasPersonalZoneElement('documents',$customer->cust_biz_id)){
                    
                    $documentsResult = $this->getCustomerDocuments($customer);
                    

                    if($documentsResult->code == 1){
                        $outData = $documentsResult->data;
                        
                    }
                }
                break;
            case "wishlist":
                $wishlistResult = $this->getCustomerWishlist($customer);
                
                if($wishlistResult->code == 1){
                    $outData = $wishlistResult->data;
                }
                break;
            case "friends":
                if(bizManager::hasPersonalZoneElement('invitefriends',$customer->cust_biz_id)){
                    
                    $friendsResult = $this->getCustomerInvitedFriends($customer);
                    
                    if($friendsResult->code == 1){
                        $outData = $friendsResult->data;
                    }
                }
                break;
            case "subscriptions":
                if(bizManager::hasPersonalZoneElement('subscriptions',$customer->cust_biz_id)){
                   
                    $subscriptionsResult = $this->getCustomerSubscriptions($customer);
                    
                    if($subscriptionsResult->code == 1){
                        $outData = $subscriptionsResult->data;
                    }
                }
                break;
            case "punchpasses":
                if(bizManager::hasPersonalZoneElement('punchpass',$customer->cust_biz_id)){
                   
                    $punch_passesResult = $this->getCustomerPunchpasses($customer);
                    
                    if($punch_passesResult->code == 1){
                        $outData = $punch_passesResult->data;
                    }
                }
                break;
            case "multiuseitems":
                $punch_passesResult = $this->getCustomerPunchpasses($customer);
                
                if($punch_passesResult->code == 1){
                    $response["multiuse"] = array_merge($outData,$punch_passesResult->data);
                }
                $subscriptionsResult = $this->getCustomerSubscriptions($customer);
                if($subscriptionsResult->code == 1){
                    $response["multiuse"] = array_merge($outData,$subscriptionsResult->data);
                }
                break;
            case "points":
               
                $dataToSend = array();                   

                $entriesResult = $this->getPointsHistoryForCustomer($customer,0,0);

                if($entriesResult->code == 1){
                    $outData = $entriesResult->data;       
                }
                break;
            case "membership":
                
                $outData = $customer->membership;
                break;
            case "badges":
               
               
                $outData = $this->getCustomerBadges($customer);
                break;
            case "default_payment":
                
                $customerBilling = new customerBillingManager();
                $defPayResult = $customerBilling->getCustomerDefaultPaymentSource($customer->cust_id);
                
                if($defPayResult->code == 1){
                    $outData = $defPayResult->data;
                }
                break;
        }
       
        return $outData;
    }

    /************************************* */
    /*   ADVANCED CUSTOMER                 */
    /************************************* */

    public function claimFromPointsShop(customerObject $customer, pointsGrantObject $item){
        try{
            $benefitObj = new customerBenefitObject();

            $benefitObj->cb_biz_id = $customer->cust_biz_id;
            $benefitObj->cb_cust_id = $customer->cust_id;
            $benefitObj->cb_type = "points_redeem";
            $benefitObj->cb_item_type = $item->pg_entity_type;
            $benefitObj->cb_external_id = $item->pg_entity_id;
            $benefitObj->cb_value = $item->pg_cost;
            $benefitObj->cb_title = $item->label;
            $benefitObj->cb_description = $item->pg_text;
            $benefitObj->cb_image = $item->pg_img;
            $benefitObj->cb_code = utilityManager::generateRedeemCode($customer->cust_id."1");
            $grantResult = customerManager::addCustomerBenefit($benefitObj);

            if($grantResult->code == 0){
                return resultObject::withData(0,'cant_claim');
            }

            $grantID = $grantResult->data;

            $this->addPointshopRedeemEntry($customer,$item,$grantID);

            $customer->cust_current_points = $customer->cust_current_points - $item->pg_cost;

            $this->updateCustomer($customer);

            $pointsCost = -1 * $item->pg_cost;

            $pointsHistoryEntry = new customerPointsHistoryObject();

            $pointsHistoryEntry->cph_biz_id = $customer->cust_biz_id;
            $pointsHistoryEntry->cph_cust_id = $customer->cust_id;
            $pointsHistoryEntry->cph_source = "redeemd_points";
            $pointsHistoryEntry->cph_value = $pointsCost;
            $pointsHistoryEntry->cph_external_id = $grantID;

            $this->addCustomerPointsHistory($pointsHistoryEntry);

            customerManager::addBadgeToCustomer($customer->cust_id,'benefits');

            $result = array();
            $result['cost'] = $item->pg_cost;
            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['item'] = $item;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function unclaimFromPointsShop(customerObject $customer,$benefitID){
        try{
            $benefitResult = customerManager::getCustomerBenefit($benefitID);
            if($benefitResult->code == 0){
                return resultObject::withData(0,'no_benefit');
            }
            $benefit = $benefitResult->data;

            $customer->cust_current_points += $benefit->cb_value;

            $this->updateCustomer($customer);

            $deleteResult = customerManager::deleteCustomerBenefitByBenefitID($benefitID);

            if($deleteResult->code == 1){
                $this->deletePointsshopRedeemEntryByBenefitIDDB($customer,$benefitID);
            }

            $result = array();
            $result['points'] = $benefit->cb_value;
            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['rewardID'] = $benefitID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerBenefitsForPersonalZone(customerObject $customer,$skip = 0, $take = 0){
        try{
            $benefitsRows = $this->getCustomerBenefitEntryRowsByCustID($customer->cust_id,$skip,$take);

            $benefitsList = array();
            foreach ($benefitsRows as $benefitRow)
            {
            	
                $benefit = customerBenefitObject::withData($benefitRow);

                $pzBenefit = $this->getPersonalZoneBenefitFromBenefit($benefit);

                if(isset($pzBenefit)){
                    $benefitsList[] = $pzBenefit;
                }
            }
            

            return resultObject::withData(1,'',$benefitsList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }    

    public function getCustomerBenefitsForItems(customerObject $customer,$items){
        try{
            $itemBenefits = array();

            foreach ($items as $item)
            {
            	$itemBenefits[$item->item_id] = array();

                $benefits = $this->getCustomerBenefitsForItem_DB($customer,$item->type,$item->item_id);

                $coupons = $this->getCustomerCouponsForItem_DB($customer,$item->type,$item->item_id);

                $itemUsableBenefits = array_merge($benefits,$coupons);

                foreach ($itemUsableBenefits as $benefitRow)
                {
                	$benefit = customerBenefitObject::withData($benefitRow);

                    $itemBenefits[$item->item_id][] = $this->getPersonalZoneBenefitFromBenefit($benefit);
                }
            }

            $cartUsableBenefit = $this->getCustomerCouponsForCart_DB($customer);

            foreach ($cartUsableBenefit as $benefitRow)
            {
                $benefit = customerBenefitObject::withData($benefitRow);

                $itemBenefits['cart'][] = $this->getPersonalZoneBenefitFromBenefit($benefit);
            }
            
            
            return resultObject::withData(1,'',$itemBenefits);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerBenefitsForCartDiscount(customerObject $customer){

    }

    public function getCustomerDocuments(customerObject $customer,$skip = 0, $take = 0){
        try{
            $documents = $this->getCustomerDocumentsDB($customer,$skip,$take);

            $docsList = array();

            foreach ($documents as $document)
            {
            	$docsList[] = customerDocumentObject::withData($document);
            }
            
            return resultObject::withData(1,'',$docsList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerWishlist(customerObject $customer,$skip = 0, $take = 0){
        try{
            $wishesRows = $this->getCustomerWishListDB($customer,$skip,$take);

            $wishes = array();

            foreach ($wishesRows as $wishRow)
            {
            	$wish = wishListObject::withData($wishRow);
                $wishResult = customerManager::getFullWishList($wish); 
                if($wishResult->code == 1){
                    $wishes[] = $wishResult->data;
                }
            }
            
            return resultObject::withData(1,'',$wishes);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerInvitedFriends(customerObject $customer){
        try{
            $friendsRows = $this->getCustomerInvitedFriendsDB($customer);

            $friendsList = array();

            foreach ($friendsRows as $friend)
            {
            	$friendsList[] = customerObject::withData($friend);
            }           

            return resultObject::withData(1,'',$friendsList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerSubscriptions(customerObject $customer,$skip = 0,$take = 0){
        try{
            $custSubRows = $this->getCustomerSubscriptionsDB($customer,$skip,$take);
            $custSubs = array();

            foreach ($custSubRows as $subRow)
            {
            	$custSubs[] = subscriptionPZObject::withData($subRow);
            }            

            return resultObject::withData(1,'',$custSubs);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerPunchpasses(customerObject $customer,$skip = 0,$take = 0){
        
        try{
            $custPpassRows = $this->getCustomerPunchpassesDB($customer,$skip,$take);
            $custPpass = array();

            foreach ($custPpassRows as $ppassRow)
            {
            	$custPpass[] = punchpassPZObject::withData($ppassRow);
            }            

            return resultObject::withData(1,'',$custPpass);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerPushes(customerObject $customer,$lastID,$deviceID,$lastreminderid = ""){
        try{
            $instance = new self();
            
            $pushesRows = $instance->getCustomerPushesDB($customer,$lastID,$deviceID,$lastreminderid);

            $pushes = array();
            
            foreach ($pushesRows as $pushRow)
            {
            	$pushes[] = pushPZObject::withData($pushRow);
            }           
            
            return resultObject::withData(1,'',$pushes);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['lastID'] = $lastID;
            $data['deviceID'] = $deviceID;
            $data['lastReminder'] = $lastreminderid;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerLimitedPushes(customerObject $customer,$deviceID,$skip = 0,$take = 20){
        try{
            $instance = new self();
            
            $pushesRows = $instance->getCustomerLimitedPushesDB($customer,$deviceID,$skip);

            $pushes = array();
            
            foreach ($pushesRows as $pushRow)
            {
            	$pushes[] = pushPZObject::withData($pushRow);
            }           
            
            return resultObject::withData(1,'',$pushes);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['deviceID'] = $deviceID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMER - DB METHODS       */
    /************************************* */
    private function loadCustomerFromDB($custID){
        
        if (!is_numeric($custID) || $custID <= 0){
            throw new Exception("Illegal value custID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customers WHERE cust_id = $custID");
    }

    private function addNewCustomerToDB(customerObject $custObj){
        $birthDate = isset($custObj->cust_birth_date) ? "'$custObj->cust_birth_date'" : "NULL";
        $weddingDate = isset($custObj->cust_weding_date) ? "'$custObj->cust_weding_date'" : "NULL";
        $privacyTime = isset($custObj->cust_privacy_time) ? "'$custObj->cust_privacy_time'" : "NULL";
        $gender = isset($custObj->cust_gender) ? "'$custObj->cust_gender'" : "NULL";
        $reg_date = isset($custObj->cust_reg_date) ? "'$custObj->cust_reg_date'" : "NOW()";
        $last_seen = isset($custObj->cust_last_seen) ? "'$custObj->cust_last_seen'" : "NOW()";
        $validation_date = isset($custObj->cust_validation_date) ? "'$custObj->cust_validation_date'" : "NULL";
        $location_updated = isset($custObj->cust_location_updated) ? "'$custObj->cust_location_updated'" : "NULL";
        $membership_since = isset($custObj->cust_membership_since) ? "'$custObj->cust_membership_since'" : "NOW()";
        $cust_pic = isset($custObj->cust_pic) ? "'$custObj->cust_pic'" : "NULL";
        $cust_address = isset($custObj->cust_address) ? "'$custObj->cust_address'" : "NULL";

         $newCustId = $this->db->execute("INSERT INTO tbl_customers SET 
                                            cust_biz_id = {$custObj->cust_biz_id},
                                            cust_reseller_id = {$custObj->cust_reseller_id},
                                            cust_market_id = {$custObj->cust_market_id},
                                            cust_status = '{$custObj->cust_status}',
                                            cust_returning = {$custObj->cust_returning},
                                            cust_ip = '".addslashes($custObj->cust_ip)."',
                                            cust_reg_date = $reg_date,
                                            cust_first_name = '".addslashes($custObj->cust_first_name)."',
                                            cust_online = {$custObj->cust_online},
                                            cust_pic = $cust_pic,
                                            cust_email = '".addslashes($custObj->cust_email)."',
                                            cust_big_pic = '".addslashes($custObj->cust_big_pic)."',
                                            cust_birth_date = $birthDate,
                                            cust_weding_date = $weddingDate,
                                            cust_address = $cust_address,
                                            cust_city = '".addslashes($custObj->cust_city)."',
                                            cust_country = {$custObj->cust_country},
                                            cust_state = {$custObj->cust_state},
                                            cust_zip = '".addslashes($custObj->cust_zip)."',
                                            cust_comments = '".addslashes($custObj->cust_comments)."',
                                            cust_gender = $gender,
                                            cust_send_mail = {$custObj->cust_send_mail},
                                            cust_phone1 = '".addslashes($custObj->cust_phone1)."',
                                            cust_phone2 = '".addslashes($custObj->cust_phone2)."',
                                            cust_mobile_serial = '".addslashes($custObj->cust_mobile_serial)."',
                                            cust_last_mobile_serial = '".addslashes($custObj->cust_last_mobile_serial)."',
                                            cust_need_photo_update = {$custObj->cust_need_photo_update},
                                            cust_appid = {$custObj->cust_appid},
                                            cust_last_seen = $last_seen,
                                            cust_device = '{$custObj->cust_device}',
                                            cust_last_device = '{$custObj->cust_last_device}',
                                            cust_valid_code = {$custObj->cust_valid_code},
                                            cust_valid_phone = '".addslashes($custObj->cust_valid_phone)."',
                                            cust_validation_date = $validation_date,
                                            cust_islead = {$custObj->cust_islead},
                                            cust_longt = {$custObj->cust_longt},
                                            cust_lati = {$custObj->cust_lati},
                                            cust_location_updated = $location_updated,
                                            cust_is_admin = {$custObj->cust_is_admin},
                                            cust_current_points = {$custObj->cust_current_points},
                                            cust_total_points = {$custObj->cust_total_points},
                                            cust_membership_id = {$custObj->cust_membership_id},
                                            cust_membership_level = {$custObj->cust_membership_level},
                                            cust_membership_since = $membership_since,
                                            cust_current_stage = {$custObj->cust_current_stage},
                                            cust_inviter = {$custObj->cust_inviter},
                                            cust_inviter_points = {$custObj->cust_inviter_points},
                                            cust_unread_chats = {$custObj->cust_unread_chats},
                                            cust_unattended_orders = {$custObj->cust_unattended_orders},
                                            cust_paymentreq_paid = {$custObj->cust_paymentreq_paid},
                                            cust_unseen_files = {$custObj->cust_unseen_files},
                                            cust_privacy_time = $privacyTime,
                                            cust_stripe_cust_id = '".addslashes($custObj->cust_stripe_cust_id)."'");

         return $newCustId;
    }

    private function upateCustomerDBRow(customerObject $custObj){
        $birthDate = isset($custObj->cust_birth_date) ? "'$custObj->cust_birth_date'" : "NULL";
        $weddingDate = isset($custObj->cust_weding_date) ? "'$custObj->cust_weding_date'" : "NULL";
        $privacyTime = isset($custObj->cust_privacy_time) ? "'$custObj->cust_privacy_time'" : "NULL";
        $gender = isset($custObj->cust_gender) ? "'$custObj->cust_gender'" : "NULL";
        $validation_date = isset($custObj->cust_validation_date) ? "'$custObj->cust_validation_date'" : "NULL";
        $location_updated = isset($custObj->cust_location_updated) ? "'$custObj->cust_location_updated'" : "NULL";
        $cust_pic = isset($custObj->cust_pic) ? "'$custObj->cust_pic'" : "NULL";
        $cust_address = isset($custObj->cust_address) ? "'$custObj->cust_address'" : "NULL";

        $this->db->execute("UPDATE tbl_customers SET 
                                            cust_biz_id = {$custObj->cust_biz_id},
                                            cust_reseller_id = {$custObj->cust_reseller_id},
                                            cust_market_id = {$custObj->cust_market_id},
                                            cust_status = '{$custObj->cust_status}',
                                            cust_returning = {$custObj->cust_returning},
                                            cust_ip = '".addslashes($custObj->cust_ip)."',
                                            cust_reg_date = '{$custObj->cust_reg_date}',
                                            cust_first_name = '".addslashes($custObj->cust_first_name)."',
                                            cust_online = {$custObj->cust_online},
                                            cust_pic = $cust_pic,
                                            cust_email = '".addslashes($custObj->cust_email)."',
                                            cust_big_pic = '".addslashes($custObj->cust_big_pic)."',
                                            cust_birth_date = $birthDate,
                                            cust_weding_date = $weddingDate,
                                            cust_address = $cust_address,
                                            cust_city = '".addslashes($custObj->cust_city)."',
                                            cust_country = {$custObj->cust_country},
                                            cust_state = {$custObj->cust_state},
                                            cust_zip = '".addslashes($custObj->cust_zip)."',
                                            cust_comments = '".addslashes($custObj->cust_comments)."',
                                            cust_gender = $gender,
                                            cust_send_mail = {$custObj->cust_send_mail},
                                            cust_phone1 = '".addslashes($custObj->cust_phone1)."',
                                            cust_phone2 = '".addslashes($custObj->cust_phone2)."',
                                            cust_mobile_serial = '".addslashes($custObj->cust_mobile_serial)."',
                                            cust_last_mobile_serial = '".addslashes($custObj->cust_last_mobile_serial)."',
                                            cust_need_photo_update = {$custObj->cust_need_photo_update},
                                            cust_appid = {$custObj->cust_appid},
                                            cust_last_seen = '{$custObj->cust_last_seen}',
                                            cust_device = '{$custObj->cust_device}',
                                            cust_last_device = '{$custObj->cust_last_device}',
                                            cust_valid_code = {$custObj->cust_valid_code},
                                            cust_valid_phone = '".addslashes($custObj->cust_valid_phone)."',
                                            cust_validation_date = $validation_date,
                                            cust_islead = {$custObj->cust_islead},
                                            cust_longt = {$custObj->cust_longt},
                                            cust_lati = {$custObj->cust_lati},
                                            cust_location_updated = $location_updated,
                                            cust_is_admin = {$custObj->cust_is_admin},
                                            cust_current_points = {$custObj->cust_current_points},
                                            cust_total_points = {$custObj->cust_total_points},
                                            cust_membership_id = {$custObj->cust_membership_id},
                                            cust_membership_level = {$custObj->cust_membership_level},
                                            cust_membership_since = '{$custObj->cust_membership_since}',
                                            cust_current_stage = {$custObj->cust_current_stage},
                                            cust_inviter = {$custObj->cust_inviter},
                                            cust_inviter_points = {$custObj->cust_inviter_points},
                                            cust_unread_chats = {$custObj->cust_unread_chats},
                                            cust_unattended_orders = {$custObj->cust_unattended_orders},
                                            cust_paymentreq_paid = {$custObj->cust_paymentreq_paid},
                                            cust_unseen_files = {$custObj->cust_unseen_files},
                                            cust_privacy_time = $privacyTime,
                                            cust_stripe_cust_id = '".addslashes($custObj->cust_stripe_cust_id)."'
                                WHERE cust_id = {$custObj->cust_id}");
    }

    private function deleteCustomerRowByCustID($custID){
        if (!is_numeric($custID) || $custID <= 0){
            throw new Exception("Illegal value custID");             
        }

        $this->db->execute("DELETE FROM tbl_customers WHERE cust_id = $custID");
    }

    private function customerExists($custID){

        if (!is_numeric($custID) || $custID <= 0){
            throw new Exception("Illegal value for customer ID");             
        }

        $records = $this->db->getVal("SELECT COUNT(cust_id) vol FROM tbl_customers WHERE cust_id = $custID");

        return $records > 0;

    }

    private function getAllCustRecordsForDevice($biz_id, $device_id, $appId,$filter_status = ""){

        if (!isset($device_id) || $device_id == ""){
            throw new Exception("Illegal value for device_id: $device_id");             
        }
        if (!is_numeric($biz_id) || $biz_id <= 0){
            throw new Exception("Illegal value for biz_id: $biz_id");             
        }
        if (!is_numeric($appId)){
            throw new Exception("Illegal value for app_id: $appId");             
        }

        $statusSql = "";
        if($filter_status != ''){
            $statusSql = " AND cust_status IN ($filter_status) ";
        }
       
        return $this->db->getTable("SELECT cust_id FROM tbl_customers 
                                    WHERE cust_mobile_serial='$device_id' 
                                    AND cust_biz_id=$biz_id 
                                    AND cust_appid=$appId
                                    $statusSql");

    }

    private function getVerifiedRecordForDevice ($biz_id, $device_id,$appId,$marketId){
        
        if (!isset($device_id) || $device_id == ""){
            throw new Exception("Illegal value for device_id: $device_id");             
        }
        if (!is_numeric($biz_id) || $biz_id <= 0){
            throw new Exception("Illegal value for biz_id: $biz_id");             
        }
        if (!is_numeric($appId)){
            throw new Exception("Illegal value for app_id: $appId");             
        }

        $marketId = (!isset($marketId) || $marketId == "" || $marketId == 'null') ? "-1" : $marketId;


        return $this->db->getVal("SELECT cust_id FROM tbl_customers 
                                  WHERE cust_mobile_serial='$device_id' 
                                  AND cust_biz_id=$biz_id 
                                  AND cust_appid=$appId 
                                  AND cust_market_id = $marketId
                                  AND cust_phone1 != ''"); 

    }

    private function getRecordsFromMultiForDevice($biz_id, $device_id, $appId, $marketId){

        if (!isset($device_id) || $device_id == ""){
            throw new Exception("Illegal value for device_id: $device_id");             
        }
        if (!is_numeric($biz_id) || $biz_id <= 0){
            throw new Exception("Illegal value for biz_id: $biz_id");             
        }
        if (!is_numeric($appId)){
            throw new Exception("Illegal value for app_id: $appId");             
        }
        if (!is_numeric($marketId)){
            throw new Exception("Illegal value for marketId: $marketId");             
        }

        return $this->db->getVal("SELECT cd_cust_id FROM tbl_cust_device,tbl_customers 
                                        WHERE cust_id=cd_cust_id 
                                        AND cd_biz_id=cust_biz_id 
                                        AND cd_biz_id=$biz_id 
                                        AND cd_device='$device_id' 
                                        AND cd_market_id = $marketId
                                        AND cd_app_id = $appId");

    }

    /* on boarding validation */

    /**
     * Return customer id by phone number, app, biz and market from DB
     * @param mixed $phone 
     * @param mixed $biz_id 
     * @param mixed $app_id 
     * @param mixed $market_id 
     * @return string
     */
    private function getCustIDRecordByPhoneNumber_DB($phone,$biz_id,$app_id,$market_id){        
        $sql = "SELECT cust_id FROM tbl_customers 
                WHERE cust_phone1='+$phone' 
                AND cust_biz_id = $biz_id 
                AND cust_appid = $app_id 
                AND cust_market_id=$market_id";

        return $this->db->getVal($sql);
    }

    /**
     * Get customer row (if exist) by suctomer ID and validation code from DB
     * @param mixed $custServerId 
     * @param mixed $validationCode 
     * @return array
     */
    private function getCustByCodeAndId_DB($custServerId, $validationCode){   
        
        if (!is_numeric($custServerId) || $custServerId <= 0){
            throw new Exception("Illegal value custServerId");             
        }

        if ($validationCode == ""){
            throw new Exception("Illegal value validationCode");             
        }

        $sql = "SELECT * FROM tbl_customers 
                WHERE cust_id=$custServerId 
                AND cust_valid_code = '$validationCode'";

        return $this->db->getRow($sql);
    }

    /**
     * Get EXISTING customer row (if exist) by Phone,BizId,AppId,MarketId from DB
     * Return row if the cus_id different from current mobile customer
     * @param customerObject $customer 
     * @param mixed $bizId 
     * @param mixed $appId 
     * @param mixed $marketId 
     * @return array
     */
    private function getExistingCustByPhoneAppMarket_DB(customerObject $currentCustomer, mobileRequestObject $request){

        if (!is_numeric($request->bizid) || $request->bizid <= 0){
            throw new Exception("Illegal value bizId");             
        }

        if (!is_numeric($request->appid) || $request->appid < 0){
            throw new Exception("Illegal value appId");             
        }

        if (!is_numeric($request->marketId)){
            throw new Exception("Illegal value marketId");             
        }

        if ($currentCustomer->cust_id == 0){
            throw new Exception("Illegal customerObject currentCustomer");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customers 
                                                    WHERE cust_phone1='{$currentCustomer->cust_valid_phone}' 
                                                    AND cust_biz_id={$request->bizid} 
                                                    AND cust_id <> {$currentCustomer->cust_id} 
                                                    AND cust_appid = {$request->appid}
                                                    AND cust_market_id = {$request->marketId}");
    }

    /**
     * Delete customers from DB for provided biz,app and deviceId
     * @param customerObject $existingCustomer 
     * @param mobileRequestObject $request 
     * @throws Exception 
     * @return double|integer|string
     */
    private function deleteMultiCustomerRecordsFromTblCust_DB(customerObject $existingCustomer,mobileRequestObject $request){

        if (!is_numeric($request->bizid) || $request->bizid <= 0){
            throw new Exception("Illegal value bizId");             
        }

        if (!is_numeric($request->appid) || $request->appid < 0){
            throw new Exception("Illegal value appid");             
        }

        if ($existingCustomer->cust_mobile_serial == ""){
            throw new Exception("Illegal value cust_mobile_serial");             
        }

        return $this->db->execute("DELETE FROM tbl_customers 
                                WHERE cust_mobile_serial='{$existingCustomer->cust_mobile_serial}'
                                AND cust_biz_id={$request->bizid}
                                AND cust_appid={$request->appid} 
                                AND cust_status in ('guest','tester')");
    }

    /**
     * Update customer ID in all tables for current user with ID of the existing user
     * @param customerObject $existingCustomer 
     * @param customerObject $currentCustomer 
     * @throws Exception 
     */
    private function updateExistingCustomerWithDataFromCurrentCustomer_DB(customerObject $existingCustomer,customerObject $currentCustomer){

        if (!is_numeric($existingCustomer->cust_id) || $existingCustomer->cust_id <= 0){
            throw new Exception("Illegal object existingCustomer");             
        }

        if (!is_numeric($currentCustomer->cust_id) || $currentCustomer->cust_id <= 0){
            throw new Exception("Illegal object currentCustomer");             
        }

        $this->db->execute("update tbl_cust_transaction SET tr_cust_id={$existingCustomer->cust_id} where tr_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_transaction_order SET cto_cust_id={$existingCustomer->cust_id} where cto_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_employee_meeting SET em_cust_id={$existingCustomer->cust_id} where em_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_benefits SET cb_cust_id={$existingCustomer->cust_id} where cb_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_card SET customer_card_cust_id={$existingCustomer->cust_id} where customer_card_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update ignore tbl_group_cust SET gc_cust_id={$existingCustomer->cust_id} where gc_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_documents SET do_cust_id={$existingCustomer->cust_id} where do_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_wish_list SET wl_customer_id={$existingCustomer->cust_id} where wl_customer_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_class_date_customers SET cdc_customer_id={$existingCustomer->cust_id} where cdc_customer_id={$currentCustomer->cust_id}");
        $this->db->execute("update IGNORE tbl_cust_push SET cup_customer_id={$existingCustomer->cust_id} where cup_customer_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_push_history SET puh_device_id='{$currentCustomer->cust_mobile_serial}' where puh_device_id='{$existingCustomer->cust_mobile_serial}' and puh_biz_id={$currentCustomer->cust_biz_id}");
        $this->db->execute("update tbl_customers SET cust_inviter={$existingCustomer->cust_id} where cust_inviter={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_biz_notification SET noti_cust_id={$existingCustomer->cust_id} where noti_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_chat SET ch_customer={$existingCustomer->cust_id} where ch_customer={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customers_punchpass SET cpp_cust_id={$existingCustomer->cust_id} where cpp_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_actions SET cact_cust_id={$existingCustomer->cust_id} where cact_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_badge_tracker SET cbt_cust_id={$existingCustomer->cust_id} where cbt_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_card SET customer_card_cust_id={$existingCustomer->cust_id} where customer_card_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_events SET cevent_cust_id={$existingCustomer->cust_id} where cevent_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_payment_sources SET cps_cust_id={$existingCustomer->cust_id} where cps_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_reviews SET cr_cust_id={$existingCustomer->cust_id} where cr_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_scratch SET cscard_cust_id={$existingCustomer->cust_id} where cscard_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_stage_hist SET csh_cust_id={$existingCustomer->cust_id} where csh_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_customer_subscription SET csu_cust_id={$existingCustomer->cust_id} where csu_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_benefits SET cb_cust_id={$existingCustomer->cust_id} where cb_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_history SET ch_cust_id={$existingCustomer->cust_id} where ch_cust_id={$currentCustomer->cust_id} AND ch_action_id <> 33");//except for onboarding
        $this->db->execute("update tbl_cust_invoice SET cin_cust_id={$existingCustomer->cust_id} where cin_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_points_history SET cph_cust_id={$existingCustomer->cust_id} where cph_cust_id={$currentCustomer->cust_id} AND cph_source <> 'complete_onboarding'");
        $this->db->execute("update tbl_cust_punch_usage SET cpuse_cust_id={$existingCustomer->cust_id} where cpuse_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_push SET cup_customer_id={$existingCustomer->cust_id} where cup_customer_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_sub_usage SET csuse_cust_id={$existingCustomer->cust_id} where csuse_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_cust_transaction SET tr_cust_id={$existingCustomer->cust_id} where tr_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_reminders SET pu_cust_id={$existingCustomer->cust_id} where pu_cust_id={$currentCustomer->cust_id}");
        $this->db->execute("update tbl_wish_list SET wl_customer_id={$existingCustomer->cust_id} where wl_customer_id={$currentCustomer->cust_id}");
    }

    /* Customer payment sources */

    public function getCustomerDefaultPaymentByCustID($custID){
        
        try{
            $paymentSourceRow = $this->getCustomerDefaultPaymentSourceRowByCustID($custID);
            if($paymentSourceRow != -1){
                $custPaySourceObj = customerPaymentSourceObject::withData($paymentSourceRow);
                $result = resultObject::withData(1,'',$custPaySourceObj); 
            }
            else{
                $result = resultObject::withData(0,'No row found');
            }
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function createCustomerPaymentSource(customerPaymentSourceObject $paySourceObj){
        
        try{
            $paySourceID = $this->addCustomerPaymentSourceRow($paySourceObj);
            $result = resultObject::withData(1,'',$paySourceID); 
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($paySourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

    }

    public function updateCustomerDefaultPaymentByCustPaySourceID($paySourceID){        
        try{
            $this->updateCustomerDefautlPaymentRowByCustpaySourceID($paySourceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$paySourceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function setPaymentSourceAsDefaultForCustomer(customerPaymentSourceObject $paySourceObj){
        
        try{
            $this->setPaymentSourceRowAsDefault($paySourceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($paySourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    private function getCustomerDefaultPaymentSourceRowByCustID($custID){
        if ($custID == "" || $custID <= 0){
            throw new Exception("Illegal customer ID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_payment_sources 
                                    WHERE cps_cust_id = $custID
                                    AND cps_default = 1");
    }

    private function addCustomerPaymentSourceRow(customerPaymentSourceObject $paySourceObj){
        return $this->db->execute("INSERT INTO tbl_customer_payment_sources SET
                                        cps_cust_id = {$paySourceObj->cps_cust_id},
                                        cps_type = '".addslashes($paySourceObj->cps_type)."',
                                        cps_ip = '".addslashes($paySourceObj->cps_ip)."',
                                        cps_token = '".addslashes($paySourceObj->cps_token)."',
                                        cps_stipe_object_id = '".addslashes($paySourceObj->cps_stipe_object_id)."',
                                        cps_card_brand = '".addslashes($paySourceObj->cps_card_brand)."',
                                        cps_country = '".addslashes($paySourceObj->cps_country)."',
                                        cps_exp_month = {$paySourceObj->cps_exp_month},
                                        cps_exp_year = {$paySourceObj->cps_exp_year},
                                        cps_funding = '".addslashes($paySourceObj->cps_funding)."',
                                        cps_last4 = '".addslashes($paySourceObj->cps_last4)."',
                                        cps_name = '".addslashes($paySourceObj->cps_name)."',
                                        cps_phone = '".addslashes($paySourceObj->cps_phone)."',
                                        cps_valid = {$paySourceObj->cps_valid},
                                        cps_default = {$paySourceObj->cps_default}");
    }

    private function updateCustomerDefautlPaymentRowByCustpaySourceID(customerPaymentSourceObject $paySourceObj){
       
        $this->db->execute("UPDATE tbl_customer_payment_sources SET
                                        cps_cust_id = {$paySourceObj->cps_cust_id},
                                        cps_type = '".addslashes($paySourceObj->cps_type)."',
                                        cps_ip = '".addslashes($paySourceObj->cps_ip)."',
                                        cps_token = '".addslashes($paySourceObj->cps_token)."',
                                        cps_stipe_object_id = '".addslashes($paySourceObj->cps_stipe_object_id)."',
                                        cps_card_brand = '".addslashes($paySourceObj->cps_card_brand)."',
                                        cps_country = '".addslashes($paySourceObj->cps_country)."',
                                        cps_exp_month = {$paySourceObj->cps_exp_month},
                                        cps_exp_year = {$paySourceObj->cps_exp_year},
                                        cps_funding = '".addslashes($paySourceObj->cps_funding)."',
                                        cps_last4 = '".addslashes($paySourceObj->cps_last4)."',
                                        cps_name = '".addslashes($paySourceObj->cps_name)."',
                                        cps_phone = '".addslashes($paySourceObj->cps_phone)."',
                                        cps_valid = {$paySourceObj->cps_valid},
                                        cps_default = {$paySourceObj->cps_default}
                                WHERE cps_id = $paySourceObj->cps_id");
    }

    private function setPaymentSourceRowAsDefault(customerPaymentSourceObject $paySourceObj){
        if($paySourceObj->cps_id <= 0 || $paySourceObj->cps_cust_id <= 0){
            throw new Exception("Payment source invalid");
        }

        $sql = "UPDATE tbl_customer_payment_sources SET
                    cps_default = CASE
                                    WHEN cps_id = {$paySourceObj->cps_id} THEN 1
                                    ELSE 0
                                  END
                WHERE cps_cust_id = {$paySourceObj->cps_cust_id}";

        $this->db->execute($sql);
    }

    /* Customer history */

    public function getCustomerHistoryEntriesByCustID($custID,$skip = 0, $take = 40){   
        try{
            $custHistoryEntriesData = $this->getCustomerHistoryEntryRowsByCustID($custID,$skip,$take);

            $custHistoryList = array();
            foreach ($custHistoryEntriesData as $entry)
            {
                $custHistoryList[] = customerHistoryObject::withData($entry);
            }
            
            $result = resultObject::withData(1,'',$custHistoryList);

            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Insert new customerHistoryObject to DB
     * Return Data = new customerHistoryObject ID
     * @param customerHistoryObject $customerHistoryObj 
     * @return resultObject
     */
    public function addCustomerHistory(customerHistoryObject $customerHistoryObj){       
        try{
            $newId = $this->addCustomerHistoryDB($customerHistoryObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerHistory from DB for provided ID
     * * Return Data = customerHistoryObject
     * @param int $customerHistoryId 
     * @return resultObject
     */
    public function getCustomerHistoryByID($customerHistoryId){
        
        try {
            $customerHistoryData = $this->loadCustomerHistoryFromDB($customerHistoryId);
            
            $customerHistoryObj = customerHistoryObject::withData($customerHistoryData);
            $result = resultObject::withData(1,'',$customerHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerHistoryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerHistory in DB
     * @param customerHistoryObject $customerHistoryObj 
     * @return resultObject
     */
    public function updateCustomerHistory(customerHistoryObject $customerHistoryObj){        
        try{
            $this->upateCustomerHistoryDB($customerHistoryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerHistory from DB
     * @param int $customerHistoryID 
     * @return resultObject
     */
    public function deleteCustomerHistoryById($customerHistoryID){
        try{
            $this->deleteCustomerHistoryByIdDB($customerHistoryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerHistoryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * close old stage history and creates new record for given customer
     * @param mixed $cust_id 
     * @return mixed
     */
    public function checkCustomerStage($cust_id){

        $result  = $this->getCustomerWithID($cust_id);
        if($result->code == 1 && $result->data->cust_biz_id != 0){
            $customerObject = $result->data;
        }else{
            $result =  resultObject::withData(0,"no_customer");
            return $this->returnAnswer($result);
        }

        $bizModel = new bizModel($customerObject->cust_biz_id);       
        $result = $bizModel->getStagePeriodForBiz();
        if($result->code == 1){
            $periodDays = $result->data;
        }else{
            $result =  resultObject::withData(0,"no_biz");
            return $this->returnAnswer($result);
        }

        if($result->code == 1){
            $periodDays = $result->data;
        }else{
            $result =  resultObject::withData(0,"no_biz");
            return $this->returnAnswer($result);
        }

        $result = $this->getStageForCustomer($customerObject->cust_id);
        if($result->code == 0){
            $result =  resultObject::withData(0,"cant_get_stage");
            return $this->returnAnswer($result);
        }

        $customerStageHistoryObject = $result->data;
        $curr_stage = $customerStageHistoryObject->csh_stage;

        $isRegistered = isset($customerObject->cust_valid_phone) && $customerObject->cust_valid_phone != '';

        if($curr_stage <= 0){//customer has no set stage yet  

            try{
                $this->updateStageHistoryForCustomer_DB($customerObject, $customerObject->cust_reg_date);
            }
            catch(Exception $e){
                $result =  resultObject::withData(0,"cant_update_stage");
                return $this->returnAnswer($result);
            }

            $stageHistoryObject = new customerStageHistoryObject();
            $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
            $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
            $stageHistoryObject->csh_stage = 1;
            $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
            $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
            $this->addCustomerStageHistory($stageHistoryObject);

            $curr_stage = 1;
        }

        if(!$isRegistered){//customer is not registered yet
            if($curr_stage != 1){

                try{
                    $this->updateStageHistoryForCustomer_DB($customerObject);
                }
                catch(Exception $e){
                    $result =  resultObject::withData(0,"cant_update_stage");
                    return $this->returnAnswer($result);
                }

                $stageHistoryObject = new customerStageHistoryObject();
                $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
                $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
                $stageHistoryObject->csh_stage = 1;
                $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
                $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
                $this->addCustomerStageHistory($stageHistoryObject);
            }
            $result =  resultObject::withData(1,"ok");
            return $this->returnAnswer($result);
        }

        $unredeemed = $this->getCustomerUnredeemedCount($customerObject->cust_id,$periodDays);
        $redeemed = $this->getCustomerRedeemedCount($customerObject->cust_id,$periodDays);
        $result = $this->getCustomerTransactionsCount($customerObject->cust_id,$periodDays);
        if($result->code == 1){
            $transactions = $result->data;
        }else{
            return $this->returnAnswer($result);
        }
        
        /* 
         * The Idea was that redeeming a reward is just like a transaction
         * The Customer converts a propoerty of his own into a product/service that is provided bt the business.
         * The system sees a redeeming action as sort of a purchase.
         * */

        if($isRegistered && $customerObject->cust_membership_level == 0 && ($unredeemed + $redeemed) == 0 && $transactions == 0){//customer is registered but has not purchased or collected anything
            if($curr_stage != 2){
                try{
                    $this->updateStageHistoryForCustomer_DB($customerObject);
                }
                catch(Exception $e){
                    $result =  resultObject::withData(0,"cant_update_stage");
                    return $this->returnAnswer($result);
                }

                $stageHistoryObject = new customerStageHistoryObject();
                $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
                $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
                $stageHistoryObject->csh_stage = 2;
                $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
                $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
                $this->addCustomerStageHistory($stageHistoryObject);
                
            }
            $result =  resultObject::withData(1,"ok");
            return $this->returnAnswer($result);
        }

        if($isRegistered && ($transactions + $redeemed) > 1){//Customer is registered and has made multiple purchases or redeemed benefits
            if($curr_stage != 5){
                try{
                    $this->updateStageHistoryForCustomer_DB($customerObject);
                }
                catch(Exception $e){
                    $result =  resultObject::withData(0,"cant_update_stage");
                    return $this->returnAnswer($result);
                }

                $stageHistoryObject = new customerStageHistoryObject();
                $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
                $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
                $stageHistoryObject->csh_stage = 5;
                $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
                $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
                $this->addCustomerStageHistory($stageHistoryObject);
                
            }
            $result =  resultObject::withData(1,"ok");
            return $this->returnAnswer($result);
        }

        if($isRegistered && ($transactions + $redeemed) == 1){//customer is registered and has made only a single purchase OR redeemd a single benefit
            if($curr_stage != 4){
                try{
                    $this->updateStageHistoryForCustomer_DB($customerObject);
                }
                catch(Exception $e){
                    $result =  resultObject::withData(0,"cant_update_stage");
                    return $this->returnAnswer($result);
                }

                $stageHistoryObject = new customerStageHistoryObject();
                $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
                $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
                $stageHistoryObject->csh_stage = 4;
                $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
                $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
                $this->addCustomerStageHistory($stageHistoryObject);
                
            }
            $result =  resultObject::withData(1,"ok");
            return $this->returnAnswer($result);
        }

        if($isRegistered && ($transactions + $redeemed) == 0 && ($unredeemed + $customerObject->cust_membership_level) > 0){//customer is registered and has collected some benefits and has not made any purchase or redeemd his benefits
            if($curr_stage != 3){
                try{
                    $this->updateStageHistoryForCustomer_DB($customerObject);
                }
                catch(Exception $e){
                    $result =  resultObject::withData(0,"cant_update_stage");
                    return $this->returnAnswer($result);
                }

                $stageHistoryObject = new customerStageHistoryObject();
                $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
                $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
                $stageHistoryObject->csh_stage = 3;
                $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
                $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
                $this->addCustomerStageHistory($stageHistoryObject);
                
            }
            $result =  resultObject::withData(1,"ok");
            return $this->returnAnswer($result);
        }


        if($curr_stage != 2){//Customer fell through checks
            try{
                $this->updateStageHistoryForCustomer_DB($customerObject);
            }
            catch(Exception $e){
                $result =  resultObject::withData(0,"cant_update_stage");
                return $this->returnAnswer($result);
            }

            $stageHistoryObject = new customerStageHistoryObject();
            $stageHistoryObject->csh_biz_id = $customerObject->cust_biz_id;
            $stageHistoryObject->csh_cust_id = $customerObject->cust_id;
            $stageHistoryObject->csh_stage = 2;
            $stageHistoryObject->csh_start_date = $customerObject->cust_reg_date;
            $stageHistoryObject->csh_next_processing = utilityManager::getSQLDateFormatWithInterval(utilityManager::getSQLDateFormatTimeCurrentTimestamp(),$periodDays,"day");
            $this->addCustomerStageHistory($stageHistoryObject);
            
        }
        $result =  resultObject::withData(1,"ok");
        return $this->returnAnswer($result);
    } 
    
    /* Customer badges */

    /**
     * Add badge to customer
     * @param mixed $cust_id 
     * @param mixed $type 
     * @return resultObject
     */
    public function addBadgeToCustomer($cust_id,$type){
        
        $sql = "INSERT INTO tbl_customer_badge_tracker SET
                    cbt_cust_id = $cust_id,
                    cbt_type = '$type'";

        $row_id = $this->db->execute($sql);

        if($row_id > 0){
            return resultObject::withData(1,"",$row_id);
        }else{
            return resultObject::withData(0,"cant_add_bgadge");
        }
    }

    /* Points shop redeem */
    public function deletePointsshopRedeemEntryByBenefitID(customerObject $customer,$benefitID){
        try{
            $this->deletePointsshopRedeemEntryByBenefitIDDB($customer,$benefitID);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['benefitID'] = $benefitID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMER - PRIVATE          */
    /************************************* */

    /* Customer appointments */

    private function getCustomerUpcomingAppointmentsDB(customerObject $customer){
        $start_date = date( "d/m/Y", mktime(0, 0, 0));

        $sql = "SELECT * FROM(
                    SELECT em_id as appoint_id,em_start start_time,'meeting' as type FROM tbl_employee_meeting
                        WHERE em_end_date >= STR_TO_DATE('$start_date','%d/%m/%Y')
                        AND em_cust_id = {$customer->cust_id}
                        AND em_biz_id = {$customer->cust_biz_id}
                    UNION
                    SELECT cdc_id as appoint_id,concat(ccd_start_date,' ',startTable.hour_start,':00') start_time,'class' as type FROM tbl_calendar_class_dates, 
                        (select * from calendar_hours) startTable,
                        (select * from calendar_hours) endTable,
					    tbl_class_date_customers
                        WHERE ccd_class_id=cdc_class_id									
					    AND cdc_class_date_id = ccd_id
					    AND cdc_customer_id = {$customer->cust_id}
                        AND startTable.hour_id = ccd_start_time
                        AND endTable.hour_id = ccd_end_time
                        AND concat(ccd_start_date,' ',startTable.hour_start) >= STR_TO_DATE('$start_date','%d/%m/%Y')
                ) t1 order by start_time";

        return $this->db->getTable($sql);
    }

    /* Customer benefits */

    private function getPersonalZoneBenefitFromBenefit(customerBenefitObject $benefit){
        $pzBenefit = null;
        switch($benefit->cb_type){
            case "loyalty":
                $benefitData = $this->getPZBenefitRowsForCustomerLoyaltyBenefit($benefit);
                $pzBenefit = benefitPZObject::withData($benefitData);
                break;
            case "coupon":                        
                $benefitData = $this->getPZBenefitRowsForCustomerCouponBenefit($benefit);                
                $pzBenefit = benefitPZObject::withData($benefitData);                        
                break;
            case "personal":
                $benefitData = $this->getPZBenefitRowsForCustomerPersonalBenefit($benefit);
                $pzBenefit = benefitPZObject::withData($benefitData);  
                break;
            case "points_redeem":
            case "scratch":
            case "membership":
                $benefitData = $this->getPZBenefitRowsForCustomerGeneralBenefit($benefit);
                $pzBenefit = benefitPZObject::withData($benefitData);  
                break;
        }

        return $pzBenefit;
    }

    private function getPZBenefitRowsForCustomerLoyaltyBenefit(customerBenefitObject $benefit){
        $data = array();
        $dataRow = $this->db->getRow("select * 
                                        from tbl_customer_card,tbl_mod_data6
                                        where customer_card_program_id=md_row_id
                                        and customer_card_id={$benefit->cb_external_id}");
        
        $data['card_id']=$benefit->cb_id;
        $data['benefit_type']="loyalty";
        $data['bizID']=$benefit->cb_biz_id;
        $data['image']=($dataRow["md_pic"] == null) ? "transparent.png" : $dataRow["md_pic"];
        $data['header']=$dataRow["md_head"];
        $data['description']=$dataRow["md_desc"];
        $data['benefit_date'] = $benefit->cb_date;
        $data['program_type']=$dataRow["md_program_type"];
        $data['stamp_type']=$dataRow["md_stamp_type"];
        $data['stamp_number']=$dataRow["md_stamp_number"];

        $data['reward_type']=$dataRow["md_reward_type"] != "" ? $dataRow["md_reward_type"] : $dataRow["md_stamp_type"];
        
        $data['reward_number']=$dataRow["md_reward_number"];
        $data['valid_from']=($dataRow["md_valid_from"] == null) ? "" : $dataRow["md_valid_from"];
        $data['valid_to']=($dataRow["md_valid_to"] == null) ? "" : $dataRow["md_valid_to"];
        $data['disclaimer']=$dataRow["md_disclaimer"];
        $data['customer_card_redeemed']=$benefit->cb_redeemed;
        $data['customer_card_redeemed_date']=($benefit->cb_redeemed_date == null) ? "" : $benefit->cb_redeemed_date;
        $data['customer_card_code']=$benefit->cb_code;
        $data['customer_card_id']=($dataRow["customer_card_id"] == null) ? 0 : $dataRow["customer_card_id"];
        $data['stamps']=($dataRow["customer_card_stamp_no"] == null) ? 0 : $dataRow["customer_card_stamp_no"];
        $data['recordID'] = $dataRow['md_row_id'];

        $stamp_items = "";
        if($dataRow["md_stamp_labels"] != ''){
            $items = explode(',',$dataRow["md_stamp_labels"]);
            foreach($items as $item){
                $stamp_items .= "\n- ".$item;
            }
            
        }

        $reward_items= "";
        if($dataRow["md_reward_labels"] != ''){
            
            $items = explode(',',$dataRow["md_reward_labels"]);
            foreach($items as $item){
                $reward_items .= "\n- ".$item;
            }
        }

        $data['stamp_items']=trim($stamp_items);
        $data['reward_items']=trim($reward_items);
        
        $rewardsArray = array();
        if($benefit->cb_redeemed == 0){
            switch ($dataRow["md_program_type"]){
                case "plus":
                    if($dataRow["md_stamp_type"] != "other"){
                        $rewards = explode(',',$dataRow["md_stamp_entry"]);
                        foreach ($rewards as $reward)
                        {
                            $rewardObject = array();
                            $rewardObject["reward_id"] = $dataRow["customer_card_id"];
                            $rewardObject["reward_type"] = "item";
                            $rewardObject["reward_value"] = $dataRow["md_reward_number"];
                            $rewardObject["redeemed_code"] = $benefit->cb_code;
                            $rewardObject["item_type"] = $data['reward_type']; 
                            $rewardObject["item_type_enum"] = utilityManager::getItemTypeIntFromItemTypeString($data['reward_type']);
                            $rewardObject["item_id"] = $reward;
                            array_push($rewardsArray,$rewardObject);
                        }
                    }
                    
                    break;
                case "discount":
                    if($dataRow["md_reward_type"] != "other"){
                        $rewards = ($dataRow["md_reward_entry"] != "") ? explode(',',$dataRow["md_reward_entry"]) : explode(',',$dataRow["md_stamp_entry"]);
                        foreach ($rewards as $reward)
                        {
                            $rewardObject = array();
                            $rewardObject["reward_id"] = $dataRow["customer_card_id"];
                            $rewardObject["reward_type"] = $dataRow["md_program_type"] == "gift" ? "item" : "discount";
                            $rewardObject["reward_value"] = $dataRow["md_reward_number"];
                            $rewardObject["redeemed_code"] = $benefit->cb_code;
                            $rewardObject["item_type"] = $data['reward_type'];     
                            $rewardObject["item_type_enum"] = utilityManager::getItemTypeIntFromItemTypeString($data['reward_type']);
                            $rewardObject["item_id"] = $reward;
                            array_push($rewardsArray,$rewardObject);
                        }

                    }
                    break;
                case "gift":
                    if($dataRow["md_reward_type"] != "other"){
                        $rewards = explode(',',$dataRow["md_reward_entry"]);
                        foreach ($rewards as $reward)
                        {
                            $rewardObject = array();
                            $rewardObject["reward_id"] = $dataRow["customer_card_id"];
                            $rewardObject["reward_type"] = "item";
                            $rewardObject["reward_value"] = $dataRow["md_reward_number"];
                            $rewardObject["redeemed_code"] = $benefit->cb_code;
                            $rewardObject["item_type"] = $data['reward_type'];  
                            $rewardObject["item_type_enum"] = utilityManager::getItemTypeIntFromItemTypeString($data['reward_type']);
                            $rewardObject["item_id"] = $reward;
                            array_push($rewardsArray,$rewardObject);
                        }
                    }
                    break;
            }
        }
        $data['rewards_list'] = $rewardsArray;
        if(count($rewardsArray) == 0){
            $data['program_type']='other';
        }

        return $data;
    }

    private function getPZBenefitRowsForCustomerCouponBenefit(customerBenefitObject $benefit){
        $data = array();
        $dataRow = $this->db->getRow("select * 
                                                        from tbl_cust_transaction_items,tbl_mod_data26
                                                        where tri_itm_row_id=md_row_id
                                                        and tri_id={$benefit->cb_external_id}");
        
        $data['card_id']=$benefit->cb_id;
        $data['benefit_type']="coupon";
        $data['bizID']=$benefit->cb_biz_id;
        $data['benefit_date'] = $benefit->cb_date;
        $data['image']=($dataRow["md_icon"] == null) ? "transparent.png" : $dataRow["md_icon"];
        $data['header']=$dataRow["md_head"];
        $data['description']=$dataRow["md_desc"];
        $data['recordID'] = $dataRow["tri_itm_row_id"];
        $rewardNumber = 0;
        $coupon_type = 'other';
        $rewardType = "";
        $stampNumber = 0;
        $data['coupon_get'] = 0;
        switch($dataRow["md_info5"]){
            case 1:
                $coupon_type = 'cart_discount';
                $rewardType = $dataRow["md_int1"] == 116 ? 'percent' : 'amount';
                $rewardNumber = $dataRow["md_price"];
                $stampNumber = isset($dataRow["md_int2"]) ? $dataRow["md_int2"] : 0;
                break;
            case 2:
                $coupon_type = 'bogo';
                $rewardType = 'product';
                $rewardNumber = $dataRow["md_int3"];
                $stampNumber =  $dataRow["md_int2"];
                break;
            default:
                break;
        }
        $data['program_type']=$coupon_type;
        $data['stamp_type']=$dataRow["md_item_type"];
        $data['stamp_number']=$stampNumber;
        $data['reward_type']=$rewardType;
        $data['reward_number']=$rewardNumber;
        $data['valid_from']=($dataRow["md_date1"] == null) ? "" : $dataRow["md_date1"];
        $data['valid_to']=($dataRow["md_date2"] == null) ? "" : $dataRow["md_date2"];
        $data['disclaimer']=$dataRow["md_info2"];
        $data['customer_card_redeemed']=$dataRow["tri_redeem_date"] == null ? 0 : 1;
        $data['customer_card_redeemed_date']=($dataRow["tri_redeem_date"] == null) ? "" : $dataRow["tri_redeem_date"];
        $data['customer_card_code']=$benefit->cb_code;
        $data['customer_card_id']=0;
        $data['stamps']=0;
        $rewards = array();
        if($coupon_type == 'bogo' && $dataRow["md_item_type"] != "other"){
            $sql = "SELECT * FROM tbl_coupon_items 
                                    WHERE ci_coupon_id = {$dataRow['md_row_id']}
                                    AND ci_type = '{$dataRow["md_item_type"]}'";
            $rewards = $this->db->getTable($sql);
        }
        $rewardsArray = array();
        if($dataRow["tri_redeem_date"] == null){
            foreach ($rewards as $reward)
            {
                $rewardObject = array();
                $rewardObject["reward_id"] = $benefit->cb_id;
                $rewardObject["reward_type"] = $rewardType;
                $rewardObject["reward_value"] = $this->getbenefitRewardItemValue($reward["ci_type"],$reward['ci_item_id']);
                
                $rewardObject["redeemed_code"] = $dataRow["tri_redeem_code"];
                $rewardObject["item_type"] = $reward["ci_type"];
                $rewardObject["item_type_enum"] = utilityManager::getItemTypeIntFromItemTypeString($reward["ci_type"]);
                $rewardObject["item_id"] = $reward['ci_item_id'];
                array_push($rewardsArray,$rewardObject);
            }
        }
        $data['rewards_list'] = $rewardsArray;
        if($data['reward_type'] == 'other' || ($data['program_type'] == 'bogo' && count($rewardsArray) == 0)){//no rewards means that this coupon is 'other' or old version
            $data['program_type'] = 'custom';
        }

        $couponManager = new couponsManager();
        $couponResult = $couponManager->getCouponByCouponID($dataRow['md_row_id']);

        $data['coupon_data'] = $couponResult->data;

        
        return $data;
    }

    private function getPZBenefitRowsForCustomerPersonalBenefit(customerBenefitObject $benefit){
        $data = array();
        $data['recordID'] = 0;
        $data['card_id']=$benefit->cb_id;
        $data['benefit_date'] = $benefit->cb_date;
        $data['benefit_type']="personal";
        $data['bizID']=$benefit->cb_biz_id;
        $data['image']=IMAGE_STORAGE_PATH."system/present.jpg";
        $data['header']=$benefit->cb_title;
        $data['description']=$benefit->cb_description;
        $data['external_id'] = 0;
        $data['program_type']="other";
        $data['stamp_type']="";
        $data['stamp_number']=0;
        $data['reward_type']="";
        $data['reward_number']=0;
        $data['valid_from']=isset($benefit->cb_valid_start) ? $benefit->cb_valid_start : '';
        $data['valid_to']=isset($benefit->cb_valid_end) ? $benefit->cb_valid_end : '';
        $data['disclaimer']="";
        $data['customer_card_redeemed']=$benefit->cb_redeemed;
        $data['customer_card_redeemed_date']= isset($benefit->cb_redeemed_date) ? $benefit->cb_redeemed_date : "";
        $data['customer_card_code']=$benefit->cb_code;
        $data['customer_card_id']=$benefit->cust_id;
        $data['stamps']=0;
        $data['rewards_list'] = array();

        return $data;
    }

    private function getPZBenefitRowsForCustomerGeneralBenefit(customerBenefitObject $benefit){
        $data = array();

        $id = $benefit->cb_external_id;
        $data['external_id'] = 0;
        if($benefit->cb_item_type != 'points'){
            $data['image'] = $benefit->cb_image;
            $data['title'] = $benefit->cb_title;
            $data['description'] = $benefit->cb_description;
            
        }
        else{
            $data['image'] = '';
            $data['title'] = $benefit->cb_value;
            $data['description'] = $benefit->cb_description;
            
        }
        
        
        $data['card_id']=$benefit->cb_id;
        $data['benefit_type']=$benefit->cb_type;
        $data['bizID']=$benefit->cb_biz_id;
        $data['image']= $data["image"] == "" ? "https://storage.googleapis.com/bobile/images/points_shop_default.jpg" : $data["image"];
        $data['header']=$data["title"];
        $data['description']=$data["description"];
        $data['benefit_date'] = $benefit->cb_date;

        $data['program_type']=$benefit->cb_item_type;
        $data['stamp_type']="";
        $data['stamp_number']=0;
        $data['reward_type']="other";
        $data['reward_number']=0;
        $data['valid_from']=($benefit->cb_valid_start == null) ? "" : $benefit->cb_valid_start;
        $data['valid_to']=($benefit->cb_valid_end == null) ? "" : $benefit->cb_valid_end;
        $data['disclaimer']="";
        $data['customer_card_redeemed']=$benefit->cb_redeemed;
        $data['customer_card_redeemed_date']=($benefit->cb_redeemed_date == null) ? "" : $benefit->cb_redeemed_date;
        $data['customer_card_code']=$benefit->cb_code;
        $data['customer_card_id']=$benefit->cust_id;
        $data['stamps']=0;
        $data['recordID'] = $benefit->cb_id;
        $rewardsArray = array();
        if($benefit->cb_redeemed == 0){
            switch ($benefit->cb_type){
                case "points_redeem":
                    $dataRow = $this->db->getRow("select * 
                                                        from tbl_points_grant
                                                        where pg_id={$benefit->cb_external_id}");

                    if($dataRow["pg_entity_type"] != "other"){
                        $rewardObject = array();
                        $rewardObject["reward_id"] = $benefit->cb_external_id;
                        $rewardObject["reward_type"] = "item";
                        $rewardObject["reward_value"] = "1";
                        $rewardObject["redeemed_code"] = $benefit->cb_code;
                        $rewardObject["item_type"] = $benefit->cb_item_type;                                        
                        $rewardObject["item_id"] = $benefit->cb_external_id;
                        $rewardObject["item_type_enum"] = utilityManager::getItemTypeIntFromItemTypeString($benefit->cb_item_type);
                        array_push($rewardsArray,$rewardObject);

                        $data['reward_type']=$benefit->cb_item_type;
                        $data['reward_number']=1;
                    }

                    
                    
                    break;
                case "scratch":
                case "membership":
                    if($benefit->cb_item_type == "product" || $benefit->cb_item_type == "service"){ 
                        $rewardObject = array();
                        $rewardObject["reward_id"] = $benefit->cb_type == "membership" ? $benefit->cb_id : $benefit->cb_scard_id;
                        $rewardObject["reward_type"] = "item";
                        $rewardObject["reward_value"] = "1";
                        $rewardObject["redeemed_code"] = $benefit->cb_code;
                        $rewardObject["item_type"] = $benefit->cb_item_type;                                        
                        $rewardObject["item_id"] = $benefit->cb_external_id;
                        $rewardObject["item_type_enum"] = utilityManager::getItemTypeIntFromItemTypeString($benefit->cb_item_type);
                        array_push($rewardsArray,$rewardObject);

                        $data['reward_type']=$benefit->cb_item_type;
                        $data['reward_number']=1;
                    }

                    

                    break;  
            }
        }
        $data['rewards_list'] = $rewardsArray;

        return $data;
    }

    private function getbenefitRewardItemValue($item_type,$itemID){
        $value = 0;
        switch($item_type){
            case 'product':
                $levelM = new levelDataManager(9);
                $product = $levelM->getLevelDataDirectByID($itemID);
                if($product->code == 1){
                    $value = $product->data['md_price'];
                }
                break;
            case 'service':
                $serviceResult = bookingManager::getServiceByID($itemID);
                if($serviceResult->code == 1){
                    $value = $serviceResult->data['bmt_price'];
                }
                break;
            case 'class':
                $classResult = bookingManager::getClassByID($itemID);
                if($classResult->code == 1){
                    $value = $classResult->data['calc_price'];
                }
                break;
        }

        return $value;
    }

    /* Customer Wishtlist */

    private function getCustomerWishListDB(customerObject $customer,$skip = 0,$take = 0){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_wish_list
                WHERE wl_biz_id = {$customer->cust_biz_id}
                AND wl_customer_id = {$customer->cust_id}
                ORDER BY wl_id
                $limit";

        return $this->db->getTable($sql);
    }

    private function getCustomerInvitedFriendsDB(customerObject $customer){
        return $this->db->getTable("select *
                                from tbl_customers
                                where cust_inviter={$customer->cust_id}
                                order by cust_inviter_points DESC");
    }

    private function getCustomerSubscriptionsDB(customerObject $customer,$skip = 0, $take = 0){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_customer_subscription,tbl_mod_data10
                WHERE csu_sub_id = md_row_id
                AND csu_cust_id = {$customer->cust_id}
                $limit";

        return $this->db->getTable($sql);
    }

    private function getCustomerPunchpassesDB(customerObject $customer,$skip = 0, $take = 0){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_customers_punchpass,tbl_mod_data11
                WHERE cpp_pass_id = md_row_id
                AND cpp_cust_id = {$customer->cust_id}
                $limit";

        return $this->db->getTable($sql);
    }

    private function getCustomerPushesDB(customerObject $customer,$lastID,$deviceID,$lastreminderid = null){
        $addPushLimits = "";
        $addReminderLimits = "";
        $masterFilter = "and cup_cust_id='$deviceID'";
        if (isset($lastreminderid)){
            $addPushLimits = "and pu_id > $lastID";
            $addReminderLimits = "and puh_id > $lastreminderid";
            $masterFilter = "and cup_customer_id='{$customer->cust_id}'";
        }

        $sql = "select * from (SELECT pu_id,pu_short_mess,pu_header,pu_img,cup_open_time,pu_sent,cup_create_time,pu_in_server,
                    pu_type,pu_external_id,pu_level,pu_mod_id,pu_view,pu_view_android,pu_based_location,'push' as source
                    FROM tbl_push,tbl_cust_push 
                    WHERE cup_push_id=pu_id
                    and pu_biz_id={$customer->cust_biz_id}
                    $masterFilter
                    and pu_based_location = 0
                    $addPushLimits
                    union
                    select puh_id pu_id,
                    puh_short_mess pu_short_mess,
                    '' pu_header,
                    '' pu_img,
                    puh_sent cup_open_time,
                    puh_sent pu_sent,
                    puh_created cup_create_time,
                    0 pu_in_server,
                    -1 pu_type,
                    0 pu_external_id,
                    1 pu_level,
                    0 pu_mod_id,
                    '' pu_view,
                    '' pu_view_android,
                    0 pu_based_location,'reminder' as source
                    from tbl_push_history where 
                            puh_type in ('Reminder','Geo','Benefit')
                            and puh_device_id='{$deviceID}' 
                            and puh_biz_id={$customer->cust_biz_id}
                            $addReminderLimits
                            )t1 order by pu_sent desc
                            ";

        return $this->db->getTable($sql);
    }

    private function getCustomerLimitedPushesDB(customerObject $customer,$deviceID,$skip = 0, $take = 20){
        $sql = "select * from (SELECT pu_id,pu_short_mess,pu_header,pu_img,cup_open_time,pu_sent,cup_create_time,pu_in_server,
                    pu_type,pu_external_id,pu_level,pu_mod_id,pu_view,pu_view_android,pu_based_location,'push' as source
                    FROM tbl_push,tbl_cust_push 
                    WHERE cup_push_id=pu_id
                    and pu_biz_id={$customer->cust_biz_id}
                    and cup_customer_id='{$customer->cust_id}'
                    and pu_based_location = 0
                    union
                    select puh_id pu_id,
                    puh_short_mess pu_short_mess,
                    '' pu_header,
                    '' pu_img,
                    puh_sent cup_open_time,
                    puh_sent pu_sent,
                    puh_created cup_create_time,
                    0 pu_in_server,
                    -1 pu_type,
                    0 pu_external_id,
                    1 pu_level,
                    0 pu_mod_id,
                    '' pu_view,
                    '' pu_view_android,
                    0 pu_based_location,'reminder' as source
                    from tbl_push_history where 
                            puh_type in ('Reminder','Geo','Benefit')
                            and puh_device_id='{$deviceID}' 
                            and puh_biz_id={$customer->cust_biz_id}
                            )t1 order by pu_sent desc
                            LIMIT $skip,$take";

        return $this->db->getTable($sql);
    }

    public function getCustomersByTypeDB($bizId,$type){

        $sql = "SELECT * FROM tbl_customers 
                    LEFT JOIN tbl_countries ON country_id=cust_country 
                    LEFT JOIN tbl_states ON state_id=cust_state 
                    WHERE cust_biz_id = $bizId 
                    AND cust_status='$type'";

        return $this->db->getTable($sql);
    }

    public function getSingleCustomerByIdForPartnersApiDB($customerId){

        $sql = "SELECT * FROM tbl_customers 
                    LEFT JOIN tbl_countries ON country_id=cust_country 
                    LEFT JOIN tbl_states ON state_id=cust_state 
                    WHERE cust_id = $customerId";

        return $this->db->getRow($sql);
    }

    /* points shop redeem */

    private function addPointshopRedeemEntry(customerObject $customer,pointsGrantObject $item, $benefitID){
        $sql = "INSERT INTO tbl_pointsshop_redeem SET
                    psr_biz_id = {$customer->cust_biz_id},
                    psr_cust_id = {$customer->cust_id},
                    psr_item_id = {$item->pg_id},
                    psr_benefit_id = $benefitID,
                    psr_cost = {$item->pg_cost}";

        return $this->db->execute($sql);
    }

    private function deletePointsshopRedeemEntryByBenefitIDDB(customerObject $customer,$benefitID){
        $sql = "DELETE FROM tbl_pointsshop_redeem 
                WHERE psr_benefit_id = $benefitID
                AND psr_cust_id = {$customer->cust_id}
                AND psr_biz_id = {$customer->cust_biz_id}";

        $this->db->execute($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERHISTORY - DB METHODS           */
    /************************************* */

    private function addCustomerHistoryDB(customerHistoryObject $obj){

        if (!isset($obj)){
            throw new Exception("customerHistoryObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_history SET 
                                        ch_cust_id = {$obj->ch_cust_id},
                                        ch_status = '".addslashes($obj->ch_status)."',
                                        ch_replace_text = '".addslashes($obj->ch_replace_text)."',
                                        ch_short_mess = '".addslashes($obj->ch_short_mess)."',
                                        ch_auto = {$obj->ch_auto},
                                        ch_action_id = {$obj->ch_action_id},
                                        ch_device_id = '".addslashes($obj->ch_device_id)."',
                                        ch_external_type = '".$obj->ch_external_type."',
                                        ch_external_id = {$obj->ch_external_id}
                                                 ");
        return $newId;
    }

    private function loadCustomerHistoryFromDB($customerHistoryID){

        if (!is_numeric($customerHistoryID) || $customerHistoryID <= 0){
            throw new Exception("Illegal value customerHistoryID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_history WHERE ch_id = $customerHistoryID");
    }

    private function upateCustomerHistoryDB(customerHistoryObject $obj){

        if (!isset($obj->ch_id) || !is_numeric($obj->ch_id) || $obj->ch_id <= 0){
            throw new Exception("customerHistoryObject value must be provided");             
        }

        $ch_createdDate = isset($obj->ch_created) ? "'".$obj->ch_created."'" : "null";

        $this->db->execute("UPDATE tbl_cust_history SET 
                                ch_cust_id = {$obj->ch_cust_id},
                                ch_created = $ch_createdDate,
                                ch_status = '".addslashes($obj->ch_status)."',
                                ch_replace_text = '".addslashes($obj->ch_replace_text)."',
                                ch_short_mess = '".addslashes($obj->ch_short_mess)."',
                                ch_auto = {$obj->ch_auto},
                                ch_action_id = {$obj->ch_action_id},
                                ch_device_id = '".addslashes($obj->ch_device_id)."'
                                WHERE ch_id = {$obj->ch_id} 
                                         ");
    }

    private function deleteCustomerHistoryByIdDB($customerHistoryID){

        if (!is_numeric($customerHistoryID) || $customerHistoryID <= 0){
            throw new Exception("Illegal value customerHistoryID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_history WHERE ch_id = $customerHistoryID");
    }

    private function getCustomerHistoryEntryRowsByCustID($custID,$skip = 0, $take = 40){
        $sql = "SELECT * FROM tbl_cust_history
                WHERE ch_cust_id = $custID
                ORDER BY ch_created DESC
                LIMIT $skip,$take";

        return $this->db->getTable($sql);
    }

    /* Customer membership */
    
    public function getCustomerMembershipByCustID($custID){
        try{
            //Get the customer
            $customerObjResult = $this->getCustomerWithID($custID);

            
            if($customerObjResult->code == 0){//no customer was found
                $result = resultObject::withData(0,'No customer found');
                return $result;
            }

            $customerObj = $customerObjResult->data;
            
            $memberShipManager = new membershipsManager();

            //Get the customer's current membership
            $custMembershipResult = $memberShipManager->getCustomerMembershipByMembershipID($customerObj->cust_membership_id);
            
            if($custMembershipResult->code == 1){
                $custMembershipObj = $custMembershipResult->data;
            }
            else{//Customer does not yet have a membership level
                $custMembershipObj = new customerMembershipObject();
            }

            //Get the customer's next level
            $nextLevel = $customerObj->cust_membership_level + 1;
            $nextMembershipResult = $memberShipManager->getBizMembershipByLevelAndBizID($customerObj->cust_biz_id,$nextLevel);

            if($nextMembershipResult->code == 1){
                //Customer has a next level - update the customer's membership object
                $custMembershipObj->setNextMembershipLevelData($nextMembershipResult->data);
                $points_to_level = $nextMembershipResult->data->bm_min_points - $customerObj->cust_total_points;// How many points the customer has left before he advances a level
                $custMembershipObj->setPointsToNextlevel($points_to_level);
            }
           
            $result = resultObject::withData(1,'',$custMembershipObj);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Check if customer's membership can be upgraded (has minimum amount of points required)
     * If yes, change membership of customer
     * Note: the memberships that are checked are only memberships that have a higher level than the customer's current level
     * @param mixed $cust_id 
     */
    public function checkCustomerMembership(customerObject $custObject, $sendPush = true){
        
        $memberManager = new membershipsManager();
        $result = $memberManager->getBizMembershipByLevelAndBizID($custObject->cust_biz_id,$custObject->cust_membership_level + 1);

        if($result->code == 1 && $result->data->bm_id > 0){
            $membershipObject = $result->data;
            
            if($membershipObject->bm_min_points <= $custObject->cust_total_points){
                $memberManager->setCustomerMemberShip($membershipObject,$custObject);
                eventManager::actionTrigger(enumCustomerActions::upgradedMembership,$custObject->cust_id,'upgraded_membership');
                customerManager::sendAsyncCustomerDataUpdateToDevice('membership',$custObject->cust_id);
                if($sendPush){

                    $cust_socket_target = $custObject->cust_id.'_'.$custObject->cust_mobile_serial;
                    $params = array();
                    $params['membership_name'] = $membershipObject->bm_name;

                    $text = pushManager::getSystemAlertPushMessageText(16,$custObject->cust_biz_id,$params);
                    $message = new stdClass();
                    $message->text = $text;

                    $fallback = new pushParamsObject();
                    $fallback->biz_id = $custObject->cust_biz_id;
                    $fallback->cust_id = $custObject->cust_id;
                    $fallback->push_type = enumPushType::biz_bobileXMembership;
                    $fallback->message = $text;


                    wsocketManager::sendIM($message,$cust_socket_target,enumSocketType::membership,$fallback);                    
                }
            }
        }
    }

    /* Customer benefits - public */

    public function getCustomerBenefitsByCustID($custID,$skip = 0, $take = 40){
        
        try{
            $benefitsList = $this->getCustomerBenefitEntryRowsByCustID($custID,$skip,$take);

            $benefits = array();

            foreach ($benefitsList as $entry)
            {
            	$benefits[] = customerBenefitObject::withData($entry);
            }            

            
            $result = resultObject::withData(1,'',$benefits);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }    
    
    /* Customer benefits - private */    

    private function getCustomerBenefitEntryRowsByCustID($custID,$skip = 0, $take = 40){
        
        
        if(!is_numeric($custID) || $custID <= 0){
            throw new Exception("Illegal customer ID");
        }

        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_cust_benefits
                WHERE cb_cust_id = $custID
                and cb_item_type <> 'points'
                order by cb_id desc
                $limit";

        return $this->db->getTable($sql);
    }    

    private function getCustomerBenefitsForItem_DB(customerObject $customer,$itemType,$itemID){
        $sql = "SELECT * FROM tbl_cust_benefits
                WHERE cb_cust_id = {$customer->cust_id}
                AND cb_item_type = '$itemType'
                AND cb_external_id = $itemID
                AND cb_redeemed = 0
                AND cb_valid_start < NOW()
                AND (cb_valid_end IS NULL OR NOW() < cb_valid_end)";

        return $this->db->getTable($sql);
    }

    private function getCustomerCouponsForItem_DB(customerObject $customer,$itemType,$itemID){
        $sql = "SELECT * FROM tbl_cust_benefits,tbl_cust_transaction_items,tbl_coupon_items
                WHERE tri_id = cb_external_id
                AND tri_itm_row_id = ci_coupon_id
                AND cb_cust_id = {$customer->cust_id}
                AND cb_type = 'coupon'
                AND ci_type = '$itemType'
                AND ci_item_id = $itemID
                AND cb_redeemed = 0
                AND cb_valid_start < NOW()
                AND (cb_valid_end IS NULL OR NOW() < cb_valid_end)";

        return $this->db->getTable($sql);
    }

    private function getCustomerCouponsForCart_DB(customerObject $customer){
        $sql = "SELECT * FROM tbl_cust_benefits,tbl_cust_transaction_items,tbl_mod_data26
                WHERE tri_id = cb_external_id
                AND tri_itm_row_id = md_row_id
                AND cb_cust_id = {$customer->cust_id}
                AND md_info5 = 1
                AND cb_type = 'coupon'
                AND cb_redeemed = 0
                AND cb_valid_start < NOW()
                AND (cb_valid_end IS NULL OR NOW() < cb_valid_end)";

        return $this->db->getTable($sql);
    }

    private function getCustomerUnredeemedCount($cust_id,$period = ""){

        $periodSQL = "";
        if($period != ""){
            $periodSQL = " AND cb_date >= (NOW() - INTERVAL $period DAY) ";
        }

        $sql = "SELECT COUNT(*) FROM tbl_cust_benefits
                WHERE cb_cust_id = $cust_id
                $periodSQL
                AND cb_redeemed = 0";

        $result = $this->db->getVal($sql);

        return $result;
    }

    private function getCustomerRedeemedCount($cust_id,$period){
        $sql = "SELECT COUNT(*) FROM tbl_cust_benefits
                WHERE cb_cust_id = $cust_id
                AND cb_redeemed_date >= (NOW() - INTERVAL $period DAY)
                AND cb_redeemed = 1";

        $result = $this->db->getVal($sql);

        return $result;
    }

    private function getCustomerTransactionsCount($cust_id,$period){

        $custOrderManager = new customerOrderManager();
        $custBookingManager = new bookingManager();

        $result = $custOrderManager->getCustTransactionsOrderCountForLastXDays($cust_id,$period);
        if($result->code == 1){
            $transCount = $result->data;
        }else{
            return resultObject::withData(0,"cant_retrieve_counts");
        }

        $result = $custBookingManager->deleteEmployeeMeetingById($cust_id,$period);
        if($result->code == 1){
            $transCount += $result->data;
        }else{
            return resultObject::withData(0,"cant_retrieve_counts");
        }
        
        if($transCount > 0){
            
            $result = $custOrderManager->getCustTransactionsOrderCount($cust_id);
            if($result->code == 1){
                $transCount = $result->data;
            }else{
                return resultObject::withData(0,"cant_retrieve_counts");
            }

            $result = $custOrderManager->getCustMeetingCount($cust_id);
            if($result->code == 1){
                $transCount += $result->data;
            }else{
                return resultObject::withData(0,"cant_retrieve_counts");
            }
        }

        return resultObject::withData(1,"",$transCount);;
    }     
    

    /************************************* */
    /*   BASIC CUSTOMERDEVICE - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerDeviceObject to DB
     * Return Data = new customerDeviceObject ID
     * @param customerDeviceObject $customerDeviceObj 
     * @return resultObject
     */
    public function addCustomerDevice(customerDeviceObject $customerDeviceObj){       
        try{
            $newId = $this->addCustomerDeviceDB($customerDeviceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerDeviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerDevice from DB for provided ID
     * * Return Data = customerDeviceObject
     * @param int $customerDeviceId 
     * @return resultObject
     */
    public function getCustomerDeviceByID($customerDeviceId){
        
        try {
            $customerDeviceData = $this->loadCustomerDeviceFromDB($customerDeviceId);
            
            $customerDeviceObj = customerDeviceObject::withData($customerDeviceData);
            $result = resultObject::withData(1,'',$customerDeviceObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerDeviceId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerDevice in DB
     * @param customerDeviceObject $customerDeviceObj 
     * @return resultObject
     */
    public function updateCustomerDevice(customerDeviceObject $customerDeviceObj){        
        try{
            $this->upateCustomerDeviceDB($customerDeviceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerDeviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerDevice from DB
     * @param int $customerDeviceID 
     * @return resultObject
     */
    public function deleteCustomerDeviceById($customerDeviceID){
        try{
            $this->deleteCustomerDeviceByIdDB($customerDeviceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerDeviceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERDEVICE - DB METHODS           */
    /************************************* */

    private function addCustomerDeviceDB(customerDeviceObject $obj){

        if (!isset($obj)){
            throw new Exception("customerDeviceObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT IGNORE INTO tbl_cust_device SET 
                                        cd_cust_id = {$obj->cd_cust_id},
                                        cd_biz_id = {$obj->cd_biz_id},
                                        cd_device = '".addslashes($obj->cd_device)."',
                                        cd_app_id = {$obj->cd_app_id},
                                        cd_market_id = {$obj->cd_market_id}
                                                 ");
        return $newId;
    }

    private function loadCustomerDeviceFromDB($customerDeviceID){

        if (!is_numeric($customerDeviceID) || $customerDeviceID <= 0){
            throw new Exception("Illegal value customerDeviceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_device WHERE cd_id = $customerDeviceID");
    }

    private function upateCustomerDeviceDB(customerDeviceObject $obj){

        if (!isset($obj->cd_id) || !is_numeric($obj->cd_id) || $obj->cd_id <= 0){
            throw new Exception("customerDeviceObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_cust_device SET 
                                cd_cust_id = {$obj->cd_cust_id},
                                cd_biz_id = {$obj->cd_biz_id},
                                cd_device = '".addslashes($obj->cd_device)."',
                                cd_app_id = {$obj->cd_app_id},
                                cd_market_id = {$obj->cd_market_id}
                                WHERE cd_id = {$obj->cd_id} 
                                         ");
    }

    private function deleteCustomerDeviceByIdDB($customerDeviceID){

        if (!is_numeric($customerDeviceID) || $customerDeviceID <= 0){
            throw new Exception("Illegal value customerDeviceID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_device WHERE cd_id = $customerDeviceID");
    }

    /************************************* */
    /*   BASIC CUSTOMERPOINTSHISTORY - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerPointsHistoryObject to DB
     * Return Data = new customerPointsHistoryObject ID
     * @param customerPointsHistoryObject $customerPointsHistoryObj 
     * @return resultObject
     */
    public function addCustomerPointsHistory(customerPointsHistoryObject $customerPointsHistoryObj){       
        try{
            $newId = $this->addCustomerPointsHistoryDB($customerPointsHistoryObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPointsHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerPointsHistory from DB for provided ID
     * * Return Data = customerPointsHistoryObject
     * @param int $customerPointsHistoryId 
     * @return resultObject
     */
    public function getCustomerPointsHistoryByID($customerPointsHistoryId){
        
        try {
            $customerPointsHistoryData = $this->loadCustomerPointsHistoryFromDB($customerPointsHistoryId);
            
            $customerPointsHistoryObj = customerPointsHistoryObject::withData($customerPointsHistoryData);
            $result = resultObject::withData(1,'',$customerPointsHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPointsHistoryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerPointsHistory in DB
     * @param customerPointsHistoryObject $customerPointsHistoryObj 
     * @return resultObject
     */
    public function updateCustomerPointsHistory(customerPointsHistoryObject $customerPointsHistoryObj){        
        try{
            $this->upateCustomerPointsHistoryDB($customerPointsHistoryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPointsHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerPointsHistory from DB
     * @param int $customerPointsHistoryID 
     * @return resultObject
     */
    public function deleteCustomerPointsHistoryById($customerPointsHistoryID){
        try{
            $this->deleteCustomerPointsHistoryByIdDB($customerPointsHistoryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPointsHistoryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Grant customer points for event
     * @param mixed $customer_id 
     * @param mixed $event_id 
     * @param mixed $points 
     * @return resultObject
     */
    public function grantEventPoints(customerObject $custObject,$event_id,$points){      
        if($points <= 0){
            return resultObject::withData(0,'zero_points');
        }

        $custPoints = new customerPointsHistoryObject();
        $custPoints->cph_biz_id = $custObject->cust_biz_id;
        $custPoints->cph_cust_id = $custObject->cust_id;
        $custPoints->cph_source = 'event';
        $custPoints->cph_value = $points;
        $custPoints->cph_reason_text = "";
        $custPoints->cph_external_id =$event_id;

        return $this->grantCustomerPoints($custObject,$custPoints);
    }

    /**
     * Grants customer points. Add row to cust_point_history
     * @param mixed $cust_id 
     * @param mixed $source 
     * @param mixed $points 
     * @param mixed $ext_id 
     * @return resultObject
     */
    public function grantCustomerPoints(customerObject $custObject,customerPointsHistoryObject $pointsObject){

        $result = $this->addCustomerPointsHistory($pointsObject);

        if($result->code == 1 &&  $result->data > 0){
            if($pointsObject->cph_value > 0 && $pointsObject->cph_source != 'points_redeem'){

                if($custObject->cust_id > 0){
                    $custObject->cust_total_points = $custObject->cust_total_points + $pointsObject->cph_value;
                    $custObject->cust_current_points = $custObject->cust_current_points + $pointsObject->cph_value;
                    
                    $this->updateCustomer($custObject);
                    customerManager::sendAsyncCustomerDataUpdateToDevice('points',$custObject->cust_id);
                    customerManager::sendAsyncCustomerDataUpdateToDevice('membership',$custObject->cust_id);
                    $this->checkCustomerMembership($custObject);
                }
            }

            return resultObject::withData(1,"ok",$result->data);
        }
        else{
            return resultObject::withData(0,"failed_add_entry");
        }
    }

    /**
     * Give points to inviter for friend registration if needed
     * @param customerObject $custObject 
     */
    public function givePointsToCustomerForFriendsRegistration(customerObject $custObject){  
        
        $eventManager = new eventManager();
        $eventRow = $eventManager->getEventByBizAndId($custObject->cust_id,34)["data"];

        if(isset($eventRow['bevent_points']) && $eventRow['bevent_points'] > 0){
            // give points to inviter
            $this->grantInviterRegisterPoints($custObject->cust_id,$eventRow['bevent_points'],$eventRow['bevent_message']);
        }
    }

    /**
     * Grants points for a friend (that was invited) that registered
     * @param mixed $friend_id - the id of the invitee
     * @param mixed $amount - amount of points granted
     * @param mixed $text - text from the biz definitions
     * @return resultObject
     */
    public function grantInviterRegisterPoints($friend_id,$amount,$text = ''){

        $inviter_id = $this->getInviter($friend_id);

        if($inviter_id == 0){
            return resultObject::withData(0,"no_inviter_found");
        }
        
        $result = $this->getCustomerWithID($inviter_id);

        if($result->code == 1 && $result->data->cust_status == 'member'){

            $inviterCustObject = $result->data;
            $pointsHistoryObject = new customerPointsHistoryObject();
            $pointsHistoryObject->cph_source = 'friend';
            $pointsHistoryObject->cph_value = $amount;
            $pointsHistoryObject->cph_external_id = $friend_id;
            $pointsHistoryObject->cph_reason_text = $text;

            $result = $this->grantCustomerPoints($inviterCustObject,$pointsHistoryObject);

            if($result->code == 1){

                $inviterCustObject->cust_inviter_points = $inviterCustObject->cust_inviter_points + $amount;
                $this->updateCustomer($inviterCustObject);

                $cust_socket_target = $inviterCustObject->getSocketIdentifier();
                
                $message = new stdClass();
                $message->amount = $amount;
                $message->text = $text;

                $fallback = new pushParamsObject();
                $fallback->biz_id = $inviterCustObject->cust_biz_id;
                $fallback->cust_id = $inviterCustObject->cust_id;
                $fallback->push_type = enumPushType::biz_bobileXPoints;
                $fallback->message = pushManager::getSystemAlertPushMessageText(enumPushType::biz_pointsFromFriend,$inviterCustObject->cust_biz_id);

                $fallback->message = str_replace("#received_points#",$amount,$fallback->message);
                $fallback->message = str_replace("#points_reason_text#",$text,$fallback->message);

                $fallback->extra_params["received_points"] = $amount;
                $fallback->extra_params["points_reason_text"] = $text;
                wsocketManager::sendIM($message,$cust_socket_target,enumSocketType::point,$fallback);

            }
            return resultObject::withData(1,"ok");
        }else{
            return resultObject::withData(0,"no_inviter_found");
        }
    }

    /**
     * Grant points to inviter when invitee (the friend) performed an action that grants the inviter points (usually - placed an order)
     * @param mixed $friend_id - the id of the invitee
     * @param mixed $amount - amount of points to grant
     * @return resultObject
     */
    public function grantInviterPointsExecute($friend_id,$amount){
        $inviter_id = $this->getInviter($friend_id);

        if($inviter_id == 0){
            return resultObject::withData(0,"no_inviter_found");
        }

        

        $result = $this->getCustomerWithID($inviter_id);

        if($result->code == 1 && $result->data->cust_status == 'member'){

            $inviterCustObject = $result->data;
            $pointsHistoryObject = new customerPointsHistoryObject();
            $pointsHistoryObject->cph_source = 'friend';
            $pointsHistoryObject->cph_value = $amount;
            $pointsHistoryObject->cph_external_id = $friend_id;
            

            $result = $this->grantCustomerPoints($inviterCustObject,$pointsHistoryObject);

            if($result->code == 1){
                $friendName = "";
                $friendObjectResult = $this->getCustomerWithID($friend_id);
                if($friendObjectResult->code == 1){
                    $friendName = $friendObjectResult->data->cust_first_name;
                }
                $inviterCustObject->cust_inviter_points = $inviterCustObject->cust_inviter_points + $amount;
                $this->updateCustomer($inviterCustObject);

                $cust_socket_target = $inviterCustObject->getSocketIdentifier();
                
                $message = new stdClass();
                $message->amount = $amount;

                $fallback = new pushParamsObject();
                $fallback->biz_id = $inviterCustObject->cust_biz_id;
                $fallback->cust_id = $inviterCustObject->cust_id;
                $fallback->push_type = enumPushType::biz_pointsFromFriend;
                $fallback->message = pushManager::getSystemAlertPushMessageText(enumPushType::biz_pointsFromFriend,$inviterCustObject->cust_biz_id);

                $fallback->message = str_replace("#received_points#",$amount,$fallback->message);
                $fallback->message = str_replace("#friend_name#",$friendName,$fallback->message);

                $fallback->extra_params["received_points"] = $amount;
                $fallback->extra_params["friend_name"] = $friendName;

                wsocketManager::sendIM($message,$cust_socket_target,enumSocketType::points_from_friend,$fallback);

            }
            return resultObject::withData(1,"ok");
        }else{
            return resultObject::withData(0,"no_inviter_found");
        }
    }

    public function getPointsHistoryForCustomer(customerObject $customer,$skip = 0,$take = 0){
        try{
            $entriesRows = $this->getPointsHistoryForCustomerDB($customer,$skip,$take);

            $entriesList = array();

            foreach ($entriesRows as $entry)
            {
            	$entriesList[] = customerPointsHistoryObject::withData($entry);
            }            

            return resultObject::withData(1,'',$entriesList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }


    /************************************* */
    /*   BASIC CUSTOMERPOINTSHISTORY - DB METHODS           */
    /************************************* */

    private function addCustomerPointsHistoryDB(customerPointsHistoryObject $obj){

        if (!isset($obj)){
            throw new Exception("customerPointsHistoryObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_points_history SET 
                                        cph_biz_id = {$obj->cph_biz_id},
                                        cph_cust_id = {$obj->cph_cust_id},
                                        cph_source = '{$obj->cph_source}',
                                        cph_value = {$obj->cph_value},
                                        cph_reason_text = '".addslashes($obj->cph_reason_text)."',
                                        cph_external_id = {$obj->cph_external_id}
                                                 ");
        return $newId;
    }

    private function loadCustomerPointsHistoryFromDB($customerPointsHistoryID){

        if (!is_numeric($customerPointsHistoryID) || $customerPointsHistoryID <= 0){
            throw new Exception("Illegal value customerPointsHistoryID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_points_history WHERE cph_id = $customerPointsHistoryID");
    }

    private function upateCustomerPointsHistoryDB(customerPointsHistoryObject $obj){

        if (!isset($obj->cph_id) || !is_numeric($obj->cph_id) || $obj->cph_id <= 0){
            throw new Exception("customerPointsHistoryObject value must be provided");             
        }

        $cph_dateDate = isset($obj->cph_date) ? "'".$obj->cph_date."'" : "null";

        $this->db->execute("UPDATE tbl_cust_points_history SET 
                                cph_biz_id = {$obj->cph_biz_id},
                                cph_cust_id = {$obj->cph_cust_id},
                                cph_date = $cph_dateDate,
                                cph_source = '{$obj->cph_source}',
                                cph_value = {$obj->cph_value},
                                cph_reason_text = '".addslashes($obj->cph_reason_text)."',
                                cph_external_id = {$obj->cph_external_id}
                                WHERE cph_id = {$obj->cph_id} 
                                         ");
    }

    private function deleteCustomerPointsHistoryByIdDB($customerPointsHistoryID){

        if (!is_numeric($customerPointsHistoryID) || $customerPointsHistoryID <= 0){
            throw new Exception("Illegal value customerPointsHistoryID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_points_history WHERE cph_id = $customerPointsHistoryID");
    }

    private function getInviter($customer_id){
        
        $sql = "SELECT cust_inviter,cust_status FROM tbl_customers WHERE cust_id = $customer_id";

        $data = $this->db->getRow($sql);

        $inviter_id = $data['cust_status'] == 'member' ? $data['cust_inviter'] : 0;

        return $inviter_id;
    }

    private function getPointsHistoryForCustomerDB(customerObject $customer,$skip = 0,$take = 0){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take"; 
        }

        $sql = "select * from(
                                        SELECT tbl_cust_points_history.*,cph_reason_text description FROM tbl_cust_points_history
                                            WHERE cph_source = 'personal_grant'
                                            AND cph_cust_id = {$customer->cust_id}
                                        UNION
                                        SELECT tbl_cust_points_history.*,IF(cph_reason_text = '','Completed on-boarding',cph_reason_text) description FROM tbl_cust_points_history
                                            WHERE cph_source = 'complete_onboarding'
                                            AND cph_cust_id = {$customer->cust_id}
                                        UNION
                                        SELECT tbl_cust_points_history.*,IF(cph_reason_text = '','Registered on web',cph_reason_text) description FROM tbl_cust_points_history
                                            WHERE cph_source = 'web_register'
                                            AND cph_cust_id = {$customer->cust_id}
                                        UNION
                                            select tbl_cust_points_history.*,IFNULL(bevent_message,event_def_message) description
                                             FROM tbl_cust_points_history INNER JOIN tbl_customer_events ON cph_external_id = cevent_id
                                                INNER JOIN tbl_events ON cevent_event_id = event_id
                                                LEFT JOIN tbl_biz_event ON cevent_event_id = bevent_event_id AND bevent_biz_id = {$customer->cust_biz_id} 
                                            WHERE cph_source = 'event'
                                            AND cph_cust_id = {$customer->cust_id}
                                        UNION
                                            select tbl_cust_points_history.*,IF(cph_reason_text = '','For a friend\'s action',cph_reason_text) description
                                             FROM tbl_cust_points_history 
                                            WHERE cph_source = 'friend'
                                            AND cph_cust_id = {$customer->cust_id}
										UNION
                                            select  tbl_cust_points_history.*,cscard_prize_text description FROM tbl_cust_points_history,tbl_customer_scratch 
                                            WHERE cph_source = 'scratch_win'
                                            AND cph_external_id = cscard_id
                                            AND cph_cust_id = {$customer->cust_id}
										UNION
                                            select   tbl_cust_points_history.*,'Scratch card consolation prize ' description FROM tbl_cust_points_history 
                                            WHERE cph_source = 'scratch_lose'
                                            AND cph_cust_id = {$customer->cust_id}
										UNION
                                            select  tbl_cust_points_history.*,label description FROM tbl_cust_points_history,(											
											SELECT tbl_cust_benefits.*,tbl_customers.*,cb_title as label FROM tbl_cust_benefits,tbl_customers WHERE cb_cust_id = cust_id AND cb_biz_id = {$customer->cust_biz_id} AND cb_type = 'points_redeem' AND (cb_item_type = 'product' OR cb_item_type = 'service')
											UNION
											SELECT tbl_cust_benefits.*,tbl_customers.*,cb_description as label FROM tbl_cust_benefits,tbl_customers WHERE cb_cust_id = cust_id AND cb_biz_id = {$customer->cust_biz_id} AND cb_type = 'points_redeem' AND cb_item_type = 'other'
											) t1
                                            WHERE cph_source = 'redeemd_points'
                                            AND cph_external_id = cb_id
                                            AND cph_cust_id = {$customer->cust_id}
                                            ) t2 
                                            order by cph_date DESC
                                            $limit";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERPUSHCAMPAIGNACTIVITY - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerPushCampaignActivityObject to DB
    * Return Data = new customerPushCampaignActivityObject ID
    * @param customerPushCampaignActivityObject $customerPushCampaignActivityObj 
    * @return resultObject
    */
    public function addCustomerPushCampaignActivity(customerPushCampaignActivityObject $customerPushCampaignActivityObj){       
            try{
                $newId = $this->addCustomerPushCampaignActivityDB($customerPushCampaignActivityObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPushCampaignActivityObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerPushCampaignActivity from DB for provided ID
    * * Return Data = customerPushCampaignActivityObject
    * @param int $customerPushCampaignActivityId 
    * @return resultObject
    */
    public function getCustomerPushCampaignActivityByID($customerPushCampaignActivityId){
        
            try {
                $customerPushCampaignActivityData = $this->loadCustomerPushCampaignActivityFromDB($customerPushCampaignActivityId);
            
                $customerPushCampaignActivityObj = customerPushCampaignActivityObject::withData($customerPushCampaignActivityData);
                $result = resultObject::withData(1,'',$customerPushCampaignActivityObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPushCampaignActivityId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update customerPushCampaignActivity in DB
    * @param customerPushCampaignActivityObject $customerPushCampaignActivityObj 
    * @return resultObject
    */
    public function updateCustomerPushCampaignActivity(customerPushCampaignActivityObject $customerPushCampaignActivityObj){        
            try{
                $this->upateCustomerPushCampaignActivityDB($customerPushCampaignActivityObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPushCampaignActivityObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerPushCampaignActivity from DB
    * @param int $customerPushCampaignActivityID 
    * @return resultObject
    */
    public function deleteCustomerPushCampaignActivityById($customerPushCampaignActivityID){
            try{
                $this->deleteCustomerPushCampaignActivityByIdDB($customerPushCampaignActivityID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPushCampaignActivityID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC CUSTOMERPUSHCAMPAIGNACTIVITY - DB METHODS           */
    /************************************* */

    private function addCustomerPushCampaignActivityDB(customerPushCampaignActivityObject $obj){

    if (!isset($obj)){
                throw new Exception("customerPushCampaignActivityObject value must be provided");             
            }

    $cup_open_timeDate = isset($obj->cup_open_time) ? "'".$obj->cup_open_time."'" : "null";

    $cup_delivery_timeDate = isset($obj->cup_delivery_time) ? "'".$obj->cup_delivery_time."'" : "null";

    $newId = $this->db->execute("INSERT INTO tbl_cust_push SET 
                                    cup_customer_id = {$obj->cup_customer_id},
                                    cup_cust_id = '".addslashes($obj->cup_cust_id)."',
                                    cup_push_id = {$obj->cup_push_id},
                                    cup_open_time = $cup_open_timeDate,
                                    cup_delivery_time = $cup_delivery_timeDate,
                                    cup_device_type = '{$obj->cup_device_type}'
                                             ");
             return $newId;
        }

    private function loadCustomerPushCampaignActivityFromDB($customerPushCampaignActivityID){

    if (!is_numeric($customerPushCampaignActivityID) || $customerPushCampaignActivityID <= 0){
                throw new Exception("Illegal value customerPushCampaignActivityID");             
            }

            return $this->db->getRow("SELECT * FROM tbl_cust_push WHERE cup_id = $customerPushCampaignActivityID");
        }

    private function upateCustomerPushCampaignActivityDB(customerPushCampaignActivityObject $obj){

    if (!isset($obj->cup_id) || !is_numeric($obj->cup_id) || $obj->cup_id <= 0){
                throw new Exception("customerPushCampaignActivityObject value must be provided");             
            }

    $cup_create_timeDate = isset($obj->cup_create_time) ? "'".$obj->cup_create_time."'" : "null";

    $cup_open_timeDate = isset($obj->cup_open_time) ? "'".$obj->cup_open_time."'" : "null";

    $cup_delivery_timeDate = isset($obj->cup_delivery_time) ? "'".$obj->cup_delivery_time."'" : "null";

    $this->db->execute("UPDATE tbl_cust_push SET 
                            cup_customer_id = {$obj->cup_customer_id},
                            cup_cust_id = '".addslashes($obj->cup_cust_id)."',
                            cup_push_id = {$obj->cup_push_id},
                            cup_create_time = $cup_create_timeDate,
                            cup_open_time = $cup_open_timeDate,
                            cup_delivery_time = $cup_delivery_timeDate,
                            cup_device_type = '{$obj->cup_device_type}'
                            WHERE cup_id = {$obj->cup_id} 
                                     ");
        }

    private function deleteCustomerPushCampaignActivityByIdDB($customerPushCampaignActivityID){

            if (!is_numeric($customerPushCampaignActivityID) || $customerPushCampaignActivityID <= 0){
                throw new Exception("Illegal value customerPushCampaignActivityID");             
            }

            $this->db->execute("DELETE FROM tbl_cust_push WHERE cup_id = $customerPushCampaignActivityID");
        }

    /************************************* */
    /*   BASIC CUSTOMERREVIEW - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerReviewObject to DB
    * Return Data = new customerReviewObject ID
    * @param customerReviewObject $customerReviewObj 
    * @return resultObject
    */
    public function addCustomerReview(customerReviewObject $customerReviewObj){       
            try{
                $newId = $this->addCustomerReviewDB($customerReviewObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerReviewObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerReview from DB for provided ID
    * * Return Data = customerReviewObject
    * @param int $customerReviewId 
    * @return resultObject
    */
    public function getCustomerReviewByID($customerReviewId){
        
            try {
                $customerReviewData = $this->loadCustomerReviewFromDB($customerReviewId);
            
                $customerReviewObj = customerReviewObject::withData($customerReviewData);
                $result = resultObject::withData(1,'',$customerReviewObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerReviewId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update customerReview in DB
    * @param customerReviewObject $customerReviewObj 
    * @return resultObject
    */
    public function updateCustomerReview(customerReviewObject $customerReviewObj){        
            try{
                $this->upateCustomerReviewDB($customerReviewObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerReviewObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerReview from DB
    * @param int $customerReviewID 
    * @return resultObject
    */
    public function deleteCustomerReviewById($customerReviewID){
            try{
                $this->deleteCustomerReviewByIdDB($customerReviewID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerReviewID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    public function getCustomerExistingReview(customerReviewObject $reviewObj){
        try{
            $reviewRow = $this->getCustomerExistingReviewDB($reviewObj);

            if(isset($reviewRow['cr_id'])){
                $review = customerReviewObject::withData($reviewRow);
                return resultObject::withData(1,'',$review);
            }
            else{
                return resultObject::withData(0);
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($review));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERREVIEW - DB METHODS           */
    /************************************* */

    private function addCustomerReviewDB(customerReviewObject $obj){

    if (!isset($obj)){
                throw new Exception("customerReviewObject value must be provided");             
            }

    $newId = $this->db->execute("INSERT INTO tbl_customer_reviews SET 
                                    cr_biz_id = {$obj->cr_biz_id},
                                    cr_cust_id = {$obj->cr_cust_id},
                                    cr_mod_id = {$obj->cr_mod_id},
                                    cr_row_id = {$obj->cr_row_id},
                                    cr_item_type = '{$obj->cr_item_type}',
                                    cr_item_id = {$obj->cr_item_id},
                                    cr_rating = {$obj->cr_rating},
                                    cr_text = '".addslashes($obj->cr_text)."'
                                    ON DUPLICATE KEY 
                                    UPDATE cr_text = '".addslashes($obj->cr_text)."',
                                           cr_rating = {$obj->cr_rating},
                                            cr_date = NOW()");
             return $newId;
        }

    private function loadCustomerReviewFromDB($customerReviewID){

    if (!is_numeric($customerReviewID) || $customerReviewID <= 0){
                throw new Exception("Illegal value customerReviewID");             
            }

            return $this->db->getRow("SELECT * FROM tbl_customer_reviews WHERE cr_id = $customerReviewID");
        }

    private function upateCustomerReviewDB(customerReviewObject $obj){

    if (!isset($obj->cr_id) || !is_numeric($obj->cr_id) || $obj->cr_id <= 0){
                throw new Exception("customerReviewObject value must be provided");             
            }

    $cr_dateDate = isset($obj->cr_date) ? "'".$obj->cr_date."'" : "null";

    $this->db->execute("UPDATE tbl_customer_reviews SET 
                            cr_biz_id = {$obj->cr_biz_id},
                            cr_cust_id = {$obj->cr_cust_id},
                            cr_date = $cr_dateDate,
                            cr_mod_id = {$obj->cr_mod_id},
                            cr_row_id = {$obj->cr_row_id},
                            cr_item_type = '{$obj->cr_item_type}',
                            cr_item_id = {$obj->cr_item_id},
                            cr_rating = {$obj->cr_rating},
                            cr_text = '".addslashes($obj->cr_text)."'
                            WHERE cr_id = {$obj->cr_id} 
                                     ");
        }

    private function deleteCustomerReviewByIdDB($customerReviewID){

            if (!is_numeric($customerReviewID) || $customerReviewID <= 0){
                throw new Exception("Illegal value customerReviewID");             
            }

            $this->db->execute("DELETE FROM tbl_customer_reviews WHERE cr_id = $customerReviewID");
        }

    /**
     * Returns existing customer review with fields from customerReview object
     * @param customerReviewObject $review 
     */
    private function getCustomerExistingReviewDB(customerReviewObject $review){
        if($review->cr_item_type == ""){
            $row = $this->db->getRow("select * FROM tbl_customer_reviews,tbl_customers 
                                            WHERE cr_cust_id = cust_id
                                            AND cr_biz_id = {$review->cr_biz_id}
                                            AND cr_mod_id = {$review->cr_mod_id}
                                            AND cr_item_type = ''
                                            AND cr_row_id = {$review->cr_row_id}
                                            AND cr_cust_id={$review->cr_cust_id}");
        }
        else{
            $row = $this->db->getRow("select * FROM tbl_customer_reviews,tbl_customers 
                                            WHERE cr_cust_id = cust_id
                                            AND cr_item_id = {$review->cr_item_id} 
                                            AND cr_biz_id = {$review->cr_biz_id}
                                            AND cr_item_type = '{$review->cr_item_type}'
                                            AND cr_mod_id = {$review->cr_mod_id}
                                            AND cr_cust_id={$review->cr_cust_id}");
        }

        return $row;
    }

    /************************************* */
    /*   BASIC CUSTOMERSCRATCHCARD - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerScratchCardObject to DB
    * Return Data = new customerScratchCardObject ID
    * @param customerScratchCardObject $customerScratchCardObj 
    * @return resultObject
    */
    public function addCustomerScratchCard(customerScratchCardObject $customerScratchCardObj){       
            try{
                $newId = $this->addCustomerScratchCardDB($customerScratchCardObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerScratchCardObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerScratchCard from DB for provided ID
    * * Return Data = customerScratchCardObject
    * @param int $customerScratchCardId 
    * @return resultObject
    */
    public function getCustomerScratchCardByID($customerScratchCardId){
        
            try {
                $customerScratchCardData = $this->loadCustomerScratchCardFromDB($customerScratchCardId);
            
                $customerScratchCardObj = customerScratchCardObject::withData($customerScratchCardData);
                $result = resultObject::withData(1,'',$customerScratchCardObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerScratchCardId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update customerScratchCard in DB
    * @param customerScratchCardObject $customerScratchCardObj 
    * @return resultObject
    */
    public function updateCustomerScratchCard(customerScratchCardObject $customerScratchCardObj){        
            try{
                $this->upateCustomerScratchCardDB($customerScratchCardObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerScratchCardObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerScratchCard from DB
    * @param int $customerScratchCardID 
    * @return resultObject
    */
    public function deleteCustomerScratchCardById($customerScratchCardID){
            try{
                $this->deleteCustomerScratchCardByIdDB($customerScratchCardID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerScratchCardID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
     * Grant a customer a scratch card
     * Return Data = array
     * @param customerObject $custObject 
     * @param mixed $event_id 
     * @param mixed $cevent_id 
     * @return resultObject
     */
    public function grantScratchCard(customerObject $custObject,$event_id,$cevent_id = 0){

        if(!bizManager::hasPremiumFeature(enumModuleNumber::loyalty_card,$custObject->cust_biz_id)){
            return resultObject::withData(0,"no_loyalty");
        }
        
        $result = $this->pullScratchCard($custObject->cust_biz_id);
       
        if($result->code == 0){
            return $result;
        }

        $selectedCard = $result->data;
        $scratchCard = new customerScratchCardObject();
        $scratchCard->cscard_biz_id = $custObject->cust_biz_id;
        $scratchCard->cscard_cust_id = $custObject->cust_id;
        $scratchCard->cscard_card_id = $selectedCard->md_row_id;
        $scratchCard->cscard_event_id = $event_id;
        $scratchCard->cscard_cevent_id = $cevent_id;
        $scratchCard->cscard_win = $selectedCard->card_won;
        $scratchCard->cscard_prize_type = $selectedCard->md_prize_type;
        $scratchCard->cscard_prize_id = $selectedCard->md_prize_id;
        $scratchCard->cscard_prize_value = $selectedCard->md_prize_value;
        $scratchCard->cscard_prize_title = $selectedCard->md_desc;
        $scratchCard->cscard_prize_text = $selectedCard->md_prize_text;
        $scratchCard->cscard_expire = $selectedCard->md_card_expire;
        $scratchCard->cscard_consolation = $selectedCard->md_consolation;
        $scratchCard->cscard_consolation_text = $selectedCard->md_consolation_text;

        $result = $this->addCustomerScratchCard($scratchCard);
        if($result->code == 0){
            return resultObject::withData(0,"failed_add_entry");
        }
        $row_id = $result->data;

        if($row_id > 0){

            $cardData = array();
            $cardData['cscard_id'] = $row_id;
            $cardData['prize_type'] = $scratchCard->cscard_prize_type;
            $cardData['title'] = $selectedCard->md_head;
            $cardData['text'] = $scratchCard->cscard_prize_text;
            $cardData['back_type'] = $selectedCard->md_back_type;
            $cardData['back_data'] = $selectedCard->md_back_type == "image" ? stripslashes($selectedCard->md_pic) : $selectedCard->md_back_data;

            $value = $scratchCard->cscard_prize_value;
            if($scratchCard->cscard_prize_type == 'product'){
                $levelManager = new levelDataManager(enumModuleNumber::mobile_shop);
                $levelResult = $levelManager->getLevelDataByID($scratchCard->cscard_prize_id);
                if($levelResult->code == 1){
                    $value = $levelResult->data->md_head;
                }else{
                    $value = "";
                }
            }
            if($scratchCard->cscard_prize_type == 'service'){
                
                $serviceResult = bookingManager::getServiceByID($scratchCard->cscard_prize_id);
                if($serviceResult->code == 1){
                    $value = $serviceResult->data->bmt_name;
                }else{
                    $value = "";
                }
            }
            if($scratchCard->cscard_prize_type == 'other'){                
                $value = $scratchCard->cscard_prize_title;
            }
            $cardData['prize_value'] = $value;

            if($scratchCard->cscard_win == 0){
                $cardData['prize_value'] = $scratchCard->cscard_consolation;
                $cardData['text'] = "consolation";
            }
            
            return resultObject::withData(1,"ok",$cardData);
        }
        else{
            return resultObject::withData(0,"failed_add_entry");
        }
    }

    /**
     * Select a random card from the biz scratch cards
     * Return Data = levelData13Object
     * @param mixed $bizId 
     * @return resultObject
     */
    private function pullScratchCard($bizId){

        try{
            $cards = $this->getAvailbleScratchCardsForBiz($bizId);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizId);
            return resultObject::withData(0,$e->getMessage());
        }

        if(count($cards) == 0){
            return resultObject::withData(0,"no_availble_cards");
        }
        
        $hand = rand(0,count($cards) - 1);
        
        if(isset($cards[$hand])){

            $scratchObject = levelData13Object::withData($cards[$hand]);

            //Check if card is winner
            $roll = rand(1,$scratchObject->md_win_ratio);

            $won = $roll == 1 ? 1 : 0;

            $scratchObject->card_won = $won;
            
            if($won == 1){
                $levelData = new levelDataManager(enumModuleNumber::scratch_card);
                $scratchObject->md_prize_current_amount = $scratchObject->md_prize_current_amount - 1;
                $levelData->updateLevelData($scratchObject);

                if($scratchObject->md_prize_current_amount <= 0){
                    notificationsManager::addInfoBannerToBiz($bizId,enumBannerType::prizes_scratch_card_over);
                }
            }
            
            return resultObject::withData(1,"ok",$scratchObject);
        }
        
        return resultObject::withData(0,"ok");       
    }

    /**
     * Get the reward of a scratched card
     * @param mixed $cust_scratch_id 
     * @param mixed $bizId
     * @return resultObject
     */
    public function getScratchedReward($cust_scratch_id,$bizId){

        $scratchResult = $this->getCustomerScratchCardByID($cust_scratch_id);
        if($scratchResult->code == 1){
            if($scratchResult->data->cscard_status == 'scratched'){
                return resultObject::withData(0,"card_already_scratched");
            }

            if($scratchResult->data->cscard_win == 0 && $scratchResult->data->cscard_consolation == 0){
                return resultObject::withData(0,"no_consolation_prize");
            }

            $customerResult = $this->getCustomerWithID($scratchResult->data->cscard_cust_id);
            if($customerResult->code != 1 || $customerResult->data->cust_id == 0){
                return resultObject::withData(0,"no_customer");
            }
            $customerObject = $customerResult->data;

            if($scratchResult->data->cscard_win == 0 && $scratchResult->data->cscard_consolation != 0){
                
                $custPoints = new customerPointsHistoryObject();
                $custPoints->cph_biz_id = $bizId;
                $custPoints->cph_cust_id = $scratchResult->data->cscard_cust_id;
                $custPoints->cph_source = 'scratch_lose';
                $custPoints->cph_value = $scratchResult->data->cscard_consolation;
                $custPoints->cph_reason_text = "";
                $custPoints->cph_external_id =$scratchResult->data->cscard_id;

                $result = $this->grantCustomerPoints($customerObject,$custPoints);
                
                if($result->code != 1){
                    return $result;
                }
            }

            if($scratchResult->data->cscard_win == 1){
                if($scratchResult->data->cscard_prize_type != 'points'){
                    $title = isset($scratchResult->data->cscard_prize_title) ? $scratchResult->data->cscard_prize_title : '';

                    $hasItem = true;
                    $image = '';
                    
                    if($scratchResult->data->cscard_prize_type == 'product'){
                        $levelDataManager = new levelDataManager(9);
                        $productResult = $levelDataManager->getLevelDataByID($scratchResult->data->cscard_prize_id);

                        $hasItem = false;
                        if($productResult->code == 1){
                            if(isset($productResult->data->md_head) && $productResult->data->md_head != ""){
                                $title = $productResult->data->md_head;
                                $image = $productResult->data->md_icon;
                                $hasItem = true;
                            }
                        }
                    }
                    if($scratchResult->data->cscard_prize_type == 'service'){

                        $bookingManager = new bookingManager();
                        $serviceResult = $bookingManager->getServiceByID($scratchResult->data->cscard_prize_id);
                        $hasItem = false;
                        if($serviceResult->code == 1){
                            if(isset($serviceResult->data->bmt_name) && $serviceResult->data->bmt_name != ""){
                                $title = $serviceResult->data->bmt_name; 
                                $hasItem = true;
                            }
                        }
                    }

                    if($hasItem){

                        $benefitObj = new customerBenefitObject();

                        $benefitObj->cb_biz_id = $bizId;
                        $benefitObj->cb_cust_id = $scratchResult->data->cscard_cust_id;
                        $benefitObj->cb_type = 'scratch';
                        $benefitObj->cb_item_type = $scratchResult->data->cscard_prize_type;
                        $benefitObj->cb_external_id = $scratchResult->data->cscard_prize_id;
                        $benefitObj->cb_value = $scratchResult->data->cscard_prize_value;
                        $benefitObj->cb_title = $title;
                        $benefitObj->cb_description = $scratchResult->data->cscard_prize_text;
                        $benefitObj->cb_image = $image;
                        $benefitObj->cb_code = utilityManager::generateRedeemCode($scratchResult->data->cscard_cust_id."1");
                        if(isset($scratchResult->data->cscard_expire) && $scratchResult->data->cscard_expire != ''){
                            $benefitObj->cb_valid_end = $scratchResult->data->cscard_expire;
                        }
                        $benefitObj->cb_scard_id = $scratchResult->data->cscard_id;

                        $grantResult = customerManager::addCustomerBenefit($benefitObj);

                        if($grantResult->code == 1){
                            customerManager::addBadgeToCustomer($scratchResult->data->cscard_cust_id,enumBadges::benefits);
                        }

                        $scratchResult->data->cscard_status = 'scratched';
                        $scratchResult->data->cscard_scratched_on = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                        $this->updateCustomerScratchCard($scratchResult->data);
                        return resultObject::withData(1,"ok");
                    }
                    else{
                        return resultObject::withData(0,"card_item_missing");
                    }
                }
                else{

                    $custPoints = new customerPointsHistoryObject();
                    $custPoints->cph_biz_id = $bizId;
                    $custPoints->cph_cust_id = $scratchResult->data->cscard_cust_id;
                    $custPoints->cph_source = 'scratch_win';
                    $custPoints->cph_value = $scratchResult->data->cscard_prize_value;
                    $custPoints->cph_reason_text = "";
                    $custPoints->cph_external_id =$scratchResult->data->cscard_id;

                    $result = $this->grantCustomerPoints($customerObject,$custPoints); 
                    if($result->code != 1){
                        return $result;
                    }

                    $scratchResult->data->cscard_status = 'scratched';
                    $scratchResult->data->cscard_scratched_on = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                    $this->updateCustomerScratchCard($scratchResult->data);
                    return resultObject::withData(1,"ok");
                }
            }else{
                return resultObject::withData(0,"failed_add_entry");
            }
        }else{
            return $scratchResult;
        }
    }

    /**
     * Summary of getCustomerScratchCard
     * @param mixed $cscard_id 
     * @return resultObject
     */
    public function getCustomerScratchCard($cscard_id){

        $scratchResult = $this->getCustomerScratchCardByID($cscard_id);

        if($scratchResult->code != 1){
            return resultObject::withData(0,"no_customer_scratch_card_found");
        }

        $levelDataManager = new levelDataManager(13);
        $scratchCardResult = $levelDataManager->getLevelDataByID($scratchResult->data->cscard_card_id);

        if($scratchCardResult->code != 1){
            return resultObject::withData(0,"no_scratch_card_found");
        }

        $cardData = array();
        $cardData['cscard_id'] = $cscard_id;
        $cardData['prize_type'] = $scratchResult->data->cscard_prize_type;
        $cardData['title'] = $scratchCardResult->data->md_head;
        $cardData['text'] = $scratchResult->data->cscard_prize_text;
        $cardData['back_type'] = $scratchCardResult->data->md_back_type;
        $cardData['back_data'] = $scratchCardResult->data->md_back_type == "image" ? $scratchCardResult->data->md_pic : $scratchCardResult->data->md_back_data;

        $value = $scratchResult->data->cscard_prize_value;

        if($scratchResult->data->cscard_prize_type == 'product'){

            $levelDataManagerProduct = new levelDataManager(9);
            $productResult = $levelDataManagerProduct->getLevelDataByID($scratchResult->data->cscard_prize_id);

            if($productResult->code == 1){
                if(isset($productResult->data->md_head) && $productResult->data->md_head != ""){
                    $value = $productResult->data->md_head;
                }
            }
        }
        if($scratchResult->data->cscard_prize_type == 'service'){

            $bookingManager = new bookingManager();
            $serviceResult = $bookingManager->getServiceByID($scratchResult->data->cscard_prize_id);
            if($serviceResult->code == 1){
                if(isset($serviceResult->data->bmt_name) && $serviceResult->data->bmt_name != ""){
                    $value = $serviceResult->data->bmt_name; 
                }
            }
        }
        if($scratchResult->data->cscard_prize_type == 'other'){            
            $value = $scratchResult->data->cscard_prize_text;
        }

        $cardData['prize_value'] = $value;

        if($scratchResult->data->cscard_win == 0){
            $cardData['prize_value'] = $scratchResult->data->cscard_consolation;
            $cardData['text'] = $scratchResult->data->cscard_consolation_text;
        }

        return resultObject::withData(0,"ok",$cardData);
    }
    /************************************* */
    /*   BASIC CUSTOMERSCRATCHCARD - DB METHODS           */
    /************************************* */

    private function addCustomerScratchCardDB(customerScratchCardObject $obj){

        if (!isset($obj)){
            throw new Exception("customerScratchCardObject value must be provided");             
        }

        $cscard_scratched_onDate = isset($obj->cscard_scratched_on) && $obj->cscard_scratched_on != "" ? "'".$obj->cscard_scratched_on."'" : "null";
        $cscard_expireDate = isset($obj->cscard_expire) && $obj->cscard_expire != "" ? "'".$obj->cscard_expire."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_customer_scratch SET 
                                        cscard_biz_id = {$obj->cscard_biz_id},
                                        cscard_cust_id = {$obj->cscard_cust_id},
                                        cscard_card_id = {$obj->cscard_card_id},
                                        cscard_win = {$obj->cscard_win},
                                        cscard_status = '{$obj->cscard_status}',
                                        cscard_scratched_on = $cscard_scratched_onDate,
                                        cscard_event_id = {$obj->cscard_event_id},
                                        cscard_cevent_id = {$obj->cscard_cevent_id},
                                        cscard_prize_type = '{$obj->cscard_prize_type}',
                                        cscard_prize_id = {$obj->cscard_prize_id},
                                        cscard_prize_value = {$obj->cscard_prize_value},
                                        cscard_prize_text = '".addslashes($obj->cscard_prize_text)."',
                                        cscard_expire = $cscard_expireDate,
                                        cscard_consolation = {$obj->cscard_consolation},
                                        cscard_consolation_text = '".addslashes($obj->cscard_consolation_text)."',
                                        cscard_prize_title = '".addslashes($obj->cscard_prize_title)."'
                                                 ");
        return $newId;
    }

    private function loadCustomerScratchCardFromDB($customerScratchCardID){

        if (!is_numeric($customerScratchCardID) || $customerScratchCardID <= 0){
            throw new Exception("Illegal value customerScratchCardID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_scratch WHERE cscard_id = $customerScratchCardID");
    }

    private function upateCustomerScratchCardDB(customerScratchCardObject $obj){

        if (!isset($obj->cscard_id) || !is_numeric($obj->cscard_id) || $obj->cscard_id <= 0){
            throw new Exception("customerScratchCardObject value must be provided");             
        }

        $cscard_dateDate = isset($obj->cscard_date) ? "'".$obj->cscard_date."'" : "null";

        $cscard_scratched_onDate = isset($obj->cscard_scratched_on) ? "'".$obj->cscard_scratched_on."'" : "null";

        $cscard_expireDate = isset($obj->cscard_expire) ? "'".$obj->cscard_expire."'" : "null";

        $this->db->execute("UPDATE tbl_customer_scratch SET 
                                cscard_biz_id = {$obj->cscard_biz_id},
                                cscard_cust_id = {$obj->cscard_cust_id},
                                cscard_card_id = {$obj->cscard_card_id},
                                cscard_win = {$obj->cscard_win},
                                cscard_date = $cscard_dateDate,
                                cscard_status = '{$obj->cscard_status}',
                                cscard_scratched_on = $cscard_scratched_onDate,
                                cscard_event_id = {$obj->cscard_event_id},
                                cscard_cevent_id = {$obj->cscard_cevent_id},
                                cscard_prize_type = '{$obj->cscard_prize_type}',
                                cscard_prize_id = {$obj->cscard_prize_id},
                                cscard_prize_value = {$obj->cscard_prize_value},
                                cscard_prize_text = '".addslashes($obj->cscard_prize_text)."',
                                cscard_expire = $cscard_expireDate,
                                cscard_consolation = {$obj->cscard_consolation},
                                cscard_consolation_text = '".addslashes($obj->cscard_consolation_text)."',
                                cscard_prize_title = '".addslashes($obj->cscard_prize_title)."'
                                WHERE cscard_id = {$obj->cscard_id} 
                                         ");
    }

    private function deleteCustomerScratchCardByIdDB($customerScratchCardID){

        if (!is_numeric($customerScratchCardID) || $customerScratchCardID <= 0){
            throw new Exception("Illegal value customerScratchCardID");             
        }

        $this->db->execute("DELETE FROM tbl_customer_scratch WHERE cscard_id = $customerScratchCardID");
    }

    private function getAvailbleScratchCardsForBiz($bizId){

        if (!is_numeric($bizId) || $bizId <= 0){
            throw new Exception("Illegal value bizId");             
        }

        $sql = "SELECT * FROM tbl_mod_data13 
                WHERE md_biz_id = $bizId
                AND (md_date1 < NOW() OR md_date1 IS NULL)
                AND (md_date2 > NOW() OR md_date2 IS NULL)
                AND md_prize_current_amount > 0
                AND md_visible_element = 1";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERDOCUMENT - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerDocumentObject to DB
    * Return Data = new customerDocumentObject ID
    * @param customerDocumentObject $customerDocumentObj 
    * @return resultObject
    */
    public function addCustomerDocument(customerDocumentObject $customerDocumentObj){       
        try{
            $newId = $this->addCustomerDocumentDB($customerDocumentObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerDocumentObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get customerDocument from DB for provided ID
    * * Return Data = customerDocumentObject
    * @param int $customerDocumentId 
    * @return resultObject
    */
    public function getCustomerDocumentByID($customerDocumentId){
        
        try {
            $customerDocumentData = $this->loadCustomerDocumentFromDB($customerDocumentId);
            
            $customerDocumentObj = customerDocumentObject::withData($customerDocumentData);
            $result = resultObject::withData(1,'',$customerDocumentObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerDocumentId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update customerDocumentObject in DB
    * @param customerDocumentObject $customerDocumentObj 
    * @return resultObject
    */
    public function updateCustomerDocument(customerDocumentObject $customerDocumentObj){        
        try{
            $this->upateCustomerDocumentDB($customerDocumentObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerDocumentObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete customerDocument from DB
    * @param int $customerDocumentID 
    * @return resultObject
    */
    public function deleteCustomerDocumentById($customerDocumentID){
        try{
            $this->deleteCustomerDocumentByIdDB($customerDocumentID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerDocumentID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERDOCUMENT - DB METHODS           */
    /************************************* */

    private function addCustomerDocumentDB(customerDocumentObject $obj){

        if (!isset($obj)){
            throw new Exception("customerDocumentObject value must be provided");             
        }

        $do_send_dateDate = isset($obj->do_send_date) ? "'".$obj->do_send_date."'" : "null";

        $do_receive_dateDate = isset($obj->do_receive_date) ? "'".$obj->do_receive_date."'" : "null";

        $do_uploaded_byDate = isset($obj->do_uploaded_by) ? "'".$obj->do_uploaded_by."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_documents SET 
                                        do_biz_id = {$obj->do_biz_id},
                                        do_cust_id = {$obj->do_cust_id},
                                        do_title = '".addslashes($obj->do_title)."',
                                        do_url = '".addslashes($obj->do_url)."',
                                        do_external_id = {$obj->do_external_id},
                                        do_form_id = {$obj->do_form_id},
                                        do_send_date = $do_send_dateDate,
                                        do_receive_date = $do_receive_dateDate,
                                        do_uploaded_by = $do_uploaded_byDate,
                                        do_type = '{$obj->do_type}'
                                                    ");
        return $newId;
    }

    private function loadCustomerDocumentFromDB($customerDocumentID){

        if (!is_numeric($customerDocumentID) || $customerDocumentID <= 0){
            throw new Exception("Illegal value customerDocumentID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_documents WHERE do_id = $customerDocumentID");
    }

    private function upateCustomerDocumentDB(customerDocumentObject $obj){

        if (!isset($obj->do_id) || !is_numeric($obj->do_id) || $obj->do_id <= 0){
            throw new Exception("customerDocumentObject value must be provided");             
        }

        $do_create_dateDate = isset($obj->do_create_date) ? "'".$obj->do_create_date."'" : "null";

        $do_send_dateDate = isset($obj->do_send_date) ? "'".$obj->do_send_date."'" : "null";

        $this->db->execute("UPDATE tbl_documents SET 
                                do_biz_id = {$obj->do_biz_id},
                                do_cust_id = {$obj->do_cust_id},
                                do_title = '".addslashes($obj->do_title)."',
                                do_url = '".addslashes($obj->do_url)."',
                                do_external_id = {$obj->do_external_id},
                                do_form_id = {$obj->do_form_id},
                                do_create_date = $do_create_dateDate,
                                do_send_date = $do_send_dateDate,
                                do_receive_date = '{$obj->do_receive_date}',
                                do_uploaded_by = '{$obj->do_uploaded_by}',
                                do_type = '{$obj->do_type}'
                                WHERE do_id = {$obj->do_id} 
                                         ");
    }

    private function deleteCustomerDocumentByIdDB($customerDocumentID){

        if (!is_numeric($customerDocumentID) || $customerDocumentID <= 0){
            throw new Exception("Illegal value customerDocumentID");             
        }

        $this->db->execute("DELETE FROM tbl_documents WHERE do_id = $customerDocumentID");
    }

    private function updateAllNewDocumentsRecievedByCustomer(customerObject $customer){
        $this->db->execute("update tbl_documents
                                set do_receive_date = now()
                                where do_cust_id={$customer->cust_id}
                                and do_biz_id = {$customer->cust_biz_id}
                                and do_uploaded_by = 'admin'
                                and do_receive_date is null
                                ");
    }

    private function getCustomerDocumentsDB(customerObject $customer,$skip = 0, $take = 0){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_documents
                    WHERE do_cust_id = {$customer->cust_id}
                    AND do_biz_id = {$customer->cust_biz_id}
                    ORDER BY do_send_date DESC
                    $limit
                ";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERSTAGEHISTORY - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerStageHistory to DB
     * Return Data = new customerStageHistory ID
     * @param customerStageHistoryObject $customerStageHistoryObj 
     * @return resultObject
     */
    public function addCustomerStageHistory(customerStageHistoryObject $customerStageHistoryObj){       
        try{
            $newId = $this->addCustomerStageHistoryDB($customerStageHistoryObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerStageHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerStageHistory from DB for provided ID
     * * Return Data = customerStageHistory
     * @param int $customerStageHistoryId 
     * @return resultObject
     */
    public function getCustomerStageHistoryByID($customerStageHistoryId){
        
        try {
            $customerStageHistoryData = $this->loadCustomerStageHistoryFromDB($customerStageHistoryId);
            
            $customerStageHistoryObj = customerStageHistoryObject::withData($customerStageHistoryData);
            $result = resultObject::withData(1,'',$customerStageHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerStageHistoryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param customerStageHistoryObject $customerStageHistoryObj 
     * @return resultObject
     */
    public function updateCustomerStageHistory(customerStageHistoryObject $customerStageHistoryObj){        
        try{
            $this->upateCustomerStageHistoryDB($customerStageHistoryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerStageHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerStageHistory from DB
     * @param int $customerStageHistoryID 
     * @return resultObject
     */
    public function deleteCustomerStageHistoryById($customerStageHistoryID){
        try{
            $this->deleteCustomerStageHistoryByIdDB($customerStageHistoryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerStageHistoryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Return active history row for given customer
     * Return Data = customerStageHistoryObject
     * @param mixed $customerId 
     * @return resultObject
     */
    public function getStageForCustomer($customerId){
        
        try {
            $customerStageHistoryData = $this->getStageForCustomer_DB($customerId);
            
            $customerStageHistoryObj = customerStageHistoryObject::withData($customerStageHistoryData);
            $result = resultObject::withData(1,'',$customerStageHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERSTAGEHISTORY - DB METHODS           */
    /************************************* */

    private function addCustomerStageHistoryDB(customerStageHistoryObject $obj){

        if (!isset($obj)){
            throw new Exception("customerStageHistory value must be provided");             
        }

        $csh_start_date = isset($obj->csh_start_date) ? $obj->csh_start_date : utilityManager::getSQLDateFormatTimeCurrentTimestamp();
        $csh_end_dateDate = isset($obj->csh_end_date) ? "'".$obj->csh_end_date."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_customer_stage_hist SET 
                                        csh_biz_id = {$obj->csh_biz_id},
                                        csh_cust_id = {$obj->csh_cust_id},
                                        csh_stage = {$obj->csh_stage},
                                        csh_start_date = '$csh_start_date',
                                        csh_end_date = $csh_end_dateDate
                                        
         ");
        return $newId;
    }

    private function loadCustomerStageHistoryFromDB($customerStageHistoryID){

        if (!is_numeric($customerStageHistoryID) || $customerStageHistoryID <= 0){
            throw new Exception("Illegal value $customerStageHistoryID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_stage_hist WHERE csh_id = $customerStageHistoryID");
    }

    private function upateCustomerStageHistoryDB(customerStageHistoryObject $obj){

        if (!isset($obj->csh_id) || !is_numeric($obj->csh_id) || $obj->csh_id <= 0){
            throw new Exception("customerStageHistory value must be provided");             
        }

        $csh_start_dateDate = isset($obj->csh_start_date) ? "'".$obj->csh_start_date."'" : "null";

        $csh_end_dateDate = isset($obj->csh_end_date) ? "'".$obj->csh_end_date."'" : "null";

        $csh_next_processingDate = isset($obj->csh_next_processing) ? "'".$obj->csh_next_processing."'" : "null";

        $this->db->execute("UPDATE tbl_customer_stage_hist SET 
                                csh_biz_id = {$obj->csh_biz_id},
                                csh_cust_id = {$obj->csh_cust_id},
                                csh_stage = {$obj->csh_stage},
                                csh_start_date = $csh_start_dateDate,
                                csh_end_date = $csh_end_dateDate,
                                csh_next_processing = $csh_next_processingDate
                                WHERE csh_id = {$obj->csh_id} 
         ");
    }

    private function deleteCustomerStageHistoryByIdDB($customerStageHistoryID){

        if (!is_numeric($customerStageHistoryID) || $customerStageHistoryID <= 0){
            throw new Exception("Illegal value $customerStageHistoryID");             
        }

        $this->db->execute("DELETE FROM tbl_customer_stage_hist WHERE csh_id = $customerStageHistoryID");
    }

    private function getStageForCustomer_DB($customerID){

        if (!is_numeric($customerID) || $customerID <= 0){
            throw new Exception("Illegal value $customerID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_stage_hist 
                                    WHERE csh_cust_id = $customerID
                                    AND csh_end_date IS NULL");
    }

    /**
     * Summary of updateStageForCustomer_DB
     * @param customerObject $custObject 
     * @throws Exception 
     */
    private function updateStageHistoryForCustomer_DB(customerObject $custObject, $endDate = null){

        if (!is_numeric($custObject->cust_id) || $custObject->cust_id <= 0){
            throw new Exception("Illegal value {$custObject->cust_id}");             
        }

        $setEndDate = isset($endDate) ? "csh_end_date = '$endDate'" : "csh_end_date = NOW()";

        $sql = "UPDATE tbl_customer_stage_hist 
                            SET $setEndDate
                            WHERE csh_cust_id = {$custObject->cust_id}
                            AND csh_end_date IS NULL";

        $this->db->execute($sql);
    }

}
