<?php

class resellerClientModel extends Model
{    
    public function __construct()
    {
        parent::__construct(); 
    }

    /**
     * Get reseller client by email and owner
     * * Return Data = resellerClientsObject
     * @param mixed $usernameEmail 
     * @param mixed $ownerId 
     * @return resultObject
     */
    public function getResellerClientByUsernameAndOwnerId($usernameEmail,$ownerId){

        try{
            $responseData = $this->getResellerClientByUsernameAndOwnerIdDB($usernameEmail,$ownerId);
            $resellerClientsObject = resellerClientsObject::withData($responseData);
            return resultObject::withData(1,"Ok",$resellerClientsObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getResellerClientByBizId($bizId){

        try{
            $responseData = $this->getResellerClientByBizIdDB($bizId);
            if(is_array($responseData)){
                $resellerClientsObject = resellerClientsObject::withData($responseData);
                return resultObject::withData(1,"Ok",$resellerClientsObject);
            }else{
                return resultObject::withData(1,"Ok","");
            } 
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }


    /*------------------------- PRIVATE FUNCTIONS-----------------------------*/

    private function getResellerClientByUsernameAndOwnerIdDB($usernameEmail,$ownerId){

        if (!isset($usernameEmail) || $usernameEmail == ""){
            throw new Exception("Illegal value for usernameEmail - $usernameEmail");             
        }

        if (!is_numeric($ownerId) || $ownerId <= 0){
            throw new Exception("Illegal value for ownerId - $ownerId");             
        }

        return $this->db->getRow("SELECT * FROM tbl_reseller_clients WHERE reseller_client_username='$usernameEmail' and reseller_client_owner_id = $ownerId");
    }

    private function getResellerClientByBizIdDB($bizId){

        if (!is_numeric($bizId) || $bizId <= 0){
            throw new Exception("Illegal value for bizId - $bizId");             
        }

        return $this->db->getRow("SELECT * FROM tbl_reseller_clients 
                                    WHERE reseller_client_id IN (SELECT client_biz_client_id 
                                                                    FROM tbl_client_biz 
                                                                    WHERE client_biz_biz_id = $bizId)
                                 ");
    }

}
