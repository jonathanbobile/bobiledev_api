<?php 

class bizFunctionObject extends functionObject { 

    public $bfu_id = "0";
    public $bfu_func_id = "0";
    public $bfu_biz_id = "0";
    public $bfu_label;
    public $bfu_info;
    public $bfu_bool1 = "0";
    public $bfu_bool2 = "0";
    public $bfu_bool3 = "0";
    public $bfu_bool4 = "1";
    public $bfu_index = "0";
    public $bfu_visible = "1";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bfu_id"])){
              throw new Exception("bizFunctionObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillFunctionData($data);

        $instance->bfu_id = $data["bfu_id"];
        $instance->bfu_func_id = $data["bfu_func_id"];
        $instance->bfu_biz_id = $data["bfu_biz_id"];
        $instance->bfu_label = $data["bfu_label"];
        $instance->bfu_info = $data["bfu_info"];
        $instance->bfu_bool1 = $data["bfu_bool1"];
        $instance->bfu_bool2 = $data["bfu_bool2"];
        $instance->bfu_bool3 = $data["bfu_bool3"];
        $instance->bfu_bool4 = $data["bfu_bool4"];
        $instance->bfu_index = $data["bfu_index"];
        $instance->bfu_visible = $data["bfu_visible"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


