<?php

/**
 * bizMembershipObject short summary.
 *
 * bizMembershipObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class bizMembershipObject extends bobileObject
{
    /* Identifiers */
    public $bm_id = "0";
    public $bm_biz_id = "0";
    public $bm_mship_level = "0";

    /* Details */
    public $bm_name = '';
    public $bm_notes = '';
    public $bm_back_color = '';
    public $bm_text_color = '';
    public $bm_img = '';

    /* Settings */
    public $bm_min_points = "0";
    public $bm_active = "1";

    /* Connected items */
    public $benefits = array();

    public static function withData($membershipData){
        if(!isset($membershipData["bm_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->bm_id = isset($membershipData["bm_id"]) && $membershipData["bm_id"] > 0 ? $membershipData["bm_id"] : 0;
        $instance->bm_biz_id = isset($membershipData["bm_biz_id"]) && $membershipData["bm_biz_id"] ? $membershipData["bm_biz_id"] : 0;
        $instance->bm_mship_level = isset($membershipData["bm_mship_level"]) && $membershipData["bm_mship_level"] > 0 ? $membershipData["bm_mship_level"] : 0;

        /* Details */
        $instance->bm_name = $membershipData["bm_name"];
        $instance->bm_notes = $membershipData["bm_notes"];
        $instance->bm_back_color = $membershipData["bm_back_color"];
        $instance->bm_text_color = $membershipData["bm_text_color"];
        $instance->bm_img = $membershipData["bm_img"];

        /* Settings */
        $instance->bm_min_points = $membershipData["bm_min_points"];
        $instance->bm_active = $membershipData["bm_active"];

        $instance->benefits = membershipsManager::getMembershipBenefitsListByMembershipID($instance->bm_id);

        return $instance;
    }

    public function fillData($membershipData){
        if(!isset($membershipData["bm_id"])){
            throw new Exception("Data incorrect");
        }

        /* Identifiers */
        $this->bm_id = isset($membershipData["bm_id"]) && $membershipData["bm_id"] > 0 ? $membershipData["bm_id"] : 0;
        $this->bm_biz_id = isset($membershipData["bm_biz_id"]) && $membershipData["bm_biz_id"] ? $membershipData["bm_biz_id"] : 0;
        $this->bm_mship_level = isset($membershipData["bm_mship_level"]) && $membershipData["bm_mship_level"] > 0 ? $membershipData["bm_mship_level"] : 0;

        /* Details */
        $this->bm_name = $membershipData["bm_name"];
        $this->bm_notes = $membershipData["bm_notes"];
        $this->bm_back_color = $membershipData["bm_back_color"];
        $this->bm_text_color = isset($membershipData["bm_text_color"]) && $membershipData["bm_text_color"] != '' ? $membershipData["bm_text_color"] : "#000000";
        $this->bm_img = $membershipData["bm_img"];

        /* Settings */
        $this->bm_min_points = $membershipData["bm_min_points"];
        $this->bm_active = $membershipData["bm_active"];

        $this->benefits = membershipsManager::getMembershipBenefitsListByMembershipID($this->bm_id);
    }

    public function setBenefits($benefitsList){
        $this->benefits = $benefitsList;
    }

    public function getBenefits(){
        return $this->benefits;
    }
}
