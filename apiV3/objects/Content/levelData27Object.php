<?php 

class levelData27Object extends levelDataObject { 

    public $md_mod_id = "27";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData27Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        return $instance;
    }
}
?>


