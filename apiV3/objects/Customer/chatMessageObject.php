<?php 

class chatMessageObject extends bobileObject { 

public $ch_id = "0";
public $ch_user = "0";
public $ch_customer = "0";
public $ch_time = "";
public $ch_message;
public $ch_dir = "1";
public $ch_new = "1";
public $cust_id = "";
public $cust_pic = "";

 function __construct(){} 

public static function withData($data){

if (!isset($data["ch_id"])){
      throw new Exception("chatMessageObject constructor requies data array provided!");
}

$instance = new self();

$instance->ch_id = $data["ch_id"];
$instance->ch_user = $data["ch_user"];
$instance->ch_customer = $data["ch_customer"];
$instance->ch_time = $data["ch_time"];
$instance->ch_message = $data["ch_message"];
$instance->ch_dir = $data["ch_dir"];
$instance->ch_new = $data["ch_new"];

return $instance;
}

function _isValid(){
      return true;
}
}
?>


