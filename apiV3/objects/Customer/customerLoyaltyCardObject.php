<?php

/**
 * customerPunchCardObject short summary.
 *
 * customerPunchCardObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerLoyaltyCardObject extends bobileObject
{
    /* Identifiers */
    public $customer_card_id = "0";
    public $customer_card_biz_id = "0";
    public $customer_card_program_id = "0";
    public $customer_card_cust_id = "0";

    /* Settings */
    public $customer_card_start_date;

    /* Status */
    public $customer_card_stamp_no = "0";    
    public $customer_card_last_stamp;
    public $customer_card_redeemed = "0";
    public $customer_card_redeemed_date;
    public $customer_card_code = "0";

    /* Card data */
    public $card_data;

    /* Card punch history */
    public $card_punches = array();


    public static function withData($custPunchCardData){
        if(!isset($custPunchCardData["customer_card_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->customer_card_id = isset($custPunchCardData["customer_card_id"]) ? $custPunchCardData["customer_card_id"] : 0;
        $instance->customer_card_biz_id = isset($custPunchCardData["customer_card_biz_id"]) ? $custPunchCardData["customer_card_biz_id"] : 0;
        $instance->customer_card_program_id = isset($custPunchCardData["customer_card_program_id"]) ? $custPunchCardData["customer_card_program_id"] : 0;
        $instance->customer_card_cust_id = isset($custPunchCardData["customer_card_cust_id"]) ? $custPunchCardData["customer_card_cust_id"] : 0;

        /* Settings */
        $instance->customer_card_start_date = $custPunchCardData["customer_card_start_date"];

        /* Status */
        $instance->customer_card_stamp_no = $custPunchCardData["customer_card_stamp_no"];        
        $instance->customer_card_last_stamp = $custPunchCardData["customer_card_last_stamp"];
        $instance->customer_card_redeemed = $custPunchCardData["customer_card_redeemed"];
        $instance->customer_card_redeemed_date = $custPunchCardData["customer_card_redeemed_date"];
        $instance->customer_card_code = $custPunchCardData["customer_card_code"];

        return $instance;
    }

    public function setCardData(levelData6Object $cardData){
        $this->card_data = $cardData;
    }

    public function getCardData(){
        return $this->card_data;
    }

    public function setPunches($punchesList){
        $this->card_punches = $punchesList;
    }

    public function getPunches(){
        return $this->card_punches;
    }
}
