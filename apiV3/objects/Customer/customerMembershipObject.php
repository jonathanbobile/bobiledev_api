<?php

/**
 * customerMembershipObject short summary.
 *
 * customerMembershipObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerMembershipObject extends bizMembershipObject
{
    public $next_membership_level = 0;    
    public $next_membership_id = 0;
    public $next_membership_name = '';
    public $next_membership_req_points = 0;
    public $next_membership_back_color = '#FFFFFF';
    public $points_to_next_level = 0;
    public $total_points = 0;
    public $current_points = 0;
    public $membership_discount = 0;

    /* Fields for mobile */
    public $membership_name = '';
    public $membership_color = '';
    public $membership_text_color = '';
    public $membership_notes = '';

    public static function withData($membershipData){
        if(!isset($membershipData["bm_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        $instance->fillData($membershipData);
        
        $instance->membership_discount = membershipsManager::getMembershipDiscount($instance->bm_id);

        $instance->membership_name = $instance->bm_name;
        $instance->membership_color = $instance->bm_back_color;
        $instance->membership_text_color = $instance->bm_text_color;
        $instance->membership_notes = $instance->bm_notes;

        return $instance;
    }    

    public function setNextMembershipLevelData(bizMembershipObject $membershipObj){
        $this->next_membership_id = $membershipObj->bm_id;
        $this->next_membership_level = $membershipObj->bm_mship_level;
        $this->next_membership_name = $membershipObj->bm_name;
        $this->next_membership_req_points = $membershipObj->bm_min_points;
        $this->next_membership_back_color = $membershipObj->bm_back_color;
    }

    public function setPointsToNextlevel($points_needed){
        $this->points_to_next_level = $points_needed;
    }
}
