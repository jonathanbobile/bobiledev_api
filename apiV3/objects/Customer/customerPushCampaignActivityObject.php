<?php 

class customerPushCampaignActivityObject extends bobileObject { 

    public $cup_id = "0";
    public $cup_customer_id = "0";
    public $cup_cust_id;
    public $cup_push_id = "0";
    public $cup_create_time = "";
    public $cup_open_time = "";
    public $cup_delivery_time;
    public $cup_device_type;//enum('','Android','IOS')

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cup_id"])){
              throw new Exception("customerPushCampaignActivityObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cup_id = $data["cup_id"];
        $instance->cup_customer_id = $data["cup_customer_id"];
        $instance->cup_cust_id = $data["cup_cust_id"];
        $instance->cup_push_id = $data["cup_push_id"];
        $instance->cup_create_time = $data["cup_create_time"];
        $instance->cup_open_time = $data["cup_open_time"];
        $instance->cup_delivery_time = $data["cup_delivery_time"];
        $instance->cup_device_type = $data["cup_device_type"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


