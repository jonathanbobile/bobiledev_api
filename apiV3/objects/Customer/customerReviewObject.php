<?php 

class customerReviewObject extends bobileObject { 

    public $cr_id = "0";
    public $cr_biz_id = "0";
    public $cr_cust_id = "0";
    public $cr_date = "";
    public $cr_mod_id = "0";
    public $cr_row_id = "0";
    public $cr_item_type;//enum('','product','service','employee','class')
    public $cr_item_id = "0";
    public $cr_rating = "0";
    public $cr_text;

    public $cr_customer_name = "";
    public $cr_customer_image = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cr_id"])){
              throw new Exception("customerReviewObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cr_id = $data["cr_id"];
        $instance->cr_biz_id = $data["cr_biz_id"];
        $instance->cr_cust_id = $data["cr_cust_id"];
        $instance->cr_date = $data["cr_date"];
        $instance->cr_mod_id = $data["cr_mod_id"];
        $instance->cr_row_id = $data["cr_row_id"];
        $instance->cr_item_type = $data["cr_item_type"];
        $instance->cr_item_id = $data["cr_item_id"];
        $instance->cr_rating = $data["cr_rating"];
        $instance->cr_text = $data["cr_text"];

        $instance->cr_customer_name = isset($data['cust_first_name']) ? $data['cust_first_name'] : "";
        $instance->cr_customer_image = isset($data['cust_pic']) ? $data['cust_pic'] : "";

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


