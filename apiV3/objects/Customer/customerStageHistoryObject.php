<?php 

class customerStageHistoryObject extends bobileObject { 

    public $csh_id = "0";
    public $csh_biz_id = "0";
    public $csh_cust_id = "0";
    public $csh_stage = "0";
    public $csh_start_date = "";
    public $csh_end_date;
    public $csh_next_processing = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["csh_id"])){
            throw new Exception("customerStageHistory constructor requies data array provided!");
        }

        $instance = new self();

        $instance->csh_id = $data["csh_id"];
        $instance->csh_biz_id = $data["csh_biz_id"];
        $instance->csh_cust_id = $data["csh_cust_id"];
        $instance->csh_stage = $data["csh_stage"];
        $instance->csh_start_date = $data["csh_start_date"];
        $instance->csh_end_date = $data["csh_end_date"];
        $instance->csh_next_processing = $data["csh_next_processing"];

        return $instance;
    }

    function _isValid(){
        return true;
    }
}
?>


