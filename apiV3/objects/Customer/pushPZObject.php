<?php 

class pushPZObject extends customerPersonalObject { 

    public $pu_id;
    public $pu_short_mess;
    public $pu_header;
    public $pu_img;
    public $pu_sent;
    public $pu_in_server;
    public $pu_type;
    public $pu_external_id;
    public $pu_level;
    public $pu_mod_id;
    public $pu_view;
    public $pu_view_android;
    public $pu_based_location;
    public $source;
    
    public $cup_create_time;
    public $cup_open_time = null;    

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["pu_id"])){
            throw new Exception("customerPushObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->pu_id = $data['pu_id'];
        $instance->pu_short_mess = $data['pu_short_mess'];
        $instance->pu_header = $data['pu_header'];
        $instance->pu_sent = $data['pu_sent'];
        $instance->pu_in_server = $data['pu_in_server'];
        $instance->pu_type = $data['pu_type'];
        $instance->pu_external_id = $data['pu_external_id'];
        $instance->pu_level = $data['pu_level'];
        $instance->pu_mod_id = $data['pu_mod_id'];
        $instance->pu_view = $data['pu_mod_id'] == 6 || $data['pu_mod_id'] == 26 ? "Info" : $data['pu_view'];
        $instance->pu_view_android = $data['pu_mod_id'] == 6 || $data['pu_mod_id'] == 26 ? "Info" : $data['pu_view_android'];
        $instance->pu_based_location = $data['pu_based_location'];
        $instance->source = $data['source'];
        $instance->cup_create_time = $data['cup_create_time'];
        $instance->cup_open_time = $data['cup_open_time'];

        return $instance;
    }

    public function PersonalZoneAPIArray(mobileRequestObject $request = null){
        $result = array();
        
        $sentTime = $this->pu_sent == null ? $this->cup_create_time : $this->pu_sent;
        $result['push_id'] = $this->pu_id;
        $result['push_html'] = "";//For now - was $tmpl_html_out
        $result['push_message'] = ($this->pu_short_mess == null) ? "" : $this->pu_short_mess;
        $result['push_open_time'] = ($this->cup_open_time == null) ? "" : $this->cup_open_time;
        $result['push_sent_time'] = $sentTime;
        $result['push_header'] = $this->pu_header;
        $result['push_in_server']=$this->pu_in_server;
        $result['push_sent_timestamp'] = strtotime( $sentTime );

        $result['push_type'] = ($this->pu_based_location == "1") ? "-1" : $this->pu_type;
        $result['push_external_id'] = $this->pu_external_id;
        $result['push_level'] = $this->pu_level;
        $result['push_mod_id'] = $this->pu_mod_id;
        $result['push_source'] = $this->source;

        $puView = $this->pu_view;
        $puViewAndroid  = $this->pu_view_android;
        if($puView == ""){
            $pushManager = new pushManager();
            $pushResult = $pushManager->getPushByID($this->pu_id);
            if($pushResult->code == 1){
                $structRow = pushManager::getModuleLastStructureForPush($pushResult->data);
                $puView = $structRow["ms_view_end"];
                $puViewAndroid  = $structRow["ms_view_end_android"];
            }
        }
        
        $result['push_view']=$puView;
        $result['push_view_android']=$puViewAndroid;

        return $result;
    } 

    function _isValid(){
          return true;
    }
}
?>


