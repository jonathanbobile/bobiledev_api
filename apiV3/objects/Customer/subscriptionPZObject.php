<?php

/**
 * subscriptionPZObject short summary.
 *
 * subscriptionPZObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class subscriptionPZObject extends levelData10Object
{
    public $cust_sub_data;

    public static function withData($data){
        if (!isset($data["md_row_id"])){
            throw new Exception("levelData10Object constructor requies data array provided!");
        }

        $instance = new self();
        
        $instance->fillLevelData($data);

        $instance->md_billing_type = $data["md_billing_type"];
        $instance->md_billing_period_unit = $data["md_billing_period_unit"];
        $instance->md_billing_paymnet_count = $data["md_billing_paymnet_count"];
        $instance->md_usage_period = $data["md_usage_period"];
        $instance->md_usage_period_unit = $data["md_usage_period_unit"];
        $instance->md_usage_period_number = $data["md_usage_period_number"];
        $instance->md_usage_period_start = $data["md_usage_period_start"];
        $instance->md_usage_period_end = $data["md_usage_period_end"];
        $instance->md_usage_rule_type = $data["md_usage_rule_type"];
        $instance->md_usage_rule_capacity = $data["md_usage_rule_capacity"];
        $instance->md_usage_rule_period_unit = $data["md_usage_rule_period_unit"];
        $instance->md_color = $data["md_color"];
        $instance->md_can_cancel = $data["md_can_cancel"];

        $instance->md_next_view = "Subscription_Info";

        $instance->extra_data = levelDataManager::getSubscriptionExtraData($instance);
        $instance->extra_data["connected_items"] = levelDataManager::getSubscriptionConnectedItems($instance);

        if(isset($data['csu_id'])){
            $custSubResult = customerSubscriptionManager::getCustomerSubscriptionByCustSubID($data['csu_id'],false);
            if($custSubResult->code == 1){
                $instance->cust_sub_data = $custSubResult->data;
            }
        }

        return $instance;
    }
}