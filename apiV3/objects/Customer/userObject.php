<?php 

class userObject extends bobileObject { 

public $usr_id = "0";
public $usr_nick_name;
public $usr_country_id = "0";
public $usr_state_id = "0";
public $usr_default_lng = "eng";
public $usr_e_mail;
public $usr_image;
public $usr_birth_day;
public $usr_gender = "0";
public $usr_mobile_num;
public $usr_mobile_serial;
public $usr_start_date = "";
public $usr_appid = "0";
public $usr_resellerid = "-1";
public $usr_marketid = "-1";
public $usr_os;

 function __construct(){} 

public static function withData($data){

if (!isset($data["usr_id"])){
      throw new Exception("userObject constructor requies data array provided!");
}

$instance = new self();

$instance->usr_id = $data["usr_id"];
$instance->usr_nick_name = $data["usr_nick_name"];
$instance->usr_country_id = $data["usr_country_id"];
$instance->usr_state_id = $data["usr_state_id"];
$instance->usr_default_lng = $data["usr_default_lng"];
$instance->usr_e_mail = $data["usr_e_mail"];
$instance->usr_image = $data["usr_image"];
$instance->usr_birth_day = $data["usr_birth_day"];
$instance->usr_gender = $data["usr_gender"];
$instance->usr_mobile_num = $data["usr_mobile_num"];
$instance->usr_mobile_serial = $data["usr_mobile_serial"];
$instance->usr_start_date = $data["usr_start_date"];
$instance->usr_appid = $data["usr_appid"];
$instance->usr_resellerid = $data["usr_resellerid"];
$instance->usr_marketid = $data["usr_marketid"];
$instance->usr_os = $data["usr_os"];

return $instance;
}

function _isValid(){
      return true;
}
}
?>


