<?php

/**
 * categoryObject short summary.
 *
 * categoryObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class categoryObject extends levelData999Object
{

    /* Products list */
    public $productIDs = array();

    public function setProductIDs($productsList){
        if(!is_array($productsList)){
            throw new Exception('Products list is not an array');
        }

        $this->productIDs = $productsList;
    }
}
