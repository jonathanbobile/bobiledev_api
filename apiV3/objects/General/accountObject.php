<?php

class accountObject extends bobileObject {

    public $ac_id = "0";
    public $ac_type;
    public $ac_username;
    public $ac_reseller_id = "0";
    public $ac_reseller_client_token;
    public $ac_name;
    public $ac_password;
    public $ac_isverified = "0";
    public $ac_isactive = "1";
    public $ac_lang = "en";
    public $ac_country = "0";
    public $ac_phone;
    public $ac_phone_valid = "0";
    public $ac_facebook_id;
    public $ac_facebook_token;
    public $ac_conf_code;
    public $ac_unsub = "0";
    public $ac_last_bizid = "0";
    public $ac_environment = "DEV";
    public $ac_wizard_status = "none";
    public $ac_need_change_pass = "0";

    function __construct(){}

    public static function withData($data){

        if (!isset($data["ac_id"])){
            throw new Exception("accountObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->ac_id = $data["ac_id"];
        $instance->ac_type = $data["ac_type"];
        $instance->ac_username = $data["ac_username"];
        $instance->ac_reseller_id = $data["ac_reseller_id"];
        $instance->ac_reseller_client_token = $data["ac_reseller_client_token"];
        $instance->ac_name = $data["ac_name"];
        $instance->ac_password = $data["ac_password"];
        $instance->ac_isverified = $data["ac_isverified"];
        $instance->ac_isactive = $data["ac_isactive"];
        $instance->ac_lang = $data["ac_lang"];
        $instance->ac_country = $data["ac_country"];
        $instance->ac_phone = $data["ac_phone"];
        $instance->ac_phone_valid = $data["ac_phone_valid"];
        $instance->ac_facebook_id = $data["ac_facebook_id"];
        $instance->ac_facebook_token = $data["ac_facebook_token"];
        $instance->ac_conf_code = $data["ac_conf_code"];
        $instance->ac_unsub = $data["ac_unsub"];
        $instance->ac_last_bizid = $data["ac_last_bizid"];
        $instance->ac_environment = $data["ac_environment"];
        $instance->ac_wizard_status = $data["ac_wizard_status"];
        $instance->ac_need_change_pass = $data["ac_need_change_pass"];
      
        return $instance;
    }

    function _isValid(){       
        return true;
    }
}
?>