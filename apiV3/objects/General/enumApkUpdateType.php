<?php

/**
 * enumApkUpdateType - the types of messages that the socket sends to the clients.
 * 
 *
 * @version 1.0
 * @author DanyG
 */
class enumApkUpdateType extends Enum{

    const no_update_needed = 'no_update_needed';
   
    const have_update = 'have_update';

    const must_update = 'must_update';
    
}

    

