<?php

class a1LicenseObject extends bobileObject {

    public $a1_id = "0";
    public $a1_request_id;
    public $a1_asset_id;
    public $a1_biz_id = "0";
    public $a1_request_type;
    public $a1_plan_id = "0";
    public $a1_customer_id;
    public $a1_customer_organization;
    public $a1_customer_first_name;
    public $a1_customer_last_name;
    public $a1_customer_email;
    public $a1_customer_phone;
    public $a1_date;
    public $a1_environment;

    function __construct(){}

    public static function withData($data){

        if (!isset($data["a1_id"])){
            throw new Exception("a1LicenseObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->a1_id = $data["a1_id"];
        $instance->a1_request_id = $data["a1_request_id"];
        $instance->a1_asset_id = $data["a1_asset_id"];
        $instance->a1_biz_id = $data["a1_biz_id"];
        $instance->a1_request_type = $data["a1_request_type"];
        $instance->a1_plan_id = $data["a1_plan_id"];
        $instance->a1_customer_id = $data["a1_customer_id"];
        $instance->a1_customer_organization = $data["a1_customer_organization"];
        $instance->a1_customer_first_name = $data["a1_customer_first_name"];
        $instance->a1_customer_last_name = $data["a1_customer_last_name"];
        $instance->a1_customer_email = $data["a1_customer_email"];
        $instance->a1_customer_phone = $data["a1_customer_phone"];
        $instance->a1_date = $data["a1_date"];
        $instance->a1_environment = $data["a1_environment"];

        return $instance;
    }

    public function getPlanId($data){

        $planId = 0;
        if(isset($data->asset->items) && count($data->asset->items) > 0){
            foreach ($data->asset->items as $item) {
                if ($item->quantity > 0) {
                    $planArray = explode("_",$item->id);
                    $planId = $planArray[1];
                }
            }
        }
        return $planId;
    }

    function _isValid(){       
        return true;
    }
}
?>