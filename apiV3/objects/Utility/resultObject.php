<?php

/**
 * resultObject short summary.
 *
 * resultObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class resultObject
{
    public $code;
    public $message = '';
    public $data;

    public static function withData($code,$message = '',$data = array()){
        $instance= new self();

        $instance->code = $code;
        $instance->message = $message;
        $instance->data = $data;

        return $instance;
    }
}
