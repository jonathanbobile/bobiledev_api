<?php

class Admin extends Controller{

    public $mobileRequestObject = array();
    public $adminModel;

    public function __construct(){
        parent::__construct();
        set_time_limit(0);
        $this->mobileRequestObject = mobileRequestObject::withData($_REQUEST);
        $this->adminModel = new adminModel($this->mobileRequestObject);
    }
    
    function login(){
        try{
            $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "1";
            $pAppid = (isset($_REQUEST["appId"]) && $_REQUEST["appId"] != "") ? $_REQUEST["appId"] : "0";
            $username = $_REQUEST["username"];
            $password = $_REQUEST["password"];

            $resellerID = $this->mobileRequestObject->resellerId > 0 ? $this->mobileRequestObject->resellerId : 0;
            $domain = "";
            $wl_domain = "app.bobile.com";
            $ownerName = "";
            $reseller_show_preview = 0;
            $reseller_billed = 0;
            if($resellerID > 0){
                $partnersModel = new partnerModel();
                $resellerResult = $partnersModel->getResellerByID($resellerID);
                
                
                if($resellerResult->code == 1){
                    
                    $reseller = $resellerResult->data;
                    if($reseller->reseller_deactivated == 1){
                        return $this->returnAnswer(resultObject::withData(0,'reseller_deactivated'));
                    }

                    $wl_domain = $reseller->reseller_wl_domain;
                    $domain = $reseller->reseller_wl_url;
                    $ownerName = $reseller->reseller_full_name;
                    $reseller_show_preview = $reseller->reseller_show_preview;
                    $reseller_billed = $reseller->reseller_billed;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'no_reseller_found'));
                }
            }

            $accountManager = new accountManager();

            $accountResult = $accountManager->getAccountForLogin($username,$password,$resellerID,$type);
            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }

            $account = $accountResult->data;


            $response = array();

            $response['account_id'] = $account->ac_id;
            $response['domain'] = $domain;
            $response['wl_domain'] = $wl_domain;
            $response["owner_name"]  = $ownerName != "" ? $ownerName : $account->ac_name;
            $response["owner_id"]  = isset($account->owner->owner_id) ? $account->owner->owner_id : -1;
            $response["reseller_id"]  = isset($account->owner->owner_reseller_id) ? $account->owner->owner_reseller_id : -1;
            $response["reseller_verified"] = $reseller_show_preview;
            $response["client_id"]  = "0";
            $response["reseller_billed"]  = $reseller_billed;            
            $response["owner_lang"]  = $account->ac_lang;

            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizListForAccount(){
        try{
            $skip = isset($_REQUEST['skip']) ? $_REQUEST['skip'] : 0;
            $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : '';
            $take = 20;
            $bizList = $this->adminModel->getFullBizListForAccountID($this->mobileRequestObject->accountID,$skip,$take,$keyword);

            return $this->returnAnswer(resultObject::withData(1,'',$bizList));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function logout($userName){
        try{
            return $this->returnAnswer($this->adminModel->logoutUser($userName));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizData(){
        try{
            $connectedAccountID = $_REQUEST['connected_account'];

            $accountManager = new accountManager();

            $accountResult = $accountManager->getAccountById($connectedAccountID);

            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }

            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $bizResult = $bizModel->getBizForAdmin($accountResult->data);            

            return $this->returnAnswer($bizResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }
    
    function getSettings(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $bizSettingsResult = $bizModel->getBizSettingsForAdmin();

            return $this->returnAnswer($bizSettingsResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setSettings(){
        try{
            
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }
}
