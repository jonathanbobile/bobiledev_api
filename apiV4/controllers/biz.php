<?php

class Biz extends Controller{

    public $mobileRequestObject = array();

    public function __construct(){
        parent::__construct();
        set_time_limit(0);
        $this->mobileRequestObject = mobileRequestObject::withData($_REQUEST);
    }
    
    public function index(){

    }

    public function getBiz(){

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $bizModel->mobileRequestObject = $this->mobileRequestObject;

        $bizResult = $bizModel->getBizStructure();
        bizManager::trackStats($this->mobileRequestObject,"api_get_biz");
        return $this->returnAnswer($bizResult);
    }

    public function getBizForWebMember(){

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $bizModel->mobileRequestObject = $this->mobileRequestObject;

        $response = array();
        $bizResult = $bizModel->getBizStructure();
        if($bizResult->code == 1){
            $response['biz'] = $bizResult->data;
            $response['owner'] = bizManager::getOwnerOrResellerFromBizID($this->mobileRequestObject->bizid);
            $response['google_tag'] = bizManager::getGoogleAdsTagForBiz($this->mobileRequestObject->bizid);
            $response['wl_brand_name'] = "";
            if(bizManager::isAppFromReseller($this->mobileRequestObject->bizid)){
                $reseller = bizManager::getOwnerOrResellerFromBizID($this->mobileRequestObject->bizid);

                $response['wl_brand_name'] = isset($reseller['reseller_wl_brand_name']) ? $reseller['reseller_wl_brand_name'] : "";
            }
        }

        return $this->returnAnswer(resultObject::withData($bizResult->code,'',$response));
    }

    public function getLevelData(){

        $levelDataManager = new levelDataManager($this->mobileRequestObject->mod_id);
        $levelDataManager->mobileRequestObject = $this->mobileRequestObject;

        $bizResult = $levelDataManager->getBizLevelData();
        bizManager::trackStats($this->mobileRequestObject,"api_get_levels");
        return $this->returnAnswer($bizResult);
    }

    /************************************* */
    /*          BOOKING FUNCTIONS          */
    /************************************* */

    public function getMeetingsList(){
        try{
            
            $emp_id = $_REQUEST["emp_id"] != '' ? $_REQUEST["emp_id"] : 0;
            $meet_type_id = $_REQUEST["meet_type"] != '' ? $_REQUEST["meet_type"] : 0;
            $start_date = mktime(0, 0, 0);
            if($_REQUEST["date_from"] != ""){
                $qure_date = mktime(0, 0, 0);
                $day = date('d',$_REQUEST["date_from"]);
                $month = date('m',$_REQUEST["date_from"]);
                $year = date('Y',$_REQUEST["date_from"]);
                $start_date = mktime(0, 0, 0, $month, $day, $year);

                if($start_date <= $qure_date) {

                    $start_date = $qure_date;
                }
            }
            $offset = $_REQUEST["offset"] != "" ? $_REQUEST["offset"] : 0;

            
           
            $result = bookingManager::getMeetingsList($emp_id,$meet_type_id,$start_date,$offset);

            $response = new stdClass();

            $response->rows = $result->data;

            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getMeetTypes(){
        try{
            $emp_id = isset($_REQUEST['emp_id']) ? $_REQUEST['emp_id'] : 0;

            if($emp_id == 0){
                $servicesResult = bookingManager::getAvailableServicesListForBizID($this->mobileRequestObject->bizid);
            }
            else{
                $employeeResult = bookingManager::getEmployeeByID($emp_id);
                if($employeeResult->code == 1){
                    $servicesResult = bookingManager::getServicesListForEmployee($employeeResult->data);
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0));
                }
            }

            $services = $servicesResult->data;

            $response = new stdClass();

            $response->rows = array();

            foreach ($services as $service)
            {
            	$response->rows[] = $service->getAPIFormattedArray($this->mobileRequestObject);
            }

            return $this->returnAnswer(resultObject::withData(1,'',$response));
            
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getEmployees(){
        try{
            $meet_type_id = isset($_REQUEST["meet_type"]) && $_REQUEST["meet_type"] != "" ? $_REQUEST["meet_type"] : 0;

            if($meet_type_id == 0){
                $employeeResult = bookingManager::getEmployeesForBizID($this->mobileRequestObject->bizid);
            }
            else{
                $serviceResult = bookingManager::getServiceByID($meet_type_id);
                if($serviceResult->code == 1){
                    $employeeResult = bookingManager::getEmployeesForService($serviceResult->data);
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0));
                }
            }

            $employees = $employeeResult->data;

            $response = new stdClass();

            $response->rows = array();

            foreach ($employees as $employee)
            {
            	$response->rows[] = $employee->getAPIFormattedArray($this->mobileRequestObject);
            }
            
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getClasses(){
        $classesResult = bookingManager::getAvailableClassesForBiz($this->mobileRequestObject->bizid);

        if($classesResult->code == 0){
            return $this->returnAnswer($classesResult);
        }

        $classResult = array();

        foreach ($classesResult->data as $class)
        {
        	$classForamtted = $class->getAPIFormattedArray($this->mobileRequestObject);
            $classResult[] = $classForamtted;
        }
        
        $result = resultObject::withData(1,'',$classResult);
        return $this->returnAnswer($result);
    }
  
    public function getClass(){
        try{
            $classId = $_REQUEST["classId"]; 

            $response = array();
            $classResult = bookingManager::getClassByID($classId);

            if($classResult->code == 1){
                $response['class_data'] = $classResult->data->getAPIFormattedArray($this->mobileRequestObject);
            }
            else{
                return $this->returnAnswer(resultObject::withData(0,'no_class'));
            }

            $datesResult = bookingManager::getClassDatesForClassByClassID($classId);

            if($datesResult->code == 1){
                $response['class_dates'] = array();

                foreach ($datesResult->data as $date)
                {
                	$dateOut = $date->getAPIFormattedArray($this->mobileRequestObject);
                    $response['class_dates'][] = $dateOut;
                }
                
            }

            return $this->returnAnswer(resultObject::withData(1,'',$response));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getClassDates(){
        try{
            $classId = $_REQUEST["classId"];

            $classDatesResult = bookingManager::getClassDatesForClassByClassID($classId);

            $classDates = array();
            if($classDatesResult->code == 1){
                foreach ($classDatesResult->data as $classDate)
                {
                	$classDates[] = $classDate->getAPIFormattedArray($this->mobileRequestObject);
                }
                
            }

            return $this->returnAnswer(resultObject::withData(1,'',$classDates));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*          MODULES FUNCTIONS          */
    /************************************* */

    public function getShopDisplay(){

        $levelDataManager = new levelDataManager(0);
        $levelDataManager->mobileRequestObject = $this->mobileRequestObject;

        $bizResult = $levelDataManager->getShopDisplay();

        return $this->returnAnswer($bizResult);
    }

    public function claimFreeCoupon(){

        $customerOrderManager = new customerOrderManager();
        $customerOrderObject = new customerOrderObject();
        $customerOrderItemObj = new customerOrderItemObject ();

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $eSettingsResult = $bizModel->getBizStoreSettings();

        if($eSettingsResult->code != 1){
            return $this->returnAnswer($eSettingsResult);
        }

        $amount = $_REQUEST["amount"] == 'null' || $_REQUEST["amount"] == '' ? 0 : $_REQUEST["amount"];

        $customerOrderObject->cto_amount = 0;
        $customerOrderObject->cto_currency = $eSettingsResult->data->ess_currency;
        $customerOrderObject->cto_device_id = $this->mobileRequestObject->deviceID;
        $customerOrderObject->cto_cust_id = $this->mobileRequestObject->server_customer_id;
        $customerOrderObject->cto_biz_id = $this->mobileRequestObject->bizid;
        $customerOrderObject->cto_status = enumOrderStatus::delivered;
        $orderResult = $customerOrderManager->initiateEmptyOrderForCustomer($this->mobileRequestObject->server_customer_id,$customerOrderObject);

        if($orderResult->code != 1){
            return $this->returnAnswer($orderResult);
        }

        $customerOrderItemObj->tri_order_id = $orderResult->data->cto_id;
        $customerOrderItemObj->tri_name = $_REQUEST["itmName"] == "null" || $_REQUEST["itmName"] == "" ? "" : $_REQUEST["itmName"];
        $customerOrderItemObj->tri_quantity = 1;
        $customerOrderItemObj->tri_price = 0;
        $customerOrderItemObj->tri_without_vat = 0;
        $customerOrderItemObj->tri_vat = 0;
        $customerOrderItemObj->tri_item_id = $_REQUEST["item_id"] == "null" || $_REQUEST["item_id"] == "" ? 0 : $_REQUEST["item_id"];
        $customerOrderItemObj->tri_item_type = 1;
        $customerOrderItemObj->tri_itm_row_id = $_REQUEST["couponID"] == "null" || $_REQUEST["couponID"] == "" ? 0 : $_REQUEST["couponID"];
        $customerOrderItemObj->tri_redeem_code = utilityManager::generateRedeemCode($this->mobileRequestObject->server_customer_id."1");
        $itemResult = $customerOrderManager->addCustomerOrderItem($customerOrderItemObj);

        if($itemResult->code != 1){
            return $this->returnAnswer($itemResult);
        }
        
        eventManager::actionTrigger(enumCustomerActions::claimedFreeCoupon,$this->mobileRequestObject->server_customer_id, "coupon",$customerOrderItemObj->tri_name,$this->mobileRequestObject->deviceID,$itemResult->data);

        $notifications = new notificationsManager();
        $params["coupon_header"] = $customerOrderItemObj->tri_name;
        $notifications->addNotification($this->mobileRequestObject->bizid,enumActions::claimCoupon,false,$params); 
        
        $benefitResult = customerManager::addRewardToCustomer($this->mobileRequestObject->server_customer_id,
                                                                'coupon',
                                                                $customerOrderItemObj->tri_redeem_code,
                                                                $itemResult->data);
        if($benefitResult->code == 1){
            couponsManager::addClaimToCoupon($_REQUEST["couponID"],1);
            bizManager::trackStats($this->mobileRequestObject,"api_claim_coupon");
            customerManager::addBadgeToCustomer($this->mobileRequestObject->server_customer_id,enumBadges::benefits);
            customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer( resultObject::withData(1,'ok') );
        }else{
            return $this->returnAnswer($benefitResult);
        }
    }

    public function unclaimFreeCoupon(){

        $customerOrderManager = new customerOrderManager();

        $resultData = $customerOrderManager->getOrderAndItemByCouponAndRedeemCode($this->mobileRequestObject->server_customer_id,
                                                                                   $this->mobileRequestObject->bizid,
                                                                                   $_REQUEST["couponID"],
                                                                                   $_REQUEST["redeemCode"]);

        if(isset($resultData['tri_redeem_date']) && $resultData['tri_redeem_date'] != ""){
            return $this->returnAnswer(resultObject::withData(0,'already_redeemed'));
        }

        $orderResult = $customerOrderManager->getCustomerOrderByID($resultData["cto_id"]);

        if($orderResult->code != 1){
            return $this->returnAnswer($orderResult);
        }

        $orderResult->data->cto_cancelled = 1;
        $orderResultUpdate = $customerOrderManager->updateCustomerOrder($orderResult->data);

        if($orderResultUpdate->code != 1){
            return $this->returnAnswer($orderResultUpdate);
        }

        if(count($orderResult->data->items) > 0){
            foreach ($orderResult->data->items as $customerOrderItemObject)
            {
                $customerOrderItemObject->tri_redeem_code = "";
                $customerOrderManager->updateCustomerOrderItem($customerOrderItemObject);

                customerManager::deleteCustomerBenefitByItemId($customerOrderItemObject->tri_id);
            }            
        }
        couponsManager::subtractClaimToCoupon($_REQUEST["couponID"],1);
        customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$this->mobileRequestObject->server_customer_id);
        return $this->returnAnswer( resultObject::withData(1,'ok',$_REQUEST["couponID"]) );
    }

    public function getPointsShop(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $pointsShopResult = $bizModel->getPointsGrantForBiz();

            if($pointsShopResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0));
            }

            $pointsItemlist = array();
            foreach ($pointsShopResult->data as $pointsItem)
            {
                if($pointsItem->connectedItemIsValid){
                    $pointsItemlist[] = $pointsItem->getAPIFormattedArray();
                }
            }
            
            return $this->returnAnswer(resultObject::withData(1,'',$pointsItemlist));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function calculateShipping(){
        try{
            $shopManager = new shopManager();
            $itemsList = json_decode(stripslashes(str_replace('\n','',$_REQUEST["items"])));
            
            $country = $_REQUEST["shipCountry"];
            $tax = $_REQUEST["tax"];
            $result = $shopManager->calculateShipping($this->mobileRequestObject->bizid,$country,$tax,$itemsList);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
        
    }

    public function getBizShopSettings(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizid);
            $settings = $bizModel->getBizStoreSettings();
            return $this->returnAnswer($settings);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*          GENERAL FUNCTIONS          */
    /************************************* */

    public function getReviews(){
        try{
            $hasConnectedMod = false;
            if($_REQUEST["cr_item_type"] != ''){
                if($_REQUEST["cr_item_type"] == 'product'){
                    $hasConnectedMod = bizManager::hasPremiumFeature(9,$this->mobileRequestObject->bizid) && bizManager::modulesAreActive(9,$this->mobileRequestObject->bizid);
                }
                else{
                    $hasConnectedMod = bizManager::hasPremiumFeature(17,$this->mobileRequestObject->bizid) && bizManager::modulesAreActive(17,$this->mobileRequestObject->bizid);
                }
            }

            $cr_item_type = $hasConnectedMod ? $_REQUEST["cr_item_type"] : '';
            $cr_item_id = $hasConnectedMod ? $_REQUEST["cr_item_id"] : 0;
            $cr_mod_id = $_REQUEST["cr_mod_id"];
            $cr_row_id = $_REQUEST["cr_row_id"];

            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            if($cr_item_type == ""){
                $reviewsResult = $bizModel->getCustomerReviewsForModuleRow($cr_mod_id,$cr_row_id);
            }
            else{
                $reviewsResult = $bizModel->getCustomerReviewsForItem($cr_item_type,$cr_item_id);
            }

            if($reviewsResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0));
            }

            return $this->returnAnswer(resultObject::withData(1,'',$reviewsResult->data));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getNeedUpdate(){

        $customerModel = new customerModel();

        $userData = customerManager::getUserByDeviceAppMarket($this->mobileRequestObject->deviceID,$this->mobileRequestObject->appid,$this->mobileRequestObject->marketId);
        $resultCustomer = $customerModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
        if($resultCustomer->code == 1){
            $resultCustomer->data->cust_last_seen = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
            $customerModel->updateCustomer($resultCustomer->data);
        }

        if(is_array($userData)){
            $userObject = userObject::withData($userData);

            $userNeedUpdateData = bizManager::isUserNeedUpdate($userObject,$this->mobileRequestObject->bizid);

            if($userNeedUpdateData == "")
            {
                return $this->returnAnswer(resultObject::withData(1,"ok",0));
            }

            return $this->returnAnswer(resultObject::withData(1,"ok",1));

        }else{
            return $this->returnAnswer(resultObject::withData(1,"ok",0));
        }
    }

    public function setUpdated(){

        $userData = customerManager::getUserByDeviceAppMarket($this->mobileRequestObject->deviceID,$this->mobileRequestObject->appid,$this->mobileRequestObject->marketId);

        if(is_array($userData)){
            $userObject = userObject::withData($userData);
            bizManager::setUserUpdated($userObject,$this->mobileRequestObject->bizid);
        }

        return $this->returnAnswer(resultObject::withData(1,"ok"));
    }

    public function getInstagram(){

        $maxId = isset($_REQUEST['max_id']) ? $_REQUEST['max_id'] : "";
        $response = bizManager::getInstagramData($_REQUEST['instagramUser'],$maxId);

        return $this->returnAnswer($response);
    }

    public function getYelp($bizYelpId){

        //account: galtomer038@gmail.com
        //password: 2wsx@wsx
        $apiKey = "IDvx_De9zBnfkrbJAzhfHj-eNJdwb2uoUToal_0lgbuheiMGlzTFWPj3brySNdnHQgZeMJlSv71833XubwWb6qPHKoadITQiVt6nU8QrPIevndy6uk3MeLn1m54QXXYx";

        $headers = array(
                        'Authorization: bearer ' . $apiKey
                    );

        $bizData = utilityManager::curl("https://api.yelp.com/v3/businesses/$bizYelpId",$headers,"",false,"");
        $bizData = json_decode($bizData);
        $bizData->rating_image_large_url = "https://storage.googleapis.com/bbassets/images/yelp_stars/{$bizData->rating}.png";

        $bizReviews = utilityManager::curl("https://api.yelp.com/v3/businesses/$bizYelpId/reviews",$headers,"",false,"");
        $bizReviews = json_decode($bizReviews);

        $reviewsArray = array();
        if(count($bizReviews->reviews) > 0){
            foreach ($bizReviews->reviews as $review)
            {
                $review->rating_image_large_url = "https://storage.googleapis.com/bbassets/images/yelp_stars/{$review->rating}.png";
                $review->time_created = strtotime($review->time_created);
                array_push($reviewsArray,$review);
            }
            
        }
        $bizData->reviews = $reviewsArray;

        return $this->returnAnswer(resultObject::withData(1,"ok",$bizData));
    }

    public function setBizHash(){

        $hash = $_REQUEST['hash'];
        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $bizResponse = $bizModel->getBiz();
        if($bizResponse->code == 1){
            $bizResponse->data->biz_smshash = $hash;
            $bizResponse = $bizModel->updateBiz($bizResponse->data);
        }
        return $this->returnAnswer($bizResponse);
    }

    public function registerGCM(){

        bizManager::deleteGcmDevice($this->mobileRequestObject);
        $gcmId = bizManager::addGcmDevice($_REQUEST["token"],$this->mobileRequestObject);
        if($gcmId > 0){
            bizManager::updateGcmDeviceLocation($gcmId);
        }
        return $this->returnAnswer(resultObject::withData(1,"ok"));
    }

    /************************************* */
    /*            PUSH FUNCTIONS           */
    /************************************* */

    public function getPushHtml(){
        
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";

        $pushMangaer = new pushManager();
        $pushMangaer->custPushOpen($pushID,$this->mobileRequestObject->server_customer_id);
        
        $pushResult = $pushMangaer->getPushByID($pushID);

        if($pushResult->code == 1){
            $pushRow = $pushResult->data;

            $pushHTMLResult = pushManager::getPushHTML($pushRow,$this->mobileRequestObject);

            return $this->returnAnswer($pushHTMLResult);
        }
        return $this->returnAnswer($pushResult);
    }

    public function setPushDelivered(){
        
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";

        $push = new pushManager();
        $push->custPushDelivered($pushID,$this->mobileRequestObject->deviceID);

        return $this->returnAnswer( resultObject::withData(1,'ok') );
    }

    public function getGreetingPush(){
        
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";

        $push = new pushManager();
        $response = $push->getGreetingPush($pushID);
        return $this->returnAnswer($response );
    }

    public function setPushOpen(){
        
        $pushID = isset($_REQUEST["push_id"]) ? $_REQUEST["push_id"] : "0";

        $pushMangaer = new pushManager();
        $pushMangaer->custPushOpen($pushID,$this->mobileRequestObject->server_customer_id);

        customerManager::subtractOneBadgeTypeForCustomer($this->mobileRequestObject->server_customer_id,'notifications');
        eventManager::actionTrigger(enumCustomerActions::openedPush,$this->mobileRequestObject->server_customer_id, "push",'',$this->mobileRequestObject->deviceID);

        return $this->returnAnswer( resultObject::withData(1,'ok') );
    }

    
}
