<?php

class Partner extends Controller{

    public $partnerModel;

    public function __construct(){

        header("access-control-allow-origin: https://affiliate.bobile.com");

        parent::__construct();
        set_time_limit(0);

        $this->partnerModel = new partnerModel();
    }
    
    //************************* A1 ***************************//
    public function a1WebHook(){

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input);
        
        $fh = fopen("debug.txt", 'a');
        fwrite($fh, print_r($event_json,true)."\n");
        fwrite($fh, print_r($_REQUEST,true)."\n");
        fclose($fh);

        $apiConfig = $this->partnerModel->getA1ApiConfig();

        if($apiConfig["code"] == 1){
            $requests = new a1ProductRequests($apiConfig["data"]);
            $requests->templteId = "TL-028-148-034";
            $requests->process();
        }else{
            print_r($apiConfig);
        }
    }

    public function a1_manager(){

        $jwtKey = $_GET["jwt"];
        
        $responseAssetId = $this->partnerModel->getAssetId($jwtKey);
        if($responseAssetId->code == 0){

            Header( "HTTP/1.1 301 Moved Permanently" ); 
            Header( "Location: ".URL."/error/errorPage/" . utilityManager::encryptIt($responseAssetId->message,true));

        }else{

            $responseLicense = $this->partnerModel->getA1LicenseByAsset($responseAssetId->data);

            if($responseLicense->code == 1){

                $licenseObject = $responseLicense->data;
                $a1ApiKey = "G6qSF3eSoIjGbjCzTpzQt2d2RJXmCNHqw0uIECkvNb4=";

                $responseToken = $this->partnerModel->getClientTokenByEmail($a1ApiKey,$licenseObject->a1_customer_email);

                if($responseToken->code == 1){
                    Header( "HTTP/1.1 301 Moved Permanently" ); 
                    Header( "Location: https://workspace.bobile.at/admin/builder/clientLanguagRedirect/" . $responseToken->data . "/de");
                }else{
                    Header( "HTTP/1.1 301 Moved Permanently" ); 
                    Header( "Location: ".URL."/error/errorPage/" . utilityManager::encryptIt($responseToken->message,true));
                }

            }else{
                Header( "HTTP/1.1 301 Moved Permanently" ); 
                Header( "Location: ".URL."/error/errorPage/" . utilityManager::encryptIt("Wrong asset ID",true));
            }
        }
    }

    //************************* BOBILE API's ***************************//

    /**
     * Return generated clientToken from resellers API key and E-mail
     * to use for create/login to SSO
     */
    public function getClientToken(){
        
        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);

        $response = $this->partnerModel->getClientTokenByEmail($this->partnerModel->getApiKeyFromHeader($headers),$_REQUEST["email"]);
        echo json_encode($response);
    }

    /**
     * Return a list of all apps that belongs to partner
     */
    public function getPartnerApps(){
        
        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);

        $response = $this->partnerModel->getPartnerApps($this->partnerModel->getApiKeyFromHeader($headers));
        echo json_encode($response);
    }

    public function getClientApps(){
        
        $headers = apache_request_headers();
        $resellerId = $this->partnerModel->preventNotAuthorizedRequest($headers);
       
        $_REQUEST["reseller_id"] = $resellerId;
        $this->partnerModel->preventNotAuthorizedClientToken($_REQUEST,$this->partnerModel->getApiKeyFromHeader($headers));

        $response = $this->partnerModel->getClientsApps($this->partnerModel->getApiKeyFromHeader($headers),$_REQUEST["clientToken"]);
        echo json_encode($response);
    }

    public function deactivatePartnerApp(){

        $headers = apache_request_headers();
        $resellerId = $this->partnerModel->preventNotAuthorizedRequest($headers);

        $_REQUEST["reseller_id"] = $resellerId;
        $this->partnerModel->preventNotAuthorizedClientToken($_REQUEST,$this->partnerModel->getApiKeyFromHeader($headers));
        $this->partnerModel->preventNotAuthorizedAppToken($_REQUEST);
        echo $resellerId;
        $response =  $this->partnerModel->deactivatePartnerApp($_REQUEST["appToken"],$_REQUEST["clientToken"]);
        echo json_encode($response);
    }

    public function activatePartnerApp(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);
        $this->partnerModel->preventNotAuthorizedAppToken($_REQUEST);

        $response =  $this->partnerModel->activatePartnerApp($_REQUEST["appToken"]);
        echo json_encode($response);
    }

    public function getAppDetails(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);
        $this->partnerModel->preventNotAuthorizedAppToken($_REQUEST);

        $response =  $this->partnerModel->getAppDetails($this->partnerModel->getApiKeyFromHeader($headers),$_REQUEST["appToken"]);
        echo json_encode($response);
    }

    public function getCustomersList(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);
        $this->partnerModel->preventNotAuthorizedAppToken($_REQUEST);

        $type = isset($_REQUEST['type']) && $_REQUEST['type'] != "" ? $_REQUEST['type'] : "member";

        $response =  $this->partnerModel->getCustomersList($this->partnerModel->getApiKeyFromHeader($headers),$_REQUEST["appToken"],$type);
        echo json_encode($response);
    }

    public function getCustomer(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);
        $this->partnerModel->preventNotAuthorizedCustomerToken($_REQUEST);

        $response =  $this->partnerModel->getCustomer($this->partnerModel->getApiKeyFromHeader($headers),$_REQUEST["customerToken"]);
        echo json_encode($response);
    }

    public function addCustomer(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $this->partnerModel->preventNotAuthorizedAppToken($data);

        $response =  $this->partnerModel->addCustomer($data);
        echo json_encode($response);
    }
    
    public function updateCustomer(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $this->partnerModel->preventNotAuthorizedCustomerToken($data);

        $response =  $this->partnerModel->updateCustomer($data);
        echo json_encode($response);
    }

    public function addDataElementWithDelete(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;
        
        $this->partnerModel->preventNotAuthorizedAppToken($data);

        if(count($data["data"]) > 0){

            $biz_id = utilityManager::decodeFromHASH($data['appToken']); 

            $levelDataManager = new levelDataManager($data["module_id"]);
            $levelDataManager->deleteLevelDataElements($biz_id,$data["level_number"],$data["parent_id"]);

            foreach ($data["data"] as $element)
            {      
                $element["appToken"] = $data["appToken"];
                $element["module_id"] = $data["module_id"];
                $element["parent_id"] = $data["parent_id"];
                $element["level_number"] = $data["level_number"];
               
                $this->partnerModel->checkLevelDataInsertRequest($element);
                
            	$responseAddElement =  $this->partnerModel->addDataElement($element);
                if($responseAddElement->code != 1){
                    echo json_encode($responseAddElement);
                    exit;
                }
            }            
        }

        echo json_encode(resultObject::withData(1,"Ok"));
    }

    public function addProduct(){

        $headers = apache_request_headers();
        $this->partnerModel->preventNotAuthorizedRequest($headers);

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $bizId = $this->partnerModel->preventNotAuthorizedAppToken($data);

        $response =  shopManager::addProducts($bizId,$data["products"]);
        echo json_encode($response);
    }
}
