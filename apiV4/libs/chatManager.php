<?php

/**
 * chatManager short summary.
 *
 * chatManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class chatManager extends Manager
{
    public $mobileRequestObject = array();

    function __construct(){
        parent::__construct();
    }

    /************************************* */
    /*   CHAT - PUBLIC                     */
    /************************************* */

    public function sendChatFromClient(chatMessageObject $chatObject){
        try{
            $chatID = $this->addChatMessageDB($chatObject);

            if($chatID <= 0){
                return resultObject::withData(0);
            }

            $params = array();
            $params['chatID'] = $chatID;
            $params['customerID'] = $chatObject->ch_customer;
            $params['deviceID'] = $this->mobileRequestObject->deviceID;
            utilityManager::asyncTask("http://127.0.0.1/".MVC_NAME.'/eventAsync/sendInstantClientChat',$params);
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($chatObject));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getUnreadChatMessagesForBiz($bizID){
        try{
            return $this->getUnreadChatMessagesForBiz_DB($bizID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CHATMESSAGE - PUBLIC        */
    /************************************* */

    /**
     * Insert new chatMessageObject to DB
     * Return Data = new chatMessageObject ID
     * @param chatMessageObject $chatMessageObj 
     * @return resultObject
     */
    public function addChatMessage(chatMessageObject $chatMessageObj){       
        try{
            $newId = $this->addChatMessageDB($chatMessageObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($chatMessageObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get chatMessage from DB for provided ID
     * * Return Data = chatMessageObject
     * @param int $chatMessageId 
     * @return resultObject
     */
    public function getChatMessageByID($chatMessageId){
        
        try {
            $chatMessageData = $this->loadChatMessageFromDB($chatMessageId);
            
            $chatMessageObj = chatMessageObject::withData($chatMessageData);
            $result = resultObject::withData(1,'',$chatMessageObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$chatMessageId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update chatMessage in DB
     * @param chatMessageObject $chatMessageObj 
     * @return resultObject
     */
    public function updateChatMessage(chatMessageObject $chatMessageObj){        
        try{
            $this->upateChatMessageDB($chatMessageObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($chatMessageObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete chatMessage from DB
     * @param int $chatMessageID 
     * @return resultObject
     */
    public function deleteChatMessageById($chatMessageID){
        try{
            $this->deleteChatMessageByIdDB($chatMessageID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$chatMessageID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getLatestMessagesForCustomer($custId,$lastID = 0){
   
        $messagesArray = array();
        $messagesArray['rows'] = array();
        try{
            $chatList = $this->loadLatestMessagesFromDB($custId,$this->mobileRequestObject->bizid,$lastID);

            if(count($chatList) > 0)
            {
                foreach ($chatList as $oneChat) 
                {
                    $chatMessageObject = chatMessageObject::withData($oneChat);

                    $chatMessageObject->cust_id=($oneChat["ch_dir"] == 1) ? "" : $this->mobileRequestObject->deviceID;
                    $chatMessageObject->cust_pic=($oneChat["cust_pic"] == null || $oneChat["ch_dir"] == 1) ? "" : $oneChat["cust_pic"];
                    $chatMessageObject->ch_message = stripslashes($chatMessageObject->ch_message);
                    array_push($messagesArray['rows'],$chatMessageObject);
                }
            }
            return resultObject::withData(1,"ok",$messagesArray);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custId));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function setChatMessagesAsReedByCustomer($cust_id){

        $instance = new self();

        $sql = "UPDATE tbl_chat 
                SET ch_new = 0  
                WHERE ch_customer = $cust_id
                AND ch_dir = 1
                AND ch_new = 1";

        $instance->db->execute($sql);
    }

    /************************************* */
    /*   BASIC CHATMESSAGE - DB METHODS           */
    /************************************* */

    private function addChatMessageDB(chatMessageObject $obj){

        if (!isset($obj)){
            throw new Exception("chatMessageObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_chat SET 
                                        ch_user = {$obj->ch_user},
                                        ch_customer = {$obj->ch_customer},
                                        ch_message = '".addslashes($obj->ch_message)."',
                                        ch_dir = {$obj->ch_dir},
                                        ch_new = {$obj->ch_new}
                                                 ");
        return $newId;
    }

    private function loadChatMessageFromDB($chatMessageID){

        if (!is_numeric($chatMessageID) || $chatMessageID <= 0){
            throw new Exception("Illegal value chatMessageID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_chat WHERE ch_id = $chatMessageID");
    }

    private function upateChatMessageDB(chatMessageObject $obj){

        if (!isset($obj->ch_id) || !is_numeric($obj->ch_id) || $obj->ch_id <= 0){
            throw new Exception("chatMessageObject value must be provided");             
        }

        $ch_timeDate = isset($obj->ch_time) ? "'".$obj->ch_time."'" : "null";

        $this->db->execute("UPDATE tbl_chat SET 
                                ch_user = {$obj->ch_user},
                                ch_customer = {$obj->ch_customer},
                                ch_time = $ch_timeDate,
                                ch_message = '".addslashes($obj->ch_message)."',
                                ch_dir = {$obj->ch_dir},
                                ch_new = {$obj->ch_new}
                                WHERE ch_id = {$obj->ch_id} 
                                         ");
    }

    private function deleteChatMessageByIdDB($chatMessageID){

        if (!is_numeric($chatMessageID) || $chatMessageID <= 0){
            throw new Exception("Illegal value chatMessageID");             
        }

        $this->db->execute("DELETE FROM tbl_chat WHERE ch_id = $chatMessageID");
    }

    private function loadLatestMessagesFromDB($custId, $bizId,$lastID = 0){

        if (!is_numeric($custId) || $custId <= 0){
            throw new Exception("Illegal value custId");             
        }

        if (!isset($bizId) || !is_numeric($bizId) || $bizId <= 0){
            throw new Exception("Illegal value bizId");             
        }
        $chatSQL = "SELECT *
                    FROM tbl_chat,tbl_customers 
                    WHERE ch_customer=cust_id
                    and ch_user = $bizId
                    and cust_id = $custId
                    and ch_id > $lastID";

        return $this->db->getTable( $chatSQL );
    }

    private function getUnreadChatMessagesForBiz_DB($bizID){
        return $this->db->getVal("select count(ch_id) FROM tbl_chat,tbl_customers where ch_customer=cust_id and ch_new = 1 and ch_dir=0 and ch_user=$bizID");
    }
}
