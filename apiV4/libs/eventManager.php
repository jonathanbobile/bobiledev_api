<?php

/**
 * eventManager short summary.
 *
 * eventManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class eventManager
{
    protected $db;

    function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }
        
    }

    public function addBizDefaultEvents($biz_id){
        $sql = "INSERT INTO tbl_biz_event(bevent_biz_id,bevent_event_id,bevent_points,bevent_message)
                SELECT $biz_id,event_id,event_def_points,event_def_message FROM tbl_events events WHERE event_is_default = 1 AND event_usage = 'biz'
                ON DUPLICATE KEY UPDATE bevent_points = events.event_def_points, bevent_message = events.event_def_message";
       
        $this->db->execute($sql);

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = array();

        return $result;
    }
    
    /**
     * Add action for customers
     * @param actionTriggerObject $actionTriggerObject 
     * @return resultObject
     */
    public function addCustomerAction(actionTriggerObject $actionTriggerObject){

        $customersModel = new customerModel();
        $customerHistoryObj = new customerHistoryObject();
        $customerHistoryObj->ch_cust_id = $actionTriggerObject->customerId;
        $customerHistoryObj->ch_action_id = $actionTriggerObject->actionId;
        $customerHistoryObj->ch_status = $actionTriggerObject->status;
        $customerHistoryObj->ch_replace_text = $actionTriggerObject->replace;
        $customerHistoryObj->ch_device_id = $actionTriggerObject->deviceId;
        $customerHistoryObj->ch_external_type = $actionTriggerObject->action_type;
        $customerHistoryObj->ch_external_id = $actionTriggerObject->external_id;
        $customerHistoryObj->ch_created = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
        if(is_array($actionTriggerObject->data)){
            foreach ($actionTriggerObject->data as $key => $entry)
            {
                $customerHistoryObj->$key = $entry;
            }
        }

        return $customersModel->addCustomerHistory($customerHistoryObj);
    }

    /**
     * Get events that are connected to biz
     * @param mixed $biz_id 
     * @return array
     */
    public function getBizEvents($biz_id){
        
        
        $sql = "SELECT * FROM tbl_biz_event,tbl_events
                WHERE bevent_event_id = event_id
                AND bevent_biz_id = $biz_id
                AND event_usage = 'biz'
                ORDER BY event_index ASC";//Hide invite friend for now

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $this->db->getTable($sql);

        return $result;
    }

    /**
     * Get a single biz-event entry
     * @param mixed $bevent_id 
     */
    public function getBizEvent($bevent_id){
        $sql = "SELECT * FROM tbl_biz_event,tbl_events 
                    WHERE bevent_event_id = event_id
                    AND bevent_id = $bevent_id";

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $this->db->getRow($sql);

        return $result;
    }

    /**
     * Get events for biz filtered by action
     * @param mixed $biz_id 
     * @param mixed $action_id 
     * @return array
     */
    public function getBizEventsForAction($biz_id,$action_id){
        $sql = "SELECT * FROM tbl_biz_event,tbl_events
                WHERE bevent_event_id = event_id
                AND bevent_is_active = 1
                AND bevent_biz_id = $biz_id
                AND event_action_id = $action_id
                AND event_active = 1";

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $this->db->getTable($sql);

        return $result;
    }

    /**
     * get event for biz filtered by ID
     * @param mixed $biz_id 
     * @param mixed $event_id 
     * @return array
     */
    public function getEventByBizAndId($biz_id,$event_id){

        $sql = "SELECT * FROM tbl_biz_event 
                    WHERE bevent_biz_id = $biz_id
                    AND bevent_event_id = $event_id";

        $result = array();
        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $this->db->getRow($sql);

        return $result;
    }

    /**
     * Gets all events that are NOT connected to biz
     * @param mixed $biz_id 
     * @return array
     */
    public function getValidEventsForBiz($biz_id){
        $instance = new self();

        $sql = "SELECT tbl_events.* FROM tbl_events 
                    LEFT JOIN (SELECT * FROM tbl_biz_event WHERE bevent_biz_id = $biz_id) bevent ON bevent.bevent_event_id = event_id
                    WHERE bevent.bevent_id IS NULL
                    AND event_usage = 'biz'
                    AND event_active = 1";

        $list = $instance->db->getTable($sql);


        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $list;

        return $result;
    }

    /**
     * Get events that are marked for scratch cards
     * @return array
     */
    public function getScratchEventsForAction($action_id){
        $sql = "SELECT * FROM tbl_events
                WHERE event_usage = 'scratch'
                AND event_action_id = $action_id
                AND event_active = 1";

        $list = $this->db->getTable($sql);


        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $list;

        return $result;

    }

    /**
     * Update or insert a new entry in biz-events
     * @param mixed $biz_id 
     * @param mixed $bevent_id 
     * @param mixed $event_id 
     * @param mixed $points 
     * @return array
     */
    public function updateBizEvent($biz_id,$bevent_id,$points,$message = ''){
        $bevent_id = $bevent_id == 0 ? 'null' : $bevent_id;
        $proMessage = addslashes($message);
        $sql = "INSERT INTO tbl_biz_event SET
                    bevent_id = $bevent_id,
                    bevent_biz_id = $biz_id,
                    bevent_points = $points,
                    bevent_message = '$proMessage'
                ON DUPLICATE KEY UPDATE
                    bevent_points = $points,
                    bevent_message = '$proMessage'";

        $entryid = $this->db->execute($sql);

        $result = array();

        if($entryid >= 0){
            $result['code'] = 1;
            $result['message'] = "ok";

            $entryid = $entryid == 0 ? $bevent_id : $entryid;

            $sql = "SELECT * FROM tbl_biz_event,tbl_events
                WHERE bevent_event_id = event_id
                AND bevent_biz_id = $biz_id
                AND bevent_id = $entryid";
            $result['data'] = $this->db->getRow($sql);
        }
        else{
            $result['code'] = 0;
            $result['message'] = "failed_add_entry";
        }

        return $result;
    }

    /**
     * Remove biz event from table
     * @param mixed $entryid 
     */
    public function deleteBizEvent($entryid){
        $sql = "DELETE FROM tbl_biz_event WHERE bevent_id = $entryid";

        $this->db->execute($sql);

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = array();

        return $result;
    }

    /**
     * Sets the state of the biz event
     * @param mixed $biz_id 
     * @param mixed $entryid 
     * @param mixed $state 
     * @return array
     */
    public function setBizEventState($biz_id,$entryid,$state){
        $sql = "UPDATE tbl_biz_event SET bevent_is_active = $state WHERE bevent_id = $entryid AND bevent_biz_id = $biz_id";

        $this->db->execute($sql);

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = array();

        return $result;
    }
    
    /**
     * Add customer_event entry 
     * @param mixed $cust_id 
     * @param mixed $event_id 
     * @return array
     */
    public function addCustomerEvent($cust_id,$event_id,$cust_history_id = 0){
        $sql = "SELECT cust_biz_id FROM tbl_customers WHERE cust_id = $cust_id";

        $biz_id = $this->db->getVal($sql);

        $sql = "INSERT INTO tbl_customer_events SET 
                cevent_cust_id = $cust_id,
                cevent_event_id = $event_id,
                cevent_biz_id = $biz_id,
                cevent_cust_history_id = $cust_history_id";

        $entryid = $this->db->execute($sql);


        $result = array();

        if($entryid > 0){
            $result['code'] = 1;
            $result['message'] = "ok";
            $result['data'] = $entryid;
        }
        else{
            $result['code'] = 0;
            $result['message'] = "failed_add_entry";
        }

        return $result;
    }

    /**
     * Calls the function that executes all the methods for checking and triggering events
     * @param mixed $action_id 
     * @param mixed $customer_id 
     */
    public static function actionTrigger($action_id,$customer_id,$status = '',$replace = '',$device_id = '',$external_id = 0,$data = array()){
        $params = array();
        $params['cust_id'] = $customer_id;
        $params['act_id'] = $action_id;
        $params['status'] = $status;
        $params['device_id'] = $device_id;
        $params['replace'] = $replace;
        $params['external_id'] = $external_id;
        utilityManager::asyncTask("https://".API_SERVER."/".MVC_NAME."/eventAsync/triggeringAction/",$params);
        return;
    }

    public function actionTriggering(actionTriggerObject $actionTriggerObject){

        if($actionTriggerObject->customerId == 0){
            return -1;
        }

        $customersModel = new customerModel();
        $result = $customersModel->getCustomerWithID($actionTriggerObject->customerId);

        if($result->code == 1 && $result->data->cust_id != 0){

            $customer = $result->data;
            $custActResult = $this->addCustomerAction($actionTriggerObject);

            if($custActResult->code == 1){
                if($actionTriggerObject->actionId == enumCustomerActions::completedPurchase){
                    sleep(10);
                }
                $this->actionTriggered($custActResult->data);
                
                if($customer->cust_status == 'guest' || $customer->cust_status == 'member'){//only guests and members are tracked
                    utilityManager::asyncTask("https://".API_SERVER."/".MVC_NAME."/eventAsync/triggerCustomerStageCheck/".$actionTriggerObject->customerId);
                }
            }
            return $custActResult->code;
        }

        $result =  resultObject::withData(0,"no_customer");
        return $this->returnAnswer($result);
        
    }

    
    /**
     * Executes the event method checks for the customer action
     * and grants rewards
     * @param mixed $cust_action_id 
     * @return void
     */
    public function actionTriggered($cust_action_id){
       
       
        $customerData = $this->getCustomersHistoryByAction($cust_action_id)["data"];
        $customerObject = customerObject::withData($customerData);
        $custHistoryObject = customerHistoryObject::withData($customerData);
       
        if($customerObject->cust_biz_id == 0 || $customerObject->cust_biz_id == ""){
            return;
        }

        $bizModel = new bizModel($customerObject->cust_biz_id);
        $result =  $bizModel->getBiz();
        if($result->code = 0){
            return;
        }
        $bizObject = $result->data;

        if($bizObject->biz_x == 0){
            return;
        }

        if($custHistoryObject->ch_device_id != ''){
            $customerMsgTarget = $customerObject->cust_id.'_'.$custHistoryObject->ch_device_id;
        }
        else{
            $customerMsgTarget = $customerObject->cust_id.'_'.$customerObject->cust_mobile_serial;
        }

        $customerModel = new customerModel();
       
       
        $bizEvents = $this->getBizEventsForAction($customerObject->cust_biz_id,$custHistoryObject->ch_action_id);
        
        if($bizEvents['code'] == 1){
           
            foreach ($bizEvents['data'] as $event)
            {
                $triggered = false;
               
                if(method_exists($this,'checkEvent_'.$event['event_type'])){                  
                    $triggered = $this->{'checkEvent_'.$event['event_type']}($custHistoryObject,$event);
                }

                if($triggered){  
                   
                    $cust_event = $this->addCustomerEvent($customerObject->cust_id,$event['event_id'],$custHistoryObject->ch_id);
                    if($cust_event['code'] == 1){
                        $customerModel->grantEventPoints($customerObject,$cust_event['data'],$event['bevent_points']);
                       

                        $message = new stdClass();
                        $message->amount = $event['bevent_points'];
                        $message->text = $event['bevent_message'];

                        $fallback = new pushParamsObject();
                        $fallback->biz_id = $customerObject->cust_biz_id;
                        $fallback->cust_id = $customerObject->cust_id;
                        $fallback->push_type = enumPushType::biz_bobileXPoints;
                        $fallback->message = pushManager::getSystemAlertPushMessageText(19,$customerObject->cust_biz_id);
                        $extraParams = array();
                        $extraParams["received_points"] = $event['bevent_points'];
                        $extraParams["points_reason_text"] = $event['bevent_message'];
                        $fallback->extra_params = $extraParams;


                        wsocketManager::sendIMAsync($message,$customerMsgTarget,enumSocketType::point,$fallback,$customerObject->cust_last_device == "Android");
                        
                    }
                }
            }
        }

        $isMember = $customerObject->cust_phone1 != '' && $customerObject->cust_phone1 == $customerObject->cust_valid_phone;
        
       
        if(!$isMember){
            return;
        }

        if($custHistoryObject->ch_action_id == enumCustomerActions::completedPurchase){
            sleep(50);
        }
        
        $scratchEvents = $this->getScratchEventsForAction($custHistoryObject->ch_action_id);
        
        if($scratchEvents['code'] == 1){
           
            foreach ($scratchEvents['data'] as $event)
            {
                $triggered = false;
                
                if(method_exists($this,'checkEvent_'.$event['event_type'])){                    
                    $triggered = $this->{'checkEvent_'.$event['event_type']}($custHistoryObject,$event);
                }
                $cust_event = $this->addCustomerEvent($customerObject->cust_id,$event['event_id'],$custHistoryObject->ch_id);
                
                if($triggered && $cust_event['code'] == 1){

                    $cardResult = $customerModel->grantScratchCard($customerObject,$event['event_id'],$cust_event['data']);
                    
                    if($cardResult->code == 1){
                       
                        $message = new stdClass();
                        $message->card = $cardResult->data;
                        
                        $fallback = new pushParamsObject();
                        $fallback->biz_id = $customerObject->cust_biz_id;
                        $fallback->cust_id = $customerObject->cust_id;
                        $fallback->push_type = enumPushType::biz_bobileXScratch;
                        $fallback->message = pushManager::getSystemAlertPushMessageText(17,$customerObject->cust_biz_id);
                       
                        
                        wsocketManager::sendIMAsync($message,$customerMsgTarget,enumSocketType::scratch,$fallback,$customerObject->cust_last_device == "Android");
                        
                    }
                    //else{

                    //    $customerModel->grantEventPoints($customerObject,$cust_event['data'],$event['event_def_points']);

                    //    $message = new stdClass();                        
                    //    $message->amount = $event['event_def_points'];

                    //    $fallback = new pushParamsObject();
                    //    $fallback->biz_id = $customerObject->cust_biz_id;
                    //    $fallback->cust_id = $customerObject->cust_id;
                    //    $fallback->push_type = enumPushType::biz_bobileXScratchempty;
                    //    $fallback->message = pushManager::getSystemAlertPushMessageText(19,$customerObject->cust_biz_id);
                        
                    //    $fallback->extra_params["received_points"] = $event['event_def_points'];

                    //    wsocketManager::sendIM($message,$customerMsgTarget,enumSocketType::scratch_empty,$fallback);

                    //}
                }
            }
        }

        return;
    }

     
    public function getCustomersHistoryByAction($cust_action_id){
        $sql = "SELECT * FROM tbl_customers,tbl_cust_history 
                WHERE ch_cust_id = cust_id
                AND ch_id = $cust_action_id";

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $this->db->getRow($sql);

        return $result;
    }

    public function getEvent($event_id){
        $sql = "SELECT * FROM tbl_events
                WHERE event_id = $event_id";

        $result = array();

        $result['code'] = 1;
        $result['message'] = "ok";
        $result['data'] = $this->db->getTable($sql);

        return $result;
    }

    public function checkPeriodEvents($period_type){
        try{
            $sql = "SELECT * FROM tbl_events WHERE event_type = 'period' AND event_period = '$period_type'";
            
            
            $events = $this->db->getTable($sql);
            
            foreach($events as $event){
                $this->serverPeriodEventCheck($event);
            }
        }
        catch(Exception $ex){
            echo print_r($ex);
        }

    }

    public function serverPeriodEventCheck($event){
       
        $customerModel = new customerModel();

        $bizSQL = "";
        if($event['event_usage'] == 'biz'){
            $bizSQL = "INNER JOIN tbl_biz_event ON cust_biz_id = bevent_biz_id 
                        AND bevent_event_id = {$event['event_id']}
                        AND bevent_is_active = 1";
        }

        $sql = "SELECT * FROM tbl_biz,tbl_customers
                    $bizSQL
                    LEFT JOIN tbl_customer_events ON cust_id = cevent_cust_id AND cevent_event_id = {$event['event_id']}
                WHERE cust_biz_id = biz_id 
                AND NOW() > DATE_ADD(cust_reg_date,INTERVAL {$event['event_data']} {$event['event_period']})
                AND NOW() < DATE_ADD(DATE_ADD(cust_reg_date,INTERVAL {$event['event_data']} {$event['event_period']}),INTERVAL 7 day)
                AND cevent_cust_id IS NULL
                AND cust_status = 'member'
                AND biz_x = 1";
        
       
        $customers = $this->db->getTable($sql);
        
        foreach ($customers as $customer)
        {
            $customerObject = customerObject::withData($customer);
        	$cust_event = $this->addCustomerEvent($customer['cust_id'],$event['event_id']);

            $cust_socket_target = $customer['cust_id'].'_'.$customer['cust_mobile_serial'];
           
            if($cust_event['code'] == 1){
                
                if($event['event_usage'] == 'biz'){

                    $customerModel->grantEventPoints($customerObject,$cust_event['data'],$customer['bevent_points']);

                    $message = new stdClass();
                    
                    $message->amount = $event['bevent_points'];
                    $message->text = $event['bevent_message'];

                    $fallback = new pushParamsObject();
                    $fallback->biz_id = $customerObject->cust_biz_id;
                    $fallback->cust_id = $customerObject->cust_id;
                    $fallback->push_type = enumPushType::biz_bobileXPoints;
                    $fallback->message = pushManager::getSystemAlertPushMessageText(19,$customerObject->cust_biz_id);

                    $fallback->message = str_replace("#received_points#",$customer['bevent_points'],$fallback->message);
                    $fallback->message = str_replace("#points_reason_text#",$event['bevent_message'],$fallback->message);
                    
                    $fallback->extra_params["received_points"] = $customer['bevent_points'];
                    $fallback->extra_params["points_reason_text"] = $event['bevent_message'];

                    wsocketManager::sendIMAsync($message,$cust_socket_target,enumSocketType::point,$fallback,$customerObject->cust_last_device == "Android");                    
                }
                else if($event['event_usage'] == 'scratch'){
                    $cust_event = $this->addCustomerEvent($customer['cust_id'],$event['event_id']);
                    $cardResult = $customerModel->grantScratchCard($customerObject,$event['event_id'],$cust_event['data']);

                    if($cardResult->code == 1){
                        $message = new stdClass();
                        $message->card = $cardResult->data;

                        $fallback = new pushParamsObject();
                        $fallback->biz_id = $customerObject->cust_biz_id;
                        $fallback->cust_id = $customerObject->cust_id;
                        $fallback->push_type = enumPushType::biz_bobileXScratch;
                        $fallback->message = pushManager::getSystemAlertPushMessageText(17,$customerObject->cust_biz_id);
                        
                        $fallback->extra_params["external_id"] = $cardResult->data['cscard_id'];

                        wsocketManager::sendIMAsync($message,$cust_socket_target,enumSocketType::scratch,$fallback,$customerObject->cust_last_device == "Android");
                    }
                    //else if($cardResult->code == 0){
                    //    $customerModel->grantEventPoints($customerObject,$cust_event['data'],$event['event_def_points']);

                    //    $message = new stdClass();
                        
                    //    $message->amount = $event['event_def_points'];
                        
                    //    $fallback = new pushParamsObject();
                    //    $fallback->biz_id = $customerObject->cust_biz_id;
                    //    $fallback->cust_id = $customerObject->cust_id;
                    //    $fallback->push_type = enumPushType::biz_bobileXScratchempty;
                    //    $fallback->message = pushManager::getSystemAlertPushMessageText(19,$customerObject->cust_biz_id);
                        
                    //    $fallback->extra_params["received_points"] = $customer['bevent_points'];
                        

                    //    wsocketManager::sendIM($message,$cust_socket_target,enumSocketType::scratch_empty,$fallback);
                    //}
                }
            }
        }     
    }

    /**
     * Checks 'direct' actions
     * Will always return true, as it just means that the action occured
     * @param mixed $cust_action 
     * @param mixed $event 
     * @return bool
     */
    private function checkEvent_direct(customerHistoryObject $custHistoryObject,$event){
        return true;
    }

    /**
     * Checks if the action was performed X times cyclically
     * @param mixed $cust_action 
     * @param mixed $event 
     * @return bool
     */
    private function checkEvent_every_x(customerHistoryObject $custHistoryObject,$event){
        
        if(!is_numeric($event['event_data'])){
            return false;
        }

        $addPeriod = $this->getEventPeriodString($event['event_period']);

        $sql = "SELECT COUNT(*) FROM tbl_cust_history 
                WHERE ch_action_id = {$event['event_action_id']}
                AND ch_cust_id = {$custHistoryObject->ch_cust_id}
                $addPeriod";

       

        $count = $this->db->getVal($sql);
        $result = false;
       
        if($count % intval($event['event_data']) == 0){
            $result = true;
        }
     
        return $result;
    }

    /**
     * Check if action was performed for the first time
     * (in a given period or generally)
     * @param mixed $cust_action 
     * @param mixed $event 
     * @return bool
     */
    private function checkEvent_first_time(customerHistoryObject $custHistoryObject,$event){
        
        $addPeriod = $this->getEventPeriodString($event['event_period']);

        $sql = "SELECT COUNT(*) FROM tbl_cust_history 
                WHERE ch_action_id = {$event['event_action_id']}
                AND ch_cust_id = {$custHistoryObject->ch_cust_id}
                $addPeriod";

        

        $count = $this->db->getVal($sql);
        $result = false;
        
        if($count == 1){
            $result = true;
        }
        
        return $result;
    }

    private function checkEvent_x_times(customerHistoryObject $custHistoryObject,$event){
        
        if(!is_numeric($event['event_data'])){
            return false;
        }

        $addPeriod = $this->getEventPeriodString($event['event_period']);

        $sql = "SELECT COUNT(*) FROM tbl_cust_history 
                WHERE ch_action_id = {$event['event_action_id']}
                AND ch_cust_id = {$custHistoryObject->ch_cust_id}
                $addPeriod";

        $count = $this->db->getVal($sql);
        $result = false;
        
        if($count == $event['event_data']){
            $result = true;
        }
        
        return $result;
    }

    /**
     * Returns the search string part for the period 
     * specified by event field "event_period"
     * returns empty string if not defined
     * @param mixed $period 
     * @return string
     */
    private function getEventPeriodString($period){
        
        $addendum = "AND ";

        switch($period){
            case 'hour':
                $addendum .= "ch_created >= NOW() - INTERVAL 1 HOUR AND ch_created <= NOW()";
                break;
            case 'day':
                $addendum .= "ch_created >= NOW() - INTERVAL 1 DAY AND ch_created <= NOW()";
                break;
            case 'week':
                $addendum.= "WEEK(ch_created) = WEEK(NOW()) AND YEAR(ch_created) = YEAR(NOW())";
                break;
            case 'month':
                $addendum.= "MONTH(ch_created) = MONTH(NOW()) AND YEAR(ch_created) = YEAR(NOW())";
                break;
            default:
                $addendum = '';
                break;
        }

        return $addendum;


    }

    function sendScratchCard($cust_id,$biz_id,$event_id){
        $customerModel = new customerModel();

        $customerResult = $customerModel->getCustomerWithID($cust_id);
        if($customerResult->code != 1){
            return;
        }
        $customer = $customerResult->data;
        $cust_socket_target = $customer->cust_id.'_'.$customer->cust_mobile_serial;
        $sql = "SELECT * FROM tbl_events WHERE event_id = $event_id";

        $event = $this->db->getRow($sql);
        $cust_event = $this->addCustomerEvent($cust_id,$event['event_id']);
        $cardResult = $customerModel->grantScratchCard($customer,$event_id,$cust_event['data']);

        if($cardResult->code == 1){
            $message = new stdClass();
            $message->card = $cardResult->data;

            $fallback = new pushParamsObject();
            $fallback->biz_id = $customer->cust_biz_id;
            $fallback->cust_id = $customer->cust_id;
            $fallback->push_type = enumPushType::biz_bobileXScratch;
            $fallback->message = pushManager::getSystemAlertPushMessageText(17,$customer->cust_biz_id);
            
            $fallback->extra_params["external_id"] = $cardResult->data['cscard_id'];

            wsocketManager::sendIMAsync($message,$cust_socket_target,enumSocketType::scratch,$fallback,$customer->cust_last_device == "Android");

        }
        else if($cardResult->code == 0){
            
            $customerModel->grantEventPoints($customer,$cust_event['data'],$event['event_def_points']);

            $message = new stdClass();
            
            $message->amount = $event['event_def_points'];
            
            $fallback = new pushParamsObject();
            $fallback->biz_id = $customer->cust_biz_id;
            $fallback->cust_id = $customer->cust_id;
            $fallback->push_type = enumPushType::biz_bobileXScratchempty;
            $fallback->message = pushManager::getSystemAlertPushMessageText(19,$customer->cust_biz_id);

            $fallback->message = str_replace("#received_points#",$event['event_def_points'],$fallback->message);
            
            $fallback->extra_params["received_points"] = $event['event_def_points'];

            wsocketManager::sendIMAsync($message,$cust_socket_target,enumSocketType::scratch_empty,$fallback,$customer->cust_last_device == "Android");
        }
    }

    
}
