<?php
if((@include "../".MVC_NAME."/libs/PHPExcel.php") !== false)
{
    include_once "../".MVC_NAME."/libs/PHPExcel.php";
}
/**
 * Methods for Utility Functions
 *
 * @version 1.0
 * @author PapTap
 */

class utilityManager {  
    
    public function __construct(){}
    
    public static function encryptIt( $string ,$replaceSlashess=false) {
        
        $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
        $qEncoded      = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $string, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
        if($replaceSlashess){
            $retString = str_replace("/","-_-_-",$qEncoded);
            $retString = str_replace("+","__-__",$retString);
            return $retString;
        }
        else{
            return $qEncoded;
        }
    }

    public static function decryptIt( $string ,$returnSlashess=false) {
        
        if($returnSlashess){
            $string = str_replace("@||@","/",$string);
            $string = str_replace("@**@","+",$string);
            $string = str_replace("-_-_-","/",$string);
            $string = str_replace("__-__","+",$string);
        }
        
        $cryptKey  = 'qJB0rGtIn5UB1xG03efyCp';
        $qDecoded      = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $string ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
        return( $qDecoded );
    }

    /**
     * Return HASH string for provided value
     * @param mixed $string 
     * @return string
     */
    public static function encodeToHASH($string) {
        
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode(KEY);

        // Generate an initialization vector
        $iv = "shgFfhjvsld4fkTb"; //openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));

        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($string, 'aes-256-cbc', $encryption_key, 0, $iv);

        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

    /**
     * Return readble string from HASH
     * @param mixed $string 
     * @return string
     */
    public static function decodeFromHASH($string) {

        // Remove the base64 encoding from our key
        $encryption_key = base64_decode(KEY);

        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"

        $dataArray = explode('::', base64_decode($string), 2);
        if(count($dataArray) < 2){
            return "";
        }
        list($encrypted_data, $iv) = $dataArray;

        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }
    
    public static function startsWith($haystack, $needle)
    {
        return $needle === "" || strpos($haystack, $needle) === 0;
    }
    
    public static function endsWith($haystack, $needle)
    {
        return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
    }
    
    public static function contains($haystack, $needle,$isCaseSensetive = false)
    {
        if(!$isCaseSensetive){
            $haystack = strtolower($haystack);
            $needle = strtolower($needle);
        }
        $pos = strrpos($haystack, $needle);
        if($pos !== false) {
            return true;
        }
        
        return false;
    }

    public static function echoJavaScriptResponseArray($responseCode,$responseMessage,$responseData="",$appData=array()){
        echo json_encode(
            array(
                "responseCode"=> $responseCode,
                "responseMessage"=>$responseMessage,
                "responseData"=>$responseData,
                "responseAppData"=>$appData
            )
        ); 
    }
    
    public static function getImageThumb($imgURL){
        if (strpos($imgURL, 'bizImages') !== false) {
            return $imgURL;
        }
        if(strpos($imgURL, '-thumbs') !== false ){
            return $imgURL;
        }
        return str_replace("storage.googleapis.com/paptap","storage.googleapis.com/paptap-thumbs",$imgURL); 
    }

    public static function getImageFull($imgURL){
        if (strpos($imgURL, 'bizImages') !== false) {
            return $imgURL;
        }
        if(strpos($imgURL, '-thumbs') === false ){
            return $imgURL;
        }
        return str_replace("storage.googleapis.com/paptap-thumbs","storage.googleapis.com/paptap",$imgURL); 
    }
    
    public static function prepareDBString($dbString){
        return htmlspecialchars(stripslashes($dbString));
    }
    
    public static function isAudioFile($fileName){
        
        $ext_list=array("mp4","flv","webmv","webma","webm","m4a","m4v","ogv","oga","mp3","midi","mid","ogg","wav","m3u8","asx","pls");
        
        $fParts = explode(".",trim($fileName));
        if(count($fParts) > 1)
        {
            $extention = array_pop($fParts);
            
            if(in_array(strtolower($extention),$ext_list)) return true;
        }
        
        return false;
        
    }
    
    public static function getYouTubeID($ytURL)
    {
        $posg = strrpos(strtoupper($ytURL), 'YOUTU');
        if($posg != false)
        {
            $link = str_replace( '-nocookie.com/v/' , '.com/watch?v=' ,$ytURL);
            $pattern = "#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=embed/)[^&\n]+|(?<=v=)[^&\??\n]+|(?<=youtu.be/)[^&\n]+#";
            preg_match($pattern, $link, $matches);

            if(isset($matches) && isset($matches[0])){
                if($matches[0] == "")
                {
                    $pattern = "/^(?:http(?:s)?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:(?:watch)?\?(?:.*&)?v(?:i)?=|(?:embed|v|vi|user)\/))([^\?&\"'>]+)/";
                    preg_match($pattern, $link, $matches);
                }
                
                $videoID = $matches[0];
                $posg = strrpos(strtoupper($videoID), 'YOUTU');
                
                if($videoID != '' && $posg == false)
                {
                    return $videoID;
                }
            }
        }
        return "";
    }
    
    public static function getVimeoObject($video_url){
        $oembed_endpoint = 'http://vimeo.com/api/oembed';
        // Create the URLs
        $xml_url = $oembed_endpoint . '.xml?url=' . rawurlencode($video_url) . '&width=640';
        
        $curl = curl_init($xml_url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $response = curl_exec($curl);
        curl_close($curl);

        // Load in the oEmbed XML
        return simplexml_load_string($response);
    }

    public static function getFacebookID($userURL)
    {
        if ($userURL == '') return '';
        if(!utilityManager::startsWith(strtolower($userURL),'http') && !utilityManager::startsWith(strtolower($userURL),'https')) $userURL = 'http://'.$userURL;
        $components = parse_url($userURL);
        $path = (isset($components['path'])) ? $components['path'] : "";
        $baseURL = $components['scheme']."://".$components['host'].$path;
        $urlArray = explode("/",rtrim($baseURL,"/"));
        
        $fbId = end($urlArray);
        $checkArray = explode("-",$fbId);
        if(count($checkArray)>1){
            $lastElement = end($checkArray);
            if(is_numeric($lastElement) && strlen($lastElement) > 10){
                $fbId = $lastElement;
            }
        }

        return trim($fbId);
    }
    
    public static function getGooglePlusID($userURL)
    {
        if ($userURL == '') return '';
        if(!utilityManager::startsWith(strtolower($userURL),'http') && !utilityManager::startsWith(strtolower($userURL),'https')) $userURL = 'http://'.$userURL;
        $components = parse_url($userURL);
        
        $combinedPath = "";
        if($components['path'] != "") {
            $pathParts = explode("/",$components['path']);
            $combinedPath = $pathParts[1];
        }
        
        $baseURL = $components['scheme']."://".$components['host']."/$combinedPath";
        $urlArray = explode("/",rtrim($baseURL,"/"));

        $id = end($urlArray);

        if (!is_numeric($id)) {
            return "+" . trim(ltrim($id, "+"));
        } else {
            return ltrim(trim($id), "+");
        }
    }
    
    public static function getInstaID($username)
    {
        //$username = strtolower($username); // sanitization
        //$token = "174354680.e43fb2f.8a3210cf8e5b44b9afdf174b4a2e4ca3";
        //$url = "https://api.instagram.com/v1/users/search?q=".$username."&access_token=".$token;   
        
        //$get = self::file_get_contents_pt($url);
        //$json = json_decode($get);

        //foreach($json->data as $user)
        //{
        //    if($user->username == $username)
        //    {
        //        return trim($user->id);
        //    }
        //}

        return trim($username); 
    }
    
    public static function RecolorImage($ImageSvgFile, $ImageColor)
    {
        $FileContents = self::file_get_contents_pt($ImageSvgFile);
        $doc = new DOMDocument();
        $doc->loadXML($FileContents) or die('Failed to load SVG file ' . $ImageSvgFile . ' as XML.  It probably contains malformed data.');

        $AllTags = $doc->getElementsByTagName("path");
        foreach ($AllTags as $ATag)
        {
            $ATag->setAttribute('fill', $ImageColor);
            $FileContents = $doc->saveXML($doc);
        }
        
        $AllTags = $doc->getElementsByTagName("polygon");
        foreach ($AllTags as $ATag)
        {
            $ATag->setAttribute('fill', $ImageColor);
            $FileContents = $doc->saveXML($doc);
        }

        $AllTags = $doc->getElementsByTagName("rect");
        foreach ($AllTags as $ATag)
        {
            $ATag->setAttribute('fill', $ImageColor);
            $FileContents = $doc->saveXML($doc);
        }

        $AllTags = $doc->getElementsByTagName("circle");
        foreach ($AllTags as $ATag)
        {
            $ATag->setAttribute('fill', $ImageColor);
            $FileContents = $doc->saveXML($doc);
        }

        $AllTags = $doc->getElementsByTagName("line");
        foreach ($AllTags as $ATag)
        {
            $ATag->setAttribute('fill', $ImageColor);
            $FileContents = $doc->saveXML($doc);
        }
        
        return $FileContents;

    }

    public static function objectToArray($object)
    {
        if(!is_object($object) && !is_array($object)) {
            return $object;
        }
        if(is_object($object)) {
            $object = get_object_vars($object);
        }
        return array_map('utilityManager::objectToArray', $object);
    }

    public static function decryptForm($formData){

        $dForm = array();
        
        foreach ($formData as $key => $value)
        {
        	$dkey = self::decryptIt($key);
            $dForm[$dkey] = $value;
        }
        
        return $dForm;
    }

    public static function getTimeSinceFormat($date)
    {
        $then = new DateTime($date);
        $now = new DateTime(date("Y-m-d H:i:s"));
        $interval = date_diff($now,$then,false);
        $result = array();
        $result['number'] = 0;
        $text = '';
        if($interval->y > 0){
            $result['text'] = 'over_year_ago';
        }
        else{
            if($interval->m > 0){
                
                $result['text'] = 'over_month_ago';
            }
            else{
                if($interval->d > 2){
                    $result['number'] = $interval->d;
                    $result['text'] = 'days_ago';
                }
                else{
                    if($interval->d > 1){
                        $result['text'] = '_yesterday';
                    }
                    else{
                        if($interval->h > 1){
                           
                            $result['number'] = $interval->h;
                            $result['text'] = 'hours_ago';
                        }
                        else{
                           
                            $result['number'] = $interval->i;
                            $result['text'] = 'minutes_ago';
                        }
                    }
                }
            }

        }

        return $result;
    }
    
    public static function getDomainFromUrl($url){
        
        $url = (!utilityManager::startsWith($url,"http")) ? "http://$url" : $url;
        return str_ireplace('www.', '', parse_url(trim(strtolower($url)), PHP_URL_HOST));

    }
    
    public static function getFileNameFromUrl($url){
        
        $path = explode('/',$url);
        $lastInArray = (count($path) > 0) ? count($path)-1 : 0;
        $path = explode('?',$path[$lastInArray]);
        return $path[0];
    }
    
    public static function curl($url,$headers,$fields,$asString=false,$type="post"){
        
        $fields_string = json_encode( $fields );
        if($asString){
            $fields_string = "";
            foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
            $fields_string = rtrim($fields_string, '&');
        }
        
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, $type=="post" );
        if(is_array($headers) && count($headers)>0){
            curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        if($fields != ""){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        }
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }

    public static function getFormattedTime($date, $withHour = false, $withTimeZone = true, $timestamp="", $onlyHour = false)
    {
        $timeZone = isset($_SESSION["appData"]['biz_time_zone']) ? $_SESSION["appData"]['biz_time_zone'] : 0;
        $country = isset($_SESSION["appData"]["biz_addr_country_id"]) ? $_SESSION["appData"]["biz_addr_country_id"] : 1;
        $addTimeZone = ($withTimeZone) ? ($timeZone + date("I")) * 3600 : 0;
            
        $temptime = ($timestamp == "") ? ( strtotime( $date ) + $addTimeZone ) : $timestamp;
        
        if($onlyHour){
            $result = $country == "1" ? date( 'h:i A', $temptime ) : date( 'H:i', $temptime );
            if($country == "1"){
                $result = ltrim($result,'0');
            }
        }else{
            if($withHour == true){
                $result = $country == "1" ? date( 'm/d/Y h:i A', $temptime ) : date( 'd/m/Y H:i', $temptime );
                if($country == "1"){
                    $split = explode(' ',$result);
                    $split[1] = ltrim($split[1],'0');
                    $result = implode(' ',$split);
                }
            }
            else{
                $result = $country == "1" ? date( 'm/d/Y', $temptime ) : date( 'd/m/Y', $temptime );
            }
        }
        return $result;
    }

    public static function getStartTimeFromHourID($hourID){
        $db = new Database();

        return $db->getVal("SELECT hour_start FROM calendar_hours WHERE hour_id = $hourID");
    }

    public static function getEndTimeFromHourID($hourID){
        $db = new Database();

        return $db->getVal("SELECT hour_end FROM calendar_hours WHERE hour_id = $hourID");
    }

    public static function getFormattedTimeByCountryID($countryID, $date, $withHour = false, $withTimeZone = true, $timestamp="", $onlyHour = false){
        $temptime = ($timestamp == "") ? strtotime( $date ) : $timestamp;
        
        if($onlyHour){
            $result = $countryID == "1" ? date( 'h:i A', $temptime ) : date( 'H:i', $temptime );
            if($countryID == "1"){
                $result = ltrim($result,'0');
            }
        }else{
            if($withHour == true){
                $result = $countryID == "1" ? date( 'm/d/Y h:i A', $temptime ) : date( 'd/m/Y H:i', $temptime );
                if($countryID == "1"){
                    $split = explode(' ',$result);
                    $split[1] = ltrim($split[1],'0');
                    $result = implode(' ',$split);
                }
            }
            else{
                $result = $countryID == "1" ? date( 'm/d/Y', $temptime ) : date( 'd/m/Y', $temptime );
            }
        }
        return $result;
    }
    
    public static function getSqlDateFormat($date, $withHour = false, $withTimeZone = true, $timestamp="", $onlyHour = false)
    {
        $timeZone = isset($_SESSION["appData"]['biz_time_zone']) ? $_SESSION["appData"]['biz_time_zone'] : 0;
        $country = isset($_SESSION["appData"]["biz_addr_country_id"]) ? $_SESSION["appData"]["biz_addr_country_id"] : 1;
        $addTimeZone = ($withTimeZone) ? ($timeZone + date("I")) * 3600 : 0;
        
        if($country != "1"){
            $date = str_replace('/', '-', $date);
        }
        
        $temptime = ($timestamp == "") ? ( strtotime( $date ) + $addTimeZone ) : $timestamp;
        
        if($onlyHour){
            $result = date( 'H:i:s', $temptime );
        }else{
            if($withHour == true){
                $result = date('Y-m-d H:i:s', $temptime);
            }else{
                $result = date('Y-m-d', $temptime);
            }            
        }
        return $result;
    }

    public static function getSqlDateFormatWithCountryIDAndTimeZone($date,$country = 1,$timeZone = 0, $withHour = false, $withTimeZone = true, $timestamp="", $onlyHour = false)
    {
       
        $addTimeZone = ($withTimeZone) ? ($timeZone + date("I")) * 3600 : 0;
        
        if($country != "1"){
            $date = str_replace('/', '-', $date);
        }
        
        $temptime = ($timestamp == "") ? ( strtotime( $date ) + $addTimeZone ) : $timestamp;
        
        if($onlyHour){
            $result = date( 'H:i:s', $temptime );
        }else{
            if($withHour == true){
                $result = date('Y-m-d H:i:s', $temptime);
            }else{
                $result = date('Y-m-d', $temptime);
            }            
        }
        return $result;
    }

    /**
     * Returns the current date in SQL format
     * @return string
     */
    public static function getSQLDateFormatTimeCurrentTimestamp(){
        return date('Y-m-d H:i:s');
    }

    /**
     * Return date with added interval in SQL format
     * unit can be: day/month/year
     * @param string $dateTimeInSqlFormat 
     * @param int $inderval 
     * @param string $unit 
     * @return string
     */
    public static function getSQLDateFormatWithInterval($dateTimeInSqlFormat,$interval,$unit){

        $intervalString = utilityManager::startsWith($interval,"-") ? $interval : "+".$interval;

        $startDate = new DateTime($dateTimeInSqlFormat);
        $startDate->modify("$intervalString $unit");
        return $startDate->format('Y-m-d H:i:s');
    }
    
    public static function getPathByUID($uid)
    {
        $digitArray = str_split($uid);
        $path = "";
        foreach ($digitArray as $digit) {
            $path .= $digit."/";
        } 
        
        return $path;
    }
    
    public static function url_exists($url) {
        //check, if a valid url is provided
        if(!filter_var($url, FILTER_VALIDATE_URL))
        {
            return false;
        }

        //initialize curl
        $curlInit = curl_init($url);
        curl_setopt($curlInit,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($curlInit,CURLOPT_HEADER,true);
        curl_setopt($curlInit,CURLOPT_NOBODY,true);
        curl_setopt($curlInit,CURLOPT_RETURNTRANSFER,true);

        //get answer
        $response = curl_exec($curlInit);

        curl_close($curlInit);

        if ($response) return true;

        return false;
    }
    
    public static function url_img_exists($url) {

        if(!filter_var($url, FILTER_VALIDATE_URL))
        {
            return false;
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if(curl_exec($ch)!==FALSE)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function file_get_contents_pt($url) {
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );  

        return @file_get_contents($url, false, stream_context_create($arrContextOptions));
    }

    public static function get_url_mime_type($url)
    {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        curl_exec($ch);
        return curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

    }



    /*
    * trim string and add 3 dots 
    */
    public static function trimStringDots($str,$maxLen){
        if( mb_strlen($str,'UTF-8')  > $maxLen) 
        {
            return mb_substr($str,0,$maxLen - 3,'UTF-8')."...";
        }
        else{
            return $str;
        }
    }
    
    public static function qrCodeGenerator($url){
        include_once(PATH . 'admin/libs/phpqrcode/qrlib.php');
        //$url = "$url/qr/index.php?SESS=FGEFDSF34G-$bizID-FGSDF456F";
        QRcode::png($url,false,QR_ECLEVEL_H,10);
    }

    public static function qrCodeText($text){
        include_once(PATH . 'admin/libs/phpqrcode/qrlib.php');
        
        QRcode::png($text,false,QR_ECLEVEL_H,10);
    }

    public static function qrURLGenerator($url){
        include(PATH . 'admin/libs/phpqrcode/qrlib.php');
        QRcode::png($url,false,QR_ECLEVEL_H,10);
    }
    
    public static function getUsersAddress($ip){
        
        $ipLite = new ip2location();
        $ipLite->setKey('aea760489e1fcc4cb1ca350766f7430af344a9682865a54721d1df1c06ff62eb');
        
        //Get errors and locations
        $locations = $ipLite->getCity($ip);
        $errors = $ipLite->getError();

        if(!empty($errors) && is_array($errors)){
            $locations["countryCode"] = "";
            
        }
        return $locations;
    }
    
    private static function extractIP(&$ip) {
        if (preg_match ("/^([0-9]{1,3}\.){3,3}[0-9]{1,3}/", $ip, $array))
            return $array;
        else
            return false;
    }
    public static function get_IP() {
        if(@$_SERVER['REMOTE_HOST']) {
            $array = self::extractIP($_SERVER['REMOTE_HOST']);
            if ($array && count($array) >= 1)
                return $array[0]; // first IP in the list
        }
        return $_SERVER['REMOTE_ADDR'];
    }
    public static function get_real_IP() {

        if(@$_SERVER['HTTP_X_FORWARDED_FOR']) { // case 1.A: proxy && HTTP_X_FORWARDED_FOR is defined
            $array = self::extractIP($_SERVER['HTTP_X_FORWARDED_FOR']);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }
        if(@$_SERVER['HTTP_X_FORWARDED']) { // case 1.B: proxy && HTTP_X_FORWARDED is defined
            $array = self::extractIP($_SERVER['HTTP_X_FORWARDED']);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }
        if(@$_SERVER['HTTP_FORWARDED_FOR']) { // case 1.C: proxy && HTTP_FORWARDED_FOR is defined
            $array = self::extractIP($_SERVER['HTTP_FORWARDED_FOR']);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }
        if(@$_SERVER['HTTP_FORWARDED']) { // case 1.D: proxy && HTTP_FORWARDED is defined
            $array = self::extractIP($_SERVER['HTTP_FORWARDED']);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }
        if(@$_SERVER['HTTP_CLIENT_IP']) { // case 1.E: proxy && HTTP_CLIENT_IP is defined
            $array = self::extractIP($_SERVER['HTTP_CLIENT_IP']);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }
        
        if(@$_SERVER['HTTP_VIA']) {
            // case 2: 
            // proxy && HTTP_(X_) FORWARDED (_FOR) not defined && HTTP_VIA defined
            // other exotic variables may be defined 
            return ( @$_SERVER['HTTP_VIA']. 
                '_' . @$_SERVER['HTTP_X_COMING_FROM'].
                '_' . @$_SERVER['HTTP_COMING_FROM']
              ) ;
        }
        if(@$_SERVER['HTTP_X_COMING_FROM'] || @$_SERVER['HTTP_COMING_FROM'] ) {
            // case 3: proxy && only exotic variables defined
            // the exotic variables are not enough, we add the REMOTE_ADDR of the proxy
            return ( @$_SERVER['REMOTE_ADDR'] . 
                '_' . @$_SERVER['HTTP_X_COMING_FROM'] .
                '_' . @$_SERVER['HTTP_COMING_FROM']
              ) ;
        }
        
        // case 4: no proxy (or tricky case: proxy+refresh)
        if(@$_SERVER['REMOTE_HOST']) {
            $array = self::extractIP($_SERVER['REMOTE_HOST']);
            if ($array && count($array) >= 1) {
                return $array[0]; // first IP in the list
            }
        }

        return @$_SERVER['REMOTE_ADDR'];
    }
    public static function retrieveIP() {
        
        if(isset($_SERVER['HTTP_VIA']) && @$_SERVER['HTTP_VIA'])  // Using proxy!
            return self::get_real_IP();
        else // Not using proxy...
            return (self::get_IP()) ? self::get_IP() : self::get_real_IP();
    }

    public static function notifyBizOfAction($biz_id,$action){
        $url = 'https://'.MAIN_SERVER.'/'.MVC_NAME.'/autojobs/asyncNotification';        
        $params = array(
                'biz_id' => $biz_id,
                'action_id' => $action
            );
        self::asyncTask($url,$params);
    }

    public static function asyncProcessOrder($order_id){
        $url = "http://127.0.0.1/".MVC_NAME.'/eventAsync/processOrder';        
        $params = array(
                'order_id' => $order_id
            );
        self::asyncTask($url,$params);
    }
    
    public static function asyncTask($url,$params = array(),$blocking = true){
        
        $post_params = array();

        foreach ($params as $key => &$val) 
        {
            $post_params[] = $key.'='.$val;
        }
        
        $post_string = implode('&', $post_params);

        $parts=parse_url($url);

        $fp = pfsockopen($parts['host'],
            isset($parts['port'])?$parts['port']:80,
            $errno, $errstr, 30);

        stream_set_blocking ($fp,$blocking);
       // socket_set_nonblock($fp);

        $out = "POST ".$parts['path']." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($post_string)."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($post_string)) $out.= $post_string;
        
        fwrite($fp, $out);
        fclose($fp);
    }

    public static function asyncNonBlockTask($url,$params = array()){
        //local address of node server
        $host='XXX.XXX.XXX.XXX';
        $port=80;
        //location where THIS script is running FROM
        $local="http://localhost";
        //json the data to send
        $data=json_encode($data);

        //some very particular headers
        $head = "GET / HTTP/1.1"."\r\n".
                "Upgrade: WebSocket"."\r\n".
                "Connection: Upgrade"."\r\n".
                "Origin: $local"."\r\n".
                "Host: $host"."\r\n".
                "Sec-WebSocket-Version: 13"."\r\n".
                "Sec-WebSocket-Key: asdasdaas76da7sd6asd6as7d"."\r\n".
                "Content-Length: ".strlen($data)."\r\n"."\r\n";
        //WebSocket handshake
        $sock = fsockopen($host, $port, $errno, $errstr, 2);
        fwrite($sock, $head ) or die('error:'.$errno.':'.$errstr);
        $headers = fread($sock, 2000);
        //see the second link above for what hybi10Encode() is doing
        fwrite($sock, hybi10Encode($data)) or die('error:'.$errno.':'.$errstr);
        $wsdata = fread($sock, 2000);
        fclose($sock);
    }
    
    public static function asyncPostCurl($url,$params = array()){
        
        $post_params = array();

        foreach ($params as $key => &$val) 
        {
            $post_params[] = $key.'='.$val;
        }
        
        $post_string = implode('&', $post_params);

        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true ); 
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        
        return $result;
    }
    
    public static function generatePassword ($length = 8){

        // start with a blank password
        $password = "";

        // define possible characters - any character in this string can be
        // picked for use in the password, so if you want to put vowels back in
        // or add special characters such as exclamation marks, this is where
        // you should do it
        $possible = "2346789bcdfghjkmnpqrtvwxyzBCDFGHJKLMNPQRTVWXYZ";

        // we refer to the length of $possible a few times, so let's grab it now
        $maxlength = strlen($possible);
        
        // check for length overflow and truncate if necessary
        if ($length > $maxlength) {
            $length = $maxlength;
        }
        
        // set up a counter for how many characters are in the password so far
        $i = 0; 
        
        // add random characters to $password until $length is reached
        while ($i < $length) { 

            // pick a random character from the possible ones
            $char = substr($possible, mt_rand(0, $maxlength-1), 1);
            
            // have we already used this character in $password?
            if (!strstr($password, $char)) { 
                // no, so it's OK to add it onto the end of whatever we've already got...
                $password .= $char;
                // ... and increase the counter by one
                $i++;
            }

        }

        // done!
        return $password;

    }

    public static function generateEmail(){
        $email = '';

        $randomText = str_replace(".","",microtime(true));

        $email = 'fake_'.$randomText.'@fake.com';
        $exists = appManager::ownerExist($email);
        $round = 0;
        while ($exists && $round < 10)
        {
            $randomText = str_replace(".","",microtime(true));
        	$email = 'fake_'.$randomText.'@fake.com';
            $exists = appManager::ownerExist($email);
            $round++;
        }
        if($round > 9){
            $email = 'fake_'.rand(90000,99999999).'@fake.com';
        }
        
        return $email;
    }

    public static function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);
        return implode(",", $rgb); // returns the rgb values separated by commas
        //return $rgb; // returns an array with the rgb values
    }

    
    public static function RGBToHSL($RGB) {
        $r = 0xFF & ($RGB >> 0x10);
        $g = 0xFF & ($RGB >> 0x8);
        $b = 0xFF & $RGB;

        $r = ((float)$r) / 255.0;
        $g = ((float)$g) / 255.0;
        $b = ((float)$b) / 255.0;

        $maxC = max($r, $g, $b);
        $minC = min($r, $g, $b);

        $l = ($maxC + $minC) / 2.0;

        if($maxC == $minC)
        {
            $s = 0;
            $h = 0;
        }
        else
        {
            if($l < .5)
            {
                $s = ($maxC - $minC) / ($maxC + $minC);
            }
            else
            {
                $s = ($maxC - $minC) / (2.0 - $maxC - $minC);
            }
            if($r == $maxC)
                $h = ($g - $b) / ($maxC - $minC);
            if($g == $maxC)
                $h = 2.0 + ($b - $r) / ($maxC - $minC);
            if($b == $maxC)
                $h = 4.0 + ($r - $g) / ($maxC - $minC);

            $h = $h / 6.0; 
        }

        $h = (int)round(255.0 * $h);
        $s = (int)round(255.0 * $s);
        $l = (int)round(255.0 * $l);

        return (object) Array('hue' => $h, 'saturation' => $s, 'lightness' => $l);
    }
    
    public static function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

            // 16 bits for "time_mid"
            mt_rand( 0, 0xffff ),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand( 0, 0x0fff ) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand( 0, 0x3fff ) | 0x8000,

            // 48 bits for "node"
            mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }

    public static function readExcel($ext,$filename){
        //$fh = fopen("debugXL.txt", 'a');
        //fwrite($fh, "$ext"."\n");
        //fclose($fh);
        if($ext == 'xls')
        {
            $objReader = new PHPExcel_Reader_Excel5();
            $objReader->setReadDataOnly(true);
        }
        else if($ext == 'csv'){
            $objReader = new PHPExcel_Reader_CSV();
        }
        else
        {
            $objReader = new PHPExcel_Reader_Excel2007();
            $objReader->setReadDataOnly(true);
        }

        $objPHPExcel = new PHPExcel();
        $objPHPExcel = $objReader->load($filename);

        $sheet = $objPHPExcel->getActiveSheet();
       
        $result = $sheet->toArray();
        return $result;
        

            
        
    }

    public static function validateDate($date)
    {
        $eFormat = $_SESSION["appData"]["biz_addr_country_id"] == "1" ? 'm-d-Y' : 'd-m-Y';
        $date = str_replace('/','-',$date);
        $d = DateTime::createFromFormat($eFormat, $date);
        return $d && $d->format($eFormat) === $date;
    }

    public static function getCoordinatesByAddress($country,$state,$city,$street,$number,$bizId=0){
                
        $address = "";
        $db = new Database();
        
        if($bizId == 0){
            $address .= ($number != "") ? "$number" : "";
            $address .= ($street != "") ? " $street" : "";
            $address .= ($city != "") ? ", $city" : "";
            $address .= ($state != "") ? ", $state" : "";
            $address .= ($country != "") ? ", $country" : "";
        }else{                    
            $bizRow = $db->getRow("select * from tbl_biz where biz_id=$bizId");
            $address .= ($bizRow["biz_addr_no"] != "") ? "{$bizRow["biz_addr_no"]}" : "";
            $address .= ($bizRow["biz_addr_street"] != "") ? " {$bizRow["biz_addr_street"]}" : "";
            $address .= ($bizRow["biz_addr_town"] != "") ? ", {$bizRow["biz_addr_town"]}" : "";
            $address .= ($bizRow["biz_addr_state"] != "") ? ", {$bizRow["biz_addr_state"]}" : "";
            $address .= ($bizRow["biz_addr_country"] != "") ? ", {$bizRow["biz_addr_country"]}" : "";
        }
        
        $address = urlencode(trim($address));
        
        $json = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$address&key=AIzaSyDU2uHOD7DHPRAXcTWVGrf1nFjLk6ioYVU"));
        
        $response = array();
        if($json->status == "OK" && isset($json->results[0]->geometry->location->lat) && isset($json->results[0]->geometry->location->lng)){
            $response["code"] = "1";
            $response["lat"] = $json->results[0]->geometry->location->lat;
            $response["lng"] = $json->results[0]->geometry->location->lng;
            
            if($bizId != 0 && $response["lat"] != ""){
                $db->execute("update tbl_biz_market set bm_biz_long={$response["lng"]},bm_biz_lati={$response["lat"]} where bm_biz_id=$bizId");
            }
        }
        else{
            $response["code"] = "0";
        }
        
        return $response;
    }
    
    public static function redirectWhiteLabelURL($ip){

        $redirectURL = "";
        require_once('/var/www/html/'.MVC_NAME.'/MVC/config.php');
        require_once('/var/www/html/'.MVC_NAME.'/MVC/Database.php');
        $visitorCountry = utilityManager::curl("https://app.bobile.com/geoip/coutryByIP.php?ip=$ip","","");
        
        if($visitorCountry != ""){
            
            $db = new Database();        
            $redirectURL = $db->getVal("select reseller_wl_url from tbl_resellers where reseller_redirect_country='$visitorCountry'");
        }
        
        if($redirectURL !="" ){
            Header( "Location: " . $redirectURL );
            exit;
        }
    }
    
    public static function redirectWhiteLabelURLWP($ip){

        $redirectURL = "";
        require_once('/var/www/html/'.MVC_NAME.'/MVC/Database.php');
        $visitorCountry = utilityManager::curl("https://app.bobile.com/geoip/coutryByIP.php?ip=$ip","","");
        
        if($visitorCountry != ""){
            
            $db = new Database("mainForWP");        
            $redirectURL = $db->getVal("select reseller_wl_url from tbl_resellers where reseller_redirect_country='$visitorCountry'");
        }
        
        if($redirectURL !="" ){
            Header( "Location: " . $redirectURL );
            exit;
        }
    }
    
    public static function getRedirectWhiteLabelURLWP($ip){

        $redirectURL = "";
        require_once('/var/www/html/'.MVC_NAME.'/MVC/Database.php');
        $visitorCountry = utilityManager::curl("https://app.bobile.com/geoip/coutryByIP.php?ip=$ip","","");
        
        if($visitorCountry != ""){
            
            $db = new Database("mainForWP");        
            $redirectURL = $db->getVal("select reseller_wl_url from tbl_resellers where reseller_redirect_country='$visitorCountry'");
        }
        
        return $redirectURL;
        
    }
    
    public static function getCurrencySymbol($ip = ''){
        if($ip == ''){
            $ip = self::get_real_IP();
        }
        $symbol = "$";
        require_once('/var/www/html/'.MVC_NAME.'/MVC/Database.php');
        $visitorCountry = utilityManager::curl("https://app.bobile.com/geoip/coutryByIP.php?ip=$ip","","");
        
        if($visitorCountry != ""){
            
            $db = new Database("mainForWP");        
            $symbol = $db->getVal("select cur_symbol from tbl_countries,tbl_currency where cur_id=country_currency and country_code='$visitorCountry'");
        }
        
        return $symbol;
        
    }
    
    public static function getCurrency($ip = ''){
        if($ip == ''){
            $ip = self::get_real_IP();
        }
        $currency = "USD";
        require_once('/var/www/html/'.MVC_NAME.'/MVC/Database.php');
        $visitorCountry = utilityManager::curl("https://app.bobile.com/geoip/coutryByIP.php?ip=$ip","","");
        
        if($visitorCountry != ""){
            
            $db = new Database("mainForWP");        
            $currency = $db->getVal("select cur_code from tbl_countries,tbl_currency where cur_id=country_currency and country_code='$visitorCountry'");
        }
        
        return $currency;
        
    }

    public static function getCurrencyCodeById($currencyId){

        $db = new Database();        
        return  $db->getVal("SELECT cur_code FROM tbl_currency WHERE cur_id=$currencyId");        
    }

    public static function getCurrencySymbolByCode($currencyCode){

        $db = new Database();        
        return  $db->getVal("SELECT cur_symbol FROM tbl_currency WHERE cur_code='$currencyCode'");        
    }

    public static function getAddressCoordinates($country,$state,$city,$street,$no){
        $address = $state != "" ? "$country,$state,$city,$street,$no" : "$country,$city,$street,$no";

        
        $geo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&key=AIzaSyBh6KuAvYuH-9CWXITaJ3SL59R5e1Tb7OA&sensor=false');

        $geo = json_decode($geo, true);
        $response = array();
        if ($geo['status'] == 'OK') {// Get Lat & Long
            $response['code'] = 1;
            $response['latitude']= $geo['results'][0]['geometry']['location']['lat'];
            $response['longitude'] = $geo['results'][0]['geometry']['location']['lng'];
        }
        else{
            $response['code'] = 0;
            $response['data'] = $geo['status']." | ".$geo['error_message'];
        }

        return $response;
    }
    
    public static function cartesian($input) {
        $result = array();

        while (list($key, $values) = each($input)) {
            // If a sub-array is empty, it doesn't affect the cartesian product
            if (empty($values)) {
                continue;
            }

            // Seeding the product array with the values from the first sub-array
            if (empty($result)) {
                foreach($values as $value) {
                    $result[] = array($key => $value);
                }
            }
            else {
                // Second and subsequent input sub-arrays work like this:
                //   1. In each existing array inside $product, add an item with
                //      key == $key and value == first item in input sub-array
                //   2. Then, for each remaining item in current input sub-array,
                //      add a copy of each existing array inside $product with
                //      key == $key and value == first item of input sub-array

                // Store all items to be added to $product here; adding them
                // inside the foreach will result in an infinite loop
                $append = array();

                foreach($result as &$product) {
                    // Do step 1 above. array_shift is not the most efficient, but
                    // it allows us to iterate over the rest of the items with a
                    // simple foreach, making the code short and easy to read.
                    $product[$key] = array_shift($values);

                    // $product is by reference (that's why the key we added above
                    // will appear in the end result), so make a copy of it here
                    $copy = $product;

                    // Do step 2 above.
                    foreach($values as $item) {
                        $copy[$key] = $item;
                        $append[] = $copy;
                    }

                    // Undo the side effecst of array_shift
                    array_unshift($values, $product[$key]);
                }

                // Out of the foreach, we can add to $results now
                $result = array_merge($result, $append);
            }
        }

        return $result;
    }

    public static function getItemTypeIntFromItemTypeString($typeString){
        $result = 0;
        switch($typeString){
            case "coupon":
                $result = 1;
                break;
            case "product":
                $result = 2;
                break;
            case "service":
            case "class":
                $result = 3;
                break;
            case "subscription":
                $result = 4;
                break;
            case "punchpass":
            case "punch_pass":
                $result = 5;
                break;
        }
        return $result;
    }

    public static function inBobileDomain(){
       
        return self::contains(strtolower($_SERVER['HTTP_HOST']),"bobile.com");
          
    }

    public static function canBeOpenedInIframe($url){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_REFERER, "https://bobile.com");
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type:application/x-www-form-urlencoded"));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_NOBODY, 1);
        $output = curl_exec($ch);

        curl_close($ch);

        $headers = [];
        $data = explode("\n",$output);
        $headers['status'] = $data[0];
        array_shift($data);
        
        foreach($data as $part){
            $middle=explode(":",$part);

            if(count($middle) > 1){
                if(utilityManager::contains(strtolower(trim($middle[0])),"x-frame-options") && utilityManager::contains(strtolower(trim($middle[1])),"sameorigin")){
                    return false;
                }
            }
        }
        return true;
    }

    ///Get directory last modification time
    public static function dirmodtime($dir){
        // 1. An array to hold the files.
        $last_modified_time = 0;

        // 2. Getting a handler to the specified directory
        $handler = opendir($dir);
       
        // 3. Looping through every content of the directory
        while ($file = readdir($handler)) {
            // 3.1 Checking if $file is not a directory
            if(is_file($dir.DIRECTORY_SEPARATOR.$file)){
                $files[] = $dir.DIRECTORY_SEPARATOR.$file;
                $filemtime = filemtime($dir.DIRECTORY_SEPARATOR.$file);
                if($filemtime>$last_modified_time) {
                    $last_modified_time = $filemtime;
                }
            }
        }

        // 4. Closing the handle
        closedir($handler);

        // 5. Returning the last modified time
        return md5($last_modified_time);
    }
    
    public static function getGraph($url,$return_json=true)
    { 
        $agent= 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_URL,$url);
        $result=curl_exec($ch);
        return ($return_json) ? json_decode($result) : $result;
    }

    public static function getCountry($country_id){
        $db = new Database();
        $result = $db->getRow("SELECT * FROM tbl_countries WHERE country_id = $country_id");

        return $result;
    }

    public static function getState($state_id){
        $db = new Database();
        $result = $db->getRow("SELECT * FROM tbl_states WHERE state_id = $state_id");

        return $result;
    }
    
    public static function getValidPhoneNumber($number,$countryCode=""){

        $number = str_replace("+","",$number);
        if($countryCode != ""){
            
            if (substr($number, 0, strlen($countryCode)) == $countryCode) {
                $number = substr($number, strlen($countryCode));
            } 
            
            $db = new Database();
            $trunk_code = $db->getVal("select tc_trunk_code from tbl_trunk_code where tc_country_code=$countryCode");
            if($trunk_code != ""){
                if (substr($number, 0, strlen($trunk_code)) == $trunk_code) {
                    $number = substr($number, strlen($trunk_code));
                } 
            }
            $number = $countryCode.$number;
        }

        return trim($number);
    }

    public static function generateRedeemCode($id=""){
        return $id.substr(str_replace(".","",microtime(true)),-10);
    }

    public static function getShortOneForLong($long=""){

        if ($long == "") return "";

        $timePart = str_replace(".","",microtime(true));
        $key = $timePart;
        $dKey = self::encryptIt($key,true);

        //the fix
        $dKey = str_replace('@','a',$dKey);
        $dKey = str_replace('*','b',$dKey);
        $dKey = str_replace('|','c',$dKey);
        $dKey = str_replace('=','d',$dKey);
        $dKey = str_replace('-','e',$dKey);
        $dKey = str_replace('_','f',$dKey);
        $dKey = str_replace('+','g',$dKey);
        $dKey = strtolower($dKey);
        $dKey = urlencode($dKey);

        $shortLink = "http://short.one/$dKey";

        $db = new Database();
        $db->execute("INSERT INTO short_one SET so_link_id = '$dKey', so_short_link = '$shortLink', so_full_link = '$long'");

        return $shortLink;

        
    }

    public static function getScrambledEmail($email){
        $parts = explode('@',$email);

        if(count($parts) != 2){
            return $email;
        }

        $parts[1] = preg_replace(array('#[A-Z0-9]#', '#[a-z]#'), array('X', 'x'), $parts[1]);
        $result = implode('@',$parts);
        return $result;

    }

    public static function getScrambledPhone($phone){
        $result = preg_replace(array('#[A-Z0-9]#', '#[a-z]#'), array('X', 'x'), $phone);
        
        return $result;
    }

    public static function getTextLanguage($text){
        $instance = new self();

        $params = array(
                        "q" => urlencode($text)
                    );

        $response = $instance->curl("https://translation.googleapis.com/language/translate/v2/detect?key=AIzaSyCaemOfU6K5Ipd0q07kn3srCAFEXaB49uQ","",$params,true);
        $responseJson = json_decode($response);
        return $responseJson->data->detections[0][0]->language == "und" ? "en" : $responseJson->data->detections[0][0]->language;
    }

    public static function mirrorString($str){
        $r = '';
        for ($i = mb_strlen($str); $i>=0; $i--) {
            $r .= mb_substr($str, $i, 1);
        }
        return $r;
    }

    public static function validateZipCodeByCountry($countryCode,$zip){
        $json = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?components=postal_code:".$zip."|country:".$countryCode."&key=".GOOGLE_MAPS_API_CODE));
        if($json->status == "OK"){
            return true;
        }else{
            return false;
        }
    }

    public static function getAddresByLongAndLati($deal_lat,$deal_long){

        $output = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$deal_lat.','.$deal_long.'&key=AIzaSyBh6KuAvYuH-9CWXITaJ3SL59R5e1Tb7OA&sensor=false');
        $geoData= json_decode($output);
        //print_r($output);
        $result = array();

        if(isset($geoData->results) && count($geoData->results)>0){
            $geoResult = $geoData->results[0];
            if(count($geoResult->address_components) > 0) {
                
                foreach ($geoResult->address_components as $component) {
                    if ($component->types[0] == "country") {
                        $result["country_code"] = $component->short_name;
                        $result["country"] = $component->long_name;
                        $result["country_id"] = self::getCountryId($result["country_code"]);
                    }
                    if ($component->short_name == "US") {
                        foreach ($geoResult->address_components as $state_component) {
                            if ($state_component->types[0] == "administrative_area_level_1") { 
                                $result["state_code"] = $state_component->short_name;
                                $result["state"] = $state_component->long_name;
                                $result["state_id"] = self::getStateId($result["state_code"]);
                            }
                        }
                    }
                    foreach ($geoResult->address_components as $address_components) {
                        if ($address_components->types[0] == "locality") { 
                            $result["city"] = $address_components->long_name;
                        }
                    }
                }
            }
        }
        return $result;
    }

    public static function isUrlValid($url){

        $file_headers = @get_headers($url);
        return !(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found');
    }

    public static function getWhiteLabelURLForBiz($bizid){
        $db = new Database();
        
        $resellerDomain = $db->getRow("SELECT reseller_wl_domain,reseller_wl_protocol 
	                                            FROM tbl_biz,tbl_owners 
                                                LEFT JOIN tbl_resellers ON reseller_id=owner_reseller_id 
	                                            WHERE owner_id=biz_owner_id 
	                                            AND biz_id = $bizid");
        
        return isset($resellerDomain['reseller_wl_protocol']) && $resellerDomain['reseller_wl_protocol'] != '' ? "{$resellerDomain['reseller_wl_protocol']}://{$resellerDomain['reseller_wl_domain']}" : CLIENT_SERVER;
    }

    public static function getWhiteLabelMarketDomainForBiz($bizid){
        $db = new Database();
        
        $resellerMarketDomainRow = $db->getRow("SELECT reseller_wl_protocol,reseller_wl_domain,reseller_wl_market_domain
	                                            FROM tbl_biz,tbl_owners LEFT JOIN tbl_resellers 
	                                            ON reseller_id=owner_reseller_id 
	                                            WHERE owner_id=biz_owner_id 
	                                            AND biz_id = $bizid");
        $marketDomain = '';
        if(is_array($resellerMarketDomainRow) && $resellerMarketDomainRow["reseller_wl_domain"] != ""){
            $marketDomain =  $resellerMarketDomainRow["reseller_wl_protocol"]."://".$resellerMarketDomainRow["reseller_wl_market_domain"];
        }
        else{
            $marketDomain = MARKET_SERVER;
        }

        
        return $marketDomain;
    }

    public static function getCountryId($country){
        $db = new Database();
        $countryNumber = ( !is_int($country) ? (ctype_digit($country)) : true );
        if(!$countryNumber)
        {
            return $db->getVal("SELECT country_id FROM tbl_countries WHERE country_code='$country'");
        }
        else{
            return $country;
        }
    }

    public static function getStateId($state,$country = 1){
        $db = new Database();
        $stateNumber = ( !is_int($state) ? (ctype_digit($state)) : true );
	    if(!$stateNumber)
	    {
		    return $db->getVal("SELECT state_id FROM tbl_states WHERE state_code='$state' AND state_country_id=$country");
	    }
        else{
            return $state;
        }
    }

    public static function getStateIdFromName($stateName){
        $db = new Database();
        $stateID = $db->getVal("SELECT state_id FROM tbl_states WHERE state_name_eng = '$stateName'");

        if($stateID == ""){
            return 0;
        }

        return $stateID;
    }

    //Action tracking
    /**
     * Calls the function that executes all the methods for checking and triggering events
     * @param mixed $action_id 
     * @param mixed $customer_id 
     */
    public static function actionTrigger(actionTriggerObject $actionTriggerObject){
        $params = array();
        $params['cust_id'] = $actionTriggerObject->customerId;
        $params['act_id'] = $actionTriggerObject->actionId;
        $params['status'] = $actionTriggerObject->status;
        $params['device_id'] = $actionTriggerObject->deviceId;
        $params['replace'] = $actionTriggerObject->replace;
        self::asyncTask("https://".API_SERVER."/".MVC_NAME."/eventAsync/triggeringAction/",$params);
        return;
    }
}
?>