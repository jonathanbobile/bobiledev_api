<?php

class adminModel extends Model
{
    private $mobileRequest;

    function __construct(mobileRequestObject $request = null)
    {
        parent::__construct();  
        $this->mobileRequest = $request;
    }

    public function setMobileRequest(mobileRequestObject $request){
        $this->mobileRequest = $request;
    }

    public function logoutUser($userName){
        try{
            $adminTable = $this->getBizAdminsForUsername($userName);

            if(count($adminTable)>0){
                foreach ($adminTable as $adminRow)
                {                    
                    $this->deleteBizAdminEntry($adminRow["ba_row_id"]);                    
                    $this->unsetCustomerAsAdmin($adminRow["ba_biz_id"]);
                }           
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$userName);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BIZ LIST - PUBLIC                 */
    /************************************* */

    public function getFullBizListForAccountID($account_id,$skip = 0,$take = 20,$keyword = ''){
        try{
            
            $accountManager = new accountManager();

            $result = array();

            $accountsListResult = $accountManager->getAllAccountsConnectedToAccountID($account_id);
            if($accountsListResult->code != 1){
                return resultObject::withData(0,'no_accounts',$result);
            }

            $accountsList = $accountsListResult->data;

            $accountsTracker = array();

            foreach ($accountsList as $account)
            {
            	$accountsTracker[$account->ac_id] = $account;
            }
            

            $bizRows = $this->getBizListForAccountsList($accountsList,$skip,$take,$keyword);

            foreach ($bizRows as $bizRow)
            {
                $biz = bizObject::withData($bizRow);
                
                //Update tbl_biz_admin
                $this->insertBizAdminEntry($biz,$bizRow['connected_account']);
                //Update tbl_customers - set this device id as admin
                $this->updateCustomerAsAdmin($biz);

                $result[] = $biz->getArrayForAdmin($accountsTracker[$bizRow['connected_account']],$bizRow['client_id']);
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$this->accountID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BIZ LIST - PRIVATE                */
    /************************************* */

    private function getBizListForAccountsList($accountList,$skip = 0,$take = 20,$keyword = ''){
        $innerSql = "";
        foreach ($accountList as $account)
        {
            if($innerSql != ""){
                $innerSql .= " UNION ";
            }
            $funcName = "getBizSqlForAccountType_{$account->ac_type}";
          
            $innerSql .= $this->$funcName($account,$keyword);
        }       


        $limit = "";
        if($take > 0){
            $limit = "LIMIT $skip,$take";
        }
        $mainSql = "SELECT * FROM ($innerSql) s 
                    GROUP BY biz_id
                    ORDER BY biz_short_name ASC 
                    $limit";
       
        return $this->db->getTable($mainSql);
    }

    private function getBizSqlForAccountType_account_owner(accountObject $account,$keyword = ''){
        if(!isset($account->owner) || !isset($account->owner->owner_id)){
            return "";
        }
        $filter = "";
        if($keyword != ""){
            $filter = "AND LOWER(biz_short_name) LIKE LOWER('%$keyword%')";
        }
        return "SELECT *,{$account->ac_id} as connected_account,0 as client_id FROM tbl_biz 
                WHERE biz_owner_id = {$account->owner->owner_id}
                $filter
                AND biz_isdeleted = 0";
    }

    private function getBizSqlForAccountType_reseller_client(accountObject $account,$keyword = ''){
        $filter = "";
        if($keyword != ""){
            $filter = "AND LOWER(biz_short_name) LIKE LOWER('%$keyword%')";
        }

        return "SELECT tbl_biz.*,{$account->ac_id} as connected_account,reseller_client_id as client_id FROM tbl_biz,tbl_client_biz,tbl_reseller_clients
                WHERE client_biz_biz_id = biz_id
                AND client_biz_client_id = reseller_client_id 
                AND reseller_client_account_id = {$account->ac_id}
                AND client_biz_active = 1 
                AND client_biz_isActivated = 1 
                $filter
                AND biz_isdeleted = 0";
    }

    private function getBizSqlForAccountType_guest(accountObject $account,$keyword = ''){
        $filter = "";
        if($keyword != ""){
            $filter = "AND LOWER(biz_short_name) LIKE LOWER('%$keyword%')";
        }

        return "SELECT tbl_biz.*,{$account->ac_id} as connected_account,0 as client_id FROM tbl_biz,tbl_guests
                WHERE gu_biz_id = biz_id
                AND gu_account_id = {$account->ac_id}
                AND gu_isactive = 1 
                AND gu_isrevoked = 0
                $filter
                AND biz_isdeleted = 0";
    }

    private function getBizListForAccountType_account_owner(accountObject $account){
        
        if(!isset($account->owner) || !isset($account->owner->owner_id)){
            return array();
        }

        $sql = "SELECT * FROM tbl_biz 
                WHERE biz_owner_id = {$account->owner->owner_id}
                AND biz_isdeleted = 0
                ORDER BY LOWER(biz_short_name) ASC";

        return $this->db->getTable($sql);
    }

    private function getBizListForAccountType_reseller_client(accountObject $account){
        $sql = "SELECT * FROM tbl_biz,tbl_client_biz,tbl_reseller_clients
                WHERE client_biz_biz_id = biz_id
                AND client_biz_client_id = reseller_client_id 
                AND reseller_client_account_id = {$account->ac_id}
                AND client_biz_active = 1 
                AND client_biz_isActivated = 1 
                AND biz_isdeleted = 0
                ORDER BY LOWER(biz_short_name) ASC";

        return $this->db->getTable($sql);
    }

    private function getBizListForAccountType_guest(accountObject $account){
        $sql = "SELECT * FROM tbl_biz,tbl_guests
                WHERE gu_biz_id = biz_id
                AND gu_account_id = {$account->ac_id}
                AND gu_isactive = 1 
                AND gu_isrevoked = 0
                AND biz_isdeleted = 0
                ORDER BY LOWER(biz_short_name) ASC";

        return $this->db->getTable($sql);
    }    

    /************************************* */
    /*   BIZ ADMIN - PRIVATE                */
    /************************************* */

    private function getBizAdminsForUsername($userName){
        return $this->db->getTable("SELECT * FROM tbl_biz_admin WHERE ba_biz_id IN
                                        (SELECT biz_id FROM tbl_biz 
                                            WHERE biz_owner_id = (SELECT owner_id FROM tbl_owners WHERE owner_username = '$userName'))
                                    AND ba_device_id='{$this->mobileRequest->deviceID}'");
    }

    private function insertBizAdminEntry(bizObject $biz,$accountID){
        $sqlInsert = "INSERT INTO tbl_biz_admin (ba_biz_id,ba_device_id,ba_account_id) VALUES 
                        ({$biz->biz_id},'{$this->mobileRequest->deviceID}',{$accountID}) 
                        ON DUPLICATE KEY UPDATE ba_biz_id={$biz->biz_id}";

        $this->db->execute($sqlInsert);
    }

    private function deleteBizAdminEntry($bizAdminID){
        $this->db->execute("delete from tbl_biz_admin where ba_row_id = $bizAdminID");
    }

    private function updateCustomerAsAdmin(bizObject $biz){
        $sql = "update tbl_customers set cust_is_admin = 1 where cust_biz_id={$biz->biz_id} and cust_mobile_serial='{$this->mobileRequest->deviceID}'";

        $this->db->execute($sql);
    }

    private function unsetCustomerAsAdmin($bizID){
        $sql = "update tbl_customers set cust_is_admin = 0 where cust_biz_id=$bizID and cust_mobile_serial='{$this->mobileRequest->deviceID}'";;
    }
}
