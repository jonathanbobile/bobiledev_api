<?php 

class bizMembersClubSettingsObject extends bobileObject { 

    public $bmcs_biz_id = "0";
    public $bmcs_membership_mode = "open";//enum('open','exclusive')
    public $bmcs_welcome_grant = "300";
    public $bmcs_email_on_details_update = "0";

     function __construct(){} 

    public static function withData($data){

    if (!isset($data["bmcs_biz_id"])){
          throw new Exception("bizMembersClubSettingsObject constructor requies data array provided!");
    }

    $instance = new self();

    $instance->bmcs_biz_id = $data["bmcs_biz_id"];
    $instance->bmcs_membership_mode = $data["bmcs_membership_mode"];
    $instance->bmcs_welcome_grant = $data["bmcs_welcome_grant"];
    $instance->bmcs_email_on_details_update = $data["bmcs_email_on_details_update"];

    return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


