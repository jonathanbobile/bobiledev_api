<?php 

class bizModObject extends bobileObject { 

    public $biz_mod_id = "0";
    public $biz_mod_biz_id = "0";
    public $biz_mod_mod_id = "0";
    public $biz_mod_mod_name;
    public $biz_mod_mobile_name;
    public $biz_mod_mod_pic = "mod1.png";
    public $biz_mod_templ_id = "1";
    public $biz_mod_active = "0";
    public $biz_mod_stuct = "0";
    public $biz_mod_intro = "0";
    public $biz_mod_index = "0";
    public $biz_mod_external_type;//enum('','product','service','employee','class')
    public $biz_mod_has_reviews = "0";
    public $biz_mod_can_delete = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["biz_mod_id"])){
              throw new Exception("bizModObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->biz_mod_id = $data["biz_mod_id"];
        $instance->biz_mod_biz_id = $data["biz_mod_biz_id"];
        $instance->biz_mod_mod_id = $data["biz_mod_mod_id"];
        $instance->biz_mod_mod_name = $data["biz_mod_mod_name"];
        $instance->biz_mod_mobile_name = $data["biz_mod_mobile_name"];
        $instance->biz_mod_mod_pic = $data["biz_mod_mod_pic"];
        $instance->biz_mod_templ_id = $data["biz_mod_templ_id"];
        $instance->biz_mod_active = $data["biz_mod_active"];
        $instance->biz_mod_stuct = $data["biz_mod_stuct"];
        $instance->biz_mod_intro = $data["biz_mod_intro"];
        $instance->biz_mod_index = $data["biz_mod_index"];
        $instance->biz_mod_external_type = $data["biz_mod_external_type"];
        $instance->biz_mod_has_reviews = $data["biz_mod_has_reviews"];
        $instance->biz_mod_can_delete = $data["biz_mod_can_delete"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


