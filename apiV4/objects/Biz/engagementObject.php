<?php 

class engagementObject extends bobileObject { 

    public $bng_id = "0";
    public $bng_biz_id = "0";
    public $bng_entity_type = "engage";//enum('module','function','engage','')
    public $bng_entity_id = "0";
    public $bng_external_id = "0";
    public $bng_symbol;
    public $bng_title;
    public $bng_text;
    public $bng_regular_text;
    public $bng_param_rec_id = "0";
    public $bng_bubble_size = "XL";//enum('XL','L','M','S','XS')
    public $bng_succ_title;
    public $bng_succ_desc;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bng_id"])){
              throw new Exception("engagementObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bng_id = $data["bng_id"];
        $instance->bng_biz_id = $data["bng_biz_id"];
        $instance->bng_entity_type = $data["bng_entity_type"];
        $instance->bng_entity_id = $data["bng_entity_id"];
        $instance->bng_external_id = $data["bng_external_id"];
        $instance->bng_symbol = $data["bng_symbol"];
        $instance->bng_title = $data["bng_title"];
        $instance->bng_text = $data["bng_text"];
        $instance->bng_regular_text = $data["bng_regular_text"];
        $instance->bng_param_rec_id = $data["bng_param_rec_id"];
        $instance->bng_bubble_size = $data["bng_bubble_size"];
        $instance->bng_succ_title = $data["bng_succ_title"];
        $instance->bng_succ_desc = $data["bng_succ_desc"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


