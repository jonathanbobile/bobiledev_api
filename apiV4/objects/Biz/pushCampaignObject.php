<?php 

class pushCampaignObject extends bobileObject { 

    public $pu_id = "0";
    public $pu_biz_id = "0";
    public $pu_created = "";
    public $pu_status;
    public $pu_short_mess;
    public $pu_sent;
    public $pu_num_sent = "0";
    public $pu_num_recived = "0";
    public $pu_html;
    public $pu_header;
    public $pu_img;
    public $pu_target;//enum('all','guests','members','groups')
    public $pu_groups;
    public $pu_send_on;
    public $pu_in_server = "0";
    public $pu_isnew = "0";
    public $pu_based_location = "0";
    public $pu_active = "0";
    public $pu_longi;
    public $pu_lati;
    public $pu_distance = "1";
    public $pu_type = "1";
    public $pu_coupon_id = "0";
    public $pu_product_id = "0";
    public $pu_loyalty_id = "0";
    public $pu_scard_id = "0";
    public $pu_event_type = "0";
    public $pu_event_send_time;
    public $pu_event_struct = "0";
    public $pu_event_greeting;
    public $pu_event_main_text;
    public $pu_event_signature;
    public $pu_external_id = "0";
    public $pu_level = "1";
    public $pu_mod_id = "0";
    public $pu_view = "Info";
    public $pu_view_android = "Info";
    public $pu_async_sent = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["pu_id"])){
              throw new Exception("pushCmpaignObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->pu_id = $data["pu_id"];
        $instance->pu_biz_id = $data["pu_biz_id"];
        $instance->pu_created = $data["pu_created"];
        $instance->pu_status = $data["pu_status"];
        $instance->pu_short_mess = $data["pu_short_mess"];
        $instance->pu_sent = $data["pu_sent"];
        $instance->pu_num_sent = $data["pu_num_sent"];
        $instance->pu_num_recived = $data["pu_num_recived"];
        $instance->pu_html = $data["pu_html"];
        $instance->pu_header = $data["pu_header"];
        $instance->pu_img = $data["pu_img"];
        $instance->pu_target = $data["pu_target"];
        $instance->pu_groups = $data["pu_groups"];
        $instance->pu_send_on = $data["pu_send_on"];
        $instance->pu_in_server = $data["pu_in_server"];
        $instance->pu_isnew = $data["pu_isnew"];
        $instance->pu_based_location = $data["pu_based_location"];
        $instance->pu_active = $data["pu_active"];
        $instance->pu_longi = $data["pu_longi"];
        $instance->pu_lati = $data["pu_lati"];
        $instance->pu_distance = $data["pu_distance"];
        $instance->pu_type = $data["pu_type"];
        $instance->pu_coupon_id = $data["pu_coupon_id"];
        $instance->pu_product_id = $data["pu_product_id"];
        $instance->pu_loyalty_id = $data["pu_loyalty_id"];
        $instance->pu_scard_id = $data["pu_scard_id"];
        $instance->pu_event_type = $data["pu_event_type"];
        $instance->pu_event_send_time = $data["pu_event_send_time"];
        $instance->pu_event_struct = $data["pu_event_struct"];
        $instance->pu_event_greeting = $data["pu_event_greeting"];
        $instance->pu_event_main_text = $data["pu_event_main_text"];
        $instance->pu_event_signature = $data["pu_event_signature"];
        $instance->pu_external_id = $data["pu_external_id"];
        $instance->pu_level = $data["pu_level"];
        $instance->pu_mod_id = $data["pu_mod_id"];
        $instance->pu_view = $data["pu_view"];
        $instance->pu_view_android = $data["pu_view_android"];
        $instance->pu_async_sent = $data["pu_async_sent"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


