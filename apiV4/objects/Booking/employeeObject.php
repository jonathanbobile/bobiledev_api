<?php

/**
 * employeeObject short summary.
 *
 * employeeObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class employeeObject extends bobileObject
{
    /* Identifiers */
    public $be_id = 0;
    public $be_biz_id = 0;
    public $be_account_id;

    /* Details */
    public $be_name;
    public $be_mail;
    public $be_default_color;
    public $be_pic;

    /* Settings */
    public $be_ipn_push_id;
    public $be_ipn_expiration;
    public $be_apple_user;
    public $be_apple_pass;
    public $be_outlook_user;
    public $be_pass;    
    public $be_job;
    public $be_active;        
    public $be_phone;
    public $be_cal_type;
    public $be_cal_id;
    public $be_cal_name;
    public $be_token;
    public $be_event_color;
    public $be_reminder_time;
    public $be_sync_token;
    public $be_token_expiration;
    
    public $workDays = array();

    function __construct(){}

    public static function withData($employeeData){
        if(!isset($employeeData["be_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->be_id = isset($employeeData["be_id"]) && $employeeData["be_id"] > 0 ? $employeeData["be_id"] : 0;
        $instance->be_biz_id = isset($employeeData["be_biz_id"]) && $employeeData["be_biz_id"] > 0 ? $employeeData["be_biz_id"] : 0;
        $instance->be_account_id = isset($employeeData["be_account_id"]) ? $employeeData["be_account_id"] : 0;

        /* Details */
        $instance->be_name = $employeeData["be_name"];
        $instance->be_mail = $employeeData["be_mail"];
        $instance->be_pic = $employeeData["be_pic"] == "" ? "https://storage.googleapis.com/paptap/system/no_employee_image.png" : utilityManager::getImageThumb($employeeData["be_pic"]) ;
        $instance->be_default_color = $employeeData["be_default_color"];

        /* Settings */
        $instance->be_pass = $employeeData["be_pass"];
        $instance->be_job = $employeeData["be_job"];
        $instance->be_active = $employeeData["be_active"];
        $instance->be_phone = $employeeData["be_phone"];
        $instance->be_cal_type = $employeeData["be_cal_type"];
        $instance->be_cal_id = $employeeData["be_cal_id"];
        $instance->be_cal_name = $employeeData["be_cal_name"];
        $instance->be_token = $employeeData["be_token"];
        $instance->be_event_color = $employeeData["be_event_color"];
        $instance->be_reminder_time = $employeeData["be_reminder_time"];
        $instance->be_sync_token = $employeeData["be_sync_token"];
        $instance->be_token_expiration = $employeeData["be_token_expiration"];
        $instance->be_ipn_push_id = $employeeData["be_ipn_push_id"];
        $instance->be_ipn_expiration = $employeeData["be_ipn_expiration"];
        $instance->be_apple_user = $employeeData["be_apple_user"];
        $instance->be_apple_pass = $employeeData["be_apple_pass"];
        $instance->be_outlook_user = $employeeData["be_outlook_user"];

        return $instance;
    }

    public function getAPIFormattedArray(mobileRequestObject $requestObject){
        $result = array();

        $result["emp_id"] = $this->be_id;
        $result["emp_name"] = $this->be_name;
        $result["emp_pic"] = $this->be_pic;

        return $result;
    }

    public function setEmployeeHours($workDays){
        if(!is_array($workDays)){
            throw new Exception('Employee hours not an array');
        }

        $this->workDays = $workDays;
    }
}
