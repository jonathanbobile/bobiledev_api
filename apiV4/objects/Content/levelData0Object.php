<?php 

class levelData0Object extends levelDataObject { 

    public $md_mod_id = "0";
    public $ms_view_type = "";
    public $ms_view_type_android = "";
    public $ms_view_end = "";
    public $ms_view_end_android = "";
    public $pg_img = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("data0LevelObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type != ""){
            if(isset($data["struct_type"]) && $data["struct_type"] == "default"){

                $instance->md_desc = $instance->md_head;

                switch($instance->md_external_type){
                    case "product":
                        $dataProduct = levelDataManager::getConectedLevelDataByExternalIdAndStructure(9,$instance->md_external_id);
                        if(is_array($dataProduct)){
                            $nib_name = $data["nib_name"];
                            $instance->md_next_view=($dataProduct["md_content"] == "0") ? $dataProduct["ms_view_type$nib_name"] : $dataProduct["ms_view_end$nib_name"];
                            $instance->md_content = $dataProduct["md_content"];
                            $instance->ms_view_type = $dataProduct["ms_view_type"];
                            $instance->ms_view_type_android = $dataProduct["ms_view_type_android"];
                            $instance->ms_view_end = $dataProduct["ms_view_end"];
                            $instance->ms_view_end_android = $dataProduct["ms_view_end_android"];
                            $instance->md_head = $dataProduct["md_head"]; 
                            $instance->md_price = $dataProduct["md_price"];
                            $instance->md_original_price = $dataProduct["md_original_price"];
                            $instance->md_icon = $dataProduct["md_icon"];
                            $instance->item_type = 2;
                        }
                        break;
                    case "subscription":
                        $dataProduct = levelDataManager::getConectedLevelDataByExternalIdAndStructure(10,$instance->md_external_id);
                        if(is_array($dataProduct)){
                            $nib_name = $data["nib_name"];
                            $instance->md_next_view=($dataProduct["md_content"] == "0") ? $dataProduct["ms_view_type$nib_name"] : $dataProduct["ms_view_end$nib_name"];
                            $instance->md_content = $dataProduct["md_content"];
                            $instance->ms_view_type = $dataProduct["ms_view_type"];
                            $instance->ms_view_type_android = $dataProduct["ms_view_type_android"];
                            $instance->ms_view_end = $dataProduct["ms_view_end"];
                            $instance->ms_view_end_android = $dataProduct["ms_view_end_android"];
                            $instance->md_head = $dataProduct["md_head"]; 
                        }
                        break;
                    case "pointshop":
                        $pointItem = levelDataManager::getConectedLevelDataPointShopItem($instance->md_external_id);
                        $instance->md_price = $pointItem['pg_cost'];
                        $instance->md_head = $pointItem['pg_title'] != "" ? $pointItem['pg_title'] : $pointItem['label'];
                        $instance->md_info = $pointItem['pg_text'];
                        break;
                    case "class":
                        $classResponse = bookingManager::getClassByID($instance->md_external_id);
                        if($classResponse->code == 1){
                            $instance->md_head = $classResponse->data->class_name;
                        }
                        $instance->item_type = 3;
                        break;
                    case "coupon":
                        $instance->md_next_view="Info";
                        break;
                }

            }else{
                switch($instance->md_external_type){
                    case "product":
                        $dataProduct = levelDataManager::getConectedLevelDataByExternalIdAndStructure(9,$instance->md_external_id);
                        $instance->md_original_price = $dataProduct["md_original_price"];
                        $instance->md_mod_id = 0;
                        if(isset($data['structure_id']) && $data['structure_id'] == 116){
                            $instance->md_icon = $data['modPic'] != "" ? $data['modPic'] : $data['modPic'] ;
                        }

                        if(isset($data['structure_id']) && $data['structure_id'] == 115){
                            $instance->md_desc = $data['md_desc'];
                        }else{
                            $instance->md_desc = isset($data['ext_md_info']) && $data['ext_md_info'] != "" ? $data['ext_md_info'] : "";
                        }
                        $instance->item_type = 2;
                        break;
                    case "coupon":
                        $customerID = isset($data['customer_id']) ? $data['customer_id'] : 0;
                        $cuponType="";
                        
                        if($data["md_info5"] == "1")
                        {
                            $cuponType = $data["md_price"].' '.utilityManager::getCurrencyCodeById($data["md_int1"]);             
                        }
                        if($data["md_info5"] == "2")
                        {
                            $cuponType = $data["md_int2"]."+".$data["md_int3"];
                        }
                        if($data["md_info5"] == "3")
                        {
                            $cuponType="";
                        }

                        $claimd = couponsManager::getClaimedCouponsByItem($data["md_row_id"]);

                        //$limit = couponsManager::getCouponLimitations($instance->md_row_id);
                        $total_claims = couponsManager::getClaimedCouponsByItem($instance->md_row_id);
                        $claims_total_limit = couponsManager::getCouponTotalLimitations($instance->md_row_id);
                        $claims_per_member_limit = couponsManager::getCouponLimitations($instance->md_row_id);

                        $left = 0;
                        $instance->md_price=($data["ite_sell_price"] == null) ? "0" : $data["ite_sell_price"];
                        if($customerID != 0){//Check if customer has exceeded the coupon's limitations
                            $claimd = couponsManager::getClaimedCoupoonsForCustomerByCoupon($instance->md_row_id,$customerID);                            

                            if ($claims_total_limit > $claims_per_member_limit){
                                $left = $instance->calcQtyLeftPersonalLimitFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$claimd);
                            }else{
                                $left = $instance->calcQtyLeftTotalFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$claimd);
                            }

                        }else{ // no Customer

                            if ($claims_total_limit > 0){
                                $left = $claims_total_limit - $total_claims;
                                
                            }else if (isset($instance->md_price) && $instance->md_price > 0){
                                
                                $left = -2;
                            }else{
                                
                                $left = -1;
                            }
                        }   

                        $instance->md_mod_id = 0;
                        $instance->md_type="layout_coupon";
                        $instance->md_next_view="Info";
                        $instance->md_desc=$cuponType;
                        $instance->md_int1=$data["md_info5"];
                        $instance->md_int2=$left;
                        
                        $instance->md_external_type="coupon";
                        $instance->md_external_id=$data["md_row_id"];

                        if($data['structure_id'] == 116){//Get info and image from data0 row
                            $instance->md_info = $data['base_info'];
                            $instance->md_icon = $data['base_pic'];
                        }
                        $instance->item_type = 1;
                        break;
                    case "pointshop":

                        $instance->md_row_id=$data["md_row_id"];
                        $instance->md_type="layout_points";
                        $instance->md_head=stripslashes($data["label"]);
                        $instance->md_desc=stripslashes($data["description"]);
                        $instance->md_icon=$data["pg_img"] == "" ? "https://storage.googleapis.com/bobile/images/points_shop_default.jpg" : str_replace("paptap-thumbs","paptap",$data["pg_img"]);
                        $instance->md_next_view="";
                        $instance->md_info=isset($data["md_info"]) ? $data["md_info"] : '';
                        $instance->md_int1=$data["pg_entity_id"]; //external id
                        $instance->md_price=$data["pg_cost"]; //points
                        $instance->md_external_type="pointshop";
                        $instance->md_external_id=$data["pg_id"];
                        $instance->md_external_sub_id = 0;

                        if($data['structure_id'] == 116){//Get info and image from data0 row
                            $instance->pg_img = $data['md_pic'];
                        }
                        break;
                    case "class":
                        $instance->md_row_id=$data["recordID"];
                        $instance->md_type="layout_class";
                        $instance->md_head=stripslashes($data["class_name"]);
                        $instance->md_desc=stripslashes($data["employee_name"]);
                        $instance->md_icon=str_replace("paptap-thumbs","paptap",$data["employee_pic"]);
                        $instance->md_price=$data["service_price"]; //service price
                        $instance->md_int3=$data["class_total_positions"] - $data["taken_slots"]; //places left
                        $instance->md_int1=$data["start_timestamp"]; //timestamp start
                        $instance->md_int2=$data["end_timestamp"]; //timestamp end
                        $instance->md_info1=$data["start_time"];// start time
                        $instance->md_info2=$data["end_time"]; // end time
                        $instance->md_info3=$data["employee_id"]; 
                        $instance->md_info4=$data["service_id"];
                        $instance->md_info5=$data["taken_slots"];
                        $instance->md_info=isset($data["base_info"]) ? $data["base_info"] : '';
                        $instance->biz_level_img["1"]=($data["class_pic"] == null) ? "" : str_replace("paptap-thumbs","paptap",$data["class_pic"]); //service pic
                        $instance->md_external_type="class";
                        $instance->md_external_id=$data["class_id"]; // class ID
                        $instance->md_external_sub_id=$data["ccd_id"]; // date ID

                        $subscriptionManager = new customerSubscriptionManager();
                        $instance->in_sub = $subscriptionManager->isItemInCustomerSubscription($data["customer_server_id"],'product',$data["recordID"]) ? "1" : "0";
                        $instance->item_type = 3;
                        break;
                    case "service":
                        $instance->md_type="layout_service";
                        $instance->md_head=stripslashes($data["bmt_name"]); //service name
                        if($data["structure_id"] == 115){
                            $instance->md_info=stripslashes($data["bmt_desc"]); // extra text
                        }
                        
                        $instance->md_desc=stripslashes($data["be_name"]); // employee name
                        $instance->md_icon=str_replace("paptap-thumbs","paptap",$data["be_pic"]); //empoyee pic
                        $instance->md_price=$data["bmt_price"]; //service price
                        $instance->md_info1=$data["bmt_duration"];// service duration
                        $instance->biz_level_img["1"]=($data["md_pic"] == null) ? "" : str_replace("paptap-thumbs","paptap",$data["md_pic"]); //service pic
                        $instance->md_external_type="service";
                        $instance->md_element="52";
                        $instance->item_type = 3;

                        $subscriptionManager = new customerSubscriptionManager();
                        $instance->in_sub = $subscriptionManager->isItemInCustomerSubscription($data["customer_server_id"],'class',$data["md_row_id"]) ? "1" : "0";
                        break;
                }
            }
        }

        return $instance;

    }

    public function calcQtyLeftTotalFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$personal_claimd){

        if ($claims_total_limit > 0 ){
            return $claims_total_limit - $total_claims;
        }else{
            if ($claims_per_member_limit > 0 ){
                return $claims_per_member_limit - $personal_claimd;
            }else{
                if (isset($this->md_price) && $this->md_price > 0){
                    return -2; // no limitations - not free coupon
                }else{
                    return  -1; // no limitations - free coupon
                }
            }
        }

    }

    public function calcQtyLeftPersonalLimitFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$personal_claimd){

        if ($claims_per_member_limit > 0 ){
            return $claims_per_member_limit - $personal_claimd;
        }else{
            if ($claims_total_limit > 0){
                return $claims_total_limit - $total_claims;
            }else{
                if (isset($this->md_price) && $this->md_price > 0){
                    return -2; // no limitations - not free coupon
                }else{
                    return  -1; // no limitations - free coupon
                }
            }
        }
    }
}
?>


