<?php 

class levelData26Object extends levelDataObject {  

    public $md_mod_id = "26";
    public $timeLeft = "";
    public $language = "";
    public $md_item_type = "";
    public $cuponType = "";
    public $min_cart_total = "";
    public $hours = "";
    public $price = "";
    public $vat = "";
    public $item_id = "";
    public $cuponClaimed = "";
    public $redeem_code = "";
    public $enddate;
    public $limit = 0;
    public $claims_left;
    public $exhausted = 0;

 
    public $connected_items = array();
    function __construct(){} 

    public static function withData($data){

        if (!is_array($data) || !isset($data["md_row_id"])){
            throw new Exception("levelData26Object constructor requies data array provided!");
        }

        $customerID = isset($data['customer_id']) ? $data['customer_id'] : 0;
        $instance = new self();

        $instance->fillLevelData($data);
        $instance->md_next_view = "Info";
        $instance->md_item_type = $data["md_item_type"];
        $instance->enddate = $data["md_date2"];
        $to_time = strtotime($data["md_date2"]);
        $from_time = strtotime("now");
        $dtData = "Expired";
        
        $mins = round(($to_time - $from_time) / 60,0);
        if($mins > 0)
        {
            $multyTime = ($mins > 1) ? "s" : "";
            $dtData = $mins. " minute$multyTime";
            if($mins >= 60)
            {
                $mins = round($mins / 60,0);
                $multyTime = ($mins > 1) ? "s" : "";
                $dtData = $mins. " hour$multyTime";
                if($mins >= 24)
                {
                    $mins = round($mins / 24,0);
                    $multyTime = ($mins > 1) ? "s" : "";
                    $dtData = $mins. " day$multyTime";
                }
            }
        }
        $instance->timeLeft=$dtData;

        if($instance->md_level_no == 2){
            $couponManager = new couponsManager();
            $included = $couponManager->getCouponConnectedItemRows($instance);

            if(count($included) > 0){
                
                $lang_code = bizManager::getAppOwnerLang($instance->md_biz_id);
                $instance->language = new languageManager($lang_code,$instance->md_biz_id);
                $instance->language->load('system');

                $text = "\n".$instance->language->get('coupon_includes_mobile');
                
                foreach ($included as $entry)
                {
                    $text .= "\n";
                    if($instance->md_item_type != 'other'){
                        $text .="-";
                    }

                    switch($instance->md_item_type){
                        case 'product':
                            $text .= $entry['md_head'];
                            break;
                        case 'service':
                            $text .= $entry['bmt_name'];
                            break;
                        case 'class':
                            $text .= $entry['calc_name'];
                            break;
                        default:
                            $text .= $entry['ci_item_notes'];
                            break;
                    }
                }
                $instance->md_info .= $text;
            }

            
        }

        $cuponType="";
        $minRequiredTotal = 0;
        if($instance->md_info5 == "1")
        {
            $value = ($instance->md_int1 == 116) ? "%" : " ".$instance->currency;
            $cuponType = $instance->md_price.$value;
            $minRequiredTotal = isset($instance->md_int2) ? $instance->md_int2 : 0;
        }
        if($instance->md_info5 == "2")
        {
            $cuponType = $instance->md_int2."+".$instance->md_int3;
        }
        if($instance->md_info5 == "3")
        {
            $cuponType="";
        }
        
        if(trim($instance->md_info3) != "" || trim($instance->md_info4) != "")
        {
            $hourslimit = $instance->md_info3." - ".$instance->md_info4;
        }
        else
        {
            $hourslimit = "";
        }

        
        

        $redeem_code = isset($data["redeem_code"]) && $data["redeem_code"] != "" ? $data["redeem_code"] : "";
        if($redeem_code == ""){//create new redeem code            
            $redeem_code = utilityManager::generateRedeemCode($instance->md_row_id);
        }

        $instance->limit = couponsManager::getCouponLimitations($instance->md_row_id);
        $total_claims = couponsManager::getClaimedCouponsByItem($instance->md_row_id);
        $claims_total_limit = couponsManager::getCouponTotalLimitations($instance->md_row_id);
        $claims_per_member_limit = couponsManager::getCouponLimitations($instance->md_row_id);

        $left = 0;
        $instance->exhausted = 0;
        $instance->price=isset($data["ite_sell_price"]) ? $data["ite_sell_price"] : 0;
        if($customerID != 0){//Check if customer has exceeded the coupon's limitations
            $claimd = couponsManager::getClaimedCoupoonsForCustomerByCoupon($instance->md_row_id,$customerID);
            
            

            if ($claims_total_limit > $claims_per_member_limit){
                $left = $instance->calcQtyLeftPersonalLimitFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$claimd);
                
            }else{
                $left = $instance->calcQtyLeftTotalFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$claimd);
            }

            $instance->exhausted = $instance->isExhausted($total_claims,$claims_total_limit,$claims_per_member_limit,$claimd) > 0 ? 1 : 0;

        }else{ // no Customer

            if ($claims_total_limit > 0){
                $left = $claims_total_limit - $total_claims;
            }else if (isset($instance->price) && $instance->price > 0){
                $left = -2;
            }else{
                $left = -1;
            }
        }        

        $instance->cuponType = $cuponType;
        $instance->min_cart_total = $minRequiredTotal;
        $instance->hours = $hourslimit;
        $instance->cuponClaimed=$left;
        $instance->redeem_code=$redeem_code;
        
        $instance->vat=isset($data["ite_vat"]) ? $data["ite_vat"] : 0;
        $instance->item_id=isset($data["ite_id"]) ? $data["ite_id"] : 0;
        $instance->item_type="1";

        return $instance;
    }

    public function calcQtyLeftTotalFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$personal_claimd){

        if ($claims_total_limit > 0 ){
           
            return $claims_total_limit - $total_claims;
        }else{
            if ($claims_per_member_limit > 0 ){
               
                return $claims_per_member_limit - $personal_claimd;
            }else{
                if (isset($this->price) && $this->price > 0){
                    return -2; // no limitations - not free coupon
                }else{
                    return  -1; // no limitations - free coupon
                }
            }
        }

    }

    public function calcQtyLeftPersonalLimitFirst($total_claims,$claims_total_limit,$claims_per_member_limit,$personal_claimd){

        if ($claims_per_member_limit > 0 ){
           
            return $claims_per_member_limit - $personal_claimd;
        }else{
            if ($claims_total_limit > 0){
              
                return $claims_total_limit - $total_claims;
            }else{
                if (isset($this->price) && $this->price > 0){
                    return -2; // no limitations - not free coupon
                }else{
                    return  -1; // no limitations - free coupon
                }
            }
        }
    }

    public function isExhausted($total_claims,$claims_total_limit,$claims_per_member_limit,$personal_claimd){
        if($claims_total_limit == 0 && $claims_per_member_limit == 0){
            return false;
        }

        $totalLeft = $claims_total_limit - $total_claims;

        if($totalLeft <= 0 && $claims_total_limit > 0){
            return true;
        }

        $memberleft = $claims_per_member_limit - $personal_claimd;

        if($memberleft <= 0 && $claims_per_member_limit > 0){
            return true;
        }

        return false;

    }

    public function setConnectedItems($items){
        $this->connected_items = $items;
    }
}
?>


