<?php

/**
 * rewardPZObject short summary.
 *
 * rewardPZObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class rewardPZObject extends bobileObject
{
    public $reward_id;
    public $reward_type;
    public $reward_value;
    public $redeemed_code;
    public $item_type;
    public $item_id;
    public $item_type_enum = 0;

    public static function withData($data){
        $instance = new self();

        $instance->reward_id = $data['reward_id'];
        $instance->reward_type = $data['reward_type'];
        $instance->reward_value = $data['reward_value'];
        $instance->redeemed_code = $data['redeemed_code'];
        $instance->item_type = isset($data['item_type']) ? $data['item_type'] : 'other';
        $instance->item_type_enum = isset($data['item_type_enum']) ? $data['item_type_enum'] : 0;
        $instance->item_id = isset($data['item_id']) ? $data['item_id'] : 0;

        return $instance;
    }
}