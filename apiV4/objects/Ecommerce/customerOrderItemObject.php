<?php

/**
 * customerOrderItemObject short summary.
 *
 * customerOrderItemObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerOrderItemObject extends bobileObject
{
    /* Identifiers */
    public $tri_id = "0";
    public $tri_order_id = "0";

    /* Details */
    public $tri_name;
    public $tri_unique_id;
    public $tri_attribute_values;
    public $tri_quantity;
    public $tri_price = "0";
    public $tri_orig_price = "0";
    public $tri_without_vat = "0";
    public $tri_vat = "0";
    public $tri_itm_row_id = "0";
    public $tri_item_id = "0";
    public $tri_item_type = "0";
    public $tri_redeem_code;
    public $tri_redeem_date;
    public $tri_variation_values;
    public $tri_item_sku;
    public $tri_reward_redeem_code;
    public $tri_reward_Id = "0";
    public $tri_reward_type;
    public $tri_reward_value;
    public $tri_item_ext_type;//enum('coupon','product','meeting','class','subscription','punch_pass')
    public $tri_item_ext_id = "0";
    public $tri_multiuse_src_type = "";//enum('','subscription','punch_pass')
    public $tri_multiuse_src_usage_id = "0";
    
    public static function withData($custOrderItemData){
        if(!isset($custOrderItemData["tri_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->tri_id = $custOrderItemData["tri_id"];
        $instance->tri_order_id = $custOrderItemData["tri_order_id"];

        /* Details */
        $instance->tri_name = $custOrderItemData["tri_name"];
        $instance->tri_unique_id = $custOrderItemData["tri_unique_id"];
        $instance->tri_attribute_values = $custOrderItemData["tri_attribute_values"];
        $instance->tri_quantity = $custOrderItemData["tri_quantity"];
        $instance->tri_price = $custOrderItemData["tri_price"];
        $instance->tri_orig_price = $custOrderItemData["tri_orig_price"];
        $instance->tri_without_vat = $custOrderItemData["tri_without_vat"];
        $instance->tri_vat = $custOrderItemData["tri_vat"];
        $instance->tri_itm_row_id = $custOrderItemData["tri_itm_row_id"];
        $instance->tri_item_id = $custOrderItemData["tri_item_id"];
        $instance->tri_item_type = $custOrderItemData["tri_item_type"];
        $instance->tri_redeem_code = $custOrderItemData["tri_redeem_code"];
        $instance->tri_redeem_date = $custOrderItemData["tri_redeem_date"];
        $instance->tri_variation_values = $custOrderItemData["tri_variation_values"];
        $instance->tri_item_sku = $custOrderItemData["tri_item_sku"];
        $instance->tri_reward_redeem_code = $custOrderItemData["tri_reward_redeem_code"];
        $instance->tri_reward_Id = $custOrderItemData["tri_reward_Id"];
        $instance->tri_reward_type = $custOrderItemData["tri_reward_type"];
        $instance->tri_reward_value = $custOrderItemData["tri_reward_value"];
        $instance->tri_item_ext_type = $custOrderItemData["tri_item_ext_type"];
        $instance->tri_item_ext_id = $custOrderItemData["tri_item_ext_id"];
        $instance->tri_multiuse_src_type = $custOrderItemData["tri_multiuse_src_type"];
        $instance->tri_multiuse_src_usage_id = $custOrderItemData["tri_multiuse_src_usage_id"];

        return $instance;
    }

    public static function fillFromRequest($data){
        $instance = new self();

        $instance->tri_id = isset($data['tri_id']) ? $data['tri_id'] : "0";
        /* Details */
        $instance->tri_name = addslashes($data["name"]);        
        $instance->tri_quantity = $data["quantity"];
        $instance->tri_price = $data["totalPrice"];
        $instance->tri_orig_price = isset($data["origItemPrice"]) && $data["origItemPrice"] != "" ? $data["origItemPrice"] : 0;        
        $instance->tri_itm_row_id = $data["recordId"];
        $instance->tri_item_id = $data["item_id"];
        $instance->tri_item_type = $data["item_type"]; 
        $instance->tri_redeem_code = isset($data["redeemCode"]) ? $data["redeemCode"] : '';
        $instance->tri_variation_values = isset($data["variationValues"]) ? addslashes($data["variationValues"]) : '';
        $instance->tri_item_sku = isset($data["productSKU"]) ? addslashes($data["productSKU"]) : '';
        $instance->tri_reward_redeem_code = $data["rewardRedeemCode"];
        $instance->tri_reward_Id = (!isset($data["rewardId"]) || $data["rewardId"] == "") ? "0" : $data["rewardId"];
        $instance->tri_reward_type = addslashes($data["rewardType"]);
        $instance->tri_reward_value = $data["rewardValue"] == "" ? "0" : $data["rewardValue"];
        $instance->tri_multiuse_src_type = isset($data["multiuse_src_type"]) ? $data["multiuse_src_type"] : '' ;
        $instance->tri_multiuse_src_usage_id = isset($data["multiuse_id"]) ? $data["multiuse_id"] : 0 ;

        return $instance;
    }
}
