<?php

/**
 * customerOrderObject short summary.
 *
 * customerOrderObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerOrderObject extends customerPersonalObject
{
    /* Identifiers */
    public $cto_id = "0";
    public $cto_cust_id = "0";
    public $cto_biz_id = "0";

    /* Details */
    public $cto_amount = "0";
    public $cto_original_amount = "0";
    public $cto_without_vat = "0";
    public $cto_vat = "0";
    public $cto_sipping = "0";
    public $cto_membership_discount = "0";
    public $cto_benefit_id = "0";
    public $cto_benefit_value = "0";
    public $cto_shipping_type_id = "0";
    public $cto_currency;
    public $cto_order_tyme;
    public $cto_device_id;   
    public $cto_cancelled = "0";
    public $cto_status = "1";
    public $cto_charge_status = "one_time";
    public $cto_note;
    public $cto_status_date;
    public $cto_addr_name;
    public $cto_addr_country;
    public $cto_addr_state;
    public $cto_addr_city;
    public $cto_addr_line1;
    public $cto_addr_line2;
    public $cto_addr_pob = "0";
    public $cto_addr_postcode;
    public $cto_addr_phone;
    public $cto_buyer_email;
    public $cto_long_order_id;
    public $cto_paid_manual = "0";
    public $cto_order_type = "eCommerce";//enum('eCommerce','paymentRequest')
    public $cto_due_date;
    public $cto_title;
    public $cto_unique_id;
    public $cto_reminder_type;//enum('','Daily','Weekly','Monthly','Yearly')
    public $cto_reminder_time;
    public $cto_next_reminder;
    public $cto_reminder_text;
    public $cto_discount = "0";
    public $cto_seen_on;

    public $status_name = 'Initiated';
    public $ship_method_name = "";
    public $ship_method_time = "";

    /* Items */
    public $items = array();

    public static function withData($custOrderData){
        if(!isset($custOrderData["cto_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->cto_id = isset($custOrderData["cto_id"]) ? $custOrderData["cto_id"] : 0;
        $instance->cto_cust_id = isset($custOrderData["cto_cust_id"]) ? $custOrderData["cto_cust_id"] : 0;
        $instance->cto_biz_id = isset($custOrderData["cto_biz_id"]) ? $custOrderData["cto_biz_id"] : 0;

        /* Details */
        $instance->cto_amount = $custOrderData["cto_amount"];
        $instance->cto_original_amount = isset($custOrderData["cto_original_amount"]) ? $custOrderData["cto_original_amount"] : 0;
        $instance->cto_without_vat = $custOrderData["cto_without_vat"];
        $instance->cto_vat = $custOrderData["cto_vat"];
        $instance->cto_sipping = $custOrderData["cto_sipping"];
        $instance->cto_membership_discount = $custOrderData["cto_membership_discount"];
        $instance->cto_benefit_id = $custOrderData["cto_benefit_id"];
        $instance->cto_benefit_value = $custOrderData["cto_benefit_value"];
        $instance->cto_shipping_type_id = $custOrderData["cto_shipping_type_id"];
        $instance->cto_currency = $custOrderData["cto_currency"];
        $instance->cto_order_tyme = $custOrderData["cto_order_tyme"];
        $instance->cto_device_id = $custOrderData["cto_device_id"];
        $instance->cto_cancelled = $custOrderData["cto_cancelled"];
        $instance->cto_status = $custOrderData["cto_status"];
        $instance->cto_charge_status = $custOrderData["cto_charge_status"];
        $instance->cto_note = $custOrderData["cto_note"];
        $instance->cto_status_date = $custOrderData["cto_status_date"];
        $instance->cto_addr_name = $custOrderData["cto_addr_name"];
        $instance->cto_addr_country = $custOrderData["cto_addr_country"];
        $instance->cto_addr_state = $custOrderData["cto_addr_state"];
        $instance->cto_addr_city = $custOrderData["cto_addr_city"];
        $instance->cto_addr_line1 = $custOrderData["cto_addr_line1"];
        $instance->cto_addr_line2 = $custOrderData["cto_addr_line2"];
        $instance->cto_addr_pob = $custOrderData["cto_addr_pob"];
        $instance->cto_addr_postcode = $custOrderData["cto_addr_postcode"];
        $instance->cto_addr_phone = $custOrderData["cto_addr_phone"];
        $instance->cto_buyer_email = $custOrderData["cto_buyer_email"];
        $instance->cto_long_order_id = $custOrderData["cto_long_order_id"];
        $instance->cto_paid_manual = $custOrderData["cto_paid_manual"];
        $instance->cto_order_type = $custOrderData["cto_order_type"];
        $instance->cto_due_date = $custOrderData["cto_due_date"];
        $instance->cto_title = $custOrderData["cto_title"];
        $instance->cto_unique_id = $custOrderData["cto_unique_id"];
        $instance->cto_reminder_type = $custOrderData["cto_reminder_type"];
        $instance->cto_reminder_time = $custOrderData["cto_reminder_time"];
        $instance->cto_next_reminder = $custOrderData["cto_next_reminder"];
        $instance->cto_reminder_text = $custOrderData["cto_reminder_text"];
        $instance->cto_discount = $custOrderData["cto_discount"];
        $instance->cto_seen_on = $custOrderData["cto_seen_on"];
        $instance->status_name = $custOrderData["ors_name"];

        $instance->ship_method_name = isset($custOrderData["ship_method_name"]) ? $custOrderData["ship_method_name"] : "";
        $instance->ship_method_time = isset($custOrderData["ship_method_time"]) ? $custOrderData["ship_method_time"] : "";

        return $instance;
    }

    public static function fillFromRequest($data,$device_id){
        $instance = new self();
        
        $instance->cto_shipping_type_id = (urldecode($data["shipMethodId"]) == "") ? 0 : urldecode($data["shipMethodId"]);
        $instance->cto_sipping = ($data["shipMethodPrice"] == "") ? 0 : $data["shipMethodPrice"];
        $instance->cto_membership_discount = ($data["membershipDiscount"] == "") ? 0 : $data["membershipDiscount"];
        
        $instance->cto_note = $data["comments"];
        $instance->cto_benefit_id = isset($data["cartBenefitId"]) ? $data["cartBenefitId"] : 0;
        $instance->cto_benefit_value = isset($data["cartBenefitValue"]) && $data["cartBenefitValue"] != '' ? $data["cartBenefitValue"] : 0;
                
        //Order Fields - Purchase Details
        $instance->cto_amount = $data["amount"];      //Becomes Total
        $instance->cto_without_vat = $data["subTotal"];        //To Add
        $instance->cto_vat = $data["tax"];            //To Add
        $instance->cto_currency = (urldecode($data["currency"]));

        //Order Fields - Shipping Details
        $instance->cto_addr_country = isset($data["shipCountry"]) ? (urldecode($data["shipCountry"])) : "";            //To Add
        $instance->cto_addr_state = isset($data["shipState"]) ? (urldecode($data["shipState"])) : "";            //To Add
        $instance->cto_addr_city = isset($data["shipCity"]) ? (urldecode($data["shipCity"])) : "";            //To Add
        $instance->cto_addr_line1 = isset($data["shipLine1"]) ? (urldecode($data["shipLine1"])) : "";            //To Add
        $instance->cto_addr_line2 = isset($data["shipLine2"]) ? (urldecode($data["shipLine2"])) : "";            //To Add
        $instance->cto_addr_postcode = isset($data["shipZip"]) ? (urldecode($data["shipZip"])) : "";             //To Add
        $instance->cto_addr_name = isset($data["shipName"]) ? (urldecode($data["shipName"])) : "";             //To Add
        $instance->cto_device_id = $device_id;

        $isBad = utilityManager::contains($data["shipPhone"],'UITextField') ? 1 : 0;       
        
        $instance->cto_addr_phone = $isBad == 1 ? '' : (urldecode($data["shipPhone"]));             //To Add
        $instance->cto_buyer_email = (urldecode($data["email"]));

        return $instance;
    }

    public function fillPaymentRequestOrder(customerOrderObject $payment){
        $this->cto_original_amount = $this->cto_amount;

        $this->cto_shipping_type_id = $payment->cto_shipping_type_id;
        $this->cto_sipping = $payment->cto_sipping;
        $this->cto_membership_discount = $payment->cto_membership_discount;
        
        $this->cto_note = $payment->cto_note;
        $this->cto_benefit_id = $payment->cto_benefit_id;
        $this->cto_benefit_value = $payment->cto_benefit_value;
        
        //Order Fields - Purchase Details
        $this->cto_amount = $payment->cto_amount;      //Becomes Total
        $this->cto_without_vat = $payment->cto_without_vat;        //To Add
        $this->cto_vat = $payment->cto_vat;            //To Add
        $this->cto_currency = $payment->cto_currency;

        //Order Fields - Shipping Details
        $this->cto_addr_country = $payment->cto_addr_country;            //To Add
        $this->cto_addr_state = $payment->cto_addr_state;            //To Add
        $this->cto_addr_city = $payment->cto_addr_city;            //To Add
        $this->cto_addr_line1 = $payment->cto_addr_line1;            //To Add
        $this->cto_addr_line2 = $payment->cto_addr_line2;            //To Add
        $this->cto_addr_postcode = $payment->cto_addr_postcode;             //To Add
        $this->cto_addr_name = $payment->cto_addr_name;             //To Add
        $this->cto_device_id = $payment->cto_device_id;

             
        
        $this->cto_addr_phone = $payment->cto_addr_phone;             //To Add
        $this->cto_buyer_email = $payment->cto_buyer_email;
    }

    public function PersonalZoneAPIArray(mobileRequestObject $request = null){
        $result = array();

        $customerBilling = new customerBillingManager();

        

        $paymentMethod = "";
        $paymentMethodDb = "";
        
        $payd = 0;
        if($this->cto_amount > 0){
            $orderInvoice = $customerBilling->getInvoiceForOrderByOrderID($this->cto_id)->data;

            $payd = $orderInvoice->cin_status == 'paid' || $orderInvoice->cin_status == 'refunded' ? 1 : 0;
            
            $transResult = $customerBilling->getTransactionForOrderByOrderID($this->cto_id);
            if($transResult->code == 1){
                $transaction = $transResult->data;
                $paymentMethodDb = $transaction->tr_paymentMethodType;
            }
            if($paymentMethodDb == "CreditCard") $paymentMethod = "Credit Card";

            //don't tuch this place (if neede) before talk to dany
            if($paymentMethod == "" && $orderInvoice->cin_stripe_invoice_id == ""){
                if($payd == 1){
                    $paymentMethod = "Cash";
                }else{
                    $paymentMethod = "N/A";
                }
            }
            if($payd == 0 && $this->cto_order_type == 'paymentRequest' && $orderInvoice->cin_status == 'pending'){
                $paymentMethod = "Cash";
            }

        }else{
            $paymentMethod = "N/A";
        }

        $result['recordID'] = $this->cto_id; 
        $result['cto_long_order_id']=$this->cto_long_order_id;
        $result['status_id']=$this->cto_status;
        $result['status_name']=$this->status_name;
        $result['status_date']=$this->cto_status_date;
        $result['cto_amount']=$this->cto_amount;
        $result['cto_without_vat']=$this->cto_without_vat;
        $result['cto_vat']=$this->cto_vat;
        $result['cto_addr_country']=$this->cto_addr_country;
        $result['cto_addr_state']=$this->cto_addr_state;
        $result['cto_addr_city']=$this->cto_addr_city;
        $result['cto_addr_line1']=$this->cto_addr_line1;
        $result['cto_addr_line2']=$this->cto_addr_line2;
        $result['cto_addr_postcode']=$this->cto_addr_postcode;
        $result['cto_addr_name']=$this->cto_addr_name;
        $result['cto_addr_phone']=$this->cto_addr_phone;
        $result['cto_buyer_email']=$this->cto_buyer_email;
        $result['cto_sipping']=$this->cto_sipping;
        $result['cto_shipping_type_id']=$this->cto_shipping_type_id;
        $result['ship_method_name']=$this->ship_method_name;
        $result['ship_method_time']=$this->ship_method_time;                
        $result['cto_order_tyme']=$this->cto_order_tyme;
        $result['cto_order_tyme_unix']=strtotime($this->cto_order_tyme);
        $result['cto_currency']=$this->cto_currency;
        $result['cto_due_date']=$this->cto_due_date;
        $result['cto_due_date_unix']=isset($this->cto_due_date) ? strtotime($this->cto_due_date) : null;
        $result['cto_note']=$this->cto_note;
        $result['cto_membership_discount']=$this->cto_membership_discount;
        $result['cto_benefit_value']=$this->cto_benefit_value;
        $result['cto_benefit_name']="";
        $result['cto_benefit_orig_value']="";
        $result['cto_benefit_orig_value_type']="";
        $result['cto_benefit_name']="";
        $result['tr_paymentMethodType']=$paymentMethod;
        $result['tr_paymentMethodStatus']=$payd;
        if($this->cto_benefit_value > 0 && $this->cto_benefit_id > 0){
            $couponResult = customerManager::getCouponDataFromBenefitByBenefitID($this->cto_benefit_id);
            if($couponResult->code == 1){
                $result['cto_benefit_orig_value']= $couponResult->data->md_price;
                $result['cto_benefit_orig_value_type']= $couponResult->data->md_int1 == 116 ? 'percent' : 'amount';
                $result['cto_benefit_name'] = $couponResult->data->md_head;
            }
        }
        if(count($this->items) > 0){
            $itemData = array();
            foreach ($this->items as $item)
            {
                $resultItem = array();
                $fromTable = $item->tri_item_type == 1 ? 26 : 9;

                $img = "";
                $item_row_id = 0;
                if($item->tri_item_type == 3){
                    $attributes = explode(',', $item->tri_variation_values);
                    if(count($attributes) > 0){
                        if($item->tri_item_ext_type == "meeting"){
                            $meetingResult = bookingManager::getEmployeeMeetingByID($item->tri_item_ext_id);
                            if($meetingResult->code == 1){
                                $meeting = $meetingResult->data;
                                $employeePic = $meeting->employee->be_pic;
                                $item_row_id = $meeting->em_meet_type;
                                $img = $employeePic != "" ? $employeePic : IMAGE_STORAGE_PATH."system/no_employee_image_square.png";
                            }
                        }
                        else{
                            $classCustomerDateResult = bookingManager::getClassDateCustomerByID($item->tri_item_ext_id);
                            if($classCustomerDateResult->code == 1){
                                $classCustomerDate = $classCustomerDateResult->data;
                                $class = bookingManager::getClassByID($classCustomerDate->cdc_class_id)->data;
                                $employeePic = $class->employee->be_pic;
                                $item_row_id = $class->calc_id;
                                $img = $employeePic != "" ? $employeePic : IMAGE_STORAGE_PATH."system/no_employee_image_square.png";
                            }

                        }
                        
                        
                    }
                }else{
                    $levelManager = new levelDataManager($fromTable);
                    $levelResult = $levelManager->getLevelDataByID($item->tri_itm_row_id);
                    $img = $levelResult->data->md_icon;
                }

                if($item->tri_item_type == 2){
                    $item_row_id = $item->tri_itm_row_id;
                }

                if($item->tri_item_type == 4){
                    $extraData = array();

                    $subResult = customerSubscriptionManager::getCustomerSubscriptionByCustSubID($item->tri_item_ext_id);

                    if($subResult->code == 1){

                        $subData = $subResult->data;
                        $extraData['limitations'] = $subData->csu_usage_rule == 'unlimited' ? '' : $subData->csu_usage_capacity.'/'.$subData->csu_usage_period_unit;

                        $extraData['start'] = $subData->csu_usage_start_time;
                        $extraData['start_unix'] = strtotime($subData->csu_usage_start_time);

                        $extraData['end'] = isset($subData->csu_usage_end_time) ? $subData->csu_usage_end_time : '';
                        $extraData['end_unix'] = isset($subData->csu_usage_end_time) ? strtotime($subData->csu_usage_end_time) : '';

                        $resultItem['extra_data'] = $extraData;
                    }
                }

                if($item->tri_item_type == 5){
                    $extraData = array();

                    $subResult = customerSubscriptionManager::getCustomerPunchpassByCustPpassID($item->tri_item_ext_id);

                    if($subResult->code == 1){
                        $subData = $subResult->data;
                        $extraData['entries'] = $subData->cpp_usage_capacity;

                        $extraData['start'] = $subData->cpp_usage_start_time;
                        $extraData['start_unix'] = strtotime($subData->cpp_usage_start_time);

                        $extraData['end'] = isset($subData->cpp_usage_end_time) ? $subData->cpp_usage_end_time : '';
                        $extraData['end_unix'] = isset($subData->cpp_usage_end_time) ? strtotime($subData->cpp_usage_end_time) : '';

                        $resultItem['extra_data'] = $extraData;
                    }
                }

                
                $resultItem["tri_id"] = $item->tri_id;
                $resultItem["tri_name"] = $item->tri_name;
                $resultItem["tri_quantity"] = $item->tri_quantity;
                $resultItem["tri_price"] = $item->tri_price;
                $resultItem["tri_orig_price"] = $item->tri_orig_price;
                $resultItem["md_icon"] = $img;
                $resultItem["tri_type"] = $item->tri_item_type;
                $resultItem["tri_variation_values"] = $item->tri_variation_values;
                $resultItem["tri_itm_row_id"] = $item_row_id;

                $itemData[] = $resultItem;
            }
            $result['items'] = $itemData;
        }
        

        return $result;
    }

    /**
     * Summary of setItems
     * @param array $itemsList - list of customerOrderItemObject
     */
    public function setItems($itemsList){
        $this->items = $itemsList;
    }

    public function getItems(){
        return $this->items;
    }
}
