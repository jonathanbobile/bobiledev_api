<?php 

class customerTransactionObject extends bobileObject { 

    public $tr_id = "0";
    public $tr_date = "";
    public $tr_biz_id = "0";
    public $tr_device_id;
    public $tr_cust_id = "0";
    public $tr_order_id = "0";
    public $tr_invoice_id = "0";
    public $tr_cust_payment_source_id = "0";
    public $tr_reference_Id;
    public $tr_capture_code;
    public $tr_reconciliation_Id;
    public $tr_type;
    public $tr_amount = "0";
    public $tr_currency;
    public $tr_paymentMethodToken;
    public $tr_paymentMethodType;
    public $tr_paymentMethodLastDigits = "0";
    public $tr_cvv;
    public $tr_vendor;
    public $tr_ip;
    public $tr_country = "0";
    public $tr_phone;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["tr_id"])){
              throw new Exception("customerPaymentChargeObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->tr_id = $data["tr_id"];
        $instance->tr_date = $data["tr_date"];
        $instance->tr_biz_id = $data["tr_biz_id"];
        $instance->tr_device_id = $data["tr_device_id"];
        $instance->tr_cust_id = $data["tr_cust_id"];
        $instance->tr_order_id = $data["tr_order_id"];
        $instance->tr_invoice_id = $data["tr_invoice_id"];
        $instance->tr_cust_payment_source_id = $data["tr_cust_payment_source_id"];
        $instance->tr_reference_Id = $data["tr_reference_Id"];
        $instance->tr_capture_code = $data["tr_capture_code"];
        $instance->tr_reconciliation_Id = $data["tr_reconciliation_Id"];
        $instance->tr_type = $data["tr_type"];
        $instance->tr_amount = $data["tr_amount"];
        $instance->tr_currency = $data["tr_currency"];
        $instance->tr_paymentMethodToken = $data["tr_paymentMethodToken"];
        $instance->tr_paymentMethodType = $data["tr_paymentMethodType"];
        $instance->tr_paymentMethodLastDigits = isset($data["tr_paymentMethodLastDigits"]) ? $data["tr_paymentMethodLastDigits"] : $instance->tr_paymentMethodLastDigits;
        $instance->tr_cvv = $data["tr_cvv"];
        $instance->tr_vendor = $data["tr_vendor"];
        $instance->tr_ip = $data["tr_ip"];
        $instance->tr_country = $data["tr_country"];
        $instance->tr_phone = $data["tr_phone"];

        return $instance;
    }

    public static function fillFromRequest($SOURCE,$device_id){
        $instance = new self();
        
        $instance->tr_reference_Id = isset($SOURCE["processorReferenceId"]) ? addslashes(urldecode($SOURCE["processorReferenceId"])) : '';
        $instance->tr_capture_code = isset($SOURCE["captureCode"]) ? addslashes(urldecode($SOURCE["captureCode"])) : '';
        $instance->tr_reconciliation_Id = addslashes(urldecode($SOURCE["reconciliationId"]));
        $instance->tr_paymentMethodToken = isset($SOURCE["paymentMethodToken"]) ? addslashes(urldecode($SOURCE["paymentMethodToken"])) : '';
        $instance->tr_paymentMethodType = addslashes(urldecode($SOURCE["paymentMethodType"]));
        $instance->tr_paymentMethodLastDigits = isset($SOURCE["paymentMethodLastDigits"]) ? addslashes(urldecode($SOURCE["paymentMethodLastDigits"])) : $instance->tr_paymentMethodLastDigits;
        $instance->tr_cvv = isset($SOURCE["cvvNumber"]) ? addslashes(urldecode($SOURCE["cvvNumber"])) : '';
        $instance->tr_ip = addslashes(urldecode($SOURCE["ipAddress"]));
        $instance->tr_type = 'CHARGE';
        $instance->tr_device_id = $device_id;
        $instance->tr_phone = addslashes(urldecode($SOURCE["phone"]));
        $instance->tr_amount = isset($SOURCE["amount"]) ? $SOURCE["amount"] : 0;
        $instance->tr_currency = addslashes(urldecode($SOURCE["currency"]));

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


