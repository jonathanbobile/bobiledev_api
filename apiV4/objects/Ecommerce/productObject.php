<?php

/**
 * productObject short summary.
 *
 * productObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class productObject extends levelData9Object
{
    /* Attributes */
    public $attributes = array();

    /* Variations */
    public $variations = array();

    /* Categories */
    public $categories = array();

    public function setVariations($variationsList){
        if(!is_array($variationsList)){
            throw new Exception('Variations list is not an array');
        }

        $this->variations = $variationsList;
    }

    public function setCategories($categoriesList){
        if(!is_array($categoriesList)){
            throw new Exception('Categories list is not an array');
        }

        $this->categories = $categoriesList;
    }
}
