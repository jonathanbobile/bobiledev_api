<?php

/**
 * Enum for info banner types
 *
 * @version 1.0
 * @author bobile
 */

class enumBannerType extends Enum {
    
    const charge_declined = 1;

    const subscription_has_been_canceled = 2;     

    const update_google_dev_agreement = 3;     

    const update_apple_dev_agreement = 4;
    
    const app_rejected_google = 5;

    const app_rejected_google = 6;

    const trial_period_timer = 7;

    const setup_payment_gateway = 8;

    const prizes_scratch_card_over = 9;

    const setup_white_label = 10;

    const activate_account = 11;

    const connected_coupon_expires_week = 12;

    const connected_class_has_only_one_date = 13;

    const account_activated = 14;

    const account_canceled_successfully = 15;

    const app_published_google = 16;

    const app_published_apple = 17;

    const app_published_bobile = 18;

    const app_updated_google = 19;

    const app_updated_apple = 20;

    const app_updated_bobile = 21;

   const charge_declined_reseller = 22;

   const activate_low_tier_account = 23;

   const connected_subscriptions_expires_week = 24;

   const connected_punch_pass_expires_week = 25;
}

?>
