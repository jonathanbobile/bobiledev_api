<?php

/**
 * pushParamsObject short summary.
 *
 * pushParamsObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class pushParamsObject extends bobileObject
{
    public $biz_id;
    public $cust_id;
    public $push_type;
    public $message = '';
    public $extra_params = array();
}