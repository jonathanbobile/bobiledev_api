<style>
    h1 {
        color: red;
        font-size: 30px;
    }
    .main-wrapper {
        text-align: center;
        margin-top:100px;
        font-family: 'Open Sans Light', 'Arial';
    }
    .error-message {
        font-size: 25px;
    }
</style>
<div class="main-wrapper">
    <h1>Error</h1>
    <div class="error-message"><?= $this->message?></div>
</div>