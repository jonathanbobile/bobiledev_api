<?php

/**
 * Manager short summary.
 *
 * Manager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class Manager
{
    protected $db;

    public function __construct(){

        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }

    }
}
