<?php

class Admin extends Controller{

    public $mobileRequestObject = array();
    public $adminModel;

    public function __construct(){
        parent::__construct();
        set_time_limit(0);
        $this->mobileRequestObject = adminRequestObject::withData($_REQUEST);
        $this->adminModel = new adminModel($this->mobileRequestObject);
    }
    
    function login(){
        try{
            $type = isset($_REQUEST["type"]) ? $_REQUEST["type"] : "1";
            
            $username = $_REQUEST["username"];
            $password = $_REQUEST["password"];

            $resellerID = $this->mobileRequestObject->resellerId > 0 ? $this->mobileRequestObject->resellerId : 0;
            if($resellerID > 0){
                $partnersModel = new partnerModel();
                $resellerResult = $partnersModel->getResellerByID($resellerID);
                
                
                if($resellerResult->code == 1){
                    
                    $reseller = $resellerResult->data;
                    if($reseller->reseller_deactivated == 1){
                        return $this->returnAnswer(resultObject::withData(0,'reseller_deactivated'));
                    }

                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'no_reseller_found'));
                }
            }

            $accountManager = new accountManager();

            $accountResult = $accountManager->getAccountForLogin($username,$password,$resellerID,$type);
            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }


            $account = $accountResult->data;


            $response = array();

            $response['ac_id'] = utilityManager::encodeToHASH($account->ac_id);
            $response["ac_name"]  = $account->ac_name;            
            $response["ac_last_biz"]  = utilityManager::encodeToHASH($account->ac_last_bizid);
            $response["ac_biz_count"]  = $this->adminModel->getBizCountForAccount($account->ac_id);

            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizListForAccount(){
        try{
            $skip = isset($_REQUEST['current_count']) ? $_REQUEST['current_count'] : 0;
            $keyword = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
            $take = 20;
            $bizList = $this->adminModel->getFullBizListForAccountID($this->mobileRequestObject->accountID,$skip,$take,$keyword);

            return $this->returnAnswer(resultObject::withData(1,'',$bizList));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function logout($userName){
        try{
            return $this->returnAnswer($this->adminModel->logoutUser($userName));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizData(){
        try{
            $connectedAccountID = $_REQUEST['connected_account'];

            $accountManager = new accountManager();

            $accountResult = $accountManager->getAccountById($connectedAccountID);

            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }

            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $bizResult = $bizModel->getBizForAdmin($accountResult->data);            

            return $this->returnAnswer($bizResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }
    
    function getSettings(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $bizSettingsResult = $bizModel->getBizSettingsForAdmin();

            return $this->returnAnswer($bizSettingsResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setSettings(){
        try{
            
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }
}
