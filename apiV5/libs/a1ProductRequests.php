<?php

/**
 * A1 Product Requests Class
 *
 * @version 1.0
 * @author bobile Dany
 */
require_once(LIBS_PATH.'Composer/vendor/autoload.php');

class a1ProductRequests extends \Connect\FulfillmentAutomation 
{
    public $templteId;
    public $activationMessage;

    /**
     * @param \Connect\Request $request
     * @return string|void
     * @return \Connect\ActivationTemplateResponse
     * @return \Connect\ActivationTileResponse  
     * @throws Exception
     * @throws \Connect\Exception
     * @throws \Connect\Fail
     * @throws \Connect\Skip
     * @throws \Connect\Inquire   
     */

    public function processRequest($request)
    {
        $partnerModel = new partnerModel();

        switch ($request->type) {
            case "purchase":

                $licenseObject = new a1LicenseObject();

                $licenseObject->a1_request_id = !isset($request->id) ? $licenseObject->a1_request_id : $request->id;
                $licenseObject->a1_asset_id = !isset($request->asset->id) ? $licenseObject->a1_asset_id : $request->asset->id;
                $licenseObject->a1_request_type = !isset($request->type) ? $licenseObject->a1_request_type : $request->type;
                $licenseObject->a1_plan_id  = $licenseObject->getPlanId($request);
                $licenseObject->a1_customer_id = !isset($request->asset->tiers->customer->id) ? $licenseObject->a1_customer_id : $request->asset->tiers->customer->id;
                $licenseObject->a1_customer_organization = !isset($request->asset->tiers->customer->name) ? $licenseObject->a1_customer_organization : $request->asset->tiers->customer->name;
                $licenseObject->a1_customer_first_name = !isset($request->asset->tiers->customer->contact_info->contact->first_name) ? $licenseObject->a1_customer_first_name : $request->asset->tiers->customer->contact_info->contact->first_name;
                $licenseObject->a1_customer_last_name = !isset($request->asset->tiers->customer->contact_info->contact->last_name) ? $licenseObject->a1_customer_last_name : $request->asset->tiers->customer->contact_info->contact->last_name;
                $licenseObject->a1_customer_email = !isset($request->asset->tiers->customer->contact_info->contact->email) ? $licenseObject->a1_customer_email : $request->asset->tiers->customer->contact_info->contact->email;
                $licenseObject->a1_environment = !isset($request->asset->connection->type) ? $licenseObject->a1_environment : $request->asset->connection->type;

                if(isset($request->asset->tiers->customer->contact_info->contact->phone_number)){
                    $phoneNumberObject = $request->asset->tiers->customer->contact_info->contact->phone_number;
                    $licenseObject->a1_customer_phone = $phoneNumberObject->country_code . $phoneNumberObject->area_code . $phoneNumberObject->phone_number;
                }

                $response = $partnerModel->addA1License($licenseObject);
                if($response->code == 1){

                    $licenseId = $response->data;

                    $historyObject = new a1HistoryObject();
                    $historyObject->a1h_license_id = $licenseId;
                    $historyObject->a1h_request_id = $licenseObject->a1_request_id;
                    $historyObject->a1h_request_type = $licenseObject->a1_request_type;
                    $historyObject->a1h_plan_id = $licenseObject->a1_plan_id;
                    $responseHistory = $partnerModel->addA1History($historyObject);

                    if($responseHistory->code == 1){
                        if(isset($this->templteId)){
                            //We may use a template defined on vendor portal as activation response, this will be what customer sees on panel
                            return new \Connect\ActivationTemplateResponse($this->templteId);
                        }else{
                            // We may use arbitrary output to be returned as approval, this will be seen on customer panel. Please see that output must be in markup format
                            return new \Connect\ActivationTileResponse($this->activationMessage);
                        }
                    }
                }
            case "cancel":
            case "suspend":
                
                $responseLicense = $partnerModel->getA1LicenseByAsset($request->asset->id);

                if($responseLicense->code == 1){
                    $licenseObject = $responseLicense->data;

                    $this->updateProcess($request,$licenseObject);

                    if($licenseObject->a1_biz_id > 0){
                        $partnerModel->suspendA1Biz($licenseObject->a1_biz_id);
                    }

                    return new \Connect\ActivationTileResponse("Your account has been suspended");
                }
            case "resume":

                $responseLicense = $partnerModel->getA1LicenseByAsset($request->asset->id);

                if($responseLicense->code == 1){
                    $licenseObject = $responseLicense->data;

                    $this->updateProcess($request,$licenseObject);

                    if($licenseObject->a1_biz_id > 0){
                        $partnerModel->restoreA1Biz($licenseObject->a1_biz_id);
                    }

                    return new \Connect\ActivationTemplateResponse($this->templteId);
                }
            case "change":
            
                $responseLicense = $partnerModel->getA1LicenseByAsset($request->asset->id);

                if($responseLicense->code == 1){
                    $licenseObject = $responseLicense->data;

                    $this->updateProcess($request,$licenseObject);
                    return new \Connect\ActivationTemplateResponse($this->templteId);
                }
            default:
                throw new \Connect\Fail("Operation not supported:".$request->type);
        }
    }

    private function updateProcess($request, a1LicenseObject $licenseObject){

        $partnerModel = new partnerModel();

        $licenseObject->a1_request_id = !isset($request->id) ? $licenseObject->a1_request_id : $request->id;
        $licenseObject->a1_request_type = !isset($request->type) ? $licenseObject->a1_request_type : $request->type;
        $licenseObject->a1_plan_id  = $licenseObject->getPlanId($request);
        $licenseObject->a1_customer_id = !isset($request->asset->tiers->customer->id) ? $licenseObject->a1_customer_id : $request->asset->tiers->customer->id;
        $licenseObject->a1_customer_organization = !isset($request->asset->tiers->customer->name) ? $licenseObject->a1_customer_organization : $request->asset->tiers->customer->name;
        $licenseObject->a1_customer_first_name = !isset($request->asset->tiers->customer->contact_info->contact->first_name) ? $licenseObject->a1_customer_first_name : $request->asset->tiers->customer->contact_info->contact->first_name;
        $licenseObject->a1_customer_last_name = !isset($request->asset->tiers->customer->contact_info->contact->last_name) ? $licenseObject->a1_customer_last_name : $request->asset->tiers->customer->contact_info->contact->last_name;
        $licenseObject->a1_customer_email = !isset($request->asset->tiers->customer->contact_info->contact->email) ? $licenseObject->a1_customer_email : $request->asset->tiers->customer->contact_info->contact->email;

        if(isset($request->asset->tiers->customer->contact_info->contact->phone_number)){
            $phoneNumberObject = $request->asset->tiers->customer->contact_info->contact->phone_number;
            $licenseObject->a1_customer_phone = $phoneNumberObject->country_code . $phoneNumberObject->area_code . $phoneNumberObject->phone_number;
        }

        $response = $partnerModel->updateA1License($licenseObject);
        if($response->code == 1){
            $historyObject = new a1HistoryObject();
            $historyObject->a1h_license_id = $licenseObject->a1_id;
            $historyObject->a1h_request_id = $licenseObject->a1_request_id;
            $historyObject->a1h_request_type = $licenseObject->a1_request_type;
            $historyObject->a1h_plan_id = $licenseObject->a1_plan_id;
            $partnerModel->addA1History($historyObject);
        }
    }

    public function processTierConfigRequest($tierConfigRequest){
       
    }

}
?>


