<?php

/**
 * adminManager short summary.
 *
 * adminManager description.
 *
 * @version 1.0
 * @author יהנתן
 */
class adminManager extends Manager
{
    public static function sendUpdateToBizDevices($bizID,$message_type,$message){
        $instance = new self();

        $activeDevices  = $instance->getActiveDevicesForBiz($bizID);

        foreach ($activeDevices as $activeDevice)
        {
        	$receiver = utilityManager::encodeToHASH($activeDevice["aad_ac_id"]);

            wsocketManager::sendIMAsync($message,$receiver,$message_type);
        }

        return;
    }

    private function getActiveDevicesForBiz($bizID){
        return $this->db->getTable("SELECT * FROM tbl_account_active_devices
                                    WHERE aad_active_biz_id = $bizID
                                    AND aad_logged_off IS NULL");
    }

}

