<?php

/**
 * customFormManager short summary.
 *
 * customFormManager description.
 *
 * @version 1.0
 * @author Dany
 */
class customFormManager extends Manager
{
    public $mobileRequestObject = array();
    protected $dbForms;

    function __construct(){
        parent::__construct();

        try {
            $this->dbForms = new Database("forms");
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        } 
    }

    /************************************* */
    /*   BASIC FORMFIELD - PUBLIC           */
    /************************************* */

    /**
     * Insert new formFieldObject to DB
     * Return Data = new formFieldObject ID
     * @param formFieldObject $formFieldObj 
     * @return resultObject
     */
    public function addFormField(formFieldObject $formFieldObj){       
        try{
            $newId = $this->addFormFieldDB($formFieldObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($formFieldObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get formField from DB for provided ID
     * * Return Data = formFieldObject
     * @param int $formFieldId 
     * @return resultObject
     */
    public function getFormFieldByID($formFieldId){
        
        try {
            $formFieldData = $this->loadFormFieldFromDB($formFieldId);
            
            $formFieldObj = formFieldObject::withData($formFieldData);
            $result = resultObject::withData(1,'',$formFieldObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$formFieldId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update formField in DB
     * @param formFieldObject $formFieldObj 
     * @return resultObject
     */
    public function updateFormField(formFieldObject $formFieldObj){        
        try{
            $this->upateFormFieldDB($formFieldObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($formFieldObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete formField from DB
     * @param int $formFieldID 
     * @return resultObject
     */
    public function deleteFormFieldById($formFieldID){
        try{
            $this->deleteFormFieldByIdDB($formFieldID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$formFieldID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*          GENERAL FUNCTIONS          */
    /************************************* */

    public function getFormFields($formId,$formDataId){
        
        $levelDataManager = new levelDataManager(32);
        $fieldsArray = array();

        $levelResponse = $levelDataManager->getLevelDataByID($formId);
        if($levelResponse->code != 1){
            return $levelResponse;
        }

        $form = $levelResponse->data;

        $formData = array();
        if($formDataId != 0){
            $formData = $this->getFormData($formId,$formDataId);
        }
        
        $response["form_id"] = $formId;
        $response["header"] = stripslashes($form->md_head);
        $response["description"] = stripslashes($form->md_info);
        $response["icon"] = $form->md_icon;
        $response["visible"] = $form->md_visible_element;
        $response["location"] = $form->md_display_location;
        
        $fieldsData = $this->loadExtendedFormFieldsFromDB($formId);

        if(count($fieldsData)>0){
            foreach ($fieldsData as $field)
            {
                $objectField = formFieldObject::withData($field);

                if($formDataId != 0){
                    $fieldColumn = $objectField->fld_type."_".$objectField->fld_id;
                    $objectField->fld_value = $formData[$fieldColumn];
                }
                array_push($fieldsArray,$objectField);
            }            
        }
        $response["fields"] = $fieldsArray;
        return $response;
    }

    public function saveUserFormData($POST){
        
        $formID = $POST["formid"];
        $formData = $this->getFormFields($formID,0);

        if(count($formData["fields"])>0){

            $sql = "insert into form_$formID set id=NULL";

            foreach ($formData["fields"] as $field)
            {
                if(isset($POST[$field->fld_id])){
                    
                    $value = "'".addslashes($POST[$field->fld_id])."'";
                    if($field->fld_type == "DATE" && $POST[$field->fld_id] == ""){
                        $value = "NULL";
                    }                    
                    
                    $sql .= ",{$field->fld_type}_{$field->fld_id} = $value";
                }
            }  
 
            $formFieldId = $this->dbForms->execute($sql);

            $customerDocumentObject = new customerDocumentObject();
            $customerDocumentObject->do_biz_id = $this->mobileRequestObject->bizid;
            $customerDocumentObject->do_cust_id = $this->mobileRequestObject->server_customer_id;
            $customerDocumentObject->do_title = $formData["header"];
            $customerDocumentObject->do_external_id = $formID;
            $customerDocumentObject->do_form_id = $formFieldId;
            $customerDocumentObject->do_uploaded_by = 'user';
            $customerDocumentObject->do_send_date = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
            $customerDocumentObject->do_type = 'form';

            $customerModel = new customerModel();
            $addResult = $customerModel->addCustomerDocument($customerDocumentObject);
            
            $notifications = new notificationsManager();
            $customerResponse = $customerModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResponse->code == 1){
                $params["cust_first_name"] = $customerResponse->data->cust_first_name;
                $params["customer_name"] = $customerResponse->data->cust_first_name;
                $params["customer_id"] = $this->mobileRequestObject->server_customer_id;
                $params["doc_title"] = $customerDocumentObject->do_title;
                $params["doc_id"] = $addResult->data;
                $notifications->addNotification($this->mobileRequestObject->bizid,enumActions::newCustomForm,false,$params); 
                return resultObject::withData(1,"ok");
            }else{
                return $customerResponse;
            }
        }
        return resultObject::withData(1,"ok");
    }

    /************************************* */
    /*          STATIC FUNCTIONS           */
    /************************************* */

    public static function getAmountOfFormFields_DB($formFieldID){

        $instance = new self();
        if (!is_numeric($formFieldID) || $formFieldID <= 0){
            throw new Exception("Illegal value formFieldID");             
        }

        return $instance->db->getVal("SELECT COUNT(fld_id) FROM tbl_form_fields WHERE fld_form_id = $formFieldID");
    }

    public static function getFormComboBoxValues_DB($formFieldID){

        $instance = new self();
        if (!is_numeric($formFieldID) || $formFieldID <= 0){
            throw new Exception("Illegal value formFieldID");             
        }

        return $instance->db->getTable("SELECT * FROM tbl_form_cb_values
                                         WHERE fcv_field_id=$formFieldID
                                         AND vcv_visible=1
                                         ORDER BY fcv_id ASC");
    }

    /************************************* */
    /*   BASIC FORMFIELD - DB METHODS           */
    /************************************* */

    private function addFormFieldDB(formFieldObject $obj){

        if (!isset($obj)){
            throw new Exception("formFieldObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_form_fields SET 
                                    fld_form_id = {$obj->fld_form_id},
                                    fld_index = {$obj->fld_index},
                                    fld_type_id = {$obj->fld_type_id},
                                    fld_label = '".addslashes($obj->fld_label)."',
                                    fld_visible = {$obj->fld_visible},
                                    fld_required = {$obj->fld_required}
                                             ");
        return $newId;
    }

    private function loadFormFieldFromDB($formFieldID){

        if (!is_numeric($formFieldID) || $formFieldID <= 0){
            throw new Exception("Illegal value formFieldID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_form_fields WHERE fld_id = $formFieldID");
    }

    private function upateFormFieldDB(formFieldObject $obj){

        if (!isset($obj->fld_id) || !is_numeric($obj->fld_id) || $obj->fld_id <= 0){
            throw new Exception("formFieldObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_form_fields SET 
                            fld_form_id = {$obj->fld_form_id},
                            fld_index = {$obj->fld_index},
                            fld_type_id = {$obj->fld_type_id},
                            fld_label = '".addslashes($obj->fld_label)."',
                            fld_visible = {$obj->fld_visible},
                            fld_required = {$obj->fld_required}
                            WHERE fld_id = {$obj->fld_id} 
                                     ");
    }

    private function deleteFormFieldByIdDB($formFieldID){

        if (!is_numeric($formFieldID) || $formFieldID <= 0){
            throw new Exception("Illegal value formFieldID");             
        }

        $this->db->execute("DELETE FROM tbl_form_fields WHERE fld_id = $formFieldID");
    }

    private function getFormData($formId,$formDataId){

        return $this->dbForms->getRow("SELECT * FROM form_$formId WHERE id=$formDataId");
    }

    private function loadExtendedFormFieldsFromDB($formId){
        
        $sql = "SELECT * FROM tbl_form_fields,tbl_form_fields_type
                             WHERE fld_type_id=fft_id
                             AND fld_form_id=$formId
                             AND fld_visible=1
                             ORDER BY fld_index";
        
        return $this->db->getTable($sql);
    }

}
