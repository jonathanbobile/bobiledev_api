<?php

/**
 * customerManager short summary.
 *
 * customerManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class customerManager extends Manager
{
    function __construct()
    {
        parent::__construct(); 
    }

    public static function updateCustomerDailyActivity($custID){
        $encodedCustID = utilityManager::encodeToHASH($custID);

        utilityManager::asyncPostCurl(AUTOJOBS_SERVER . "/admin/autojobs/updateDailyActivityForCustomer/".$encodedCustID,array());
    }

    /************************************* */
    /*   CUSTOMERBENEFITS PUBLIC           */
    /************************************* */

    public static function getCustomerBenefit($benefitID){
        try{
            $instance= new self();
            $benefitData = $instance->getCustomerBenefitRow($benefitID);
            $benefit = customerBenefitObject::withData($benefitData);
            return resultObject::withData(1,'',$benefit);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$benefitID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Add benefit to customer
     * @param customerBenefitObject $custBenefitObj 
     * @return resultObject
     */
    public static function addCustomerBenefit(customerBenefitObject $custBenefitObj){
        $instance= new self();
        $benefitID = $instance->addCustomerBenefitRow($custBenefitObj);

        if($benefitID > 0){
            return resultObject::withData(1,"ok",$benefitID);
        }
        else{
            return resultObject::withData(0,"error_insert_benefit");
        }
    }

    public static function updateCustomerBenefit(customerBenefitObject $custBenefitObj){
        
        try{
            $instance= new self();
            $instance->updateCustomerBenefitRow($custBenefitObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custBenefitObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function deleteCustomerBenefitByBenefitID($benefitID){
        try{
            $instance= new self();
            $instance->deleteCustomerBenefitRowByBenefitID($benefitID);
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$benefitID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getBenefitByCode($code){
        try{
            $instance= new self();
            $benefitRow = $instance->getBenefitByCode_DB($code);

            if(isset($benefitRow['cb_id']) && $benefitRow['cb_id'] > 0){
                $benefit = customerBenefitObject::withData($benefitRow);

                return resultObject::withData(1,'',$benefit);
            }
            else{
                return resultObject::withData(0,'no_benefit');
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$code);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getCouponDataFromBenefitByBenefitID($benefitID){
        try{
            $instance= new self();

            $benefitRow = $instance->getBenefitRowByID($benefitID);
            $itemResult = customerOrderManager::getCustomerOrderItemByID($benefitRow['cb_external_id']);
            if($itemResult->code != 1){
                return resultObject::withData(0,'no_item');
            }

            $item = $itemResult->data;

            $levelManager = new levelDataManager(26);

            return $levelManager->getLevelDataByID($item->tri_itm_row_id);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$code);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function deleteCustomerBenefitByItemId($itemID){
        try{
            $instance= new self();
            $instance->deleteCustomerBenefitByItemId_DB($itemID);
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$itemID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   CUSTOMERBENEFITS PRIVATE          */
    /************************************* */

    private function getCustomerBenefitRow($benefitID){
        $sql = "SELECT * FROM tbl_cust_benefits
                WHERE cb_id = $benefitID";

        return $this->db->getRow($sql);
    }

    private function addCustomerBenefitRow(customerBenefitObject $obj){
        $cb_valid_start = isset($obj->cb_valid_start) && $obj->cb_valid_start != "" ? "'".$obj->cb_valid_start."'" : "NOW()";

        $cb_valid_endDate = isset($obj->cb_valid_end) ? "'".$obj->cb_valid_end."'" : "null";

        $cb_redeemed_dateDate = isset($obj->cb_redeemed_date) ? "'".$obj->cb_redeemed_date."'" : "null";

        $sql = "INSERT INTO tbl_cust_benefits SET
                    cb_biz_id = {$obj->cb_biz_id},
                    cb_cust_id = {$obj->cb_cust_id},
                    cb_type = '{$obj->cb_type}',
                    cb_source_row_id = {$obj->cb_source_row_id},
                    cb_external_id = {$obj->cb_external_id},
                    cb_title = '".addslashes($obj->cb_title)."',
                    cb_description = '".addslashes($obj->cb_description)."',
                    cb_image = '".addslashes($obj->cb_image)."',
                    cb_valid_end = $cb_valid_endDate,
                    cb_redeemed = {$obj->cb_redeemed},
                    cb_redeemed_date = $cb_redeemed_dateDate,
                    cb_code = '{$obj->cb_code}',
                    cb_item_type = '{$obj->cb_item_type}',
                    cb_scard_id = {$obj->cb_scard_id},
                    cb_value = {$obj->cb_value},
                    cb_granted_by = '{$obj->cb_granted_by}',
                    cb_granted_by_id = {$obj->cb_granted_by_id}";

        return $this->db->execute($sql);
    }

    private function updateCustomerBenefitRow(customerBenefitObject $obj){
        if (!isset($obj->cb_id) || !is_numeric($obj->cb_id) || $obj->cb_id <= 0){
            throw new Exception("customerBenefitObject value must be provided");             
        }

        $cb_dateDate = isset($obj->cb_date) ? "'".$obj->cb_date."'" : "null";

        $cb_valid_startDate = isset($obj->cb_valid_start) ? "'".$obj->cb_valid_start."'" : "null";

        $cb_valid_endDate = isset($obj->cb_valid_end) ? "'".$obj->cb_valid_end."'" : "null";

        $cb_redeemed_dateDate = isset($obj->cb_redeemed_date) ? "'".$obj->cb_redeemed_date."'" : "null";

        $this->db->execute("UPDATE tbl_cust_benefits SET 
                                cb_biz_id = {$obj->cb_biz_id},
                                cb_cust_id = {$obj->cb_cust_id},
                                cb_type = '{$obj->cb_type}',
                                cb_source_row_id = {$obj->cb_source_row_id},
                                cb_external_id = {$obj->cb_external_id},
                                cb_date = $cb_dateDate,
                                cb_title = '".addslashes($obj->cb_title)."',
                                cb_description = '".addslashes($obj->cb_description)."',
                                cb_image = '".addslashes($obj->cb_image)."',
                                cb_valid_start = $cb_valid_startDate,
                                cb_valid_end = $cb_valid_endDate,
                                cb_redeemed = {$obj->cb_redeemed},
                                cb_redeemed_date = $cb_redeemed_dateDate,
                                cb_code = '{$obj->cb_code}',
                                cb_item_type = '{$obj->cb_item_type}',
                                cb_scard_id = {$obj->cb_scard_id},
                                cb_value = {$obj->cb_value},
                                cb_granted_by = '{$obj->cb_granted_by}',
                                cb_granted_by_id = {$obj->cb_granted_by_id}
                                WHERE cb_id = {$obj->cb_id} 
         ");

        return;
    }
    
    private function deleteCustomerBenefitRowByBenefitID($benefitID){
        $sql = "DELETE FROM tbl_cust_benefits WHERE cb_id = $benefitID";

        $this->db->execute($sql);
    }

    private function deleteCustomerBenefitByItemId_DB($itemID){

        $sql = "DELETE FROM tbl_cust_benefits WHERE cb_external_id = $itemID";

        $this->db->execute($sql);
    }

    /************************************* */
    /*   CUSTOMER PUBLIC                   */
    /************************************* */

    public static function getCustomerBizID($cust_id){
        try{
            $instance = new self();

            return $instance->getCustomerBizID_DB($cust_id);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            return 0;
        }
    }

    public static function redeemBenefitByID($benefitID){
        try{
            $instance = new self();

            $code = $instance->redeemBenefitByID_execute($benefitID);

            if($code == 1){
                return resultObject::withData(1);
            }
            else{
                return resultObject::withData(0,'',$code);
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$benefitID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getBenefitLabel($benefitID){
        try{
            $instance = new self();
            $benefit = $instance->getBenefitRowByID($benefitID);

            return resultObject::withData(1,'',$instance->getBenefitLabel_DB($benefit));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$benefitID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function redeemBenefitWithCode($code){
        try{
            $instance = new self();            

            $benefitRow = $instance->getBenefitByCode_DB($code);
            
            if(isset($benefitRow['cb_id']) && $benefitRow['cb_id'] > 0){
                $code = $instance->redeemBenefitByID_execute($benefitRow['cb_id']);

                if($code == 1){
                    return resultObject::withData(1,'',$instance->getBenefitLabel_DB($benefitRow));
                }
                else{
                    return resultObject::withData(0,'',$code);
                }
            }
            else{
                return resultObject::withData(0,'no_benefit');
            }
            
            
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$code);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function addRewardToCustomer($custID,$type,$code,$external_id = 0,$source_id = 0){
        try{
            $instance = new self();

            $benefitID = $instance->addBenefitRowToCustomer($custID,$type,$code,$external_id,$source_id);
            self::sendAsyncCustomerDataUpdateToDevice('benefits',$custID);
            return resultObject::withData(1,'',$benefitID);
        }
        catch(Exception $e){
            $data = array();
            $data['custID'] = $custID;
            $data['type'] = $type;
            $data['code'] = $code;
            $data['external_id'] = $external_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }


    }
    
    public static function addOneUnreadChatForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unread_chats = cust_unread_chats+1 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function subtractOneUnreadChatForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unread_chats = cust_unread_chats-1 WHERE cust_id = $custId AND cust_unread_chats > 0";
        $instance->db->execute($sql);
    }

    public static function zeroUnreadChatsForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unread_chats = 0 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function addOnePaymentRequestPaidForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_paymentreq_paid = cust_paymentreq_paid+1 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function zeroPaymentRequestPaidForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_paymentreq_paid = 0 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function addOneUnattendedOrderForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unattended_orders = cust_unattended_orders+1 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function subtractUnattendedOrderForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unattended_orders = cust_unattended_orders-1 WHERE cust_id = $custId AND cust_unattended_orders > 0";
        $instance->db->execute($sql);
    }

    public static function zeroUnattendedOrdersForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unattended_orders = 0 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function addOneUnseenFileForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unseen_files = cust_unseen_files+1 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function subtractOneUnseenFileForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unseen_files = cust_unseen_files-1 WHERE cust_id = $custId  AND cust_unseen_files > 0";
        $instance->db->execute($sql);
    }

    public static function zeroUnseenFilesForCust ($custId = 0){
        if ($custId == 0) return;
        $instance = new self();
        $sql = "UPDATE tbl_customers SET cust_unseen_files = 0 WHERE cust_id = $custId";
        $instance->db->execute($sql);
    }

    public static function addBadgeToCustomer($cust_id,$type){
        try{
            $instance = new self();
            
            $badgeID = $instance->addBadgeToCustomer_DB($cust_id,$type);

            self::sendAsyncCustomerDataUpdateToDevice('badges',$cust_id);

            return resultObject::withData(1,'',$badgeID);
        }
        catch(Exception $e){
            $data = array();
            $data['custID'] = $cust_id;
            $data['type'] = $type;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function grantInviterPoints($cust_id,$amount){
        $url = "http://127.0.0.1/".MVC_NAME.'/eventAsync/grantInviterPoints';        
        $params = array(
                'friend_id' => $cust_id,
                'amount' => $amount
            );
        utilityManager::asyncTask($url,$params);
    }    

    public static function getCustomerSocketIdentifierByID($customer_id){
        try{
            $instance = new self();

            return $instance->getCustomerSocketIdentifier_DB($customer_id);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customer_id);
            return "";
        }
    }

    public static function getCustomersPunchCardbyCardID($custId,$programId,$stampNumber){
        
        $instance = new self();

        try{
            $stamps = array();
            $entry = $instance->getCustomersPunchCardbyCardID_DB($custId,$programId,$stampNumber);
            
            if(isset($entry["customer_card_id"])){
                $stamps = customerLoyaltyCardObject::withData($entry);
            }else{
                $stamps = new customerLoyaltyCardObject();
            }

            $result = resultObject::withData(1,'',$stamps);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function subtractOneBadgeTypeForCustomer($cust_id,$type){
        try{
            $instance = new self();
            $sql = "DELETE FROM tbl_customer_badge_tracker
                        WHERE cbt_cust_id = $cust_id
                        AND cbt_type = '$type'
                        ORDER BY cbt_date ASC
                        LIMIT 1";

            $instance->db->execute($sql);

            $result = resultObject::withData(1,'');
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function zeroBadgeTypeForCustomer($cust_id,$type){
        try{
            $instance = new self();
            $sql = "DELETE FROM tbl_customer_badge_tracker
                    WHERE cbt_cust_id = $cust_id
                    AND cbt_type = '$type'";

            $instance->db->execute($sql);

            $result = resultObject::withData(1,'');
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function sendFile($POST,mobileRequestObject $mobileRequestObject){

        $customerModel = new customerModel();
        $customerRes = $customerModel->getCustomerWithID($mobileRequestObject->server_customer_id);
        if($customerRes->code != 1){
            return resultObject::withData(0,"no_customer");
        }

        $custName = $customerRes->data->cust_first_name;
        moduleDataManager::addDocumentsModuleIfMissing($mobileRequestObject->bizid);

        $responseUpload = self::uploadDocument($POST,$mobileRequestObject);

        if($responseUpload->code != 1){
            return $responseUpload;
        }

        $customerModel = new customerModel();
        $responseDocument = $customerModel->getCustomerDocumentByID($responseUpload->data);

        if($responseDocument->code != 1){
            return $responseDocument;
        }

        $docRow = $responseDocument->data;

        $notifications = new notificationsManager();
        $params["cust_first_name"] = $custName;
        $params["doc_url"] = $docRow->do_url;
        $params["customer_id"] = $mobileRequestObject->server_customer_id;
        $notifications->addNotification($mobileRequestObject->bizid,enumActions::newDocument,false,$params); 

        self::addOneUnseenFileForCust($mobileRequestObject->server_customer_id);

        eventManager::actionTrigger(enumCustomerActions::sentFile,$mobileRequestObject->server_customer_id,"document",'',$mobileRequestObject->deviceID);

        self::sendAsyncCustomerDataUpdateToDevice('documents',$mobileRequestObject->server_customer_id);

        if($docRow->do_id != "0"){
            $doc['recordID']=$docRow->do_id;
            $doc['title']=$docRow->do_title;
            $doc['url']=$docRow->do_url;
            $doc['add_date']=$docRow->do_create_date;
            $doc['send_date']=($docRow->do_send_date == null) ? "" :$docRow->do_send_date;
            $doc['receive_date']=($docRow->do_receive_date == null) ? "" :$docRow->do_receive_date;
            $doc['add_date_timestamp']=strtotime($docRow->do_create_date);
            $doc['send_date_timestamp']=($docRow->do_send_date == null) ? "" :strtotime($docRow->do_send_date);
            $doc['receive_date_timestamp']=($docRow->do_receive_date == null) ? "" :strtotime($docRow->do_receive_date);
            $doc['uploaded_by']=$docRow->do_uploaded_by;
            $doc['type']=$docRow->do_type;
            $doc['external_id']=$docRow->do_external_id;
            $doc['form_id']=$docRow->do_form_id;
            $doc['md_next_view'] = "";

            return resultObject::withData(1,"ok",$doc);
        }else{
            return resultObject::withData(0,"no_document");
        }
    }

    public static function uploadDocument($POST,mobileRequestObject $mobileRequestObject){
        
        $bizId = $mobileRequestObject->bizid;
        $fileName = urldecode($POST["fileName"]);
        $fileData = $POST["fileData"];
        $title = urldecode($POST["title"]);
        $custId = $mobileRequestObject->server_customer_id;
        $uploadedBy = "user";

        $fileExtention = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
        $newFileName = str_replace(" ","_",pathinfo($fileName, PATHINFO_FILENAME))."_".str_replace(".","",microtime(true)).".".pathinfo($fileName, PATHINFO_EXTENSION);
        $fileType = "";
        switch ($fileExtention){
            case "xlsx":
            case "xls":
            case "csv":
                $fileType="exl";
                break;
            case "doc":
            case "docx":
                $fileType="doc";
                break;
            case "pdf":
                $fileType="pdf";
                break;
            case "gif":
            case "png":
            case "jpg":
            case "jpeg":
                $fileType="img";
                break;
        }
        
        $binaryData=base64_decode($fileData);
        $image_storage = imageStorage::getStorageAdapter();
        $urlFile = $image_storage->uploadDocument($binaryData, $bizId.'/docs/'.$newFileName,$fileExtention); 
        
        $customerModel = new customerModel();
        $customerDocumentObject = new customerDocumentObject();
        $customerDocumentObject->do_biz_id = $bizId;
        $customerDocumentObject->do_cust_id = $custId;
        $customerDocumentObject->do_title = $title;
        $customerDocumentObject->do_uploaded_by = $uploadedBy;
        $customerDocumentObject->do_type = $fileType;
        $customerDocumentObject->do_url = $urlFile;
        $customerDocumentObject->do_send_date = utilityManager::getSQLDateFormatTimeCurrentTimestamp();

        return $customerModel->addCustomerDocument($customerDocumentObject);
    }

    public static function updateCustomerLocationByCoordinates(mobileRequestObject $mobileRequestObject){

        if(isset($mobileRequestObject->long) && $mobileRequestObject->long != ""){
            $url = "http://127.0.0.1/".MVC_NAME.'/eventAsync/updateCustomerAddressByLocation';        
            $params = array(
                    'custId' => $mobileRequestObject->server_customer_id,
                    'long' => $mobileRequestObject->long,
                    'lati' => $mobileRequestObject->lati
                );
            utilityManager::asyncTask($url,$params);
        }

    }

    public static function updateCustomerLocationByIP($custID,$ip){
        $url = "http://127.0.0.1/".MVC_NAME.'/eventAsync/updateCustomerAddressByIP';        
        $params = array(
                'custID' => $custID,
                'ip' => $ip
            );
        utilityManager::asyncTask($url,$params);
    }

    public static function setDeviceIP($deviceID,$bizID,$ip){
        $instance = new self();

        $sql = "INSERT INTO tbl_device_ip SET
                    device_id = '$deviceID',
                    device_biz_id = $bizID,
                    device_ip = '$ip'
                ON DUPLICATE KEY UPDATE
                    device_ip = '$ip',
                    device_update = NOW()";

        $instance->db->execute($sql);
    }

    public static function getDeviceIP($deviceID,$bizID){
        $instance = new self();

        $sql = "SELECT device_ip FROM tbl_device_ip 
                    WHERE device_id = '$deviceID'
                    AND device_biz_id = $bizID";

        return $instance->db->getVal($sql);
    }

    public static function getCustomerstripeID($custID){
        $instance = new self();

        return $instance->getCustomerstripeIDDB($custID);
    }

    public static function isOtherCustomerExistByStatusAndPhone($bizId,$customerId,$status,$phone){
        
        $instance = new self();
        $sql = "SELECT COUNT(cust_id) FROM tbl_customers 
                    WHERE cust_biz_id=$bizId
                    AND cust_id <> $customerId
                    AND cust_status='$status'
                    AND cust_phone1 = '$phone'";
        return $instance->db->getVal($sql) > 0;
    }

    public static function isOtherCustomerExistByStatusAndEmail($bizId,$customerId,$status,$email){
        
        $instance = new self();
        $sql = "SELECT COUNT(cust_id) FROM tbl_customers 
                    WHERE cust_biz_id=$bizId
                    AND cust_id <> $customerId
                    AND cust_status='$status'
                    AND cust_email = '$email'";
        return $instance->db->getVal($sql) > 0;
    }

    public static function sendAsyncCustomerDataUpdateToDevice($type,$custID,$deviceID = '',$device_type = ''){
        $url = "http://127.0.0.1/".MVC_NAME."/eventAsync/sendAsyncPersonalZoneData";
        $data = array();
        $data['type'] = $type;
        $data['cust_id'] = $custID;
        $data['device_id'] = $deviceID;
        $data['device_type'] = $device_type;
        utilityManager::asyncTask($url,$data,false);
    }   

    public static function getAllCustomerDeviceIDs($custID)
    {
        $instance = new self();
        $deviceIds = array();

        //Main device
        $mainDevice = $instance->db->getRow("SELECT cust_mobile_serial,cust_device FROM tbl_customers WHERE cust_id = $custID");

        $deviceIds[] = array(
                'device_id' => $mainDevice['cust_mobile_serial'],
                'is_main' => 1,
                'type' => $mainDevice['cust_device']
            );

        //Multi devices
        $multiDeviceIds = $instance->db->getTable("SELECT * FROM tbl_cust_device WHERE cd_cust_id = $custID");

        foreach ($multiDeviceIds as $value)
        {            
            if(!utilityManager::contains($value['cd_device'],'web_')){
                $deviceIds[] = array(
                    'device_id' => $value['cd_device'],
                    'is_main' => 0,
                    'type' => $value['cd_device_type']
                );
            }


        } 

        //Web member page
        $deviceIds[] = array(
                'device_id' => utilityManager::encodeToHASH($custID),
                'is_main' => 0,
                'type' => 'web'
            );
        
        return $deviceIds;
    }

    public static function getRandomAvatar(){
        $randIndex = rand(1,28);
        
        return "https://storage.googleapis.com/bbassets/avatars/avatar_$randIndex.png";
    }

    public static function isFraudulentCustomerID($customerID){
        
        $instance = new self();

        $custIsFraud = $instance->db->getVal("SELECT cust_is_fraud FROM tbl_customers WHERE cust_id = $customerID");

        return isset($custIsFraud) && $custIsFraud == '1';
    }  
    
    public static function setFraudulentCustomerID($customerID){

        $instance = new self();

        $instance->db->execute("UPDATE tbl_customers SET cust_is_fraud = 1 WHERE cust_id = $customerID");
    }

    public static function isDeviceIOS($deviceID){
        $instance = new self();

        return $instance->db->getVal("SELECT COUNT(pid) FROM apns_devices WHERE deviceuid = '$deviceID'") > 0;
    }

    public static function isCustomerDeleted($custID){
        $instance = new self();
        return $instance->db->getVal("SELECT cust_deleted FROM tbl_customers WHERE cust_id = $custID") == 1;
    }

    /************************************* */
    /*   CUSTOMER PRIVATE                  */
    /************************************* */

    private function getCustomerBizID_DB($cust_id){
        $sql = "SELECT cust_biz_id FROM tbl_customers WHERE cust_id = $cust_id";

        return $this->db->getVal($sql);
    }

    private function addBenefitRowToCustomer($custID,$type,$code,$external_id = 0,$source_id = 0){
        $bizID = $this->getCustomerBizID_DB($custID);
        $sql = "INSERT INTO tbl_cust_benefits SET
                        cb_biz_id = $bizID,
                        cb_cust_id = $custID,
                        cb_type = '$type',
                        cb_source_row_id = $source_id,
                        cb_external_id = $external_id,
                        cb_code = '$code'";

        return $this->db->execute($sql);
    }

    private function redeemBenefitByID_execute($benefitID){
        $benefit = $this->getBenefitRowByID($benefitID);

        $code = 0;
        if(!isset($benefit['cb_id'])){
            $code = -4;
        }

        if($benefit['cb_redeemed'] == 1){                
            $code = -1;
        }

        $now = time();
        $endValid = strtotime($benefit['cb_valid_end']);
        $startValid = strtotime($benefit['cb_valid_start']);

        if(isset($benefit['cb_valid_start']) && $startValid > $now){
            $code = -3;
            
            
        }

        if(isset($benefit['cb_valid_end']) && $now > $endValid){
            $code = -2;
        }

        if($code == 0){
            $this->setBenefitRedeemed($benefitID);

            if($benefit['cb_external_id'] != 0){
                if($benefit['cb_type'] == 'loyalty'){
                    $this->setLoyaltyCardRedeemed($benefit['cb_external_id']);               
                }
                if($benefit['cb_type'] == 'coupon'){
                    if($benefit['cb_external_id'] != 0){
                        $this->setCouponTransactionItemRedeemed($benefit['cb_external_id']); 
                    }
                    eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$benefit['cb_cust_id'],'coupon_redeemed',$this->getBenefitLabel($benefit['cb_external_id'])->data,'',$benefit['cb_external_id']);
                }
            }
            $code = 1;
        }

        return $code;
    }

    private function getBenefitRowByID($benefitID){
        $sql = "SELECT * FROM tbl_cust_benefits WHERE cb_id = $benefitID";

        return $this->db->getRow($sql);
    }

    private function setBenefitRedeemed($benefitID){
        $sql = "UPDATE tbl_cust_benefits SET cb_redeemed = 1,cb_redeemed_date = now() WHERE cb_id = $benefitID";
        $this->db->execute($sql);
    }

    private function setLoyaltyCardRedeemed($cc_id){
        $this->db->execute("UPDATE tbl_customer_card SET customer_card_redeemed = 1,customer_card_redeemed_date=now() WHERE customer_card_id = $cc_id");        
    }

    private function setCouponTransactionItemRedeemed($tri_id){
        $this->db->execute("update tbl_cust_transaction_items set tri_redeem_date=now() where tri_id=$tri_id");
    }

    private function getBenefitByCode_DB($code){
        $sql = "SELECT * FROM tbl_cust_benefits
                        WHERE cb_code='$code'
                        AND cb_code <> '0'";

        return $this->db->getRow($sql);
    }

    private function getBenefitLabel_DB($benefitRow){
        if($benefitRow['cb_title'] != ''){
            return $benefitRow['cb_title'];
        }
        else{
            $sql = "";
            if($benefitRow['cb_type'] == 'loyalty'){
                $sql = "SELECT md_head FROM tbl_customer_card,tbl_mod_data6
                                    WHERE md_row_id = customer_card_program_id
                                    AND customer_card_id = {$benefitRow['cb_external_id']}";               
            }

            if($benefitRow['cb_type'] == 'coupon'){
                $sql = "select md_head from tbl_cust_transaction_items,tbl_mod_data26
                                    where tri_itm_row_id=md_row_id
                                    and tri_id= {$benefitRow['cb_external_id']}";

                
            }

            return $this->db->getVal($sql);
        }
    }

    private function addBadgeToCustomer_DB($cust_id,$type){
        $sql = "INSERT INTO tbl_customer_badge_tracker SET
                    cbt_cust_id = $cust_id,
                    cbt_type = '$type'";

        return $this->db->execute($sql);
    }

    private function getCustomerSocketIdentifier_DB($custID){
        $sql = "SELECT CONCAT(CONCAT(cust_id,'_'),cust_mobile_serial) as socket_id FROM tbl_customers WHERE cust_id = $custID";

        return $this->db->getVal($sql);
    }

    private function getCustomersPunchCardbyCardID_DB($custId,$programId,$stampNumber){

        if(!is_numeric($custId) || $custId <= 0){
            throw new Exception("Ilegal customer ID");
        }

        $sql = "select * from tbl_customer_card 
                    where customer_card_cust_id=$custId
                    and customer_card_program_id=$programId
                    and customer_card_stamp_no <> $stampNumber";

        return $this->db->getRow($sql);
    }

    private function getCustomerstripeIDDB($custID){
        return $this->db->getVal("SELECT cust_stripe_cust_id FROM tbl_customers WHERE cust_id = $custID");
    }

    /************************************* */
    /*   BASIC USER - PUBLIC           */
    /************************************* */

    /**
     * Insert new userObject to DB
     * Return Data = new userObject ID
     * @param userObject $userObj 
     * @return resultObject
     */
    public function addUser(userObject $userObj){       
        try{
            $newId = $this->addUserDB($userObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($userObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get user from DB for provided ID
     * * Return Data = userObject
     * @param int $userId 
     * @return resultObject
     */
    public function getUserByID($userId){
        
        try {
            $userData = $this->loadUserFromDB($userId);
            
            $userObj = userObject::withData($userData);
            $result = resultObject::withData(1,'',$userObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$userId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param userObject $userObj 
     * @return resultObject
     */
    public function updateUser(userObject $userObj){        
        try{
            $this->upateUserDB($userObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($userObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete user from DB
     * @param int $userID 
     * @return resultObject
     */
    public function deleteUserById($userID){
        try{
            $this->deleteUserByIdDB($userID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$userID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getUserByDeviceAppMarket($deviceId,$appId,$marketId){
        
        $instance = new self();

        return $instance->db->getRow("SELECT * 
                                        FROM tbl_users 
                                        WHERE usr_mobile_serial='$deviceId' 
                                        AND usr_appid=$appId 
                                        AND usr_marketid=$marketId");
    }

    /************************************* */
    /*   BASIC USER - DB METHODS           */
    /************************************* */

    private function addUserDB(userObject $obj){

        if (!isset($obj)){
            throw new Exception("userObject value must be provided");             
        }

        $usr_birth_dayDate = isset($obj->usr_birth_day) ? "'".$obj->usr_birth_day."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_users SET 
                                        usr_nick_name = '".addslashes($obj->usr_nick_name)."',
                                        usr_country_id = {$obj->usr_country_id},
                                        usr_state_id = {$obj->usr_state_id},
                                        usr_default_lng = '".addslashes($obj->usr_default_lng)."',
                                        usr_e_mail = '".addslashes($obj->usr_e_mail)."',
                                        usr_image = '".addslashes($obj->usr_image)."',
                                        usr_birth_day = $usr_birth_dayDate,
                                        usr_gender = {$obj->usr_gender},
                                        usr_mobile_num = '".addslashes($obj->usr_mobile_num)."',
                                        usr_mobile_serial = '".addslashes($obj->usr_mobile_serial)."',
                                        usr_appid = {$obj->usr_appid},
                                        usr_resellerid = {$obj->usr_resellerid},
                                        usr_marketid = {$obj->usr_marketid},
                                        usr_os = '".addslashes($obj->usr_os)."'
                                     ");
        return $newId;
    }

    private function loadUserFromDB($userID){

        if (!is_numeric($userID) || $userID <= 0){
            throw new Exception("Illegal value $userID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_users WHERE usr_id = $userID");
    }

    private function upateUserDB(userObject $obj){

        if (!isset($obj->usr_id) || !is_numeric($obj->usr_id) || $obj->usr_id <= 0){
            throw new Exception("userObject value must be provided");             
        }

        $usr_birth_dayDate = isset($obj->usr_birth_day) ? "'".$obj->usr_birth_day."'" : "null";

        $usr_start_dateDate = isset($obj->usr_start_date) ? "'".$obj->usr_start_date."'" : "null";

        $this->db->execute("UPDATE tbl_users SET 
                                usr_nick_name = '".addslashes($obj->usr_nick_name)."',
                                usr_country_id = {$obj->usr_country_id},
                                usr_state_id = {$obj->usr_state_id},
                                usr_default_lng = '".addslashes($obj->usr_default_lng)."',
                                usr_e_mail = '".addslashes($obj->usr_e_mail)."',
                                usr_image = '".addslashes($obj->usr_image)."',
                                usr_birth_day = $usr_birth_dayDate,
                                usr_gender = {$obj->usr_gender},
                                usr_mobile_num = '".addslashes($obj->usr_mobile_num)."',
                                usr_mobile_serial = '".addslashes($obj->usr_mobile_serial)."',
                                usr_start_date = $usr_start_dateDate,
                                usr_appid = {$obj->usr_appid},
                                usr_resellerid = {$obj->usr_resellerid},
                                usr_marketid = {$obj->usr_marketid},
                                usr_os = '".addslashes($obj->usr_os)."'
                                WHERE usr_id = {$obj->usr_id} 
                             ");
    }

    private function deleteUserByIdDB($userID){

        if (!is_numeric($userID) || $userID <= 0){
            throw new Exception("Illegal value $userID");             
        }

        $this->db->execute("DELETE FROM tbl_users WHERE usr_id = $userID");
    }

    /************************************* */
    /*   BASIC WISHLIST - PUBLIC           */
    /************************************* */

    /**
     * Insert new wishListObject to DB
     * Return Data = new wishListObject ID
     * @param wishListObject $wishListObj 
     * @return resultObject
     */
    public function addWishList(wishListObject $wishListObj){       
        try{
            $newId = $this->addWishListDB($wishListObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($wishListObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get wishList from DB for provided ID
     * * Return Data = wishListObject
     * @param int $wishListId 
     * @return resultObject
     */
    public function getWishListByID($wishListId){
        
        try {
            $wishListData = $this->loadWishListFromDB($wishListId);
            
            $wishListObj = wishListObject::withData($wishListData);
            $result = resultObject::withData(1,'',$wishListObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$wishListId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param wishListObject $wishListObj 
     * @return resultObject
     */
    public function updateWishList(wishListObject $wishListObj){        
        try{
            $this->upateWishListDB($wishListObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($wishListObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete wishList from DB
     * @param int $wishListID 
     * @return resultObject
     */
    public function deleteWishListById($wishListID){
        try{
            $this->deleteWishListByIdDB($wishListID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$wishListID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getWishListForElement($bizID,$customerId,$modId,$rowId){

        $instance = new self();

        return $instance->db->getRow("SELECT * 
                                        FROM tbl_wish_list 
                                        WHERE wl_biz_id=$bizID 
                                        AND wl_customer_id=$customerId 
                                        AND wl_mod_id = $modId 
                                        AND wl_row_id = $rowId");
    }

    public static function getFullWishList(wishListObject $wishListObject){

        switch($wishListObject->wl_mod_id){
            case 3:
                $levelDataManager = new levelDataManager(3);
                $resultLevel = $levelDataManager->getLevelDataByID($wishListObject->wl_row_id);

                if($resultLevel->code != 1){
                    return $resultLevel;
                }
                
                if($resultLevel->data->md_row_id == 0 || $resultLevel->data->md_visible_element == 0){
                    resultObject::withData(1,"no_items");
                }

                $bizModel = new bizModel($wishListObject->wl_biz_id);
                $result = $bizModel->getBizModByBizIDAndModID($wishListObject->wl_biz_id,3);
                $allowReviews = 0;
                if($result->code == 1){
                    $allowReviews = $result->data->biz_mod_has_reviews;
                }
                $wishListObject->title = $resultLevel->data->md_head;
                $wishListObject->img = $resultLevel->data->md_icon;
                $wishListObject->label = $resultLevel->data->md_date;
                $wishListObject->md_review_score = $resultLevel->data->md_price;
                $wishListObject->allow_reviews = $allowReviews;
                $wishListObject->ext_type = $resultLevel->data->md_external_type;
                $wishListObject->ext_id = $resultLevel->data->md_external_id;

                break;
            case 9:

                $levelDataManager = new levelDataManager(9);
                $resultLevel = $levelDataManager->getLevelDataByID($wishListObject->wl_row_id);

                if($resultLevel->code != 1){
                    return $resultLevel;
                }
                
                if($resultLevel->data->md_row_id == 0 || $resultLevel->data->md_visible_element == 0){
                    resultObject::withData(1,"no_items");
                }

                $wishListObject->title = $resultLevel->data->md_head;
                $wishListObject->img = $resultLevel->data->md_icon;
                $wishListObject->label = $resultLevel->data->md_date;
                $wishListObject->price = $resultLevel->data->md_price;
                $wishListObject->orig_price = $resultLevel->data->md_original_price;
                $wishListObject->needs_shipping = $resultLevel->data->md_bool2;
                $wishListObject->currency = $resultLevel->data->currency;
                
                break;
            case 26:

                $levelDataManager = new levelDataManager(26);
                $resultLevel = $levelDataManager->getLevelDataByID($wishListObject->wl_row_id);

                if($resultLevel->code != 1){
                    return $resultLevel;
                }
                
                if($resultLevel->data->md_row_id == 0 || $resultLevel->data->md_visible_element == 0){
                    resultObject::withData(1,"no_items");
                }

                $price = 0;
                $cuponType="";

                if($resultLevel->data->md_info5 == "1")
                {
                    $price = $resultLevel->data->md_price;
                    $cuurency = $resultLevel->data->md_int1 == 116 ? "%" : " ".$resultLevel->data->currency;
                    $cuponType = $price.$cuurency;  
                }
                if($resultLevel->data->md_info5 == "2")
                {
                    $cuponType = $resultLevel->data->md_int2."+".$resultLevel->data->md_int3;
                }
                if($resultLevel->data->md_info5 == "3")
                {
                    $cuponType="";
                }

                $wishListObject->currency = $resultLevel->data->currency;
                $wishListObject->title = $resultLevel->data->md_head;
                $wishListObject->img = $resultLevel->data->md_icon;
                $wishListObject->label = $cuponType;
                $wishListObject->price = $price;
                

                break;
            default:

                $levelDataManager = new levelDataManager($wishListObject->wl_mod_id);
                $resultLevel = $levelDataManager->getLevelDataByID($wishListObject->wl_row_id);

                if($resultLevel->code != 1){
                    return $resultLevel;
                }
                
                if($resultLevel->data->md_row_id == 0 || $resultLevel->data->md_visible_element == 0){
                    resultObject::withData(1,"no_items");
                }

                $bizModel = new bizModel($wishListObject->wl_biz_id);
                $result = $bizModel->getBizModByBizIDAndModID($wishListObject->wl_biz_id,$wishListObject->wl_mod_id);
                $allowReviews = 0;
                if($result->code == 1){
                    $allowReviews = $result->data->biz_mod_has_reviews;
                }
                $wishListObject->title = $resultLevel->data->md_head;
                $wishListObject->img = $resultLevel->data->md_icon;
                $wishListObject->label = "";
                $wishListObject->md_review_score = $resultLevel->data->md_price;
                $wishListObject->allow_reviews = $allowReviews;
                $wishListObject->ext_type = $resultLevel->data->md_external_type;
                $wishListObject->ext_id = $resultLevel->data->md_external_id;
                $wishListObject->has_ext_action = $resultLevel->data->md_external_has_action;
                $wishListObject->ext_action_title = $resultLevel->data->md_external_action_title;

                break;
        }

        return resultObject::withData(1,"ok",$wishListObject);
    }

    /************************************* */
    /*   BASIC WISHLIST - DB METHODS           */
    /************************************* */

    private function addWishListDB(wishListObject $obj){

        if (!isset($obj)){
            throw new Exception("wishListObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT IGNORE INTO tbl_wish_list SET 
                                        wl_biz_id = {$obj->wl_biz_id},
                                        wl_customer_id = {$obj->wl_customer_id},
                                        wl_mod_id = {$obj->wl_mod_id},
                                        wl_row_id = {$obj->wl_row_id},
                                        wl_level_id = {$obj->wl_level_id}
                                    ");
        return $newId;
    }

    private function loadWishListFromDB($wishListID){

        if (!is_numeric($wishListID) || $wishListID <= 0){
            throw new Exception("Illegal value $wishListID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_wish_list WHERE wl_id = $wishListID");
    }

    private function upateWishListDB(wishListObject $obj){

        if (!isset($obj->wl_id) || !is_numeric($obj->wl_id) || $obj->wl_id <= 0){
            throw new Exception("wishListObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_wish_list SET 
                                wl_biz_id = {$obj->wl_biz_id},
                                wl_customer_id = {$obj->wl_customer_id},
                                wl_mod_id = {$obj->wl_mod_id},
                                wl_row_id = {$obj->wl_row_id},
                                wl_level_id = {$obj->wl_level_id}
                                WHERE wl_id = {$obj->wl_id} 
         ");
    }

    private function deleteWishListByIdDB($wishListID){

        if (!is_numeric($wishListID) || $wishListID <= 0){
            throw new Exception("Illegal value $wishListID");             
        }

        $this->db->execute("DELETE FROM tbl_wish_list WHERE wl_id = $wishListID");
    }

    /************************************* */
    /*   BASIC CUSTOMER LOYALTY - PUBLIC   */
    /************************************* */

    public static function getCustomerLoyaltyCardsByCustomerID($custID,$skip = 0, $take = 40){
        
        try{
            $instance = new self();
            $punchCardsList = $instance->getCustomerPunchCardEntriesbyCustID($custID,$skip,$take);
            $punchCards = array();
            foreach ($punchCardsList as $entry)
            {
                $customerPunchCard = customerLoyaltyCardObject::withData($entry);
                $loyaltyCard = levelData6Object::withData($entry);
                $customerPunchCard->setCardData($loyaltyCard);
                $punchCards[] = $customerPunchCard;
                
            }
            $result = resultObject::withData(1,'',$punchCards);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

    }

    public static function getLoyaltyCardByCardID($cardID){
        
        try{
            $instance = new self();
            $loyaltyRow = $instance->getLoyaltyCardByCardIDDB($cardID);

            $customerPunchCard = customerLoyaltyCardObject::withData($loyaltyRow);
            $loyaltyCard = levelData6Object::withData($loyaltyRow);
            $customerPunchCard->setCardData($loyaltyCard);

            return resultObject::withData(1,'',$loyaltyCard);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

    }

    public static function getStampsForLoyaltyCardByCardID($cardID){
        
        try{
            $instance = new self();
            $stamps = array();
            $stampsList = $instance->getStampEntriesForPunchCardbyCardID($cardID);
            
            foreach ($stampsList as $entry)
            {
            	$stamps[] = customerLoyaltyCardStampObject::withData($entry);
            }
            
            
            $result = resultObject::withData(1,'',$stamps);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cardID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function addLoyaltyCardToCustomer(customerLoyaltyCardObject $custLoyaltyObject){
        try{
            $instance = new self();
            $custCardID = $instance->addLoyaltyCardRowToCustomer($custLoyaltyObject);

            return resultObject::withData(1,'',$custCardID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltyObject));
            return resultObject::withData(0,$e->getMessage());
        }
    }


    public static function addCustomerLoyaltyStampToCustomerFromProductPurchase(customerObject $customer,$itemID,$amount = 1,$orderID = 0){
        try{
            $instance = new self();
            $loyaltyRows = $instance->getAutoStampedLoyaltyCardsConnectedToItemForCustomer($customer,$itemID,'product');

            if(count($loyaltyRows) == 0){
                return resultObject::withData(0,'no_loyalties');
            }

            $custLoyaltyObject = null;
            for ($i = 0; $i < count($loyaltyRows); $i++)
            {
            	$loyaltyRow = $loyaltyRows[$i];
                if(isset($loyaltyRow['customer_card_id'])){//customer has this loyalty card available for stamping
                    $custLoyaltyObject = customerLoyaltyCardObject::withData($loyaltyRow);
                    break;//exit the loop
                }
            }
            
            if($custLoyaltyObject == null){//customer has no loyalty cards that are already stamped -need to add new loyalty card

                $LoyaltytoAdd = $loyaltyRows[0];

                $custLoyaltyObject = $instance->addLoyaltyCardToCustomerByLoyaltyID($customer,$LoyaltytoAdd['md_row_id']);

                $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Start new card','purchase',$orderID);
            }    

            $levelDataManager = new levelDataManager(6);
            $loyaltyRow = $levelDataManager->getLevelDataDirectByID($custLoyaltyObject->customer_card_program_id)->data;

            $requiredStamps = $loyaltyRow['md_stamp_number'];  

            for ($i = 0; $i < $amount; $i++)
            {
                if($custLoyaltyObject->customer_card_stamp_no >= $requiredStamps){
                    $custLoyaltyObject = $instance->addLoyaltyCardToCustomerByLoyaltyID($customer,$custLoyaltyObject->customer_card_program_id);

                    $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Start new card','purchase',$orderID);
                }

            	$newStampID = $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Punch','purchase',$orderID);                
                
                if($newStampID > 0){
                    $custLoyaltyObject->customer_card_stamp_no = $custLoyaltyObject->customer_card_stamp_no + 1;
                    $custLoyaltyObject->customer_card_last_stamp = Date('Y-m-d H:i:s');
                    
                    if($custLoyaltyObject->customer_card_stamp_no >= $requiredStamps){//required stamps met - add benefit and add code to customer card
                        $custLoyaltyObject->customer_card_code = utilityManager::generateRedeemCode($custLoyaltyObject->customer_card_cust_id."1"); 

                        $instance->grantBenefitFromLoyaltyCard($custLoyaltyObject,$loyaltyRow['md_head'],$loyaltyRow['md_desc']);

                        $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Complete card','purchase',$orderID);

                       
                        self::addBadgeToCustomer($custLoyaltyObject->customer_card_cust_id,enumBadges::benefits);
                        self::sendAsyncCustomerDataUpdateToDevice('benefits',$custLoyaltyObject->customer_card_cust_id);
                        $instance->upateCustomerLoyaltyCardDB($custLoyaltyObject);

                        eventManager::actionTrigger(enumCustomerActions::completedPunchCard,$custLoyaltyObject->customer_card_cust_id,'completed_punch','','',$custLoyaltyObject->customer_card_id);                       
                    }
                    else{
                        eventManager::actionTrigger(enumCustomerActions::punchedCard,$custLoyaltyObject->customer_card_cust_id,'punched','','',$custLoyaltyObject->customer_card_id);
                    }
                }
                
            }

            $instance->upateCustomerLoyaltyCardDB($custLoyaltyObject);            

            return resultObject::withData(1,'',$custLoyaltyObject->customer_card_id);
            
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['itemID'] = $itemID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function addCustomerLoyaltyStampToCustomerFromMeeting(customerObject $customer,$itemID,$meetingID = 0){
        try{
            $instance = new self();
            $loyaltyRows = $instance->getAutoStampedLoyaltyCardsConnectedToItemForCustomer($customer,$itemID,'service');

            if(count($loyaltyRows) == 0){
                return resultObject::withData(0,'no_loyalties');
            }

            $custLoyaltyObject = null;
            foreach ($loyaltyRows as $loyaltyRow)
            {
            	if(isset($loyaltyRow['customer_card_id'])){//customer has this loyalty card available for stamping                    
                    $custLoyaltyObject = customerLoyaltyCardObject::withData($loyaltyRow);
                    break;
                }
            }

            if($custLoyaltyObject == null){//customer has no loyalty cards that are already stamped -need to add new loyalty card

                $LoyaltytoAdd = $loyaltyRows[0];

                $custLoyaltyObject = $instance->addLoyaltyCardToCustomerByLoyaltyID($customer,$LoyaltytoAdd['md_row_id']);

                $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Start new card','appointment',$meetingID);
            }

            $levelDataManager = new levelDataManager(6);
            $loyaltyRow = $levelDataManager->getLevelDataDirectByID($custLoyaltyObject->customer_card_program_id)->data;

            $requiredStamps = $loyaltyRow['md_stamp_number'];  

            $newStampID = $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Punch','appointment',$meetingID);
                
            if($newStampID > 0){
                $custLoyaltyObject->customer_card_stamp_no = $custLoyaltyObject->customer_card_stamp_no + 1;
                $custLoyaltyObject->customer_card_last_stamp = Date('Y-m-d H:i:s');
                $stampAdded = true;
                    
                if($custLoyaltyObject->customer_card_stamp_no >= $requiredStamps){//required stamps met - add benefit and add code to customer card
                    $custLoyaltyObject->customer_card_code = utilityManager::generateRedeemCode($custLoyaltyObject->customer_card_cust_id."1"); 

                    $instance->grantBenefitFromLoyaltyCard($custLoyaltyObject,$loyaltyRow['md_head'],$loyaltyRow['md_desc']);

                    $instance->addStampToCustomerLoyaltyCardFromAction($custLoyaltyObject,'Complete card','appointment',$meetingID);

                    self::addBadgeToCustomer($custLoyaltyObject->customer_card_cust_id,enumBadges::benefits);
                    self::sendAsyncCustomerDataUpdateToDevice('benefits',$custLoyaltyObject->customer_card_cust_id);                    

                    eventManager::actionTrigger(enumCustomerActions::completedPunchCard,$custLoyaltyObject->customer_card_cust_id,'completed_punch','','',$custLoyaltyObject->customer_card_id);                       
                }
                else{
                    eventManager::actionTrigger(enumCustomerActions::punchedCard,$custLoyaltyObject->customer_card_cust_id,'punched','','',$custLoyaltyObject->customer_card_id);
                }
            }
            
            $instance->upateCustomerLoyaltyCardDB($custLoyaltyObject);

            return resultObject::withData(1,'',$custLoyaltyObject->customer_card_id);
            
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['itemID'] = $itemID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BASIC CUSTOMER LOYALTY - PRIVATE  */
    /************************************* */

    private function getCustomerPunchCardEntriesbyCustID($custID,$skip = 0, $take = 40){
        if(!is_numeric($custID) || $custID <= 0){
            throw new Exception("Ilegal customer loyalty ID");
        }

        $sql = "SELECT * FROM tbl_customer_card,tbl_mod_data6
                WHERE  customer_card_program_id = md_row_id
                AND customer_card_cust_id = $custID
                LIMIT $skip,$take";

        return $this->db->getTable($sql);
    }

    private function getStampEntriesForPunchCardbyCardID($cardID){
        if(!is_numeric($cardID) || $cardID <= 0){
            throw new Exception("Ilegal customer loyalty ID");
        }

        $sql = "SELECT * FROM tbl_customer_card_stamps
                WHERE customer_card_stamp_card_id = $cardID
                ORDER BY customer_card_stamp_date DESC";

        return $this->db->getTable($sql);
    }

    private function getLoyaltyCardByCardIDDB($cardID){
        $sql = "select * 
                    from tbl_customer_card,tbl_mod_data6
                    where customer_card_program_id=md_row_id
                    and customer_card_id=$cardID";

        return $this->db->getRow($sql);
    }

    private function addLoyaltyCardRowToCustomer(customerLoyaltyCardObject $obj){
       
        if (!isset($obj)){
            throw new Exception("customerLoyaltyCardObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_customer_card SET 
                customer_card_biz_id = {$obj->customer_card_biz_id},
                customer_card_program_id = {$obj->customer_card_program_id},
                customer_card_cust_id = {$obj->customer_card_cust_id},
                customer_card_stamp_no = {$obj->customer_card_stamp_no},
                customer_card_redeemed = {$obj->customer_card_redeemed}
         ");



        return $newId;
       
    }

    private function upateCustomerLoyaltyCardDB(customerLoyaltyCardObject $obj){

        if (!isset($obj->customer_card_id) || !is_numeric($obj->customer_card_id) || $obj->customer_card_id <= 0){
            throw new Exception("customerLoyaltyCardObject value must be provided");             
        }

        $customer_card_start_dateDate = isset($obj->customer_card_start_date) ? "'".$obj->customer_card_start_date."'" : "null";

        $customer_card_last_stampDate = isset($obj->customer_card_last_stamp) ? "'".$obj->customer_card_last_stamp."'" : "null";

        $customer_card_redeemed_dateDate = isset($obj->customer_card_redeemed_date) ? "'".$obj->customer_card_redeemed_date."'" : "null";

        $this->db->execute("UPDATE tbl_customer_card SET 
            customer_card_biz_id = {$obj->customer_card_biz_id},
            customer_card_program_id = {$obj->customer_card_program_id},
            customer_card_cust_id = {$obj->customer_card_cust_id},
            customer_card_stamp_no = {$obj->customer_card_stamp_no},
            customer_card_start_date = $customer_card_start_dateDate,
            customer_card_last_stamp = $customer_card_last_stampDate,
            customer_card_redeemed = {$obj->customer_card_redeemed},
            customer_card_redeemed_date = $customer_card_redeemed_dateDate,
            customer_card_code = '{$obj->customer_card_code}'
            WHERE customer_card_id = {$obj->customer_card_id} 
         ");
    }

    private function deleteCustomerLoyaltyCardByIdDB($customerLoyaltyCardID){

        if (!is_numeric($customerLoyaltyCardID) || $customerLoyaltyCardID <= 0){
            throw new Exception("Illegal value $customerLoyaltyCardID");             
        }

        $this->db->execute("DELETE FROM tbl_customer_card WHERE customer_card_id = $customerLoyaltyCardID");
    }

    private function getAutoStampedLoyaltyCardsConnectedToItemForCustomer(customerObject $customer,$itemID,$itemType){
        $sql = "SELECT *,IF(customer_card_stamp_no IS NULL,0,customer_card_stamp_no) customerStampCount FROM tbl_loyalty_stamp_items,tbl_mod_data6
                    LEFT JOIN tbl_customer_card ON customer_card_program_id = md_row_id AND customer_card_cust_id = {$customer->cust_id} AND customer_card_code = 0
                WHERE lstamp_loyalty_id = md_row_id
                AND md_bool1 = 1
                AND lstamp_item_id = $itemID
                AND lstamp_item_type = '$itemType'
                AND (customer_card_id IS NULL
                    OR customer_card_code = 0)
                GROUP BY md_row_id
                ORDER BY md_stamp_number,customerStampCount ASC";

        return $this->db->getTable($sql);
    }

    private function grantBenefitFromLoyaltyCard(customerLoyaltyCardObject $custLoyaltyObject,$title,$description){
         $benefit = new customerBenefitObject();

        $benefit->cb_biz_id = $custLoyaltyObject->customer_card_biz_id;
        $benefit->cb_cust_id = $custLoyaltyObject->customer_card_cust_id;
        $benefit->cb_type = 'loyalty';
        $benefit->cb_source_row_id = $custLoyaltyObject->customer_card_program_id;
        $benefit->cb_external_id = $custLoyaltyObject->customer_card_id;
        $benefit->cb_code = $custLoyaltyObject->customer_card_code;
        $benefit->cb_title = $title;
        $benefit->cb_description = $description;

        self::addCustomerBenefitRow($benefit);
    }

    private function addStampToCustomerLoyaltyCardFromAction(customerLoyaltyCardObject $custLoyaltyObject,$status = 'Punch',$origin,$originID = 0){
        $newStamp = new customerLoyaltyCardStampObject();

        $newStamp->customer_card_stamp_card_id = $custLoyaltyObject->customer_card_id;
        $newStamp->customer_card_stamp_origin = $origin;
        $newStamp->customer_card_status = $status;
        $newStamp->customer_card_stamp_origin_id = $originID;

        return $this->addCustomerLoyaltyCardStampDB($newStamp);
    }

    private function getRequiredStampsNumberForLoyaltyCardByID($loyaltyID){
        $sql = "SELECT md_stamp_number FROM tbl_mod_data6 WHERE md_row_id = $loyaltyID";

        return $this->db->getVal($sql);
    }
    
    private function addLoyaltyCardToCustomerByLoyaltyID(customerObject $customer,$loyaltyID){
        $custLoyaltyObject= new customerLoyaltyCardObject();

        $custLoyaltyObject->customer_card_cust_id = $customer->cust_id;
        $custLoyaltyObject->customer_card_biz_id = $customer->cust_biz_id;
        $custLoyaltyObject->customer_card_program_id = $loyaltyID;

        $newCardID = $this->addLoyaltyCardRowToCustomer($custLoyaltyObject);

        $newCustLoyaltyRow = $this->getLoyaltyCardByCardIDDB($newCardID);

        return customerLoyaltyCardObject::withData($newCustLoyaltyRow);
    }
    
    /************************************* */
    /*   BASIC CUSTOMERLOYALTYCARDSTAMP - DB METHODS           */
    /************************************* */

    private function addCustomerLoyaltyCardStampDB(customerLoyaltyCardStampObject $obj){

        if (!isset($obj)){
            throw new Exception("customerLoyaltyCardStampObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_customer_card_stamps SET 
            customer_card_stamp_card_id = {$obj->customer_card_stamp_card_id},
            customer_card_stamp_origin = '{$obj->customer_card_stamp_origin}',
            customer_card_stamp_details = '".addslashes($obj->customer_card_stamp_details)."',
            customer_card_status = '{$obj->customer_card_status}',
            customer_card_stamp_origin_id = {$obj->customer_card_stamp_origin_id}
                     ");
         return $newId;
    }

    private function loadCustomerLoyaltyCardStampFromDB($customerLoyaltyCardStampID){

        if (!is_numeric($customerLoyaltyCardStampID) || $customerLoyaltyCardStampID <= 0){
            throw new Exception("Illegal value $customerLoyaltyCardStampID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_card_stamps WHERE customer_card_stamp_id = $customerLoyaltyCardStampID");
    }

    private function upateCustomerLoyaltyCardStampDB(customerLoyaltyCardStampObject $obj){

        if (!isset($obj->customer_card_stamp_id) || !is_numeric($obj->customer_card_stamp_id) || $obj->customer_card_stamp_id <= 0){
            throw new Exception("customerLoyaltyCardStampObject value must be provided");             
        }

        $customer_card_stamp_dateDate = isset($obj->customer_card_stamp_date) ? "'".$obj->customer_card_stamp_date."'" : "null";

        $this->db->execute("UPDATE tbl_customer_card_stamps SET 
            customer_card_stamp_card_id = {$obj->customer_card_stamp_card_id},
            customer_card_stamp_date = $customer_card_stamp_dateDate,
            customer_card_stamp_origin = '{$obj->customer_card_stamp_origin}',
            customer_card_stamp_details = '".addslashes($obj->customer_card_stamp_details)."',
            customer_card_status = '{$obj->customer_card_status}',
            customer_card_stamp_origin_id = {$obj->customer_card_stamp_origin_id}
            WHERE customer_card_stamp_id = {$obj->customer_card_stamp_id} 
         ");
    }

    private function deleteCustomerLoyaltyCardStampByIdDB($customerLoyaltyCardStampID){

        if (!is_numeric($customerLoyaltyCardStampID) || $customerLoyaltyCardStampID <= 0){
            throw new Exception("Illegal value $customerLoyaltyCardStampID");             
        }

        $this->db->execute("DELETE FROM tbl_customer_card_stamps WHERE customer_card_stamp_id = $customerLoyaltyCardStampID");
    }

    /************************************* */
    /*   EXCLUSIVE CUSTOMER                */
    /************************************* */

    public static function startMemberDuration($biz_id, $cust_id){

        $instance = new self();
        if(!$instance->customerExistInExclusiveClub($cust_id)){
            
            $bizRule = bizManager::getMemberClubRulesForBiz($biz_id);

            $duration = "NULL";
            if($bizRule["bmr_mode"] == "duration"){
                $duration = "DATE_ADD(NOW(), INTERVAL {$bizRule["bmr_duration_number"]} {$bizRule["bmr_duration_period"]})";
            }
            $instance->db->execute("INSERT INTO tbl_customer_club_membership
                                    SET ccm_biz_id=$biz_id,
                                    ccm_customer_id=$cust_id,
                                    ccm_original_end_date = $duration;
                                    ");
        }
    }

    public static function getMemberExpirationDate($cust_id){

        $instance = new self();
        $currentCustomer = $instance->getCurrentExclusiveCustomer($cust_id);

        return (isset($currentCustomer["ccm_original_end_date"]) && $currentCustomer["ccm_original_end_date"] != "") ? $currentCustomer["ccm_original_end_date"] : "";
    }

    private function getCurrentExclusiveCustomer($cust_id){

        return $this->db->getRow("SELECT * FROM tbl_customer_club_membership 
                                WHERE ccm_customer_id = $cust_id
                                AND ccm_ended_date is NULL");
    }

    private function customerExistInExclusiveClub($cust_id){

        return $this->db->getVal("SELECT count(ccm_id) FROM tbl_customer_club_membership 
                                WHERE ccm_customer_id = $cust_id") > 0;
    }

}

