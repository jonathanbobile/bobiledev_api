<?php
/**
 * Methods for Send Mail Functions
 *
 * @version 1.0
 * @author PapTap
 */

class emailManager {

    protected $db;

    public function __construct()
    {
        try {
            $this->db = new Database();
            $this->dbAPI = new Database("api");
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }
    }

    public function getCampaignData($campaignID,$ownerId=0)
    {
        $data["mail_row"] =  $this->db->getRow("select * from tbl_campaign where ca_id=$campaignID");
        $data["header"] = "";
        $data["footer"] = "";
        $data["fromName"] = "";

        if($data["mail_row"]["ca_has_full_layout"] == 0 && $data["mail_row"]["ca_custom_html"] == ""){
            $data["header"] = $this->db->getVal("select ca_mail from tbl_campaign where ca_id=125" );
            $data["footer"] = $this->db->getVal("select ca_mail from tbl_campaign where ca_id=126" );
        }

        if($ownerId != 0){
            $customRow = $this->db->getRow("select * from tbl_campaign_owner where cao_campaign_id=$campaignID and cao_owner_id=$ownerId");
            if($customRow["cao_id"] != ""){
                $data["mail_row"]["ca_subject"] = $customRow["cao_subject"];
                $data["mail_row"]["ca_header"] = $customRow["cao_header"];
                $data["mail_row"]["ca_text_1"] = $customRow["cao_text_1"];
                $data["mail_row"]["ca_text_2"] = $customRow["cao_text_2"];
                $data["mail_row"]["ca_text_3"] = $customRow["cao_text_3"];
                $data["mail_row"]["ca_button_text"] = $customRow["cao_button_text"];
                $data["mail_row"]["ca_footer"] = $customRow["cao_footer"];
                $data["mail_row"]["ca_back_color"] = $customRow["cao_back_color"];
                $data["mail_row"]["ca_text_color"] = $customRow["cao_text_color"];
                $data["mail_row"]["ca_button_color"] = $customRow["cao_button_color"];
                $data["fromName"] = $customRow["cao_from_name"];
                $data["header"] = "";
                $data["footer"] = "";
            }
        }

        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_back_color#' , $data["mail_row"]["ca_back_color"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_text_color#' , $data["mail_row"]["ca_text_color"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_button_color#' , $data["mail_row"]["ca_button_color"] , $data["mail_row"]["ca_custom_html"]);

        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_header#' , $data["mail_row"]["ca_header"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_text_1#' , $data["mail_row"]["ca_text_1"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_text_2#' , $data["mail_row"]["ca_text_2"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_text_3#' , $data["mail_row"]["ca_text_3"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_button_text#' , $data["mail_row"]["ca_button_text"] , $data["mail_row"]["ca_custom_html"]);
        $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_footer#' , $data["mail_row"]["ca_footer"] , $data["mail_row"]["ca_custom_html"]);

        if($data["mail_row"]["ca_button_url"] != ""){
            $addToUrl = ($campaignID == 214 || $campaignID ==216) ? "" : "?eid=#eid#";
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_button_url#' , $data["mail_row"]["ca_button_url"].$addToUrl , $data["mail_row"]["ca_custom_html"]);
        }else{
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#ca_button_url#' , $data["mail_row"]["ca_button_url"] , $data["mail_row"]["ca_custom_html"]);
        }


        if(trim($data["mail_row"]["ca_text_2"]) == ""){
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_paragraph_2#' , "none", $data["mail_row"]["ca_custom_html"]);
        }else{
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_paragraph_2#' , "", $data["mail_row"]["ca_custom_html"]);
        }

        if(trim($data["mail_row"]["ca_text_3"]) == ""){
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_paragraph_3#' , "none", $data["mail_row"]["ca_custom_html"]);
        }else{
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_paragraph_3#' , "", $data["mail_row"]["ca_custom_html"]);
        }

        if(trim($data["mail_row"]["ca_button_text"]) == ""){
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_button#' , "none", $data["mail_row"]["ca_custom_html"]);
        }else{
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_button#' , "", $data["mail_row"]["ca_custom_html"]);
        }

        if(trim($data["mail_row"]["ca_footer"]) == ""){
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_footer#' , "none", $data["mail_row"]["ca_custom_html"]);
        }else{
            $data["mail_row"]["ca_custom_html"] = str_replace ( '#hide_footer#' , "", $data["mail_row"]["ca_custom_html"]);
        }

        if($data["mail_row"]["ca_custom_html"] != ""){
            $data["mail_row"]["ca_mail"] = $data["mail_row"]["ca_custom_html"];
        }

        $data["header"] = str_replace ( '#hidden_header#' , $data["mail_row"]["ca_hiddenHeader"] , $data["header"]);
        $data["header"] = str_replace ( '#back_color#' , $data["mail_row"]["ca_back_color"] , $data["header"]);

        $data["mail_row"] = str_replace ( '#hidden_header#' , $data["mail_row"]["ca_hiddenHeader"] , $data["mail_row"]);
        $data["mail_row"] = str_replace ( '#back_color#' , $data["mail_row"]["ca_back_color"] , $data["mail_row"]);

        return $data;
    }

    private function getAccountRow($acID,$ignoreunsub = false)
    {
        $unsub = $ignoreunsub ? "" : " AND ac_unsub=0";
        return $this->db->getRow("select
                            '' biz_contact_name,'' biz_short_name,owner_conf_code biz_conf_code,'' biz_id, IFNULL(reseller_wl_brand_logo, 'https://storage.googleapis.com/bobile/images/emails/b_logo.png') biz_icon,reseller_id,reseller_wl_brand_logo,reseller_wl_domain,IFNULL(reseller_company_name,ac_name) fromName,
                            ac_username as owner_username,ac_password as owner_password,owner_id,ac_name as owner_name
                            from tbl_account,tbl_owners left join tbl_resellers on reseller_id=owner_reseller_id
                            where ac_id=$acID
                            and owner_account_id=ac_id
                            $unsub
                            and ac_bad_email=0");
    }

    private function getClientRow($acID,$ignoreunsub = false)
    {

        $ownerId = $this->db->getVal("SELECT owner_id FROM tbl_owners WHERE owner_account_id=$acID");
        if($ownerId == ""){
            $ownerId = $this->db->getVal("SELECT owner_id FROM tbl_account,tbl_owners
                            WHERE ac_id=$acID
                            AND ac_reseller_id = owner_reseller_id");
        }
        $ownerId = ($ownerId == "") ? 0 : $ownerId;

        $unsub = $ignoreunsub ? "" : " AND ac_unsub=0";

        return $this->db->getRow("select
                            '' biz_contact_name,'' biz_short_name,'' biz_conf_code,'' biz_id, reseller_wl_brand_logo biz_icon,reseller_id,reseller_wl_brand_logo,reseller_wl_domain,IFNULL(reseller_full_name,ac_name) fromName,
                            ac_username as owner_username,ac_password as owner_password,'$ownerId' as owner_id,ac_name as owner_name
                            from tbl_account left join tbl_resellers on reseller_id=ac_reseller_id
                            where ac_id=$acID
                           $unsub");
    }

    private function getOwnerRow($ownerID,$ignoreunsub = false)
    {
        $unsub = $ignoreunsub ? "" : " AND ac_unsub=0";
        return $this->db->getRow("select
                            '' biz_contact_name,'' biz_short_name,owner_conf_code biz_conf_code,'' biz_id, reseller_wl_brand_logo biz_icon,reseller_id,reseller_wl_brand_logo,reseller_wl_domain,IFNULL(reseller_company_name,ac_name) fromName,
                            ac_username as owner_username,ac_password as owner_password,owner_id,ac_name as owner_name
                            from tbl_account,tbl_owners left join tbl_resellers on reseller_id=owner_reseller_id
                            where owner_id=$ownerID
                            and owner_account_id=ac_id
                            $unsub
                            and ac_bad_email=0");
    }

    private function getCustomerRow($customerID){
        $sql = "SELECT * FROM tbl_customers WHERE cust_id = $customerID";

        return $this->db->getRow($sql);
    }

    private function getAppRow($appID,$ignoreunsub = false)
    {
        $unsub = $ignoreunsub ? "" : " AND ac_unsub=0";
        return $this->db->getRow("select
                            biz_contact_name,biz_short_name,owner_conf_code biz_conf_code,biz_id, biz_icon,reseller_id,reseller_wl_brand_logo,reseller_wl_domain,reseller_wl_protocol,IFNULL(reseller_company_name,ac_name) fromName,
                            ac_username as owner_username,ac_password as owner_password,owner_id,ac_name as owner_name,ac_conf_code
                            from tbl_biz,tbl_account,tbl_owners left join tbl_resellers on reseller_id=owner_reseller_id
                            where
                            biz_owner_id = owner_id
                            AND owner_account_id = ac_id
                            AND biz_id=$appID
                            $unsub
                            and ac_bad_email=0");
    }

    private function getResellerClientRow($appID)
    {
        return $this->db->getRow("SELECT tbl_account.* FROM tbl_account,tbl_reseller_clients,tbl_client_biz
                                    WHERE ac_id = reseller_client_account_id
                                    AND reseller_client_id = client_biz_client_id
                                    AND client_biz_biz_id = $appID
                                    AND (client_biz_active = 1 OR (client_biz_active = 0 AND client_biz_isActivated = 0))");
    }

    public static function getMailDataByIdOwner($ownerID,$campaignID,$extraParams = array())
    {
        $userRow = "";
        $instance = new self();
        $response = array();

        $userRow = $instance->getOwnerRow($ownerID,true);

        if(is_array($userRow)){

            $camp = $instance->getCampaignData($campaignID,$ownerID);

            $data = $instance->fillMailReplaicments($campaignID,$camp["header"].$camp["mail_row"]["ca_mail"].$camp["footer"],$userRow,$extraParams);
            $body = $data["text"];

            $data = $instance->fillMailReplaicments($campaignID,$camp["mail_row"]["ca_mail_text"],$userRow,$extraParams);
            $body_text = $data["text"];

            $data = $instance->fillMailReplaicments($campaignID,$camp["mail_row"]["ca_subject"],$userRow,$extraParams);
            $subject = $data["text"];

            $response["body"] = $body;
            $response["bodyText"] = $body_text;
            $response["subject"] = $subject;
        }

        return $response;
    }

    public static function sendSystemMailOwner($ownerID,$campaignID,$type,$extraParams = array(),$ignoreunsub = false)
    {
        $params = array(
                        "owner_id" => $ownerID,
                        "campaign_id" => $campaignID,
                        "type" => $type,
                        "extraParams" => $extraParams,
                    );

        $params["ignore_unsub"] = $ignoreunsub ? 1 : 0;

        utilityManager::curl(EMAIL_SERVER . "/admin/emailAsync/sendSystemMailOwner",array(),$params);
    }

    public static function sendSystemMailAccount($account,$campaignID,$type,$extraParams = array(),$ignoreunsub = false){
        $params = array(
                        "account_id" => $account["ac_id"],
                        "campaign_id" => $campaignID,
                        "type" => $type,
                        "extraParams" => $extraParams,
                    );

        $params["ignore_unsub"] = $ignoreunsub ? 1 : 0;

        utilityManager::curl(EMAIL_SERVER . "/admin/emailAsync/sendSystemMailAccount",array(),$params);
    }

    public static function sendSystemMailClient($account,$campaignID,$type,$extraParams = array(),$ignoreunsub = false,$appID = 0){
        $params = array(
                        "account_id" => $account["ac_id"],
                        "campaign_id" => $campaignID,
                        "type" => $type,
                        "extraParams" => $extraParams,
                    );

        $params["ignore_unsub"] = $ignoreunsub ? 1 : 0;

        utilityManager::curl(EMAIL_SERVER . "/admin/emailAsync/sendSystemMailClient",array(),$params);
    }

    public static function sendSystemMailCustomer($customerID,$campaignID,$type,$extraParams = array(),$ignoreunsub = false){
        $params = array(
                        "customer_id" => $customerID,
                        "campaign_id" => $campaignID,
                        "type" => $type,
                        "extraParams" => $extraParams,
                    );

        $params["ignore_unsub"] = $ignoreunsub ? 1 : 0;

        utilityManager::curl(EMAIL_SERVER . "/admin/emailAsync/sendSystemMailCustomer",array(),$params);
    }

    public static function sendSystemMailApp($appID,$campaignID,$type,$extraParams = array(),$ignoreunsub = false)
    {
        $params = array(
                        "biz_id" => $appID,
                        "campaign_id" => $campaignID,
                        "type" => $type,
                        "extraParams" => $extraParams,
                    );

        $params["ignore_unsub"] = $ignoreunsub ? 1 : 0;

        utilityManager::curl(EMAIL_SERVER . "/admin/emailAsync/sendSystemMailApp",array(),$params);
    }

    public static function testSystemMailApp($appID,$campaignID,$type,$extraParams = array(),$to)
    {
        $userRow = "";
        $instance = new self();

        $userRow = $instance->getAppRow($appID);

        if(is_array($userRow)){

            $camp = $instance->getCampaignData($campaignID);

            $data = $instance->fillMailReplaicments($campaignID,$camp["header"].$camp["mail_row"]["ca_mail"].$camp["footer"],$userRow,$extraParams);
            $body = $data["text"];
            $emcode = $data["emcode"];

            $data = $instance->fillMailReplaicments($campaignID,$camp["mail_row"]["ca_mail_text"],$userRow,$extraParams);
            $body_text = $data["text"];

            $data = $instance->fillMailReplaicments($campaignID,$camp["mail_row"]["ca_subject"],$userRow,$extraParams);
            $subject = $data["text"];

            $toName = (isset($extraParams["to_name"])) ? $extraParams["to_name"] : $userRow['owner_name'];
            //$toEmail = (isset($extraParams["to_email"])) ? $extraParams["to_email"] : $userRow['owner_username'];

            return $instance->addToQueue($type, $to, $subject, $body, $toName,$campaignID,$emcode,$appID,$body_text);
        }

        return -1;
    }

    private function fillMailReplaicments($campaignID,$repTEXT,$userRow,$extraParams = array()){

        //List of fields for select
        //biz_contact_name,biz_short_name,biz_conf_code,biz_id,biz_icon
        //owner_username,owner_password,owner_id from tbl_biz,tbl_owners
        if(isset($userRow) && $userRow != NULL){
            $emcode = $userRow["owner_id"].microtime(true);
            $encUsername = utilityManager::encryptIt( $userRow["owner_username"] ,true);
            $random_hash = isset($userRow["ac_conf_code"]) ? $userRow["ac_conf_code"] : $userRow["biz_conf_code"];
            $toEmail = (isset($extraParams["to_email"])) ? $extraParams["to_email"] : $userRow["owner_username"];
            $toEmail = utilityManager::encryptIt( $toEmail ,true);

            $downloads = "";
            $entries= "";
            if ($userRow["biz_id"] != ""){
                $downloads = $this->getDownloads($userRow["biz_id"]);
                $entries = $this->getEntries($userRow["biz_id"]);
            }

            if(isset($userRow["biz_id"]) && $userRow["biz_id"] != "" && $userRow["biz_id"] != 0){
                $unsubscribeCode = $toEmail."/".utilityManager::encryptIt($userRow["biz_id"] ,true);
            }else{
                $unsubscribeCode = $toEmail;
            }

            $repTEXT = str_replace ( '#name#' , $userRow["owner_name"] , $repTEXT);
            $repTEXT = str_replace ( '#biz_name#' , $userRow["biz_short_name"] , $repTEXT);
            $repTEXT = str_replace ( '#username#' , $userRow["owner_username"] , $repTEXT);
            $repTEXT = str_replace ( '#username_enc#' , $encUsername , $repTEXT);
            $repTEXT = str_replace ( '#username_unsub#' , $unsubscribeCode , $repTEXT);
            $repTEXT = str_replace ( '#password#' , $userRow["owner_password"] , $repTEXT);
            $repTEXT = str_replace ( '#biz_id#' , $userRow["biz_id"] , $repTEXT);
            $repTEXT = str_replace ( '#biz_icon#' ,$userRow["biz_icon"], $repTEXT);
            $repTEXT = str_replace ( '#biz_icon_origin#' ,$userRow["biz_icon"] , $repTEXT);
            $repTEXT = str_replace ( '#owner_icon#' , (isset($userRow["reseller_wl_brand_logo"]) && $userRow["reseller_wl_brand_logo"] != "") ? $userRow["reseller_wl_brand_logo"] : "https://storage.googleapis.com/bobile/images/bobile_logo.png" , $repTEXT);
            $repTEXT = str_replace ( '#owner_site#' , (isset($userRow["reseller_wl_domain"]) && isset($userRow["reseller_wl_protocol"]) && $userRow["reseller_wl_domain"] != "") ? $userRow["reseller_wl_protocol"]."://".$userRow["reseller_wl_domain"] : "https://bobile.com" , $repTEXT);
            $repTEXT = str_replace ( '#rid#' , (isset($userRow["reseller_id"]) && $userRow["reseller_id"] != "") ? $userRow["reseller_id"] : "0" , $repTEXT);
            $repTEXT = str_replace ( 'http://bobile.support/admin' , (isset($userRow["reseller_wl_domain"]) && isset($userRow["reseller_wl_protocol"]) && $userRow["reseller_wl_domain"] != "") ? $userRow["reseller_wl_protocol"]."://".$userRow["reseller_wl_domain"]."/admin" : "http://bobile.support/admin" , $repTEXT);
            $repTEXT = str_replace ( 'http://bobile.support/unsubscribe' , (isset($userRow["reseller_wl_domain"]) && isset($userRow["reseller_wl_protocol"]) && $userRow["reseller_wl_domain"] != "") ? $userRow["reseller_wl_protocol"]."://".$userRow["reseller_wl_domain"]."/unsubscribe" : "http://bobile.support/unsubscribe" , $repTEXT);
            $repTEXT = str_replace ( '#ecode#' , $emcode , $repTEXT);
            $repTEXT = str_replace ( '#random_hash#' , $random_hash , $repTEXT);
            $repTEXT = str_replace ( '#compid#' , $campaignID , $repTEXT);
            $repTEXT = str_replace ( '#downloads#' , $downloads , $repTEXT);
            $repTEXT = str_replace ( '#entries#' , $entries , $repTEXT);
        }

        if(count($extraParams)>0){
            foreach ($extraParams as $key => $value)
            {
                if($key != 'ctos_name' && $key != 'big_market'){
                    if(isset($extraParams[$key])) $repTEXT = str_replace ( '#'.$key.'#' , $value , $repTEXT);
                }
                else{
                    if(isset($extraParams["ctos_name"])) $repTEXT = str_replace ( '#order_status#' ,$extraParams["ctos_name"] , $repTEXT);
                    if(isset($extraParams["big_market"])) $repTEXT = str_replace ( '#market#' ,$extraParams["big_market"] , $repTEXT);
                }
            }
        }

        if(isset($userRow) && $userRow != NULL){
            if($userRow["biz_id"] != ""){
                $repTEXT = str_replace ( '#create_token#' , bizManager::getAppToken($userRow["biz_id"],"creator/postPreview",35) , $repTEXT);
                $repTEXT = str_replace ( '#eid#' , utilityManager::encryptIt( $userRow["biz_id"] ,true) , $repTEXT);
            }else{
                $repTEXT = str_replace ( '#create_token#' , "" , $repTEXT);
                $repTEXT = str_replace ( '#eid#' , "" , $repTEXT);
            }
        }

        $repTEXT = str_replace ( 'http://bobile.support/admin' , (isset($extraParams["reseller_wl_domain"]) && isset($extraParams["reseller_wl_protocol"]) && $extraParams["reseller_wl_domain"] != "") ? $extraParams["reseller_wl_protocol"]."://".$extraParams["reseller_wl_domain"]."/admin" : "http://bobile.support/admin" , $repTEXT);

        $data["text"] = $repTEXT;
        $data["emcode"] = $emcode;
        return $data;
    }

    private function getEmailAccountByType($type)
    {
        $account = array();

        switch ($type )
        {
        	case enumEmailType::SystemMailSMTP:
                    $account["from"] = "support@bobile.com";
                    $account["fromName"] = "Bobile Team";
                    $account["smtpserver"] = "mail.paptap.com,465";
                    $account["smtpuser"] = "support@bobile.com";
                    $account["smtppass"] = "pa55w0rd1qaz";
                break;
            case enumEmailType::SystemMailAmazon: //backwards compatible for older mailing since smtp is not available on Google
                    $account["from"] = "support@care.bobile.com";
                    $account["fromName"] = "Bobile Team";
                    $account["smtpserver"] = "http://104.197.241.210/send.php";
                    $account["sg"] = "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";
                break;
            case enumEmailType::SystemMailSGCare:
                    $account["from"] = "support@bobile.support";
                    $account["fromName"] = "Bobile Team";
                    $account["smtpserver"] = "http://104.197.241.210/send.php";
                    $account["sg"] = "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"; //New account admin@bobile.com

                    //$account["sg"] = "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"; //Older SendGrid Account at ira@bobile.com
                    //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
                break;

            case enumEmailType::BobileMailSGCampaign:
                $account["from"] = "support@bobile.info";
                $account["fromName"] = "Bobile Team";
                $account["smtpserver"] = "http://104.197.241.210/send.php";
                $account["sg"] = "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"; //New account admin@bobile.com

                //$account["sg"] = "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"; //Older SendGrid Account at ira@bobile.com
                //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
                break;

            //case enumEmailType::SystemMailSGSpecial:
            //        $account["from"] = "noreply@paptapspecials.com";
            //        $account["fromName"] = "Bobile Team";
            //        $account["smtpserver"] = "http://paptapspecials.com/send.php";
            //        $account["sg"] = "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";
            //    break;
            //case enumEmailType::SystemMailSGBilling:
            //        $account["from"] = "noreply@paptapbilling.com";
            //        $account["fromName"] = "Bobile Team";
            //        $account["smtpserver"] = "http://paptapbilling.com/send.php";
            //        $account["sg"] = "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";
            //    break;
            //case enumEmailType::SystemMailSgOld:
            //        $account["from"] = "care@paptapsupport.com";
            //        $account["fromName"] = "Bobile Team";
            //        $account["smtpserver"] = "http://paptapsupport.com/send.php";
            //        $account["sg"] = "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";
            //    break;

            //case enumEmailType::BobileSystemMailSGCare:
            //        $account["from"] = "support@bobile.com";
            //        $account["fromName"] = "Bobile Team";
            //        $account["smtpserver"] = "http://104.197.241.210/send.php";
            //        $account["sg"] = "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";
            //    break;
            case enumEmailType::SystemMailSGBeeZee:
                $account["from"] = "support@beezee.me";
                $account["fromName"] = "BeeZeeMe Team";
                $account["smtpserver"] = "http://104.197.241.210/send.php";
                $account["sg"] = "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";
                break;
        }

        return $account;
    }

    private function addToQueue($type, $to, $subject, $body, $toName,$campaignID,$emcode,$appID,$text="",$fromName=""){

        $subject = addslashes($subject);
        $body = addslashes($body);
        $text = addslashes($text);
        $toName = addslashes($toName);
        $fromName = addslashes($fromName);



        return $this->db->execute("insert into tbl_queue set
                                qu_to='$to',
                                qu_subject='$subject',
                                qu_body='$body',
                                qu_text='$text',
                                qu_to_name='$toName',
                                qu_from_name='$fromName',
                                qu_comp_id=$campaignID,
                                qu_account=$type,
                                qu_biz_id=$appID,
                                qu_code='$emcode'
                                ") != -1;
    }

    public static function dequeueWithLimit($limit)
    {
        $instance = new self();
        $result = $instance->db->getTable("select *
			                            from tbl_queue
                                        where qu_from is null
			                            LIMIT $limit");

        if(is_array($result)){
            foreach ($result as $record)
            {
                $params = array(
                    "queId" => $record["qu_id"]
                );

                utilityManager::asyncTask(AUTOJOBS_SERVER."/admin/autojobs/asyncDequeue",$params);
            }
        }
    }

    public static function asyncExecuteSendMail($queId){

        $instance = new self();
        $record = $instance->db->getRow("select *
			                            from tbl_queue
                                        where qu_id=$queId");

        $instance->db->execute("delete from tbl_queue where qu_id=$queId");

        if($record["qu_id"] != ""){
            $res_mail = $instance->executeSendMail($record["qu_account"],$record["qu_to"],$record["qu_subject"],$record["qu_body"],$record["qu_to_name"],$record["qu_text"],$record["qu_biz_id"],$record["qu_from_name"]);

            if($res_mail == "1")
            {
                if($record["qu_code"] != ""){
                    $updateMail = "insert ignore into tbl_email_stats set eml_compain=".$record["qu_comp_id"].",eml_code='{$record["qu_code"]}',eml_biz_id=".$record["qu_biz_id"];
                    $instance->db->execute($updateMail);
                }
            }
        }
    }

    public function sendTestMail($to,$toName,$server,$user,$password,$subject,$text){
        try{

            $body = $text;
            $body_text = "";
            $fromName = "Mail Tester";

            $data["to"] = $to;
            $data["toName"] = urlencode($toName);
            $data["subject"] = urlencode($subject);
            $data["body"] = urlencode($body);
            $data["bodyText"] = urlencode($body_text);
            $data["from"] = $user;
            $data["fromName"] = urlencode($fromName);
            $data["smtpserver"] = $server;
            $data["smtpuser"] = $user;
            $data["smtppass"] = $password;

            $result = array();
            $result['code'] = 1;
            $result['data'] = utilityManager::asyncPostCurl(AUTOJOBS_SERVER."/smtp/sendMail.php",$data,10);

            return $result;
        }
        catch(Exception $ex){
            $result = array();
            $result['code'] = 0;
            return $result;
        }
    }

    private function executeSendMail($type, $to, $subject, $body, $toName,$body_text,$bizID=0,$fromName=""){

        $resellerRow  = appManager::getReseller($bizID);

        if($resellerRow["reseller_wl_smtp_server"] != ""){
            $data["to"] = $to;
            $data["toName"] = urlencode($toName);
            $data["subject"] = urlencode($subject);
            $data["body"] = urlencode($body);
            $data["bodyText"] = urlencode($body_text);
            $data["from"] = $resellerRow["reseller_wl_support_email"];
            $data["fromName"] = urlencode($fromName != "" ? $fromName : $resellerRow["reseller_full_name"]);
            $data["smtpserver"] = $resellerRow["reseller_wl_smtp_server"];
            $data["smtpuser"] = $resellerRow["reseller_wl_smtp_user"];
            $data["smtppass"] = $resellerRow["reseller_wl_smtp_pass"];

            utilityManager::asyncPostCurl(AUTOJOBS_SERVER."/smtp/sendMail.php",$data);

            return 1;

        }else{

            $type = ($resellerRow["reseller_wl_email_type"] != 0) ? $resellerRow["reseller_wl_email_type"] : $type;

            $account = $this->getEmailAccountByType($type);

            if(isset($account["sg"])){

                $url = $account["smtpserver"];
                $fields = array(
                    'sendTo' => $to,
                    'sendToName' => $toName,
                    'sentFromMail' => $account["from"],
                    'sentFromName' => urlencode($fromName != "" ? $fromName : $account["fromName"]),
                    'replyTo' => $account["from"],
                    'subject' => urlencode(stripcslashes($subject)),
                    'body' => urlencode(stripcslashes($body)),
                    'text' => urlencode(stripcslashes($body_text)),
                    'sg' => $account["sg"]
                );

                $fields_string = "";
                foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
                $fields_string = rtrim($fields_string, '&');

                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL, $url);
                curl_setopt($ch,CURLOPT_POST, count($fields));
                curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

                $response = curl_exec($ch);

                return ($response) ? "1" : "-1";
            }
            return 0;
        }
    }

    public static function sendCustomMail($type,$to,$subject,$body,$toName)
    {
        $instance = new self();
        return $instance->addToQueue($type, $to, $subject, $body, $toName,0,"",0);
    }

    public static function getDefaulProvider() {
        return enumEmailType::SystemMailSGCare;
    }

    private function getDownloads($bizID){

        $downloads = 0;
        if($bizID != ""){
            $downloads = $this->db->getVal("SELECT SUM(Units) dlownloads FROM itc_sales,tbl_biz
                            WHERE AppleIdentifier = biz_sbmt_apl_bundle_suffix
                            AND biz_id = $bizID");
        }
        if($downloads < 10) $downloads = 10;
        return $downloads;
    }

    private function getEntries($bizID){

        $entries = $this->dbAPI->getVal("SELECT ROUND(SUM(Vol)/3,0) AVG_ENTRIES FROM (
                            SELECT (abv_volume_andr + abv_volume_ios) Vol FROM analytics_biz_visits
                            WHERE abv_biz_id = $bizID
                            ORDER BY abv_id desc
                            LIMIT 3
                            )SU ");
        if($entries < 10) $entries = 10;
        return $entries;
    }

    public function releaseCampaignOwner($compId,$type=enumEmailType::SystemMailSGCare,$limit="",$bizID=""){

        $addToSQL="";
        if($bizID !=""){
            $addToSQL = " AND biz_id=$bizID ";
        }

        $addLimit="";
        if($limit!="")
        {
            $addLimit = " LIMIT $limit ";
        }

        $sqlComp = "SELECT owner_id
            FROM tbl_biz,tbl_owners,tbl_account
            WHERE biz_owner_id = owner_id
            AND ac_id=owner_account_id
            AND ac_bad_email = 0
            AND ac_unsub = 0
            $addToSQL
            GROUP BY owner_username
            $addLimit";

        $data = $this->db->getTable($sqlComp);

        if(count($data)>0){
            foreach ($data as $oneOwner)
            {
                self::sendSystemMailOwner($oneOwner["owner_id"],$compId,$type);
            }
        }
    }

    public function releaseCampaignBiz($compId,$type=enumEmailType::SystemMailSGCare,$limit="",$bizID="",$alsoPremiums = "",$to=""){

        $addToSQL="";
        if($bizID !=""){
            $addToSQL = " and biz_id=$bizID ";
        }

        if ($alsoPremiums == ""){
            $addToSQL = $addToSQL." and biz_id not in (SELECT distinct(tf_biz_id) FROM tbl_transaction_feature WHERE tf_end_time is null and tf_price > 0) ";
        }

        // ***************   test select *****************//
          $sqlComp = "SELECT biz_id,owner_username,owner_password
             FROM tbl_owners, tbl_biz,tbl_account
             WHERE biz_owner_id = owner_id
             AND ac_id=owner_account_id
             AND ac_bad_email = 0
             AND ac_unsub = 0
             AND biz_bad_email = 0
             AND biz_unsubscribe = 0
             AND owner_username not in ('%s','null','(null)')
             AND biz_id=$bizID
             GROUP BY owner_username
             ";

        $data = $this->db->getTable($sqlComp);

        if(count($data)>0){
            foreach ($data as $oneBiz)
            {
                $code = "";
                switch($compId){
                    case 179:
                        $code = utilityManager::encryptIt("{$oneBiz["owner_username"]}@|@{$oneBiz["owner_password"]}@|@{$oneBiz["biz_id"]}@|@offers/specials/bfriday",true);
                        break;
                    case 180:
                        $code = utilityManager::encryptIt("{$oneBiz["owner_username"]}@|@{$oneBiz["owner_password"]}@|@{$oneBiz["biz_id"]}@|@offers/specials/cmonday",true);
                        break;
                    case 181:
                        $code = utilityManager::encryptIt("{$oneBiz["owner_username"]}@|@{$oneBiz["owner_password"]}@|@{$oneBiz["biz_id"]}@|@offers/claim_reward/1",true);
                        break;
                    case 182:
                        $code = utilityManager::encryptIt("{$oneBiz["owner_username"]}@|@{$oneBiz["owner_password"]}@|@{$oneBiz["biz_id"]}@|@offers/specials/xmas",true);
                        break;
                }

                //echo $code;
                $extraParams["auto_login"] = $code;
                if($to != ""){
                    $extraParams["to_email"] = $to;
                }
                self::sendSystemMailApp($oneBiz["biz_id"],$compId,$type,$extraParams);
            }
        }
    }

    public function markBadEmail($email)
    {
        $email = addslashes($email);
        $this->db->execute("update tbl_account set ac_bad_email=1 where ac_username='$email'");
    }

    public function markUnsubscribeEmail($email)
    {
        $email = addslashes($email);
        $this->db->execute("update tbl_account set ac_unsub=1 where ac_username='$email'");
    }

    public function getCustomersEmailList($type){
        return $this->db->getTable("select * from tbl_campaign where ca_entity='$type' order by ca_subject");
    }

    public function getPushNotifications(){
        return $this->db->getTable("select * from tbl_biz_notif_params where ntp_push_text <> ''  AND ntp_to_customer = 0 order by ntp_noti_header");
    }

    public function getCustomersPushNotifications(){
        return $this->db->getTable("select * from tbl_biz_notif_params where ntp_to_customer = 1");
    }

    public function getCustomersSingleEmail($emailId,$owner_id){
        return $this->db->getRow("select * from tbl_campaign left join tbl_campaign_owner on ca_id = cao_campaign_id and cao_owner_id = $owner_id
                                    where ca_id=$emailId
                                    ");
    }

    public static function getNotificationMessageFromBiz($pushId,$biz_id,$extraParams = array()){

        $instance = new self();
        $userRow = $instance->getAppRow($biz_id,true);
        $pushRow = $instance->getCustomersSinglePushFromBiz($pushId,$biz_id,$userRow["owner_id"]);

        $message = isset($pushRow['ntpo_push_text']) && $pushRow['ntpo_push_text'] != "" ? $pushRow['ntpo_push_text'] : $pushRow['ntp_push_text'];


        $message = str_replace ( '#biz_name#' , $userRow["biz_short_name"] , $message);

        if(count($extraParams)>0){
            foreach ($extraParams as $key => $value)
            {
                if(isset($extraParams[$key])) $message = str_replace ( '#'.$key.'#' , $value , $message);
            }
        }

        return $message;
    }

    public function getCustomersSinglePushFromBiz($pushId,$biz_id,$owner_id){
        $pushRow = $this->db->getRow("select * from tbl_biz_notif_params left join tbl_biz_notif_params_owner on ntp_id = ntpo_notification_id and ntpo_biz_id = $biz_id
                                    where ntp_id=$pushId
                                    ");

        if(!isset($pushRow["ntpo_id"])){
            return $this->getCustomersSinglePush($pushId,$owner_id);
        }

        return $pushRow;
    }

    public function getCustomersSinglePush($pushId,$owner_id){
        return $this->db->getRow("select * from tbl_biz_notif_params left join tbl_biz_notif_params_owner on ntp_id = ntpo_notification_id and ntpo_owner_id = $owner_id AND ntpo_biz_id = 0
                                    where ntp_id=$pushId
                                    ");
    }

    public function saveEmail($postData){

        $subject = addslashes($postData["subject"]);
        $backColor = addslashes($postData["backColor"]);
        $textColor = addslashes($postData["textColor"]);
        $buttonColor = addslashes($postData["buttonColor"]);
        $textHeader = addslashes(trim($postData["textHeader"]));
        $textText1 = addslashes(trim($postData["textText1"]));
        $textText2 = addslashes(trim($postData["textText2"]));
        $textText3 = addslashes(trim($postData["textText3"]));
        $textFooter = addslashes(trim($postData["textFooter"]));
        $textButton = addslashes(trim($postData["textButton"]));
        $from = addslashes(trim($postData["from"]));

        $sql = "INSERT INTO tbl_campaign_owner SET
                    cao_campaign_id = {$postData["eid"]},
                    cao_owner_id = {$postData["oid"]},
                    cao_subject = '$subject',
                    cao_back_color = '$backColor',
                    cao_text_color = '$textColor',
                    cao_button_color = '$buttonColor',
                    cao_header = '$textHeader',
                    cao_text_1 = '$textText1',
                    cao_text_2 = '$textText2',
                    cao_text_3 = '$textText3',
                    cao_footer = '$textFooter',
                    cao_button_text = '$textButton',
                    cao_from_name = '$from'
                ON DUPLICATE KEY
                UPDATE cao_subject = '$subject',
                    cao_back_color = '$backColor',
                    cao_text_color = '$textColor',
                    cao_button_color = '$buttonColor',
                    cao_header = '$textHeader',
                    cao_text_1 = '$textText1',
                    cao_text_2 = '$textText2',
                    cao_text_3 = '$textText3',
                    cao_footer = '$textFooter',
                    cao_button_text = '$textButton',
                    cao_from_name = '$from'
                ";

        $this->db->execute($sql);
    }

    public function savePush($postData){

        $message = addslashes($postData["message"]);

        $sql = "INSERT INTO tbl_biz_notif_params_owner SET
                    ntpo_notification_id = {$postData["pid"]},
                    ntpo_owner_id = {$postData["oid"]},
                    ntpo_push_text = '$message'
                ON DUPLICATE KEY
                UPDATE ntpo_push_text = '$message'
                ";

        $this->db->execute($sql);
    }
    public function resetEmail($postData){

        $masterRow = $this->db->getRow("select * from tbl_campaign where ca_id={$postData["eid"]}");

        $subject = addslashes($masterRow["ca_subject"]);
        $backColor = addslashes($masterRow["ca_back_color"]);
        $textColor = addslashes($masterRow["ca_text_color"]);
        $buttonColor = addslashes($masterRow["ca_button_color"]);
        $textHeader = addslashes(trim($masterRow["ca_header"]));
        $textText1 = addslashes(trim($masterRow["ca_text_1"]));
        $textText2 = addslashes(trim($masterRow["ca_text_2"]));
        $textText3 = addslashes(trim($masterRow["ca_text_3"]));
        $textFooter = addslashes(trim($masterRow["ca_footer"]));
        $textButton = addslashes(trim($masterRow["ca_button_text"]));

        $sql = "INSERT INTO tbl_campaign_owner SET
                    cao_campaign_id = {$postData["eid"]},
                    cao_owner_id = {$postData["oid"]},
                    cao_subject = '$subject',
                    cao_back_color = '$backColor',
                    cao_text_color = '$textColor',
                    cao_button_color = '$buttonColor',
                    cao_header = '$textHeader',
                    cao_text_1 = '$textText1',
                    cao_text_2 = '$textText2',
                    cao_text_3 = '$textText3',
                    cao_footer = '$textFooter',
                    cao_button_text = '$textButton'
                ON DUPLICATE KEY
                UPDATE cao_subject = '$subject',
                    cao_back_color = '$backColor',
                    cao_text_color = '$textColor',
                    cao_button_color = '$buttonColor',
                    cao_header = '$textHeader',
                    cao_text_1 = '$textText1',
                    cao_text_2 = '$textText2',
                    cao_text_3 = '$textText3',
                    cao_footer = '$textFooter',
                    cao_button_text = '$textButton'
                ";

        $this->db->execute($sql);
    }

    public function resetPush($postData){

        $masterRow = $this->db->getRow("select * from tbl_biz_notif_params where ntp_id={$postData["pid"]}");

        $message = addslashes($masterRow["ntp_push_text"]);

        $sql = "INSERT INTO tbl_biz_notif_params_owner SET
                    ntpo_notification_id = {$postData["pid"]},
                    ntpo_owner_id = {$postData["oid"]},
                    ntpo_push_text = '$message'
                ON DUPLICATE KEY
                UPDATE ntpo_push_text = '$message'
                ";

        $this->db->execute($sql);
    }
}


?>
