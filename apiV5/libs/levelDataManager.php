
<?php 

class levelDataManager extends Manager
{
 
    private $modId;
    public $mobileRequestObject = array();

    function __construct($modId,$mobileRequestObject = array())
    {
        parent::__construct();
        $this->modId = $modId;
        $this->mobileRequestObject = $mobileRequestObject;
    }

/************************************* */
/*   BASIC LEVELDATA - PUBLIC           */
/************************************* */

    /**
    * Insert new levelDataObject to DB
    * Return Data = new levelDataObject ID
    * @param levelDataObject $levelDataObj 
    * @return resultObject
    */
    public function addLevelData(levelDataObject $levelDataObj){    
    
        $callFunction = "addLevelData{$this->modId}DB";
    
        try{
            $newId = $this->{$callFunction}($levelDataObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($levelDataObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get levelData from DB for provided ID
    * * Return Data = levelDataObject
    * @param int $levelDataId 
    * @return resultObject
    */
    public function getLevelDataByID($levelDataId){
        
        $objectName = "levelData{$this->modId}Object";

        try {
            $levelDataData = $this->loadDataLevelFromDB($levelDataId);

            $levelDataObj = $objectName::withData($levelDataData);
            $result = resultObject::withData(1,'',$levelDataObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$levelDataId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update levelData in DB
    * @param levelDataObject $levelDataObj 
    * @return resultObject
    */
    public function updateLevelData(levelDataObject $levelDataObj){   
        
        $callFunction = "upateLevelData{$this->modId}DB";

        try{
            $this->{$callFunction}($levelDataObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($levelDataObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete levelData from DB
    * @param int $levelDataID 
    * @return resultObject
    */
    public function deleteLevelDataById($levelDataID){

        try{
            $this->deleteDataLevelByIdDB($levelDataID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$levelDataID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function deleteLevelDataElements($bizId,$levelNo,$parent){

        if (!is_numeric($bizId) || $bizId <= 0){
            throw new Exception("Illegal value bizId");             
        }

        $this->db->execute("DELETE FROM tbl_mod_data{$this->modId} WHERE md_level_no=$levelNo AND md_parent=$parent AND md_biz_id=$bizId");
    }

    /**
    * Returns levelData directly from DB
    * @param int $levelDataID 
    * @return resultObject
    */
    public function getLevelDataDirectByID($levelDataID){
        try{
            $data = $this->loadDataLevelFromDB($levelDataID);
            return resultObject::withData(1,'',$data);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$levelDataID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Returns levelData directly from DB by external ID
     * @param int $externalID 
     * @return resultObject
     */
    public function loadDataLevelByExternalId($externalID){
        try{
            $data = $this->loadDataLevelByExternalId_DB($externalID);
            return resultObject::withData(1,'',$data);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$externalID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC LEVELDATA - DB METHODS           */
    /************************************* */

    private function loadDataLevelFromDB($dataLevelID){

        if (!is_numeric($dataLevelID) || $dataLevelID <= 0){
            throw new Exception("Illegal value dataLevelID");             
        }
        
        $nib_name = "";
        if(isset($_REQUEST["android"])){
            $nib_name = "_android";
        }

        $server_customer_id = isset($_REQUEST["server_customer_id"]) ? $_REQUEST["server_customer_id"] : "";

        return $this->db->getRow("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data{$this->modId} 
                                    WHERE md_row_id = $dataLevelID");
    }

    private function loadDataLevelByExternalId_DB($externalID){

        if (!is_numeric($externalID) || $externalID <= 0){
            throw new Exception("Illegal value dataLevelID");             
        }
        
        return $this->db->getRow("SELECT * FROM tbl_mod_data{$this->modId} WHERE md_external_id = $externalID");
    }

    private function deleteDataLevelByIdDB($dataLevelID){

        if (!is_numeric($dataLevelID) || $dataLevelID <= 0){
            throw new Exception("Illegal value dataLevelID");             
        }

        $this->db->execute("DELETE FROM tbl_mod_data{$this->modId} WHERE md_row_id = $dataLevelID");
    }

    /* MOD 0 - Main menu */
    private function addLevelData0DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data0 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_external_sub_id = {$obj->md_external_sub_id},
                                        md_belong_to_structure = {$obj->md_belong_to_structure}
             ");
        return $newId;
    }

    private function upateLevelData0DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data0 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_external_sub_id = {$obj->md_external_sub_id},
                                md_belong_to_structure = {$obj->md_belong_to_structure}
                                WHERE md_row_id = {$obj->md_row_id} 
             ");
    }

    /* MOD 1 - About Us */
    private function addLevelData1DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data1 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData1DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data1 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_id = {$obj->md_external_id}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 2 - Show Room */
    private function addLevelData2DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data2 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_external_has_action = {$obj->md_external_has_action},
                                        md_external_action_title = '".addslashes($obj->md_external_action_title)."'
         ");
        return $newId;
    }

    private function upateLevelData2DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data2 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_external_has_action = {$obj->md_external_has_action},
                                md_external_action_title = '".addslashes($obj->md_external_action_title)."'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 3 - News */
    private function addLevelData3DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data3 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData3DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data3 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 4 - Photos */
    private function addLevelData4DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data4 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_first_img = '".addslashes($obj->md_first_img)."',
                                        md_photos = {$obj->md_photos},
                                        md_albums = {$obj->md_albums},
                                        md_temp = {$obj->md_temp},
                                        md_thumb_uploaded = {$obj->md_thumb_uploaded},
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData4DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data4 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_first_img = '".addslashes($obj->md_first_img)."',
                                md_photos = {$obj->md_photos},
                                md_albums = {$obj->md_albums},
                                md_temp = {$obj->md_temp},
                                md_thumb_uploaded = {$obj->md_thumb_uploaded},
                                md_external_id = {$obj->md_external_id}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 6 - Loyalty */
    private function addLevelData6DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_valid_fromDate = isset($obj->md_valid_from) ? "'".$obj->md_valid_from."'" : "null";

        $md_valid_toDate = isset($obj->md_valid_to) ? "'".$obj->md_valid_to."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data6 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_program_type = '{$obj->md_program_type}',
                                        md_stamp_type = '{$obj->md_stamp_type}',
                                        md_stamp_number = {$obj->md_stamp_number},
                                        md_stamp_entry = '".addslashes($obj->md_stamp_entry)."',
                                        md_stamp_labels = '".addslashes($obj->md_stamp_labels)."',
                                        md_reward_type = '{$obj->md_reward_type}',
                                        md_reward_number = {$obj->md_reward_number},
                                        md_reward_entry = '".addslashes($obj->md_reward_entry)."',
                                        md_reward_labels = '".addslashes($obj->md_reward_labels)."',
                                        md_valid_from = $md_valid_fromDate,
                                        md_valid_to = $md_valid_toDate,
                                        md_disclaimer = '".addslashes($obj->md_disclaimer)."',
                                        md_program_name = '".addslashes($obj->md_program_name)."',
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData6DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_valid_fromDate = isset($obj->md_valid_from) ? "'".$obj->md_valid_from."'" : "null";

        $md_valid_toDate = isset($obj->md_valid_to) ? "'".$obj->md_valid_to."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data6 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_program_type = '{$obj->md_program_type}',
                                md_stamp_type = '{$obj->md_stamp_type}',
                                md_stamp_number = {$obj->md_stamp_number},
                                md_stamp_entry = '".addslashes($obj->md_stamp_entry)."',
                                md_stamp_labels = '".addslashes($obj->md_stamp_labels)."',
                                md_reward_type = '{$obj->md_reward_type}',
                                md_reward_number = {$obj->md_reward_number},
                                md_reward_entry = '".addslashes($obj->md_reward_entry)."',
                                md_reward_labels = '".addslashes($obj->md_reward_labels)."',
                                md_valid_from = $md_valid_fromDate,
                                md_valid_to = $md_valid_toDate,
                                md_disclaimer = '".addslashes($obj->md_disclaimer)."',
                                md_program_name = '".addslashes($obj->md_program_name)."',
                                md_external_id = {$obj->md_external_id}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 9 - Product */
    private function addLevelData9DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data9 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_featured = {$obj->md_featured},
                                        md_has_discount = {$obj->md_has_discount},
                                        md_discount_type = '{$obj->md_discount_type}',
                                        md_discount_amount = {$obj->md_discount_amount},
                                        md_adjusted_price = {$obj->md_adjusted_price},
                                        md_original_price = {$obj->md_original_price},
                                        md_weight = {$obj->md_weight},
                                        md_external_id = {$obj->md_external_id},
                                        md_qty_instock = {$obj->md_qty_instock},
                                        md_safety_stock = {$obj->md_safety_stock}
         ");
        return $newId;
    }

    private function upateLevelData9DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data9 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_featured = {$obj->md_featured},
                                md_has_discount = {$obj->md_has_discount},
                                md_discount_type = '{$obj->md_discount_type}',
                                md_discount_amount = {$obj->md_discount_amount},
                                md_adjusted_price = {$obj->md_adjusted_price},
                                md_original_price = {$obj->md_original_price},
                                md_weight = {$obj->md_weight},
                                md_external_id = {$obj->md_external_id},
                                md_qty_instock = {$obj->md_qty_instock},
                                md_safety_stock = {$obj->md_safety_stock}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 999 - Product Category*/
    private function addLevelData999DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data999 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element}
         ");
        return $newId;
    }

    private function upateLevelData999DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data999 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 10 - Subscription */
    private function addLevelData10DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_usage_period_endDate = isset($obj->md_usage_period_end) ? "'".$obj->md_usage_period_end."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data10 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_deleted = {$obj->md_deleted},
                                        md_featured = {$obj->md_featured},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_billing_type = '{$obj->md_billing_type}',
                                        md_billing_period_unit = '{$obj->md_billing_period_unit}',
                                        md_billing_paymnet_count = {$obj->md_billing_paymnet_count},
                                        md_usage_period = '{$obj->md_usage_period}',
                                        md_usage_period_unit = '{$obj->md_usage_period_unit}',
                                        md_usage_period_number = {$obj->md_usage_period_number},
                                        md_usage_period_end = $md_usage_period_endDate,
                                        md_usage_rule_type = '{$obj->md_usage_rule_type}',
                                        md_usage_rule_capacity = {$obj->md_usage_rule_capacity},
                                        md_usage_rule_period_unit = '{$obj->md_usage_rule_period_unit}',
                                        md_color = '".addslashes($obj->md_color)."',
                                        md_can_cancel = {$obj->md_can_cancel}
         ");
        return $newId;
    }

    private function upateLevelData10DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_usage_period_startDate = isset($obj->md_usage_period_start) ? "'".$obj->md_usage_period_start."'" : "null";

        $md_usage_period_endDate = isset($obj->md_usage_period_end) ? "'".$obj->md_usage_period_end."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data10 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_deleted = {$obj->md_deleted},
                                md_featured = {$obj->md_featured},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_billing_type = '{$obj->md_billing_type}',
                                md_billing_period_unit = '{$obj->md_billing_period_unit}',
                                md_billing_paymnet_count = {$obj->md_billing_paymnet_count},
                                md_usage_period = '{$obj->md_usage_period}',
                                md_usage_period_unit = '{$obj->md_usage_period_unit}',
                                md_usage_period_number = {$obj->md_usage_period_number},
                                md_usage_period_start = $md_usage_period_startDate,
                                md_usage_period_end = $md_usage_period_endDate,
                                md_usage_rule_type = '{$obj->md_usage_rule_type}',
                                md_usage_rule_capacity = {$obj->md_usage_rule_capacity},
                                md_usage_rule_period_unit = '{$obj->md_usage_rule_period_unit}',
                                md_color = '".addslashes($obj->md_color)."',
                                md_can_cancel = {$obj->md_can_cancel}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 11 - Punch Pass */
    private function addLevelData11DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_usage_period_endDate = isset($obj->md_usage_period_end) ? "'".$obj->md_usage_period_end."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data11 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_deleted = {$obj->md_deleted},
                                        md_featured = {$obj->md_featured},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_usage_total_capacity = {$obj->md_usage_total_capacity},
                                        md_usage_period = '{$obj->md_usage_period}',
                                        md_usage_period_unit = '{$obj->md_usage_period_unit}',
                                        md_usage_period_number = {$obj->md_usage_period_number},
                                        md_usage_period_end = $md_usage_period_endDate,
                                        md_usage_rule_type = '{$obj->md_usage_rule_type}',
                                        md_usage_rule_capacity = {$obj->md_usage_rule_capacity},
                                        md_usage_rule_period_unit = '{$obj->md_usage_rule_period_unit}',
                                        md_color = '".addslashes($obj->md_color)."'
         ");
        return $newId;
    }

    private function upateLevelData11DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_usage_period_startDate = isset($obj->md_usage_period_start) ? "'".$obj->md_usage_period_start."'" : "null";

        $md_usage_period_endDate = isset($obj->md_usage_period_end) ? "'".$obj->md_usage_period_end."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data11 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_deleted = {$obj->md_deleted},
                                md_featured = {$obj->md_featured},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_usage_total_capacity = {$obj->md_usage_total_capacity},
                                md_usage_period = '{$obj->md_usage_period}',
                                md_usage_period_unit = '{$obj->md_usage_period_unit}',
                                md_usage_period_number = {$obj->md_usage_period_number},
                                md_usage_period_start = $md_usage_period_startDate,
                                md_usage_period_end = $md_usage_period_endDate,
                                md_usage_rule_type = '{$obj->md_usage_rule_type}',
                                md_usage_rule_capacity = {$obj->md_usage_rule_capacity},
                                md_usage_rule_period_unit = '{$obj->md_usage_rule_period_unit}',
                                md_color = '".addslashes($obj->md_color)."'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 12 - File Sharing */
    private function addLevelData12DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data12 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element}
         ");
        return $newId;
    }

    private function upateLevelData12DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data12 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 13 - Scratch Cards */
    private function addLevelData13DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_card_expireDate = isset($obj->md_card_expire) ? "'".$obj->md_card_expire."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data13 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_win_ratio = {$obj->md_win_ratio},
                                        md_prize_type = '{$obj->md_prize_type}',
                                        md_prize_id = {$obj->md_prize_id},
                                        md_prize_value = {$obj->md_prize_value},
                                        md_prize_text = '".addslashes($obj->md_prize_text)."',
                                        md_prize_amount = {$obj->md_prize_amount},
                                        md_prize_current_amount = {$obj->md_prize_current_amount},
                                        md_back_type = '{$obj->md_back_type}',
                                        md_back_data = '".addslashes($obj->md_back_data)."',
                                        md_card_expire = $md_card_expireDate,
                                        md_consolation = {$obj->md_consolation},
                                        md_consolation_text = '".addslashes($obj->md_consolation_text)."'
         ");
        return $newId;
    }

    private function upateLevelData13DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $md_card_expireDate = isset($obj->md_card_expire) ? "'".$obj->md_card_expire."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data13 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_win_ratio = {$obj->md_win_ratio},
                                md_prize_type = '{$obj->md_prize_type}',
                                md_prize_id = {$obj->md_prize_id},
                                md_prize_value = {$obj->md_prize_value},
                                md_prize_text = '".addslashes($obj->md_prize_text)."',
                                md_prize_amount = {$obj->md_prize_amount},
                                md_prize_current_amount = {$obj->md_prize_current_amount},
                                md_back_type = '{$obj->md_back_type}',
                                md_back_data = '".addslashes($obj->md_back_data)."',
                                md_card_expire = $md_card_expireDate,
                                md_consolation = {$obj->md_consolation},
                                md_consolation_text = '".addslashes($obj->md_consolation_text)."'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 19 - Social Kit */
    private function addLevelData19DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data19 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData19DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data19 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_id = {$obj->md_external_id}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 23 - Tutorials */
    private function addLevelData23DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data23 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_external_has_action = {$obj->md_external_has_action},
                                        md_external_action_title = '".addslashes($obj->md_external_action_title)."'
         ");
        return $newId;
    }

    private function upateLevelData23DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data23 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_external_has_action = {$obj->md_external_has_action},
                                md_external_action_title = '".addslashes($obj->md_external_action_title)."'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 24 - Video Sharing */
    private function addLevelData24DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data24 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_first_img = '".addslashes($obj->md_first_img)."',
                                        md_photos = {$obj->md_photos},
                                        md_albums = {$obj->md_albums},
                                        md_temp = {$obj->md_temp},
                                        md_external_id = {$obj->md_external_id},
                                        md_external_type = '{$obj->md_external_type}'
         ");
        return $newId;
    }

    private function upateLevelData24DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data24 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_first_img = '".addslashes($obj->md_first_img)."',
                                md_photos = {$obj->md_photos},
                                md_albums = {$obj->md_albums},
                                md_temp = {$obj->md_temp},
                                md_external_id = {$obj->md_external_id},
                                md_external_type = '{$obj->md_external_type}'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 25 - 2nd Show Room */
    private function addLevelData25DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data25 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_external_has_action = {$obj->md_external_has_action},
                                        md_external_action_title = '".addslashes($obj->md_external_action_title)."'
         ");
        return $newId;
    }

    private function upateLevelData25DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data25 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_external_has_action = {$obj->md_external_has_action},
                                md_external_action_title = '".addslashes($obj->md_external_action_title)."'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 26 - Coupons */
    private function addLevelData26DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data26 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_id = {$obj->md_external_id},
                                        md_item_type = '{$obj->md_item_type}'
         ");
        return $newId;
    }

    private function upateLevelData26DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data26 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_id = {$obj->md_external_id},
                                md_item_type = '{$obj->md_item_type}'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 27 - Blog */
    private function addLevelData27DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data27 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id},
                                        md_external_has_action = {$obj->md_external_has_action},
                                        md_external_action_title = '".addslashes($obj->md_external_action_title)."'
         ");
        return $newId;
    }

    private function upateLevelData27DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data27 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id},
                                md_external_has_action = {$obj->md_external_has_action},
                                md_external_action_title = '".addslashes($obj->md_external_action_title)."'
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 29 - Push */
    private function addLevelData29DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data29 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element}
         ");
        return $newId;
    }

    private function upateLevelData29DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data29 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 32 - Custom Form */
    private function addLevelData32DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data32 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_display_location = '{$obj->md_display_location}',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '".addslashes($obj->md_info1)."',
                                        md_info2 = '".addslashes($obj->md_info2)."',
                                        md_info3 = '".addslashes($obj->md_info3)."',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData32DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data32 SET 
                                md_row_id_desc = {$obj->md_row_id_desc},
                                md_biz_id = {$obj->md_biz_id},
                                md_mod_id = {$obj->md_mod_id},
                                md_level_no = {$obj->md_level_no},
                                md_parent = {$obj->md_parent},
                                md_index = {$obj->md_index},
                                md_element = {$obj->md_element},
                                md_editble = {$obj->md_editble},
                                md_required = {$obj->md_required},
                                md_dragble = {$obj->md_dragble},
                                md_content = {$obj->md_content},
                                md_icon = '".addslashes($obj->md_icon)."',
                                md_pic = '".addslashes($obj->md_pic)."',
                                md_head = '".addslashes($obj->md_head)."',
                                md_desc = '".addslashes($obj->md_desc)."',
                                md_display_location = '{$obj->md_display_location}',
                                md_loc = '".addslashes($obj->md_loc)."',
                                md_date = $md_dateDate,
                                md_info = '".addslashes($obj->md_info)."',
                                md_score = {$obj->md_score},
                                md_price = {$obj->md_price},
                                md_info1 = '".addslashes($obj->md_info1)."',
                                md_info2 = '".addslashes($obj->md_info2)."',
                                md_info3 = '".addslashes($obj->md_info3)."',
                                md_info4 = '".addslashes($obj->md_info4)."',
                                md_info5 = '".addslashes($obj->md_info5)."',
                                md_pic1 = '".addslashes($obj->md_pic1)."',
                                md_pic2 = '".addslashes($obj->md_pic2)."',
                                md_pic3 = '".addslashes($obj->md_pic3)."',
                                md_pic4 = '".addslashes($obj->md_pic4)."',
                                md_pic5 = '".addslashes($obj->md_pic5)."',
                                md_pic6 = '".addslashes($obj->md_pic6)."',
                                md_pic7 = '".addslashes($obj->md_pic7)."',
                                md_pic8 = '".addslashes($obj->md_pic8)."',
                                md_pic9 = '".addslashes($obj->md_pic9)."',
                                md_pic10 = '".addslashes($obj->md_pic10)."',
                                md_date1 = $md_date1Date,
                                md_date2 = $md_date2Date,
                                md_int1 = {$obj->md_int1},
                                md_int2 = {$obj->md_int2},
                                md_int3 = {$obj->md_int3},
                                md_bool1 = {$obj->md_bool1},
                                md_bool2 = {$obj->md_bool2},
                                md_biz_id_real = {$obj->md_biz_id_real},
                                md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                md_hidden = {$obj->md_hidden},
                                md_tap_like = {$obj->md_tap_like},
                                md_visible_element = {$obj->md_visible_element},
                                md_external_type = '{$obj->md_external_type}',
                                md_external_id = {$obj->md_external_id}
                                WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /* MOD 35 - Web View */
    private function addLevelData35DB(levelDataObject $obj){

        if (!isset($obj)){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_mod_data35 SET 
                                        md_row_id_desc = {$obj->md_row_id_desc},
                                        md_biz_id = {$obj->md_biz_id},
                                        md_mod_id = {$obj->md_mod_id},
                                        md_level_no = {$obj->md_level_no},
                                        md_parent = {$obj->md_parent},
                                        md_index = {$obj->md_index},
                                        md_element = {$obj->md_element},
                                        md_editble = {$obj->md_editble},
                                        md_required = {$obj->md_required},
                                        md_dragble = {$obj->md_dragble},
                                        md_content = {$obj->md_content},
                                        md_icon = '".addslashes($obj->md_icon)."',
                                        md_pic = '".addslashes($obj->md_pic)."',
                                        md_head = '".addslashes($obj->md_head)."',
                                        md_desc = '".addslashes($obj->md_desc)."',
                                        md_display_location = '{$obj->md_display_location}',
                                        md_loc = '".addslashes($obj->md_loc)."',
                                        md_info = '".addslashes($obj->md_info)."',
                                        md_score = {$obj->md_score},
                                        md_price = {$obj->md_price},
                                        md_info1 = '{$obj->md_info1}',
                                        md_info2 = '{$obj->md_info2}',
                                        md_info3 = '{$obj->md_info3}',
                                        md_info4 = '".addslashes($obj->md_info4)."',
                                        md_info5 = '".addslashes($obj->md_info5)."',
                                        md_pic1 = '".addslashes($obj->md_pic1)."',
                                        md_pic2 = '".addslashes($obj->md_pic2)."',
                                        md_pic3 = '".addslashes($obj->md_pic3)."',
                                        md_pic4 = '".addslashes($obj->md_pic4)."',
                                        md_pic5 = '".addslashes($obj->md_pic5)."',
                                        md_pic6 = '".addslashes($obj->md_pic6)."',
                                        md_pic7 = '".addslashes($obj->md_pic7)."',
                                        md_pic8 = '".addslashes($obj->md_pic8)."',
                                        md_pic9 = '".addslashes($obj->md_pic9)."',
                                        md_pic10 = '".addslashes($obj->md_pic10)."',
                                        md_date1 = $md_date1Date,
                                        md_date2 = $md_date2Date,
                                        md_int1 = {$obj->md_int1},
                                        md_int2 = {$obj->md_int2},
                                        md_int3 = {$obj->md_int3},
                                        md_bool1 = {$obj->md_bool1},
                                        md_bool2 = {$obj->md_bool2},
                                        md_biz_id_real = {$obj->md_biz_id_real},
                                        md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                                        md_hidden = {$obj->md_hidden},
                                        md_tap_like = {$obj->md_tap_like},
                                        md_visible_element = {$obj->md_visible_element},
                                        md_external_type = '{$obj->md_external_type}',
                                        md_external_id = {$obj->md_external_id}
         ");
        return $newId;
    }

    private function upateLevelData35DB(levelDataObject $obj){

        if (!isset($obj->md_row_id) || !is_numeric($obj->md_row_id) || $obj->md_row_id <= 0){
            throw new Exception("levelDataObject value must be provided");             
        }

        $md_dateDate = isset($obj->md_date) ? "'".$obj->md_date."'" : "null";

        $md_date1Date = isset($obj->md_date1) ? "'".$obj->md_date1."'" : "null";

        $md_date2Date = isset($obj->md_date2) ? "'".$obj->md_date2."'" : "null";

        $this->db->execute("UPDATE tbl_mod_data35 SET 
                            md_row_id_desc = {$obj->md_row_id_desc},
                            md_biz_id = {$obj->md_biz_id},
                            md_mod_id = {$obj->md_mod_id},
                            md_level_no = {$obj->md_level_no},
                            md_parent = {$obj->md_parent},
                            md_index = {$obj->md_index},
                            md_element = {$obj->md_element},
                            md_editble = {$obj->md_editble},
                            md_required = {$obj->md_required},
                            md_dragble = {$obj->md_dragble},
                            md_content = {$obj->md_content},
                            md_icon = '".addslashes($obj->md_icon)."',
                            md_pic = '".addslashes($obj->md_pic)."',
                            md_head = '".addslashes($obj->md_head)."',
                            md_desc = '".addslashes($obj->md_desc)."',
                            md_display_location = '{$obj->md_display_location}',
                            md_loc = '".addslashes($obj->md_loc)."',
                            md_date = $md_dateDate,
                            md_info = '".addslashes($obj->md_info)."',
                            md_score = {$obj->md_score},
                            md_price = {$obj->md_price},
                            md_info1 = '{$obj->md_info1}',
                            md_info2 = '{$obj->md_info2}',
                            md_info3 = '{$obj->md_info3}',
                            md_info4 = '".addslashes($obj->md_info4)."',
                            md_info5 = '".addslashes($obj->md_info5)."',
                            md_pic1 = '".addslashes($obj->md_pic1)."',
                            md_pic2 = '".addslashes($obj->md_pic2)."',
                            md_pic3 = '".addslashes($obj->md_pic3)."',
                            md_pic4 = '".addslashes($obj->md_pic4)."',
                            md_pic5 = '".addslashes($obj->md_pic5)."',
                            md_pic6 = '".addslashes($obj->md_pic6)."',
                            md_pic7 = '".addslashes($obj->md_pic7)."',
                            md_pic8 = '".addslashes($obj->md_pic8)."',
                            md_pic9 = '".addslashes($obj->md_pic9)."',
                            md_pic10 = '".addslashes($obj->md_pic10)."',
                            md_date1 = $md_date1Date,
                            md_date2 = $md_date2Date,
                            md_int1 = {$obj->md_int1},
                            md_int2 = {$obj->md_int2},
                            md_int3 = {$obj->md_int3},
                            md_bool1 = {$obj->md_bool1},
                            md_bool2 = {$obj->md_bool2},
                            md_biz_id_real = {$obj->md_biz_id_real},
                            md_facebook_id = '".addslashes($obj->md_facebook_id)."',
                            md_hidden = {$obj->md_hidden},
                            md_tap_like = {$obj->md_tap_like},
                            md_visible_element = {$obj->md_visible_element},
                            md_external_type = '{$obj->md_external_type}',
                            md_external_id = {$obj->md_external_id}
                            WHERE md_row_id = {$obj->md_row_id} 
         ");
    }

    /************************************* */
    /*   GENERAL - GET LEVEL DATA           */
    /************************************* */

    public function getBizLevelData(){

        switch ($this->mobileRequestObject->mod_id)
        {
            case 0:
                return $this->getMainMenuLevelData();
            case 3:
                return $this->getNewsLevelData();
            case 6:
                return $this->getLoyaltyLevelData();
            case 9:
               
                return $this->getProductLevelData();
            case 26:
                return $this->getCouponLevelData();
            case 10:
            case 11:
                return $this->getSubscriptionPunchPassLevelData();
            case 35:
                return $this->getWebBrowsersLevelData();
        	default:
                return $this->getDefaultLevelData();
        }
        
    }

    public function getShopDisplay(){

        $result = array();
        $responseFeaturedObjects = array();
        $responseDiscountObjects = array();
        try{

            $responseLevel = $this->getFeaturedProducts_DB();
            if(count($responseLevel) != 0){

                foreach ($responseLevel as $entry)
                {
                    $dataObject = levelData9Object::withData($entry);
                    array_push($responseFeaturedObjects,$dataObject);
                }
            }
            $result["featured"] = $responseFeaturedObjects;

            $responseLevel = $this->getDiscountProducts_DB();
            if(count($responseLevel) != 0){

                foreach ($responseLevel as $entry)
                {
                    $dataObject = levelData9Object::withData($entry);
                    array_push($responseDiscountObjects,$dataObject);
                }
            }
            $result["discounted"] = $responseDiscountObjects;

            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getSingleCouponLevelsData($parent,$level,$customerID = 0){

        return $this->getCouponsLevelData_singleCoupon_direct($parent,$level,$customerID);
    }

    public function getSingleLoyaltyLevelsData($parent,$level){
        return $this->getLoyaltyLevelData_singleCard_direct($parent,$level);
    }

    public function searchLevelDataForBizID($searchWord,$skip = 0,$limit = 5){
        try{
            $obj = "levelData{$this->modId}Object";

            $resultDataRows = $this->searchLevelDataForBizID_DB($searchWord,$skip,$limit);

            $results = array();
            foreach ($resultDataRows as $dataRow)
            {
            	$results[] = $obj::withData($dataRow);
            }
            
            return resultObject::withData(1,'',$results);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$searchWord);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   PRIVATE - GET LEVEL DATA           */
    /************************************* */

    private function getDefaultLevelData(){
        
        $responseObjects = array();

        $responseLevel = self::getBizLevelData_DB();

        try{

            $obj = "levelData{$this->mobileRequestObject->mod_id}Object";

            if(count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $dataObject = $obj::withData($row);
                    array_push($responseObjects,$dataObject);
                }
            }
            $result["rows"] = $responseObjects;
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getProductLevelData(){

        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;
        $parent = $this->mobileRequestObject->parent == "" ? "0" : $this->mobileRequestObject->parent;
        $isCategory = isset($this->mobileRequestObject->category) &&  $this->mobileRequestObject->category == "1" ? "1" : "0";

        try{

            if($level_no >= 1 && $parent > 0 && $isCategory == "0"){ 

                return $this->getProductLevelData_singleProduct();
                
            }else{
                
                return $this->getProductLevelData_filteredProducts();
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getProductLevelData_singleProduct(){

        $shopManager = new shopManager();
        $responseObjects = array();

        $mod_id = $this->mobileRequestObject->mod_id == '' || $this->mobileRequestObject->mod_id == 'null' ? 0 : $this->mobileRequestObject->mod_id;
        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;
        $parent = $this->mobileRequestObject->parent == "" ? "0" : $this->mobileRequestObject->parent;

        $allVariatopns = $shopManager->getAllVariationsForProduct($this->mobileRequestObject->parent);
        $responce["variants"] = $allVariatopns;

        $responseLevel = self::getBizSingleLevelDataById_DB($parent);
        $productRow = levelData9Object::withData($responseLevel); 

        //image
        $dataObject = levelData9Object::withData($responseLevel);
        $dataObject->md_row_id=$productRow->md_row_id;
        $dataObject->md_mod_id = $mod_id;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_image"; 
        $dataObject->biz_level_img["1"]= str_replace("paptap-thumbs","paptap",$productRow->md_icon);   
        $dataObject->in_sub = $productRow->in_sub;
        array_push($responseObjects,$dataObject);

        //title
        $dataObject = new levelData9Object();
        $dataObject->md_row_id=$productRow->md_row_id;
        $dataObject->md_mod_id = $mod_id;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_name"; 
        $dataObject->md_desc=stripslashes($productRow->md_head);  
        $dataObject->in_sub = $productRow->in_sub;
        array_push($responseObjects,$dataObject);

        //price
        $dataObject = new levelData9Object();
        $dataObject->md_row_id=$productRow->md_row_id;
        $dataObject->md_mod_id = $mod_id;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_price"; 
        $dataObject->md_price=$productRow->md_price;  
        $dataObject->md_original_price=$productRow->md_original_price;
        $dataObject->currency = $productRow->currency;
        $dataObject->in_sub = $productRow->in_sub;
        array_push($responseObjects,$dataObject);

        $price["price"] = $productRow->md_price;
        $price["original_price"] = $productRow->md_original_price;
        $price["currency"] = $productRow->currency;

        //slider
        if($productRow->md_pic1 != "" || $productRow->md_pic2 != "" || $productRow->md_pic3 != "" || $productRow->md_pic4 != "" || $productRow->md_pic5 != "" || $productRow->md_pic6 != "" || $productRow->md_pic7 != "" || $productRow->md_pic8 != "" || $productRow->md_pic9 != "" || $productRow->md_pic10 != ""){
            $dataObject = new levelData9Object();
            $dataObject->md_row_id=$productRow->md_row_id;
            $dataObject->md_mod_id = $mod_id;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type="Slider"; 
            $dataObject->biz_level_img["1"]=$productRow->md_pic;
            $dataObject->biz_level_img["2"]=$productRow->md_pic1;
            $dataObject->biz_level_img["3"]=$productRow->md_pic2;
            $dataObject->biz_level_img["4"]=$productRow->md_pic3;
            $dataObject->biz_level_img["5"]=$productRow->md_pic4;
            $dataObject->biz_level_img["6"]=$productRow->md_pic5;
            $dataObject->biz_level_img["7"]=$productRow->md_pic6;
            $dataObject->biz_level_img["8"]=$productRow->md_pic7;
            $dataObject->biz_level_img["9"]=$productRow->md_pic8;
            $dataObject->biz_level_img["10"]=$productRow->md_pic9;
            $dataObject->biz_level_img["11"]=$productRow->md_pic10; 
            $dataObject->in_sub = $productRow->in_sub;
            array_push($responseObjects,$dataObject);
        }

        //SKU
        $dataObject = new levelData9Object();
        $dataObject->md_row_id=$productRow->md_row_id;
        $dataObject->md_mod_id = $mod_id;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_sku"; 
        $dataObject->md_info=stripslashes($productRow->md_info1);       
        $dataObject->in_sub = $productRow->in_sub;
        array_push($responseObjects,$dataObject);

        // attributes
        $responseLevel = self::getBizLevelData_DB(2);
        if(count($responseLevel)>0){

            $nib_name = "";
            if($this->mobileRequestObject->android == "1"){
                $nib_name = "_android";
            }

            $attrIndex = 0;
            foreach ($responseLevel as $row)
            {
                $row["nib_name"] = $nib_name;
                $row["customer_server_id"] = $this->mobileRequestObject->server_customer_id;
                $row["variants"] = $allVariatopns;
                $row["attrPosition"] = $attrIndex;
                $dataObject = levelData9Object::withData($row);
                $attrIndex = $dataObject->attributePosition;
                array_push($responseObjects,$dataObject);
            }
        }

        //description
        $dataObject = new levelData9Object();
        $dataObject->md_row_id=$productRow->md_row_id;
        $dataObject->md_mod_id = $mod_id;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_description"; 
        $dataObject->md_info=stripslashes($productRow->md_desc); 
        $dataObject->in_sub = $productRow->in_sub;
        array_push($responseObjects,$dataObject);

        //add purchase button
        $dataObject = new levelData9Object();
        $dataObject->md_row_id=$productRow->md_row_id;
        $dataObject->md_mod_id = $mod_id;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="shop_button"; 
        $dataObject->md_head="Buy Now";  
        $dataObject->currency = $productRow->currency;
        $dataObject->md_index=99;
        
        array_push($responseObjects,$dataObject);

        $responce["rows"] = $responseObjects;
        $responce["price"] = $price;

        return resultObject::withData(1,"ok",$responce);
   
    }

    private function getProductLevelData_filteredProducts(){

        $response = array();
        $responseObjects = array();

        $parent = $this->mobileRequestObject->parent == "" ? "0" : $this->mobileRequestObject->parent;
        $prodId = (isset($_REQUEST["prodId"]) && $_REQUEST["prodId"] != 0 && $_REQUEST["prodId"] != "") ? $_REQUEST["prodId"] : "";
        $isCategory = isset($this->mobileRequestObject->category) &&  $this->mobileRequestObject->category == "1" ? "1" : "0";
    
        $qs = $_REQUEST["qs"];
        
        if($parent == "0" || $parent == "-1") 
            $filters['keyword'] = $qs;
        else
            $filters['category'] = $parent;
        
        if($prodId != ""){
            $filters['productId'] = $prodId;
        }

        $filters['itemsPerPage'] = isset($_REQUEST['itemsPerPage']) ? $_REQUEST['itemsPerPage'] : -1;
        $filters['existingItems'] = isset($_REQUEST['existingItems']) ? $_REQUEST['existingItems'] : 0;
        
        $responseLevel = $this->getFilteredProductsLevelData_DB($filters);
       
        if(count($responseLevel) != 0){// search/categor - not empty result

            foreach ($responseLevel as $entry)
            {
                $dataObject = levelData9Object::withData($entry);
                $dataObject->md_parent = -1;
                if($isCategory == 1){
                    $dataObject->md_parent = $parent;
                }
                array_push($responseObjects,$dataObject);
            }

            if(isset($this->mobileRequestObject->server_customer_id) && $this->mobileRequestObject->server_customer_id != "0" && $parent !=""){
                $levelDataManagerCategory = new levelDataManager("999");
                $categoryResult = $levelDataManagerCategory->getLevelDataByID($parent);
                if($categoryResult->code == 1 && $categoryResult->data->md_head != ""){

                    $customersModel = new customerModel();
                    $customerHistoryObj = new customerHistoryObject();
                    $customerHistoryObj->ch_cust_id = $this->mobileRequestObject->server_customer_id;
                    $customerHistoryObj->ch_status = "category";
                    $customerHistoryObj->ch_replace_text = $categoryResult->data->md_head;
                    $customersModel->addCustomerHistory($customerHistoryObj);
                }
            }
        }
        $response["rows"] = $responseObjects;

        return resultObject::withData(1,"ok",$response);

    }
    private function getFeaturedProducts_DB(){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $storeRow = $bizModel->getBizStoreSettings();

        $stockSQL = "";
        if($storeRow->data->ess_hide_out_of_stock == 1){
            $stockSQL = " AND md_qty_instock > 0";
        }

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data9 LEFT JOIN ecommerce_categories_products ecp ON md_row_id = ecp.product_id,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE md_featured = 1
                                    AND ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 9
                                    AND ms_level_no=1
                                    AND md_level_no=1
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    $stockSQL
                                    GROUP BY md_row_id
                                    ORDER BY md_index
                                    LIMIT 20");
    }
    private function getDiscountProducts_DB(){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $storeRow = $bizModel->getBizStoreSettings();

        $order = 'ORDER BY md_row_id DESC';
        switch($storeRow->data->ess_product_order){
            case 'newest_desc':
                $order = ' ORDER BY md_date DESC';
                break;
            case 'newest_asc':
                $order = ' ORDER BY md_date ASC';
                break;
            case 'abc_desc':
                $order = ' ORDER BY LOWER(md_head) DESC';
                break;
            case 'abc_asc':
                $order = ' ORDER BY LOWER(md_head) ASC';
                break;
            case 'price_desc':
                $order = ' ORDER BY md_price DESC';
                break;
            case 'price_asc':
                $order = ' ORDER BY md_price ASC';
                break;
            default:
                break;
        }   
        
        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $storeRow = $bizModel->getBizStoreSettings();

        $stockSQL = "";
        if($storeRow->data->ess_hide_out_of_stock == 1){
            $stockSQL = " AND md_qty_instock > 0";
        }
        

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data9 LEFT JOIN ecommerce_categories_products ecp ON md_row_id = ecp.product_id,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE md_original_price <> md_price
                                    AND ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 9
                                    AND ms_level_no=1
                                    AND md_level_no=1
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    $stockSQL
                                    GROUP BY md_row_id
                                    $order
                                    LIMIT 20");
    }

    private function getCouponLevelData(){

        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;

        try{

            if($level_no == 1){               
                return $this->getCouponsLevelData_list();
            }else{ 
                $couponDataResult = $this->getCouponsLevelData_singleCoupon();
                if($couponDataResult->code == 1){
                    $couponData = $couponDataResult->data['rows'][0];
                    $customersModel = new customerModel();
                    $customerHistoryObj = new customerHistoryObject();
                    $customerHistoryObj->ch_cust_id = $this->mobileRequestObject->server_customer_id;
                    $customerHistoryObj->ch_status = "coupon_view";
                    $customerHistoryObj->ch_replace_text = $couponData->md_head;
                    $customersModel->addCustomerHistory($customerHistoryObj);
                }
                return $couponDataResult;
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getCouponsLevelData_list(){

        $responseObjects = array();

        $responseLevel = self::getCouponsBizLevelData_DB();
        try{

            if(count($responseLevel)>0){
                $customerID = isset($this->mobileRequestObject->server_customer_id) && $this->mobileRequestObject->server_customer_id != "" ? $this->mobileRequestObject->server_customer_id : 0;
                foreach ($responseLevel as $row)
                {
                    $row['customer_id'] = $customerID;
                    $dataObject = levelData26Object::withData($row);
                    array_push($responseObjects,$dataObject);
                }
            }
            $result["rows"] = $responseObjects;
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }

    }

    private function getCouponsLevelData_singleCoupon(){
        $parent = $this->mobileRequestObject->parent == "" ? "0" : $this->mobileRequestObject->parent;
        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;
        $customerID = isset($this->mobileRequestObject->server_customer_id) && $this->mobileRequestObject->server_customer_id != "" ? $this->mobileRequestObject->server_customer_id : 0;
        return $this->getCouponsLevelData_singleCoupon_direct($parent,$level_no,$customerID);
    }

    private function getCouponsLevelData_singleCoupon_direct($parent,$level_no,$customerID = 0){
        $responseObjects = array();        
        $responseLevel = self::getSingleCouponBizLevelData_DB($parent);
        $responseLevel["md_level_no"] = $level_no;
        $responseLevel['customer_id'] = $customerID;
        $couponRow = levelData26Object::withData($responseLevel);

        //image
        $dataObject = levelData26Object::withData($responseLevel);
        $dataObject->md_row_id=$couponRow->md_row_id;
        $dataObject->cuponType =$couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="coupon_image"; 
        $dataObject->price = $couponRow->price;
        $dataObject->md_original_price = $couponRow->price;
        $dataObject->item_id = $couponRow->item_id;
        $dataObject->md_desc=stripslashes($couponRow->md_head);
        $dataObject->biz_level_img["1"]= str_replace("paptap-thumbs","paptap",$couponRow->md_icon);   
        array_push($responseObjects,$dataObject);

        //title
        $dataObject = new levelData26Object();
        $dataObject->md_row_id=$couponRow->md_row_id;
        $dataObject->cuponType =$couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_name"; 
        $dataObject->md_desc=stripslashes($couponRow->md_head);  
        array_push($responseObjects,$dataObject);

        //price
        $dataObject = new levelData26Object();
        $dataObject->md_row_id = $couponRow->md_row_id;
        $dataObject->cuponType = $couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type = "coupon_price"; 
        $dataObject->price = $couponRow->price; 
        $dataObject->md_price = $couponRow->price;
        $dataObject->md_original_price = $couponRow->price;
        $dataObject->currency = $couponRow->currency;
        array_push($responseObjects,$dataObject);

        //claimed
        $dataObject = new levelData26Object();
        $dataObject->md_row_id = $couponRow->md_row_id;
        $dataObject->cuponType = $couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type = "coupon_claimed"; 
        $dataObject->cuponClaimed = $couponRow->cuponClaimed;  
        array_push($responseObjects,$dataObject);

        //short description
        $dataObject = new levelData26Object();
        $dataObject->md_row_id = $couponRow->md_row_id;
        $dataObject->cuponType = $couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type = "product_description"; 
        $dataObject->label_key_android = "short_description";
        $dataObject->md_info = $couponRow->md_desc;  
        array_push($responseObjects,$dataObject);

        //time left
        $dataObject = new levelData26Object();
        $dataObject->md_row_id = $couponRow->md_row_id;
        $dataObject->cuponType = $couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type = "coupon_time_left"; 
        $dataObject->timeLeft = $couponRow->timeLeft; 
        $dataObject->md_info = $couponRow->md_date2;
        array_push($responseObjects,$dataObject);

        //valid at
        if($couponRow->md_info3 != "" || $couponRow->md_info4 != ""){
            $dataObject = new levelData26Object();
            $dataObject->md_row_id = $couponRow->md_row_id;
            $dataObject->cuponType = $couponRow->cuponType;
            $dataObject->md_mod_id = 26;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type = "product_description"; 
            $dataObject->label_key_android = "menu_label_118";
            $dataObject->label_key_ios = "118";
            $dataObject->md_info = $couponRow->md_info3." - ".$couponRow->md_info4; 
            array_push($responseObjects,$dataObject);
        }

        //disclaimer
        if($couponRow->md_info2 != ""){
            $dataObject = new levelData26Object();
            $dataObject->md_row_id = $couponRow->md_row_id;
            $dataObject->cuponType = $couponRow->cuponType;
            $dataObject->md_mod_id = 26;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type = "product_description"; 
            $dataObject->label_key_android = "menu_label_119";
            $dataObject->label_key_ios = "119";
            $dataObject->md_info = $couponRow->md_info2; 
            array_push($responseObjects,$dataObject);
        }

        //description
        if($couponRow->md_info != ""){
            $dataObject = new levelData26Object();
            $dataObject->md_row_id = $couponRow->md_row_id;
            $dataObject->cuponType = $couponRow->cuponType;
            $dataObject->md_mod_id = 26;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type = "product_description"; 
            $dataObject->md_info = $couponRow->md_info; 
            array_push($responseObjects,$dataObject);
        }

        //minimum cart total
        if($couponRow->min_cart_total > 0){
            $dataObject = new levelData26Object();
            $dataObject->md_row_id = $couponRow->md_row_id;
            $dataObject->cuponType = $couponRow->cuponType;
            $dataObject->md_mod_id = 26;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type = "coupon_minimum_cart"; 
            $dataObject->min_cart_total = $couponRow->min_cart_total; 
            array_push($responseObjects,$dataObject);
        }

        //terms and condition
        if($couponRow->md_info1 != ""){
            $dataObject = new levelData26Object();
            $dataObject->md_row_id = $couponRow->md_row_id;
            $dataObject->cuponType = $couponRow->cuponType;
            $dataObject->md_mod_id = 26;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type = "coupon_terms"; 
            $dataObject->md_info1 = $couponRow->md_info1; 
            array_push($responseObjects,$dataObject);
        }

        //button
        $dataObject = new levelData26Object();
        $dataObject->md_row_id = $couponRow->md_row_id;
        $dataObject->cuponType = $couponRow->cuponType;
        $dataObject->md_mod_id = 26;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type = "coupon_button"; 
        array_push($responseObjects,$dataObject);

        $responce["rows"] = $responseObjects;
        return resultObject::withData(1,"ok",$responce);
    }

    private function getLoyaltyLevelData(){

        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;

        try{

            if($level_no == 1){               
                return $this->getLoyaltyLevelData_list();
            }else{ 
                return $this->getLoyaltyLevelData_singleCard();
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getLoyaltyLevelData_list(){

        $responseObjects = array();

        $responseLevel = self::getLoyaltyBizLevelData_DB();
        try{

            if(count($responseLevel)>0){
                
                foreach ($responseLevel as $row)
                {
                    $cardObjectResult =  customerManager::getCustomersPunchCardbyCardID($row["customer_server_id"],$row["md_row_id"],$row["md_stamp_number"]);

                    if($row["md_visible_element"] == 1 || ($row["md_visible_element"] == 0 && $cardObjectResult->data->customer_card_stamp_no != 0)){
                        $row["cardObject"] = $cardObjectResult->data;
                        $dataObject = levelData6Object::withData($row);
                        array_push($responseObjects,$dataObject);
                    }
                }
            }
            $result["rows"] = $responseObjects;
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }

    }

    private function getLoyaltyLevelData_singleCard(){

        $parent = $this->mobileRequestObject->parent == "" ? "0" : $this->mobileRequestObject->parent;
        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;

        return $this->getLoyaltyLevelData_singleCard_direct($parent,$level_no);
    }

    private function getLoyaltyLevelData_singleCard_direct($parent,$level_no){

        $responseObjects = array();
        $responseLevel = self::getSingleLoyaltyBizLevelData_DB($parent);
        $responseLevel["md_level_no"] = $level_no;
        $cardObjectResult =  customerManager::getCustomersPunchCardbyCardID($responseLevel["customer_server_id"],$responseLevel["md_row_id"],$responseLevel["md_stamp_number"]);
        $responseLevel["cardObject"] = $cardObjectResult->data;
        $cardRow = levelData6Object::withData($responseLevel);

        //image
        $dataObject = levelData6Object::withData($responseLevel);
        $dataObject->md_row_id=$cardRow->md_row_id;
        $dataObject->md_program_type =$cardRow->md_program_type;
        $dataObject->md_stamp_number =$cardRow->md_stamp_number;
        $dataObject->md_reward_number =$cardRow->md_reward_number;
        $dataObject->md_mod_id = 6;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="coupon_image"; 
        $dataObject->md_desc =$cardRow->md_program_name;
        $dataObject->biz_level_img["1"]= str_replace("paptap-thumbs","paptap",$cardRow->md_pic);   
        array_push($responseObjects,$dataObject);

        //title
        $dataObject = new levelData6Object();
        $dataObject->md_row_id=$cardRow->md_row_id;
        $dataObject->md_desc =$cardRow->md_program_name;
        $dataObject->md_mod_id = 6;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="product_name";   
        array_push($responseObjects,$dataObject);

        //stamps
        $dataObject = new levelData6Object();
        $dataObject->md_row_id=$cardRow->md_row_id;
        $dataObject->md_stamp_number =$cardRow->md_stamp_number;
        $dataObject->customer_card_stamp_no =$cardRow->customer_card_stamp_no;
        $dataObject->md_program_type =$cardRow->md_program_type;
        $dataObject->md_program_name =$cardRow->md_program_name;
        $dataObject->md_stamp_number =$cardRow->md_stamp_number;
        $dataObject->md_reward_number =$cardRow->md_reward_number;
        $dataObject->md_mod_id = 6;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="loyalty_stamps";   
        array_push($responseObjects,$dataObject);

        //description
        $dataObject = new levelData6Object();
        $dataObject->md_row_id=$cardRow->md_row_id;
        $dataObject->md_info =$cardRow->md_desc;
        $dataObject->md_mod_id = 6;
        $dataObject->md_level_no = $level_no;
        $dataObject->md_parent = $parent;
        $dataObject->md_type="Paragraph"; 
        $dataObject->label_key_ios="120"; 
        $dataObject->label_key_android="menu_label_120";
        array_push($responseObjects,$dataObject);

        //description - stamps
        if($cardRow->stamp_items != ""){
            $dataObject = new levelData6Object();
            $dataObject->md_row_id=$cardRow->md_row_id;
            $dataObject->md_info =$cardRow->stamp_items;
            $dataObject->md_mod_id = 6;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type="Paragraph"; 
            $dataObject->label_key_ios="1402"; 
            $dataObject->label_key_android="stamp_items";
            array_push($responseObjects,$dataObject);
        }

        //description - rewards
        if($cardRow->reward_items != ""){
            $dataObject = new levelData6Object();
            $dataObject->md_row_id=$cardRow->md_row_id;
            $dataObject->md_info =$cardRow->reward_items;
            $dataObject->md_mod_id = 6;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type="Paragraph"; 
            $dataObject->label_key_ios="1403"; 
            $dataObject->label_key_android="reward_items";
            array_push($responseObjects,$dataObject);
        }

        //valid at
        if($cardRow->md_valid_from != "" || $cardRow->md_valid_to != ""){
            $dataObject = new levelData6Object();
            $dataObject->md_row_id=$cardRow->md_row_id;
            $dataObject->md_valid_from =$cardRow->md_valid_from;
            $dataObject->md_valid_to =$cardRow->md_valid_to;
            $dataObject->md_mod_id = 6;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type="loyalty_valid";   
            array_push($responseObjects,$dataObject);
        }

        //disclaimer
        if($cardRow->md_disclaimer != ""){
            $dataObject = new levelData6Object();
            $dataObject->md_row_id=$cardRow->md_row_id;
            $dataObject->md_info =$cardRow->md_disclaimer;
            $dataObject->md_mod_id = 6;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type="Paragraph";
            $dataObject->label_key_ios="119"; 
            $dataObject->label_key_android="menu_label_119";
            array_push($responseObjects,$dataObject);
        }

        $responce["rows"] = $responseObjects;
        return resultObject::withData(1,"ok",$responce);
    }

    private function getNewsLevelData(){
        
        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;
        $parent = $this->mobileRequestObject->parent == "" ? "0" : $this->mobileRequestObject->parent;

        $responseObjects = array();

        if($level_no==2){ // add parent image and header to inside the news data  
            
            $responseLevel = self::getBizSingleLevelDataById_DB($parent);
            $newsRow = levelData3Object::withData($responseLevel);

            if($newsRow->md_icon != ""){

                $dataObject = new levelData3Object();
                $dataObject->md_row_id=$newsRow->md_row_id;
                $dataObject->md_level_no = $level_no;
                $dataObject->md_parent = $parent;
                $dataObject->md_type="product_image"; 
                $dataObject->biz_level_img["1"]= str_replace("paptap-thumbs","paptap",$newsRow->md_icon);   
                array_push($responseObjects,$dataObject);
                
            }

            $dataObject = new levelData3Object();
            $dataObject->md_row_id=$newsRow->md_row_id;
            $dataObject->md_level_no = $level_no;
            $dataObject->md_parent = $parent;
            $dataObject->md_type="large_text"; 
            $dataObject->md_info = stripslashes($newsRow->md_head); 
            array_push($responseObjects,$dataObject);

            $responseLevel = self::getNewsBizLevelDataNoLimits_DB();
        }else{
            $skip = isset($_REQUEST["existingItems"]) ? $_REQUEST["existingItems"] : 0;
            $responseLevel = self::getNewsBizLevelData_DB($skip);
        }

        try{

            if(count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $dataObject = levelData3Object::withData($row);
                    array_push($responseObjects,$dataObject);
                }
            }
            $result["rows"] = $responseObjects;
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getSubscriptionPunchPassLevelData(){
        
        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;
        $obj = "levelData{$this->mobileRequestObject->mod_id}Object";

        $responseObjects = array();

        if ($level_no == 1){
            $subsResult = $this->getDefaultLevelData();
           
            $result = array();
            $result['rows'] = array();
            if($subsResult->code == 1){
                foreach ($subsResult->data['rows'] as $sub)
                {
                    $expired = false;
                    if($sub->md_usage_period == 'period' && isset($sub->md_usage_period_end)){
                        $expiresOn = strtotime($sub->md_usage_period_end);

                        $expired = time() > $expiresOn;
                    }
                	if(!$expired && $sub->md_deleted != 1){
                        $result['rows'][] = $sub;
                    }
                }
                
            }

            return resultObject::withData($subsResult->code,$subsResult->message,$result);
        }else{
            $responseLevel = self::getInnerLevelData_DB();
            try{

                if(count($responseLevel)>0){
                    foreach ($responseLevel as $row)
                    {
                        $row["md_level_no"] = $level_no;
                        $dataObject = $obj::withData($row);
                        array_push($responseObjects,$dataObject);
                    }
                }
                $result["rows"] = $responseObjects;
                return resultObject::withData(1,"ok",$result);
            }
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage());
                return resultObject::withData(0,$e->getMessage());
            }
        }
    }

    private function getWebBrowsersLevelData(){
        
        $responseObjects = array();
        $instance_id = isset($_REQUEST["prodId"]) && $_REQUEST["prodId"] != "" ? $_REQUEST["prodId"] : "0";

        $responseLevel = self::getWebBrowserLevelData_DB($instance_id);
        try{

            if(count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $dataObject = levelData35Object::withData($row);
                    array_push($responseObjects,$dataObject);
                }
            }
            $result["rows"] = $responseObjects;
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getMainMenuLevelData(){

        $structId = 0;

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $result = $bizModel->getBizModByBizIDAndModID($this->mobileRequestObject->bizid,0);

        if($result->code == 1){
            $structId = $result->data->biz_mod_stuct;
        }
        $menuHasElements = $bizModel->mainMenuHasElementsByStructure($structId);
      
        if($menuHasElements){
            switch($structId){
                case 111:
                case 113:
                case 115:
                case 116:
                    return $this->getMainMenuLevelData_elements($structId);
                case 120:     
                    $response = $this->getMainMenuLevelData_subscription($structId);
                    $resultSubscription["rows"] = $response->data;
                    return resultObject::withData(1,"ok",$resultSubscription);
                default: //112-114
                    return $this->getMainMenuLevelData_default($structId);
            }
        }else{
            return $this->getDefaultLevelData();
        }
    }
    private function getMainMenuLevelData_default($structId){

        $responseObjects = array();
        $level_no = $this->mobileRequestObject->level_no == "" ? "1" : $this->mobileRequestObject->level_no;

        $responseLevel = $this->getLevelDataByStructure_DB($structId);
        
        try{
            if(count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $row['md_level_no'] = $level_no;
                    $row['struct_type'] = "default";
                    $row['customer_id'] = $this->mobileRequestObject->server_customer_id;
                    $dataObject = levelData0Object::withData($row);
                    array_push($responseObjects,$dataObject);
                }
            }
            $result["rows"] = $responseObjects;
            $result["extra_data"] = self::getConectedLevelDataServiceEmployeeButton($structId,$this->mobileRequestObject->bizid);
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }
    private function getMainMenuLevelData_subscription($structId){

        $responseObjects = array();

        $responseLevel = self::getMainMenuSelectedSubscriptions_DB($structId);
        try{
            if(count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $dataObject = levelData10Object::withData($row);
                    $dataObject->md_mod_id = 0;
                    array_push($responseObjects,$dataObject);
                }
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }

        $responseLevel = self::getMainMenuSelectedPunchPasses_DB($structId);
        try{
            if(count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $dataObject = levelData11Object::withData($row);
                    $dataObject->md_mod_id = 0;
                    array_push($responseObjects,$dataObject);
                }
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }

        return resultObject::withData(1,"ok",$responseObjects);
    }
    private function getMainMenuLevelData_elements($structId){

        $responseObjects = array();
        $responseLevel = array();

        $connectedType = $this->getMainMenuConnectedType_DB($structId);
        switch($connectedType){
            case "product":
                $responseLevel = $this->getSelectedProducts_MainMenu($connectedType,$structId);
                break;
            case "coupon":
                $responseLevel = $this->getSelectedCoupons_MainMenu($connectedType,$structId);
                break;
            case "pointshop":
                $responseLevel = $this->getSelectedPoints_MainMenu($connectedType,$structId);
                break;
            case "class":
                $responseLevel = $this->getSelectedClassDates_MainMenu($connectedType,$structId);
                break;
            case "service":
                $responseLevel = $this->getSelectedServiceEmployee_MainMenu($connectedType,$structId);
                break;
            case 'sub_punch':
                $resultSubscription = $this->getMainMenuLevelData_subscription($structId);
                if($resultSubscription->code == 1){
                    $responseObjects = $resultSubscription->data;
                }
                break;
            default:
                $responseLevel = $this->getDefaultLevelData();
        }

        try{
            if($connectedType != "sub_punch" && count($responseLevel)>0){
                foreach ($responseLevel as $row)
                {
                    $row['customer_id'] = $this->mobileRequestObject->server_customer_id;
                    $dataObject = levelData0Object::withData($row);
                    array_push($responseObjects,$dataObject);
                }
            }
            $result["rows"] = $responseObjects;
            $result["membership"] = "Get the data from client object";
            return resultObject::withData(1,"ok",$result);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    // DB functions
    private function getBizLevelData_DB($pLevel = 0){
        $levelNumber = $pLevel == 0 ? $this->mobileRequestObject->level_no : $pLevel;
        $mainModId = $this->mobileRequestObject->mod_id ;
        $orderBy = "md_index" ;
        if($this->mobileRequestObject->mod_id == 999){
            $mainModId = 9;            
        }        

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data{$this->mobileRequestObject->mod_id}
                                    LEFT JOIN (SELECT COUNT(md_row_id) cnt,md_parent innerpar, MAX(md_info) link FROM tbl_mod_data{$this->mobileRequestObject->mod_id} WHERE md_biz_id={$this->mobileRequestObject->bizid} GROUP BY md_parent) intbl ON md_row_id = innerpar,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = $mainModId
                                    AND ms_level_no=1
                                    AND md_level_no = $levelNumber
                                    AND md_parent = {$this->mobileRequestObject->parent}
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    ORDER BY $orderBy");
    }

    private function searchLevelDataForBizID_DB($searchWord,$skip = 0,$take = 5){
        $limitSQL = "";
        if($take > 0){
            $limitSQL = " LIMIT $skip,$take";
        }

        $mainModId = $this->mobileRequestObject->mod_id ;
        $orderBy = "md_index" ;
        if($this->mobileRequestObject->mod_id == 999){
            $mainModId = 9;            
        }

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data{$this->mobileRequestObject->mod_id}
                                    LEFT JOIN (SELECT COUNT(md_row_id) cnt,md_parent innerpar, MAX(md_info) link FROM tbl_mod_data{$this->mobileRequestObject->mod_id} WHERE md_biz_id={$this->mobileRequestObject->bizid} GROUP BY md_parent) intbl ON md_row_id = innerpar,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = $mainModId
                                    AND ms_level_no=1
                                    AND md_level_no = 1
                                    AND md_parent = 0
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    AND (md_head LIKE '%$searchWord%' OR md_desc LIKE '%$searchWord%')
                                    ORDER BY $orderBy
                                    $limitSQL");
    }

    private function getNewsBizLevelData_DB($skip){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data3
                                    LEFT JOIN (SELECT COUNT(md_row_id) cnt,md_parent innerpar, MAX(md_info) link FROM tbl_mod_data3 WHERE md_biz_id={$this->mobileRequestObject->bizid} GROUP BY md_parent) intbl ON md_row_id = innerpar,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 3
                                    AND ms_level_no=1
                                    AND md_level_no = {$this->mobileRequestObject->level_no}
                                    AND md_parent = {$this->mobileRequestObject->parent}
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    ORDER BY md_date desc, md_row_id desc
                                    LIMIT $skip, 10");
    }
    private function getNewsBizLevelDataNoLimits_DB(){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data3
                                    LEFT JOIN (SELECT COUNT(md_row_id) cnt,md_parent innerpar, MAX(md_info) link FROM tbl_mod_data3 WHERE md_biz_id={$this->mobileRequestObject->bizid} GROUP BY md_parent) intbl ON md_row_id = innerpar,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 3
                                    AND ms_level_no=1
                                    AND md_level_no = {$this->mobileRequestObject->level_no}
                                    AND md_parent = {$this->mobileRequestObject->parent}
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    ORDER BY md_index");
    }

    private function getCouponsBizLevelData_DB(){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $redeem_code = isset($_REQUEST['redeem_code']) ? $_REQUEST['redeem_code'] : "";

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id, '$redeem_code' redeem_code 
                                    FROM tbl_mod_struct,tbl_biz_mod,tbl_mod_elements,tbl_mod_data26 LEFT JOIN tbl_items ON ite_md_id = md_row_id AND ite_mode_id=26 AND ite_active=1
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 26
                                    AND ms_level_no=1
                                    AND md_level_no = {$this->mobileRequestObject->level_no}
                                    AND md_parent = {$this->mobileRequestObject->parent}
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    AND md_date2 >= now()
                                    ORDER BY md_index");
    }

    private function getSingleCouponBizLevelData_DB($rowId){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $redeem_code = isset($_REQUEST['redeem_code']) ? $_REQUEST['redeem_code'] : "";

        return $this->db->getRow("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id, '$redeem_code' redeem_code 
                                    FROM tbl_mod_struct,tbl_biz_mod,tbl_mod_elements,tbl_mod_data26 LEFT JOIN tbl_items ON ite_md_id = md_row_id AND ite_mode_id=26 AND ite_active=1
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_mod_id = 26
                                    AND ms_level_no=1
                                    AND md_row_id = $rowId
                                    ORDER BY md_index");
    }

    private function getLoyaltyBizLevelData_DB(){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id
                                    FROM tbl_mod_struct,tbl_biz_mod,tbl_mod_data6
                                    WHERE ms_mod_id=6 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 6
                                    AND ms_level_no=1
                                    AND md_level_no = {$this->mobileRequestObject->level_no}
                                    AND md_parent = {$this->mobileRequestObject->parent}
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND if(md_valid_to is not null,md_valid_to >= now(), 1=1)
                                    ORDER BY md_index");
    }

    private function getSingleLoyaltyBizLevelData_DB($rowId){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getRow("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id
                                    FROM tbl_mod_struct,tbl_biz_mod,tbl_mod_data6
                                    WHERE ms_mod_id=6 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND biz_mod_mod_id = 6
                                    AND md_row_id = $rowId
                                    ORDER BY md_index");
    }

    private function getBizSingleLevelDataById_DB($rowId){

        $mainModId = $this->mobileRequestObject->mod_id ;
        $orderBy = "md_index" ;
        if($this->mobileRequestObject->mod_id == 999){
            $mainModId = 9;            
        }

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getRow("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id FROM tbl_mod_data{$this->mobileRequestObject->mod_id},tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = $mainModId
                                    AND ms_level_no=1
                                    AND md_row_id = $rowId
                                    ORDER BY $orderBy");
    }

    private function getFilteredProductsLevelData_DB($filter){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $storeRow = $bizModel->getBizStoreSettings();

        $stockSQL = "";
        if($storeRow->data->ess_hide_out_of_stock == 1){
            $stockSQL = " AND md_qty_instock > 0";
        }

        $where = "1 = 1";
        if(isset($filter['category'])) {
            if(is_array($filter['category'])){
                $categories = $filter['category'];
            }
            else{
                $categories = explode(",",$filter['category']);
            }
            $group = '';
            foreach ($categories as $category)
            {
                if($group != ''){
                    $group .= ',';
                }
            	$group .= intval($category);
            }

            $where .= ' AND ecp.category_id IN (' . $group .')';
            $order = 'ORDER BY ecp.product_order';
        } else {
            
            

            $order = 'ORDER BY md_row_id DESC';
            switch($storeRow->data->ess_product_order){
                case 'newest_desc':
                    $order = ' ORDER BY md_date DESC';
                    break;
                case 'newest_asc':
                    $order = ' ORDER BY md_date ASC';
                    break;
                case 'abc_desc':
                    $order = ' ORDER BY LOWER(md_head) DESC';
                    break;
                case 'abc_asc':
                    $order = ' ORDER BY LOWER(md_head) ASC';
                    break;
                case 'price_desc':
                    $order = ' ORDER BY md_original_price DESC';
                    break;
                case 'price_asc':
                    $order = ' ORDER BY md_original_price ASC';
                    break;
                default:
                    break;
            }           
        }

        if(isset($filter['productId'])){
            $where .= ' AND md_row_id = '.$filter['productId'];
        }

        if(isset($filter['keyword'])) {


            $searchString = strtolower(addslashes($filter['keyword']));

            $where .= " AND (
                                LOWER(md_head) LIKE '%$searchString%'
                                OR
                                LOWER(md_desc) LIKE '%$searchString%'
                                OR
                                LOWER(md_info1) LIKE '%$searchString%'
                                ";

            $where .= " ) ";


        }

        if (isset($filter['linked_category'])) {
            $order = 'ORDER BY ecp.category_id != ' . $filter['linked_category'] . ' OR ISNULL(ecp.category_id), ecp.product_order';
        } else {
            $filter['linked_category'] = 0;
        }


        $itemsPer = 8;
        $limit = "";
        if(isset($filter['itemsPerPage']) && $filter['itemsPerPage'] != ''){
            $itemsPer = $filter['itemsPerPage'];
        }
        if(!isset($filter['existingItems'])){
            $filter['existingItems'] = 0;
        }
        if ($filter['existingItems'] == 0) {
            if($itemsPer != -1){
                $limit = 'LIMIT '.$itemsPer;
            }
        } else {
            $limit = ' LIMIT ' . (($filter['existingItems'])) . ',' . $itemsPer;
        }
     
        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data9 LEFT JOIN ecommerce_categories_products ecp ON md_row_id = ecp.product_id,
                                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE $where
                                    AND ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_active=1
                                    AND biz_mod_mod_id = 9
                                    AND ms_level_no=1
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_visible_element=1
                                    AND md_element = 23
                                    $stockSQL
                                    GROUP BY md_row_id
                                    $order
                                    $limit");

    }

    private function getInnerLevelData_DB(){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $levelsSQL =  "select *,'$nib_name' nib_name,'$server_customer_id' customer_server_id
                    from tbl_mod_data{$this->mobileRequestObject->mod_id} main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data{$this->mobileRequestObject->mod_id} where md_biz_id={$this->mobileRequestObject->bizid} group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements
					where biz_mod_active=1 
                    and md_biz_id={$this->mobileRequestObject->bizid} 
                    and me_id=md_element 
					and md_biz_id = biz_mod_biz_id 
                    and biz_mod_mod_id = {$this->mobileRequestObject->mod_id}
					and ms_mod_id=md_mod_id 
                    and ms_level_no={$this->mobileRequestObject->level_no}
                    and (md_row_id = {$this->mobileRequestObject->parent} OR md_parent = {$this->mobileRequestObject->parent})
					and ms_template=biz_mod_stuct 
                    and md_visible_element=1
					order by md_level_no";
        
        return $this->db->getTable( $levelsSQL );
    }

    private function getWebBrowserLevelData_DB($instance_id){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $levelsSQL =  "select *,'$nib_name' nib_name,'$server_customer_id' customer_server_id
                    from tbl_mod_data{$this->mobileRequestObject->mod_id} main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data{$this->mobileRequestObject->mod_id} where md_biz_id={$this->mobileRequestObject->bizid} group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements
					where biz_mod_active=1 
                    and md_biz_id={$this->mobileRequestObject->bizid} 
                    and me_id=md_element 
					and md_biz_id = biz_mod_biz_id 
                    and biz_mod_mod_id = 0
					and ms_mod_id=35 
                    and ms_level_no={$this->mobileRequestObject->level_no}
                    and md_external_id = $instance_id
                    and md_visible_element=1
					order by md_index";

        return $this->db->getTable( $levelsSQL );
    }

    private function getLevelDataByStructure_DB($structure){

        $mainModId = $this->mobileRequestObject->mod_id ;
        $orderBy = "md_index" ;

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *,'$nib_name' nib_name,'$server_customer_id' customer_server_id 
                                    FROM tbl_mod_data$mainModId,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements 
                                    WHERE ms_mod_id=md_mod_id 
                                    AND ms_template=biz_mod_stuct
                                    AND md_biz_id = biz_mod_biz_id
                                    AND me_id=md_element
                                    AND biz_mod_mod_id = $mainModId
                                    AND md_biz_id = {$this->mobileRequestObject->bizid}
                                    AND md_belong_to_structure=$structure
                                    ORDER BY $orderBy");
    }

    private function getMainMenuSelectedSubscriptions_DB($struct_id){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $sql = "SELECT '$nib_name' nib_name,'$server_customer_id' customer_server_id, subs.*,tbl_mod_elements.*,tbl_mod_struct.*,base.md_index as md_index,base.md_info as base_info,base.md_pic as base_pic FROM tbl_mod_data10 as subs 
                    INNER JOIN tbl_mod_data0 as base ON base.md_external_type = 'subscription' AND subs.md_row_id = base.md_external_id
                    INNER JOIN tbl_mod_elements ON subs.md_element = me_id
                    INNER JOIN tbl_mod_struct ON ms_mod_id = subs.md_mod_id
                WHERE base.md_biz_id = {$this->mobileRequestObject->bizid}
                AND base.md_belong_to_structure = $struct_id
                AND (subs.md_usage_period <> 'period' OR (md_usage_period_end IS NOT NULL AND md_usage_period_end > NOW()))
                AND subs.md_deleted = 0
                AND subs.md_visible_element = 1
                GROUP BY subs.md_row_id";

        $rows = $this->db->getTable($sql);

        if($struct_id == 116){
            foreach ($rows as $key => $entry)
            {
            	$rows[$key]['md_info'] = $entry['base_info'];
                $rows[$key]['md_icon'] = $entry['base_pic'];
            }
            
        }

        return $rows;
    }

    private function getMainMenuSelectedPunchPasses_DB($struct_id){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $sql = "SELECT '$nib_name' nib_name,'$server_customer_id' customer_server_id, subs.*,tbl_mod_elements.*,tbl_mod_struct.*,base.md_index as md_index,base.md_info as base_info,base.md_pic as base_pic FROM tbl_mod_data11 as subs 
                    INNER JOIN tbl_mod_data0 as base ON base.md_external_type = 'punch_pass' AND subs.md_row_id = base.md_external_id
                    INNER JOIN tbl_mod_elements ON subs.md_element = me_id
                    INNER JOIN tbl_mod_struct ON ms_mod_id = subs.md_mod_id
                WHERE base.md_biz_id = {$this->mobileRequestObject->bizid}
                AND base.md_belong_to_structure = $struct_id
                AND (subs.md_usage_period <> 'period' OR (md_usage_period_end IS NOT NULL AND md_usage_period_end > NOW()))
                AND subs.md_deleted = 0
                AND subs.md_visible_element = 1
                GROUP BY subs.md_row_id";

        $rows = $this->db->getTable($sql);

        if($struct_id == 116){
            foreach ($rows as $key => $entry)
            {
            	$rows[$key]['md_info'] = $entry['base_info'];
                $rows[$key]['md_icon'] = $entry['base_pic'];
            }
            
        }

        return $rows;
    }

    private function getMainMenuConnectedType_DB($struct_id){

        return $this->db->getVal("SELECT md_external_type 
                                                FROM tbl_mod_data0 
                                                WHERE md_biz_id={$this->mobileRequestObject->bizid}
                                                AND md_level_no=1 
                                                AND md_belong_to_structure=$struct_id");
    }

    private function getSelectedProducts_MainMenu($externalType,$structure){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $bizModel = new bizModel($this->mobileRequestObject->bizid);
        $storeRow = $bizModel->getBizStoreSettings();

        $stockSQL = "";
        if($storeRow->data->ess_hide_out_of_stock == 1){
            $stockSQL = " AND md_qty_instock > 0";
        }

        $levelsSQL =  "select *, '$nib_name' nib_name,'$server_customer_id' customer_server_id, '$structure' structure_id
                    from tbl_mod_data9 main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data9 where md_biz_id={$this->mobileRequestObject->bizid} group by md_parent) intbl
					on main.md_row_id = intbl.innerpar,tbl_mod_struct,tbl_biz_mod,tbl_mod_elements,
                    (select md_pic as modPic, md_external_type,md_external_id,md_external_sub_id,md_info as ext_md_info,md_index as true_index from tbl_mod_data0 where md_level_no=2 and md_biz_id={$this->mobileRequestObject->bizid} and md_external_type='$externalType' and md_belong_to_structure=$structure) tb1
					where md_row_id= tb1.md_external_id
                    and biz_mod_active=1 
                    and md_biz_id={$this->mobileRequestObject->bizid} 
                    and me_id=md_element 
					and md_biz_id = biz_mod_biz_id 
                    and biz_mod_mod_id = 0
					and ms_mod_id=9 
                    and ms_level_no=1 
                    and md_visible_element=1
                    $stockSQL
					order by true_index ASC";    

        return $this->db->getTable( $levelsSQL );
    }

    private function getSelectedCoupons_MainMenu($externalType,$structure){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $levelsSQL =  "select *, '$nib_name' nib_name,'$server_customer_id' customer_server_id, '$structure' structure_id
                    from tbl_mod_data26 main 
					LEFT JOIN (select count(md_row_id) cnt,md_parent innerpar, max(md_info) link from tbl_mod_data26 where md_biz_id={$this->mobileRequestObject->bizid} group by md_parent) intbl on main.md_row_id = intbl.innerpar
                    LEFT JOIN tbl_items on ite_md_id = md_row_id  and ite_mode_id=26 and ite_active=1,
                    tbl_mod_struct,tbl_biz_mod,tbl_mod_elements,
                    (select md_external_type,md_external_id,md_external_sub_id,md_info as base_info,md_pic as base_pic from tbl_mod_data0 where md_level_no=2 and md_biz_id={$this->mobileRequestObject->bizid} and md_external_type='$externalType' and md_belong_to_structure=$structure) tb1
					where md_row_id=tb1.md_external_id
                    and biz_mod_active=1 
                    and md_biz_id={$this->mobileRequestObject->bizid} 
                    and me_id=md_element 
					and md_biz_id = biz_mod_biz_id 
                    and biz_mod_mod_id = 0
					and ms_mod_id=26 
                    and ms_level_no=1 
                    and md_visible_element=1
                    and md_date2 >= now()
					order by md_index";
        return $this->db->getTable( $levelsSQL );
    }

    private function getSelectedPoints_MainMenu($externalType,$structure){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $levelsSQL =  "SELECT *, '$nib_name' nib_name,'$server_customer_id' customer_server_id, '$structure' structure_id FROM 
                                (SELECT tbl_points_grant.*,md_head as label, pg_text as description FROM tbl_points_grant INNER JOIN tbl_mod_data9 ON pg_entity_id = md_row_id AND pg_biz_id = {$this->mobileRequestObject->bizid} AND pg_entity_type = 'product'
                                UNION
                                SELECT tbl_points_grant.*,bmt_name as label, pg_text as description FROM tbl_points_grant INNER JOIN tbl_biz_cal_meet_type ON pg_entity_id = bmt_id AND pg_biz_id = {$this->mobileRequestObject->bizid} AND pg_entity_type = 'service'
                                UNION
                                SELECT tbl_points_grant.*,pg_title as label, pg_text as description FROM tbl_points_grant WHERE pg_biz_id = {$this->mobileRequestObject->bizid} AND pg_entity_type = 'other' ) as s,
                                (select * from tbl_mod_data0 where md_level_no=2 and md_biz_id={$this->mobileRequestObject->bizid} and md_external_type='$externalType' and md_belong_to_structure=$structure) tb1
					            where pg_id=md_external_id
                            ORDER BY md_index";

        return $this->db->getTable( $levelsSQL );
    }

    private function getSelectedClassDates_MainMenu($externalType,$structure){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        $dbTable = $this->db->getTable("select * 
                                        from tbl_calendar_classes,tbl_calendar_class_employee,tbl_employee,
                                        (SELECT ccd_class_id,min(concat(ccd_start_date,' ',hour_start)) as myDate 
                                        FROM tbl_calendar_class_dates,calendar_hours 
                                        where ccd_start_time=hour_id 
                                        and concat(ccd_start_date,' ',hour_start) > now()
                                        and ccd_biz_id={$this->mobileRequestObject->bizid}
                                        group by ccd_class_id) t1,
                                        (select md_row_id rowId,md_external_id ex_id, md_index,md_info base_info,md_pic base_pic from tbl_mod_data0 where md_level_no=2 and md_biz_id={$this->mobileRequestObject->bizid} and md_external_type='$externalType' and md_belong_to_structure=$structure) t2
                                        where ccd_class_id=calc_id 
                                        and calc_id=ex_id
                                        and calc_id=ce_class_id
                                        and be_id=ce_employee_id
                                        and calc_isvisible = 1
                                        and calc_isdeleted = 0
                                        order by md_index
                                ");
        
        $responce = array();
        if(count($dbTable) > 0){
            
            foreach ($dbTable as $oneClass) {
                $duration = $oneClass["calc_duration"];
                $durationUnits = $oneClass["calc_duration_size"];
                $price = 0;
                if($oneClass["calc_service"] != "0"){
                    $serviceRow = $this->db->getRow("select bmt_size,bmt_duration,bmt_price from tbl_biz_cal_meet_type where bmt_id = {$oneClass["calc_service"]}");
                    $duration = $serviceRow["bmt_duration"];
                    $durationUnits = $serviceRow["bmt_size"];
                    $price = $serviceRow["bmt_price"];
                }else{
                    $price = $oneClass["calc_price"];
                }

                $sql = "SELECT calc_max_cust FROM tbl_calendar_classes WHERE calc_id = {$oneClass["calc_id"]}";
                $maxCust = $this->db->getVal($sql);

                $dbTableDates = $this->db->getTable("select ccd_start_date,ccd_end_date, ccd_id, startTable.hour_start startDate,endTable.hour_end endDate
                                        from tbl_calendar_class_dates
                                        LEFT JOIN (
                                            SELECT cdc_class_date_id as date_id,COUNT(*) as custCount FROM tbl_class_date_customers
                                            WHERE cdc_class_id = {$oneClass["calc_id"]}
                                            GROUP BY cdc_class_date_id
                                        ) s ON ccd_id = date_id,
                                        (select * from calendar_hours) startTable,
                                        (select * from calendar_hours) endTable
                                        where ccd_class_id={$oneClass["calc_id"]}
                                        and startTable.hour_id = ccd_start_time
                                        and endTable.hour_id = ccd_end_time
                                        and concat(ccd_start_date,' ',startTable.hour_start) > now()
                                        and ccd_iscanceled = 0
                                        and ccd_isvisible = 1
                                        AND (custCount IS NULL OR custCount < $maxCust)
                                        order by ccd_start_date,ccd_start_time
                                        limit 3
                                ");

                if(count($dbTableDates) > 0){   
                    
                    foreach ($dbTableDates as $oneDate) {
                        $dataObject = array();
                        $booked = $this->db->getVal("select count(cdc_id) 
                                                    from tbl_class_date_customers 
                                                    where cdc_biz_id={$this->mobileRequestObject->bizid}
                                                    and cdc_class_id={$oneClass["calc_id"]} 
                                                    and cdc_class_date_id={$oneDate["ccd_id"]}");
                        
                        $startStamp = strtotime( $oneDate["ccd_start_date"]." UTC");
                        $endStamp = strtotime( $oneDate["ccd_end_date"]." UTC");
                        $startTimeStamp = strtotime( $oneDate["ccd_start_date"] . " " . $oneDate["startDate"].":00 UTC");
                        $endTimeStamp = strtotime( $oneDate["ccd_end_date"] . " " . $oneDate["endDate"].":00 UTC");

                        $dataObject['recordID']=$oneDate["ccd_id"];
                        $dataObject['class_id']=$oneClass["calc_id"];
                        $dataObject['class_name']=$oneClass["calc_name"];
                        $dataObject['class_description']=$oneClass["calc_desc"];
                        $dataObject['class_pic']=isset($oneClass["base_pic"]) ? $oneClass["base_pic"] : '';
                        $dataObject['class_info']=isset($oneClass["base_info"]) ? $oneClass["base_info"] : '';
                        $dataObject['class_total_positions']=$oneClass["calc_max_cust"];
                        $dataObject['based_on_service']=$oneClass["calc_duration_by_service"];
                        $dataObject['service_id']=$oneClass["calc_service"];
                        $dataObject['service_price']=$price;
                        $dataObject['class_duration']=$duration;
                        $dataObject['duration_units']=$durationUnits;
                        $dataObject['employee_id']=$oneClass["ce_employee_id"];
                        $dataObject['employee_name']=$oneClass["be_name"];
                        $dataObject['employee_email']=$oneClass["be_mail"];
                        $dataObject['employee_pic']=($oneClass["be_pic"] == null) ? "" : $oneClass["be_pic"];

                        $dataObject['ccd_id']=$oneDate["ccd_id"];
                        $dataObject['start_date']=$startStamp;
                        $dataObject['start_time']=$oneDate["startDate"];
                        $dataObject['start_timestamp']=$startTimeStamp;
                        $dataObject['end_date']=$endStamp;
                        $dataObject['end_time']=$oneDate["endDate"];
                        $dataObject['end_timestamp']=$endTimeStamp;
                        $dataObject['taken_slots']=$booked;
                        $dataObject['nib_name']=$nib_name;
                        $dataObject['customer_server_id']=$server_customer_id;
                        $dataObject['md_row_id']=0;
                        $dataObject['md_biz_id']=$this->mobileRequestObject->bizid;
                        $dataObject['md_mod_id']=0;
                        $dataObject['md_external_type']="class";
                        array_push($responce,$dataObject);
                    }
                }
            }
        }
        return $responce;
    }

    private function getSelectedServiceEmployee_MainMenu($externalType,$structure){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $server_customer_id = $this->mobileRequestObject->server_customer_id;

        return $this->db->getTable("SELECT *, '$nib_name' nib_name,'$server_customer_id' customer_server_id, '$structure' structure_id
                                    FROM tbl_biz_cal_meet_type,
                                    (select * from tbl_mod_data0 where md_level_no=2 and md_biz_id={$this->mobileRequestObject->bizid} and md_external_type='$externalType' and md_belong_to_structure=$structure) t2,
                                    tbl_employee
                                    WHERE bmt_id=md_external_id
                                    AND be_id=md_external_sub_id
                                    AND bmt_biz_id = {$this->mobileRequestObject->bizid}");
    }

    public function getElementByParentAndElementType($parentId,$elementId){
        return $this->db->getRow("SELECT * FROM tbl_mod_data{$this->modId} WHERE md_parent=$parentId AND md_element=$elementId");
    }

    /************************************* */
    /*          STATIC FUNCTIONS           */
    /************************************* */

    public static function getGalleryImage($modID,$rowID){ 
        
        $instance = new self($modID);

        return $instance->db->getVal("SELECT md_icon FROM tbl_mod_data$modID WHERE md_parent = $rowID AND md_element <> 10 ORDER BY md_row_id LIMIT 1");
    }

    public static function getVideoImage($modID,$rowID){ 
        $instance = new self($modID);
        $md_info = $instance->db->getVal("SELECT md_info FROM tbl_mod_data$modID WHERE md_parent = $rowID AND md_element <> 10 ORDER BY md_row_id LIMIT 1");     
        return self::generateVideoThumb($md_info);
    }

    public static function generateVideoThumb($md_info){

        $video_thumb = "";
        if(utilityManager::contains(strtolower($md_info),"youtu")){
            $videoID = utilityManager::getYouTubeID($md_info);
            
            if($videoID != ''){
                $video_thumb = "https://img.youtube.com/vi/$videoID/hqdefault.jpg";
            }
        }

        if(utilityManager::contains(strtolower($md_info),"vimeo")){
            $vObject = utilityManager::getVimeoObject($md_info);

            if(isset($vObject->thumbnail_url) && $vObject->thumbnail_url != ''){
                $thumbnail = $vObject->thumbnail_url;
                $video_thumb = (string)$thumbnail;    
            }  
        }      
        return $video_thumb;
    }

    public static function getImagesCount(levelDataObject $data){ //$modID,$rowID

        $instance = new self($data->md_mod_id);
        return $instance->db->getVal("SELECT count(md_row_id) FROM tbl_mod_data{$data->md_mod_id} WHERE md_parent = {$data->md_row_id} AND md_element <> 10 AND md_level_no = {$data->md_level_no} + 1 ");
    }

    public static function getVideosCount(levelDataObject $data){ 

        $instance = new self($data->md_mod_id);

        return $instance->db->getVal("SELECT count(md_row_id) FROM tbl_mod_data{$data->md_mod_id} WHERE md_parent = {$data->md_row_id} AND md_element = 10 AND md_content = 1 AND md_visible_element = 1");

    }

    public static function getImageAlbumCount(levelDataObject $data){
        
        $instance = new self($data->md_mod_id);
        return $instance->db->getVal("SELECT count(md_row_id) FROM tbl_mod_data{$data->md_mod_id} WHERE md_parent = {$data->md_row_id} AND md_element = 10");
    }

    public static function getVideoAlbumCount(levelDataObject $data){        
        
        $instance = new self($data->md_mod_id);
        return $instance->db->getVal("SELECT count(md_row_id) FROM tbl_mod_data{$data->md_mod_id} WHERE md_parent = {$data->md_row_id} AND md_element = 10 AND md_content = 0 AND md_visible_element = 1");
    }

    public static function getSubscriptionExtraData(levelDataObject $data){        
        
        $instance = new self($data->md_mod_id);

        $sql = "SELECT  md_row_id as sub_id,
                        md_billing_type as billing_type,
                        md_billing_period_unit as billing_period_unit,
                        md_usage_period as usage_period,    
                        md_usage_period_unit as usage_period_unit,
                        md_usage_period_number as usage_period_number,
                        UNIX_TIMESTAMP(md_usage_period_start) as usage_period_start,
                        UNIX_TIMESTAMP(md_usage_period_end) as usage_period_end,
                        md_usage_rule_type as usage_rule_type,
                        md_usage_rule_capacity as usage_rule_capacity,
                        md_usage_rule_period_unit as usage_rule_period_unit,
                        md_color as color,
                        md_can_cancel as can_cancel,
                        md_featured as is_featured
                        FROM tbl_mod_data10  
                    WHERE md_biz_id = {$data->md_biz_id}
                    AND md_row_id = {$data->md_row_id}";        
        return $instance->db->getRow($sql);
    }

    public static function getSubscriptionConnectedItems(levelDataObject $data){

        $instance = new self($data->md_mod_id);
        $customerSubscriptionManager = new customerSubscriptionManager();
        $items = $customerSubscriptionManager->getSubscriptionItems($data->md_row_id);
       
        $result = array();

        if(count($items)>0){
            foreach ($items as $entry)
            {
                $itemRow = array();
                if($entry['sitem_type'] != 'other'){
                    switch ($entry['sitem_type'])
                    {
                        case "product":
                            $itemRow = $instance->getConnected_product($entry['sitem_ext_id']);
                            break;
                        case "meeting":
                            $itemRow = $instance->getConnected_meeting($entry['sitem_ext_id']);
                            break;
                        case "service":
                            $itemRow = $instance->getConnected_service($entry['sitem_ext_id']);
                            break;
                        case "class":
                            $itemRow = $instance->getConnected_class($entry['sitem_ext_id']);
                            break;
                    	default:
                    }              
                }
                else{
                    $itemOther['title'] = $entry['sitem_text'];
                    $itemOther['ext_id'] = 0; 
                    
                    $itemRow = $instance->fillConnectedItem($itemOther);
                }           
                $itemRow['item_type'] = $entry['sitem_type'];

                if($itemRow['title'] != ''){
                    $result[] = $itemRow;
                }
            }
        }

        return $result;
    }

    public static function getPunchPassExtraData(levelDataObject $data){

        $instance = new self($data->md_mod_id);

        $sql = "SELECT  md_row_id as ppass_id,
                        md_usage_period as usage_period,    
                        md_usage_period_unit as usage_period_unit,
                        md_usage_period_number as usage_period_number,
                        UNIX_TIMESTAMP(md_usage_period_start) as usage_period_start,
                        UNIX_TIMESTAMP(md_usage_period_end) as usage_period_end,
                        md_usage_rule_type as usage_rule_type,
                        md_usage_rule_capacity as usage_rule_capacity,
                        md_usage_rule_period_unit as usage_rule_period_unit,
                        md_usage_total_capacity as total_uses,
                        md_color as color,
                        md_featured as is_featured
                        FROM tbl_mod_data11  
                    WHERE md_biz_id = {$data->md_biz_id}
                    AND md_row_id = {$data->md_row_id}";

        
        return $instance->db->getRow($sql);
    }

    public static function getPunchPassConnectedItems(levelDataObject $data){

        $instance = new self($data->md_mod_id);
        $customerSubscriptionManager = new customerSubscriptionManager();
        $items = $customerSubscriptionManager->getPunchPassItems($data->md_row_id);

        $result = array();

        if(count($items)>0){
            foreach ($items as $entry)
            {
                $itemRow = array();
                if($entry['ppitem_type'] != 'other'){
                    switch ($entry['ppitem_type'])
                    {
                        case "product":
                            $itemRow = $instance->getConnected_product($entry['ppitem_ext_id']);
                            break;
                        case "meeting":
                            $itemRow = $instance->getConnected_meeting($entry['ppitem_ext_id']);
                            break;
                        case "service":
                            $itemRow = $instance->getConnected_service($entry['ppitem_ext_id']);
                            break;
                        case "class":
                            $itemRow = $instance->getConnected_class($entry['ppitem_ext_id']);
                            break;
                        default:
                    }                
                }
                else{
                    
                    $itemOther['title'] = $entry['ppitem_note'];
                    $itemOther['ext_id'] = 0; 
                    
                    $itemRow = $instance->fillConnectedItem($itemOther);
                }           
                $itemRow['item_type'] = $entry['ppitem_type'];

                $result[] = $itemRow;
            }
        }

        return $result;
    }

    public static function getConectedLevelDataByExternalIdAndStructure($modId,$externalId){

        $instance = new self($modId);

        $sql = "SELECT *
                    FROM tbl_mod_data$modId,tbl_mod_struct
					WHERE md_row_id=$externalId
					AND ms_mod_id=$modId 
                    AND ms_level_no=1";

        
        return $instance->db->getRow($sql);
    }

    public static function getConectedLevelDataPointShopItem($itemID){

        $instance = new self(0);

        $sql = "SELECT tbl_points_grant.*,md_head as label, pg_text as description FROM tbl_points_grant INNER JOIN tbl_mod_data9 ON pg_entity_id = md_row_id AND pg_id = $itemID AND pg_entity_type = 'product'
                                UNION
                                SELECT tbl_points_grant.*,bmt_name as label, pg_text as description FROM tbl_points_grant INNER JOIN tbl_biz_cal_meet_type ON pg_entity_id = bmt_id AND pg_id = $itemID AND pg_entity_type = 'service'
                                UNION
                                SELECT tbl_points_grant.*,pg_title as label, pg_text as description FROM tbl_points_grant WHERE pg_id = $itemID AND pg_entity_type = 'other' ";

        return $instance->db->getRow($sql);
    }

    public static function getConectedLevelDataServiceEmployeeButton($structure,$bizId){

        $instance = new self(0);

        return $instance->db->getRow("SELECT *  
                                    FROM tbl_biz_cal_meet_type,
                                    (select md_row_id rowId,md_external_id ex_id,md_external_sub_id employee_id,md_pic,md_info from tbl_mod_data0 where md_level_no=1 and md_biz_id=$bizId and md_external_type='service' and md_element=48 and md_belong_to_structure=$structure) t2,
                                    tbl_employee
                                    WHERE bmt_id=ex_id
                                    AND be_id=employee_id
                                    AND bmt_biz_id = $bizId
                                    LIMIT 1");
    }

    public static function getfirstTextElementForNewsChildObject($parentId){

        $instance = new self(0);

        $hasParagraph = $instance->db->getVal("SELECT COUNT(*) 
                                        FROM tbl_mod_data3 
                                        WHERE md_parent = $parentId 
                                        AND md_element = 6") > 0;

        if(!$hasParagraph){
            return "";
        }

        return $instance->db->getVal("SELECT md_info 
                                        FROM tbl_mod_data3 
                                        WHERE md_parent = $parentId 
                                        AND md_element = 6 
                                        ORDER BY md_index 
                                        LIMIT 1");
    }
    
    public static function getNextIndex(levelDataObject $data){

        $instance = new self(0);

        return $instance->db->getVal("SELECT MAX(md_index) + 1
                                        FROM tbl_mod_data{$data->md_mod_id} 
                                        WHERE md_biz_id = {$data->md_biz_id} 
                                        AND md_parent = {$data->md_parent}
                                        AND md_level_no={$data->md_level_no}");
    }

    public static function addImageToQueue(levelDataObject $data,$rowId,$columnName){

        $instance = new self(0);

        $instance->db->execute("INSERT INTO tbl_queue_import_pic SET
                                    ip_biz_id = {$data->md_biz_id}, 
                                    ip_mod_id = {$data->md_mod_id},
                                    ip_row_id = $rowId,
                                    ip_column_name = '$columnName',
                                    ip_url = '{$data->$columnName}'");
    }

    private function getConnected_product($item_id){

        $sql = "SELECT * FROM tbl_mod_data9
                WHERE md_row_id = $item_id";

        $row = $this->db->getRow($sql);

        $result = array();

        $result['title'] = $row['md_head'];
        $result['ext_id'] = $row['md_row_id'];
        $result['price'] = $row['md_price'];
        $result['desc'] = $row['md_desc'];


        return $this->fillConnectedItem($result);
    }

    private function getConnected_meeting($item_id){
        $sql = "SELECT * FROM tbl_employee_meeting,tbl_employee
                WHERE em_emp_id = be_id
                AND em_id = $item_id";

        $row = $this->db->getRow($sql);

        $result = array();

        $result['title'] = $row['em_name'].' '.utilityManager::getFormattedTime($row['em_start_date']).' - '.$row['be_name'];
        $result['ext_id'] = $row['em_id'];

        return $this->fillConnectedItem($result);
    }

    private function getConnected_service($item_id){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type
                WHERE bmt_id = $item_id";

        $row = $this->db->getRow($sql);

        $result = array();

        $result['title'] = $row['bmt_name'];
        $result['ext_id'] = $row['bmt_id'];
        $result['price'] = $row['bmt_price'];
        $result['desc'] = $row['bmt_desc'];
        $result['duration'] = $row['bmt_duration'];

        return $this->fillConnectedItem($result);
    }

    private function getConnected_class($item_id){
        $sql = "SELECT *,IF(calc_duration_by_service = 1,bmt_duration,calc_duration) as duration FROM tbl_calendar_classes
                LEFT JOIN tbl_biz_cal_meet_type ON calc_service = bmt_id
                WHERE calc_id = $item_id";

        $row = $this->db->getRow($sql);

        $result = array();

        $result['title'] = $row['calc_name'];
        $result['ext_id'] = $row['calc_id'];
        $result['price'] = $row['bmt_price'];
        $result['desc'] = $row['bmt_desc'];
        $result['duration'] = $row['duration'];

        return $this->fillConnectedItem($result);
    }

    private function fillConnectedItem($data){
        $result = array();

        $result['title'] = isset($data['title']) ? $data['title'] : '';
        $result['ext_id'] = isset($data['ext_id']) ? $data['ext_id'] : '';
        $result['price'] = isset($data['price']) ? $data['price'] : '';
        $result['desc'] = isset($data['desc']) ? $data['desc'] : '';
        $result['duration'] = isset($data['duration']) ? $data['duration'] : '';

        return $result;
    }
}
?>

