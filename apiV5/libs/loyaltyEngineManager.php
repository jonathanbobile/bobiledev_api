<?php

/**
 * loyaltyEngineManager short summary.
 *
 * loyaltyEngineManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class loyaltyEngineManager extends Manager
{
    public static function updateLoyaltySentTime($trackingID){
        $params = array();
        $params["tracking_id"] = $trackingID;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setSendTime",$params);
    }

    public static function updateLoyaltyDeliveredTime($trackingID){
        $params = array();
        $params["tracking_id"] = $trackingID;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setDeliverTime",$params);
    }

    public static function updateLoyaltyOpenTime($trackingID){
        $params = array();
        $params["tracking_id"] = $trackingID;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setOpenTime",$params);
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUE - PUBLIC           */
    /************************************* */

    /**
     * Insert new loyaltyQueueObject to DB
     * Return Data = new loyaltyQueueObject ID
     * @param loyaltyQueueObject $loyaltyQueueObj
     * @return resultObject
     */
    public function addLoyaltyQueue(loyaltyQueueObject $loyaltyQueueObj){
        try{
            $newId = $this->addLoyaltyQueueDB($loyaltyQueueObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get loyaltyQueue from DB for provided ID
     * * Return Data = loyaltyQueueObject
     * @param int $loyaltyQueueId
     * @return resultObject
     */
    public function getLoyaltyQueueByID($loyaltyQueueId){

        try {
            $loyaltyQueueData = $this->loadLoyaltyQueueFromDB($loyaltyQueueId);

            $loyaltyQueueObj = loyaltyQueueObject::withData($loyaltyQueueData);
            $result = resultObject::withData(1,'',$loyaltyQueueObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param loyaltyQueueObject $loyaltyQueueObj
     * @return resultObject
     */
    public function updateLoyaltyQueue(loyaltyQueueObject $loyaltyQueueObj){
        try{
            $this->upateLoyaltyQueueDB($loyaltyQueueObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete loyaltyQueue from DB
     * @param int $loyaltyQueueID
     * @return resultObject
     */
    public function deleteLoyaltyQueueById($loyaltyQueueID){
        try{
            $this->deleteLoyaltyQueueByIdDB($loyaltyQueueID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUE - DB METHODS           */
    /************************************* */

    private function addLoyaltyQueueDB(loyaltyQueueObject $obj){

        if (!isset($obj)){
            throw new Exception("loyaltyQueueObject value must be provided");
        }

        $clq_grant_valid_fromDate = isset($obj->clq_grant_valid_from) ? "'".$obj->clq_grant_valid_from."'" : "null";

        $clq_grant_valid_toDate = isset($obj->clq_grant_valid_to) ? "'".$obj->clq_grant_valid_to."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_loyalty_queue SET
                                        clq_biz_id = {$obj->clq_biz_id},
                                        clq_cust_id = {$obj->clq_cust_id},
                                        clq_event_id = {$obj->clq_event_id},
                                        clq_priority = {$obj->clq_priority},
                                        clq_cust_history_id = {$obj->clq_cust_history_id},
                                        clq_grant_entity = '{$obj->clq_grant_entity}',
                                        clq_grant_point_amount = {$obj->clq_grant_point_amount},
                                        clq_grant_entity_id = {$obj->clq_grant_entity_id},
                                        clq_grant_extra_data = '".addslashes($obj->clq_grant_extra_data)."',
                                        clq_grant_title = '".addslashes($obj->clq_grant_title)."',
                                        clq_grant_description = '".addslashes($obj->clq_grant_description)."',
                                        clq_grant_valid_from = $clq_grant_valid_fromDate,
                                        clq_grant_valid_to = $clq_grant_valid_toDate,
                                        clq_message = '".addslashes($obj->clq_message)."'
                                                 ");
        return $newId;
    }

    private function loadLoyaltyQueueFromDB($loyaltyQueueID){

        if (!is_numeric($loyaltyQueueID) || $loyaltyQueueID <= 0){
            throw new Exception("Illegal value $loyaltyQueueID");
        }

        return $this->db->getRow("SELECT * FROM tbl_loyalty_queue WHERE clq_id = $loyaltyQueueID");
    }

    private function upateLoyaltyQueueDB(loyaltyQueueObject $obj){

        if (!isset($obj->clq_id) || !is_numeric($obj->clq_id) || $obj->clq_id <= 0){
            throw new Exception("loyaltyQueueObject value must be provided");
        }

        $clq_send_timeDate = isset($obj->clq_send_time) ? "'".$obj->clq_send_time."'" : "null";

        $clq_grant_valid_fromDate = isset($obj->clq_grant_valid_from) ? "'".$obj->clq_grant_valid_from."'" : "null";

        $clq_grant_valid_toDate = isset($obj->clq_grant_valid_to) ? "'".$obj->clq_grant_valid_to."'" : "null";

        $this->db->execute("UPDATE tbl_loyalty_queue SET
                                clq_biz_id = {$obj->clq_biz_id},
                                clq_cust_id = {$obj->clq_cust_id},
                                clq_send_time = $clq_send_timeDate,
                                clq_event_id = {$obj->clq_event_id},
                                clq_priority = {$obj->clq_priority},
                                clq_cust_history_id = {$obj->clq_cust_history_id},
                                clq_grant_entity = '{$obj->clq_grant_entity}',
                                clq_grant_point_amount = {$obj->clq_grant_point_amount},
                                clq_grant_entity_id = {$obj->clq_grant_entity_id},
                                clq_grant_extra_data = '".addslashes($obj->clq_grant_extra_data)."',
                                clq_grant_title = '".addslashes($obj->clq_grant_title)."',
                                clq_grant_description = '".addslashes($obj->clq_grant_description)."',
                                clq_grant_valid_from = $clq_grant_valid_fromDate,
                                clq_grant_valid_to = $clq_grant_valid_toDate,
                                clq_message = '".addslashes($obj->clq_message)."'
                                WHERE clq_id = {$obj->clq_id}
                                         ");
    }

    private function deleteLoyaltyQueueByIdDB($loyaltyQueueID){

        if (!is_numeric($loyaltyQueueID) || $loyaltyQueueID <= 0){
            throw new Exception("Illegal value $loyaltyQueueID");
        }

        $this->db->execute("DELETE FROM tbl_loyalty_queue WHERE clq_id = $loyaltyQueueID");
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUELOG - PUBLIC           */
    /************************************* */

    /**
     * Insert new loyaltyQueueLogObject to DB
     * Return Data = new loyaltyQueueLogObject ID
     * @param loyaltyQueueLogObject $loyaltyQueueLogObj
     * @return resultObject
     */
    public function addLoyaltyQueueLog(loyaltyQueueLogObject $loyaltyQueueLogObj){
        try{
            $newId = $this->addLoyaltyQueueLogDB($loyaltyQueueLogObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueLogObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get loyaltyQueueLog from DB for provided ID
     * * Return Data = loyaltyQueueLogObject
     * @param int $loyaltyQueueLogId
     * @return resultObject
     */
    public function getLoyaltyQueueLogByID($loyaltyQueueLogId){

        try {
            $loyaltyQueueLogData = $this->loadLoyaltyQueueLogFromDB($loyaltyQueueLogId);

            $loyaltyQueueLogObj = loyaltyQueueLogObject::withData($loyaltyQueueLogData);
            $result = resultObject::withData(1,'',$loyaltyQueueLogObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueLogId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param loyaltyQueueLogObject $loyaltyQueueLogObj
     * @return resultObject
     */
    public function updateLoyaltyQueueLog(loyaltyQueueLogObject $loyaltyQueueLogObj){
        try{
            $this->upateLoyaltyQueueLogDB($loyaltyQueueLogObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueLogObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete loyaltyQueueLog from DB
     * @param int $loyaltyQueueLogID
     * @return resultObject
     */
    public function deleteLoyaltyQueueLogById($loyaltyQueueLogID){
        try{
            $this->deleteLoyaltyQueueLogByIdDB($loyaltyQueueLogID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueLogID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUELOG - DB METHODS           */
    /************************************* */

    private function addLoyaltyQueueLogDB(loyaltyQueueLogObject $obj){

        if (!isset($obj)){
            throw new Exception("loyaltyQueueLogObject value must be provided");
        }

        $clql_send_timeDate = isset($obj->clql_send_time) ? "'".$obj->clql_send_time."'" : "NOW()";

        $clql_grant_valid_fromDate = isset($obj->clql_grant_valid_from) ? "'".$obj->clql_grant_valid_from."'" : "null";

        $clql_grant_valid_toDate = isset($obj->clql_grant_valid_to) ? "'".$obj->clql_grant_valid_to."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_loyalty_queue_log SET
                                        clql_biz_id = {$obj->clql_biz_id},
                                        clql_cust_id = {$obj->clql_cust_id},
                                        clql_send_time = $clql_send_timeDate,
                                        clql_event_id = {$obj->clql_event_id},
                                        clql_priority = {$obj->clql_priority},
                                        clql_cust_history_id = {$obj->clql_cust_history_id},
                                        clql_grant_entity = '{$obj->clql_grant_entity}',
                                        clql_grant_point_amount = {$obj->clql_grant_point_amount},
                                        clql_grant_entity_id = {$obj->clql_grant_entity_id},
                                        clql_grant_extra_data = '".addslashes($obj->clql_grant_extra_data)."',
                                        clql_grant_title = '".addslashes($obj->clql_grant_title)."',
                                        clql_grant_description = '".addslashes($obj->clql_grant_description)."',
                                        clql_grant_valid_from = $clql_grant_valid_fromDate,
                                        clql_grant_valid_to = $clql_grant_valid_toDate,
                                        clql_message = '".addslashes($obj->clql_message)."'
                                                 ");
        return $newId;
    }

    private function loadLoyaltyQueueLogFromDB($loyaltyQueueLogID){

        if (!is_numeric($loyaltyQueueLogID) || $loyaltyQueueLogID <= 0){
            throw new Exception("Illegal value $loyaltyQueueLogID");
        }

        return $this->db->getRow("SELECT * FROM tbl_loyalty_queue_log WHERE clql_id = $loyaltyQueueLogID");
    }

    private function upateLoyaltyQueueLogDB(loyaltyQueueLogObject $obj){

        if (!isset($obj->clql_id) || !is_numeric($obj->clql_id) || $obj->clql_id <= 0){
            throw new Exception("loyaltyQueueLogObject value must be provided");
        }

        $clql_send_timeDate = isset($obj->clql_send_time) ? "'".$obj->clql_send_time."'" : "null";

        $clql_grant_valid_fromDate = isset($obj->clql_grant_valid_from) ? "'".$obj->clql_grant_valid_from."'" : "null";

        $clql_grant_valid_toDate = isset($obj->clql_grant_valid_to) ? "'".$obj->clql_grant_valid_to."'" : "null";

        $this->db->execute("UPDATE tbl_loyalty_queue_log SET
                                clql_biz_id = {$obj->clql_biz_id},
                                clql_cust_id = {$obj->clql_cust_id},
                                clql_send_time = $clql_send_timeDate,
                                clql_event_id = {$obj->clql_event_id},
                                clql_priority = {$obj->clql_priority},
                                clql_cust_history_id = {$obj->clql_cust_history_id},
                                clql_grant_entity = '{$obj->clql_grant_entity}',
                                clql_grant_point_amount = {$obj->clql_grant_point_amount},
                                clql_grant_entity_id = {$obj->clql_grant_entity_id},
                                clql_grant_extra_data = '".addslashes($obj->clql_grant_extra_data)."',
                                clql_grant_title = '".addslashes($obj->clql_grant_title)."',
                                clql_grant_description = '".addslashes($obj->clql_grant_description)."',
                                clql_grant_valid_from = $clql_grant_valid_fromDate,
                                clql_grant_valid_to = $clql_grant_valid_toDate,
                                clql_message = '".addslashes($obj->clql_message)."'
                                WHERE clql_id = {$obj->clql_id}
                                         ");
    }

    private function deleteLoyaltyQueueLogByIdDB($loyaltyQueueLogID){

        if (!is_numeric($loyaltyQueueLogID) || $loyaltyQueueLogID <= 0){
            throw new Exception("Illegal value $loyaltyQueueLogID");
        }

        $this->db->execute("DELETE FROM tbl_loyalty_queue_log WHERE clql_id = $loyaltyQueueLogID");
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYKILL - PUBLIC           */
    /************************************* */

    /**
     * Insert new custLoyaltyKillObject to DB
     * Return Data = new custLoyaltyKillObject ID
     * @param custLoyaltyKillObject $custLoyaltyKillObj
     * @return resultObject
     */
    public function addCustLoyaltyKill(custLoyaltyKillObject $custLoyaltyKillObj){
        try{
            $newId = $this->addCustLoyaltyKillDB($custLoyaltyKillObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltyKillObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get custLoyaltyKill from DB for provided ID
     * * Return Data = custLoyaltyKillObject
     * @param int $custLoyaltyKillId
     * @return resultObject
     */
    public function getCustLoyaltyKillByID($custLoyaltyKillId){

        try {
            $custLoyaltyKillData = $this->loadCustLoyaltyKillFromDB($custLoyaltyKillId);

            $custLoyaltyKillObj = custLoyaltyKillObject::withData($custLoyaltyKillData);
            $result = resultObject::withData(1,'',$custLoyaltyKillObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltyKillId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param custLoyaltyKillObject $custLoyaltyKillObj
     * @return resultObject
     */
    public function updateCustLoyaltyKill(custLoyaltyKillObject $custLoyaltyKillObj){
        try{
            $this->upateCustLoyaltyKillDB($custLoyaltyKillObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltyKillObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete custLoyaltyKill from DB
     * @param int $custLoyaltyKillID
     * @return resultObject
     */
    public function deleteCustLoyaltyKillById($custLoyaltyKillID){
        try{
            $this->deleteCustLoyaltyKillByIdDB($custLoyaltyKillID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltyKillID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYKILL - DB METHODS           */
    /************************************* */

    private function addCustLoyaltyKillDB(custLoyaltyKillObject $obj){

        if (!isset($obj)){
            throw new Exception("custLoyaltyKillObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_loyalty_killed SET
                                        clk_biz_id = {$obj->clk_biz_id},
                                        clk_cust_id = {$obj->clk_cust_id},
                                        clk_cevent_id = {$obj->clk_cevent_id},
                                        clk_kill_reason = '{$obj->clk_kill_reason}'
                                                 ");
        return $newId;
    }

    private function loadCustLoyaltyKillFromDB($custLoyaltyKillID){

        if (!is_numeric($custLoyaltyKillID) || $custLoyaltyKillID <= 0){
            throw new Exception("Illegal value $custLoyaltyKillID");
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_loyalty_killed WHERE clk_id = $custLoyaltyKillID");
    }

    private function upateCustLoyaltyKillDB(custLoyaltyKillObject $obj){

        if (!isset($obj->clk_id) || !is_numeric($obj->clk_id) || $obj->clk_id <= 0){
            throw new Exception("custLoyaltyKillObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_cust_loyalty_killed SET
                                clk_biz_id = {$obj->clk_biz_id},
                                clk_cust_id = {$obj->clk_cust_id},
                                clk_cevent_id = {$obj->clk_cevent_id},
                                clk_kill_reason = '{$obj->clk_kill_reason}'
                                WHERE clk_id = {$obj->clk_id}
                                         ");
    }

    private function deleteCustLoyaltyKillByIdDB($custLoyaltyKillID){

        if (!is_numeric($custLoyaltyKillID) || $custLoyaltyKillID <= 0){
            throw new Exception("Illegal value $custLoyaltyKillID");
        }

        $this->db->execute("DELETE FROM tbl_cust_loyalty_killed WHERE clk_id = $custLoyaltyKillID");
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYSENT - PUBLIC           */
    /************************************* */

    /**
     * Insert new custLoyaltySentObject to DB
     * Return Data = new custLoyaltySentObject ID
     * @param custLoyaltySentObject $custLoyaltySentObj
     * @return resultObject
     */
    public function addCustLoyaltySent(custLoyaltySentObject $custLoyaltySentObj){
        try{
            $newId = $this->addCustLoyaltySentDB($custLoyaltySentObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltySentObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get custLoyaltySent from DB for provided ID
     * * Return Data = custLoyaltySentObject
     * @param int $custLoyaltySentId
     * @return resultObject
     */
    public function getCustLoyaltySentByID($custLoyaltySentId){

        try {
            $custLoyaltySentData = $this->loadCustLoyaltySentFromDB($custLoyaltySentId);

            $custLoyaltySentObj = custLoyaltySentObject::withData($custLoyaltySentData);
            $result = resultObject::withData(1,'',$custLoyaltySentObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltySentId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param custLoyaltySentObject $custLoyaltySentObj
     * @return resultObject
     */
    public function updateCustLoyaltySent(custLoyaltySentObject $custLoyaltySentObj){
        try{
            $this->upateCustLoyaltySentDB($custLoyaltySentObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltySentObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete custLoyaltySent from DB
     * @param int $custLoyaltySentID
     * @return resultObject
     */
    public function deleteCustLoyaltySentById($custLoyaltySentID){
        try{
            $this->deleteCustLoyaltySentByIdDB($custLoyaltySentID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltySentID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYSENT - DB METHODS           */
    /************************************* */

    private function addCustLoyaltySentDB(custLoyaltySentObject $obj){

        if (!isset($obj)){
            throw new Exception("custLoyaltySentObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_loyalty_sent SET
                                        cls_biz_id = {$obj->cls_biz_id},
                                        cls_cust_id = {$obj->cls_cust_id},
                                        cls_cevent_id = {$obj->cls_cevent_id},
                                        cls_granted_entity = '{$obj->cls_granted_entity}',
                                        cls_granted_entity_id = {$obj->cls_granted_entity_id},
                                        cls_channel_sent = '{$obj->cls_channel_sent}',
                                        cls_channel_send_id = {$obj->cls_channel_send_id}
                                                 ");
        return $newId;
    }

    private function loadCustLoyaltySentFromDB($custLoyaltySentID){

        if (!is_numeric($custLoyaltySentID) || $custLoyaltySentID <= 0){
            throw new Exception("Illegal value $custLoyaltySentID");
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_loyalty_sent WHERE cls_id = $custLoyaltySentID");
    }

    private function upateCustLoyaltySentDB(custLoyaltySentObject $obj){

        if (!isset($obj->cls_id) || !is_numeric($obj->cls_id) || $obj->cls_id <= 0){
            throw new Exception("custLoyaltySentObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_cust_loyalty_sent SET
                                cls_biz_id = {$obj->cls_biz_id},
                                cls_cust_id = {$obj->cls_cust_id},
                                cls_cevent_id = {$obj->cls_cevent_id},
                                cls_granted_entity = '{$obj->cls_granted_entity}',
                                cls_granted_entity_id = {$obj->cls_granted_entity_id},
                                cls_channel_sent = '{$obj->cls_channel_sent}',
                                cls_channel_send_id = {$obj->cls_channel_send_id}
                                WHERE cls_id = {$obj->cls_id}
                                         ");
    }

    private function deleteCustLoyaltySentByIdDB($custLoyaltySentID){

        if (!is_numeric($custLoyaltySentID) || $custLoyaltySentID <= 0){
            throw new Exception("Illegal value $custLoyaltySentID");
        }

        $this->db->execute("DELETE FROM tbl_cust_loyalty_sent WHERE cls_id = $custLoyaltySentID");
    }
}