<?php

/**
 * partnersManager short summary.
 *
 * partnersManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class partnersManager extends Manager
{
    public static function getPartnerCurrentPlan($resellerID){
        $instance = new self();

        return $instance->db->getRow("SELECT * FROM tbl_transaction_feature,tbl_plans
                                        WHERE tf_entity_id = pl_id
                                        AND tf_reseller_id=$resellerID
                                        AND tf_entity='plan'
                                        AND tf_end_time IS NULL");
    }

    public static function checkIfVerifiedMarketDomain($domain){
        $instance = new self();

        if(utilityManager::contains($domain,"bobile.com")){
            return true;
        }

        return $instance->db->getVal("SELECT COUNT(*) FROM tbl_resellers
                                WHERE reseller_wl_market_domain LIKE '%$domain%'") > 0;
    }
}