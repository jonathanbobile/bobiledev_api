<?php
/**
 * Methods for Send SMS Notifications
 *
 * @version 1.0
 * @author PapTap
 */

class smsManager {

    protected $db;

    private $AUTH_ID = 'MAOWYYOGFINZAWY2I5OG';
    private $AUTH_TOKEN = 'ZWYzMjU5ZjU1MzQ5NjI2N2RiZDI3M2VlZWJlMDc4';
    private $src = '+18889943205';
    private $twilioURL = "https://twilio.bobile.com";

    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }
    }

    public static function sendMessage($to,$message,$location="")
    {
        if(!utilityManager::toSendSms()){
            return -1;
        }

        $instance = new self();

        $url = $instance->twilioURL."/twilio_manager/sms.php";
        $data = array("to" => "$to", "message" => "$message","token" => utilityManager::encodeToHASH($to));

        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec( $ch );
        $responseCode =  curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($responseCode !=  200){

            $errorBody = json_decode($response);
            $errorMessage = isset($errorBody->message) ? addslashes($errorBody->message) : "";
            $instance->db->execute("
                                INSERT INTO tbl_sms_errors SET
                                se_to_phone = '$to',
                                se_message = '$errorMessage',
                                se_from_phone = '{$instance->src}'
                                ");
        }
        curl_close($ch);
        return $response;
    }

    public static function sendMessage_old($to,$message,$location="")
    {


        $instance = new self();
        $url = 'https://api.plivo.com/v1/Account/'.$instance->AUTH_ID.'/Message/';
        $data = array("src" => $instance->src, "dst" => "$to", "text" => "$message");
        $data_string = json_encode($data);
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_USERPWD, $instance->AUTH_ID . ":" . $instance->AUTH_TOKEN);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec( $ch );
        $responseCode =  curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($responseCode >=  300){

            $instance = new self();
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $errorBody = json_decode(substr($response , $header_size));
            $errorMessage = isset($errorBody->error) ? addslashes($errorBody->error) : "";
            $instance->db->execute("
                                INSERT INTO tbl_sms_errors SET
                                se_to_phone = '$to',
                                se_message = '$errorMessage',
                                se_from_phone = '{$instance->src}'
                                ");
        }
        curl_close($ch);
        return $response;
    }

    public static function receiveSMS($data){
        $instance = new self();

        $text = addslashes($data["Text"]);
        $textArray = explode(":",$data["Text"]);
        $code = trim($textArray[1]);
        $instance->db->execute("
                                INSERT INTO tbl_apple_credentials_sms SET
                                acs_text = '$text',
                                acs_code = '$code',
                                acs_from = '{$data["From"]}',
                                acs_uuid = '{$data["MessageUUID"]}'
                                ");
    }

    public static function getAllNewSMS(){
        $instance = new self();

        $smsTable = $instance->db->getTable("
                                SELECT * FROM tbl_apple_credentials_sms
                                WHERE acs_new = 1
                                ");

        $response = array();
        if(count($smsTable) > 0){
            $response = $smsTable;
        }
        return $response;
    }

    public static function setSmsAsRead($smsId){
        $instance = new self();

        $instance->db->execute("
                                UPDATE tbl_apple_credentials_sms SET
                                acs_new = 0
                                WHERE acs_id=$smsId
                                ");

        $response["code"] = 1;
        return $response;
    }

    public static function makeCodeCall($to,$code,$lang="en"){

        if(!utilityManager::toMakeCall()){
            return -1;
        }

        $instance = new self();
        $params = array(
           'to' => $to,   # The phone numer to which the call will be placed
           'answer_url' => "https://".MAIN_SERVER."/admin/api/getAnswerXML/$code/$lang",
           'answer_method' => "GET", # The method used to call the answer_url
           'token' => utilityManager::encodeToHASH($to)
       );

        $url = $instance->twilioURL."/twilio_manager/call.php";
        $data = $params;
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec( $ch );
        $responseCode =  curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($responseCode !=  200){

            $errorBody = json_decode($response);
            $errorMessage = "call: ";
            $errorMessage .= isset($errorBody->message) ? addslashes($errorBody->message) : "";
            $instance->db->execute("
                                INSERT INTO tbl_sms_errors SET
                                se_to_phone = '$to',
                                se_message = '$errorMessage',
                                se_from_phone = '{$instance->src}'
                                ");
        }

        curl_close($ch);
        return $response;

    }

    public static function makeCodeCall_old($to,$code,$lang="en"){

        $instance = new self();

        $params = array(
           'to' => $to,   # The phone numer to which the call will be placed
           'from' => $instance->src, # The phone number to be used as the caller id
           'answer_url' => "https://".MAIN_SERVER."/admin/api/getAnswerXML/$code/$lang",
           'answer_method' => "GET", # The method used to call the answer_url
       );

        $url = 'https://api.plivo.com/v1/Account/'.$instance->AUTH_ID.'/Call/';
        $data = $params;
        $data_string = json_encode($data);
        $ch=curl_init($url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_USERPWD, $instance->AUTH_ID . ":" . $instance->AUTH_TOKEN);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $response = curl_exec( $ch );
        curl_close($ch);
        return $response;

    }

public static function canSendCodeSms($phoneNumber,$customerId){

        $instance = new self();

        $validByCountry = $instance->db->getVal("SELECT count(cust_id) FROM tbl_biz,tbl_customers
                                                WHERE cust_id=$customerId
                                                AND biz_id=cust_biz_id
                                                AND (biz_id not in (370652)
                                                OR (biz_world=1 OR cust_country=biz_addr_country_id OR cust_country=0 OR cust_country IS NULL))") > 0;

        $validByAmmount = $instance->db->getVal("
                                SELECT count(vc_id) FROM  tbl_valid_code_sms_phones
                                WHERE vc_phone_number='$phoneNumber'
                                AND vc_tume > DATE_ADD(NOW(), INTERVAL -1 DAY)
                                ") < 3;

        return $validByCountry && $validByAmmount;
    }

    public static function getDifferentNumbersCountForCustomerID($customerId){
        $instance = new self();
        return $instance->db->getVal("
                                    SELECT count(*) FROM
                                    (SELECT vc_phone_number FROM tbl_valid_code_sms_phones
                                    WHERE vc_cust_id=$customerId
                                    AND vc_tume > DATE_ADD(NOW(), INTERVAL -1 DAY)) s
                                    ");
    }

    public static function addSendCodeSms($phoneNumber,$custID){

        $instance = new self();

        if(!utilityManager::startsWith($phoneNumber,"+")){
            $phoneNumber = "+".$phoneNumber;
        }

        $instance->db->execute("
                                    INSERT INTO tbl_valid_code_sms_phones
                                    SET vc_phone_number='$phoneNumber',
                                    vc_cust_id = $custID
                                    ");
    }

    public static function deleteSendCodeSms($phoneNumber){

        $instance = new self();

        $instance->db->execute("
                                DELETE FROM tbl_valid_code_sms_phones
                                WHERE vc_phone_number='$phoneNumber'
                                ");
    }

    public static function addSendCodeVoiceCall($phoneNumber,$bizID,$customerID,$customer_ip){
        $canSend = true;
        $instance = new self();
        $ipBLocked = self::isBlockedIP($customer_ip);

        if($instance->AttemptedInLastFiveMinutesFromNumber($phoneNumber,$customerID)){
            return resultObject::withData(0,'');
        }

        $instance->db->execute("INSERT INTO tbl_valid_code_call_phones SET
                                    vccp_phone_number = '$phoneNumber',
                                    vccp_biz_id = $bizID,
                                    vccp_cust_id = $customerID");

        $count = $instance->db->getVal("SELECT COUNT(*) FROM tbl_valid_code_call_phones
                                            WHERE (vccp_phone_number = '$phoneNumber'
                                            OR vccp_cust_id = $customerID)
                                            AND vccp_biz_id = $bizID");

        if($count > 3 || $ipBLocked){
            $canSend = false;
            $instance->db->execute("INSERT INTO tbl_blacklisted_numbers SET
                                        bn_phone_number = '$phoneNumber',
                                        bn_customer_id = $customerID,
                                        bn_biz_id = $bizID");
        }

        $custCount = $instance->db->getVal("SELECT COUNT(*) FROM tbl_blacklisted_numbers WHERE bn_customer_id = $customerID");

        if($custCount >= 3 || $ipBLocked){
            $canSend = false;
            $instance->db->execute("UPDATE tbl_customers SET cust_is_fraud = 1 WHERE cust_id = $customerID");
        }

        $code = $canSend ? 1 : 0;
        return resultObject::withData($code,'');
    }

    public static function deleteSendCodeVoiceCall($phoneNumber,$bizID,$customerID){
        $instance = new self();

        $instance->db->execute("DELETE FROM tbl_valid_code_call_phones
                                    WHERE vccp_phone_number = '$phoneNumber'
                                    AND vccp_biz_id = $bizID");

        $instance->db->execute("DELETE FROM tbl_blacklisted_numbers
                                    WHERE bn_customer_id = $customerID
                                    OR (bn_phone_number = '$phoneNumber' AND bn_biz_id = $bizID)");

        $instance->db->execute("UPDATE tbl_customers SET cust_is_fraud = 0 WHERE cust_id = $customerID");

    }

    public static function isFraudulentCountry($bizID,$countryID){
        $fraudCountInCountry = bizManager::getCountFradulentCustomerInCountryForBiz($bizID,$countryID);

        return $fraudCountInCountry >=3;
    }

    private function AttemptedInLastFiveMinutesFromNumber($phoneNumber,$customerID){
        $sql = "SELECT COUNT(*) FROM tbl_valid_code_call_phones
                WHERE (vccp_cust_id = $customerID
                OR vccp_phone_number = '$phoneNumber')
                AND vccp_time > DATE_SUB(NOW(),INTERVAL 5 MINUTE)";

        $lastAttemptCount = $this->db->getVal($sql);

        return $lastAttemptCount > 0;
    }

    public static function isBlacklistedSerial($serial){
        $instance = new self();

        return $instance->db->getVal("SELECT COUNT(*) FROM tbl_blacklisted_serials 
                                        WHERE bs_serial LIKE '$serial'") > 0;
    }

    public static function addSerialToBlackList($serial,$customerID){
        $instance = new self();

        $instance->db->execute("INSERT INTO tbl_blacklisted_serials SET
                                    bs_serial = '$serial',
                                    bs_last_cust_id = $customerID");
    }

    public static function isBlockedIP($ip){
        return $ip == "37.120.218.88";
    }
}

?>
