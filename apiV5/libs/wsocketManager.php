<?php


require ROOT_PATH.'libs/zeroMQ/vendor/autoload.php';

/**
 * Manages the websocket interactions
 *
 * wsocketManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class wsocketManager
{
    protected $db;

    public function __construct() {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }      
    }
   
    public function getConnections(){
        $data = array();
        $data['type'] = 'getConnections';        

        $result = $this->Sender($data);

        return $result;
    }

    public static function sendIMAsync($message,$receiver,$message_type,pushParamsObject $fallback = null,$forAndroid = false){
        $params = array();
        $params['message'] = json_encode($message);
        $params['receiver'] = $receiver;
        $params['message_type'] = $message_type;
        if(isset($fallback)){
            $params['fallback'] = json_encode($fallback);
        }
        $params["for_android"] = $forAndroid ? 1 : 0;
        utilityManager::asyncTask("http://127.0.0.1/".MVC_NAME.'/eventAsync/sendInstantMessage',$params);
    }

    public static function sendIM($message,$receiver,$message_type,pushParamsObject $fallback = null,$forAndroid = false,$trackingID = ""){
        $instance = new self();
        try{
           
            $msg_status = new stdClass();
            $msg_status->code = '-1';
            if($forAndroid){
                $counter = 3; 
                $sent = false;
                $stop = false;
                while(!$stop){
                    $msg_status = $instance->sendMessage($receiver,$message_type,$message); 

                    $sent = $msg_status->code == 1;
                    $counter--;

                    $stop = $sent || $counter <= 0;
                    if(!$sent){
                        sleep(1);
                    }
                    
                }
            }
            else{
                $msg_status = $instance->sendMessage($receiver,$message_type,$message); 
            }

            if($msg_status->code == 1){
                if($trackingID != ""){
                    loyaltyEngineManager::updateLoyaltyOpenTime($trackingID);
                }
            }
            else{
                
                if(isset($fallback)){
                    if($trackingID != ""){
                        $fallback->extra_params["tracking_id"] = $trackingID;
                    }
                    pushManager::sendBizPushWithParams($fallback);
                }
            }
            exit;
        }
        catch(Exception $e){
            exit;
        }
    }

    public static function sendIMSynchronous($message,$receiver,$message_type,pushParamsObject $fallback = null,$forAndroid = false){
        $instance = new self();
        try{
            
            $msg_status = new stdClass();
            $msg_status->code = '-1';
            if($forAndroid){
                $counter = 3; 
                $sent = false;
                $stop = false;
                while(!$stop){
                    $msg_status = $instance->sendMessage($receiver,$message_type,$message); 

                    $sent = $msg_status->code == 1;
                    $counter--;

                    $stop = $sent || $counter <= 0;
                    if(!$sent){
                        sleep(1);
                    }
                    
                }
            }
            else{
                $msg_status = $instance->sendMessage($receiver,$message_type,$message); 
            }

            if($msg_status->code != 1){
                $result = resultObject::withData(0);
                if(isset($fallback)){
                    pushManager::sendBizPushWithParams($fallback);
                }
            }
            else{
                $result = resultObject::withData(1);
            }

            return $result;
        }
        catch(Exception $e){
            $data = array();
            $data['message'] = $message;
            $data['receiver'] = $receiver;
            $data['type'] = $message_type;
            $data['fallback'] = isset($fallback) ? $fallback : '';
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(-1);
            
            if(isset($fallback)){
                pushManager::sendBizPushWithParams($fallback);
            }
            return $result;
        }
    }

    public function logConnection($identity,$connection_id){
        $ident_parts = explode('_',$identity);
        if(count($ident_parts) == 2){
            $custID = $ident_parts[0];
            $deviceID = $ident_parts[1];
        }
        else{
            return -1;
        }

        $my_ip = $_SERVER['SERVER_ADDR'];

        $sql = "INSERT INTO tbl_socket_connections SET
                    sc_cust_id = $custID,
                    sc_device_id = '$deviceID',
                    sc_identity = '$identity',
                    sc_server_ip = '$my_ip',
                    sc_connection_id = $connection_id
                ON DUPLICATE KEY UPDATE
                    sc_server_ip = '$my_ip',
                    sc_connection_id = $connection_id,
                    sc_connection_time = NOW()";

        $result = $this->db->execute($sql);

        return $result;
    }

    public function deleteConnection($sconn_id){
        if(!isset($sconn_id) || $sconn_id == ""){
            return;
        }

        $sql = "DELETE FROM tbl_socket_connections WHERE sc_id = $sconn_id";

        $this->db->execute($sql);
    }


    public function sendMessage($target_id,$type,$message){
        
        $data = array();
        $data['type'] = 'sendMessage';
        $dataInner = array();
        $dataInner['target'] = $target_id;
        $dataInner['body'] = array();
        $dataInner['body']['type'] = $type;
        $dataInner['body']['data'] = $message;
        $dataInner['body']['code'] = 1;
        $dataInner['body']['timecode'] = time();
        $data['data'] = $dataInner;

        $result = $this->Sender($data);

        return $result;
    }

    public function Sender($data){
      //  $loop = React\EventLoop\Factory::create();

        $context = new ZMQContext();;
        
        $requester = new ZMQSocket($context, ZMQ::SOCKET_REQ);

        $requester->setSockOpt(ZMQ::SOCKOPT_SNDTIMEO,1000);
        $requester->setSockOpt(ZMQ::SOCKOPT_RCVTIMEO,1000);
        $result = new stdClass();
        $result->code = '-1';
        $result->message = 'no_server';
        try{
            
            $requester->connect('tcp://localhost:5555');
            
            
            $toSend = json_encode($data);
            
            $requester->send($toSend);
            $response = $requester->recv();
            
           // $loop->run();
            
            if($response != ''){
                $result = json_decode($response);
            }
        }
        catch(Exception $ex){
            echo "ex \n";
            $result = new stdClass();
            $result->code = '-1';
            $result->message = 'no_server - '.$ex->getMessage();
        }
        return $result;
    }
}
