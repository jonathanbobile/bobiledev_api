<?php

class bizModel extends Model
{

    public $bizID;
    public $mobileRequestObject = array();

    function __construct($bizID)
    {
        parent::__construct();

        $this->bizID = $bizID;
    }

    /**********************************/
    /*        Basic Biz function      */
    /**********************************/

    /**
     * Get biz object from DB (bizID provided by the constructor parameter)
     * Return Data = bizObject
     * @return resultObject
     */
    public function getBiz(){

        try{
            $responseData = $this->getBizByBizIdDB($this->bizID);
            $bizObject = bizObject::withData($responseData);
            return resultObject::withData(1,"Ok",$bizObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Isert biz to DB
     * * Returns Data = new biz id
     * @param bizObject $bizObject
     * @return resultObject
     */
    public function addBiz(bizObject $bizObject){

        try{
            $newId = $this->addBizDB($bizObject);
            return resultObject::withData(1,"Ok",$newId);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Update biz in DB
     * @param bizObject $bizObject
     * @return resultObject
     */
    public function updateBiz(bizObject $bizObject){

        try{
            $this->updateBizDB($bizObject);
            return resultObject::withData(1,"Ok");
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Delete biz row from DB
     */
    public function DeleteBiz(){
        try{
            $this->deleteBizByBizIdDB($this->bizID);
            return resultObject::withData(1);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getBizStructure(){

        $responseObject = array();

        // general biz data
        $responseBiz = self::getBiz();
        if($responseBiz->code == 1){
            $responseObject = $this->getBizStructureGeneral($responseBiz->data);
        }else{
            return $responseBiz;
        }

        // owner
        $ownerManager = new ownerManager($responseBiz->data->biz_owner_id);
        $responseOwner = $ownerManager->getOwnerByID($responseBiz->data->biz_owner_id);
        if($responseOwner->code == 1){
            $responseObject = $this->getBizStructureOwner($responseOwner->data,$responseObject);
        }else{
            return $responseOwner;
        }

        // store settings
        $responseStore = self::getBizStoreSettings();
        if($responseStore->code == 1){
            $responseObject = $this->getBizStructureStore($responseStore->data,$responseObject);
        }else{
            return $responseStore;
        }


        // Processor loadBizPaymentProcessorFromDB
        $responseProsessor = self::getBizPaymentProcessorByID(3);
        if($responseProsessor->code == 1){
            $responseObject = $this->getBizStructureProsessor($responseProsessor->data,$responseObject);
        }else{
            return $responseProsessor;
        }

        $responseObject = $this->getBizPaymentOptions($responseObject);

        // theme
        $responseTheme = self::getBizThemeByID($responseBiz->data->biz_theme);
        if($responseTheme->code == 1){
            $responseObject = $this->getBizStructureTheme($responseTheme->data,$responseObject);
        }else{
            return $responseTheme;
        }

        //booking
        $calSettingsObgect = bizManager::getBizCalendarSettingsByBizID($this->bizID);
        if(isset($calSettingsObgect)){
            $responseObject = $this->getDefaultBookingSettings($calSettingsObgect,$responseObject);
        }


        // main menu
        $data = self::loadMainMenuDataFromDB();
        $moduleStructureObject= moduleStructureObject::withData($data);
        $bizModObject= bizModObject::withData($data);
        $responseObject = $this->getBizStructureMainMenu($moduleStructureObject,$bizModObject,$responseObject);

        // biz modules
        $modDataManager = new moduleDataManager();
        try{
            $dataModules = $modDataManager->loadModulesForMobileFromDB($bizModObject);
            $responseObject = $this->getBizModulesArray($dataModules,$responseObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

        // biz structure
        $responseObject = $this->getBizStructureArray($dataModules,$responseTheme->data,$responseObject);

        // biz fields
        $responseObject = $this->getBizFieldsArray($responseObject);

        // biz tabbar
        $responseObject = $this->getBizTabbarArray($responseObject);

        // biz navigation
        $responseObject = $this->getBizNavigationArray($bizModObject,$responseObject);

        // biz engagement
        $responseObject = $this->getBizEngagementArray($bizModObject,$responseObject);

        return resultObject::withData(1,"ok",$responseObject);

    }

    public function getBizForAdmin(accountObject $account){
        $responseObject = array();

        // general biz data
        $responseBiz = self::getBiz();
        if($responseBiz->code != 1){
            return $responseBiz;
        }

        $biz = $responseBiz->data;

        $responseObject["biz_short_name"] = $biz->biz_short_name;
        $responseObject["biz_icon"] = $biz->biz_icon;
        $responseObject["biz_logo"] = $biz->biz_logo;
        $responseObject["biz_splash"] = $biz->biz_submit_splash;
        $responseObject["biz_copyright"] = $biz->biz_copy_right;
        $responseObject["biz_theme"] = $biz->biz_theme;
        $responseObject["biz_layout"] = $biz->biz_menu;

        $responseObject["biz_phone1"] = $biz->biz_office_tele;
        $responseObject["biz_phone2"] = $biz->biz_mobile_tele;
        $responseObject["biz_email"] = $biz->biz_e_mail;
        $responseObject["biz_website"] = $biz->biz_website;

        $responseObject["biz_country"] = $biz->biz_addr_country_id;
        $responseObject["biz_state"] = $biz->biz_addr_state_id;
        $responseObject["biz_city"] = $biz->biz_addr_town;
        $responseObject["biz_street"] = $biz->biz_addr_street;
        $responseObject["biz_street_number"] = $biz->biz_addr_no;
        $responseObject["biz_zip"] = $biz->biz_postal_code;

        $responseObject["biz_paymentMethodToken"] = $biz->biz_paymentMethodToken;
        $responseObject["biz_paymentMethodType"] = $biz->biz_paymentMethodType;
        $responseObject["biz_cvv"] = $biz->biz_cvv;
        $responseObject["biz_cc_last_digits"] = $biz->biz_cc_last_digits;
        $responseObject["biz_payment_name"] = $biz->biz_payment_name;
        $responseObject["biz_payment_email"] = $biz->biz_payment_email;

        if($biz->biz_submit_intern_time != ""){
            $responseObject["appl_status"] = ($biz->biz_appl_status == "0") ? "1" : $biz->biz_appl_status;
            $responseObject["google_status"] = ($biz->biz_goog_status == "0") ? "1" : $biz->biz_goog_status;
            $responseObject["amazon_status"] = ($biz->biz_amaz_status == "0") ? "1" : $biz->biz_amaz_status;
        }else{
            $responseObject["appl_status"] = $biz->biz_appl_status;
            $responseObject["google_status"] = $biz->biz_goog_status;
            $responseObject["amazon_status"] = $biz->biz_amaz_status;
        }

        $responseObject["biz_status"] = $biz->biz_status;

        $trialDays = bizManager::getTrialDaysForBiz($biz->biz_id);
        $responseObject["biz_trial_days"] = ($trialDays == "" || $trialDays < 0) ? 0 : $trialDays;
        $responseObject["biz_on_sale"] = $biz->biz_on_sale;

        $currentPlan = bizManager::getBizCurrentPlan($biz->biz_id);

        $responseObject["current_plan_name"] = $currentPlan["pl_name"];
        $responseObject["current_plan_price"] = $currentPlan["pl_plrice"];
        $responseObject["current_plan_id"] = $currentPlan["pl_id"];
        $responseObject["current_plan_group"] = $currentPlan["pl_type"];

        $chatManager = new chatManager();

        $responseObject["unread_messages"] = $chatManager->getUnreadChatMessagesForBiz($biz->biz_id);

        $responseObject = $this->getSocialPages($responseObject);

        $responseObject = $this->getWorkingHours($responseObject);

        $responseObject["stats_installs"] = bizStatsManager::getInstalls("DATE_SUB(NOW(),INTERVAL 5 MONTH)","NOW()",$biz->biz_id);
        $responseObject["stats_entries"] = bizStatsManager::getStatistics("DATE_SUB(NOW(),INTERVAL 5 MONTH)","NOW()",$biz->biz_id);
        $responseObject["stats_top_pages"] = bizStatsManager::getPopularScreens("DATE_SUB(NOW(),INTERVAL 5 MONTH)","NOW()",$biz->biz_id);
        $responseObject["stats_install_location"] = bizStatsManager::getInstallLocation("DATE_SUB(NOW(),INTERVAL 5 MONTH)","NOW()",$biz->biz_id);

        // biz modules
        $modDataManager = new moduleDataManager();
        $dataModules = $modDataManager->loadModulesForAdminForBiz($biz->biz_id);
        $responseObject = $this->getBizModulesArrayForAdmin($dataModules,$responseObject);


        // biz theme
        $responseTheme = self::getBizThemeByID($biz->biz_theme);
        if($responseTheme->code == 1){
            $responseObject = $this->getBizStructureTheme($responseTheme->data,$responseObject);
        }

        $bizMainMenuRow = $this->getBizModByBizIDAndModID_DB($biz->biz_id,0);

        $responseObject["menu_id"] = $bizMainMenuRow["biz_mod_stuct"];

        $permissionManage = new permissionsManager();
        $accType = $account->ac_reseller_id != 0 || $account->ac_type == "guest" ? "Client" : "";

        $responseObject["biz_permissions"] = $permissionManage->getMobileBizPermissions($account->ac_id,$biz->biz_id,$accType);
        return resultObject::withData(1,"ok",$responseObject);
    }

    public function getBizSettingsForAdmin(){

        $responseBiz = self::getBiz();
        if($responseBiz->code != 1){
            return $responseBiz;
        }

        $biz = $responseBiz->data;

        $responseObject = array();

        $responseObject["biz_category_id"] = $biz->biz_category_id;
        $responseObject["biz_sub_category_id"] = $biz->biz_sub_category_id;
        $responseObject["biz_addr_country_id"] = $biz->biz_addr_country_id;
        $responseObject["biz_addr_state_id"] = $biz->biz_addr_state_id;
        $responseObject["biz_dir"] = $biz->biz_dir;
        $responseObject["biz_time_zone"] = $biz->biz_time_zone;



        $billing = new billingManager();
        $plans = $billing->getPlansForBiz($biz);

        $responseObject["plans"] = $plans["plans"];
        $responseObject["customPlan"] = $plans["customPlan"];

        $invoiceTable = $billing->getAllInvoices($biz->biz_id);

        if($biz->biz_addr_country_id == '1') {
            $dFormatPHP = 'm/d/Y';
        } else {
            $dFormatPHP = 'd/m/Y';
        }
        $invoiceArray = array();
        if(count($invoiceTable) > 0){
            foreach ($invoiceTable as $oneInvoice)
            {
                $totalAmount = ($oneInvoice['tr_amount'] == "") ? number_format($oneInvoice['tri_total_amount'],2) : number_format($oneInvoice['tr_amount'],2);
                $paymentMethod = ($totalAmount < 0) ? "Refund" : (($oneInvoice['tr_paymentMethodType'] == "CreditCard") ? "Credit Card" : "PayPal");
                $invoiceData = array();
                $invoiceData["invoice_ref"] = $oneInvoice["tri_id"] + 100000;
                $invoiceData["charge_date"] = date($dFormatPHP, strtotime($oneInvoice['tri_time']));
                $invoiceData["payment_method"] = ($oneInvoice['tr_paymentMethodType']=="" && $totalAmount != 0) ? "Not paid yet" : $paymentMethod;
                $invoiceData["payment_status"] = ($oneInvoice['tr_paymentMethodType']=="" && $totalAmount != 0) ? "0" : "1";
                $invoiceData["total_price"] = $totalAmount;
                array_push($invoiceArray,$invoiceData);
            }
        }
        $responseObject["billing"] = $invoiceArray;

        return resultObject::withData(1,'',$responseObject);
    }



    /**********************************/
    /*        App Store function      */
    /**********************************/

    /**
     * Get list of all app srtores with stage object connected
     * * Return array of bizAppStore Objects
     * @return array
     */
    public function getBizAppstoresAndStatus(){

        $returnArray = array();
        $appStores = $this->getBizAppsores();
        $stages = $this->getAppstoreStageList();
        $bizObjectResponse = $this->getBiz();

        if($bizObjectResponse->code == 1){

            $bizObject = $bizObjectResponse->data;

            foreach ($appStores as $appStore) {

                switch ($appStore->id) {
                    case '1': //Apple
                        if(isset($bizObject->biz_submit_intern_time) && $bizObject->biz_submit_intern_time != ""){
                            $appStore->stage = $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
                        }else{
                            $appStore->stage = $this->getStageForApple($stages,$bizObject);
                        }
                        array_push($returnArray,$appStore);
                        break;

                    case '2': //Google
                        if(isset($bizObject->biz_submit_intern_time) && $bizObject->biz_submit_intern_time != ""){
                            $appStore->stage = $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
                        }else{
                            $appStore->stage = $this->getStageForGoogle($stages,$bizObject);
                        }
                        array_push($returnArray,$appStore);
                        break;

                    case '3': //Amazon
                        if(isset($bizObject->biz_submit_intern_time) && $bizObject->biz_submit_intern_time != ""){
                            $appStore->stage = $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
                        }else{
                            $appStore->stage = $this->getStageForAmazon($stages,$bizObject);
                        }
                        array_push($returnArray,$appStore);
                        break;

                    case '4': //Market
                        $appStore->stage = $this->getStageForBobileMarket($stages,$bizObject);
                        array_push($returnArray,$appStore);
                        break;
                }

            }

            return resultObject::withData(1,"Ok",$returnArray);

        }else{
            return $bizObjectResponse;
        }


    }

    /**
     * Get list of all app srtores for biz
     * * Return array of bizAppStore Objects
     * @return array
     */
    public function getBizAppsores(){

        $appstoreData = $this->getBizAppsoresDB();

        $appstoresArray = array();
        if(count($appstoreData)>0){
            foreach ($appstoreData as $app)
            {
            	$appObject = bizAppStore::withData($app);
                $appObject->connectedToBiz = $app["connected"] == 1;
                array_push($appstoresArray,$appObject);
            }
        }

        return $appstoresArray;
    }

    /**
     * Return a list of all availble stages
     * * Return array of bigMarketStages Objects
     * @return array
     */
    public function getAppstoreStageList(){

        $stagesData = $this->getAppstoreStageListDB();

        $stagessArray = array();

        if(count($stagesData)>0){
            foreach ($stagesData as $stage)
            {
            	$stageObject = bigMarketStages::withData($stage);
                array_push($stagessArray,$stageObject);
            }
        }

        return $stagessArray;

    }

    /**
     * Check if biz has active credentials in apple
     * @param bizObject $bizObject
     * @return boolean
     */
    public function hasActiveAppleAccount(bizObject $bizObject){

        return $this->db->getVal("select count(ac_id) FROM tbl_apple_credentials
                                                WHERE ac_owner_id={$bizObject->biz_owner_id}
                                                AND ac_biz_id = {$bizObject->biz_id}
                                                AND ac_status = 'active'") >= 1;
    }

    /**
     * Check if biz has active credentials in google
     * @param bizObject $bizObject
     * @return boolean
     */
    public function hasActiveGoogleAccount(bizObject $bizObject){

        return $this->db->getVal("select count(gc_id) from tbl_google_credentials
                                                where gc_owner_id={$bizObject->biz_owner_id}
                                                AND gc_verified = 1") >= 1;
    }

    /**********************************/
    /*         Coupons function        */
    /**********************************/

    public function getActiveCoupons(){
        try{
            $couponRows = $this->getActiveCouponRows();

            $coupons = array();

            foreach ($couponRows as $entry)
            {
            	$coupons[] = levelData26Object::withData($entry);
            }

            return resultObject::withData(1,'',$coupons);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getActiveCouponRows(){
        $sql = "SELECT  * FROM tbl_mod_data26
                WHERE md_biz_id = {$this->bizID}
                AND md_visible_element = 1
                AND (md_date1 IS NULL OR md_date1 < NOW())
                AND (md_date2 IS NULL OR md_date2 > NOW())";

        return $this->getTable($sql);
    }


    /**********************************/
    /*        Private Biz function      */
    /**********************************/

    private function getBizByBizIdDB($bizId){

        if (!is_numeric($bizId) || $bizId <= 0){
            throw new Exception("Illegal value bizId");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz WHERE biz_id=$bizId");
    }

    private function addBizDB(bizObject $bizObject){

        if (!isset($bizObject)){
            throw new Exception("bizObject value must be provided");
        }

        $sql = "
                INSERT INTO tbl_biz SET
                    biz_username = '".addslashes($bizObject->biz_username)."',
                    biz_password = '".addslashes($bizObject->biz_password)."',
                    biz_owner_id = {$bizObject->biz_owner_id},
                    biz_theme = {$bizObject->biz_theme},
                    biz_menu = {$bizObject->biz_menu},
                    biz_category_id = {$bizObject->biz_category_id},
                    biz_sub_category_id = {$bizObject->biz_sub_category_id},
                    biz_name = '".addslashes($bizObject->biz_name)."',
                    biz_short_name = '".addslashes($bizObject->biz_short_name)."',
                    biz_addr_street = '".addslashes($bizObject->biz_addr_street)."',
                    biz_addr_no = '".addslashes($bizObject->biz_addr_no)."',
                    biz_addr_town_id = {$bizObject->biz_addr_town_id},
                    biz_addr_state_id = {$bizObject->biz_addr_state_id},
                    biz_addr_country_id = {$bizObject->biz_addr_country_id},
                    biz_postal_code = '".addslashes($bizObject->biz_postal_code)."',
                    biz_office_tele = '".addslashes($bizObject->biz_office_tele)."',
                    biz_mobile_tele = '".addslashes($bizObject->biz_mobile_tele)."',
                    biz_e_mail = '".addslashes($bizObject->biz_e_mail)."',
                    biz_website = '".addslashes($bizObject->biz_website)."',
                    biz_logo = '".addslashes($bizObject->biz_logo)."',
                    biz_logo_no_shadow = {$bizObject->biz_logo_no_shadow},
                    biz_icon = '".addslashes($bizObject->biz_icon)."',
                    biz_contact_name = '".addslashes($bizObject->biz_contact_name)."',
                    biz_status = {$bizObject->biz_status},
                    biz_odot = '".addslashes($bizObject->biz_odot)."',
                    biz_need_update = {$bizObject->biz_need_update},
                    biz_mobile_serial = '".addslashes($bizObject->biz_mobile_serial)."',
                    biz_default_lng = '".addslashes($bizObject->biz_default_lng)."',
                    biz_dir = '{$bizObject->biz_dir}',
                    biz_first_mode_id = {$bizObject->biz_first_mode_id},
                    biz_addr_country = '".addslashes($bizObject->biz_addr_country)."',
                    biz_addr_state = '".addslashes($bizObject->biz_addr_state)."',
                    biz_addr_town = '".addslashes($bizObject->biz_addr_town)."',
                    biz_menu_header = '".addslashes($bizObject->biz_menu_header)."',
                    biz_menu_mobile_header = '".addslashes($bizObject->biz_menu_mobile_header)."',
                    biz_conf_mail = {$bizObject->biz_conf_mail},
                    biz_conf_code = '".addslashes($bizObject->biz_conf_code)."',
                    biz_need_publish = {$bizObject->biz_need_publish},
                    biz_tou = {$bizObject->biz_tou},
                    biz_world = {$bizObject->biz_world},
                    biz_auto_info = {$bizObject->biz_auto_info},
                    biz_use_icon = {$bizObject->biz_use_icon},
                    biz_aprove_date = '{$bizObject->biz_aprove_date}',
                    biz_apr_publish_date = '{$bizObject->biz_apr_publish_date}',
                    biz_push_left = {$bizObject->biz_push_left},
                    biz_coupon_left = {$bizObject->biz_coupon_left},
                    biz_package = {$bizObject->biz_package},
                    biz_package_type = {$bizObject->biz_package_type},
                    biz_package_date = '{$bizObject->biz_package_date}',
                    biz_plan_id = {$bizObject->biz_plan_id},
                    biz_canceled = {$bizObject->biz_canceled},
                    biz_payment_price = {$bizObject->biz_payment_price},
                    biz_paymentMethodToken = '".addslashes($bizObject->biz_paymentMethodToken)."',
                    biz_paymentMethodType = '".addslashes($bizObject->biz_paymentMethodType)."',
                    biz_cvv = '".addslashes($bizObject->biz_cvv)."',
                    biz_cc_last_digits = '".addslashes($bizObject->biz_cc_last_digits)."',
                    biz_payment_name = '".addslashes($bizObject->biz_payment_name)."',
                    biz_payment_email = '".addslashes($bizObject->biz_payment_email)."',
                    biz_card_subtype = '".addslashes($bizObject->biz_card_subtype)."',
                    biz_card_country_code = '".addslashes($bizObject->biz_card_country_code)."',
                    biz_card_vendor = '".addslashes($bizObject->biz_card_vendor)."',
                    biz_card_type = '".addslashes($bizObject->biz_card_type)."',
                    biz_card_issuer = '".addslashes($bizObject->biz_card_issuer)."',
                    biz_card_level = '".addslashes($bizObject->biz_card_level)."',
                    biz_card_bin = '".addslashes($bizObject->biz_card_bin)."',
                    biz_zooz_app_key = '".addslashes($bizObject->biz_zooz_app_key)."',
                    biz_zooz_unique_id = '".addslashes($bizObject->biz_zooz_unique_id)."',
                    biz_zooz_program_id = '".addslashes($bizObject->biz_zooz_program_id)."',
                    biz_secret = {$bizObject->biz_secret},
                    biz_time_zone = {$bizObject->biz_time_zone},
                    biz_cal_24h = {$bizObject->biz_cal_24h},
                    biz_cal_display_start = {$bizObject->biz_cal_display_start},
                    biz_cal_display_end = {$bizObject->biz_cal_display_end},
                    biz_cal_default_color = {$bizObject->biz_cal_default_color},
                    biz_cal_last_emp_id = {$bizObject->biz_cal_last_emp_id},
                    biz_cal_last_emp_name = '".addslashes($bizObject->biz_cal_last_emp_name)."',
                    biz_bad_email = {$bizObject->biz_bad_email},
                    biz_unsubscribe = {$bizObject->biz_unsubscribe},
                    biz_submit_avl_date = '{$bizObject->biz_submit_avl_date}',
                    biz_submit_mrkt_lang = '".addslashes($bizObject->biz_submit_mrkt_lang)."',
                    biz_copy_right = '".addslashes($bizObject->biz_copy_right)."',
                    biz_submit_desc = '".addslashes($bizObject->biz_submit_desc)."',
                    biz_submit_keys = '".addslashes($bizObject->biz_submit_keys)."',
                    biz_submit_sprt_url = '".addslashes($bizObject->biz_submit_sprt_url)."',
                    biz_submit_mrkt_url = '".addslashes($bizObject->biz_submit_mrkt_url)."',
                    biz_submit_priv_url = '".addslashes($bizObject->biz_submit_priv_url)."',
                    biz_submit_appl_phone = '".addslashes($bizObject->biz_submit_appl_phone)."',
                    biz_submit_intern_time = '{$bizObject->biz_submit_intern_time}',
                    biz_submit_extern_time = '{$bizObject->biz_submit_extern_time}',
                    biz_submit_completed_treatment = {$bizObject->biz_submit_completed_treatment},
                    biz_submit_icon = '".addslashes($bizObject->biz_submit_icon)."',
                    biz_submit_splash = '".addslashes($bizObject->biz_submit_splash)."',
                    biz_system_lang = '".addslashes($bizObject->biz_system_lang)."',
                    biz_step = {$bizObject->biz_step},
                    biz_sbmt_apl_sku = '".addslashes($bizObject->biz_sbmt_apl_sku)."',
                    biz_sbmt_apl_bundle_id = '".addslashes($bizObject->biz_sbmt_apl_bundle_id)."',
                    biz_sbmt_apl_bundle_suffix = '".addslashes($bizObject->biz_sbmt_apl_bundle_suffix)."',
                    biz_sbmt_apl_version = '".addslashes($bizObject->biz_sbmt_apl_version)."',
                    biz_sbmt_apl_date = '{$bizObject->biz_sbmt_apl_date}',
                    biz_sbmt_goo_version = '".addslashes($bizObject->biz_sbmt_goo_version)."',
                    biz_sbmt_goo_date = '{$bizObject->biz_sbmt_goo_date}',
                    biz_sbmt_goo_build_version_num = '".addslashes($bizObject->biz_sbmt_goo_build_version_num)."',
                    biz_sbmt_apl_build_version_num = '".addslashes($bizObject->biz_sbmt_apl_build_version_num)."',
                    biz_sbmt_goo_pack_id = '".addslashes($bizObject->biz_sbmt_goo_pack_id)."',
                    biz_sbmt_ama_version = '".addslashes($bizObject->biz_sbmt_ama_version)."',
                    biz_sbmt_ama_pack_id = '".addslashes($bizObject->biz_sbmt_ama_pack_id)."',
                    biz_sbmt_ama_suffix = '".addslashes($bizObject->biz_sbmt_ama_suffix)."',
                    biz_sbmt_price_tier = '".addslashes($bizObject->biz_sbmt_price_tier)."',
                    biz_hide_hint = {$bizObject->biz_hide_hint},
                    biz_intro = {$bizObject->biz_intro},
                    biz_rejected = {$bizObject->biz_rejected},
                    biz_custom_logo = {$bizObject->biz_custom_logo},
                    biz_manual_publish = {$bizObject->biz_manual_publish},
                    biz_facebook_id = '".addslashes($bizObject->biz_facebook_id)."',
                    biz_facebook_token = '".addslashes($bizObject->biz_facebook_token)."',
                    biz_facebook_step = {$bizObject->biz_facebook_step},
                    biz_twit_url = '".addslashes($bizObject->biz_twit_url)."',
                    biz_linkedin_url = '".addslashes($bizObject->biz_linkedin_url)."',
                    biz_youtube_url = '".addslashes($bizObject->biz_youtube_url)."',
                    biz_insta_url = '".addslashes($bizObject->biz_insta_url)."',
                    biz_ggp_url = '".addslashes($bizObject->biz_ggp_url)."',
                    biz_yelp_id = '".addslashes($bizObject->biz_yelp_id)."',
                    biz_submit_apl_date = '{$bizObject->biz_submit_apl_date}',
                    biz_submit_goo_date = '{$bizObject->biz_submit_goo_date}',
                    biz_submit_ama_date = '{$bizObject->biz_submit_ama_date}',
                    biz_pub_apl_date = '{$bizObject->biz_pub_apl_date}',
                    biz_pub_goo_date = '{$bizObject->biz_pub_goo_date}',
                    biz_pub_ama_date = '{$bizObject->biz_pub_ama_date}',
                    biz_rej_appl_date = '{$bizObject->biz_rej_appl_date}',
                    biz_rej_goog_date = '{$bizObject->biz_rej_goog_date}',
                    biz_rej_amaz_date = '{$bizObject->biz_rej_amaz_date}',
                    biz_bobile_status = {$bizObject->biz_bobile_status},
                    biz_appl_status = {$bizObject->biz_appl_status},
                    biz_goog_status = {$bizObject->biz_goog_status},
                    biz_amaz_status = {$bizObject->biz_amaz_status},
                    biz_rej_amaz_desc = '".addslashes($bizObject->biz_rej_amaz_desc)."',
                    biz_rej_goog_desc = '".addslashes($bizObject->biz_rej_goog_desc)."',
                    biz_rej_appl_desc = '".addslashes($bizObject->biz_rej_appl_desc)."',
                    biz_ok_to_submit = {$bizObject->biz_ok_to_submit},
                    biz_fb_app_id = '".addslashes($bizObject->biz_fb_app_id)."',
                    biz_appl_need_update = {$bizObject->biz_appl_need_update},
                    biz_goog_need_update = {$bizObject->biz_goog_need_update},
                    biz_amaz_need_update = {$bizObject->biz_amaz_need_update},
                    biz_sbmt_whats_new = '".addslashes($bizObject->biz_sbmt_whats_new)."',
                    biz_sbmt_market_lang = '".addslashes($bizObject->biz_sbmt_market_lang)."',
                    biz_site_builder = '".addslashes($bizObject->biz_site_builder)."',
                    biz_source = '".addslashes($bizObject->biz_source)."',
                    biz_sola_id = {$bizObject->biz_sola_id},
                    biz_credits = {$bizObject->biz_credits},
                    biz_stage = {$bizObject->biz_stage},
                    biz_app_rank = {$bizObject->biz_app_rank},
                    biz_adwords_id = '".addslashes($bizObject->biz_adwords_id)."',
                    biz_isdeleted = {$bizObject->biz_isdeleted},
                    biz_appl_need_update_list = {$bizObject->biz_appl_need_update_list},
                    biz_goog_need_update_list = {$bizObject->biz_goog_need_update_list},
                    biz_amaz_need_update_list = {$bizObject->biz_amaz_need_update_list},
                    biz_assets_refreshed = {$bizObject->biz_assets_refreshed},
                    biz_trial_start_date = '{$bizObject->biz_trial_start_date}',
                    biz_manual_submit = {$bizObject->biz_manual_submit},
                    biz_google_pdf_submit_date = '".addslashes($bizObject->biz_google_pdf_submit_date)."',
                    biz_google_submit_step = {$bizObject->biz_google_submit_step},
                    biz_on_sale = {$bizObject->biz_on_sale},
                    biz_pcas_demo = {$bizObject->biz_pcas_demo},
                    biz_temp = {$bizObject->biz_temp},
                    biz_x = {$bizObject->biz_x},
                    biz_smb = '{$bizObject->biz_smb}',
                    biz_admin_help_time = '{$bizObject->biz_admin_help_time}',
                    biz_push_type = '{$bizObject->biz_push_type}',
                    biz_firebase_project = '".addslashes($bizObject->biz_firebase_project)."',
                    biz_banner_show_date = '{$bizObject->biz_banner_show_date}',
                    biz_smshash = '".addslashes($bizObject->biz_smshash)."',
                    biz_sent_apk_link = {$bizObject->biz_sent_apk_link},
                    biz_reseller_amount_due = {$bizObject->biz_reseller_amount_due},
                    biz_from_rbclient = {$bizObject->biz_from_rbclient},
                    biz_membership_mode = '{$bizObject->biz_membership_mode}',
                    biz_stripe_account_id = '{$bizObject->biz_stripe_account}'
        ";

        return $this->db->execute($sql);
    }

    private function updateBizDB(bizObject $bizObject){

        if (!isset($bizObject->biz_id) || !is_numeric($bizObject->biz_id) || $bizObject->biz_id <= 0){
            throw new Exception("bizObject value must be provided");
        }

        $sql = "
                UPDATE tbl_biz SET
                    biz_username = '".addslashes($bizObject->biz_username)."',
                    biz_password = '".addslashes($bizObject->biz_password)."',
                    biz_owner_id = {$bizObject->biz_owner_id},
                    biz_theme = {$bizObject->biz_theme},
                    biz_menu = {$bizObject->biz_menu},
                    biz_category_id = {$bizObject->biz_category_id},
                    biz_sub_category_id = {$bizObject->biz_sub_category_id},
                    biz_name = '".addslashes($bizObject->biz_name)."',
                    biz_short_name = '".addslashes($bizObject->biz_short_name)."',
                    biz_addr_street = '".addslashes($bizObject->biz_addr_street)."',
                    biz_addr_no = '".addslashes($bizObject->biz_addr_no)."',
                    biz_addr_town_id = {$bizObject->biz_addr_town_id},
                    biz_addr_state_id = {$bizObject->biz_addr_state_id},
                    biz_addr_country_id = {$bizObject->biz_addr_country_id},
                    biz_postal_code = '".addslashes($bizObject->biz_postal_code)."',
                    biz_office_tele = '".addslashes($bizObject->biz_office_tele)."',
                    biz_mobile_tele = '".addslashes($bizObject->biz_mobile_tele)."',
                    biz_e_mail = '".addslashes($bizObject->biz_e_mail)."',
                    biz_website = '".addslashes($bizObject->biz_website)."',
                    biz_logo = '".addslashes($bizObject->biz_logo)."',
                    biz_logo_no_shadow = {$bizObject->biz_logo_no_shadow},
                    biz_icon = '".addslashes($bizObject->biz_icon)."',
                    biz_contact_name = '".addslashes($bizObject->biz_contact_name)."',
                    biz_status = {$bizObject->biz_status},
                    biz_odot = '".addslashes($bizObject->biz_odot)."',
                    biz_need_update = {$bizObject->biz_need_update},
                    biz_mobile_serial = '".addslashes($bizObject->biz_mobile_serial)."',
                    biz_default_lng = '".addslashes($bizObject->biz_default_lng)."',
                    biz_dir = '{$bizObject->biz_dir}',
                    biz_first_mode_id = {$bizObject->biz_first_mode_id},
                    biz_addr_country = '".addslashes($bizObject->biz_addr_country)."',
                    biz_addr_state = '".addslashes($bizObject->biz_addr_state)."',
                    biz_addr_town = '".addslashes($bizObject->biz_addr_town)."',
                    biz_start_date = '{$bizObject->biz_start_date}',
                    biz_menu_header = '".addslashes($bizObject->biz_menu_header)."',
                    biz_menu_mobile_header = '".addslashes($bizObject->biz_menu_mobile_header)."',
                    biz_conf_mail = {$bizObject->biz_conf_mail},
                    biz_conf_code = '".addslashes($bizObject->biz_conf_code)."',
                    biz_need_publish = {$bizObject->biz_need_publish},
                    biz_tou = {$bizObject->biz_tou},
                    biz_world = {$bizObject->biz_world},
                    biz_auto_info = {$bizObject->biz_auto_info},
                    biz_use_icon = {$bizObject->biz_use_icon},
                    biz_aprove_date = '{$bizObject->biz_aprove_date}',
                    biz_apr_publish_date = '{$bizObject->biz_apr_publish_date}',
                    biz_push_left = {$bizObject->biz_push_left},
                    biz_coupon_left = {$bizObject->biz_coupon_left},
                    biz_package = {$bizObject->biz_package},
                    biz_package_type = {$bizObject->biz_package_type},
                    biz_package_date = '{$bizObject->biz_package_date}',
                    biz_plan_id = {$bizObject->biz_plan_id},
                    biz_canceled = {$bizObject->biz_canceled},
                    biz_payment_price = {$bizObject->biz_payment_price},
                    biz_paymentMethodToken = '".addslashes($bizObject->biz_paymentMethodToken)."',
                    biz_paymentMethodType = '".addslashes($bizObject->biz_paymentMethodType)."',
                    biz_cvv = '".addslashes($bizObject->biz_cvv)."',
                    biz_cc_last_digits = '".addslashes($bizObject->biz_cc_last_digits)."',
                    biz_payment_name = '".addslashes($bizObject->biz_payment_name)."',
                    biz_payment_email = '".addslashes($bizObject->biz_payment_email)."',
                    biz_card_subtype = '".addslashes($bizObject->biz_card_subtype)."',
                    biz_card_country_code = '".addslashes($bizObject->biz_card_country_code)."',
                    biz_card_vendor = '".addslashes($bizObject->biz_card_vendor)."',
                    biz_card_type = '".addslashes($bizObject->biz_card_type)."',
                    biz_card_issuer = '".addslashes($bizObject->biz_card_issuer)."',
                    biz_card_level = '".addslashes($bizObject->biz_card_level)."',
                    biz_card_bin = '".addslashes($bizObject->biz_card_bin)."',
                    biz_zooz_app_key = '".addslashes($bizObject->biz_zooz_app_key)."',
                    biz_zooz_unique_id = '".addslashes($bizObject->biz_zooz_unique_id)."',
                    biz_zooz_program_id = '".addslashes($bizObject->biz_zooz_program_id)."',
                    biz_secret = {$bizObject->biz_secret},
                    biz_time_zone = {$bizObject->biz_time_zone},
                    biz_cal_24h = {$bizObject->biz_cal_24h},
                    biz_cal_display_start = {$bizObject->biz_cal_display_start},
                    biz_cal_display_end = {$bizObject->biz_cal_display_end},
                    biz_cal_default_color = {$bizObject->biz_cal_default_color},
                    biz_cal_last_emp_id = {$bizObject->biz_cal_last_emp_id},
                    biz_cal_last_emp_name = '".addslashes($bizObject->biz_cal_last_emp_name)."',
                    biz_bad_email = {$bizObject->biz_bad_email},
                    biz_unsubscribe = {$bizObject->biz_unsubscribe},
                    biz_submit_avl_date = '{$bizObject->biz_submit_avl_date}',
                    biz_submit_mrkt_lang = '".addslashes($bizObject->biz_submit_mrkt_lang)."',
                    biz_copy_right = '".addslashes($bizObject->biz_copy_right)."',
                    biz_submit_desc = '".addslashes($bizObject->biz_submit_desc)."',
                    biz_submit_keys = '".addslashes($bizObject->biz_submit_keys)."',
                    biz_submit_sprt_url = '".addslashes($bizObject->biz_submit_sprt_url)."',
                    biz_submit_mrkt_url = '".addslashes($bizObject->biz_submit_mrkt_url)."',
                    biz_submit_priv_url = '".addslashes($bizObject->biz_submit_priv_url)."',
                    biz_submit_appl_phone = '".addslashes($bizObject->biz_submit_appl_phone)."',
                    biz_submit_intern_time = '{$bizObject->biz_submit_intern_time}',
                    biz_submit_extern_time = '{$bizObject->biz_submit_extern_time}',
                    biz_submit_completed_treatment = {$bizObject->biz_submit_completed_treatment},
                    biz_submit_icon = '".addslashes($bizObject->biz_submit_icon)."',
                    biz_submit_splash = '".addslashes($bizObject->biz_submit_splash)."',
                    biz_system_lang = '".addslashes($bizObject->biz_system_lang)."',
                    biz_step = {$bizObject->biz_step},
                    biz_sbmt_apl_sku = '".addslashes($bizObject->biz_sbmt_apl_sku)."',
                    biz_sbmt_apl_bundle_id = '".addslashes($bizObject->biz_sbmt_apl_bundle_id)."',
                    biz_sbmt_apl_bundle_suffix = '".addslashes($bizObject->biz_sbmt_apl_bundle_suffix)."',
                    biz_sbmt_apl_version = '".addslashes($bizObject->biz_sbmt_apl_version)."',
                    biz_sbmt_apl_date = '{$bizObject->biz_sbmt_apl_date}',
                    biz_sbmt_goo_version = '".addslashes($bizObject->biz_sbmt_goo_version)."',
                    biz_sbmt_goo_date = '{$bizObject->biz_sbmt_goo_date}',
                    biz_sbmt_goo_build_version_num = '".addslashes($bizObject->biz_sbmt_goo_build_version_num)."',
                    biz_sbmt_apl_build_version_num = '".addslashes($bizObject->biz_sbmt_apl_build_version_num)."',
                    biz_sbmt_goo_pack_id = '".addslashes($bizObject->biz_sbmt_goo_pack_id)."',
                    biz_sbmt_ama_version = '".addslashes($bizObject->biz_sbmt_ama_version)."',
                    biz_sbmt_ama_pack_id = '".addslashes($bizObject->biz_sbmt_ama_pack_id)."',
                    biz_sbmt_ama_suffix = '".addslashes($bizObject->biz_sbmt_ama_suffix)."',
                    biz_sbmt_price_tier = '".addslashes($bizObject->biz_sbmt_price_tier)."',
                    biz_hide_hint = {$bizObject->biz_hide_hint},
                    biz_intro = {$bizObject->biz_intro},
                    biz_rejected = {$bizObject->biz_rejected},
                    biz_custom_logo = {$bizObject->biz_custom_logo},
                    biz_manual_publish = {$bizObject->biz_manual_publish},
                    biz_facebook_id = '".addslashes($bizObject->biz_facebook_id)."',
                    biz_facebook_token = '".addslashes($bizObject->biz_facebook_token)."',
                    biz_facebook_step = {$bizObject->biz_facebook_step},
                    biz_twit_url = '".addslashes($bizObject->biz_twit_url)."',
                    biz_linkedin_url = '".addslashes($bizObject->biz_linkedin_url)."',
                    biz_youtube_url = '".addslashes($bizObject->biz_youtube_url)."',
                    biz_insta_url = '".addslashes($bizObject->biz_insta_url)."',
                    biz_ggp_url = '".addslashes($bizObject->biz_ggp_url)."',
                    biz_yelp_id = '".addslashes($bizObject->biz_yelp_id)."',
                    biz_submit_apl_date = '{$bizObject->biz_submit_apl_date}',
                    biz_submit_goo_date = '{$bizObject->biz_submit_goo_date}',
                    biz_submit_ama_date = '{$bizObject->biz_submit_ama_date}',
                    biz_pub_apl_date = '{$bizObject->biz_pub_apl_date}',
                    biz_pub_goo_date = '{$bizObject->biz_pub_goo_date}',
                    biz_pub_ama_date = '{$bizObject->biz_pub_ama_date}',
                    biz_rej_appl_date = '{$bizObject->biz_rej_appl_date}',
                    biz_rej_goog_date = '{$bizObject->biz_rej_goog_date}',
                    biz_rej_amaz_date = '{$bizObject->biz_rej_amaz_date}',
                    biz_bobile_status = {$bizObject->biz_bobile_status},
                    biz_appl_status = {$bizObject->biz_appl_status},
                    biz_goog_status = {$bizObject->biz_goog_status},
                    biz_amaz_status = {$bizObject->biz_amaz_status},
                    biz_rej_amaz_desc = '".addslashes($bizObject->biz_rej_amaz_desc)."',
                    biz_rej_goog_desc = '".addslashes($bizObject->biz_rej_goog_desc)."',
                    biz_rej_appl_desc = '".addslashes($bizObject->biz_rej_appl_desc)."',
                    biz_ok_to_submit = {$bizObject->biz_ok_to_submit},
                    biz_fb_app_id = '".addslashes($bizObject->biz_fb_app_id)."',
                    biz_appl_need_update = {$bizObject->biz_appl_need_update},
                    biz_goog_need_update = {$bizObject->biz_goog_need_update},
                    biz_amaz_need_update = {$bizObject->biz_amaz_need_update},
                    biz_sbmt_whats_new = '".addslashes($bizObject->biz_sbmt_whats_new)."',
                    biz_sbmt_market_lang = '".addslashes($bizObject->biz_sbmt_market_lang)."',
                    biz_site_builder = '".addslashes($bizObject->biz_site_builder)."',
                    biz_source = '".addslashes($bizObject->biz_source)."',
                    biz_sola_id = {$bizObject->biz_sola_id},
                    biz_credits = {$bizObject->biz_credits},
                    biz_stage = {$bizObject->biz_stage},
                    biz_app_rank = {$bizObject->biz_app_rank},
                    biz_adwords_id = '".addslashes($bizObject->biz_adwords_id)."',
                    biz_isdeleted = {$bizObject->biz_isdeleted},
                    biz_appl_need_update_list = {$bizObject->biz_appl_need_update_list},
                    biz_goog_need_update_list = {$bizObject->biz_goog_need_update_list},
                    biz_amaz_need_update_list = {$bizObject->biz_amaz_need_update_list},
                    biz_assets_refreshed = {$bizObject->biz_assets_refreshed},
                    biz_trial_start_date = '{$bizObject->biz_trial_start_date}',
                    biz_manual_submit = {$bizObject->biz_manual_submit},
                    biz_google_pdf_submit_date = '".addslashes($bizObject->biz_google_pdf_submit_date)."',
                    biz_google_submit_step = {$bizObject->biz_google_submit_step},
                    biz_on_sale = {$bizObject->biz_on_sale},
                    biz_pcas_demo = {$bizObject->biz_pcas_demo},
                    biz_temp = {$bizObject->biz_temp},
                    biz_x = {$bizObject->biz_x},
                    biz_smb = '{$bizObject->biz_smb}',
                    biz_admin_help_time = '{$bizObject->biz_admin_help_time}',
                    biz_push_type = '{$bizObject->biz_push_type}',
                    biz_firebase_project = '".addslashes($bizObject->biz_firebase_project)."',
                    biz_banner_show_date = '{$bizObject->biz_banner_show_date}',
                    biz_smshash = '".addslashes($bizObject->biz_smshash)."',
                    biz_sent_apk_link = {$bizObject->biz_sent_apk_link},
                    biz_reseller_amount_due = {$bizObject->biz_reseller_amount_due},
                    biz_from_rbclient = {$bizObject->biz_from_rbclient},
                    biz_membership_mode = '{$bizObject->biz_membership_mode}',
                    biz_stripe_account_id = '{$bizObject->biz_stripe_account}'
                    WHERE biz_id = {$bizObject->biz_id}
        ";

        $this->db->execute($sql);
    }

    private function deleteBizByBizIdDB($bizId){
        $sql = "DELETE FROM tbl_biz WHERE biz_id = $bizId";

        $this->db->execute($sql);
    }

    private function getBizStructureGeneral(bizObject $bizObject){

        $responseObject = array();
        $responseObject["biz_id"] = $bizObject->biz_id;
        $responseObject["biz_icon"] = $bizObject->biz_icon;
        $responseObject["biz_x"] = $bizObject->biz_x;
        $responseObject["biz_logo"] = $bizObject->biz_logo;
        $responseObject["biz_submit_splash"] = $bizObject->biz_submit_splash;
        $responseObject["biz_logo_no_shadow"] = $bizObject->biz_logo_no_shadow;
        $responseObject["biz_short_name"] = $bizObject->biz_short_name;
        $responseObject["biz_theme_id"] = $bizObject->biz_theme;
        $responseObject["isActive"] = $bizObject->biz_status == 0 || $bizObject->biz_isdeleted == 1 ? 0 : 1;
        $responseObject["biz_lang"] = bizManager::getAppOwnerLang($bizObject->biz_id);
        $responseObject["bizPrivacyUrl"] = $bizObject->biz_submit_priv_url;
        $responseObject["biz_terms_url"] = utilityManager::getWhiteLabelMarketDomainForBiz($bizObject->biz_id)."/mobile/app/terms/".utilityManager::encodeToHASH($bizObject->biz_id);
        // stripe public key
        if(bizManager::checkIfBizHasStripe($bizObject->biz_id)){
            $result = bizManager::getStripeAccountForBiz($bizObject->biz_id);

            if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                $responseObject['stripe_pk'] = STRIPE_PUBLIC_KEY_SANDBOX;
            }else{
                $responseObject['stripe_pk'] = STRIPE_PUBLIC_KEY;
            }
        }else{
            $responseObject['stripe_pk'] = '';
        }

        //Check Update Software Version For each Platform
        $responseObject['bizApkUpdateType'] = enumApkUpdateType::no_update_needed;
        if($this->mobileRequestObject->OS == "Android"){
            $androidVersionData = bizManager::getAndroidVersionDataForBiz($bizObject->biz_id);

            if($androidVersionData['bizApkVersion'] > $this->mobileRequestObject->bobileVersionNumber && $this->mobileRequestObject->appid > 2){
                if($androidVersionData['hasGooglePlay'] != 1){
                    $responseObject['bizApkUpdateType'] = $androidVersionData['bizApkUpdateType'];
                }
            }else{
                $responseObject['bizApkUpdateType'] = enumApkUpdateType::no_update_needed;
            }
            $responseObject['hasGooglePlay'] = $androidVersionData['hasGooglePlay'];
            $responseObject['bizApkUpdateLocation'] = $androidVersionData['bizApkUpdateLocation'];
            $responseObject['bizApkUpdateUrl'] = $androidVersionData['bizApkUpdateUrl'];
        }else{ //IOS
            $iosVersionData = bizManager::getIosVersionDataForBiz($bizObject->biz_id,$this->mobileRequestObject->bundleId);

            if($iosVersionData['bizIpaVersion'] > $this->mobileRequestObject->papTapVersion && $this->mobileRequestObject->appid > 2){
                $responseObject['bizIpaUpdateType'] = $iosVersionData['bizIpaUpdateType'];
            }else{
                $responseObject['bizIpaUpdateType'] = enumApkUpdateType::no_update_needed;
            }
            $responseObject['bizIpaUpdateLocation'] = $iosVersionData['bizIpaUpdateLocation'];
            $responseObject['bizIpaUpdateUrl'] = $iosVersionData['bizIpaUpdateUrl'];
        }

        $bizMemberSettingsResult = $this->getBizMembersClubSettingsByID($this->bizID);
        if($bizMemberSettingsResult->code == 1){
            $bizMembersClubObj = $bizMemberSettingsResult->data;

            if(!$bizMembersClubObj->isWelcomeRewardValid()){
                $bizMembersClubObj->bmcs_welcome_entity = "points";
            }
            $responseObject["biz_welcome_entity"] = $bizMembersClubObj->bmcs_welcome_entity;
            switch($bizMembersClubObj->bmcs_welcome_entity){
                case "custom":
                    $responseObject["biz_welcome_label"] = $bizMembersClubObj->bmcs_welcome_title;
                    break;
                case "coupon":
                    $levelDataManager = new levelDataManager(26);

                    $couponResult = $levelDataManager->getLevelDataDirectByID($bizMembersClubObj->bmcs_welcome_entity_id);
                    if($couponResult->code != 1){
                        $responseObject["biz_welcome_label"] = "";
                    }
                    else{
                        $responseObject["biz_welcome_label"] = $couponResult->data["md_head"];
                    }
                    break;
                default:
                    $responseObject["biz_welcome_label"] = "";
                    break;
            }
            $responseObject["biz_welcome_grant"] = $bizMembersClubObj->bmcs_welcome_grant;
            $responseObject["biz_membership_mode"] = $bizMembersClubObj->bmcs_membership_mode;
        }
        else{
            $responseObject["biz_welcome_entity"] = "points";
            $responseObject["biz_welcome_label"] = "";
            $responseObject["biz_welcome_grant"] = $bizObject->biz_welcome_grant;
            $responseObject["biz_membership_mode"] = $bizObject->biz_membership_mode;
        }

        if($bizObject->biz_x == 0){
            $responseObject["biz_welcome_entity"] = "points";
            $responseObject["biz_welcome_grant"] = 0;
        }

        $responseObject["bizHasHash"] = $bizObject->biz_smshash != "" ? 1 : 0;
        $responseObject['biz_office_tele']=($bizObject->biz_office_tele == null) ? "" : $bizObject->biz_office_tele;
        $responseObject['biz_mobile_tele']=($bizObject->biz_mobile_tele == null) ? "" : $bizObject->biz_mobile_tele;
        $responseObject['biz_email']=($bizObject->biz_e_mail == null) ? "" : $bizObject->biz_e_mail;
        $responseObject['biz_addr_street']=($bizObject->biz_addr_street == null) ? "" : $bizObject->biz_addr_street;
        $responseObject['biz_addr_no']=($bizObject->biz_addr_no == null) ? "" : $bizObject->biz_addr_no;
        $responseObject['biz_addr_town_id']=($bizObject->biz_addr_town == null) ? "" : $bizObject->biz_addr_town;
        $responseObject['biz_addr_state_id']=($bizObject->biz_addr_state == null) ? "" : $bizObject->biz_addr_state;
        $country = utilityManager::getCountry($bizObject->biz_addr_country_id);
        $responseObject['biz_addr_country_id']=$country["country_name_eng"];
        $responseObject['biz_country_id']=($bizObject->biz_addr_country_id == null) ? "" : $bizObject->biz_addr_country_id;
        $responseObject["biz_layout"] = $bizObject->biz_menu;
        $responseObject["biz_zooz_app_key"] = $bizObject->biz_zooz_app_key;
        $responseObject["biz_zooz_unique_id"] = $bizObject->biz_zooz_unique_id;

        $responseObject["biz_has_geo_push"] = $this->getBizHasGeoPush();
        $responseObject["location_reason"] = "all";
        $responseObject["pzArray"] = $this->getBizPersonalZoneArray($bizObject);
        $responseObject["biz_stripe_account"] = $bizObject->biz_stripe_account;

        $responseObject["biz_welcome_font"] = bizManager::getWelcomeFontForLanguage($responseObject["biz_lang"]);

        //Main menu label
        $mainMenuMod = $this->getBizModByBizIDAndModID_DB($bizObject->biz_id,0);

        $responseObject['main_menu_label'] = $mainMenuMod['biz_mod_mobile_name'];

        //Default employee and service
        $calSettings = bizManager::getBizCalendarSettingsByBizID($bizObject->biz_id);
        $responseObject['biz_cal_def_employee'] = $calSettings->biz_cal_def_employee;
        $responseObject['biz_cal_def_meeting'] = $calSettings->biz_cal_def_meeting;

        //$responseObject["XXXXX"] = $bizObject->XXXXXX;
        //$responseObject["XXXXX"] = $bizObject->XXXXXX;
        //$responseObject["XXXXX"] = $bizObject->XXXXXX;
        //$responseObject["XXXXX"] = $bizObject->XXXXXX;
        //$responseObject["XXXXX"] = $bizObject->XXXXXX;
        //$responseObject["XXXXX"] = $bizObject->XXXXXX;

        return $responseObject;
    }
    private function getBizStructureTheme(bizThemeObject $themeObject, $responseObject){

        $responseObject["biz_theme_name"] = $themeObject->tmpl_name;
        $responseObject["biz_theme_font"] = $themeObject->tmpl_font;
        $responseObject["biz_theme_font_bold"] = $themeObject->tmpl_font_bold;
        $responseObject["biz_theme_color1"] = $themeObject->tmpl_color1_freestyle;
        $responseObject["biz_theme_color2"] = $themeObject->tmpl_color2_freestyle;
        $responseObject["biz_theme_color3"] = $themeObject->tmpl_color3_freestyle;
        $responseObject["biz_theme_color4"] = $themeObject->tmpl_color4_freestyle;
        $responseObject["biz_theme_color5"] = $themeObject->tmpl_color5_freestyle;
        $responseObject["biz_theme_live_background"] = $themeObject->tmpl_live_background;
        $responseObject["biz_theme_live_magic"] = $themeObject->tmpl_live_magic;
        $responseObject["biz_theme_magic_type"] = $themeObject->tmpl_sidemagic_bg_type;

        return $responseObject;
    }
    private function getBizStructureMainMenu(moduleStructureObject $moduleStructureObject,bizModObject $bizModObject, $responseObject){

        $viewTypeMainMenuToSend = "ms_view_type";
        $nibNameMainMenuToSend = "ms_nib_name";
        if($this->mobileRequestObject->android == "1"){
            $viewTypeMainMenuToSend = "ms_view_type_android";
            $nibNameMainMenuToSend = "ms_nib_name_android";
        }

        $responseObject["biz_mainmenu_view_type"] = $moduleStructureObject->$viewTypeMainMenuToSend;
        $responseObject["biz_mainmenu_nib_name"] = $moduleStructureObject->$nibNameMainMenuToSend;
        $responseObject["biz_menu_header"] = $bizModObject->biz_mod_mobile_name;

        return $responseObject;
    }
    private function getBizStructureOwner(ownerObject $ownerObject, $responseObject){

        $responseObject["owner_zooz_merchant_id"] = $ownerObject->owner_zooz_merchant_id;

        return $responseObject;
    }
    private function getBizStructureProsessor(bizPaymentProcessorObject $bizPaymentProcessorObject, $responseObject){

        $responseObject["accept_cash"] = $bizPaymentProcessorObject->bp_biz_id != 0 && $bizPaymentProcessorObject->bp_active == 1 ? 1 : 0;
        $responseObject["accept_payments"] = bizManager::checkBizHasAnyPaymentGateway($this->bizID) ? 1 : 0;

        return $responseObject;
    }

    private function getBizPaymentOptions($responseObject){
        $paymentOptions = array();

        $hasNewStripe = false;
        //Stripe option
        if(bizManager::checkIfBizHasStripe($this->bizID)){
            $result = bizManager::getStripeAccountForBiz($this->bizID);

            if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                $stripe_pk = STRIPE_PUBLIC_KEY_SANDBOX;
            }else{
                $stripe_pk = STRIPE_PUBLIC_KEY;
            }

            $option = array();
            $option['name'] = 'stripe';
            $option['data'] = array();
            $option['data']['pk'] = $stripe_pk;
            $option['data']['account'] = $responseObject["biz_stripe_account"];
            $paymentOptions[] = $option;
            $hasNewStripe = true;
        }

        $bizProcessors = $this->getBizConnectedActiveProcessors();

        foreach ($bizProcessors as $processor)
        {
            $option = array();
        	switch($processor['pr_name']){
                case 'PayPal':
                    $option['name'] = 'paypal';
                    $option['data'] = bizPaymentProcessorObject::withData($processor);

                    $paymentOptions[] = $option;
                    break;
                case 'Cash':
                    $option['name'] = 'cash';
                    $option['data'] = bizPaymentProcessorObject::withData($processor);

                    $paymentOptions[] = $option;
                    break;
                case 'Stripe':
                    if(!$hasNewStripe){
                        $option['name'] = 'creditcard';
                        $option['data'] = bizPaymentProcessorObject::withData($processor);

                        $paymentOptions[] = $option;
                    }
                    break;
            }
        }


        $responseObject['paymentOptions'] = $paymentOptions;

        return $responseObject;
    }

    private function getBizStructureStore(bizStoreSettingsObject $bizStoreSettingsObject, $responseObject){

        $responseObject["ess_currency"] = $bizStoreSettingsObject->ess_currency;
        $responseObject["ess_currency_symbol"] = $bizStoreSettingsObject->ess_currency_symbol;
        $responseObject["ess_tax"] = $bizStoreSettingsObject->ess_tax_rate;
        $responseObject["ess_checkout_comment_title"] = $bizStoreSettingsObject->ess_checkout_comment_title;
        $responseObject["ess_include_tax"] = $bizStoreSettingsObject->ess_include_tax;
        $responseObject["ess_require_shipping"] = $bizStoreSettingsObject->ess_require_shipping;

        return $responseObject;
    }
    private function getBizPersonalZoneArray(bizObject $bizObject){

        $pzSQL = "SELECT *
                        FROM tbl_biz_personal
                        WHERE tbpz_biz_id={$this->bizID}
                        AND tbpz_is_visible=1
                        AND tbpz_mod_id != 26
                        AND tbpz_blocked = 0
                        ORDER BY tbpz_index";
        $personalZone = $this->db->getTable( $pzSQL );

        $personalZoneArray = array();
        if(count($personalZone) > 0)
        {
            $ownerCountry = bizManager::getAppOwnerCountry($this->bizID);
            $isCountryStrypeSupported = bizManager::isCountryAvailbleOnStripeByCountryId($ownerCountry);
            foreach ($personalZone as $oneElement){

                $showTab = true;
                if($oneElement['tbpz_mod_id'] == 10){
                    $showTab = $isCountryStrypeSupported;
                }

                if($showTab){
                    $elementObgect = array();
                    $elementObgect["type"] = $oneElement["tbpz_type"];
                    $elementObgect["label"] = $oneElement["tbpz_label"];

                    if($bizObject->biz_x == "0" && $oneElement["tbpz_type"] == "invitefriends"){
                        continue;
                    }

                    array_push($personalZoneArray,$elementObgect);
                }
            }
        }
        return $personalZoneArray;
    }
    private function getBizModulesArray($data,$responseObject){

        $biz_modulesArray = array();
        if(count($data) > 0){

            foreach ($data as $oneMod){

                $rowId = $oneMod["row_id"];

                $levelDataManager = new levelDataManager($oneMod["biz_mod_mod_id"]);
                $resultLevel = $levelDataManager->loadDataLevelByExternalId($oneMod["biz_mod_instance_id"]);
                if($resultLevel->code == 1 && $rowId == 0 && is_array($resultLevel->data)){
                    $rowId = $resultLevel->data["md_row_id"];
                }

                $elementObgect = array();
                $combinedImg = $oneMod["biz_mod_mod_pic"];
                $largeImage = $oneMod["biz_mod_mod_large_pic"] != "" ? $oneMod["biz_mod_mod_large_pic"] : bizManager::getModuleDefaultlargeImageUrl($oneMod["biz_mod_mod_id"]);
                $elementObgect['biz_mod_instance_id']=$oneMod["biz_mod_instance_id"] == null ? "0" : $oneMod["biz_mod_instance_id"];
                $elementObgect['row_id']=$rowId;
                $elementObgect['biz_mod_mod_id']=$oneMod["biz_mod_mod_id"];
                $elementObgect['biz_mod_mod_name']=$oneMod["biz_mod_mod_name"];
                $elementObgect['biz_mod_mod_pic']=$combinedImg;
                $elementObgect['biz_mod_mod_large_pic']=$largeImage;
                $elementObgect['mod_reg_type']=$oneMod["mod_reg_type"];
                $elementObgect['biz_mod_has_reviews']=$oneMod["biz_mod_has_reviews"];
                array_push($biz_modulesArray,$elementObgect);
            }
        }
        $responseObject["biz_modules"] = $biz_modulesArray;
        return $responseObject;
    }
    private function getBizModulesArrayForAdmin($data,$responseObject){

        $biz_modulesArray = array();
        if(count($data) > 0){

            foreach ($data as $oneMod){
                $elementObgect = array();
                $combinedImg = $oneMod["biz_mod_mod_pic"];

                $elementObgect['row_id']=$oneMod["row_id"];
                $elementObgect['biz_mod_mod_id']=$oneMod["biz_mod_mod_id"];
                $elementObgect['biz_mod_active']=$oneMod["biz_mod_active"];
                $elementObgect['biz_mod_mod_name']=$oneMod["biz_mod_mod_name"];
                $elementObgect['biz_mod_mobile_name']=$oneMod["biz_mod_mobile_name"];
                $elementObgect['biz_mod_mod_pic']=$combinedImg;
                $elementObgect['mod_reg_type']=$oneMod["mod_reg_type"];

                array_push($biz_modulesArray,$elementObgect);
            }
        }
        $responseObject["modules"] = $biz_modulesArray;
        return $responseObject;
    }
    private function getBizStructureArray($data,bizThemeObject $themeObject,$responseObject){

        $nib_name = "";
        if($this->mobileRequestObject->android == "1"){
            $nib_name = "_android";
        }

        $biz_modules_structArray = array();
        if(count($data) > 0){
            foreach ($data as $oneMod){

                $modStructSQL = "SELECT ms_mod_id,ms_level_no,ms_view_type$nib_name ms_view_type,
                                    ms_nib_name$nib_name ms_nib_name
                                    from tbl_mod_struct
                                    WHERE ms_mod_id=".$oneMod["biz_mod_mod_id"]."
                                    and ms_template=".$oneMod["biz_mod_stuct"]."
                                    and ms_level_no=1
                                    order by ms_mod_id,ms_level_no";
                $modStruct = $this->db->getTable( $modStructSQL );

                if(count($modStruct) > 0)
                {
                    foreach ($modStruct as $oneModStruct)
                    {
                        $elementObgect = array();
                        if($themeObject->tmpl_transparent == "1") $oneModStruct["ms_btn_image"] = "";
                        $elementObgect['ms_mod_id']=$oneModStruct["ms_mod_id"];
                        $elementObgect['ms_level_no']=$oneModStruct["ms_level_no"];
                        $elementObgect['ms_view_type']=$oneModStruct["ms_view_type"];
                        $elementObgect['ms_nib_name']=$oneModStruct["ms_nib_name"];
                        array_push($biz_modules_structArray,$elementObgect);
                    }
                }
            }
        }
        $responseObject["biz_modules_struct"] = $biz_modules_structArray;
        return $responseObject;
    }
    private function getBizFieldsArray($responseObject){

        $biz_fieldsArray = array();

        $fieldsSQL = "SELECT cfi_field_id,cfi_req,cfi_order,fi_type,cfi_type,IF (ISNULL(cfi_label),REPLACE(fi_name,'f_',''),cfi_label) cfi_label FROM (
                        SELECT cfi_field_id, cfi_req, cfi_order, fi_type, fi_name,
                        IF (ISNULL(cfi_type),cfi_type2,cfi_type) cfi_type,
                        IF (ISNULL(cfi_label),cfi_type,cfi_label) cfi_label FROM (
	                        SELECT fi_id cfi_field_id,fi_req cfi_req,fi_order cfi_order,fi_type,fi_name,fi_name cfi_type, null cfi_type2, null cfi_label
	                        FROM tbl_fields
	                        WHERE fi_id in(11,12,13,9)
	                        UNION ALL
	                        SELECT cfi_field_id cfi_field_id, cfi_req,fi_order cfi_order, fi_type,fi_name,null, fi_name cfi_type2, cfi_label
	                        FROM tbl_fields,tbl_cust_fields
	                        WHERE fi_id = cfi_field_id
                            AND cfi_cust_id={$this->bizID}
                            ORDER BY cfi_order
                            ) as innerSelect
                            )as outerSELECT";

        $fieldsList = $this->db->getTable( $fieldsSQL );
        $lastIndex = 0;
        if(count($fieldsList) > 0)
        {
            foreach ($fieldsList as $oneField)
            {
                $elementObgect = array();
                $elementObgect['cfi_field_id']=$oneField["cfi_field_id"];
                $elementObgect['cfi_req']=$oneField["cfi_req"];
                $elementObgect['cfi_order']=$oneField["cfi_order"];
                $elementObgect['cfi_type']=$oneField["fi_type"];
                $elementObgect['cfi_label']=$oneField["cfi_label"];
                $lastIndex =$oneField["cfi_order"];
                array_push($biz_fieldsArray,$elementObgect);
            }
        }

        ////custom fields
        //$fieldsSQL = "SELECT * FROM tbl_custom_fields
        //                WHERE custom_field_biz_id = {$this->bizID}";
        //$lastIndex++;
        //$fieldsList = $this->db->getTable( $fieldsSQL );
        //if(count($fieldsList) > 0)
        //{
        //    foreach ($fieldsList as $oneField)
        //    {
        //        $elementObgect = array();
        //        $elementObgect['cfi_field_id']="custom_".$oneField["custom_field_id"];
        //        $elementObgect['cfi_req']=0;
        //        $elementObgect['cfi_order']=$lastIndex;
        //        $elementObgect['cfi_type']=$oneField["custom_field_type"];
        //        $elementObgect['cfi_label']=$oneField["custom_field_label"];
        //        $lastIndex++;
        //        array_push($biz_fieldsArray,$elementObgect);
        //    }
        //}

        $responseObject["biz_fields"] = $biz_fieldsArray;
        return $responseObject;
    }
    private function getBizTabbarArray($responseObject){

        $tabbarArray = array();

        $functionsSQL = "SELECT fu_id,
		                    bfu_label,
		                    fu_icon_name,
		                    bfu_index,
		                    fu_ios_selector,
		                    fu_and_selector,
		                    bfu_info,
		                    bfu_bool1,
		                    bfu_bool2,
		                    bfu_bool3,
		                    bfu_bool4,
                            bfu_id,
                            fu_req_mod
                    FROM tbl_biz_functions, tbl_functions
                    WHERE bfu_func_id = fu_id
                    AND bfu_biz_id = {$this->bizID}
                    AND fu_id not in (8,3)
                    order by bfu_index";

        $functionsList = $this->db->getTable( $functionsSQL );

        if(count($functionsList) > 0)
        {
            foreach ($functionsList as $oneFunction){
                $canAdd = true;
                $rowId = ($oneFunction["bfu_info"] == null) ? "" : $oneFunction["bfu_info"];

                if($oneFunction["fu_req_mod"] != 0) {

                    if($oneFunction["fu_req_mod"] == 35){

                        $rowId = $oneFunction["bfu_id"];

                        $levelDataManager = new levelDataManager(35);
                        $resultLevel = $levelDataManager->loadDataLevelByExternalId($oneFunction["bfu_id"]);
                        if($resultLevel->code == 1 && is_array($resultLevel->data)){
                            if($resultLevel->data["md_element"] == 53 && $resultLevel->data["md_info1"] == "") $canAdd= false;
                            if($resultLevel->data["md_element"] == 54 && $resultLevel->data["md_head"] == "") $canAdd= false;
                        }

                    }else{

                        $levelDataManager = new levelDataManager($oneFunction["fu_req_mod"]);
                        $resultLevel = $levelDataManager->loadDataLevelByExternalId($oneFunction["bfu_id"]);
                        if($resultLevel->code == 1 && $rowId == 0 && is_array($resultLevel->data)){

                            $fieldsCount = customFormManager::getAmountOfFormFields_DB($resultLevel->data["md_row_id"]);
                            if($fieldsCount == 0){
                                $canAdd= false;
                            }

                            $rowId = $resultLevel->data["md_row_id"];
                        }
                    }
                }

                if($oneFunction["fu_id"] == 9 || $oneFunction["fu_id"] == 10){
                    $rowId = $oneFunction["bfu_id"];

                    $levelDataManager = new levelDataManager(35);
                    $resultLevel = $levelDataManager->loadDataLevelByExternalId($oneFunction["bfu_id"]);
                    if($resultLevel->code == 1 && is_array($resultLevel->data)){
                        if($resultLevel->data["md_element"] == 53 && $resultLevel->data["md_info1"] == "") $canAdd= false;
                        if($resultLevel->data["md_element"] == 54 && $resultLevel->data["md_head"] == "") $canAdd= false;
                    }
                }

                if($this->mobileRequestObject->OS != 'Android' && $oneFunction["fu_id"] == 7 && utilityManager::contains($this->mobileRequestObject->bundleId,'bb.bb')){//don't allow rate for IPA
                    $canAdd = false;
                }

                $elementObgect = array();
                $elementObgect['fu_id']=$oneFunction["fu_id"];
                $elementObgect['fu_label']=($oneFunction["bfu_label"] == null) ? "" : $oneFunction["bfu_label"];
                $elementObgect['fu_icon']=$oneFunction["fu_icon_name"];
                $elementObgect['fu_index']=$oneFunction["bfu_index"];
                $elementObgect['fuselector']=($this->mobileRequestObject->android == "1")? $oneFunction["fu_and_selector"]: $oneFunction["fu_ios_selector"];
                $elementObgect['fu_info']=$rowId;
                $elementObgect['fu_bool1']=$oneFunction["bfu_bool1"];
                $elementObgect['fu_bool2']=$oneFunction["bfu_bool2"];
                $elementObgect['fu_bool3']=$oneFunction["bfu_bool3"];
                $elementObgect['fu_bool4']=$oneFunction["bfu_bool4"];
                if($canAdd){
                    array_push($tabbarArray,$elementObgect);
                }
            }
        }
        $responseObject["tabbar"] = $tabbarArray;
        return $responseObject;
    }
    private function getBizNavigationArray(bizModObject $bizModObject,$responseObject){

        $biz_navigationArray = array();

        $navigations = $this->db->getTable("SELECT * FROM tbl_biz_navigation WHERE bn_biz_id = {$this->bizID} ORDER BY bn_index");

        for($i=0;$i<4;$i++){
            foreach ($navigations as $nav){
                switch ($i) {
                    case '0': //content or custom action
                        if($bizModObject->biz_mod_stuct != "0" && $bizModObject->biz_mod_stuct != "91"){//display custom action
                            if($nav["bn_index"] == 0){
                                $elementObgect = $this->generateNavigationItem($nav);
                                $elementObgect["nav_index"] = "0";
                                array_push($biz_navigationArray,$elementObgect);
                            }
                        }else{ //display content button
                            if($nav["bn_index"] == 1){
                                $elementObgect = $this->generateNavigationItem($nav);
                                $elementObgect["nav_index"] = "0";
                                array_push($biz_navigationArray,$elementObgect);
                            }
                        }
                        break;
                    case '1': //personal zone
                        if($nav["bn_index"] == 2){
                            $elementObgect = $this->generateNavigationItem($nav);
                            $elementObgect["nav_index"] = "1";
                            array_push($biz_navigationArray,$elementObgect);
                        }
                        break;
                    case '2': //custom action
                        if($nav["bn_index"] == 3){
                            $elementObgect = $this->generateNavigationItem($nav);
                            $elementObgect["nav_index"] = "2";
                            array_push($biz_navigationArray,$elementObgect);
                        }
                        break;
                    case '3': //more
                        if($nav["bn_index"] == 4){
                            $elementObgect = $this->generateNavigationItem($nav);
                            $elementObgect["nav_index"] = "3";
                            array_push($biz_navigationArray,$elementObgect);
                        }
                        break;
                }
            }
        }

        $responseObject["biz_navigation"] = $biz_navigationArray;
        return $responseObject;
    }
    private function generateNavigationItem($nav){

        $elementObgect = array();

        switch ($nav["bn_entity"]) {
            case 'function':
                $moduleName = "";
                $result = $this->getBizModByBizIDAndModID($this->bizID,"31");
                if($result->code == 1){
                    $moduleName = $result->data->biz_mod_mobile_name;
                }

                $external_id = 0;

                $elementObgect['nav_entity_type']="module";
                $elementObgect['nav_entity_id']="31";
                $elementObgect['nav_index']=$nav["bn_index"];
                $elementObgect['nav_label']=$moduleName;
                $elementObgect['nav_icon']="mod31.svg";
                $elementObgect['nav_selector']="";
                $elementObgect['nav_selector_android']="";
                $elementObgect['nav_external_id']="31";
                break;

            case 'module':

                $moduleName = "";
                $result = $this->getBizModByBizIDAndModID($this->bizID,$nav["bn_entity_id"]);
                if($result->code == 1){
                    $moduleName = $result->data->biz_mod_mobile_name;
                }

                $external_id = 0;
                if($nav["bn_entity_id"] == 35){
                    $browserRow = $this->db->getRow("select * from tbl_mod_data35 where md_external_type = 'module' AND md_external_id = {$nav["bn_external_id"]}");
                    if($browserRow["md_element"] == 53 && $browserRow["md_info1"] != ""){
                        $external_id=$nav["bn_external_id"];
                    }
                    else if($browserRow["md_element"] == 54 && $browserRow["md_head"] != ""){
                        $external_id=$nav["bn_external_id"];
                    }
                    $moduleName = $this->db->getVal("SELECT biz_mod_mobile_name FROM tbl_biz_mod WHERE biz_mod_id = $external_id");
                }
                else if($nav["bn_entity_id"] == 32){

                    $formRow = $this->db->getRow("select * from tbl_mod_data32 where md_external_type = 'module' AND md_external_id = {$nav["bn_external_id"]}");
                    if($formRow != "" && $external_id == 0){
                        $fieldsCount = $this->db->getVal("select count(fld_id) from tbl_form_fields where fld_form_id = {$formRow['md_row_id']}");
                        if($fieldsCount != 0){
                            $external_id = $formRow['md_row_id'];
                        }
                        $moduleName = $formRow['md_head'];
                    }
                }

                $elementObgect['nav_entity_type']=$nav["bn_entity"];
                $elementObgect['nav_entity_id']=$nav["bn_entity_id"];
                $elementObgect['nav_index']=$nav["bn_index"];
                $elementObgect['nav_label']=$moduleName;
                $elementObgect['nav_icon']="mod".$nav["bn_entity_id"].".svg";
                $elementObgect['nav_selector']="";
                $elementObgect['nav_selector_android']="";
                $elementObgect['nav_external_id']=$external_id;
                break;

            case 'menu':

                    $elementObgect['nav_entity_type']=$nav["bn_entity"];
                    $elementObgect['nav_entity_id']=$nav["bn_entity_id"];
                    $elementObgect['nav_index']=$nav["bn_index"];
                    $elementObgect['nav_label']=(isset($nav["bn_label"])) ? $nav["bn_label"]: "";
                    $elementObgect['nav_icon']=(isset($nav["bn_icon"])) ? $nav["bn_icon"]: "";
                    $elementObgect['nav_selector']=$nav["bn_selector_ios"];
                    $elementObgect['nav_selector_android']=$nav["bn_selector_and"];
                break;
        }
        return $elementObgect;
    }
    private function getBizEngagementArray(bizModObject $bizModObject,$responseObject){

        $engagements = $this->db->getTable("SELECT * FROM tbl_biz_engagement
                                                WHERE bng_biz_id = {$this->bizID}
                                                ORDER BY bng_bubble_size");

        $engagement_dataArray = array();

        if (count($engagements)> 0){

            foreach ($engagements as $eng) {

                $elementObgect = array();
                switch ($eng["bng_entity_type"]) {
                    case 'engage':
                        $result = $this->getBizEngagementByID($eng["bng_entity_id"]);
                        if($result->code == 1){
                            $engagementData = $result->data;

                            $elementObgect['eng_id']=$eng["bng_id"];
                            $elementObgect['eng_bubble_size']=$eng["bng_bubble_size"];
                            $elementObgect['eng_entity']=$eng["bng_entity_type"];
                            $elementObgect['eng_entity_id']= $engagementData->eng_entity_id;
                            $elementObgect['eng_external_id']= $engagementData->bng_external_id;
                            $elementObgect['eng_type']=$engagementData->eng_type;
                            $elementObgect['action_selector']=$engagementData->eng_action_selector;
                            $elementObgect['action_selector_android']=$engagementData->eng_action_selector;
                            $elementObgect['regular_selector']=$engagementData->eng_regular_selector;
                            $elementObgect['eng_regular_selector_freestyle']=$engagementData->eng_regular_selector_freestyle;
                            $elementObgect['regular_selector_android']=$engagementData->eng_regular_selector_android;
                            $elementObgect['symbol']="mod{$engagementData->eng_entity_id}.svg";
                            $elementObgect['title']=$eng["bng_title"];
                            $elementObgect['text']=$eng["bng_text"];
                            $elementObgect['regular_text']=$eng["bng_succ_desc"];
                            $elementObgect['regular_title']=$eng["bng_succ_title"];

                            $paramsData = $this->loadBizEngagementParams_DB($eng["bng_id"]);
                            $paramsArray = array();
                            if (count($paramsData) > 0){
                                foreach ($paramsData as $param) {
                                    $paramObject = array();
                                    $paramObject["paramName"] = $param["bep_param_name"];
                                    $paramObject["paramValue"] = $param["bep_param_value"];
                                    array_push($paramsArray,$paramObject);
                                }
                            }
                            $elementObgect['eng_params'] = $paramsArray;
                            $elementObgect['source'] = 'engage';
                        }

                        break;

                    case 'module':

                        $moduleName = "";
                        if($eng["bng_entity_id"] == 35){
                            $moduleName = $eng['bng_title'];
                        }
                        else{
                            $result = $this->getBizModByBizIDAndModID($this->bizID,$eng["bng_entity_id"]);
                            if($result->code == 1){
                                $moduleName = $result->data->biz_mod_mobile_name;
                            }
                        }


                        if($eng["bng_external_id"] != 0 && $eng["bng_entity_id"] == 32){

                            $levelDataManager = new levelDataManager(32);
                            $resultLevel = $levelDataManager->loadDataLevelByExternalId($eng["bng_external_id"]);
                            if($resultLevel->code == 1 && is_array($resultLevel->data)){
                                $eng["bng_external_id"] = $resultLevel->data["md_row_id"];
                            }else{
                                $eng["bng_external_id"] = 0;
                            }
                        }

                        $elementObgect['eng_id']="0";
                        $elementObgect['eng_bubble_size']=$eng["bng_bubble_size"];
                        $elementObgect['eng_entity']=$eng["bng_entity_type"];
                        $elementObgect['eng_entity_id']=$eng["bng_entity_id"];
                        $elementObgect['eng_external_id']= $eng["bng_external_id"];
                        $elementObgect['eng_type']="";
                        $elementObgect['action_selector']="";
                        $elementObgect['action_selector_android']="openModule";
                        $elementObgect['regular_selector']="";
                        $elementObgect['regular_selector_android']="";
                        $elementObgect['symbol']="";
                        $elementObgect['title']=$moduleName;
                        $elementObgect['text']="";
                        $elementObgect['regular_text']="";
                        $elementObgect['regular_title']="";
                        $elementObgect['source'] = 'module';
                        break;


                    default:

                        $result = $this->getBizFunctionByID($eng["bng_external_id"]);
                        if($result->code == 1){
                            $functionData = $result->data;

                            if($eng["bng_external_id"] != 0 && $eng["bng_entity_id"] == 11){
                                $levelDataManager = new levelDataManager(32);
                                $resultLevel = $levelDataManager->loadDataLevelByExternalId($eng["bng_external_id"]);
                                if($resultLevel->code == 1 && is_array($resultLevel->data)){
                                    $eng["bng_external_id"] = $resultLevel->data["md_row_id"];
                                }else{
                                    $eng["bng_external_id"] = 0;
                                }
                            }

                            $elementObgect['eng_id']="0";
                            $elementObgect['eng_bubble_size']=$eng["bng_bubble_size"];
                            $elementObgect['eng_entity']=$eng["bng_entity_type"];
                            $elementObgect['eng_entity_id']=$eng["bng_entity_id"];
                            $elementObgect['eng_external_id']= $eng["bng_external_id"];
                            $elementObgect['eng_type']="";
                            $elementObgect['action_selector']=$functionData->fu_ios_selector;
                            $elementObgect['action_selector_android']=$functionData->fu_and_selector;
                            $elementObgect['regular_selector']="";
                            $elementObgect['regular_selector_android']="";
                            $elementObgect['symbol']="";
                            $elementObgect['title']=$functionData->bfu_label;
                            $elementObgect['text']="";
                            $elementObgect['regular_text']=$functionData->bfu_info;
                            $elementObgect['regular_title']="";
                            $elementObgect['source'] = 'function';
                        }
                        break;

                }
                array_push($engagement_dataArray,$elementObgect);
            }
        }

        $responseObject["engagement_data"] = $engagement_dataArray;
        return $responseObject;
    }
    private function getBizHasGeoPush(){

        $geoPushCount = $this->db->getVal("SELECT count(pu_id)
                                                FROM tbl_push
                                                WHERE pu_based_location=1
                                                AND pu_active = 1
                                                AND pu_biz_id={$this->bizID}");

        $geoSubCount = $this->db->getVal("SELECT COUNT(*) FROM tbl_mod_data10,tbl_subscription_usage_mech
                    WHERE md_row_id = sum_sub_id
                    AND md_biz_id = {$this->bizID}
                    AND sum_geo = 1");

        return ($geoSubCount + $geoPushCount) > 0 ? 1 : 0;
    }
    private function getDefaultBookingSettings(bizCalendarSettingsObject $calendarSettingsObject,$responseObject){

        $responseObject["biz_cal_def_employee"] = $calendarSettingsObject->biz_cal_def_employee;
        $responseObject["biz_cal_def_meeting"] = $calendarSettingsObject->biz_cal_def_meeting;

        return $responseObject;

    }
    private function getSocialPages($responseObject){
        $biz_facebook_page = $this->db->getVal("select md_head from tbl_mod_data19 where md_biz_id=$this->bizID and md_element=16");
        $biz_twitter_page = $this->db->getVal("select md_head from tbl_mod_data19 where md_biz_id=$this->bizID and md_element=17");
        $biz_google_page = $this->db->getVal("select md_head from tbl_mod_data19 where md_biz_id=$this->bizID and md_element=18");
        $biz_instagram_page = $this->db->getVal("select md_head from tbl_mod_data19 where md_biz_id=$this->bizID and md_element=19");
        $biz_yelp_page = $this->db->getVal("select md_head from tbl_mod_data19 where md_biz_id=$this->bizID and md_element=20");

        $responseObject["biz_facebook_page"] = ($biz_facebook_page != "") ? "https://facebook.com/$biz_facebook_page" : "";
        $responseObject["biz_twitter"] = ($biz_twitter_page != "") ? "https://twitter.com/$biz_twitter_page" : "";
        $responseObject["biz_google_plus"] = ($biz_google_page != "") ? "https://plus.google.com/$biz_google_page" : "";
        $responseObject["biz_instagram"] = ($biz_instagram_page != "") ? "https://instagram.com/$biz_instagram_page" : "";
        $responseObject["biz_yelp"] = ($biz_yelp_page != "") ? "https://yelp.com/$biz_yelp_page" : "";

        return $responseObject;
    }
    private function getWorkingHours($responseObject){
        $workHourRows = $this->getBizWorkHours();

        $workHours = array();

        foreach ($workHourRows as $workHourRow)
        {
        	$workDay = bizCalendarHoursObject::withData($workHourRow);

            $workHours[] = $workDay->getArrayForBizAdmin();
        }

        $responseObject["work_hours"] = $workHours;

        return $responseObject;
    }


    /**********************************/
    /*   Private App Store function   */
    /**********************************/

    private function getBizAppsoresDB(){

        $sql = "SELECT tbl_appstores.*,
                CASE
                WHEN tbl_biz_appstores.id IS NULL THEN 0
                ELSE 1 END as connected
                FROM tbl_appstores LEFT JOIN tbl_biz_appstores ON tbl_appstores.id = appstore_id AND tbl_biz_appstores.biz_id = {$this->bizID}
                ORDER BY tbl_appstores.id";

        return $this->db->getTable($sql);
    }

    private function getAppstoreStageListDB(){
        return $this->db->getTable("SELECT * FROM tbl_big_markets_stages");
    }

    /**
     * Return stage object from collection by provided ID
     * @param array $stages
     * @param int $stageId
     * @return bigMarketStages
     */
    private function getStageByIdFromArray($stages, $stageId){

        $stageObject = new bigMarketStages();

        foreach ($stages as $stage)
        {
        	if($stage->bms_stage_id == $stageId){
                $stageObject = $stage;
                break;
            }
        }

        return $stageObject;
    }

    /**
     * Reture the stage object (status) of the app in Apple Store
     * @param mixed $stages
     * @param bizObject $bizObject
     * @return bigMarketStages
     */
    private function getStageForApple($stages,bizObject $bizObject){

        if(!$this->hasActiveAppleAccount($bizObject)){

            return $this->getStageByIdFromArray($stages,enumStage::DEVELOPER_ACCOUNT_REQUIRED);

        }else{

            switch ($bizObject->biz_appl_status) {
                case '1':
                    return $this->getStageByIdFromArray($stages,enumStage::IN_SUBMISSION);
                case '2':
                    return $this->getStageByIdFromArray($stages,enumStage::APPROVED_LIVE);
                case '3':
                    return $this->getStageByIdFromArray($stages,enumStage::REJECTED);
                default:
                    return $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
            }
        }
    }

    /**
     * Reture the stage object (status) of the app in Amazon
     * @param mixed $stages
     * @param bizObject $bizObject
     * @return bigMarketStages
     */
    private function getStageForAmazon($stages,bizObject $bizObject){

        switch ($bizObject->biz_amaz_status) {
            case '1':
                return $this->getStageByIdFromArray($stages,enumStage::IN_SUBMISSION);
            case '2':
                return $this->getStageByIdFromArray($stages,enumStage::APPROVED_LIVE);
            case '3':
                return $this->getStageByIdFromArray($stages,enumStage::REJECTED);
            default:
                return $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
        }
    }

    /**
     * Reture the stage object (status) of the app in Google Play
     * @param mixed $stages
     * @param bizObject $bizObject
     * @return bigMarketStages
     */
    private function getStageForGoogle($stages,bizObject $bizObject){

        if(!$this->hasActiveGoogleAccount($bizObject)){

            return $this->getStageByIdFromArray($stages,enumStage::DEVELOPER_ACCOUNT_REQUIRED);

        }else{

            switch ($bizObject->biz_goog_status) {
                case '1':
                    return $this->getStageByIdFromArray($stages,enumStage::IN_SUBMISSION);
                case '2':
                    return $this->getStageByIdFromArray($stages,enumStage::APPROVED_LIVE);
                case '3':
                    return $this->getStageByIdFromArray($stages,enumStage::REJECTED);
                default:
                    return $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
            }
        }
    }

    /**
     * Reture the stage object (status) of the app in Google Play
     * @param mixed $stages
     * @param bizObject $bizObject
     * @return bigMarketStages
     */
    private function getStageForBobileMarket($stages,bizObject $bizObject){

        switch ($bizObject->biz_bobile_status) {
            case '1':
                return $this->getStageByIdFromArray($stages,enumStage::IN_SUBMISSION);
            case '2':
                return $this->getStageByIdFromArray($stages,enumStage::APPROVED_LIVE);
            case '3':
                return $this->getStageByIdFromArray($stages,enumStage::REJECTED);
            default:
                return $this->getStageByIdFromArray($stages,enumStage::NOT_PUBLISHED);
        }
    }

    /************************************* */
    /*   BASIC BIZCALENDARHOURS - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizCalendarHoursObject to DB
     * Return Data = new bizCalendarHoursObject ID
     * @param bizCalendarHoursObject $bizCalendarHoursObj
     * @return resultObject
     */
    public function addBizCalendarHours(bizCalendarHoursObject $bizCalendarHoursObj){
        try{
            $newId = $this->addBizCalendarHoursDB($bizCalendarHoursObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizCalendarHoursObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizCalendarHours from DB for provided ID
     * * Return Data = bizCalendarHoursObject
     * @param int $bizCalendarHoursId
     * @return resultObject
     */
    public function getBizCalendarHoursByID($bizCalendarHoursId){

        try {
            $bizCalendarHoursData = $this->loadBizCalendarHoursFromDB($bizCalendarHoursId);

            $bizCalendarHoursObj = bizCalendarHoursObject::withData($bizCalendarHoursData);
            $result = resultObject::withData(1,'',$bizCalendarHoursObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizCalendarHoursId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizCalendarHours in DB
     * @param bizCalendarHoursObject $bizCalendarHoursObj
     * @return resultObject
     */
    public function updateBizCalendarHours(bizCalendarHoursObject $bizCalendarHoursObj){
        try{
            $this->upateBizCalendarHoursDB($bizCalendarHoursObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizCalendarHoursObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizCalendarHours from DB
     * @param int $bizCalendarHoursID
     * @return resultObject
     */
    public function deleteBizCalendarHoursById($bizCalendarHoursID){
        try{
            $this->deleteBizCalendarHoursByIdDB($bizCalendarHoursID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizCalendarHoursID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZCALENDARHOURS - DB METHODS           */
    /************************************* */

    private function addBizCalendarHoursDB(bizCalendarHoursObject $obj){

        if (!isset($obj)){
            throw new Exception("bizCalendarHoursObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_cal_hours SET
                                        bch_biz_id = {$obj->bch_biz_id},
                                        bch_day_no = {$obj->bch_day_no},
                                        bch_day_name = '".addslashes($obj->bch_day_name)."',
                                        bch_begin_hour = {$obj->bch_begin_hour},
                                        bch_finish_hour = {$obj->bch_finish_hour},
                                        bch_working_day = {$obj->bch_working_day}");
        return $newId;
    }

    private function loadBizCalendarHoursFromDB($bizCalendarHoursID){

        if (!is_numeric($bizCalendarHoursID) || $bizCalendarHoursID <= 0){
            throw new Exception("Illegal value bizCalendarHoursID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_cal_hours WHERE bch_id = $bizCalendarHoursID");
    }

    private function upateBizCalendarHoursDB(bizCalendarHoursObject $obj){

        if (!isset($obj->bch_id) || !is_numeric($obj->bch_id) || $obj->bch_id <= 0){
            throw new Exception("bizCalendarHoursObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_cal_hours SET
                                bch_biz_id = {$obj->bch_biz_id},
                                bch_day_no = {$obj->bch_day_no},
                                bch_day_name = '".addslashes($obj->bch_day_name)."',
                                bch_begin_hour = {$obj->bch_begin_hour},
                                bch_finish_hour = {$obj->bch_finish_hour},
                                bch_working_day = {$obj->bch_working_day}
                                WHERE bch_id = {$obj->bch_id}");
    }

    private function deleteBizCalendarHoursByIdDB($bizCalendarHoursID){

        if (!is_numeric($bizCalendarHoursID) || $bizCalendarHoursID <= 0){
            throw new Exception("Illegal value bizCalendarHoursID");
        }

        $this->db->execute("DELETE FROM tbl_biz_cal_hours WHERE bch_id = $bizCalendarHoursID");
    }

    private function getBizWorkHours(){
        return $this->db->getTable("SELECT bch_id,
                                        bch_biz_id,
                                        bch_day_no,
                                        bch_day_name,
                                        bch_begin_hour,
                                        bch_finish_hour,
                                        bch_working_day,
                                        day_start_hour,
                                        day_finish_hour
                                 FROM tbl_biz_cal_hours,
                                 (SELECT hour_id starter_id, hour_start day_start_hour FROM calendar_hours) ST,
                                 (SELECT hour_id finisher_id, hour_start day_finish_hour FROM calendar_hours) FI
                                 WHERE starter_id = bch_begin_hour
                                 AND finisher_id = bch_finish_hour
                                 AND bch_biz_id = $this->bizID");
    }

    /************************************* */
    /*   BASIC BIZENGAGEMENT - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizEngagementObject to DB
     * Return Data = new bizEngagementObject ID
     * @param bizEngagementObject $bizEngagementObj
     * @return resultObject
     */
    public function addBizEngagement(bizEngagementObject $bizEngagementObj){
        try{
            $newId = $this->addBizEngagementDB($bizEngagementObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizEngagementObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizEngagement from DB for provided ID
     * * Return Data = bizEngagementObject
     * @param int $bizEngagementId
     * @return resultObject
     */
    public function getBizEngagementByID($bizEngagementId){

        try {
            $bizEngagementData = $this->loadBizEngagementFromDB($bizEngagementId);

            $bizEngagementObj = bizEngagementObject::withData($bizEngagementData);
            $result = resultObject::withData(1,'',$bizEngagementObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizEngagementId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizEngagement in DB
     * @param bizEngagementObject $bizEngagementObj
     * @return resultObject
     */
    public function updateBizEngagement(bizEngagementObject $bizEngagementObj){
        try{
            $this->upateBizEngagementDB($bizEngagementObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizEngagementObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizEngagement from DB
     * @param int $bizEngagementID
     * @return resultObject
     */
    public function deleteBizEngagementById($bizEngagementID){
        try{
            $this->deleteBizEngagementByIdDB($bizEngagementID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizEngagementID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZENGAGEMENT - DB METHODS           */
    /************************************* */

    private function addBizEngagementDB(bizEngagementObject $obj){

        if (!isset($obj)){
            throw new Exception("bizEngagementObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_engagement SET
                                        eng_name = '".addslashes($obj->eng_name)."',
                                        eng_entity = '".addslashes($obj->eng_entity)."',
                                        eng_entity_id = {$obj->eng_entity_id},
                                        eng_type = '".addslashes($obj->eng_type)."',
                                        eng_default_symb = '".addslashes($obj->eng_default_symb)."',
                                        eng_default_top_title = '".addslashes($obj->eng_default_top_title)."',
                                        eng_default_text = '".addslashes($obj->eng_default_text)."',
                                        eng_action_selector = '".addslashes($obj->eng_action_selector)."',
                                        eng_default_regular_text = '".addslashes($obj->eng_default_regular_text)."',
                                        eng_regular_selector = '".addslashes($obj->eng_regular_selector)."',
                                        eng_regular_selector_freestyle = '".addslashes($obj->eng_regular_selector_freestyle)."',
                                        eng_regular_selector_android = '".addslashes($obj->eng_regular_selector_android)."',
                                        eng_param_recrod_id = '".addslashes($obj->eng_param_recrod_id)."',
                                        eng_default_succ_title = '".addslashes($obj->eng_default_succ_title)."',
                                        eng_default_succ_desc = '".addslashes($obj->eng_default_succ_desc)."',
                                        eng_select_url = '".addslashes($obj->eng_select_url)."'
                                                 ");
        return $newId;
    }

    private function loadBizEngagementFromDB($bizEngagementID){

        if (!is_numeric($bizEngagementID) || $bizEngagementID <= 0){
            throw new Exception("Illegal value bizEngagementID");
        }

        return $this->db->getRow("SELECT * FROM tbl_engagement WHERE eng_id = $bizEngagementID");
    }

    private function upateBizEngagementDB(bizEngagementObject $obj){

        if (!isset($obj->eng_id) || !is_numeric($obj->eng_id) || $obj->eng_id <= 0){
            throw new Exception("bizEngagementObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_engagement SET
                                eng_name = '".addslashes($obj->eng_name)."',
                                eng_entity = '".addslashes($obj->eng_entity)."',
                                eng_entity_id = {$obj->eng_entity_id},
                                eng_type = '".addslashes($obj->eng_type)."',
                                eng_default_symb = '".addslashes($obj->eng_default_symb)."',
                                eng_default_top_title = '".addslashes($obj->eng_default_top_title)."',
                                eng_default_text = '".addslashes($obj->eng_default_text)."',
                                eng_action_selector = '".addslashes($obj->eng_action_selector)."',
                                eng_default_regular_text = '".addslashes($obj->eng_default_regular_text)."',
                                eng_regular_selector = '".addslashes($obj->eng_regular_selector)."',
                                eng_regular_selector_freestyle = '".addslashes($obj->eng_regular_selector_freestyle)."',
                                eng_regular_selector_android = '".addslashes($obj->eng_regular_selector_android)."',
                                eng_param_recrod_id = '".addslashes($obj->eng_param_recrod_id)."',
                                eng_default_succ_title = '".addslashes($obj->eng_default_succ_title)."',
                                eng_default_succ_desc = '".addslashes($obj->eng_default_succ_desc)."',
                                eng_select_url = '".addslashes($obj->eng_select_url)."'
                                WHERE eng_id = {$obj->eng_id}
                                         ");
    }

    private function deleteBizEngagementByIdDB($bizEngagementID){

        if (!is_numeric($bizEngagementID) || $bizEngagementID <= 0){
            throw new Exception("Illegal value bizEngagementID");
        }

        $this->db->execute("DELETE FROM tbl_engagement WHERE eng_id = $bizEngagementID");
    }

    private function loadBizEngagementParams_DB($bizEngagementID){

        if (!is_numeric($bizEngagementID) || $bizEngagementID <= 0){
            throw new Exception("Illegal value bizEngagementID");
        }

        return $this->db->getTable("SELECT * FROM tbl_biz_engage_params WHERE bep_bng_id = $bizEngagementID");
    }


    /************************************* */
    /*   BASIC BIZFUNCTION - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizFunctionObject to DB
     * Return Data = new bizFunctionObject ID
     * @param bizFunctionObject $bizFunctionObj
     * @return resultObject
     */
    public function addBizFunction(bizFunctionObject $bizFunctionObj){
        try{
            $newId = $this->addBizFunctionDB($bizFunctionObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizFunctionObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizFunction from DB for provided ID
     * * Return Data = bizFunctionObject
     * @param int $bizFunctionId
     * @return resultObject
     */
    public function getBizFunctionByID($bizFunctionId){

        try {
            $bizFunctionData = $this->loadBizFunctionFromDB($bizFunctionId);

            $bizFunctionObj = bizFunctionObject::withData($bizFunctionData);
            $result = resultObject::withData(1,'',$bizFunctionObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizFunctionId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizFunction in DB
     * @param bizFunctionObject $bizFunctionObj
     * @return resultObject
     */
    public function updateBizFunction(bizFunctionObject $bizFunctionObj){
        try{
            $this->upateBizFunctionDB($bizFunctionObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizFunctionObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizFunction from DB
     * @param int $bizFunctionID
     * @return resultObject
     */
    public function deleteBizFunctionById($bizFunctionID){
        try{
            $this->deleteBizFunctionByIdDB($bizFunctionID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizFunctionID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZFUNCTION - DB METHODS           */
    /************************************* */

    private function addBizFunctionDB(bizFunctionObject $obj){

        if (!isset($obj)){
            throw new Exception("bizFunctionObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_functions SET
                                        bfu_func_id = {$obj->bfu_func_id},
                                        bfu_biz_id = {$obj->bfu_biz_id},
                                        bfu_label = '".addslashes($obj->bfu_label)."',
                                        bfu_info = '".addslashes($obj->bfu_info)."',
                                        bfu_bool1 = {$obj->bfu_bool1},
                                        bfu_bool2 = {$obj->bfu_bool2},
                                        bfu_bool3 = {$obj->bfu_bool3},
                                        bfu_bool4 = {$obj->bfu_bool4},
                                        bfu_index = {$obj->bfu_index},
                                        bfu_visible = {$obj->bfu_visible}
                                                 ");
        return $newId;
    }

    private function loadBizFunctionFromDB($bizFunctionID){

        if (!is_numeric($bizFunctionID) || $bizFunctionID <= 0){
            throw new Exception("Illegal value bizFunctionID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_functions,tbl_functions
                                    WHERE bfu_func_id = fu_id
                                    AND bfu_id = $bizFunctionID");
    }

    private function upateBizFunctionDB(bizFunctionObject $obj){

        if (!isset($obj->bfu_id) || !is_numeric($obj->bfu_id) || $obj->bfu_id <= 0){
            throw new Exception("bizFunctionObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_functions SET
                                bfu_func_id = {$obj->bfu_func_id},
                                bfu_biz_id = {$obj->bfu_biz_id},
                                bfu_label = '".addslashes($obj->bfu_label)."',
                                bfu_info = '".addslashes($obj->bfu_info)."',
                                bfu_bool1 = {$obj->bfu_bool1},
                                bfu_bool2 = {$obj->bfu_bool2},
                                bfu_bool3 = {$obj->bfu_bool3},
                                bfu_bool4 = {$obj->bfu_bool4},
                                bfu_index = {$obj->bfu_index},
                                bfu_visible = {$obj->bfu_visible}
                                WHERE bfu_id = {$obj->bfu_id}
                                         ");
    }

    private function deleteBizFunctionByIdDB($bizFunctionID){

        if (!is_numeric($bizFunctionID) || $bizFunctionID <= 0){
            throw new Exception("Illegal value bizFunctionID");
        }

        $this->db->execute("DELETE FROM tbl_biz_functions WHERE bfu_id = $bizFunctionID");
    }

    /************************************* */
    /*   BASIC BIZMOD - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizModObject to DB
     * Return Data = new bizModObject ID
     * @param bizModObject $bizModObj
     * @return resultObject
     */
    public function addBizMod(bizModObject $bizModObj){
        try{
            $newId = $this->addBizModDB($bizModObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizModObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizMod from DB for provided ID
     * * Return Data = bizModObject
     * @param int $bizModId
     * @return resultObject
     */
    public function getBizModByID($bizModId){

        try {
            $bizModData = $this->loadBizModFromDB($bizModId);

            $bizModObj = bizModObject::withData($bizModData);
            $result = resultObject::withData(1,'',$bizModObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizModId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizMod from DB for provided Biz ID and Mod Id
     * @param mixed $bizId
     * @param mixed $bizModId
     * @return resultObject
     */
    public function getBizModByBizIDAndModID($bizId,$bizModId){

        try {
            $bizModData = $this->getBizModByBizIDAndModID_DB($bizId,$bizModId);

            $bizModObj = bizModObject::withData($bizModData);
            $result = resultObject::withData(1,'',$bizModObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizModId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizMod in DB
     * @param bizModObject $bizModObj
     * @return resultObject
     */
    public function updateBizMod(bizModObject $bizModObj){
        try{
            $this->upateBizModDB($bizModObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizModObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizMod from DB
     * @param int $bizModID
     * @return resultObject
     */
    public function deleteBizModById($bizModID){
        try{
            $this->deleteBizModByIdDB($bizModID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizModID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZMOD - DB METHODS           */
    /************************************* */

    private function addBizModDB(bizModObject $obj){

        if (!isset($obj)){
            throw new Exception("bizModObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_mod SET
                                        biz_mod_biz_id = {$obj->biz_mod_biz_id},
                                        biz_mod_mod_id = {$obj->biz_mod_mod_id},
                                        biz_mod_mod_name = '".addslashes($obj->biz_mod_mod_name)."',
                                        biz_mod_mobile_name = '".addslashes($obj->biz_mod_mobile_name)."',
                                        biz_mod_mod_pic = '".addslashes($obj->biz_mod_mod_pic)."',
                                        biz_mod_templ_id = {$obj->biz_mod_templ_id},
                                        biz_mod_active = {$obj->biz_mod_active},
                                        biz_mod_stuct = {$obj->biz_mod_stuct},
                                        biz_mod_intro = {$obj->biz_mod_intro},
                                        biz_mod_index = {$obj->biz_mod_index},
                                        biz_mod_external_type = '{$obj->biz_mod_external_type}',
                                        biz_mod_has_reviews = {$obj->biz_mod_has_reviews},
                                        biz_mod_can_delete = {$obj->biz_mod_can_delete}
                                                 ");
        return $newId;
    }

    private function loadBizModFromDB($bizModID){

        if (!is_numeric($bizModID) || $bizModID <= 0){
            throw new Exception("Illegal value bizModID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_mod WHERE biz_mod_id = $bizModID");
    }

    private function getBizModByBizIDAndModID_DB($bizId,$bizModID){

        if (!is_numeric($bizModID) || $bizModID < 0){
            throw new Exception("Illegal value bizModID");
        }

        if (!is_numeric($bizId) || $bizId <= 0){
            throw new Exception("Illegal value bizId");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_mod WHERE biz_mod_biz_id=$bizId AND biz_mod_mod_id = $bizModID");
    }

    private function upateBizModDB(bizModObject $obj){

        if (!isset($obj->biz_mod_id) || !is_numeric($obj->biz_mod_id) || $obj->biz_mod_id <= 0){
            throw new Exception("bizModObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_mod SET
                                biz_mod_biz_id = {$obj->biz_mod_biz_id},
                                biz_mod_mod_id = {$obj->biz_mod_mod_id},
                                biz_mod_mod_name = '".addslashes($obj->biz_mod_mod_name)."',
                                biz_mod_mobile_name = '".addslashes($obj->biz_mod_mobile_name)."',
                                biz_mod_mod_pic = '".addslashes($obj->biz_mod_mod_pic)."',
                                biz_mod_templ_id = {$obj->biz_mod_templ_id},
                                biz_mod_active = {$obj->biz_mod_active},
                                biz_mod_stuct = {$obj->biz_mod_stuct},
                                biz_mod_intro = {$obj->biz_mod_intro},
                                biz_mod_index = {$obj->biz_mod_index},
                                biz_mod_external_type = '{$obj->biz_mod_external_type}',
                                biz_mod_has_reviews = {$obj->biz_mod_has_reviews},
                                biz_mod_can_delete = {$obj->biz_mod_can_delete}
                                WHERE biz_mod_id = {$obj->biz_mod_id}
                                         ");
    }

    private function deleteBizModByIdDB($bizModID){

        if (!is_numeric($bizModID) || $bizModID <= 0){
            throw new Exception("Illegal value bizModID");
        }

        $this->db->execute("DELETE FROM tbl_biz_mod WHERE biz_mod_id = $bizModID");
    }

    private function loadMainMenuDataFromDB(){

        $mainMenuSQL = "SELECT * FROM tbl_biz_mod, tbl_mod_struct
                        WHERE biz_mod_biz_id = {$this->bizID}
                        AND biz_mod_mod_id = 0
                        AND biz_mod_stuct = ms_template";

        return $this->db->getRow( $mainMenuSQL );
    }

    /************************************* */
    /*   BASIC BIZNAVIGATION - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizNavigationObject to DB
     * Return Data = new bizNavigationObject ID
     * @param bizNavigationObject $bizNavigationObj
     * @return resultObject
     */
    public function addBizNavigation(bizNavigationObject $bizNavigationObj){
        try{
            $newId = $this->addBizNavigationDB($bizNavigationObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizNavigationObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizNavigation from DB for provided ID
     * * Return Data = bizNavigationObject
     * @param int $bizNavigationId
     * @return resultObject
     */
    public function getBizNavigationByID($bizNavigationId){

        try {
            $bizNavigationData = $this->loadBizNavigationFromDB($bizNavigationId);

            $bizNavigationObj = bizNavigationObject::withData($bizNavigationData);
            $result = resultObject::withData(1,'',$bizNavigationObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizNavigationId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizNavigation in DB
     * @param bizNavigationObject $bizNavigationObj
     * @return resultObject
     */
    public function updateBizNavigation(bizNavigationObject $bizNavigationObj){
        try{
            $this->upateBizNavigationDB($bizNavigationObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizNavigationObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizNavigation from DB
     * @param int $bizNavigationID
     * @return resultObject
     */
    public function deleteBizNavigationById($bizNavigationID){
        try{
            $this->deleteBizNavigationByIdDB($bizNavigationID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizNavigationID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZNAVIGATION - DB METHODS           */
    /************************************* */

    private function addBizNavigationDB(bizNavigationObject $obj){

        if (!isset($obj)){
            throw new Exception("bizNavigationObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_navigation SET
                                        bn_biz_id = {$obj->bn_biz_id},
                                        bn_entity = '{$obj->bn_entity}',
                                        bn_entity_id = {$obj->bn_entity_id},
                                        bn_external_id = {$obj->bn_external_id},
                                        bn_index = {$obj->bn_index},
                                        bn_label = '".addslashes($obj->bn_label)."',
                                        bn_icon = '".addslashes($obj->bn_icon)."',
                                        bn_selector_ios = '".addslashes($obj->bn_selector_ios)."',
                                        bn_selector_and = '".addslashes($obj->bn_selector_and)."'
                                                 ");
        return $newId;
    }

    private function loadBizNavigationFromDB($bizNavigationID){

        if (!is_numeric($bizNavigationID) || $bizNavigationID <= 0){
            throw new Exception("Illegal value bizNavigationID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_navigation WHERE bn_id = $bizNavigationID");
    }

    private function upateBizNavigationDB(bizNavigationObject $obj){

        if (!isset($obj->bn_id) || !is_numeric($obj->bn_id) || $obj->bn_id <= 0){
            throw new Exception("bizNavigationObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_navigation SET
                                bn_biz_id = {$obj->bn_biz_id},
                                bn_entity = '{$obj->bn_entity}',
                                bn_entity_id = {$obj->bn_entity_id},
                                bn_external_id = {$obj->bn_external_id},
                                bn_index = {$obj->bn_index},
                                bn_label = '".addslashes($obj->bn_label)."',
                                bn_icon = '".addslashes($obj->bn_icon)."',
                                bn_selector_ios = '".addslashes($obj->bn_selector_ios)."',
                                bn_selector_and = '".addslashes($obj->bn_selector_and)."'
                                WHERE bn_id = {$obj->bn_id}
                                         ");
    }

    private function deleteBizNavigationByIdDB($bizNavigationID){

        if (!is_numeric($bizNavigationID) || $bizNavigationID <= 0){
            throw new Exception("Illegal value bizNavigationID");
        }

        $this->db->execute("DELETE FROM tbl_biz_navigation WHERE bn_id = $bizNavigationID");
    }

    /************************************* */
    /*   BASIC GROUP - PUBLIC           */
    /************************************* */

    /**
     * Insert new groupObject to DB
     * Return Data = new groupObject ID
     * @param groupObject $groupObj
     * @return resultObject
     */
    public function addGroup(groupObject $groupObj){
        try{
            $newId = $this->addGroupDB($groupObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($groupObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get group from DB for provided ID
     * * Return Data = groupObject
     * @param int $groupId
     * @return resultObject
     */
    public function getGroupByID($groupId){

        try {
            $groupData = $this->loadGroupFromDB($groupId);

            $groupObj = groupObject::withData($groupData);
            $result = resultObject::withData(1,'',$groupObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$groupId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update group in DB
     * @param groupObject $groupObj
     * @return resultObject
     */
    public function updateGroup(groupObject $groupObj){
        try{
            $this->upateGroupDB($groupObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($groupObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete group from DB
     * @param int $groupID
     * @return resultObject
     */
    public function deleteGroupById($groupID){
        try{
            $this->deleteGroupByIdDB($groupID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$groupID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC GROUP - DB METHODS           */
    /************************************* */

    private function addGroupDB(groupObject $obj){

        if (!isset($obj)){
            throw new Exception("groupObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_groups SET
                                        cg_biz_id = {$obj->cg_biz_id},
                                        cg_name = '".addslashes($obj->cg_name)."',
                                        cg_description = '".addslashes($obj->cg_description)."',
                                        cg_type = {$obj->cg_type}
                                                 ");
        return $newId;
    }

    private function loadGroupFromDB($groupID){

        if (!is_numeric($groupID) || $groupID <= 0){
            throw new Exception("Illegal value groupID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_groups WHERE cg_id = $groupID");
    }

    private function upateGroupDB(groupObject $obj){

        if (!isset($obj->cg_id) || !is_numeric($obj->cg_id) || $obj->cg_id <= 0){
            throw new Exception("groupObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_groups SET
                                cg_biz_id = {$obj->cg_biz_id},
                                cg_name = '".addslashes($obj->cg_name)."',
                                cg_description = '".addslashes($obj->cg_description)."',
                                cg_type = {$obj->cg_type}
                                WHERE cg_id = {$obj->cg_id}
                                         ");
    }

    private function deleteGroupByIdDB($groupID){

        if (!is_numeric($groupID) || $groupID <= 0){
            throw new Exception("Illegal value groupID");
        }

        $this->db->execute("DELETE FROM tbl_biz_groups WHERE cg_id = $groupID");
    }

    /************************************* */
    /*   BASIC BIZPAYMENTPROCESSOR - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizPaymentProcessorObject to DB
     * Return Data = new bizPaymentProcessorObject ID
     * @param bizPaymentProcessorObject $bizPaymentProcessorObj
     * @return resultObject
     */
    public function addBizPaymentProcessor(bizPaymentProcessorObject $bizPaymentProcessorObj){
        try{
            $newId = $this->addBizPaymentProcessorDB($bizPaymentProcessorObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizPaymentProcessorObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizPaymentProcessor from DB for provided ID
     * * Return Data = bizPaymentProcessorObject
     * @param int $bizPaymentProcessorId
     * @return resultObject
     */
    public function getBizPaymentProcessorByID($bizPaymentProcessorId){

        try {
            $bizPaymentProcessorData = $this->loadBizPaymentProcessorFromDB($bizPaymentProcessorId);

            if(is_array($bizPaymentProcessorData)){
                $bizPaymentProcessorObj = bizPaymentProcessorObject::withData($bizPaymentProcessorData);
                $result = resultObject::withData(1,'',$bizPaymentProcessorObj);
            }else{
                $result = resultObject::withData(1,'',new bizPaymentProcessorObject());
            }
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizPaymentProcessorId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizPaymentProcessor in DB
     * @param bizPaymentProcessorObject $bizPaymentProcessorObj
     * @return resultObject
     */
    public function updateBizPaymentProcessor(bizPaymentProcessorObject $bizPaymentProcessorObj){
        try{
            $this->upateBizPaymentProcessorDB($bizPaymentProcessorObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizPaymentProcessorObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizPaymentProcessor from DB
     * @param int $bizPaymentProcessorID
     * @return resultObject
     */
    public function deleteBizPaymentProcessorById($bizPaymentProcessorID){
        try{
            $this->deleteBizPaymentProcessorByIdDB($bizPaymentProcessorID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizPaymentProcessorID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZPAYMENTPROCESSOR - DB METHODS           */
    /************************************* */

    private function addBizPaymentProcessorDB(bizPaymentProcessorObject $obj){

        if (!isset($obj)){
            throw new Exception("bizPaymentProcessorObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_processors SET
                                        bp_biz_id = {$obj->bp_biz_id},
                                        bp_proc_id = {$obj->bp_proc_id},
                                        bp_type = '".addslashes($obj->bp_type)."',
                                        bp_wallet_id = '".addslashes($obj->bp_wallet_id)."',
                                        bp_active = {$obj->bp_active},
                                        bp_processor_id = '".addslashes($obj->bp_processor_id)."'
                                                 ");
        return $newId;
    }

    private function loadBizPaymentProcessorFromDB($bizPaymentProcessorID){

        if (!is_numeric($bizPaymentProcessorID) || $bizPaymentProcessorID <= 0){
            throw new Exception("Illegal value bizPaymentProcessorID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_processors WHERE bp_proc_id=$bizPaymentProcessorID AND bp_biz_id = {$this->bizID}");
    }

    private function upateBizPaymentProcessorDB(bizPaymentProcessorObject $obj){

        if (!isset($obj->bp_biz_id) || !is_numeric($obj->bp_biz_id) || $obj->bp_biz_id <= 0){
            throw new Exception("bizPaymentProcessorObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_processors SET
                                bp_type = '".addslashes($obj->bp_type)."',
                                bp_wallet_id = '".addslashes($obj->bp_wallet_id)."',
                                bp_active = {$obj->bp_active},
                                bp_processor_id = '".addslashes($obj->bp_processor_id)."'
                                WHERE bp_biz_id = {$obj->bp_biz_id}  AND bp_proc_id={$obj->bp_proc_id}
                                         ");
    }

    private function deleteBizPaymentProcessorByIdDB($bizPaymentProcessorID){

        if (!is_numeric($bizPaymentProcessorID) || $bizPaymentProcessorID <= 0){
            throw new Exception("Illegal value bizPaymentProcessorID");
        }

        $this->db->execute("DELETE FROM tbl_biz_processors WHERE bp_proc_id = $bizPaymentProcessorID AND bp_biz_id = {$this->bizID}");
    }

    private function getBizConnectedActiveProcessors(){
        $sql = "SELECT * FROM tbl_biz_processors,tbl_processors
                WHERE bp_biz_id = {$this->bizID}
                AND bp_proc_id = pr_id
                AND bp_active = 1";

        return $this->db->getTable($sql);
    }

    private function bizHasNonCashProcessors(){
        $sql = "SELECT COUNT(*) FROM tbl_biz_processors
                WHERE bp_biz_id = {$this->bizID}
                AND bp_proc_id <> 3
                AND bp_active = 1";

        return $this->db->getVal($sql) > 0;
    }

    /************************************* */
    /*   BASIC BIZSTORESETTINGS - PUBLIC           */
    /************************************* */

    /**
    * Insert new bizStoreSettingsObject to DB
    * Return Data = new bizStoreSettingsObject ID
    * @param bizStoreSettingsObject $bizStoreSettingsObj
    * @return resultObject
    */
    public function addBizStoreSettings(bizStoreSettingsObject $bizStoreSettingsObj){
            try{
                $newId = $this->addBizStoreSettingsDB($bizStoreSettingsObj);
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizStoreSettingsObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get bizStoreSettings from DB for provided ID
    * * Return Data = bizStoreSettingsObject
    * @param int $bizStoreSettingsId
    * @return resultObject
    */
    public function getBizStoreSettings(){

            try {
                $bizStoreSettingsData = $this->loadBizStoreSettingsFromDB($this->bizID);

                $bizStoreSettingsObj = bizStoreSettingsObject::withData($bizStoreSettingsData);
                $result = resultObject::withData(1,'',$bizStoreSettingsObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$this->bizID);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update bizStoreSettings in DB
    * @param bizStoreSettingsObject $bizStoreSettingsObj
    * @return resultObject
    */
    public function updateBizStoreSettings(bizStoreSettingsObject $bizStoreSettingsObj){
            try{
                $this->upateBizStoreSettingsDB($bizStoreSettingsObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizStoreSettingsObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete bizStoreSettings from DB
    * @param int $bizStoreSettingsID
    * @return resultObject
    */
    public function deleteBizStoreSettingsById($bizStoreSettingsID){
            try{
                $this->deleteBizStoreSettingsByIdDB($bizStoreSettingsID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizStoreSettingsID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC BIZSTORESETTINGS - DB METHODS           */
    /************************************* */

    private function addBizStoreSettingsDB(bizStoreSettingsObject $obj){

    if (!isset($obj)){
                throw new Exception("bizStoreSettingsObject value must be provided");
            }

    $newId = $this->db->execute("INSERT INTO ecommerce_store_settings SET
                                            ess_biz_id = {$obj->ess_biz_id},
                                            ess_currency = '".addslashes($obj->ess_currency)."',
                                            ess_currency_symbol = '".addslashes($obj->ess_currency_symbol)."',
                                            ess_weight_units = '".addslashes($obj->ess_weight_units)."',
                                            ess_tax_rate = {$obj->ess_tax_rate},
                                            ess_shipping_time = {$obj->ess_shipping_time},
                                            ess_shipping_price = {$obj->ess_shipping_price},
                                            ess_shipping_policy_on = {$obj->ess_shipping_policy_on},
                                            ess_refund_policy_on = {$obj->ess_refund_policy_on},
                                            ess_custom_policy_on = {$obj->ess_custom_policy_on},
                                            ess_shipping_policy = '".addslashes($obj->ess_shipping_policy)."',
                                            ess_refund_policy = '".addslashes($obj->ess_refund_policy)."',
                                            ess_custom_policy = '".addslashes($obj->ess_custom_policy)."',
                                            ess_business_name = '".addslashes($obj->ess_business_name)."',
                                            ess_phone = '".addslashes($obj->ess_phone)."',
                                            ess_email = '".addslashes($obj->ess_email)."',
                                            ess_include_tax = {$obj->ess_include_tax},
                                            ess_require_shipping = {$obj->ess_require_shipping},
                                            ess_checkout_comment_title = '".addslashes($obj->ess_checkout_comment_title)."',
                                            ess_product_order = '{$obj->ess_product_order}',
                                            ess_set_cash_as_paid = {$obj->ess_set_cash_as_paid},
                                            ess_track_stock = {$obj->ess_track_stock},
                                            ess_hide_out_of_stock = {$obj->ess_hide_out_of_stock},
                                            ess_require_cash_drawer = {$obj->ess_require_cash_drawer},
                                            ess_allow_employee_refund = {$obj->ess_allow_employee_refund},
                                            ess_finalize_by_amount = {$obj->ess_finalize_by_amount},
                                            ess_finalize_amount = {$obj->ess_finalize_amount},
                                            ess_finalize_by_time = '{$obj->ess_finalize_by_time}',
                                            ess_finalize_period = '{$obj->ess_finalize_period}',
                                            ess_finalize_period_nmber = {$obj->ess_finalize_period_nmber},
                                            ess_auto_reminder = {$obj->ess_auto_reminder},
                                            ess_reminder_after = '{$obj->ess_reminder_after}',
                                            ess_reminder_before = {$obj->ess_reminder_before},
                                            ess_handle_warn_before_due_date = {$obj->ess_handle_warn_before_due_date},
                                            ess_handle_unpaid_orders = {$obj->ess_handle_unpaid_orders},
                                            ess_points_expire_period = '{$obj->ess_points_expire_period}',
                                            ess_points_expire_number = {$obj->ess_points_expire_number}
                                             ");
             return $newId;
        }

    private function loadBizStoreSettingsFromDB($bizID){

        if (!is_numeric($bizID) || $bizID <= 0){
            throw new Exception("Illegal value bizID");
        }

        return $this->db->getRow("SELECT * FROM ecommerce_store_settings WHERE ess_biz_id = $bizID");
    }

    private function upateBizStoreSettingsDB(bizStoreSettingsObject $obj){

    if (!isset($obj->ess_id) || !is_numeric($obj->ess_id) || $obj->ess_id <= 0){
                throw new Exception("bizStoreSettingsObject value must be provided");
            }

    $this->db->execute("UPDATE ecommerce_store_settings SET
                                            ess_biz_id = {$obj->ess_biz_id},
                                            ess_currency = '".addslashes($obj->ess_currency)."',
                                            ess_currency_symbol = '".addslashes($obj->ess_currency_symbol)."',
                                            ess_weight_units = '".addslashes($obj->ess_weight_units)."',
                                            ess_tax_rate = {$obj->ess_tax_rate},
                                            ess_shipping_time = {$obj->ess_shipping_time},
                                            ess_shipping_price = {$obj->ess_shipping_price},
                                            ess_shipping_policy_on = {$obj->ess_shipping_policy_on},
                                            ess_refund_policy_on = {$obj->ess_refund_policy_on},
                                            ess_custom_policy_on = {$obj->ess_custom_policy_on},
                                            ess_shipping_policy = '".addslashes($obj->ess_shipping_policy)."',
                                            ess_refund_policy = '".addslashes($obj->ess_refund_policy)."',
                                            ess_custom_policy = '".addslashes($obj->ess_custom_policy)."',
                                            ess_business_name = '".addslashes($obj->ess_business_name)."',
                                            ess_phone = '".addslashes($obj->ess_phone)."',
                                            ess_email = '".addslashes($obj->ess_email)."',
                                            ess_include_tax = {$obj->ess_include_tax},
                                            ess_require_shipping = {$obj->ess_require_shipping},
                                            ess_checkout_comment_title = '".addslashes($obj->ess_checkout_comment_title)."',
                                            ess_product_order = '{$obj->ess_product_order}',
                                            ess_set_cash_as_paid = {$obj->ess_set_cash_as_paid},
                                            ess_track_stock = {$obj->ess_track_stock},
                                            ess_hide_out_of_stock = {$obj->ess_hide_out_of_stock},
                                            ess_require_cash_drawer = {$obj->ess_require_cash_drawer},
                                            ess_allow_employee_refund = {$obj->ess_allow_employee_refund},
                                            ess_finalize_by_amount = {$obj->ess_finalize_by_amount},
                                            ess_finalize_amount = {$obj->ess_finalize_amount},
                                            ess_finalize_by_time = '{$obj->ess_finalize_by_time}',
                                            ess_finalize_period = '{$obj->ess_finalize_period}',
                                            ess_finalize_period_nmber = {$obj->ess_finalize_period_nmber},
                                            ess_auto_reminder = {$obj->ess_auto_reminder},
                                            ess_reminder_after = '{$obj->ess_reminder_after}',
                                            ess_reminder_before = {$obj->ess_reminder_before},
                                            ess_handle_warn_before_due_date = {$obj->ess_handle_warn_before_due_date},
                                            ess_handle_unpaid_orders = {$obj->ess_handle_unpaid_orders},
                                            ess_points_expire_period = '{$obj->ess_points_expire_period}',
                                            ess_points_expire_number = {$obj->ess_points_expire_number}
                                            WHERE ess_id = {$obj->ess_id}
                                                     ");
        }

    private function deleteBizStoreSettingsByIdDB($bizStoreSettingsID){

            if (!is_numeric($bizStoreSettingsID) || $bizStoreSettingsID <= 0){
                throw new Exception("Illegal value bizStoreSettingsID");
            }

            $this->db->execute("DELETE FROM ecommerce_store_settings WHERE ess_id = $bizStoreSettingsID");
        }

    /************************************* */
    /*   BASIC BIZMEMBERSCLUBSETTINGS - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizMembersClubSettingsObject to DB
     * Return Data = new bizMembersClubSettingsObject ID
     * @param bizMembersClubSettingsObject $bizMembersClubSettingsObj
     * @return resultObject
     */
    public function addBizMembersClubSettings(bizMembersClubSettingsObject $bizMembersClubSettingsObj){
        try{
            $newId = $this->addBizMembersClubSettingsDB($bizMembersClubSettingsObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){

            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizMembersClubSettings from DB for provided ID
     * * Return Data = bizMembersClubSettingsObject
     * @param int $bizMembersClubSettingsId
     * @return resultObject
     */
    public function getBizMembersClubSettingsByID($bizMembersClubSettingsId){

        try {
            $bizMembersClubSettingsData = $this->loadBizMembersClubSettingsFromDB($bizMembersClubSettingsId);

            $bizMembersClubSettingsObj = bizMembersClubSettingsObject::withData($bizMembersClubSettingsData);
            $result = resultObject::withData(1,'',$bizMembersClubSettingsObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {

            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param bizMembersClubSettingsObject $bizMembersClubSettingsObj
     * @return resultObject
     */
    public function updateBizMembersClubSettings(bizMembersClubSettingsObject $bizMembersClubSettingsObj){
        try{
            $this->upateBizMembersClubSettingsDB($bizMembersClubSettingsObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){

            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizMembersClubSettings from DB
     * @param int $bizMembersClubSettingsID
     * @return resultObject
     */
    public function deleteBizMembersClubSettingsById($bizMembersClubSettingsID){
        try{
            $this->deleteBizMembersClubSettingsByIdDB($bizMembersClubSettingsID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){

            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    
    /************************************* */
    /*   BASIC BIZMEMBERSCLUBSETTINGS - DB METHODS           */
    /************************************* */

    private function addBizMembersClubSettingsDB(bizMembersClubSettingsObject $obj){

        if (!isset($obj)){
            throw new Exception("bizMembersClubSettingsObject value must be provided");             
        }

        $bmcs_welcome_valid_fromDate = isset($obj->bmcs_welcome_valid_from) ? "'".$obj->bmcs_welcome_valid_from."'" : "null";

        $bmcs_welcome_valid_toDate = isset($obj->bmcs_welcome_valid_to) ? "'".$obj->bmcs_welcome_valid_to."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_biz_members_club_settings SET 
                                        bmcs_membership_mode = '{$obj->bmcs_membership_mode}',
                                        bmcs_welcome_entity = '{$obj->bmcs_welcome_entity}',
                                        bmcs_welcome_grant = {$obj->bmcs_welcome_grant},
                                        bmcs_email_on_details_update = {$obj->bmcs_email_on_details_update},
                                        bmcs_welcome_entity_id = {$obj->bmcs_welcome_entity_id},
                                        bmcs_welcome_title = '".addslashes($obj->bmcs_welcome_title)."',
                                        bmcs_welcome_description = '".addslashes($obj->bmcs_welcome_description)."',
                                        bmcs_welcome_valid_from = $bmcs_welcome_valid_fromDate,
                                        bmcs_welcome_valid_to = $bmcs_welcome_valid_toDate
                                                 ");
        return $newId;
    }

    private function loadBizMembersClubSettingsFromDB($bizMembersClubSettingsID){

        if (!is_numeric($bizMembersClubSettingsID) || $bizMembersClubSettingsID <= 0){
            throw new Exception("Illegal value $bizMembersClubSettingsID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_members_club_settings WHERE bmcs_biz_id = $bizMembersClubSettingsID");
    }

    private function upateBizMembersClubSettingsDB(bizMembersClubSettingsObject $obj){

        if (!isset($obj->bmcs_biz_id) || !is_numeric($obj->bmcs_biz_id) || $obj->bmcs_biz_id <= 0){
            throw new Exception("bizMembersClubSettingsObject value must be provided");             
        }

        $bmcs_welcome_valid_fromDate = isset($obj->bmcs_welcome_valid_from) ? "'".$obj->bmcs_welcome_valid_from."'" : "null";

        $bmcs_welcome_valid_toDate = isset($obj->bmcs_welcome_valid_to) ? "'".$obj->bmcs_welcome_valid_to."'" : "null";

        $this->db->execute("UPDATE tbl_biz_members_club_settings SET 
                                bmcs_membership_mode = '{$obj->bmcs_membership_mode}',
                                bmcs_welcome_entity = '{$obj->bmcs_welcome_entity}',
                                bmcs_welcome_grant = {$obj->bmcs_welcome_grant},
                                bmcs_email_on_details_update = {$obj->bmcs_email_on_details_update},
                                bmcs_welcome_entity_id = {$obj->bmcs_welcome_entity_id},
                                bmcs_welcome_title = '".addslashes($obj->bmcs_welcome_title)."',
                                bmcs_welcome_description = '".addslashes($obj->bmcs_welcome_description)."',
                                bmcs_welcome_valid_from = $bmcs_welcome_valid_fromDate,
                                bmcs_welcome_valid_to = $bmcs_welcome_valid_toDate
                                WHERE bmcs_biz_id = {$obj->bmcs_biz_id} 
                                         ");
    }

    private function deleteBizMembersClubSettingsByIdDB($bizMembersClubSettingsID){

        if (!is_numeric($bizMembersClubSettingsID) || $bizMembersClubSettingsID <= 0){
            throw new Exception("Illegal value $bizMembersClubSettingsID");             
        }

        $this->db->execute("DELETE FROM tbl_biz_members_club_settings WHERE bmcs_biz_id = $bizMembersClubSettingsID");
    }

    /************************************* */
    /*   BASIC EMPLOYEEWEEKDAY - PUBLIC           */
    /************************************* */

    /**
    * Insert new employeeWeekdayObject to DB
    * Return Data = new employeeWeekdayObject ID
    * @param employeeWeekdayObject $employeeWeekdayObj
    * @return resultObject
    */
    public function addEmployeeWeekday(employeeWeekdayObject $employeeWeekdayObj){
            try{
                $newId = $this->addEmployeeWeekdayDB($employeeWeekdayObj);
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeWeekdayObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get employeeWeekday from DB for provided ID
    * * Return Data = employeeWeekdayObject
    * @param int $employeeWeekdayId
    * @return resultObject
    */
    public function getEmployeeWeekdayByID($employeeWeekdayId){

        try {
            $employeeWeekdayData = $this->loadEmployeeWeekdayFromDB($employeeWeekdayId);

            $employeeWeekdayObj = employeeWeekdayObject::withData($employeeWeekdayData);
            $result = resultObject::withData(1,'',$employeeWeekdayObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeWeekdayId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Update employeeWeekdayObject in DB
    * @param employeeWeekdayObject $employeeWeekdayObj
    * @return resultObject
    */
    public function updateEmployeeWeekday(employeeWeekdayObject $employeeWeekdayObj){
        try{
            $this->upateEmployeeWeekdayDB($employeeWeekdayObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeWeekdayObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete employeeWeekday from DB
    * @param int $employeeWeekdayID
    * @return resultObject
    */
    public function deleteEmployeeWeekdayById($employeeWeekdayID){
        try{
            $this->deleteEmployeeWeekdayByIdDB($employeeWeekdayID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeWeekdayID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC EMPLOYEEWEEKDAY - DB METHODS           */
    /************************************* */

    private function addEmployeeWeekdayDB(employeeWeekdayObject $obj){

        if (!isset($obj)){
            throw new Exception("employeeWeekdayObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_employee_hours SET
                            bch_emp_id = {$obj->bch_emp_id},
                            bch_day_no = {$obj->bch_day_no},
                            bch_day_name = '".addslashes($obj->bch_day_name)."',
                            bch_begin_hour = {$obj->bch_begin_hour},
                            bch_finish_hour = {$obj->bch_finish_hour},
                            bch_working_day = {$obj->bch_working_day}
                                        ");
        return $newId;
    }

    private function loadEmployeeWeekdayFromDB($employeeWeekdayID){

        if (!is_numeric($employeeWeekdayID) || $employeeWeekdayID <= 0){
            throw new Exception("Illegal value employeeWeekdayID");
        }

        return $this->db->getRow("SELECT * FROM tbl_employee_hours WHERE bch_id = $employeeWeekdayID");
    }

    private function upateEmployeeWeekdayDB(employeeWeekdayObject $obj){

        if (!isset($obj->bch_id) || !is_numeric($obj->bch_id) || $obj->bch_id <= 0){
                    throw new Exception("employeeWeekdayObject value must be provided");
                }

        $this->db->execute("UPDATE tbl_employee_hours SET
                                bch_emp_id = {$obj->bch_emp_id},
                                bch_day_no = {$obj->bch_day_no},
                                bch_day_name = '".addslashes($obj->bch_day_name)."',
                                bch_begin_hour = {$obj->bch_begin_hour},
                                bch_finish_hour = {$obj->bch_finish_hour},
                                bch_working_day = {$obj->bch_working_day}
                                WHERE bch_id = {$obj->bch_id}
                                         ");
    }

    private function deleteEmployeeWeekdayByIdDB($employeeWeekdayID){

        if (!is_numeric($employeeWeekdayID) || $employeeWeekdayID <= 0){
            throw new Exception("Illegal value employeeWeekdayID");
        }

        $this->db->execute("DELETE FROM tbl_employee_hours WHERE bch_id = $employeeWeekdayID");
    }

    /************************************* */
    /*   BASIC POINTSGRANT - PUBLIC           */
    /************************************* */

    /**
    * Insert new pointsGrantObject to DB
    * Return Data = new pointsGrantObject ID
    * @param pointsGrantObject $pointsGrantObj
    * @return resultObject
    */
    public function addPointsGrant(pointsGrantObject $pointsGrantObj){
            try{
                $newId = $this->addPointsGrantDB($pointsGrantObj);
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($pointsGrantObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get pointsGrant from DB for provided ID
    * * Return Data = pointsGrantObject
    * @param int $pointsGrantId
    * @return resultObject
    */
    public function getPointsGrantByID($pointsGrantId){

            try {
                $pointsGrantData = $this->loadPointsGrantFromDB($pointsGrantId);

                $pointsGrantObj = pointsGrantObject::withData($pointsGrantData);
                $result = resultObject::withData(1,'',$pointsGrantObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$pointsGrantId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update a1History in DB
    * @param pointsGrantObject $pointsGrantObj
    * @return resultObject
    */
    public function updatePointsGrant(pointsGrantObject $pointsGrantObj){
            try{
                $this->upatePointsGrantDB($pointsGrantObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($pointsGrantObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete pointsGrant from DB
    * @param int $pointsGrantID
    * @return resultObject
    */
    public function deletePointsGrantById($pointsGrantID){
            try{
                $this->deletePointsGrantByIdDB($pointsGrantID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$pointsGrantID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    public function getPointsGrantForBiz($skip = 0,$take = 0){
        try{
            $pointsShopRows = $this->getPointsGrantByBizIDB($skip,$take);

            $pointsShopList = array();
            foreach ($pointsShopRows as $pointItemRow)
            {
            	$pointsShopList[] = pointsGrantObject::withData($pointItemRow);
            }

            return resultObject::withData(1,'',$pointsShopList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$this->bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

    }



    public function getBizPopularPointShopItem(){
        try{

            $itemRow = $this->getBizPopularPointShopItemDB($this->bizID);

            $pointsItem = pointsGrantObject::withData($itemRow);

            return resultObject::withData(1,'',$pointsItem);
        }
        catch(Exception $e){

            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BASIC POINTSGRANT - DB METHODS           */
    /************************************* */

    private function addPointsGrantDB(pointsGrantObject $obj){

    if (!isset($obj)){
                throw new Exception("pointsGrantObject value must be provided");
            }

    $newId = $this->db->execute("INSERT INTO tbl_points_grant SET
                                    pg_biz_id = {$obj->pg_biz_id},
                                    pg_entity_type = '{$obj->pg_entity_type}',
                                    pg_entity_id = {$obj->pg_entity_id},
                                    pg_text = '".addslashes($obj->pg_text)."',
                                    pg_cost = {$obj->pg_cost},
                                    pg_img = '".addslashes($obj->pg_img)."',
                                    pg_title = '".addslashes($obj->pg_title)."'
                                             ");
             return $newId;
        }

    private function loadPointsGrantFromDB($pointsGrantID){

    if (!is_numeric($pointsGrantID) || $pointsGrantID <= 0){
                throw new Exception("Illegal value pointsGrantID");
            }

            return $this->db->getRow("SELECT * FROM tbl_points_grant WHERE pg_id = $pointsGrantID");
        }

    private function upatePointsGrantDB(pointsGrantObject $obj){

    if (!isset($obj->pg_id) || !is_numeric($obj->pg_id) || $obj->pg_id <= 0){
                throw new Exception("pointsGrantObject value must be provided");
            }

    $this->db->execute("UPDATE tbl_points_grant SET
                            pg_biz_id = {$obj->pg_biz_id},
                            pg_entity_type = '{$obj->pg_entity_type}',
                            pg_entity_id = {$obj->pg_entity_id},
                            pg_text = '".addslashes($obj->pg_text)."',
                            pg_cost = {$obj->pg_cost},
                            pg_img = '".addslashes($obj->pg_img)."',
                            pg_title = '".addslashes($obj->pg_title)."'
                            WHERE pg_id = {$obj->pg_id}
                                     ");
        }

    private function deletePointsGrantByIdDB($pointsGrantID){

        if (!is_numeric($pointsGrantID) || $pointsGrantID <= 0){
            throw new Exception("Illegal value pointsGrantID");
        }

        $this->db->execute("DELETE FROM tbl_points_grant WHERE pg_id = $pointsGrantID");
    }

    private function getPointsGrantByBizIDB($skip = 0,$take = 0){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_points_grant
                WHERE pg_biz_id = {$this->bizID}
                $limit";

        return $this->db->getTable($sql);
    }

    private function getBizPopularPointShopItemDB($bizID){
        $sql = "SELECT * FROM tbl_points_grant
                WHERE pg_biz_id = $bizID
                ORDER BY pg_cost DESC
                LIMIT 1";

        return $this->db->getRow($sql);
    }

    /************************************* */
    /*   BASIC BIZTHEME - PUBLIC           */
    /************************************* */

    /**
    * Insert new bizThemeObject to DB
    * Return Data = new bizThemeObject ID
    * @param bizThemeObject $bizThemeObj
    * @return resultObject
    */
    public function addBizTheme(bizThemeObject $bizThemeObj){
        try{
            $newId = $this->addBizThemeDB($bizThemeObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizThemeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get bizTheme from DB for provided ID
    * * Return Data = bizThemeObject
    * @param int $bizThemeId
    * @return resultObject
    */
    public function getBizThemeByID($bizThemeId){

        try {
            $bizThemeData = $this->loadBizThemeFromDB($bizThemeId);

            $bizThemeObj = bizThemeObject::withData($bizThemeData);
            $result = resultObject::withData(1,'',$bizThemeObj);
            return $result;
          }
          //catch exception
          catch(Exception $e) {
              errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizThemeId);
              $result = resultObject::withData(0,$e->getMessage());
              return $result;
          }
    }

    /**
    * Update bizTheme in DB
    * @param bizThemeObject $bizThemeObj
    * @return resultObject
    */
    public function updateBizTheme(bizThemeObject $bizThemeObj){
        try{
            $this->upateBizThemeDB($bizThemeObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizThemeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete bizTheme from DB
    * @param int $bizThemeID
    * @return resultObject
    */
    public function deleteBizThemeById($bizThemeID){
        try{
            $this->deleteBizThemeByIdDB($bizThemeID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizThemeID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZTHEME - DB METHODS           */
    /************************************* */

    private function addBizThemeDB(bizThemeObject $obj){

        if (!isset($obj)){
            throw new Exception("bizThemeObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_template SET
        tmpl_name = '".addslashes($obj->tmpl_name)."',
        tmpl_html = '".addslashes($obj->tmpl_html)."',
        tmpl_html_out = '".addslashes($obj->tmpl_html_out)."',
        tmpl_img1_default = '".addslashes($obj->tmpl_img1_default)."',
        tmpl_background = '".addslashes($obj->tmpl_background)."',
        tmpl_module = {$obj->tmpl_module},
        tmpl_category = {$obj->tmpl_category},
        tmpl_unique = {$obj->tmpl_unique},
        tmpl_font = '".addslashes($obj->tmpl_font)."',
        tmpl_font_bold = '".addslashes($obj->tmpl_font_bold)."',
        tmpl_font_color = '".addslashes($obj->tmpl_font_color)."',
        tmpl_transparent = {$obj->tmpl_transparent},
        tmpl_head_color = '".addslashes($obj->tmpl_head_color)."',
        tmpl_img_feel = '".addslashes($obj->tmpl_img_feel)."',
        tmpl_active = {$obj->tmpl_active},
        tmpl_have_phone = {$obj->tmpl_have_phone},
        tmpl_gen_bg_type = {$obj->tmpl_gen_bg_type},
        tmpl_tabbar_bg_type = {$obj->tmpl_tabbar_bg_type},
        tmpl_tabbar_shade_type = {$obj->tmpl_tabbar_shade_type},
        tmpl_navbar_bg_type = {$obj->tmpl_navbar_bg_type},
        tmpl_sidebar_bg_type = {$obj->tmpl_sidebar_bg_type},
        tmpl_sidemagic_bg_type = {$obj->tmpl_sidemagic_bg_type},
        tmpl_shadow_alpha = '".addslashes($obj->tmpl_shadow_alpha)."',
        theme_tab_icon_type = {$obj->theme_tab_icon_type},
        tmpl_color_1 = '".addslashes($obj->tmpl_color_1)."',
        tmpl_color_2 = '".addslashes($obj->tmpl_color_2)."',
        tmpl_color_3 = '".addslashes($obj->tmpl_color_3)."',
        tmpl_color_4 = '".addslashes($obj->tmpl_color_4)."',
        tmpl_color_5 = '".addslashes($obj->tmpl_color_5)."',
        tmpl_color_6 = '".addslashes($obj->tmpl_color_6)."',
        tmpl_color_7 = '".addslashes($obj->tmpl_color_7)."',
        tmpl_family = {$obj->tmpl_family},
        tmpl_color1_freestyle = '".addslashes($obj->tmpl_color1_freestyle)."',
        tmpl_color2_freestyle = '".addslashes($obj->tmpl_color2_freestyle)."',
        tmpl_color3_freestyle = '".addslashes($obj->tmpl_color3_freestyle)."',
        tmpl_color4_freestyle = '".addslashes($obj->tmpl_color4_freestyle)."',
        tmpl_color5_freestyle = '".addslashes($obj->tmpl_color5_freestyle)."',
        tmpl_live_background = {$obj->tmpl_live_background},
        tmpl_live_magic = {$obj->tmpl_live_magic}
                 ");
         return $newId;
    }

    private function loadBizThemeFromDB($bizThemeID){

        if (!is_numeric($bizThemeID) || $bizThemeID <= 0){
            throw new Exception("Illegal value bizThemeID");
        }

        return $this->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $bizThemeID");
    }

    private function upateBizThemeDB(bizThemeObject $obj){

        if (!isset($obj->tmpl_id) || !is_numeric($obj->tmpl_id) || $obj->tmpl_id <= 0){
            throw new Exception("bizThemeObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_template SET
        tmpl_name = '".addslashes($obj->tmpl_name)."',
        tmpl_html = '".addslashes($obj->tmpl_html)."',
        tmpl_html_out = '".addslashes($obj->tmpl_html_out)."',
        tmpl_img1_default = '".addslashes($obj->tmpl_img1_default)."',
        tmpl_background = '".addslashes($obj->tmpl_background)."',
        tmpl_module = {$obj->tmpl_module},
        tmpl_category = {$obj->tmpl_category},
        tmpl_unique = {$obj->tmpl_unique},
        tmpl_font = '".addslashes($obj->tmpl_font)."',
        tmpl_font_bold = '".addslashes($obj->tmpl_font_bold)."',
        tmpl_font_color = '".addslashes($obj->tmpl_font_color)."',
        tmpl_transparent = {$obj->tmpl_transparent},
        tmpl_head_color = '".addslashes($obj->tmpl_head_color)."',
        tmpl_img_feel = '".addslashes($obj->tmpl_img_feel)."',
        tmpl_active = {$obj->tmpl_active},
        tmpl_have_phone = {$obj->tmpl_have_phone},
        tmpl_gen_bg_type = {$obj->tmpl_gen_bg_type},
        tmpl_tabbar_bg_type = {$obj->tmpl_tabbar_bg_type},
        tmpl_tabbar_shade_type = {$obj->tmpl_tabbar_shade_type},
        tmpl_navbar_bg_type = {$obj->tmpl_navbar_bg_type},
        tmpl_sidebar_bg_type = {$obj->tmpl_sidebar_bg_type},
        tmpl_sidemagic_bg_type = {$obj->tmpl_sidemagic_bg_type},
        tmpl_shadow_alpha = '".addslashes($obj->tmpl_shadow_alpha)."',
        theme_tab_icon_type = {$obj->theme_tab_icon_type},
        tmpl_color_1 = '".addslashes($obj->tmpl_color_1)."',
        tmpl_color_2 = '".addslashes($obj->tmpl_color_2)."',
        tmpl_color_3 = '".addslashes($obj->tmpl_color_3)."',
        tmpl_color_4 = '".addslashes($obj->tmpl_color_4)."',
        tmpl_color_5 = '".addslashes($obj->tmpl_color_5)."',
        tmpl_color_6 = '".addslashes($obj->tmpl_color_6)."',
        tmpl_color_7 = '".addslashes($obj->tmpl_color_7)."',
        tmpl_family = {$obj->tmpl_family},
        tmpl_color1_freestyle = '".addslashes($obj->tmpl_color1_freestyle)."',
        tmpl_color2_freestyle = '".addslashes($obj->tmpl_color2_freestyle)."',
        tmpl_color3_freestyle = '".addslashes($obj->tmpl_color3_freestyle)."',
        tmpl_color4_freestyle = '".addslashes($obj->tmpl_color4_freestyle)."',
        tmpl_color5_freestyle = '".addslashes($obj->tmpl_color5_freestyle)."',
        tmpl_live_background = {$obj->tmpl_live_background},
        tmpl_live_magic = {$obj->tmpl_live_magic}
        WHERE tmpl_id = {$obj->tmpl_id}
                 ");
    }

    private function deleteBizThemeByIdDB($bizThemeID){

        if (!is_numeric($bizThemeID) || $bizThemeID <= 0){
            throw new Exception("Illegal value bizThemeID");
        }

        $this->db->execute("DELETE FROM tbl_template WHERE tmpl_id = $bizThemeID");
    }

    /************************************* */
    /*   BASIC SUBSCRIPTION - PUBLIC           */
    /************************************* */

    public function addSubscription(levelData10Object $subscription){
        try{
             $levelManager = new levelDataManager(10);
             $result = $levelManager->addLevelData($subscription);
             //To do - add items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($subscription));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getSubscriptionBySubscriptionID($subscriptionID){
        try{
            $levelManager = new levelDataManager(10);
            $subscriptionData = $levelManager->getLevelDataDirectByID($subscriptionID);

            $subscriptionObj = levelData10Object::withData($subscriptionData->data);

            //To do - get items


            return resultObject::withData(1,'',$subscriptionObj);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$subscriptionID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updateSubscription(levelData10Object $subscription){
        try{
             $levelManager = new levelDataManager(10);
             $result = $levelManager->updateLevelData($subscription);
             //To do - update items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($subscription));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteSubscription($subscriptionID){
        try{
             $levelManager = new levelDataManager(10);

             //To do - delete items


             $result = $levelManager->deleteLevelDataById($subscriptionID);

             return resultObject::withData(1);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$subscriptionID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BASIC PUNCHPASS - PUBLIC           */
    /************************************* */

    public function addPunchpass(levelData11Object $punchpass){
        try{
             $levelManager = new levelDataManager(11);
             $result = $levelManager->addLevelData($punchpass);
             //To do - add items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($punchpass));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getPunchpassByPunchpassID($punchpassID){
        try{
            $levelManager = new levelDataManager(11);
            $punchpassData = $levelManager->getLevelDataDirectByID($punchpassID);

            $punchpassObj = levelData11Object::withData($punchpassData->data);

            //To do - get items


            return resultObject::withData(1,'',$punchpassObj);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$punchpassID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updatePunchpassObject(levelData11Object $punchpass){
        try{
             $levelManager = new levelDataManager(11);
             $result = $levelManager->updateLevelData($punchpass);
             //To do - update items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($punchpass));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deletePunchpass($punchpassID){
        try{
             $levelManager = new levelDataManager(11);

             //To do - delete items


             $result = $levelManager->deleteLevelDataById($punchpassID);

             return resultObject::withData(1);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$punchpassID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BASIC LOYALTY CARD - PUBLIC           */
    /************************************* */

    public function addLoyaltyCard(levelData6Object $loyaltyCard){
        try{
             $levelManager = new levelDataManager(6);
             $result = $levelManager->addLevelData($loyaltyCard);
             //To do - add items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyCard));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getLoyaltyCardByLoyaltyCardID($loyaltyCardID){
        try{
            $levelManager = new levelDataManager(11);
            $loyaltyData = $levelManager->getLevelDataDirectByID($loyaltyCardID);

            $loyaltyObj = levelData6Object::withData($loyaltyData->data);

            //To do - get items


            return resultObject::withData(1,'',$loyaltyObj);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyCardID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updateLoyaltyCard(levelData6Object $loyaltyCard){
        try{
             $levelManager = new levelDataManager(6);
             $result = $levelManager->updateLevelData($loyaltyCard);
             //To do - update items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyCard));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteLoyaltyCard($loyaltyCardID){
        try{
             $levelManager = new levelDataManager(6);

             //To do - delete items


             $result = $levelManager->deleteLevelDataById($loyaltyCardID);

             return resultObject::withData(1);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyCardID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

     /************************************* */
    /*   BASIC SCRATCH CARD - PUBLIC           */
    /************************************* */

    public function addScratchCard(scratchCardObject $scratchCard){
        try{
             $levelManager = new levelDataManager(13);
             $result = $levelManager->addLevelData($scratchCard);
             //To do - add items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($scratchCard));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getScratchCardByScratchCardID($scratchCardID){
        try{
            $levelManager = new levelDataManager(13);
            $scratchData = $levelManager->getLevelDataDirectByID($scratchCardID);

            $scratchObj = scratchCardObject::withData($scratchData->data);

            //To do - get items


            return resultObject::withData(1,'',$scratchObj);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$scratchCardID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updateScratchCard(scratchCardObject $scratchCard){
        try{
             $levelManager = new levelDataManager(13);
             $result = $levelManager->updateLevelData($scratchCard);
             //To do - update items

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($scratchCard));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteScratchCard($scratchCardID){
        try{
             $levelManager = new levelDataManager(13);

             //To do - delete items


             $result = $levelManager->deleteLevelDataById($scratchCardID);

             return resultObject::withData(1);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$scratchCardID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   GENERAL - PUBLIC           */
    /************************************* */

    /**
     * Different periods for different biz types
     * @return resultObject
     */
    public function getStagePeriodForBiz(){

        $result = $this->getBiz();
        if($result->code == 1 && $result->data->biz_id != 0){
            $bizObject = $this->data;

            switch($bizObject->biz_smb){
                case 'smb_services':
                    $periodDays = 45;
                    break;
                case 'smb_products':
                    $periodDays = 90;
                    break;
                case 'smb_content':
                    $periodDays = 90;
                    break;
                case 'smb_food':
                    $periodDays = 90;
                    break;
                default:
                    $periodDays = 30;
                    break;
            }
            return resultObject::withData(1,"",$periodDays);

        }else{
            return resultObject::withData(0,"no_biz");
        }
    }

    public function mainMenuHasElementsByStructure($structId){

        return $this->db->getVal("select count(md_row_id) from tbl_mod_data0 where md_biz_id={$this->bizID} and md_belong_to_structure=$structId") > 0;
    }

    public function getCustomerReviewsForModuleRow($modID,$rowID){
        try{
            $reviewRows = $this->getCustomerReviewsForModuleRowDB($modID,$rowID);

            $reviews = array();

            foreach ($reviewRows as $reviewRow)
            {
            	$reviews[] = customerReviewObject::withData($reviewRow);
            }

            return resultObject::withData(1,'',$reviews);
        }
        catch(Exception $e){
            $data= array();
            $data['mod'] = $modID;
            $data['row'] = $rowID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerReviewsForItem($itemType,$itemID){
        try{
            $reviewRows = $this->getCustomerReviewsForItemDB($itemType,$itemID);

            $reviews = array();

            foreach ($reviewRows as $reviewRow)
            {
            	$reviews[] = customerReviewObject::withData($reviewRow);
            }

            return resultObject::withData(1,'',$reviews);
        }
        catch(Exception $e){
            $data= array();
            $data['type'] = $itemType;
            $data['item'] = $itemID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   GENERAL - PRIVATE                 */
    /************************************* */

    private function getCustomerReviewsForModuleRowDB($modID,$rowID){
        $sql = "select * FROM tbl_customer_reviews,tbl_customers
                                            WHERE cr_cust_id = cust_id
                                            AND cr_biz_id = {$this->bizID}
                                            AND cr_mod_id = $modID
                                            AND cr_item_type = ''
                                            AND cr_row_id = $rowID
                                            ORDER BY cr_id DESC";

        return $this->db->getTable($sql);
    }

    private function getCustomerReviewsForItemDB($itemType,$itemID){
        $sql = "select * FROM tbl_customer_reviews,tbl_customers
                                            WHERE cr_cust_id = cust_id
                                            AND cr_item_id = $itemID
                                            AND cr_biz_id = {$this->bizID}
                                            AND cr_item_type = '$itemType'
                                            ORDER BY cr_id DESC";

        return $this->db->getTable($sql);
    }
}
