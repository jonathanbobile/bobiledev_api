<?php

class marketModel extends Model
{

    
    function __construct()
    {
        parent::__construct();        
    }

    /************************************* */
    /*   BASIC BIZMARKET - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizMarketObject to DB
     * Return Data = new bizMarketObject ID
     * @param bizMarketObject $bizMarketObj 
     * @return resultObject
     */
    public function addBizMarket(bizMarketObject $bizMarketObj){       
        try{
            $newId = $this->addBizMarketDB($bizMarketObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizMarketObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizMarket from DB for provided ID
     * * Return Data = bizMarketObject
     * @param int $bizMarketId 
     * @return resultObject
     */
    public function getBizMarketByID($bizMarketId){
        
        try {
            $bizMarketData = $this->loadBizMarketFromDB($bizMarketId);
            
            $bizMarketObj = bizMarketObject::withData($bizMarketData);
            $result = resultObject::withData(1,'',$bizMarketObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizMarketId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param bizMarketObject $bizMarketObj 
     * @return resultObject
     */
    public function updateBizMarket(bizMarketObject $bizMarketObj){        
        try{
            $this->upateBizMarketDB($bizMarketObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizMarketObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizMarket from DB
     * @param int $bizMarketID 
     * @return resultObject
     */
    public function deleteBizMarketById($bizMarketID){
        try{
            $this->deleteBizMarketByIdDB($bizMarketID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizMarketID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZMARKET - DB METHODS           */
    /************************************* */

    private function addBizMarketDB(bizMarketObject $obj){

        if (!isset($obj)){
            throw new Exception("bizMarketObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_market SET 
                                        bm_rating = {$obj->bm_rating},
                                        bm_user_rating = {$obj->bm_user_rating},
                                        bm_great_app = {$obj->bm_great_app},
                                        bm_editor_choice = {$obj->bm_editor_choice},
                                        bm_biz_ip = '".addslashes($obj->bm_biz_ip)."',
                                        bm_biz_long = {$obj->bm_biz_long},
                                        bm_biz_lati = {$obj->bm_biz_lati}
                                                 ");
        return $newId;
    }

    private function loadBizMarketFromDB($bizMarketID){

        if (!is_numeric($bizMarketID) || $bizMarketID <= 0){
            throw new Exception("Illegal value bizMarketID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_market WHERE bm_biz_id = $bizMarketID");
    }

    private function upateBizMarketDB(bizMarketObject $obj){

        if (!isset($obj->bm_biz_id) || !is_numeric($obj->bm_biz_id) || $obj->bm_biz_id <= 0){
            throw new Exception("bizMarketObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_biz_market SET 
                                bm_rating = {$obj->bm_rating},
                                bm_user_rating = {$obj->bm_user_rating},
                                bm_great_app = {$obj->bm_great_app},
                                bm_editor_choice = {$obj->bm_editor_choice},
                                bm_biz_ip = '".addslashes($obj->bm_biz_ip)."',
                                bm_biz_long = {$obj->bm_biz_long},
                                bm_biz_lati = {$obj->bm_biz_lati}
                                WHERE bm_biz_id = {$obj->bm_biz_id} 
                                         ");
    }

    private function deleteBizMarketByIdDB($bizMarketID){

        if (!is_numeric($bizMarketID) || $bizMarketID <= 0){
            throw new Exception("Illegal value bizMarketID");             
        }

        $this->db->execute("DELETE FROM tbl_biz_market WHERE bm_biz_id = $bizMarketID");
    }


}
