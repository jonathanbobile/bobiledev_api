<?php

/**
 * bigMarketStages short summary.
 *
 * bigMarketStages description.
 *
 * @version 1.0
 * @author Dany
 */
class bigMarketStages extends bobileObject
{
    public $bms_stage_id = "0";
    public $bms_stage_name;
    public $bms_stage_text;
    public $bms_action_type;//enum('payment','form','none','storepage','')
    public $bms_home_title;
    public $bms_home_btn;

    function __construct(){
       
    }

    public static function withData($data){
        if(!isset($data["bms_stage_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        $instance->bms_stage_id = $data["bms_stage_id"];
        $instance->bms_stage_name = $data["bms_stage_name"];
        $instance->bms_stage_text = $data["bms_stage_text"];
        $instance->bms_action_type = $data["bms_action_type"];
        $instance->bms_home_title = $data["bms_home_title"];
        $instance->bms_home_btn = $data["bms_home_btn"];

        return $instance;
    }
}
