<?php 

class bizEngagementObject extends bobileObject { 

    public $eng_id = "0";
    public $eng_name;
    public $eng_entity;
    public $eng_entity_id = "0";
    public $eng_type;
    public $eng_default_symb;
    public $eng_default_top_title;
    public $eng_default_text;
    public $eng_action_selector;
    public $eng_default_regular_text;
    public $eng_regular_selector;
    public $eng_regular_selector_freestyle;
    public $eng_regular_selector_android;
    public $eng_param_recrod_id;
    public $eng_default_succ_title = "Thank you!";
    public $eng_default_succ_desc = "You will love it.";
    public $eng_select_url;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["eng_id"])){
              throw new Exception("bizEngagementObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->eng_id = $data["eng_id"];
        $instance->eng_name = $data["eng_name"];
        $instance->eng_entity = $data["eng_entity"];
        $instance->eng_entity_id = $data["eng_entity_id"];
        $instance->eng_type = $data["eng_type"];
        $instance->eng_default_symb = $data["eng_default_symb"];
        $instance->eng_default_top_title = $data["eng_default_top_title"];
        $instance->eng_default_text = $data["eng_default_text"];
        $instance->eng_action_selector = $data["eng_action_selector"];
        $instance->eng_default_regular_text = $data["eng_default_regular_text"];
        $instance->eng_regular_selector = $data["eng_regular_selector"];
        $instance->eng_regular_selector_freestyle = $data["eng_regular_selector_freestyle"];
        $instance->eng_regular_selector_android = $data["eng_regular_selector_android"];
        $instance->eng_param_recrod_id = $data["eng_param_recrod_id"];
        $instance->eng_default_succ_title = $data["eng_default_succ_title"];
        $instance->eng_default_succ_desc = $data["eng_default_succ_desc"];
        $instance->eng_select_url = $data["eng_select_url"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


