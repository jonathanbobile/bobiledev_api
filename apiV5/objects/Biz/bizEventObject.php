<?php 

class bizEventObject extends bobileObject { 

    public $event_id = "0";
    public $event_action_id = "0";
    public $event_type = "direct";//enum('every_x','first_time','direct','x_times','period')
    public $event_period = "general";//enum('general','day','week','month','hour')
    public $event_data;
    public $event_label;
    public $event_usage = "biz";//enum('biz','scratch','','')
    public $event_active = "1";
    public $event_req_mod = "0";
    public $event_def_points = "0";
    public $event_def_message;
    public $event_is_default = "1";
    public $event_tip_id = "0";
    public $event_index = "0";
    public $bevent_id = "0";
    public $bevent_biz_id = "0";
    public $bevent_event_id = "0";
    public $bevent_grant_entity = "points";//enum('points','coupon','custom')
    public $bevent_points = "0";
    public $bevent_points_calculation = "direct";//enum('direct','relative')
    public $bevent_points_rate = "0";
    public $bevent_max_points = "0";
    public $bevent_grant_entity_id = "0";
    public $bevent_grant_title;
    public $bevent_grant_description;
    public $bevent_grant_valid_from;
    public $bevent_grant_valid_to;
    public $bevent_message;
    public $bevent_limit_period = "unlimited";//enum('unlimited','week','month','year','lifetime')
    public $bevent_limit_number = "0";
    public $bevent_is_active = "1";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bevent_id"])){
              throw new Exception("bizEventObject constructor requies data array provided!");
        }

        $instance = new self();


        $instance->event_id = $data["event_id"];
        $instance->event_action_id = $data["event_action_id"];
        $instance->event_type = $data["event_type"];
        $instance->event_period = $data["event_period"];
        $instance->event_data = $data["event_data"];
        $instance->event_label = $data["event_label"];
        $instance->event_usage = $data["event_usage"];
        $instance->event_active = $data["event_active"];
        $instance->event_req_mod = $data["event_req_mod"];
        $instance->event_def_points = $data["event_def_points"];
        $instance->event_def_message = $data["event_def_message"];
        $instance->event_is_default = $data["event_is_default"];
        $instance->event_tip_id = $data["event_tip_id"];
        $instance->event_index = $data["event_index"];
        $instance->bevent_id = $data["bevent_id"];
        $instance->bevent_biz_id = $data["bevent_biz_id"];
        $instance->bevent_event_id = $data["bevent_event_id"];
        $instance->bevent_grant_entity = $data["bevent_grant_entity"];
        $instance->bevent_points = $data["bevent_points"];
        $instance->bevent_points_calculation = $data["bevent_points_calculation"];
        $instance->bevent_points_rate = $data["bevent_points_rate"];
        $instance->bevent_grant_entity_id = $data["bevent_grant_entity_id"];
        $instance->bevent_grant_title = $data["bevent_grant_title"];
        $instance->bevent_grant_description = $data["bevent_grant_description"];
        $instance->bevent_grant_valid_from = $data["bevent_grant_valid_from"];
        $instance->bevent_grant_valid_to = $data["bevent_grant_valid_to"];
        $instance->bevent_message = $data["bevent_message"];
        $instance->bevent_limit_period = $data["bevent_limit_period"];
        $instance->bevent_limit_number = $data["bevent_limit_number"];
        $instance->bevent_is_active = $data["bevent_is_active"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


