<?php 

class bizMarketObject extends bobileObject { 

public $bm_biz_id = "0";
public $bm_rating = "0";
public $bm_user_rating = "0";
public $bm_great_app = "0";
public $bm_editor_choice = "0";
public $bm_biz_ip;
public $bm_biz_long = "0";
public $bm_biz_lati = "0";

 function __construct(){} 

public static function withData($data){

if (!isset($data["bm_biz_id"])){
      throw new Exception("bizMarketObject constructor requies data array provided!");
}

$instance = new self();

$instance->bm_biz_id = $data["bm_biz_id"];
$instance->bm_rating = $data["bm_rating"];
$instance->bm_user_rating = $data["bm_user_rating"];
$instance->bm_great_app = $data["bm_great_app"];
$instance->bm_editor_choice = $data["bm_editor_choice"];
$instance->bm_biz_ip = $data["bm_biz_ip"];
$instance->bm_biz_long = $data["bm_biz_long"];
$instance->bm_biz_lati = $data["bm_biz_lati"];

return $instance;
}

function _isValid(){
      return true;
}
}
?>


