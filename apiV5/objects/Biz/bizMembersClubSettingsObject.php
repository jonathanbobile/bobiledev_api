<?php 

class bizMembersClubSettingsObject extends bobileObject { 

    
    public $bmcs_biz_id = "0";
    public $bmcs_membership_mode = "open";//enum('open','exclusive')
    public $bmcs_welcome_entity = "points";//enum('points','coupon','custom')
    public $bmcs_welcome_grant = "300";
    public $bmcs_email_on_details_update = "0";
    public $bmcs_welcome_entity_id = "0";
    public $bmcs_welcome_title;
    public $bmcs_welcome_description;
    public $bmcs_welcome_valid_from;
    public $bmcs_welcome_valid_to;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bmcs_biz_id"])){
            throw new Exception("bizMembersClubSettingsObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bmcs_biz_id = $data["bmcs_biz_id"];
        $instance->bmcs_membership_mode = $data["bmcs_membership_mode"];
        $instance->bmcs_welcome_entity = $data["bmcs_welcome_entity"];
        $instance->bmcs_welcome_grant = $data["bmcs_welcome_grant"];
        $instance->bmcs_email_on_details_update = $data["bmcs_email_on_details_update"];
        $instance->bmcs_welcome_entity_id = $data["bmcs_welcome_entity_id"];
        $instance->bmcs_welcome_title = $data["bmcs_welcome_title"];
        $instance->bmcs_welcome_description = $data["bmcs_welcome_description"];
        $instance->bmcs_welcome_valid_from = $data["bmcs_welcome_valid_from"];
        $instance->bmcs_welcome_valid_to = $data["bmcs_welcome_valid_to"];

        return $instance;
    }

    function isWelcomeRewardValid(){
        $result = true;
        switch($this->bmcs_welcome_entity){
            case "custom":
                $afterStart = !isset($this->bmcs_welcome_valid_from) || time() >= strtotime($this->bmcs_welcome_valid_from); 
                $beforeEnd = !isset($this->bmcs_welcome_valid_to) || time() <= strtotime($this->bmcs_welcome_valid_to);

                $result = $afterStart && $beforeEnd;
                break;
            case "coupon":
                $result = couponsManager::isCouponAvailableGeneral($this->bmcs_welcome_entity_id);
                break;
        }

        return $result;
    }

    function _isValid(){
          return true;
    }
}
?>


