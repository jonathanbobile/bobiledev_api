<?php 

class bizStoreSettingsObject extends bobileObject { 

    public $ess_id = "0";
    public $ess_biz_id = "0";
    public $ess_currency = "USD";
    public $ess_currency_symbol = "$";
    public $ess_weight_units;
    public $ess_tax_rate = "0";
    public $ess_shipping_time = "5";
    public $ess_shipping_price = "0";
    public $ess_shipping_policy_on = "0";
    public $ess_refund_policy_on = "1";
    public $ess_custom_policy_on = "0";
    public $ess_shipping_policy;
    public $ess_refund_policy;
    public $ess_custom_policy;
    public $ess_business_name;
    public $ess_phone;
    public $ess_email;
    public $ess_include_tax = "0";
    public $ess_require_shipping = "1";
    public $ess_checkout_comment_title;
    public $ess_product_order = "newest_desc";//enum('newest_desc','newest_asc','abc_desc','abc_asc','price_desc','price_asc')
    public $ess_set_cash_as_paid = "0";
    public $ess_track_stock = "0";
    public $ess_hide_out_of_stock = "0";
    public $ess_require_cash_drawer = "0";
    public $ess_allow_employee_refund = "1";
    public $ess_finalize_by_amount = "0";
    public $ess_finalize_amount = "0";
    public $ess_finalize_by_time = "none";//enum('none','period','recurring')
    public $ess_finalize_period = "day";//enum('day','week','month')
    public $ess_finalize_period_nmber = "0";
    public $ess_auto_reminder = "0";
    public $ess_reminder_after = "none";//enum('none','week','month')
    public $ess_reminder_before = "0";
    public $ess_handle_warn_before_due_date = "1";
    public $ess_handle_unpaid_orders = "0";
    public $ess_points_expire_period = "never";//enum('never','month','years')
    public $ess_points_expire_number = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["ess_id"])){
            throw new Exception("bizStoreSettingsObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->ess_id = $data["ess_id"];
        $instance->ess_biz_id = $data["ess_biz_id"];
        $instance->ess_currency = $data["ess_currency"];
        $instance->ess_currency_symbol = $data["ess_currency_symbol"];
        $instance->ess_weight_units = $data["ess_weight_units"];
        $instance->ess_tax_rate = $data["ess_tax_rate"];
        $instance->ess_shipping_time = $data["ess_shipping_time"];
        $instance->ess_shipping_price = $data["ess_shipping_price"];
        $instance->ess_shipping_policy_on = $data["ess_shipping_policy_on"];
        $instance->ess_refund_policy_on = $data["ess_refund_policy_on"];
        $instance->ess_custom_policy_on = $data["ess_custom_policy_on"];
        $instance->ess_shipping_policy = $data["ess_shipping_policy"];
        $instance->ess_refund_policy = $data["ess_refund_policy"];
        $instance->ess_custom_policy = $data["ess_custom_policy"];
        $instance->ess_business_name = $data["ess_business_name"];
        $instance->ess_phone = $data["ess_phone"];
        $instance->ess_email = $data["ess_email"];
        $instance->ess_include_tax = $data["ess_include_tax"];
        $instance->ess_require_shipping = $data["ess_require_shipping"];
        $instance->ess_checkout_comment_title = $data["ess_checkout_comment_title"];
        $instance->ess_product_order = $data["ess_product_order"];
        $instance->ess_set_cash_as_paid = $data["ess_set_cash_as_paid"];
        $instance->ess_track_stock = $data["ess_track_stock"];
        $instance->ess_hide_out_of_stock = $data["ess_hide_out_of_stock"];
        $instance->ess_require_cash_drawer = $data["ess_require_cash_drawer"];
        $instance->ess_allow_employee_refund = $data["ess_allow_employee_refund"];
        $instance->ess_finalize_by_amount = $data["ess_finalize_by_amount"];
        $instance->ess_finalize_amount = $data["ess_finalize_amount"];
        $instance->ess_finalize_by_time = $data["ess_finalize_by_time"];
        $instance->ess_finalize_period = $data["ess_finalize_period"];
        $instance->ess_finalize_period_nmber = $data["ess_finalize_period_nmber"];
        $instance->ess_auto_reminder = $data["ess_auto_reminder"];
        $instance->ess_reminder_after = $data["ess_reminder_after"];
        $instance->ess_reminder_before = $data["ess_reminder_before"];
        $instance->ess_handle_warn_before_due_date = $data["ess_handle_warn_before_due_date"];
        $instance->ess_handle_unpaid_orders = $data["ess_handle_unpaid_orders"];
        $instance->ess_points_expire_period = $data["ess_points_expire_period"];
        $instance->ess_points_expire_number = $data["ess_points_expire_number"];

        return $instance;
    }

    function _isValid(){
        return true;
    }
}
?>


