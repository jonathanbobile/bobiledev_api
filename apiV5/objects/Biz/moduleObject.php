<?php 

class moduleObject extends bobileObject { 

    public $mod_id = "0";
    public $mod_name;
    public $mod_pic;
    public $mod_active = "0";
    public $mod_index = "0";
    public $mod_description;
    public $mod_short_desc;
    public $mod_payd = "0";
    public $mod_reg_type = "0";
    public $mod_have_ui = "1";
    public $mod_have_layout = "1";
    public $mod_line_color = "#555555";
    public $mod_struct = "0";
    public $mod_type = "1";
    public $mod_feature = "0";
    public $mod_menuid = "0";
    public $mod_sell_text;
    public $mod_price = "0";
    public $mod_group = "0";
    public $mod_store = "0";
    public $mod_plan_group = "0";
    public $mod_set_as_home_screen = "1";
    public $mod_can_be_hidden = "1";
    public $mod_ui_type = "No UI";//enum('Tree','Inner','No UI','List','form','webview')
    public $mod_element_based = "No";//enum('No','Inner','Outer')
    public $mod_turn_to_page_element = "0";
    public $mod_svg = "more.svg";
    public $mod_url;
    public $mod_tooltip_id = "0";
    public $mod_plan_only = "0";
    public $mod_can_link = "0";
    public $mod_can_review = "0";
    public $mod_min_instances = "1";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["mod_id"])){
              throw new Exception("moduleObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->mod_id = $data["mod_id"];
        $instance->mod_name = $data["mod_name"];
        $instance->mod_pic = $data["mod_pic"];
        $instance->mod_active = $data["mod_active"];
        $instance->mod_index = $data["mod_index"];
        $instance->mod_description = $data["mod_description"];
        $instance->mod_short_desc = $data["mod_short_desc"];
        $instance->mod_payd = $data["mod_payd"];
        $instance->mod_reg_type = $data["mod_reg_type"];
        $instance->mod_have_ui = $data["mod_have_ui"];
        $instance->mod_have_layout = $data["mod_have_layout"];
        $instance->mod_line_color = $data["mod_line_color"];
        $instance->mod_struct = $data["mod_struct"];
        $instance->mod_type = $data["mod_type"];
        $instance->mod_feature = $data["mod_feature"];
        $instance->mod_menuid = $data["mod_menuid"];
        $instance->mod_sell_text = $data["mod_sell_text"];
        $instance->mod_price = $data["mod_price"];
        $instance->mod_group = $data["mod_group"];
        $instance->mod_store = $data["mod_store"];
        $instance->mod_plan_group = $data["mod_plan_group"];
        $instance->mod_set_as_home_screen = $data["mod_set_as_home_screen"];
        $instance->mod_can_be_hidden = $data["mod_can_be_hidden"];
        $instance->mod_ui_type = $data["mod_ui_type"];
        $instance->mod_element_based = $data["mod_element_based"];
        $instance->mod_turn_to_page_element = $data["mod_turn_to_page_element"];
        $instance->mod_svg = $data["mod_svg"];
        $instance->mod_url = $data["mod_url"];
        $instance->mod_tooltip_id = $data["mod_tooltip_id"];
        $instance->mod_plan_only = $data["mod_plan_only"];
        $instance->mod_can_link = $data["mod_can_link"];
        $instance->mod_can_review = $data["mod_can_review"];
        $instance->mod_min_instances = $data["mod_min_instances"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


