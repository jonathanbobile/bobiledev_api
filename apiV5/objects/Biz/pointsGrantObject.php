<?php

/**
 * pointsGrantObject short summary.
 *
 * pointsGrantObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class pointsGrantObject extends bobileObject
{
    /* Identifiers */
    public $pg_id = 0;
    public $pg_biz_id = 0;

    /* Settings */
    public $pg_entity_type = 'other';
    public $pg_entity_id = 0;
    public $pg_cost = 0;

    /* Details */
    public $pg_text = "";    
    public $pg_img = "";
    public $pg_title = "";

    public $label = "";
    public $connected_object = null;
    public $connectedItemIsValid = true;

    function __construct(){
        
    }

    public static function withData($pointsGrantData){
        if(!isset($pointsGrantData["pg_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->pg_id = isset($pointsGrantData["pg_id"]) && $pointsGrantData["pg_id"] > 0 ? $pointsGrantData["pg_id"] : 0;
        $instance->pg_biz_id = isset($pointsGrantData["pg_biz_id"]) && $pointsGrantData["pg_biz_id"] > 0 ? $pointsGrantData["pg_biz_id"] : 0;

        /* Settings */
        $instance->pg_entity_type = $pointsGrantData["pg_entity_type"];
        $instance->pg_entity_id = $pointsGrantData["pg_entity_id"];
        $instance->pg_cost = $pointsGrantData["pg_cost"];

        /* Details */
        $instance->pg_text = $pointsGrantData["pg_text"];            
        $instance->pg_img = $pointsGrantData["pg_img"];
        $instance->pg_title = $pointsGrantData["pg_title"];

        if($instance->pg_entity_type == 'product'){
            $levelManager = new levelDataManager(9);

            $productResult = $levelManager->getLevelDataByID($instance->pg_entity_id);
            if($productResult->code == 1){
                $instance->connected_object = $productResult->data;
                $instance->label = $productResult->data->md_head;
            }
            else{
                $instance->connectedItemIsValid = false;
            }
        }
        else if($instance->pg_entity_type == 'service'){
            $serviceResult = bookingManager::getServiceByID($instance->pg_entity_id);
            if($serviceResult->code == 1){
                $instance->connected_object = $serviceResult->data;
                $instance->label = $serviceResult->data->bmt_name;
            }
            else{
                $instance->connectedItemIsValid = false;
            }
        }
        else{
            $instance->label = $instance->pg_title;
        }

        return $instance;
    }

    public function getAPIFormattedArray(){
        $result = array();

        $result["id"] = $this->pg_id;
        $result["external_id"] = $this->pg_entity_id;
        $result["type"] = $this->pg_entity_type;
        $result["text"] = $this->label;
        $result["description"] = $this->pg_text;
        $result["img"] = $this->pg_img == "" ? "https://storage.googleapis.com/bobile/images/points_shop_default.jpg" : $this->pg_img;
        $result["points"] = $this->pg_cost;

        return $result;
    }
}
