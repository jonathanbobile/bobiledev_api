<?php

/**
 * classDateObject short summary.
 *
 * classDateObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class classDateObject extends bobileObject
{
    /* Identifiers */
    public $ccd_id;
    public $ccd_biz_id;
    public $ccd_class_id;

    /* Settings */
    public $ccd_start_date;
    public $ccd_start_time;
    public $ccd_end_date;
    public $ccd_end_time;
    public $ccd_iscanceled;
    public $ccd_isvisible;

    public $start_hour;
    public $end_hour;

    public $booked_count;
    public $class_date_customers = array();

    public static function withData($classDateData){
        if(!isset($classDateData["ccd_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->ccd_id = isset($classDateData["ccd_id"]) && $classDateData["ccd_id"] > 0 ? $classDateData["ccd_id"] : 0;
        $instance->ccd_biz_id = isset($classDateData["ccd_biz_id"]) && $classDateData["ccd_biz_id"] > 0 ? $classDateData["ccd_biz_id"] : 0;
        $instance->ccd_class_id = isset($classDateData["ccd_class_id"]) && $classDateData["ccd_class_id"] > 0 ? $classDateData["ccd_class_id"] : 0;

        /* Settings */
        $instance->ccd_start_date = $classDateData["ccd_start_date"];
        $instance->ccd_start_time = $classDateData["ccd_start_time"];
        $instance->ccd_end_date = $classDateData["ccd_end_date"];
        $instance->ccd_end_time = $classDateData["ccd_end_time"];
        $instance->ccd_iscanceled = $classDateData["ccd_iscanceled"];
        $instance->ccd_isvisible = $classDateData["ccd_isvisible"];

        $instance->start_hour = $classDateData['start_hour'];
        $instance->end_hour = $classDateData['end_hour'];

        $instance->booked_count = bookingManager::getClassDateBookedCount($instance->ccd_id);

        return $instance;
    }

    public function getAPIFormattedArray(mobileRequestObject $requestObj){
        $result = array();

        $startStamp = strtotime( $this->ccd_start_date." UTC");
        $startTimeStamp = strtotime( $this->ccd_start_date . " " . $this->start_hour.":00 UTC");
        $endTimeStamp = strtotime( $this->ccd_start_date . " " . $this->end_hour.":00 UTC");

        $result['ccd_id']=$this->ccd_id;
        $result['start_date']=$startStamp;
        $result['start_time']=$this->startDate;
        $result['start_timestamp']=$startTimeStamp;
        $result['end_time']=$this->endDate;
        $result['end_timestamp']=$endTimeStamp;
        $result['taken_slots']=$this->booked_count;
        $result['is_listed'] = bookingManager::isCustomerInClassDate($requestObj->server_customer_id,$this) ? 1 : 0;
        $result['in_sub'] = customerSubscriptionManager::isItemInCustomerSubscription($requestObj->server_customer_id,'class',$this->ccd_class_id) ? "1" : "0";

        return $result;
    }

    public function setBookedCount($count){
        $this->booked_count = $count;
    }

    public function getBookedCount(){
        return $this->booked_count;
    }

    public function setCustomers($customerslist){
        $this->class_date_customers = $customerslist;
    }

    public function getCustomers(){
        return $this->class_date_customers;
    }
}
