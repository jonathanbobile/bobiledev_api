<?php

/**
 * employeeMeetingObject short summary.
 *
 * employeeMeetingObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class employeeMeetingObject extends customerPersonalObject
{
    public $em_id = "0";
    public $em_emp_id = "0";
    public $em_biz_id = "0";
    public $em_name;
    public $em_scheduled_on = "";
    public $em_class_id = "0";
    public $em_class_date_id = "0";
    public $em_start_date;
    public $em_start_time = "0";
    public $em_start;
    public $em_end_date;
    public $em_end_time = "0";
    public $em_end;
    public $em_color = "#fe789d";
    public $em_from_mobile = "0";
    public $em_cust_device_id;
    public $em_cust_id = "0";
    public $em_cust_name;
    public $em_cust_cancelled = "0";
    public $em_meet_type = "0";
    public $em_cust_request;
    public $em_is_recurring = "none";//enum('none','weekly','monthly','yearly')
    public $em_recurring_group = "0";
    public $em_external_id;
    public $em_source = "Internal";//enum('Internal','External')
    public $em_all_day_event = "0";
    public $em_payment_source = "none";//enum('none','order','subscription','punch_pass')
    public $em_payment_source_id = "0";
    public $em_payment_source_inner_id = 0;

    /* connected */
    public $employee;
    public $customer;
    
    public static function withData($employeeMeetingData){
        if(!isset($employeeMeetingData["em_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->em_id = isset($employeeMeetingData["em_id"]) && $employeeMeetingData["em_id"] > 0 ? $employeeMeetingData["em_id"] : 0;
        $instance->em_emp_id = isset($employeeMeetingData["em_emp_id"]) ? $employeeMeetingData["em_emp_id"] : 0;
        $instance->em_biz_id = isset($employeeMeetingData["em_biz_id"]) && $employeeMeetingData["em_biz_id"] > 0 ? $employeeMeetingData["em_biz_id"] : 0;
        $instance->em_class_id = isset($employeeMeetingData["em_class_id"]) ? $employeeMeetingData["em_class_id"] : 0;
        $instance->em_class_date_id = isset($employeeMeetingData["em_class_date_id"]) ? $employeeMeetingData["em_class_date_id"] : 0;
        $instance->em_cust_id = isset($employeeMeetingData["em_cust_id"]) ? $employeeMeetingData["em_cust_id"] : 0;

        /* Details */
        $instance->em_name = $employeeMeetingData["em_name"];
        $instance->em_scheduled_on = $employeeMeetingData["em_scheduled_on"];
        $instance->em_cust_name = $employeeMeetingData["em_cust_name"];
        $instance->em_color = $employeeMeetingData["em_color"];

        /* Settings */
        $instance->em_start_date = $employeeMeetingData["em_start_date"];
        $instance->em_start_time = $employeeMeetingData["em_start_time"];
        $instance->em_start = $employeeMeetingData["em_start"];
        $instance->em_end_date = $employeeMeetingData["em_end_date"];
        $instance->em_end_time = $employeeMeetingData["em_end_time"];
        $instance->em_end = $employeeMeetingData["em_end"];        
        $instance->em_from_mobile = $employeeMeetingData["em_from_mobile"];
        $instance->em_cust_device_id = $employeeMeetingData["em_cust_device_id"];
        $instance->em_cust_cancelled = $employeeMeetingData["em_cust_cancelled"];
        $instance->em_meet_type = $employeeMeetingData["em_meet_type"];
        $instance->em_cust_request = $employeeMeetingData["em_cust_request"];
        $instance->em_is_recurring = $employeeMeetingData["em_is_recurring"];
        $instance->em_recurring_group = $employeeMeetingData["em_recurring_group"];
        $instance->em_external_id = $employeeMeetingData["em_external_id"];
        $instance->em_source = $employeeMeetingData["em_source"];
        $instance->em_all_day_event = $employeeMeetingData["em_all_day_event"];
        $instance->em_payment_source = $employeeMeetingData["em_payment_source"];
        $instance->em_payment_source_id = $employeeMeetingData["em_payment_source_id"];
        $instance->em_payment_source_inner_id = $employeeMeetingData["em_payment_source_inner_id"];

        if($instance->em_emp_id > 0){
            $employeeResult = bookingManager::getEmployeeByID($instance->em_emp_id);
            if($employeeResult->code == 1){
                $instance->employee = $employeeResult->data;
            }
        }
        else{
            $instance->employee = new employeeObject();
            $instance->employee->be_name = "";
        }
        

        return $instance;
    }

    public function PersonalZoneAPIArray(mobileRequestObject $request = null){
        $result = array();

        $startStamp = strtotime( $this->em_start );
        $endStamp = strtotime( $this->em_end );

        $result["meet_id"] = $this->em_id;
        $result["meet_secondary_id"] = 0;
        $result["meet_emp_id"] = $this->em_emp_id;
        $result["meet_emp_name"] = $this->employee->be_name;
        $result["meet_emp_pic"] = $this->employee->be_pic == null ? "" : $this->employee->be_pic;
        $result["meet_type_id"] = $this->em_meet_type;
        $result["meet_type_name"] = $this->em_name;
        $result["meet_start_timestamp"] = $startStamp;
        $result["meet_end_timestamp"] = $endStamp;

        return $result;
    }

    public function setEmployee(employeeObject $employee){
        $this->employee = $employee;
    }

    public function getEmployee(){
        return $this->employee;
    }

    public function setCustomer(customerObject $customer){
        $this->customer = $customer;
    }

    public function getCustomer(){
        return $this->customer;
    }
}
