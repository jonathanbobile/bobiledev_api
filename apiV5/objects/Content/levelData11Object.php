<?php 

class levelData11Object extends levelDataObject { 

    public $md_mod_id = "11";
    public $md_usage_total_capacity = "0";
    public $md_usage_period;//enum('unlimited','limited','period')
    public $md_usage_period_unit;//enum('day','week','month','year','')
    public $md_usage_period_number = "0";
    public $md_usage_period_start = "";
    public $md_usage_period_end;
    public $md_usage_rule_type = "unlimited";//enum('unlimited','n_times_period')
    public $md_usage_rule_capacity = "0";
    public $md_usage_rule_period_unit;//enum('day','week','month','year','hour')
    public $md_color;

    public $md_deleted = "0";
    public $extra_data = array();

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData11Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        $instance->md_usage_total_capacity = $data["md_usage_total_capacity"];
        $instance->md_usage_period = $data["md_usage_period"];
        $instance->md_usage_period_unit = $data["md_usage_period_unit"];
        $instance->md_usage_period_number = $data["md_usage_period_number"];
        $instance->md_usage_period_start = $data["md_usage_period_start"];
        $instance->md_usage_period_end = $data["md_usage_period_end"];
        $instance->md_usage_rule_type = $data["md_usage_rule_type"];
        $instance->md_usage_rule_capacity = $data["md_usage_rule_capacity"];
        $instance->md_usage_rule_period_unit = $data["md_usage_rule_period_unit"];
        $instance->md_color = $data["md_color"];

        $instance->extra_data = levelDataManager::getPunchPassExtraData($instance);
        $instance->extra_data["connected_items"] = levelDataManager::getPunchPassConnectedItems($instance);
        $instance->md_deleted = $data["md_deleted"];

        return $instance;
    }
}
?>


