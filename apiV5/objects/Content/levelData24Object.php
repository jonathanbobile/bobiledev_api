<?php 

class levelData24Object extends levelDataObject { 

    public $md_mod_id = "24";

    public $md_first_img;
    public $md_temp = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData24Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        if($data["md_icon"] == "" && $data["md_element"] == 10 && $data["md_content"] == "1"){
            $instance->md_icon = levelDataManager::getVideoImage($data["md_biz_id"],$data["md_row_id"]);
        }

        $instance->md_photos = ($data["md_photos"] == 0) ? "0" : $data["md_photos"];
        if($data["md_element"] == 10){
            $instance->md_photos = levelDataManager::getVideosCount($instance);
            $instance->md_albums = levelDataManager::getVideoAlbumCount($instance);
        }

        $instance->md_first_img = $data["md_first_img"];
        $instance->md_temp = $data["md_temp"];

        return $instance;
    }
}
?>


