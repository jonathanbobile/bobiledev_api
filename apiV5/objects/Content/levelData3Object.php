<?php 

class levelData3Object extends levelDataObject { 

    public $md_mod_id = "3";
    public $postTimeStamp = "";
    public $postTime = "";
    public $news = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData3Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        $to_time = strtotime($data["md_date"]);
        $from_time = strtotime("now");
        
        $mins = round(abs($to_time - $from_time) / 60,0);
        $multyTime = ($mins > 1) ? "s" : "";
        $dtData = $mins. " minute$multyTime ago";
        if($mins >= 60)
        {
            $mins = round($mins / 60,0);
            $multyTime = ($mins > 1) ? "s" : "";
            $dtData = $mins. " hour$multyTime ago";
            if($mins >= 24)
            {
                $mins = round($mins / 24,0);
                $multyTime = ($mins > 1) ? "s" : "";
                $dtData = $mins. " day$multyTime ago";
            }
        }

        $instance->postTimeStamp=$to_time;
        $instance->postTime=$dtData;

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        if($instance->md_level_no == 1){
            $instance->news = levelDataManager::getfirstTextElementForNewsChildObject($instance->md_row_id);
        }

        return $instance;
    }
}
?>


