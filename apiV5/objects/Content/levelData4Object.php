<?php 

class levelData4Object extends levelDataObject { 

    public $md_mod_id = "4";

    public $md_first_img;
    public $md_temp = "0";
    public $md_thumb_uploaded = "1";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData4Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        if($data["md_icon"] == "" && $data["md_element"] == 10){
            if($data["md_first_img"] == ""){
                $image = levelDataManager::getGalleryImage($data["md_biz_id"],$data["md_row_id"]);

                if($image){
                    $instance->md_icon = $image;
                }
                else{
                    $instance->md_icon = '';
                }
            }
            else{
                $instance->md_icon = $data["md_first_img"];
            }            
        }

        $instance->md_photos = ($data["md_photos"] == 0) ? "0" : $data["md_photos"];
        if($data["md_element"] == 10){
            $instance->md_photos = levelDataManager::getImagesCount($instance);
            $instance->md_albums = levelDataManager::getImageAlbumCount($instance);
        }

        $instance->md_first_img = $data["md_first_img"];
        $instance->md_temp = $data["md_temp"];
        $instance->md_thumb_uploaded = $data["md_thumb_uploaded"];

        return $instance;
    }
}
?>


