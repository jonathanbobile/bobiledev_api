<?php

/**
 * customerBenefitObject short summary.
 *
 * customerBenefitObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerBenefitObject extends customerPersonalObject
{
    public $cb_id = "0";
    public $cb_biz_id = "0";
    public $cb_cust_id = "0";
    public $cb_type;//enum('loyalty','personal','points_redeem','membership','scratch','cust_event','coupon')
    public $cb_source_row_id = "0";
    public $cb_external_id = "0";
    public $cb_date = "";
    public $cb_title;
    public $cb_description;
    public $cb_image;
    public $cb_valid_start = "";
    public $cb_valid_end;
    public $cb_redeemed = "0";
    public $cb_redeemed_date;
    public $cb_code = "0";
    public $cb_item_type;//enum('','product','service','other','points','class')
    public $cb_scard_id = "0";
    public $cb_value = "0";
    public $cb_granted_by;//enum('event','purchase','direct','loyalty','membership','scratch')
    public $cb_granted_by_id = "0";

    /* Connected items */
    public $redeemed_item; // The item that the benefit was redeemd for (if it is redeemed)
    public $benefit_source;//The item that granted the benefit: 'loyalty','personal','points_redeem','membership','scratch','cust_event','coupon'

    public static function withData($custBenefitdata){
        if(!isset($custBenefitdata["cb_id"])){
            throw new Exception("Data incorrect");
        }

        $now = new DateTime("now");


        $instance = new self();

        /* Identifiers */
        $instance->cb_id = isset($custBenefitdata["cb_id"]) ? $custBenefitdata["cb_id"] : 0;
        $instance->cb_biz_id = isset($custBenefitdata["cb_biz_id"]) ? $custBenefitdata["cb_biz_id"] : 0;
        $instance->cb_cust_id = isset($custBenefitdata["cb_cust_id"]) ? $custBenefitdata["cb_cust_id"] : 0;

        /* Settings */
        $instance->cb_type = $custBenefitdata["cb_type"];
        $instance->cb_source_row_id = $custBenefitdata["cb_source_row_id"];
        $instance->cb_external_id = $custBenefitdata["cb_external_id"];
        $instance->cb_date = $custBenefitdata["cb_date"];
        $instance->cb_title = $custBenefitdata["cb_title"];
        $instance->cb_description = $custBenefitdata["cb_description"];
        $instance->cb_image = $custBenefitdata["cb_image"];
        $instance->cb_valid_start = isset($custBenefitdata["cb_valid_start"]) && $custBenefitdata["cb_valid_start"] != "" ? $custBenefitdata["cb_valid_start"] : $now->format('Y-m-d H:i:s');
        $instance->cb_valid_end = $custBenefitdata["cb_valid_end"];
        $instance->cb_redeemed = $custBenefitdata["cb_redeemed"];
        $instance->cb_redeemed_date = $custBenefitdata["cb_redeemed_date"];
        $instance->cb_code = $custBenefitdata["cb_code"];
        $instance->cb_item_type = $custBenefitdata["cb_item_type"];
        $instance->cb_scard_id = $custBenefitdata["cb_scard_id"];
        $instance->cb_value = $custBenefitdata["cb_value"];        
        $instance->cb_granted_by = $custBenefitdata["cb_granted_by"];
        $instance->cb_granted_by_id = $custBenefitdata["cb_granted_by_id"];    

        

        return $instance;
    }

    
}
