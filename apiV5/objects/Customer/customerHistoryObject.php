<?php

/**
 * custHistoryObject short summary.
 *
 * custHistoryObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerHistoryObject extends bobileObject
{
    /* Identifiers */
    public $ch_id = 0;
    public $ch_cust_id = 0;

    /* Details */
    public $ch_created = '';
    public $ch_status = '';
    public $ch_replace_text = '';
    public $ch_short_mess = '';
    public $ch_auto = 1;
    public $ch_action_id = 0;
    public $ch_device_id = '';
    public $ch_external_type = '';
    public $ch_external_id = 0;
    public $ch_external_source_entity = '';
    public $ch_external_source_id = 0;

    public static function withData($custHistoryEntrydata){
        if(!isset($custHistoryEntrydata["ch_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->ch_id = isset($custHistoryEntrydata["ch_id"]) && $custHistoryEntrydata["ch_id"] > 0 ? $custHistoryEntrydata["ch_id"] : 0;
        $instance->ch_cust_id = isset($custHistoryEntrydata["ch_cust_id"]) && $custHistoryEntrydata["ch_cust_id"] > 0 ? $custHistoryEntrydata["ch_cust_id"] : 0;

        /* Details */
        $instance->ch_created = isset($custHistoryEntrydata["ch_created"]) ? $custHistoryEntrydata["ch_created"] : $instance->ch_created;
        $instance->ch_status = $custHistoryEntrydata["ch_status"];
        $instance->ch_replace_text = $custHistoryEntrydata["ch_replace_text"];
        $instance->ch_short_mess = $custHistoryEntrydata["ch_short_mess"];
        $instance->ch_auto = $custHistoryEntrydata["ch_auto"];
        $instance->ch_action_id = $custHistoryEntrydata["ch_action_id"];
        $instance->ch_device_id = $custHistoryEntrydata["ch_device_id"];
        $instance->ch_external_type = $custHistoryEntrydata["ch_external_type"];
        $instance->ch_external_id = $custHistoryEntrydata["ch_external_id"];
        $instance->ch_external_source_entity = $custHistoryEntrydata["ch_external_source_entity"];
        $instance->ch_external_source_id = $custHistoryEntrydata["ch_external_source_id"];

        return $instance;
    }
}
