<?php

/**
 * customerPucnCardStampObject short summary.
 *
 * customerPucnCardStampObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerLoyaltyCardStampObject extends bobileObject
{

    /* Identifiers */
    public $customer_card_stamp_id = "0";
    public $customer_card_stamp_card_id = "0";

    /* Details */
    public $customer_card_stamp_date;
    public $customer_card_stamp_origin;//enum('manual','purchase','','')
    public $customer_card_stamp_details;
    public $customer_card_status = "Punch";//enum('Punch','Redeem','Start new card','Complete card')
    public $customer_card_stamp_origin_id = 0;

    public static function withData($cardStampData){
        if(!isset($cardStampData["customer_card_stamp_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->customer_card_stamp_id = isset($cardStampData["customer_card_stamp_id"]) ? $cardStampData["customer_card_stamp_id"] : 0;
        $instance->customer_card_stamp_card_id = isset($cardStampData["customer_card_stamp_card_id"]) ? $cardStampData["customer_card_stamp_card_id"] : 0;

        /* Details */
        $instance->customer_card_stamp_date = $cardStampData["customer_card_stamp_date"];
        $instance->customer_card_stamp_origin = $cardStampData["customer_card_stamp_origin"];
        $instance->customer_card_stamp_details = $cardStampData["customer_card_stamp_details"];
        $instance->customer_card_status = $cardStampData["customer_card_status"];
        $instance->customer_card_stamp_origin_id = $cardStampData['customer_card_stamp_origin_id'];
        return $instance;
    }
}
