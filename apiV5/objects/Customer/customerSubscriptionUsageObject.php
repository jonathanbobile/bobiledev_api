<?php 

class customerSubscriptionUsageObject extends bobileObject { 

    public $csuse_id = "0";
    public $csuse_cust_id = "0";
    public $csuse_biz_id = "0";
    public $csuse_csu_id = "0";
    public $csuse_type;//enum('usage','renew','usage_renew','freeze','resume','cancel','start','user_cancel','ended')
    public $csuse_time = "";
    public $csuse_item_type;//enum('product','service','class','other','order','')
    public $csuse_item_id = "0";
    public $csuse_source = "auto";//enum('auto','manual','scan','geolocation')
    public $csuse_source_id = "0";
    public $csuse_note;
    public $csuse_status = "active";//enum('active','revoked')
    public $csuse_status_change_time;
    public $csuse_status_changer_id = "0";
    public $csuse_status_note = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["csuse_id"])){
            throw new Exception("customerSubscriptionUsageObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->csuse_id = $data["csuse_id"];
        $instance->csuse_cust_id = $data["csuse_cust_id"];
        $instance->csuse_biz_id = $data["csuse_biz_id"];
        $instance->csuse_csu_id = $data["csuse_csu_id"];
        $instance->csuse_type = $data["csuse_type"];
        $instance->csuse_time = $data["csuse_time"];
        $instance->csuse_item_type = $data["csuse_item_type"];
        $instance->csuse_item_id = $data["csuse_item_id"];
        $instance->csuse_source = $data["csuse_source"];
        $instance->csuse_source_id = $data["csuse_source_id"];
        $instance->csuse_note = $data["csuse_note"];
        $instance->csuse_status = $data["csuse_status"];
        $instance->csuse_status_change_time = $data["csuse_status_change_time"];
        $instance->csuse_status_changer_id = $data["csuse_status_changer_id"];
        $instance->csuse_status_note = $data["csuse_status_note"];

        return $instance;
    }

    function _isValid(){
        return true;
    }
}
?>


