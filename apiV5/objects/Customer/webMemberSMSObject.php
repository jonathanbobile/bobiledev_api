<?php 

class webMemberSMSObject extends bobileObject { 

    public $wms_id = "0";
    public $wms_biz_id = "0";
    public $wms_cust_id = "0";
    public $wms_push_type = "0";
    public $wms_extra_params;
    public $wms_sent_on = "";
    public $wms_seen_on;

     function __construct(){} 

    public static function withData($data){

        if (!isset($data["wms_id"])){
              throw new Exception("webMemberSMSObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->wms_id = $data["wms_id"];
        $instance->wms_biz_id = $data["wms_biz_id"];
        $instance->wms_cust_id = $data["wms_cust_id"];
        $instance->wms_push_type = $data["wms_push_type"];
        $instance->wms_extra_params = $data["wms_extra_params"];
        $instance->wms_sent_on = $data["wms_sent_on"];
        $instance->wms_seen_on = $data["wms_seen_on"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


