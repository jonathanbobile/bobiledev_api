<?php 

class customerTransactionHistory extends bobileObject { 

    public $ctoh_id = "0";
    public $ctoh_order_id = "0";
    public $ctoh_status_id = "0";
    public $ctoh_status_date = "";
    public $ctos_id = "0";
    public $ctos_name;
    public $ctos_color;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["ctoh_id"])){
              throw new Exception("customerTransactionHistory constructor requies data array provided!");
        }

        $instance = new self();

        $instance->ctoh_id = $data["ctoh_id"];
        $instance->ctoh_order_id = $data["ctoh_order_id"];
        $instance->ctoh_status_id = $data["ctoh_status_id"];
        $instance->ctoh_status_date = $data["ctoh_status_date"];
        $instance->ctos_id = $data["ctos_id"];
        $instance->ctos_name = $data["ctos_name"];
        $instance->ctos_color = $data["ctos_color"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


