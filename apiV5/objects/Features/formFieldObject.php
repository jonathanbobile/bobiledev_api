<?php 

class formFieldObject extends bobileObject { 

    public $fld_id = "0";
    public $fld_form_id = "0";
    public $fld_index = "0";
    public $fld_type_id = "0";
    public $fld_label;
    public $fld_visible = "1";
    public $fld_required = "0";
    public $fft_id = 0;
    public $fft_index = 0;
    public $fft_label = "";
    public $fft_icon = "";
    public $fld_type = "";
    public $fft_db_type = "";
    public $fld_type_ios = 0;
    public $fld_value = "";
    public $combo_data = array();

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["fld_id"])){
              throw new Exception("formFieldObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fld_id = $data["fld_id"];
        $instance->fld_form_id = $data["fld_form_id"];
        $instance->fld_index = $data["fld_index"];
        $instance->fld_type_id = $data["fld_type_id"];
        $instance->fld_label = $data["fld_label"];
        $instance->fld_visible = $data["fld_visible"];
        $instance->fld_required = $data["fld_required"];

        $instance->fft_id = isset($data["fft_id"]) ? $data["fft_id"] : 0;
        $instance->fft_index = isset($data["fft_index"]) ? $data["fft_index"] : 0;
        $instance->fft_label = isset($data["fft_label"]) ? $data["fft_label"] : "";
        $instance->fft_icon = isset($data["fft_icon"]) ? $data["fft_icon"] : "";
        $instance->fld_type = isset($data["fft_prefix"]) ? $data["fft_prefix"] : "";
        $instance->fft_db_type = isset($data["fft_db_type"]) ? $data["fft_db_type"] : "";
        $instance->fld_type_ios = isset($data["fft_prefix_ios"]) ? $data["fft_prefix_ios"] : 0;

        $comboData = array();
        if($instance->fld_type_id == "5"){
            
            $dataCombo = customFormManager::getFormComboBoxValues_DB($instance->fld_id);
            if(count($dataCombo)>0){
                
                $objectValue = array();
                $objectValue["value_id"] = "0";
                $objectValue["value_label"] = "";
                array_push($comboData,$objectValue);
                
                foreach ($dataCombo as $comboValue)
                {
                    $objectValue["value_id"] = $comboValue["fcv_id"];
                    $objectValue["value_label"] = stripslashes($comboValue["fcv_value"]);
                    array_push($comboData,$objectValue);
                }
            }                    
        }
        $instance->combo_data = $comboData;

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


