<?php

/**
 * adminRequestObject short summary.
 *
 * adminRequestObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class adminRequestObject
{
    public $acID;
    public $resellerID;
    public $idemKey;
    public $deviceID;
    public $deviceType;
	public $deviceModel;
    public $deviceOrient;
    public $OS;
    public $OSVersion;
    public $lang;
    public $bobileVersionNumber;
    public $long;
    public $lati;
    public $appid;
    public $bundleId;

    function __construct(){}

    public static function withData($GET){
        $instance = new self();

        $instance->acID = utilityManager::decodeFromHASH($GET["ac_id"]);
        $instance->resellerID = utilityManager::decodeFromHASH($GET["re_id"]);
        $instance->idemKey = $GET["idem_key"];
        $instance->deviceID = $GET["deviceID"];
        $instance->lang = isset($GET["lang"]) ? $GET["lang"] : "en";
        $instance->deviceType = isset($GET["deviceType"]) ? $GET["deviceType"] : "";
        $instance->deviceModel = isset($GET["deviceModel"]) ? $GET["deviceModel"] : "";
        $instance->deviceOrient = isset($GET["deviceOrient"]) ? $GET["deviceOrient"] : "";
        $instance->OS = isset($GET["OS"]) ? $GET["OS"] : "Android";
        $instance->OSVersion = isset($GET["OSVersion"]) ? $GET["OSVersion"] : "";
        $instance->bobileVersionNumber = isset($GET["bobileVersionNumber"]) ? $GET["bobileVersionNumber"] : 0;
        $instance->long = isset($GET["long"]) ? $GET["long"] : "";
        $instance->lati = isset($GET["lati"]) ? $GET["lati"] : "";
        $instance->appid = (!isset($GET["appid"]) || $GET["appid"] == 'null' || $GET["appid"] == '') ? 0 : $GET["appid"];
        $instance->bundleId = isset($GET["bundle_id"]) ? $GET["bundle_id"] : "";

        return $instance;
    }
}