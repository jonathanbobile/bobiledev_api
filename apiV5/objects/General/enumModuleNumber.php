<?php

/**
 * enumModuleNumber - number of moduls like in tbl_module table.
 * 
 *
 * @version 1.0
 * @author JonathanM
 */
class enumModuleNumber extends Enum{

    const main_menu = 0;
    const about_us = 1;
    const show_room = 2;
    const news = 3;
    const gallery = 4;
    const loyalty_card = 6;
    const chat = 7;
    const payment_request = 8;
    const mobile_shop = 9;
    const mobile_shop_category = 999;
    const subscription = 10;
    const punch_pass = 11;
    const file_sharing = 12;
    const scratch_card = 13;
    const membership = 14;
    const personal_zone = 16;
    const booking = 17;
    const point_shop = 18;
    const social_kit = 19;
    const tutorials = 23;
    const video_sharing = 24;
    const show_room_2 = 25;
    const coupons = 26;
    const blog = 27;
    const push_manager = 29;
    const customer_groups = 30;
    const custom_form = 32;
    const classes = 34;
    const web_view = 25;
}

    

