<?php 

class guestObject extends bobileObject { 

    public $gu_id = "0";
    public $gu_account_id = "0";
    public $gu_name;
    public $gu_biz_id = "0";
    public $gu_isactive = "0";
    public $gu_biz_invite_date = "";
    public $gu_invite_token;
    public $gu_verify_code;
    public $gu_isrevoked = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["gu_id"])){
              throw new Exception("guest constructor requies data array provided!");
        }

        $instance = new self();

        $instance->gu_id = $data["gu_id"];
        $instance->gu_account_id = $data["gu_account_id"];
        $instance->gu_name = $data["gu_name"];
        $instance->gu_biz_id = $data["gu_biz_id"];
        $instance->gu_isactive = $data["gu_isactive"];
        $instance->gu_biz_invite_date = $data["gu_biz_invite_date"];
        $instance->gu_invite_token = $data["gu_invite_token"];
        $instance->gu_verify_code = $data["gu_verify_code"];
        $instance->gu_isrevoked = $data["gu_isrevoked"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


