<?php

/**
 * levelDataObject short summary.
 *
 * levelDataObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class levelDataObject extends bobileObject
{
    /* Identifiers */
    public $md_row_id = "0";
    public $md_row_id_desc = "0";
    public $md_biz_id = "0";
    public $md_mod_id = "1";

    /* Settings */
    public $md_level_no = "1";
    public $md_parent = "0";
    public $md_index = "0";
    public $md_element = "0";
    public $md_editble = "1";
    public $md_required = "0";
    public $md_dragble = "1";
    public $md_content = "0";
    public $md_icon = "";
    public $md_pic = "";
    public $md_head = "";
    public $md_desc = "";
    public $md_loc = "";
    public $md_date;
    public $md_info = "";
    public $md_score = "0";
    public $md_price = "0";
    public $md_info1 = "";
    public $md_info2 = "";
    public $md_info3 = "";
    public $md_info4 = "";
    public $md_info5 = "";
    public $md_pic1 = "";
    public $md_pic2 = "";
    public $md_pic3 = "";
    public $md_pic4 = "";
    public $md_pic5 = "";
    public $md_pic6 = "";
    public $md_pic7 = "";
    public $md_pic8 = "";
    public $md_pic9 = "";
    public $md_pic10 = "";
    public $md_date1;
    public $md_date2;
    public $md_int1 = "0";
    public $md_int2 = "0";
    public $md_int3 = "0";
    public $md_bool1 = 0;
    public $md_bool2 = 0;
    public $md_biz_id_real = "0";
    public $md_facebook_id;
    public $md_hidden = "0";
    public $md_tap_like = "0";
    public $md_visible_element = "1";
    public $md_external_id = "0";
    public $md_external_sub_id = "0";
    public $md_external_type = "";
    public $md_next_view = "";
    public $md_review_score = "0";
    public $item_type = 0;
    public $currency = "USD";
    public $variations_count = 0;
    public $in_sub = 0;
    public $cnt=0;
    public $md_type = "";
    public $biz_level_img = "";
    public $md_link="";
    public $md_albums=0;
    public $md_photos=0;
    public $md_original_price=0;
    public $item_sku="";
    public $label_key_ios="";
    public $label_key_android="";
    public $md_external_has_action = 0;
    public $md_external_action_title = "";
    public $md_deleted = 0;


    public static function withData($levelData){

        $instance = new self();
        return $instance;
    }

    public function fillLevelData($levelData){

        $bizModel = new bizModel($levelData["md_biz_id"]);
        $storeRow = $bizModel->getBizStoreSettings();

        $this->md_row_id = $levelData["md_row_id"];
        $this->md_row_id_desc = isset($levelData["md_row_id_desc"]) ? $levelData["md_row_id_desc"] : 0;
        $this->md_biz_id = $levelData["md_biz_id"];
        $this->md_mod_id = $levelData["md_mod_id"];
        $this->md_level_no = isset($levelData["md_level_no"]) ? $levelData["md_level_no"] : 1;
        $this->md_parent = isset($levelData["md_parent"]) ? $levelData["md_parent"] : 0;
        $this->md_index = isset($levelData["md_index"]) ? $levelData["md_index"] : 0; 
        $this->md_element = isset($levelData["md_element"]) ? $levelData["md_element"] : 0;
        $this->md_editble = isset($levelData["md_editble"]) ? $levelData["md_editble"] : 0; 
        $this->md_required = isset($levelData["md_required"]) ? $levelData["md_required"] : 0; 
        $this->md_dragble = isset($levelData["md_dragble"]) ? $levelData["md_dragble"] : 0; 
        $this->md_content = isset($levelData["md_content"]) ? $levelData["md_content"] : 0; 
        $this->md_icon = isset($levelData["md_icon"]) ? $levelData["md_icon"] : "";        
        $this->md_head = isset($levelData["md_head"]) ? stripslashes($levelData["md_head"]) : ""; 
        $this->md_desc = isset($levelData["md_desc"]) ? $levelData["md_desc"] : ""; 
        $this->md_loc = !isset($levelData["md_loc"]) || $levelData["md_loc"] == null ? "" : $levelData["md_loc"];
        $this->md_date = isset($levelData["md_date"]) ? $levelData["md_date"] : ""; 
        $this->md_info = isset($levelData["md_info"]) ? $levelData["md_info"] : ""; 
        $this->md_score = !isset($levelData["md_score"]) || $levelData["md_score"] == null ? 0 : $levelData["md_score"];
        $this->md_price = !isset($levelData["md_price"]) || $levelData["md_price"] == null ? 0 : $levelData["md_price"];
        $this->md_info1 = isset($levelData["md_info1"]) ? $levelData["md_info1"] : ""; 
        $this->md_info2 = isset($levelData["md_info2"]) ? $levelData["md_info2"] : ""; 
        $this->md_info3 = isset($levelData["md_info3"]) ? $levelData["md_info3"] : ""; 
        $this->md_info4 = isset($levelData["md_info4"]) ? $levelData["md_info4"] : ""; 
        $this->md_info5 = isset($levelData["md_info5"]) ? $levelData["md_info5"] : ""; 
        $this->md_pic = !isset($levelData["md_pic"]) || $levelData["md_pic"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic"]);
        $this->md_pic1 = !isset($levelData["md_pic1"]) || $levelData["md_pic1"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic1"]);
        $this->md_pic2 = !isset($levelData["md_pic2"]) || $levelData["md_pic2"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic2"]);
        $this->md_pic3 = !isset($levelData["md_pic3"]) || $levelData["md_pic3"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic3"]);
        $this->md_pic4 = !isset($levelData["md_pic4"]) || $levelData["md_pic4"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic4"]);
        $this->md_pic5 = !isset($levelData["md_pic5"]) || $levelData["md_pic5"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic5"]);
        $this->md_pic6 = !isset($levelData["md_pic6"]) || $levelData["md_pic6"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic6"]);
        $this->md_pic7 = !isset($levelData["md_pic7"]) || $levelData["md_pic7"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic7"]);
        $this->md_pic8 = !isset($levelData["md_pic8"]) || $levelData["md_pic8"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic8"]);
        $this->md_pic9 = !isset($levelData["md_pic9"]) || $levelData["md_pic9"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic9"]);
        $this->md_pic10 = !isset($levelData["md_pic10"]) || $levelData["md_pic10"] == null ? "" : str_replace("paptap-thumbs","paptap",$levelData["md_pic10"]);
        $this->md_date1 = !isset($levelData["md_date1"]) || $levelData["md_date1"] == null ? "" : $levelData["md_date1"];
        $this->md_date2 = !isset($levelData["md_date2"]) || $levelData["md_date2"] == null ? "" : $levelData["md_date2"];
        $this->md_int1 = isset($levelData["md_int1"]) ? $levelData["md_int1"] : 0; 
        $this->md_int2 = isset($levelData["md_int2"]) ? $levelData["md_int2"] : 0; 
        $this->md_int3 = isset($levelData["md_int3"]) ? $levelData["md_int3"] : 0;
        $this->md_bool1 = isset($levelData["md_bool1"]) ? $levelData["md_bool1"] : 0; 
        $this->md_bool2 = isset($levelData["md_bool2"]) ? $levelData["md_bool2"] : 0; 
        $this->md_biz_id_real = isset($levelData["md_biz_id_real"]) ? $levelData["md_biz_id_real"] : 0;
        $this->md_facebook_id = isset($levelData["md_facebook_id"]) ? $levelData["md_facebook_id"] : ""; 
        $this->md_hidden = isset($levelData["md_hidden"]) ? $levelData["md_hidden"] : 0; 
        $this->md_tap_like = isset($levelData["md_tap_like"]) ? $levelData["md_tap_like"] : 0; 
        $this->md_visible_element = isset($levelData["md_visible_element"]) ? $levelData["md_visible_element"] : 0; 
        $this->md_external_id = isset($levelData["md_external_id"]) ? $levelData["md_external_id"] : 0;
        $this->md_external_type = isset($levelData["md_external_type"]) ? $levelData["md_external_type"] : "";
        $this->md_external_sub_id = isset($levelData["md_external_sub_id"]) ? $levelData["md_external_sub_id"] : 0;
        $this->cnt = isset($levelData["cnt"]) && $levelData["cnt"] != null && $levelData["cnt"] != "" ? $levelData["cnt"] : 0;
        $this->md_type=(isset($levelData["me_name"]) && $levelData["me_name"] != null) ? $levelData["me_name"] : "";
        $this->md_link = isset($levelData["link"]) && $levelData["link"] != null ? $levelData["link"] : "";
        $this->md_external_has_action = isset($levelData["md_external_has_action"]) && $levelData["md_external_has_action"] != null ? $levelData["md_external_has_action"] : 0;
        $this->md_external_action_title = isset($levelData["md_external_action_title"]) && $levelData["md_external_action_title"] != null ? $levelData["md_external_action_title"] : "";
        $this->md_deleted = isset($levelData["md_deleted"]) && $levelData["md_deleted"] != null ? $levelData["md_deleted"] : 0;

        $this->biz_level_img["1"]=(isset($levelData["md_pic"]) && $levelData["md_pic"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic) : "";
        $this->biz_level_img["2"]=(isset($levelData["md_pic1"]) && $levelData["md_pic1"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic1) : "";
        $this->biz_level_img["3"]=(isset($levelData["md_pic2"]) && $levelData["md_pic2"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic2) : "";
        $this->biz_level_img["4"]=(isset($levelData["md_pic3"]) && $levelData["md_pic3"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic3) : "";
        $this->biz_level_img["5"]=(isset($levelData["md_pic4"]) && $levelData["md_pic4"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic4) : "";
        $this->biz_level_img["6"]=(isset($levelData["md_pic5"]) && $levelData["md_pic5"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic5) : "";
        $this->biz_level_img["7"]=(isset($levelData["md_pic6"]) && $levelData["md_pic6"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic6) : "";
        $this->biz_level_img["8"]=(isset($levelData["md_pic7"]) && $levelData["md_pic7"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic7) : "";
        $this->biz_level_img["9"]=(isset($levelData["md_pic8"]) && $levelData["md_pic8"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic8) : "";
        $this->biz_level_img["10"]=(isset($levelData["md_pic9"]) && $levelData["md_pic9"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic9) : "";
        $this->biz_level_img["11"]=(isset($levelData["md_pic10"]) && $levelData["md_pic10"] != null) ? str_replace("paptap-thumbs","paptap",$this->md_pic10) : "";

        $nibName = isset($levelData["nib_name"]) ? $levelData["nib_name"] : "";
        $viewType = isset($levelData["ms_view_type$nibName"]) ? $levelData["ms_view_type$nibName"] : "";
        $viewTypeEnd = isset($levelData["ms_view_end$nibName"]) ? $levelData["ms_view_end$nibName"] : "";
        $this->md_next_view = (isset($levelData["md_content"]) && $levelData["md_content"] == "0") ? $viewType : $viewTypeEnd;

        $this->md_review_score = !isset($levelData["md_price"]) || ($levelData["md_price"] == null) ? "0" : $levelData["md_price"];
        $this->currency = $storeRow->data->ess_currency;

        if(isset($levelData["md_element"]) && $levelData["md_element"] == 8 && utilityManager::startsWith($levelData["md_info"],"audio:")){
            $this->md_info = str_replace("audio:","",$levelData["md_info"]);
            $this->md_bool2 = 1;
        }

        if(isset($levelData["md_element"]) && $levelData["md_element"] == 7){
            
            $videoUrl = levelDataManager::generateVideoThumb($levelData["md_info"]);
            if( $videoUrl != ""){
                $this->md_pic = $videoUrl;
            }
        }

        if(isset($levelData["md_element"]) && $levelData["md_element"] == 30){
            $state = ($levelData["md_info1"] != "") ? ", ".$levelData["md_info1"] : "";
            $this->md_info = $levelData["md_info"].$state.", ".$levelData["md_info2"].", ".$levelData["md_info3"]." ".$levelData["md_info4"].", ".$levelData["md_info5"];
        }
    }

    public function fillConnectedProduct($levelDataProduct){
        
        $shopManager = new shopManager();
        $levelDataManager = new levelDataManager(9);
        $itemRow = $shopManager->getItemTypeByModId("9");

        $this->item_type = $itemRow["it_id"];
        $this->md_bool2 = ($itemRow["it_need_shipment"] == null) ? 0 : $itemRow["it_need_shipment"];
        $this->in_sub = 0;

        if(isset($levelDataProduct["customer_server_id"]) && $levelDataProduct["customer_server_id"] != ''){
            $this->in_sub =  customerSubscriptionManager::isItemInCustomerSubscription($levelDataProduct["customer_server_id"],'product',$this->md_row_id) ? "1" : "0";
        }

        $productResponse = $levelDataManager->getLevelDataByID($levelDataProduct["md_external_id"]);
        
        if($productResponse->code == 1 && $productResponse->data->md_row_id > 0){
            $this->md_price = $productResponse->data->md_price;
        }
        
    }

    public function _isValid(){
        return isset($this->md_row_id) && is_numeric($this->md_row_id) && $this->md_row_id > 0;
    }
}
