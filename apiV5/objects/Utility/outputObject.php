<?php

/**
 * outputObject  - the object that is used to return answers from controllers 
 * that need to respond to both Web requests and socket requests
 *
 * @version 1.0
 * @author JonathanM
 */
class outputObject
{
    public $responseCode;
    public $responseMessage = '';
    public $responseData;
    public $version = BOBILE_VERSION;

    public static function withData($code,$message = '',$data = array()){
        $instance= new self();

        $instance->responseCode = $code;
        $instance->responseMessage = $message;
        $instance->responseData = $data;

        return $instance;
    }

    public static function fromResult(resultObject $result){
        $instance= new self();

        $instance->responseCode = $result->code;
        $instance->responseMessage = $result->message;
        $instance->responseData = $result->data;

        return $instance;
    }
}
