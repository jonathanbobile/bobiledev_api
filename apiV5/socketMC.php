<?php
//header('Access-Control-Allow-Origin: *');
/**
 * 
 * A simple, clean and secure PHP Login Script embedded into a small framework.
 * Also available in other versions: one-file, minimal, advanced. See php-login.net for more info.
 *
 * MVC FRAMEWORK VERSION
 *
 * @author Panique
 * @link http://www.php-login.net/
 * @link https://github.com/panique/php-login/
 * @license http://opensource.org/licenses/MIT MIT License
 */

// Load application config (error reporting, database credentials etc.)
require 'MVC/config.php';

// The auto-loader to load the php-login related internal stuff automatically
require 'MVC/autoload.php';

class socketMC
{
    private $app;

    function __construct(){
        $this->app = new Application(false);
    }

    function runProcess($fakeURL,array $data){
        return $this->app->runApplicationWithParams($fakeURL,$data);
    }
}



