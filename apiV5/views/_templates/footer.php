


</div>
</div>
</div>
<div class="pmodal-background"></div>
<div class="pmodal-wrapper">
    <div class="pmodal-content">
        <div class="pmodal-header"><span class="pmodal-title"></span></div>
        <div class="pmodal-body">
        </div>
        <div class="pmodal-footer"></div>
    </div>
</div>
<div id="options-list-orig" style="display: none">
    <div class="options-top">
        <?if($this->controller != 'markets' ){?>
        <div class="row">
            <div class="col-md-12">
                <div class="option-holder optionSelector" data-pt-name="myapps" data-pt-location="account">
                    <div class="option-image">
                        <?include('../admin/img/svg/NOTIFICATIONS_SVG_20_20/myapps.svg');?>
                    </div>
                    <span><?=$this->language->get('My_Apps')?></span>
                </div>
            </div>
        </div>
       <?}?>
        <?if($this->controller == 'markets' ){?>
        <div class="row">
            <div class="col-md-12">
                <div class="option-holder optionSelector" data-pt-name="pcas" data-pt-location="account">
                    <div class="option-image">
                        <?include('../admin/img/svg/NOTIFICATIONS_SVG_20_20/markets.svg');?>
                    </div>
                    <span><?=$this->language->get('my_markets_menu')?></span>
                </div>
            </div>
        </div>
        <?}?>

    </div>
    <div class="options-bottom">
        <div class="secondary-text">
            <?= isset($_SESSION["username"]) ? $_SESSION["username"] : $_SESSION["appData"]["owner_username"]?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="option-holder optionSelector"  data-pt-name="settings" data-pt-location="account">
                    <div class="option-image">
                        <?include('../admin/img/svg/account.svg')?>
                    </div>
                    <span><?=$this->language->get('My_Account')?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="option-holder logOut">
                    <div class="option-image">
                        <?include('../admin/img/svg/logout.svg')?>
                    </div>
                    <span><?=$this->language->get('Log_Out')?></span>
                </div>
            </div>
        </div>        
    </div>

</div>

<div id="mobile-notification" style="display: none">
</div>
<div class="imageActionsFrame hidden-tag">
    <div class="image-element-actions-holder imageActionsHolder">
         <div class="image-element-action addimage-element-action changeElemImage">
             <span class="change-image-text"><?= $this->language->get('change_image')?></span>
             <span class="add-image-text"><?= $this->language->get('add_new_image')?></span>
        </div>
        <div class="image-element-action cropElemImage">
            <img src="/admin/img/photo_manager/crop.png" />
        </div>
        <div class="image-element-action filterElemImage">
            <img src="/admin/img/photo_manager/filters.png" />
        </div>
       <div class="image-element-action deleteElemImage">
            <img src="/admin/img/photo_manager/delete.png" />
        </div>
        <div class="image-element-action undoimage-element-action undoElemImage">
            <img src="/admin/img/photo_manager/undo.png" />
        </div>
    </div>
</div>
<?if(isset($_SESSION['appData']) && !appManager::appPreviewInstalled($_SESSION['appData']['biz_owner_id'])){?>

<div class="mobile-banner" style="display: none;">
    <div class="close-icon">
        <img src="/admin/img/close.png" />
    </div>

    <div class="banner-text">
        Install "App Manager" to see your app on your mobile
    </div>
    <div class="banner-btn">
        <a class="second-btn no-img" href="/qr/?bizid=2">
            <span>Go!</span>
        </a>
    </div>
</div>
<?}?>
</div>
   
</div>



<?php include('stats_scripts.php'); ?>
<?if(!appManager::ourDomain() && (!isset($_SESSION['clientid']) || (isset($_SESSION['clientid']) && $_SESSION['clientid'] == 0)) &&  $this->controller != "wizard"){?>
<?
if(isset($_SESSION["rid"]) && $_SESSION["rid"] != "" && $_SESSION["rid"] != "0"){
?>
<!-- Start of bobilehelp Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed || function (e, t) { var n, o, d, i, s, a = [], r = document.createElement("iframe"); window.zEmbed = function () { a.push(arguments) }, window.zE = window.zE || window.zEmbed, r.src = "javascript:false", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document; try { o = s } catch (e) { n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s } o.open()._l = function () { var e = this.createElement("script"); n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "bobilehelp.zendesk.com", this.zEQueue = a, this.body.appendChild(e) }, o.write('<body onload="document._l();">'), o.close() }();
    /*]]>*/</script>
<!-- End of bobilehelp Zendesk Widget script -->

<!--<script>
    $zopim(function () {
        $zopim.livechat.setOnStatus(function (status) {
            if (status == "offline") {
                $zopim.livechat.badge.show();
                $zopim.livechat.badge.setText('Leave a message!');
            }
        })
    })
</script>-->
<?
}else{
?>
<script>
    window.intercomSettings = {
        app_id: "ceyraqtj"
    };
</script>
<script>
    (function () { var w = window; var ic = w.Intercom; if (typeof ic === "function") { ic('reattach_activator'); ic('update', intercomSettings); } else { var d = document; var i = function () { i.c(arguments) }; i.q = []; i.c = function (args) { i.q.push(args) }; w.Intercom = i; function l() { var s = d.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = 'https://widget.intercom.io/widget/ceyraqtj'; var x = d.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x); } if (w.attachEvent) { w.attachEvent('onload', l); } else { w.addEventListener('load', l, false); } } })()
</script>
<?}
}?>

<?if(!(  isset($_SESSION['rid']) && $_SESSION['rid'] > 0)  ){?>

    <?if(isset($_SESSION['purchase_success']) && $_SESSION['purchase_success'] == 1){
        $_SESSION['purchase_success'] = 0;
          
        //$tag = 'New Purchase';
        //if(isset($_SESSION['free_trial']) && $_SESSION['free_trial'] == 1 ){
        //    $tag = 'Free Trial';
        //    $_SESSION['free_trial'] = 0;
        //}

        if(appManager::ourDomain()){?>


            <!-- Google Code for Purchase Conversion Page OUR WHITE LABEL -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 852697501;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "8YdaCOSSknEQncPMlgM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt="" src="//www.googleadservices.com/pagead/conversion/852697501/?label=8YdaCOSSknEQncPMlgM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>
            <!-- END Google Code for Purchase Conversion Page OUR WHITE LABEL -->


        <?}else{?>

            <!-- Yahoo's Pixel -->
            <img src="https://sp.analytics.yahoo.com/spp.pl?a=10000&.yp=437857" />
            <!-- End of Yahoo's Pixel-->

            <!-- Google Code for bobile purchase Conversion Page 2nd Google Account (BOBILE-NEW) -->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 860360262;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "Ar8ZCOjTwW4QxpygmgM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt="" src="//www.googleadservices.com/pagead/conversion/860360262/?label=Ar8ZCOjTwW4QxpygmgM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>


            <!-- Google Code for bobile Purchase Conversion Page Primary Google Account (BOBILE)-->
            <script type="text/javascript">
                /* <![CDATA[ */
                var google_conversion_id = 987591235;
                var google_conversion_language = "en";
                var google_conversion_format = "3";
                var google_conversion_color = "ffffff";
                var google_conversion_label = "57OOCP3knWwQw-T11gM";
                var google_remarketing_only = false;
                /* ]]> */
            </script>
            <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
                <div style="display: inline;">
                    <img height="1" width="1" style="border-style: none;" alt="" src="//www.googleadservices.com/pagead/conversion/987591235/?label=57OOCP3knWwQw-T11gM&amp;guid=ON&amp;script=0" />
                </div>
            </noscript>
            <!-- END Google Code for bobile Purchase Conversion Page -->


            <!-- Facebook Pixel Code -->
            <script>
                !function (f, b, e, v, n, t, s) {
                    if (f.fbq) return; n = f.fbq = function () {
                        n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                    }; if (!f._fbq) f._fbq = n;
                    n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
                    t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
                }(window,
                document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');

                fbq('init', '1559126987743654');
                fbq('track', "Purchase", { value: 1, currency: 'USD' });
            </script>
            <noscript>
                <img height="1" width="1" style="display: none"
                    src="https://www.facebook.com/tr?id=1559126987743654&ev=PageView&noscript=1" />
            </noscript>
            <!-- End Facebook Pixel Code -->


            <!-- Yotpo Conversion Tracking -->
            <script type="text/javascript">(function e() { var e = document.createElement("script"); e.type = "text/javascript", e.async = true, e.src = "//staticw2.yotpo.com/fq5QE0Xf02fKqS5gOwDaFas7BYLvs9Hu1NpA7KUW/widget.js"; var t = document.getElementsByTagName("script")[0]; t.parentNode.insertBefore(e, t) })();</script>
            <script>            yotpoTrackConversionData = { orderId: "{{order_name|handleize}}", orderAmount: "{{subtotal_price|money_without_currency}}", orderCurrency: "{{shop.currency }}" }</script>
            <noscript>
                <img src="//api.yotpo.com/conversion_tracking.gif?app_key=fq5QE0Xf02fKqS5gOwDaFas7BYLvs9Hu1NpA7KUW&order_id={{order_name|handleize}}&order_amount={{subtotal_price|money_without_currency}}&order_currency={{ shop.currency }}" width="1" height="1"></noscript>
            <!-- End of conversion Yotpo -->

            <!-- Google Tag Manager -->
            <script>
                //dataLayer.push({ 'event': '<?= isset($tag) ? $tag : "";?>' });
                //window.uetq = window.uetq || [];
                //window.uetq.push({ 'ec': 'Conversion', 'ea': 'Purchase', 'el': 'Client', 'ev': '1' });
            </script>          
            <!-- End Google Tag Manager -->

            <!-- Bing pixel-->
            <script>(function (w, d, t, r, u) { var f, n, i; w[u] = w[u] || [], f = function () { var o = { ti: "5566112" }; o.q = w[u], w[u] = new UET(o), w[u].push("pageLoad") }, n = d.createElement(t), n.src = r, n.async = 1, n.onload = n.onreadystatechange = function () { var s = this.readyState; s && s !== "loaded" && s !== "complete" || (f(), n.onload = n.onreadystatechange = null) }, i = d.getElementsByTagName(t)[0], i.parentNode.insertBefore(n, i) })(window, document, "script", "//bat.bing.com/bat.js", "uetq");</script>
            <noscript>
                <img src="//bat.bing.com/action/0?ti=5566112&Ver=2" height="0" width="0" style="display: none; visibility: hidden;" /></noscript>
            <!-- End Bing pixel-->

            <!-- capterra pixel-->
            <script type="text/javascript">
                var capterra_vkey = '338ff285025a6ccd7b82fedb3545aee9',
                capterra_vid = '2111528',
                capterra_prefix = (('https:' == document.location.protocol) ? 'https://ct.capterra.com' : 'http://ct.capterra.com');

                (function () {
                    var ct = document.createElement('script'); ct.type = 'text/javascript'; ct.async = true;
                    ct.src = capterra_prefix + '/capterra_tracker.js?vid=' + capterra_vid + '&vkey=' + capterra_vkey;
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ct, s);
                })();
            </script>
            <!-- End capterra pixel-->

        <?}?>
    <?} //End of Purchase verification ?>
<?} //End of NOT Reseller Verification?>

<?

if(isset($_SESSION['rid']) && $_SESSION['rid'] > 0){ 

    if(isset($_SESSION['reseller_welcome']) && $_SESSION['reseller_welcome'] == 1){
        $_SESSION['reseller_welcome'] = 0;
    ?>
        <script>
            if (typeof dataLayer != 'undefined') {
                dataLayer.push({ 'event': 'Reseller Regis' });
            }
        </script>

        
    <?} //End of welcome verification?>

    <!-- LinkedIn Pixel -->
    <script type="text/javascript">_linkedin_data_partner_id = "88645";</script>
    <script type="text/javascript">
        (function(){var s = document.getElementsByTagName("script")[0];
        var b = document.createElement("script");
        b.type = "text/javascript";b.async = true;
        b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
        s.parentNode.insertBefore(b, s);})();
    </script>
    <noscript>
        <img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=88645&fmt=gif" />
    </noscript>
    <!-- End LinkedIn pixel-->


<?} //End of reseller verification?>

</body>
</html>
