<?php

class Customer extends Controller{

    private $customersModel;
    public $mobileRequestObject = array();
    private $language;

    function __construct($echo_answer = true){
        parent::__construct($echo_answer);
        set_time_limit(0);
        $this->customersModel = new customerModel();
        $this->mobileRequestObject = mobileRequestObject::withData($_REQUEST);
        $lang_code = bizManager::getLanguageCodeFromPrefix($this->mobileRequestObject->lang);
        $this->language = new languageManager($lang_code,$this->mobileRequestObject->bizid);
        $this->language->load('system');
        $this->language->load('customers');
    }

    /************************************* */
    /*   CUSTOMER IDENTITY FUNCTIONS       */
    /************************************* */

    function getClient(){
        try{

            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);

            if($customerResult->code == 1){
                $customerResult->data->cust_last_seen = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                $customerResult->data->cust_last_mobile_serial = $this->mobileRequestObject->deviceID;
                $customerResult->data->cust_last_device = $this->mobileRequestObject->OSType;
                $this->customersModel->updateCustomer($customerResult->data);
                $this->sendPersonalDataAsync($this->mobileRequestObject->server_customer_id,$this->mobileRequestObject->deviceID,$this->mobileRequestObject->OSType);
                eventManager::actionTrigger(enumCustomerActions::enteredApplication,$this->mobileRequestObject->server_customer_id, "visited",'',$this->mobileRequestObject->deviceID);
            }

            return $this->returnAnswer($customerResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setClient(){
        try{

            $this->mobileRequestObject->server_customer_id = 0;//cannot have a customer id before checking by device
            $exisitngCustResult = $this->customersModel->getCusIdByDevice($this->mobileRequestObject,'"guest","tester","preview"');

            $resultData = array();

            if($exisitngCustResult->data > 0){
                $existingCustomerDataResult = $this->customersModel->getCustomerWithID($exisitngCustResult->data);
                if($existingCustomerDataResult->code == 1){
                    $existingCustomer = $existingCustomerDataResult->data;

                    if(isset($existingCustomer->cust_valid_phone) && $existingCustomer->cust_phone1 == '' && $existingCustomer->cust_status != 'member'){
                        //This was the same customer that is now trying to be created. Return the one in the DB
                        $resultData['server_customer_id'] = $exisitngCustResult->data;

                        $result = resultObject::withData(1,'',$resultData);
                        return $this->returnAnswer($result);
                    }
                }
            }

            $clientResult = $this->customersModel->createNewCustomerFromRequest($this->mobileRequestObject,$_REQUEST);

            //Add welcome grant event
            eventManager::actionTrigger(enumCustomerActions::completedOnboarding,$clientResult->data,'complete_on_boarding','',$this->mobileRequestObject->deviceID);
            $custPointsHist = new customerPointsHistoryObject();
            $custPointsHist->cph_biz_id = $this->mobileRequestObject->bizid;
            $custPointsHist->cph_cust_id = $clientResult->data;
            $custPointsHist->cph_source = 'complete_onboarding';
            $custPointsHist->cph_value = bizManager::getBizWelcomeGrant($this->mobileRequestObject->bizid);
            $this->customersModel->addCustomerPointsHistory($custPointsHist);
            $resultData['server_customer_id'] = $clientResult->data;

            bizManager::trackStats($this->mobileRequestObject,"api_new_customer");
            $result = resultObject::withData(1,'',$resultData);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function validatePhoneNumber(){
        try{
            $cCode = isset($_REQUEST['countryCode']) ? str_replace("+","",$_REQUEST['countryCode']) : "" ;
            $phone = "+".utilityManager::getValidPhoneNumber(trim($_REQUEST['cust_phone']),$cCode);

            $name = isset($_REQUEST['cust_name']) ? $_REQUEST['cust_name'] : "";

            $cust_id = $this->mobileRequestObject->server_customer_id;


            $resultData = array();
            $resultData['success'] = 1;
            $result = resultObject::withData(1,'',$resultData);

            //Update code and phone for the current cust_id and send SMS
            $validationCodeResult = $this->customersModel->setCustomerValidationCodeAndPhoneAndName($cust_id,$phone,$name);

            if($validationCodeResult->code == 1){
                $validationCode = $validationCodeResult->data;
                //Send Sms with validation code
                $this->sendValidationCodeSMS($validationCode,$phone,$cust_id);
            }



            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function validateExisitngMemberPhone(){
        try{
            $cCode = isset($_REQUEST['countryCode']) ? str_replace("+","",$_REQUEST['countryCode']) : "" ;
            $phone = utilityManager::getValidPhoneNumber(trim($_REQUEST['phone']),$cCode);
            $existingCustResult = $this->customersModel->getCustIDByValidPhoneNumber($phone,$this->mobileRequestObject);
            $name = isset($_REQUEST['name']) ? $_REQUEST['name'] : "";
            $custID = 0;

            if($existingCustResult->code == 1 && $existingCustResult->data > 0){//customer exists with phone number
                $custID = $existingCustResult->data;

                $result = $this->customersModel->getCustomerWithID($custID);

                $customer = $result->data;

                if($customer->cust_status != 'member'){
                    $result = resultObject::withData(0,"no_customer");
                }
                else{

                    $validationCodeResult = $this->customersModel->setCustomerValidationCodeAndPhoneAndName($custID,"+".$phone,$name);

                    if($validationCodeResult->code == 1){
                        $validationCode = $validationCodeResult->data;
                        //Send Sms with validation code
                        $this->sendValidationCodeSMS($validationCode,"+".$phone,$custID);


                    }
                }
                $data = array();
                $data['cust_id'] = $customer->cust_id;
                $result = resultObject::withData(1,'',$data);
            }
            else{
                $result = resultObject::withData(0,"no_customer");
            }


            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function validateClubMemberByPhone(){
        try{

            $cCode = isset($_REQUEST['countryCode']) ? str_replace("+","",$_REQUEST['countryCode']) : "" ;
            $phone = utilityManager::getValidPhoneNumber(trim($_REQUEST['phone']),$cCode);
            $existingCustResult = $this->customersModel->getCustIDByValidPhoneNumber($phone,$this->mobileRequestObject);

            if($existingCustResult->code == 1 && $existingCustResult->data > 0){
                $data = array();
                $data['cust_id'] = $existingCustResult->data;
                $result = resultObject::withData(1,'',$data);

                //Update code and phone for the current cust_id and send SMS
                $validationCodeResult = $this->customersModel->setCustomerValidationCodeAndPhoneAndName($existingCustResult->data,"+".$phone,$_REQUEST['name']);

                if($validationCodeResult->code == 1){
                    $validationCode = $validationCodeResult->data;
                    //Send Sms with validation code
                    $this->sendValidationCodeSMS($validationCode,$phone,$existingCustResult->data);
                }
            }else{
                $result = resultObject::withData(0,"no_customer");
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function updateCustomer(){
        try{

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);

            if($customerResult->code != 1){
                return $this->returnAnswer($customerResult);
            }

            $custObject = $customerResult->data;
            $custObject->cust_first_name = isset($_REQUEST["cust_first_name"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_first_name"]) : "";
            $custObject->cust_email = isset($_REQUEST["cust_email"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_email"]) : "";
            $custObject->cust_phone2 = isset($_REQUEST["cust_phone2"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_phone2"]) : "";
            $custObject->cust_address = isset($_REQUEST["cust_address"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_address"]) : "";
            $custObject->cust_city = isset($_REQUEST["cust_city"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_city"]) : "";
            $custObject->cust_zip = isset($_REQUEST["cust_zip"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_zip"]) : "";
            $custObject->cust_country = isset($_REQUEST["cust_country"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_country"]) : "";
            $custObject->cust_state = isset($_REQUEST["cust_state"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_state"]) : "";
            $custObject->cust_gender = isset($_REQUEST["cust_gender"]) ? str_replace('(null)' , "" ,$_REQUEST["cust_gender"]) : "";

            $custObject->cust_mobile_serial = $this->mobileRequestObject->deviceID;
            $custObject->cust_appid = $this->mobileRequestObject->appid;
            $custObject->cust_reseller_id = $this->mobileRequestObject->resellerId;
            $custObject->cust_market_id = $this->mobileRequestObject->marketId;

            if($custObject->cust_state == "") $custObject->cust_state = 0;
            if($custObject->cust_country == "") $custObject->cust_country = 0;


            $custObject->cust_birth_date = isset($_REQUEST["cust_birth_date"]) && $_REQUEST["cust_birth_date"] != "" ? $_REQUEST["cust_birth_date"] : $custObject->cust_birth_date;
            $custObject->cust_weding_date = isset($_REQUEST["cust_wedding_date"]) && $_REQUEST["cust_wedding_date"] != "" ? $_REQUEST["cust_wedding_date"] : $custObject->cust_weding_date;



            $custObject->cust_gender = ($custObject->cust_gender == "" || $custObject->cust_gender == 'null') ? "null" : $custObject->cust_gender + 1;

            $name = "";
            if($this->mobileRequestObject->base != "")
            {
                $binary=base64_decode($this->mobileRequestObject->base);

                $image_storage = imageStorage::getStorageAdapter();

                $newFileName = str_replace(".","",microtime(true));
                $name_big = $image_storage->uploadFileData($binary, $this->mobileRequestObject->bizid . '/user_' . $newFileName . '.png', 300, 300);
                $name = str_replace("storage.googleapis.com/paptap","storage.googleapis.com/paptap-thumbs", $name_big);

                $custObject->cust_pic = $name;
                $custObject->cust_big_pic = $name_big;
                $custObject->cust_need_photo_update = 1;
            }

            $customerModel->updateCustomer($custObject);

            if($custObject->cust_phone1 != "" && $custObject->cust_phone1 == $custObject->cust_valid_phone){
                eventManager::actionTrigger(enumCustomerActions::updateProfile,$this->mobileRequestObject->server_customer_id, "personal_details",'',$this->mobileRequestObject->deviceID);
            }

            if($custObject->cust_city == ""){
                customerManager::updateCustomerLocationByCoordinates($this->mobileRequestObject);
            }

            $result = resultObject::withData(1,"ok",$name);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function updateCustomerLocationByCoordinates(){
        customerManager::updateCustomerLocationByCoordinates($this->mobileRequestObject);
    }

    /**
     * Try to get customer server id from device.
     * If there is no customer with provided device the function will create new one
     * @return resultObject
     */
    function getCustomerIDForDeviceOrCreateNewCustomer(){

        try{
            $existingCustomerResult = $this->customersModel->getCusIdByDevice($this->mobileRequestObject);
            if ($existingCustomerResult->code == 1 && $existingCustomerResult->data == ""){ //This means that the search ended succesfully but no customer found

                $custObject = new customerObject();

                //Customer Name
                $cust_first_name = isset($_REQUEST["cust_first_name"]) ? $_REQUEST["cust_first_name"] : "";
                $custName = addslashes(urldecode(str_replace('(null)' , "" ,$cust_first_name)));
                if($custName == ""){
                    $nameResult = $this->customersModel->getGeneratedCustomerName();
                    if($nameResult->code == 1){
                        $custName = $nameResult->data;
                    }
                }

                //Customer Email
                $cust_email = isset($_REQUEST["cust_email"]) ? $_REQUEST["cust_email"] : "";
                $cust_email = addslashes(urldecode(str_replace('(null)' , "" ,$cust_email)));

                //Customer Status
                $custStatus = "guest";
                if($this->mobileRequestObject->appid == "2" || (isset($_REQUEST["is_appetize"]) && $_REQUEST["is_appetize"] == 1)){
                    $custStatus = "preview";
                }else{

                    if($this->mobileRequestObject->marketId > 0){
                        $isTesterResult = $this->customersModel->isAppleOrGoogleTester($this->mobileRequestObject->bizid,$this->mobileRequestObject->OSType);
                        if($isTesterResult->data){
                            $custName = "Tester";
                            $custStatus = "tester";
                        }
                    }
                }

                $custObject->cust_biz_id = $this->mobileRequestObject->bizid;
                $custObject->cust_mobile_serial = $this->mobileRequestObject->deviceID;
                $custObject->cust_first_name = $custName;
                $custObject->cust_email = $cust_email;
                $custObject->cust_device = $this->mobileRequestObject->OSType;
                $custObject->cust_appid = $this->mobileRequestObject->appid;
                $custObject->cust_market_id = $this->mobileRequestObject->marketId;
                $custObject->cust_reseller_id = $this->mobileRequestObject->resellerId;
                $custObject->cust_status = $custStatus;

                return $this->customersModel->addNewCustomer($custObject);

            }else{
                return $existingCustomerResult;
            }
        }catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }

    }

    /**
     * Check if the received code belong to customer and validates it
     * @return outputObject|string
     */
    public function validatePhoneCode(){

        $code = (!isset($_REQUEST['code']) || trim($_REQUEST['code']) == "") ? -1 : trim($_REQUEST['code']);
        $reuseProfile = isset($_REQUEST['reuse_profile']) && $_REQUEST['reuse_profile'] == 1;
        if($code == -1){
            $result =  resultObject::withData(0,"no_validation_code");
            return $this->returnAnswer($result);
        }
        if(isset($this->mobileRequestObject->server_customer_id) && $this->mobileRequestObject->server_customer_id != "" && $this->mobileRequestObject->server_customer_id > 0){
            $response = $this->customersModel->getCustByCodeAndId($this->mobileRequestObject->server_customer_id,$code);

            if($response->code == 1){//code is correct for customer ID

                $currentCustomer = $response->data;

                $bizModel = new bizModel($this->mobileRequestObject->bizid);


                if($currentCustomer->cust_id == 0){

                    $result =  resultObject::withData(0,"wrong_code");
                    return $this->returnAnswer($result);
                }else{
                    $bizResponse = $bizModel->getBiz();
                    if($bizResponse->code == 1){
                        $bizRow = $bizResponse->data;

                        if($bizRow->biz_membership_mode == "exclusive"){
                            if($currentCustomer->cust_active == 0){
                                $result =  resultObject::withData(0,"member_disabled");
                                return $this->returnAnswer($result);
                            }

                            customerManager::startMemberDuration($this->mobileRequestObject->bizid,$this->mobileRequestObject->server_customer_id);
                        }
                    }


                    smsManager::deleteSendCodeSms($currentCustomer->cust_valid_phone);
                    smsManager::deleteSendCodeVoiceCall($currentCustomer->cust_valid_phone,$this->mobileRequestObject->bizid,$this->mobileRequestObject->server_customer_id);

                    $response = $this->customersModel->getExistingCustByPhoneAppMarket($currentCustomer,$this->mobileRequestObject);

                    if($response->code == 1){
                        $existingCustomer = $response->data;
                        if($existingCustomer->cust_id == 0){//No previous customer with phone exists

                            if($currentCustomer->cust_returning == 1){
                                eventManager::actionTrigger(enumCustomerActions::welcomeBack,$currentCustomer->cust_id,'welcome_back','',$this->mobileRequestObject->deviceID);

                                $currentCustomer->cust_phone1 = $currentCustomer->cust_valid_phone;
                                $currentCustomer->cust_validation_date = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                                $currentCustomer->cust_status = 'member';
                                $currentCustomer->cust_returning = 0;
                                $this->customersModel->updateCustomer($currentCustomer);
                            }
                            else{

                                if(isset($_REQUEST['from_welcome']) && $_REQUEST['from_welcome'] == 1){
                                    eventManager::actionTrigger(enumCustomerActions::completedRegistration,$currentCustomer->cust_id,'complete_register_wizard','',$this->mobileRequestObject->deviceID);
                                }
                                else{

                                    if($currentCustomer->cust_phone1 != '' && $currentCustomer->cust_phone1 == $currentCustomer->cust_valid_phone){
                                        eventManager::actionTrigger(enumCustomerActions::lateRegistration,$currentCustomer->cust_id,'late_register','',$this->mobileRequestObject->deviceID);
                                    }
                                }

                                $currentCustomer->cust_phone1 = $currentCustomer->cust_valid_phone;
                                $currentCustomer->cust_validation_date = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                                $currentCustomer->cust_status = 'member';
                                $currentCustomer->cust_returning = 0;
                                $this->customersModel->updateCustomer($currentCustomer);

                                $this->customersModel->givePointsToCustomerForFriendsRegistration($currentCustomer);
                            }

                            $this->customersModel->checkCustomerMembership($currentCustomer,false);
                            $resultData = array();

                            $resultData['server_customer_id'] = $currentCustomer->cust_id;
                            $resultData['phone'] = $currentCustomer->cust_valid_phone;
                            $response = resultObject::withData(1,"",$resultData);
                            return $this->returnAnswer($response);
                        }else{//Previous customer with phone exists


                            if(!$reuseProfile){//The customer does not want to use the existing entry
                                $resultData = array();
                                $resultData['cust_exist'] = 1;
                                $resultData['code'] = $code;
                                return $this->returnAnswer(resultObject::withData(1,'',$resultData));
                            }

                            if($existingCustomer->cust_mobile_serial != "" && !utilityManager::contains($existingCustomer->cust_mobile_serial,'web_')){
                                $custDevice = new customerDeviceObject();
                                $custDevice->cd_biz_id = $this->mobileRequestObject->bizid;
                                $custDevice->cd_cust_id = $existingCustomer->cust_id;
                                $custDevice->cd_device = $existingCustomer->cust_mobile_serial;
                                $custDevice->cd_app_id = $this->mobileRequestObject->appid;
                                $custDevice->cd_device_type = $this->mobileRequestObject->android == 1 ? 'Android' : "IOS";
                                $custDevice->cd_market_id = -1;
                                $response = $this->customersModel->addCustomerDevice($custDevice);
                                if($response->code == 0){
                                    return $this->returnAnswer($response);
                                }

                                $response = $this->customersModel->deleteMultiCustomerRecordsFromTblCust($existingCustomer,$this->mobileRequestObject);
                                if($response->code == 0){
                                    return $this->returnAnswer($response);
                                }
                            }

                            if($this->mobileRequestObject->deviceID != ""  && !utilityManager::contains($existingCustomer->cust_mobile_serial,'web_')){
                                $custDevice = new customerDeviceObject();
                                $custDevice->cd_biz_id = $this->mobileRequestObject->bizid;
                                $custDevice->cd_cust_id = $existingCustomer->cust_id;
                                $custDevice->cd_device = $this->mobileRequestObject->deviceID;
                                $custDevice->cd_app_id = $this->mobileRequestObject->appid;
                                $custDevice->cd_device_type = $this->mobileRequestObject->android == 1 ? 'Android' : "IOS";
                                $custDevice->cd_market_id = -1;
                                $response = $this->customersModel->addCustomerDevice($custDevice);
                                if($response->code == 0){
                                    return $this->returnAnswer($response);
                                }
                            }

                            $existingCustomer->cust_mobile_serial = $this->mobileRequestObject->deviceID;
                            $existingCustomer->cust_validation_date = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                            $existingCustomer->cust_status = 'member';
                            $existingCustomer->cust_returning = 0;
                            $existingCustomer->cust_current_points = $existingCustomer->cust_current_points + $currentCustomer->cust_current_points;
                            $existingCustomer->cust_total_points = $existingCustomer->cust_total_points + $currentCustomer->cust_total_points;

                            //Remove points that customer received for onboarding
                            $existingCustomer->cust_total_points = $existingCustomer->cust_total_points - bizManager::getBizWelcomeGrant($this->mobileRequestObject->bizid);
                             $existingCustomer->cust_current_points = $existingCustomer->cust_current_points - bizManager::getBizWelcomeGrant($this->mobileRequestObject->bizid);

                            $response = $this->customersModel->updateCustomer($existingCustomer);
                            if($response->code == 0){
                                return $this->returnAnswer($response);
                            }

                            $response = $this->customersModel->updateExistingCustomerWithDataFromCurrent($existingCustomer,$currentCustomer);
                            if($response->code == 0){
                                return $this->returnAnswer($response);
                            }

                            $response = $this->customersModel->deleteCustomerByCustId($currentCustomer->cust_id);
                            if($response->code == 0){
                                return $this->returnAnswer($response);
                            }

                            eventManager::actionTrigger(enumCustomerActions::welcomeBack,$existingCustomer->cust_id,'welcome_back','',$this->mobileRequestObject->deviceID);

                            $this->customersModel->checkCustomerMembership($existingCustomer,false);
                            $resultData = array();

                            $resultData['server_customer_id'] = $existingCustomer->cust_id;
                            $resultData['phone'] = $existingCustomer->cust_valid_phone;
                            $response = resultObject::withData(1,"",$resultData);
                            return $this->returnAnswer($response);
                        }
                    }else{
                        return $this->returnAnswer($response);
                    }
                }
            }else{
                return $this->returnAnswer($response);
            }
        }else{
            $result =  resultObject::withData(0,"no_server_customer_id");
            return $this->returnAnswer($result);
        }
    }

    public function getIP(){
        $ipaddress = utilityManager::get_real_IP();


        return $this->returnAnswer(resultObject::withData(1,'',$ipaddress));
    }

    public function setDeviceIP(){
        $ip = utilityManager::get_real_IP();
        $device = $this->mobileRequestObject->deviceID;
        customerManager::setDeviceIP($device,$this->mobileRequestObject->bizid,$ip);
    }

    /************************************* */
    /*   CUSTOMER ORDER FUNCTIONS          */
    /************************************* */

    public function setCustomerOrder(){
        try{

            if(isset($_REQUEST['orderId']) && $_REQUEST['orderId'] > 0){
                return $this->payPaymentRequest($_REQUEST);
            }

            $cust_id = $this->mobileRequestObject->server_customer_id;
            $customerResult = $this->customersModel->getCustomerWithID($cust_id);

            $ipAddress = utilityManager::get_real_IP();

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $customerBilling = new customerBillingManager();

            $response = array();

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }

            //update customer details
            $buyName = addslashes(urldecode($_REQUEST["name"]));

            $customer->cust_first_name = $buyName;
            $customer->cust_email = $_REQUEST['email'];
            if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != ''){
                $customer->cust_phone2 = "+".utilityManager::getValidPhoneNumber($_REQUEST['phone']);
            }

            $this->customersModel->updateCustomer($customer);

            //check validity
            if(!$orderObject->_isValid()){
                return $this->returnAnswer(resultObject::withData(-1));
            }

            //create order
            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1));
            }

            $orderObject = $orderResult->data;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();

            $deferEventTrigger = false;
            foreach ($jarray as $item)
            {
                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustSubscription($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustPunchPass($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_punchpass_usage'));
                        }
                    }



                }

                if($itemObject->tri_item_type == 3 || $itemObject->tri_item_type == 4 || $itemObject->tri_item_type == 5){
                    $deferEventTrigger = true;
                }

                $itemObjectsList[] = $itemObject;
            }


            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount <= 0){
                utilityManager::asyncProcessOrder($orderObject->cto_id);
                if(!$deferEventTrigger){
                    eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$orderObject->cto_id);
                }

                $response['code'] = 1;
                $response["order_id"] = $orderObject->cto_id;
                $response["long_order_id"] = $orderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            //create invoice
             $invoiceResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($orderObject->cto_id,$invoiceID);
            $_REQUEST["ipAddress"] = $ipAddress;
            $transaction = customerTransactionObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $transaction->tr_cust_id = $customer->cust_id;
            $transaction->tr_biz_id = $customer->cust_biz_id;
            $transaction->tr_invoice_id = $invoiceID;
            $transaction->tr_country = bizManager::getBizCountryID($transaction->tr_biz_id);

            if($transaction->tr_paymentMethodType != "Cash"){

                $chargeAttempt = $customerBilling->addDirectSuccesfulCustomerTransaction($transaction);
            }
            else{
                $bizModel = new bizModel($customer->cust_biz_id);
                $bizStoreSettings = $bizModel->getBizStoreSettings()->data;
                $customerBilling->setCustInvoicePendingByInvoiceID($invoiceID);
                if($bizStoreSettings->ess_set_cash_as_paid == 1){
                    $customerBilling->settleInvoiceWithCashTransaction($invoiceID,$transaction);
                }
                $chargeAttempt = resultObject::withData(1);
            }


            if($chargeAttempt->code == 1){
                utilityManager::asyncProcessOrder($orderObject->cto_id);
                if(!$deferEventTrigger){
                    eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$orderObject->cto_id);
                }
                $response['code'] = 1;
                $response["order_id"] = $orderObject->cto_id;
                $response["long_order_id"] = $orderObject->cto_long_order_id;
                $response['charge_code'] = $chargeAttempt->code;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }
            else{
                $response['code'] = 0;
                $response['charge_code'] = $chargeAttempt->code;
                $response['charge_message'] = $chargeAttempt->message;
                return $this->returnAnswer(resultObject::withData(0,'',$response));
            }

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function payPaymentRequest($GET){
        try{
            $cust_id = $this->mobileRequestObject->server_customer_id;
            $customerResult = $this->customersModel->getCustomerWithID($cust_id);
            $ipAddress = utilityManager::get_real_IP();

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $customerBilling = new customerBillingManager();

            $response = array();

            $orderObject = customerOrderObject::fillFromRequest($GET,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_biz_id = $customer->cust_biz_id;

            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }

            //update customer details
            $buyName = addslashes(urldecode($GET["name"]));

            $customer->cust_first_name = $buyName;
            $customer->cust_email = $GET['email'];
            if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != ''){
                $customer->cust_phone2 = "+".utilityManager::getValidPhoneNumber($_REQUEST['phone']);
            }

            $this->customersModel->updateCustomer($customer);

            $orderID = $GET['orderId'];

            $existingOrderResult = customerOrderManager::getCustomerOrderByID($orderID);

            if($existingOrderResult->code != 1){
                //return failure
                return $this->returnAnswer(resultObject::withData(-1));
            }

            $existingOrderObject = $existingOrderResult->data;

            //fill existing order data with the one received
            $existingOrderObject->fillPaymentRequestOrder($orderObject);


            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount == 0){
                $response['code'] = 1;
                $response["order_id"] = $existingOrderObject->cto_id;
                $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($existingOrderObject->cto_id,$invoiceID);

            $GET["ipAddress"] = $ipAddress;
            $transaction = customerTransactionObject::fillFromRequest($GET,$this->mobileRequestObject->deviceID);
            $transaction->tr_cust_id = $customer->cust_id;
            $transaction->tr_biz_id = $customer->cust_biz_id;
            $transaction->tr_invoice_id = $invoiceID;
            $transaction->tr_country = bizManager::getBizCountryID($transaction->tr_biz_id);

            if($transaction->tr_paymentMethodType != "Cash"){

                $chargeAttempt = $customerBilling->addDirectSuccesfulCustomerTransaction($transaction);
            }
            else{
                $bizModel = new bizModel($customer->cust_biz_id);
                $bizStoreSettings = $bizModel->getBizStoreSettings()->data;
                $customerBilling->setCustInvoicePendingByInvoiceID($invoiceID);
                if($bizStoreSettings->ess_set_cash_as_paid == 1){
                    $customerBilling->settleInvoiceWithCashTransaction($invoiceID,$transaction);
                }
                $chargeAttempt = resultObject::withData(1);
            }


            if($chargeAttempt->code == 1){
                customerOrderManager::updateCustomerOrder($existingOrderObject);
                utilityManager::asyncProcessOrder($existingOrderObject->cto_id);
                eventManager::actionTrigger(enumCustomerActions::paidPayment,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$existingOrderObject->cto_id);
                customerManager::sendAsyncCustomerDataUpdateToDevice('requests',$cust_id);
                $response['code'] = 1;
                $response["order_id"] = $existingOrderObject->cto_id;
                $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
                $response['charge_code'] = $chargeAttempt->code;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }
            else{
                $response['code'] = 0;
                $response['charge_code'] = $chargeAttempt->code;
                $response['charge_message'] = $chargeAttempt->message;
                return $this->returnAnswer(resultObject::withData(0,'',$response));
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($GET));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function setCustomerOrderStripe(){
        try{
            if(isset($_REQUEST['orderId']) && $_REQUEST['orderId'] > 0){
                return $this->stripePaymentRequest($_REQUEST);
            }

            $cust_id = $this->mobileRequestObject->server_customer_id;
            $customerResult = $this->customersModel->getCustomerWithID($cust_id);

            $ipAddress = utilityManager::get_real_IP();

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);

            $orderObject->cto_cust_id = $customer->cust_id;
            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }

            //update customer details
            $buyName = addslashes(urldecode($_REQUEST["name"]));

            $customer->cust_first_name = $buyName;
            $customer->cust_email = $_REQUEST['email'];
            if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != ''){
                $customer->cust_phone2 = "+".utilityManager::getValidPhoneNumber($_REQUEST['phone']);
            }

            $this->customersModel->updateCustomer($customer);

            //check validity
            if(!$orderObject->_isValid()){
                return $this->returnAnswer(resultObject::withData(-1,'order_invalid'));
            }

            $customerBilling = new customerBillingManager();
            $customerBilling->setBillingBizID($customer->cust_biz_id);


            if($customer->cust_stripe_cust_id == ''){//need to create customer account in stripe
                $stripeAddResult = $customerBilling->addCustomerToStripe($customer);
                if($stripeAddResult->code == 1){
                    if($customer->cust_stripe_cust_id == ""){
                        $customer->cust_stripe_cust_id = $stripeAddResult->data;

                        $this->customersModel->updateCustomer($customer);
                    }
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,"cant_add_to_stripe"));
                }
            }



            $paymentMethodId = "";
            if(isset($_REQUEST['payment_method_id']) && $_REQUEST['payment_method_id'] != ""){//new payment method - add to DB
                $payMethodResult = $customerBilling->getPaymentMethodDetails($customer,$_REQUEST['payment_method_id']);

                if($payMethodResult->code == 1){
                    $payMethod = $payMethodResult->data;

                    $customerBilling->attachPaymentMethodToCustomer($customer,$payMethod);

                    $paymentMethodId = $_REQUEST['payment_method_id'];

                    customerManager::sendAsyncCustomerDataUpdateToDevice('default_payment',$customer->cust_id);
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$payMethodResult));
                }

            }
            else{
                $defaultPaySourceResult = $customerBilling->getCustomerDefaultPaymentSource($customer->cust_id);

                if($defaultPaySourceResult->code == 1){
                    $paymentMethodId = $defaultPaySourceResult->data->stripe_card_id;
                }
            }

            if($paymentMethodId == "" && $orderObject->amount > 0){
                return $this->returnAnswer(resultObject::withData(-1,$this->language->get('no_pay_source_selected')));
            }

            $response = array();
            $isRecurring = false;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $deferEventTrigger = false;
            $itemObjectsList = array();

            foreach ($jarray as $item)
            {
                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                //Check if subscription or punchpass uses are valid
                if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';

                        $subUseValid = customerSubscriptionManager::checkCustSubscriptionUsageIsValid($usageObject);

                        if($subUseValid->code != 1){
                            return $this->returnAnswer(resultObject::withData(-1,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';


                        $punchUseValid = customerSubscriptionManager::checkCustPunchpassusageIsvalid($usageObject);

                        if($punchUseValid->code != 1){
                            return $this->returnAnswer(resultObject::withData(-1,'no_punchpass_usage'));
                        }
                    }


                }

                if($itemObject->tri_item_type == 3 || $itemObject->tri_item_type == 4 || $itemObject->tri_item_type == 5){
                    $deferEventTrigger = true;
                }

                if($itemObject->tri_item_type == 4){//is a subscription - recurring payment
                    $isRecurring = true;
                }

                $itemObjectsList[] = $itemObject;
            }

            $orderObject->items = $itemObjectsList;




            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount <= 0){
                //create order
                $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

                if($orderResult->code == 0){
                    return $this->returnAnswer(resultObject::withData(-1,''));
                }

                $orderObject = $orderResult->data;

                //connect items to order
                customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

                utilityManager::asyncProcessOrder($orderObject->cto_id);
                if(!$deferEventTrigger){
                    eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$orderObject->cto_id);
                }
                $response['code'] = 1;
                $response["order_id"] = $orderObject->cto_id;
                $response["long_order_id"] = $orderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            if(!isset($_REQUEST['pay_intent_id']) || $_REQUEST['pay_intent_id'] == ""){

                $stripePayIntentResult = $customerBilling->createPaymentIntent($customer,$orderObject,$paymentMethodId);

                if($stripePayIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$stripePayIntentResult));
                }

                $payIntentResult = $customerBilling->getStripePaymentIntent($stripePayIntentResult->data['intentId']);

                if($payIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$payIntentResult));
                }
                $payIntent = $payIntentResult->data;

                if($isRecurring){
                    $customerBilling->updateStripePaymentMethodToRecurring($payIntent->id);
                }

            }
            else{
                $stripeIntentResult = $customerBilling->getStripePaymentIntent($_REQUEST['pay_intent_id']);
                if($stripeIntentResult->code == 1){
                    $payIntent = $stripeIntentResult->data;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'error_pay_intent_id'));
                }
            }



            $stripeConfrimResult = $customerBilling->confirmPaymentIntent($payIntent);

            if($stripeConfrimResult->code == 0){

                $response['code'] = 0;
                $response['charge_code'] = "charge_code";
                $response['charge_message'] = "charge_message";
                $response['pay_intent_id'] = $stripeConfrimResult->data["intentId"];
                $response['client_secret'] = $stripeConfrimResult->data["clientSecret"];
                $response['is_action_required'] = 1;
                return $this->returnAnswer(resultObject::withData(0,'',$response));

            }else if($stripeConfrimResult->code != 1){
                return $this->returnAnswer(resultObject::withData(-1,"Error",$stripeConfrimResult));
            }else{
                $_REQUEST['pay_intent_id'] = $payIntent->id;
            }
            //Paymnet was successful - create DB rows
            //create order

            //Add any subscription or punch pass uses
            foreach ($orderObject->items as $key => $itemObject)
            {
            	if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';

                        $subUse = customerSubscriptionManager::useCustSubscription($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';


                        $subUse = customerSubscriptionManager::useCustPunchPass($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_punchpass_usage'));
                        }
                    }

                    $orderObject->items[$key] = $itemObject;

                }

            }

            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,''));
            }

            $orderObject = $orderResult->data;

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($orderObject->cto_id,$invoiceID);

            //Mark invoice as paid
            $chargCode = $customerBilling->markCustomerInvoicePaidFromPaymentIntent($invoiceID,$payIntent,$this->mobileRequestObject->deviceID);

            //Process order and
            utilityManager::asyncProcessOrder($orderObject->cto_id);


            //End purchase successfully
            if(!$deferEventTrigger){
                eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$orderObject->cto_id);
            }
            $response['code'] = 1;
            $response["order_id"] = $orderObject->cto_id;
            $response["long_order_id"] = $orderObject->cto_long_order_id;
            $response['charge_code'] = $chargCode->code;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(-1,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function stripePaymentRequest($GET){
        try{
            $cust_id = $this->mobileRequestObject->server_customer_id;
            $customerResult = $this->customersModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $customerBilling = new customerBillingManager();
            $customerBilling->setBillingBizID($customer->cust_biz_id);
            $response = array();

            $orderObject = customerOrderObject::fillFromRequest($GET,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_biz_id = $customer->cust_biz_id;

            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }

            //update customer details
            $buyName = addslashes(urldecode($GET["name"]));

            $customer->cust_first_name = $buyName;
            $customer->cust_email = $GET['email'];
            if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != ''){
                $customer->cust_phone2 = "+".utilityManager::getValidPhoneNumber($_REQUEST['phone']);
            }

            $this->customersModel->updateCustomer($customer);

            if($customer->cust_stripe_cust_id == ''){//need to create customer account in stripe
                $stripeAddResult = $customerBilling->addCustomerToStripe($customer);
                if($stripeAddResult->code == 1){
                    if($customer->cust_stripe_cust_id == ""){
                        $customer->cust_stripe_cust_id = $stripeAddResult->data;

                        $this->customersModel->updateCustomer($customer);
                    }
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,"cant_add_to_stripe"));
                }
            }

            $paymentMethodId = "";
            if(isset($_REQUEST['payment_method_id']) && $_REQUEST['payment_method_id'] != ""){//new payment method - add to DB
                $payMethodResult = $customerBilling->getPaymentMethodDetails($customer,$_REQUEST['payment_method_id']);

                if($payMethodResult->code == 1){
                    $payMethod = $payMethodResult->data;

                    $customerBilling->attachPaymentMethodToCustomer($customer,$payMethod);

                    $paymentMethodId = $_REQUEST['payment_method_id'];

                    customerManager::sendAsyncCustomerDataUpdateToDevice('default_payment',$customer->cust_id);
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$payMethodResult));
                }

            }
            else{
                $defaultPaySourceResult = $customerBilling->getCustomerDefaultPaymentSource($customer->cust_id);

                if($defaultPaySourceResult->code == 1){
                    $paymentMethodId = $defaultPaySourceResult->data->stripe_card_id;
                }
            }

            if($paymentMethodId == "" && $orderObject->amount > 0){
                return $this->returnAnswer(resultObject::withData(-1,$this->language->get('no_pay_source_selected')));
            }

            $orderID = $GET['orderId'];

            $existingOrderResult = customerOrderManager::getCustomerOrderByID($orderID);

            if($existingOrderResult->code != 1){
                //return failure
                return $this->returnAnswer(resultObject::withData(-1,'invalid order'));
            }

            $existingOrderObject = $existingOrderResult->data;

            //fill existing order data with the one received
            $existingOrderObject->fillPaymentRequestOrder($orderObject);

            //if is free purchase - then do not attempt to charge and do not create invoice
            if($existingOrderObject->cto_amount == 0){
                $response['code'] = 1;
                $response["order_id"] = $existingOrderObject->cto_id;
                $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            if(!isset($_REQUEST['pay_intent_id']) || $_REQUEST['pay_intent_id'] == ""){

                $stripePayIntentResult = $customerBilling->createPaymentIntent($customer,$existingOrderObject,$paymentMethodId);

                if($stripePayIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$stripePayIntentResult));
                }

                $payIntentResult = $customerBilling->getStripePaymentIntent($stripePayIntentResult->data['intentId']);

                if($payIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$payIntentResult));
                }
                $payIntent = $payIntentResult->data;

            }
            else{
                $stripeIntentResult = $customerBilling->getStripePaymentIntent($_REQUEST['pay_intent_id']);
                if($stripeIntentResult->code == 1){
                    $payIntent = $stripeIntentResult->data;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'error_pay_intent_id'));
                }
            }

            $stripeConfrimResult = $customerBilling->confirmPaymentIntent($payIntent);

            if($stripeConfrimResult->code == 0){

                $response['code'] = 0;
                $response['charge_code'] = "charge_code";
                $response['charge_message'] = "charge_message";
                $response['pay_intent_id'] = $stripeConfrimResult->data["intentId"];
                $response['client_secret'] = $stripeConfrimResult->data["clientSecret"];
                $response['is_action_required'] = 1;
                return $this->returnAnswer(resultObject::withData(0,'',$response));

            }else if($stripeConfrimResult->code != 1){
                return $this->returnAnswer(resultObject::withData(-1,"Error",$stripeConfrimResult));
            }else{
                $_REQUEST['pay_intent_id'] = $payIntent->id;
            }
            //Paymnet was successful

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($existingOrderObject->cto_id,$invoiceID);

            //Mark invoice as paid
            $chargCode = $customerBilling->markCustomerInvoicePaidFromPaymentIntent($invoiceID,$payIntent);

            //Process order and
            customerOrderManager::updateCustomerOrder($existingOrderObject);
            utilityManager::asyncProcessOrder($existingOrderObject->cto_id);


            //End purchase successfully
            eventManager::actionTrigger(enumCustomerActions::paidPayment,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$existingOrderObject->cto_id);
            $response['code'] = 1;
            $response["order_id"] = $existingOrderObject->cto_id;
            $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
            $response['charge_code'] = $chargCode->code;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($GET));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getPaymentRequest(){
        try{
            $payRequestID = $_REQUEST["requestid"];
            $payRequestOrderResult = customerOrderManager::getCustomerOrderByID($payRequestID);

            if($payRequestOrderResult->code == 1){
                $payRequestData = $payRequestOrderResult->data;
                $payRequest = $payRequestData->PersonalZoneAPIArray();
                return $this->returnAnswer(resultObject::withData(1,'',$payRequest));
            }
            else{
                return $this->returnAnswer(resultObject::withData(0));
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getBenefitsForCart(){
        try{
            $outputData = array();
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer',$outputData));
            }
            $customer = $customerResult->data;

            $itemsList = json_decode(stripslashes(str_replace('\n','',$_REQUEST["items"])));

            $itemBenefitsResult = $this->customersModel->getCustomerBenefitsForItems($customer,$itemsList);

            if($itemBenefitsResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'error',$outputData));
            }

            return $this->returnAnswer($itemBenefitsResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************** */
    /*   CUSTOMER COUPON FUNCTIONS          */
    /************************************** */

    public function canClaimCoupon(){
        try{
            $couponID = $_REQUEST['coupon_id'];

            $couponLimit = couponsManager::getCouponLimitations($couponID);
            $couponTotalLimit = couponsManager::getCouponTotalLimitations($couponID);

            if($couponLimit == 0 && $couponTotalLimit == 0){//coupon has no limits
                return $this->returnAnswer(resultObject::withData(1));
            }

            $customerClaimedCount = couponsManager::getClaimedCoupoonsForCustomerByCoupon($couponID,$this->mobileRequestObject->server_customer_id);

            if($couponLimit > 0 && $customerClaimedCount >= $couponLimit){
                return $this->returnAnswer(resultObject::withData(1,'exceeded_limit'));
            }

            $totalCouponClaimedCount = couponsManager::getClaimedCouponsByItem($couponID);

            if($couponTotalLimit > 0 && $totalCouponClaimedCount >= $couponTotalLimit){
                return $this->returnAnswer(resultObject::withData(1,'exceeded_limit'));
            }

            return $this->returnAnswer(resultObject::withData(1));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }


    /************************************* */
    /*   PERSONAL ZONE FUNCTIONS           */
    /************************************* */

    public function getPersonalZone($type = ""){
        try{
            $outputData = array();
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer',$outputData));
            }

            $customer = $customerResult->data;
            $response = array();
            if($type == "" || $type == "meetings"){
                $meetings = array();
                $meetings['featured'] = array();
                if(bizManager::hasPersonalZoneElement('appointments',$this->mobileRequestObject->bizid)){
                    $meetingsListResult = $this->customersModel->getAllCustomersUpcomingAppointments($customer);


                    if($meetingsListResult->code == 1){
                        foreach ($meetingsListResult->data as $meeting)
                        {
                            $entry = $meeting->PersonalZoneAPIArray();
                            $meetings['featured'][] = $entry;
                        }
                    }
                }
                $response["meetings"] = $meetings;
            }

            if($type == "" || $type == "push"){
                $pushes = array();
                $pushes['featured'] = array();
                if(bizManager::hasPersonalZoneElement('notifications',$this->mobileRequestObject->bizid)){


                    $skip = isset($_REQUEST['skip']) ? $_REQUEST['skip'] : 0;
                    $pushesResult = $this->customersModel->getCustomerLimitedPushes($customer,$this->mobileRequestObject->deviceID,$skip);

                    if($pushesResult->code == 1){
                        foreach ($pushesResult->data as $push)
                        {

                            $pushes['featured'][] = $push->PersonalZoneAPIArray();
                        }

                    }
                }

                $response["push"] = $pushes;
            }

            if($type == "" || $type == "orders"){
                $orders = array();
                if(bizManager::hasPersonalZoneElement('orders',$this->mobileRequestObject->bizid)){
                    $skip = 0;
                    $take = 0;
                    if(isset($_REQUEST['skip'])){
                        $skip = $_REQUEST['skip'];
                        $take = 15;
                    }
                    $ordersResult = customerOrderManager::getCustomerPersonalZoneOrders($customer,'eCommerce',$skip,$take);


                    if($ordersResult->code == 1){
                        foreach ($ordersResult->data as $orderObj)
                        {
                            $order = $orderObj->PersonalZoneAPIArray();
                            $orders[] = $order;
                        }
                    }
                }
                $response["orders"] = $orders;
            }

            if($type == "" || $type == "benefits"){
                $benefits = array();
                if(bizManager::hasPersonalZoneElement('benefits',$this->mobileRequestObject->bizid)){
                    $skip = 0;
                    $take = 0;
                    if(isset($_REQUEST['skip'])){
                        $skip = $_REQUEST['skip'];
                        $take = 15;
                    }
                    $benefitsResult = $this->customersModel->getCustomerBenefitsForPersonalZone($customer,$skip,$take);
                    if($benefitsResult->code == 1){
                        foreach ($benefitsResult->data as $benefit)
                        {

                            $benefits[] = $benefit->PersonalZoneAPIArray($this->mobileRequestObject);
                        }
                    }
                }
                $response["benefits"] = $benefits;
            }

            if($type == "" || $type == "requests"){
                $orders = array();
                if(bizManager::hasPersonalZoneElement('requests',$this->mobileRequestObject->bizid)){
                    $skip = 0;
                    $take = 0;
                    if(isset($_REQUEST['skip'])){
                        $skip = $_REQUEST['skip'];
                        $take = 15;
                    }
                    $ordersResult = customerOrderManager::getCustomerPersonalZoneOrders($customer,'paymentRequest',$skip,$take);


                    if($ordersResult->code == 1){
                        foreach ($ordersResult->data as $orderObj)
                        {
                            $order = $orderObj->PersonalZoneAPIArray();
                            $orders[] = $order;
                        }
                    }
                }
                $response["requests"] = $orders;
            }

            if($type == "" || $type == "documents"){
                $documentsList = array();
                if(bizManager::hasPersonalZoneElement('documents',$this->mobileRequestObject->bizid)){
                    $skip = 0;
                    $take = 0;
                    if(isset($_REQUEST['skip'])){
                        $skip = $_REQUEST['skip'];
                        $take = 15;
                    }
                    $documentsResult = $this->customersModel->getCustomerDocuments($customer,$skip,$take);


                    if($documentsResult->code == 1){
                        foreach ($documentsResult->data as $doc)
                        {
                            $documentsList[] = $doc->PersonalZoneAPIArray($this->mobileRequestObject);
                        }

                    }
                }
                $response["documents"] = $documentsList;
            }

            if($type == "" || $type == "wishlist"){
                $skip = 0;
                $take = 0;
                if(isset($_REQUEST['skip'])){
                    $skip = $_REQUEST['skip'];
                    $take = 15;
                }
                $wishlistResult = $this->customersModel->getCustomerWishlist($customer,$skip,$take);
                $wishes = array();
                if($wishlistResult->code == 1){

                    foreach ($wishlistResult->data as $wish)
                    {

                        $wishes[] = $wish->PersonalZoneAPIArray();
                    }
                }

                $response["wishlist"] = $wishes;
            }

            if($type == "" || $type == "membership"){
                $response["membership"] = $customer->membership;
            }

            if($type == "" || $type == "friends"){
                $friends = array();
                if(bizManager::hasPersonalZoneElement('invitefriends',$this->mobileRequestObject->bizid)){
                    $friendsResult = $this->customersModel->getCustomerInvitedFriends($customer);

                    if($friendsResult->code == 1){
                        foreach ($friendsResult->data as $friend)
                        {
                            $friends[] = $friend->getArrayForFriendList();
                        }

                    }
                }
                $response["friends"] = $friends;
            }

            if($type == "" || $type == "customer"){
                $response["customer"] = $customer;
            }

            if($type == "" || $type == "subscriptions"){
                $subscriptions = array();
                if(bizManager::hasPersonalZoneElement('subscriptions',$this->mobileRequestObject->bizid)){
                    $skip = 0;
                    $take = 0;
                    if(isset($_REQUEST['skip'])){
                        $skip = $_REQUEST['skip'];
                        $take = 15;
                    }
                    $subscriptionsResult = $this->customersModel->getCustomerSubscriptions($customer,$skip,$take);

                    if($subscriptionsResult->code == 1){
                        $subscriptions = $subscriptionsResult->data;
                    }
                }
                $response["subscriptions"] = $subscriptions;
            }

            if($type == "" || $type == "punchpasses"){
                $punch_passes = array();
                if(bizManager::hasPersonalZoneElement('punchpass',$this->mobileRequestObject->bizid)){
                    $skip = 0;
                    $take = 0;
                    if(isset($_REQUEST['skip'])){
                        $skip = $_REQUEST['skip'];
                        $take = 15;
                    }
                    $punch_passesResult = $this->customersModel->getCustomerPunchpasses($customer,$skip,$take);

                    if($punch_passesResult->code == 1){
                        $punch_passes = $punch_passesResult->data;
                    }
                }
                $response["punchpasses"] = $punch_passes;
            }

            if($type == "multiuseitems"){
                $response["multiuse"] = array();
                $punch_passesResult = $this->customersModel->getCustomerPunchpasses($customer);
                if($punch_passesResult->code == 1){
                    $response["multiuse"] = array_merge($response["multiuse"],$punch_passesResult->data);
                }
                $subscriptionsResult = $this->customersModel->getCustomerSubscriptions($customer);
                if($subscriptionsResult->code == 1){
                    $response["multiuse"] = array_merge($response["multiuse"],$subscriptionsResult->data);
                }
            }

            if($type == "" || $type == "points"){
                $response["points"] = array();
                $bizModel = new bizModel($this->mobileRequestObject->bizid);
                $popularItemResult = $bizModel->getBizPopularPointShopItem();
                if($popularItemResult->code == 1){
                    $response["points"]["popular_item"] = $popularItemResult->data->getAPIFormattedArray();
                }
                else{
                    $response["points"]["popular_item"] = null;
                }


                $response["points"]["friends"] = array();

                $friendsResult = $this->customersModel->getCustomerInvitedFriends($customer);

                if($friendsResult->code == 1){
                    foreach ($friendsResult->data as $friend)
                    {
                        $response["points"]["friends"][] = $friend->getArrayForFriendList();
                    }

                }

                $entriesResult = $this->customersModel->getPointsHistoryForCustomer($customer,0,10);

                $response["points"]["history"] = array();

                if($entriesResult->code == 1){
                    foreach ($entriesResult->data as $entry)
                    {
                        $response["points"]["history"][] = $entry->getAPIFormattedArray();
                    }
                }
            }

            $result = resultObject::withData(1,'',$response);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }



    }

    public function getIsRedeemed(){
        try{
            $redeemcode = $_REQUEST["redeemcode"];

            $benefitResult = customerManager::getBenefitByCode($redeemcode);
            if($benefitResult->code == 1){
                $benefit = $benefitResult->data;
                if($benefit->cb_redeemed == 1 && $benefit->cb_redeemed_date != null){
                    $redeemDate = strtotime($benefit->cb_redeemed_date);
                    return $this->returnAnswer(resultObject::withData(1,'',$redeemDate));
                }
                else{
                    return $this->returnAnswer(resultObject::withData(1,'',''));
                }
            }

            return $this->returnAnswer(resultObject::withData(0));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    public function sendFile(){
        $result = customerManager::sendFile($_REQUEST,$this->mobileRequestObject);
        return $this->returnAnswer($result);
    }

    public function getWidgetData(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }
            $customer = $customerResult->data;

            $response = array();
            $defaultSubResult = customerSubscriptionManager::getCustomerPrimaryMultiuseEntry($this->mobileRequestObject->server_customer_id);

            if($defaultSubResult->code == 1){
                $response['data'] = $defaultSubResult->data;
                $response['code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            $response['data'] = $customer->membership;
            $response['code'] = 2;
            return $this->returnAnswer(resultObject::withData(1,'',$response));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getClaimedCouponData(){
        try{
            $benefitID = $_REQUEST['card_id'];

            $benefitResult = customerManager::getCouponDataFromBenefitByBenefitID($benefitID);

            return $this->returnAnswer($benefitResult);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*   REVIEWS FUNCTIONS                 */
    /************************************* */

    public function setReview(){
        try{
            $hasConnectedMod = false;
            if(isset($_REQUEST["cr_item_type"]) && $_REQUEST["cr_item_type"] != ''){
                if($_REQUEST["cr_item_type"] == 'product'){
                    $hasConnectedMod = bizManager::hasPremiumFeature(9,$this->mobileRequestObject->bizid) && bizManager::modulesAreActive(9,$this->mobileRequestObject->bizid);
                }
                else{
                    $hasConnectedMod = bizManager::hasPremiumFeature(17,$this->mobileRequestObject->bizid) && bizManager::modulesAreActive(17,$this->mobileRequestObject->bizid);
                }
            }

            $customerReview = new customerReviewObject();

            $customerReview->cr_item_type = $hasConnectedMod ? $_REQUEST["cr_item_type"] : '';
            $customerReview->cr_item_id = $hasConnectedMod ? $_REQUEST["cr_item_id"] : 0;
            $customerReview->cr_rating = $_REQUEST["cr_rating"];
            $customerReview->cr_text = $_REQUEST["cr_text"];
            $customerReview->cr_mod_id = $_REQUEST["cr_mod_id"];
            $customerReview->cr_row_id = $_REQUEST["cr_row_id"];
            $customerReview->cr_cust_id = $this->mobileRequestObject->server_customer_id;
            $customerReview->cr_biz_id = $this->mobileRequestObject->bizid;


            $this->customersModel->addCustomerReview($customerReview);

            $result = bizManager::updateRowReview($customerReview);
            bizManager::needUpdateApp($this->mobileRequestObject->bizid);

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getSingleReview(){
        try{
            $review = new customerReviewObject();

            $review->cr_item_type = $_REQUEST["cr_item_type"];
            $review->cr_item_id = $_REQUEST["cr_item_id"];
            $review->cr_mod_id = $_REQUEST["cr_mod_id"];
            $review->cr_row_id = $_REQUEST["cr_row_id"];
            $review->cr_biz_id = $this->mobileRequestObject->bizid;
            $review->cr_cust_id = $this->mobileRequestObject->server_customer_id;



            $reviewResult = $this->customersModel->getCustomerExistingReview($review);

            if($reviewResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0));
            }

            return $this->returnAnswer(resultObject::withData(1,'',$reviewResult->data));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*   MEETINGS FUNCTIONS               */
    /************************************* */

    public function setMeeting(){
        try{
            date_default_timezone_set("UTC");

            $meeting = new employeeMeetingObject();

            $meeting->em_emp_id = $_REQUEST["emp_id"];
            $meeting->em_meet_type = $_REQUEST["meet_type"];
            $meeting->em_name = addslashes($_REQUEST["meet_name"]);
            $start_time_stamp = $_REQUEST["start_time"];
            $end_time_stamp = $_REQUEST["end_time"];
            $meeting->em_cust_request = addslashes($_REQUEST["custom_text"]);
            $meeting->em_payment_source = isset($_REQUEST['payment_source']) && $_REQUEST['payment_source'] != '' ? $_REQUEST['payment_source'] : 'none';
            $meeting->em_payment_source_id = isset($_REQUEST['payment_source_id']) && $_REQUEST['payment_source_id'] != '' ? $_REQUEST['payment_source_id'] : 0;
            $meeting->em_biz_id = $this->mobileRequestObject->bizid;
            $meeting->em_from_mobile = 1;
            $meeting->em_cust_device_id = $this->mobileRequestObject->deviceID;
            $meeting->em_cust_id = $this->mobileRequestObject->server_customer_id;

            $customerResult = $this->customersModel->getCustomerWithID($meeting->em_cust_id);



            $responce = new stdClass();


            if ($this->mobileRequestObject->bizid == "0"){
                $responce->result["code"] = "1";
                $responce->result["value"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No bizid";
                return $this->returnAnswer(resultObject::withData(1,'',$responce));
            }

            if ($meeting->em_emp_id == "0"){
                $responce->result["code"] = "2";
                $responce->result["value"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Emplyee";
                return $this->returnAnswer(resultObject::withData(1,'',$responce));
            }

            if ($meeting->em_meet_type == "0"){
                $responce->result["code"] = "3";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Meeting Type";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_name == ""){
                $responce->result["code"] = "4";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Meeting Name";
                return $this->returnAnswer(resultObject::withData(1,'',$responce));
            }

            if ($start_time_stamp == "" || $start_time_stamp == "(null)" || $start_time_stamp == "0"){
                $responce->result["code"] = "5";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Start Time";
                return $this->returnAnswer(resultObject::withData(1,'',$responce));
            }

            if ($end_time_stamp == "" || $end_time_stamp == "(null)" || $end_time_stamp == "0"){
                $responce->result["code"] = "6";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No End Time";
                return $this->returnAnswer(resultObject::withData(1,'',$responce));
            }

            if ($customerResult->code == 0){
                $responce->result["code"] = "7";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Customer ID";
                return $this->returnAnswer(resultObject::withData(1,'',$responce));
            }

            $result = bookingManager::setMeeting($meeting,$customerResult->data,$start_time_stamp,$end_time_stamp);

            if($result->code == 1){
                bizManager::addBizOfferingUsage($this->mobileRequestObject->bizid,'appointment',$meeting->em_meet_type);
                customerManager::addBadgeToCustomer($customerResult->data->cust_id,enumBadges::appointments);
                customerManager::sendAsyncCustomerDataUpdateToDevice('meetings',$customerResult->data->cust_id);
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    /************************************* */
    /*   FAVORITES FUNCTIONS               */
    /************************************* */

    public function setFavorites(){

        $customerManager = new customerManager();
        $bizModel = new bizModel($this->mobileRequestObject->bizid);

        $userData = customerManager::getUserByDeviceAppMarket($this->mobileRequestObject->deviceID,$this->mobileRequestObject->appid,$this->mobileRequestObject->marketId);
        if(is_array($userData)){
            $userObject = userObject::withData($userData);
        }else{
            $userObject = new userObject();
            $userObject->usr_nick_name='_customer_';
            $userObject->usr_country_id=$this->mobileRequestObject->country;
            $userObject->usr_state_id=$this->mobileRequestObject->state;
            $userObject->usr_default_lng=$this->mobileRequestObject->lang;
            $userObject->usr_mobile_num=$this->mobileRequestObject->phoneNumber;
            $userObject->usr_mobile_serial=$this->mobileRequestObject->deviceID;
            $userObject->usr_appid=$this->mobileRequestObject->appid;
            $userObject->usr_resellerid=$this->mobileRequestObject->resellerId;
            $userObject->usr_marketid=$this->mobileRequestObject->marketId;

            $userResult = $customerManager->addUser($userObject);
            if($userResult->code == 1){
                $userObject->usr_id = $userResult->data;
            }
        }

        if(bizManager::isUserExistForBiz($userObject,$this->mobileRequestObject->bizid)){
            bizManager::clearNeedUpdateAppForUser($userObject,$this->mobileRequestObject->bizid);
        }else{
            bizManager::addUser($userObject,$this->mobileRequestObject->bizid);

            $bizResponse =   $bizModel->getBiz();
            if($bizResponse->code == 1){
                if ($this->appid > 0 && ($bizResponse->data->biz_appl_status == 2 || $bizResponse->data->biz_goog_status == 2)){
                    $notifications = new notificationsManager();
                    $notifications->addNotification($this->mobileRequestObject->bizid,enumActions::newInstallation);
                }
            }
        }
        bizManager::trackStats($this->mobileRequestObject,"api_set_favorites");
        return $this->returnAnswer(resultObject::withData(1,'ok'));
    }

    public function unsetFavorites(){

        $userData = customerManager::getUserByDeviceAppMarket($this->mobileRequestObject->deviceID,$this->mobileRequestObject->appid,$this->mobileRequestObject->marketId);
        $userObject = userObject::withData($userData);
        bizManager::deleteUser($userObject,$this->mobileRequestObject->bizid);
        bizManager::trackStats($this->mobileRequestObject,"api_unset_favorites");
        return $this->returnAnswer(resultObject::withData(1,'ok'));
    }

    public function setWishList(){

        $customerManager = new customerManager();

        $rowId = $_REQUEST["row_id"];
        $modId = $_REQUEST["mod_id"];
        $levelId = $_REQUEST["level"];

        $wishListObject = new wishListObject();
        $wishListObject->wl_biz_id = $this->mobileRequestObject->bizid;
        $wishListObject->wl_customer_id = $this->mobileRequestObject->server_customer_id;
        $wishListObject->wl_mod_id = $modId;
        $wishListObject->wl_row_id = $rowId;
        $wishListObject->wl_level_id = $levelId;

        $responseWish = $customerManager->addWishList($wishListObject);

        if($responseWish->code != 1){
            return $this->returnAnswer($responseWish);
        }

        eventManager::actionTrigger(enumCustomerActions::addedToFavorites,$this->mobileRequestObject->server_customer_id,'added_favorite','',$this->mobileRequestObject->deviceID);


        customerManager::sendAsyncCustomerDataUpdateToDevice('wishlist',$this->mobileRequestObject->server_customer_id);
        $responseObject = $customerManager::getFullWishList($wishListObject);
        return $this->returnAnswer($responseObject);
    }

    public function unsetWishList(){

        $customerManager = new customerManager();

        $rowId = $_REQUEST["row_id"];
        $modId = $_REQUEST["mod_id"];

        $wishListData = customerManager::getWishListForElement($this->mobileRequestObject->bizid,$this->mobileRequestObject->server_customer_id,$modId,$rowId);
        if(is_array($wishListData)){
            $wishListObject = wishListObject::withData($wishListData);
            $deleteResult = $customerManager->deleteWishListById($wishListObject->wl_id);
            customerManager::sendAsyncCustomerDataUpdateToDevice('wishlist',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer($deleteResult);
        }

        return $this->returnAnswer(resultObject::withData(1,'not_found'));
    }

    /************************************* */
    /*   CLASSES FUNCTIONS                 */
    /************************************* */

    public function checkClassDate(){
        try{
            $classID = $_REQUEST["classId"];
            $dateID = $_REQUEST["dateId"];
            $custID = $this->mobileRequestObject->server_customer_id;

            $classResult = bookingManager::getClassByID($classID);

            if($classResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_class'));
            }

            $class = $classResult->data;

            $classDateResult = bookingManager::getClassDateByID($dateID);

            if($classDateResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_class_date'));
            }

            $classDate = $classDateResult->data;

            $existsInDate = bookingManager::isCustomerInClassDate($custID,$classDate);

            if($existsInDate){
                return $this->returnAnswer(resultObject::withData(0,'already_exist'));
            }

            if($classDate->booked_count >= $class->calc_max_cust){
                return $this->returnAnswer(resultObject::withData(0,'no_place'));
            }

            return $this->returnAnswer(resultObject::withData(1));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function setClassDate(){
        try{
            $classID = $_REQUEST["classId"];
            $dateID = $_REQUEST["dateId"];
            $custID = $this->mobileRequestObject->server_customer_id;

            $customerResult = $this->customersModel->getCustomerWithID($custID);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $classResult = bookingManager::getClassByID($classID);

            if($classResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_class'));
            }

            $class = $classResult->data;

            $classDateResult = bookingManager::getClassDateByID($dateID);

            if($classDateResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_class_date'));
            }

            $classDate = $classDateResult->data;

            $existsInDate = bookingManager::isCustomerInClassDate($custID,$classDate);

            if($existsInDate){
                return $this->returnAnswer(resultObject::withData(0,'already_exist'));
            }

            if($classDate->booked_count >= $class->calc_max_cust){
                return $this->returnAnswer(resultObject::withData(0,'no_place'));
            }

            $customerDate = new classDateCustomerObject();
            $customerDate->cdc_biz_id = $classDate->ccd_biz_id;
            $customerDate->cdc_class_id = $classID;
            $customerDate->cdc_class_date_id = $dateID;
            $customerDate->cdc_customer_id = $custID;
            $customerDate->cdc_payment_source = isset($_REQUEST['payment_source']) && $_REQUEST['payment_source'] != '' ? $_REQUEST['payment_source'] : 'none';
            $customerDate->cdc_payment_source_id = isset($_REQUEST['payment_source_id']) && $_REQUEST['payment_source_id'] != '' ? $_REQUEST['payment_source_id'] : 0;
            $customerDate->cdc_comments = $_REQUEST["comments"];



            $result = bookingManager::signUpCustomerToClassdate($customer,$customerDate);

            if($result->code == 1){
                bizManager::addBizOfferingUsage($this->mobileRequestObject->bizid,'class',$classID);
                eventManager::actionTrigger(enumCustomerActions::classsignup,$customer->cust_id,"class",'',$this->mobileRequestObject->deviceID,$dateID);
                customerManager::addBadgeToCustomer($customerResult->data->cust_id,enumBadges::appointments);
                bizManager::needUpdateApp($this->mobileRequestObject->bizid);
            }
            customerManager::sendAsyncCustomerDataUpdateToDevice('meetings',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*   CHAT FUNCTIONS                    */
    /************************************* */

    public function sendChat(){

        $message = $this->mobileRequestObject->OS == 'Android' ?  urldecode($_REQUEST["message"]) : $_REQUEST["message"];

        $chatManager = new chatManager();
        $chatManager->mobileRequestObject = $this->mobileRequestObject;
        $customerModel = new customerModel();
        $chatMessageObject = new chatMessageObject();

        $resultCustomer = $customerModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
        if($resultCustomer->code != 1){
            return $this->returnAnswer(resultObject::withData(0,'no_customer'));
        }

        $chatMessageObject->ch_user = $this->mobileRequestObject->bizid;
        $chatMessageObject->ch_customer = $this->mobileRequestObject->server_customer_id;
        $chatMessageObject->ch_message = $message;
        $chatMessageObject->ch_dir = 0;
        $chatMessageObject->ch_new = 1;

        $resultChat = $chatManager->sendChatFromClient($chatMessageObject);

        if($resultChat->code == 1){
            eventManager::actionTrigger(enumCustomerActions::sentChat,$this->mobileRequestObject->server_customer_id,'sent_chat','',$this->mobileRequestObject->deviceID);
        }

        $chatMessages = $chatManager->getLatestMessagesForCustomer($this->mobileRequestObject->server_customer_id);
        return $this->returnAnswer($chatMessages);

    }

    public function syncChat(){
        try{
            $chatManager = new chatManager();
            $customerModel = new customerModel();

            $lastID = isset($_REQUEST['lastid']) ? $_REQUEST['lastid'] : 0;

            $resultCustomer = $customerModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($resultCustomer->code == 1){
                $resultCustomer->data->cust_last_seen = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                $customerModel->updateCustomer($resultCustomer->data);
            }

            if(isset($_REQUEST['from_chat']) && $_REQUEST['from_chat'] == 1){
                chatManager::setChatMessagesAsReedByCustomer($this->mobileRequestObject->server_customer_id);
            }

            $chatManager->mobileRequestObject = $this->mobileRequestObject;

            $chatMessages = $chatManager->getLatestMessagesForCustomer($this->mobileRequestObject->server_customer_id,$lastID);
            return $this->returnAnswer($chatMessages);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*   CUSTOM FORM FUNCTIONS             */
    /************************************* */

    public function getFieldsList(){

        if(!isset($_REQUEST["formid"]) || $_REQUEST["formid"] == ""){
            return $this->returnAnswer(resultObject::withData(0,"no_form_id"));
        }
        if(isset($_REQUEST["formDataId"]) && trim($_REQUEST["formDataId"]) === ""){
            return $this->returnAnswer(resultObject::withData(0,"no_form_data_id"));
        }

        $formDataId = isset($_REQUEST["formDataId"]) ? $_REQUEST["formDataId"] : 0;

        $customFormManager = new customFormManager();
        $response = $customFormManager->getFormFields($_REQUEST["formid"],$formDataId);

        return $this->returnAnswer(resultObject::withData(1,"ok",$response));
    }

    public function saveCustomForm(){

        $customFormManager = new customFormManager();
        $customFormManager->mobileRequestObject = $this->mobileRequestObject;

        $result = $customFormManager->saveUserFormData($_REQUEST);
        customerManager::addOneUnseenFileForCust($this->mobileRequestObject->server_customer_id);
       
        eventManager::actionTrigger(enumCustomerActions::filledForm,$this->mobileRequestObject->server_customer_id, "custumform",'',$this->mobileRequestObject->deviceID);
        customerManager::sendAsyncCustomerDataUpdateToDevice('documents',$this->mobileRequestObject->server_customer_id);
        return $this->returnAnswer($result);
    }

    /************************************* */
    /*   CUSTOMER SUBSCRIPTION FUNCTIONS   */
    /************************************* */

    public function getSubscriptionToUseForService(){
        try{
            $service_id = $_REQUEST['item_id'];

            $subUseResult = customerSubscriptionManager::getFirstActiveCustomerSubscriptionsIDForItem($this->mobileRequestObject->server_customer_id,'service',$service_id);

            if($subUseResult->code == 1){
                return $this->returnAnswer(resultObject::withData(1,'',$subUseResult->data));
            }

            return $this->returnAnswer(resultObject::withData(0));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getSubscriptionToUseForClass(){
        try{
            $class_id = $_REQUEST['item_id'];

            $subUseResult = customerSubscriptionManager::getFirstActiveCustomerSubscriptionsIDForItem($this->mobileRequestObject->server_customer_id,'class',$class_id);

            if($subUseResult->code == 1){
                return $this->returnAnswer(resultObject::withData(1,'',$subUseResult->data));
            }

            return $this->returnAnswer(resultObject::withData(0));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getSubscriptionToUseForProduct(){
        try{
            $product_id = $_REQUEST['item_id'];

            $subUseResult = customerSubscriptionManager::getFirstActiveCustomerSubscriptionsIDForItem($this->mobileRequestObject->server_customer_id,'product',$product_id);

            if($subUseResult->code == 1){
                return $this->returnAnswer(resultObject::withData(1,'',$subUseResult->data));
            }

            return $this->returnAnswer(resultObject::withData(0));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function useCustomerSubscriptionForProduct(){
        try{
            $csub_id = $_REQUEST['csub_id'];
            $product_id = $_REQUEST['product_id'];

            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $usageObj = new customerSubscriptionUsageObject();

            $usageObj->csuse_item_id = $product_id;
            $usageObj->csuse_item_type = 'product';
            $usageObj->csuse_csu_id = $csub_id;
            $usageObj->csuse_biz_id = $this->mobileRequestObject->bizid;
            $usageObj->csuse_cust_id = $this->mobileRequestObject->server_customer_id;

            $usageResult = customerSubscriptionManager::useCustSubscription($usageObj);

            if($usageResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'error_using_subscrption'));
            }

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);

            if(!$orderObject->_isValid()){
               return $this->returnAnswer(resultObject::withData(-1));
            }

            //create order
            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($customer->cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1));
            }

            $orderObject = $orderResult->data;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();
            foreach ($jarray as $item)
            {
                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                $itemObjectsList[] = $itemObject;
            }

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            utilityManager::asyncProcessOrder($orderObject->cto_id);

            eventManager::actionTrigger(enumCustomerActions::usedSubscription,$customer->cust_id, "used_subscription",'',$this->mobileRequestObject->deviceID,$csub_id);
            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$this->mobileRequestObject->server_customer_id);

            $response['code'] = 1;
            $response["order_id"] = $orderObject->cto_id;
            $response["long_order_id"] = $orderObject->cto_long_order_id;
            $response['charge_code'] = 1;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function useCustomerPunchPassForProduct(){
        try{
            $cpassb_id = $_REQUEST['cpass_id'];
            $product_id = $_REQUEST['product_id'];

            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $usageObj = new customerPunchUsageObject();

            $usageObj->cpuse_item_id = $product_id;
            $usageObj->cpuse_item_type = 'product';
            $usageObj->cpuse_cpp_id = $cpassb_id;
            $usageObj->cpuse_biz_id = $this->mobileRequestObject->bizid;
            $usageObj->cpuse_cust_id = $this->mobileRequestObject->server_customer_id;

            $usageResult = customerSubscriptionManager::useCustPunchPass($usageObj);

            if($usageResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'error_using_punchpass'));
            }

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);

            if(!$orderObject->_isValid()){
                return $this->returnAnswer(resultObject::withData(-1));
            }

            //create order
            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($customer->cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1));
            }

            $orderObject = $orderResult->data;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();
            foreach ($jarray as $item)
            {
                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                $itemObjectsList[] = $itemObject;
            }

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            utilityManager::asyncProcessOrder($orderObject->cto_id);

            eventManager::actionTrigger(enumCustomerActions::usedPunchPass,$customer->cust_id, "used_punch_pass",'',$this->mobileRequestObject->deviceID,$cpassb_id);
            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$this->mobileRequestObject->server_customer_id);
            $response['code'] = 1;
            $response["order_id"] = $orderObject->cto_id;
            $response["long_order_id"] = $orderObject->cto_long_order_id;
            $response['charge_code'] = 1;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function setPrimaryCustomerSubscription(){
        try{
            $csub_id = $_REQUEST['cust_sub_id'];

            $result = customerSubscriptionManager::setCustomerSubscriptionAsPrimary($this->mobileRequestObject->server_customer_id,$csub_id);
            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$this->mobileRequestObject->server_customer_id,$this->mobileRequestObject->deviceID);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function setPrimaryCustomerPunchpass(){
        try{
            $cpass_id = $_REQUEST['cust_pass_id'];

            $result = customerSubscriptionManager::setCustomerPunchPassAsPrimary($this->mobileRequestObject->server_customer_id,$cpass_id);
            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$this->mobileRequestObject->server_customer_id,$this->mobileRequestObject->deviceID);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function cancelSubscription(){
        try{
            $csub_id = $_REQUEST['cust_sub_id'];
            $note = $_REQUEST['cancellation_notes'];
            $response = array();

            $custID = $this->mobileRequestObject->server_customer_id;

            $customerResult = $this->customersModel->getCustomerWithID($custID);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $custSubResult = customerSubscriptionManager::getCustomerSubscriptionByCustSubID($csub_id);

            if($custSubResult->code == 0){
                $response['code'] = 0;
                $this->returnAnswer(resultObject::withData(0,'no_subscription',$response));
            }

            $custSub = $custSubResult->data;

            $custSub->csu_status = 'user_canceled';
            $custSub->csu_is_primary = 0;

            $updateResult = customerSubscriptionManager::updateCustomerSubsciption($custSub);

            if($updateResult->code == 1){

                $usageObj = new customerSubscriptionUsageObject();

                $usageObj->csuse_cust_id = $custID;
                $usageObj->csuse_biz_id = $this->mobileRequestObject->bizid;
                $usageObj->csuse_csu_id = $csub_id;
                $usageObj->csuse_status_note = $note;
                $usageObj->csuse_type = 'user_cancel';

                customerSubscriptionManager::addCustomerSubscriptionUsage($usageObj);

                $params = array();
                $params['sub_name'] = $custSub->subscription->md_head;
                emailManager::sendSystemMailCustomer($custID,242,emailManager::getDefaulProvider(),$params);


                $params['cust_first_name'] = $customer->cust_first_name;
                emailManager::sendSystemMailOwner(bizManager::getOwnerIdByBizId($this->mobileRequestObject->bizid),243,emailManager::getDefaulProvider(),$params);

                $message = emailManager::getNotificationMessageFromBiz(34,$this->mobileRequestObject->bizid,$params);
                pushManager::sendPushAdmin($this->mobileRequestObject->bizid,enumPushType::admin_messageOnly,$message,$params);
            }
            else{
                $response['code'] = 0;
                $this->returnAnswer(resultObject::withData(0,'could_not_cancel',$response));
            }
            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$this->mobileRequestObject->server_customer_id);
            $response['code'] = 1;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getCustomerPrimaryMultiuse(){
        try{
            $primaryResult = customerSubscriptionManager::getCustomerPrimaryMultiuseEntry($this->mobileRequestObject->server_customer_id);

            return $this->returnAnswer($primaryResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function chekIteminCustomerMultiuse(){
        try{
            $type = $_REQUEST['item_type'];
            $item_id = $_REQUEST['item_id'];

            $itemInSub = customerSubscriptionManager::isItemInCustomerSubscription($this->mobileRequestObject->server_customer_id,$type,$item_id);

            $code = $itemInSub ? 1 : 0;

            return $this->returnAnswer(resultObject::withData($code));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function validateMultiuseCartCheckout(){
        try{
            if(!isset($_REQUEST['subs'])){
                return $this->returnAnswer(resultObject::withData(0,'no_subs'));
            }
            $multiUsed = $_REQUEST['subs'];

            $usedSubs = array();
            $usedPPass = array();

            foreach (json_decode($multiUsed) as $used)
            {
                if($used->type == 'subscription'){
                    if(isset($usedSubs[$used->id])){
                        $usedSubs[$used->id] = $usedSubs[$used->id] + 1;
                    }
                    else{
                        $usedSubs[$used->id] = 1;
                    }
                }
                else{
                    if(isset($usedPPass[$used->id])){
                        $usedPPass[$used->id] = $usedPPass[$used->id] + 1;
                    }
                    else{
                        $usedPPass[$used->id] = 1;
                    }
                }
            }

            $result = array();
            $result['code'] = 1;
            $result['errors'] = array();
            foreach ($usedSubs as $key => $quantity)
            {
                $status = customerSubscriptionManager::checkCustSubscriptionCartValidity($key,$quantity);
                if($status->code == 0){
                    $result['errors'][] = $status->data;
                    $result['code'] = 0;
                }
            }


            foreach ($usedPPass as $key => $quantity)
            {
                $status = customerSubscriptionManager::checkCustPunchpassCartValidity($key,$quantity);
                if($status->code == 0){
                    $result['errors'][] = $status->data;
                    $result['code'] = 0;
                }

            }

            return $this->returnAnswer(resultObject::withData($result['code'],'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function usedGeoSubscription(){
        try{
            $usage = new customerSubscriptionUsageObject();

            $usage->csuse_csu_id = $_REQUEST['csu_id'];
            $usage->csuse_cust_id = $this->mobileRequestObject->server_customer_id;
            $usage->csuse_biz_id = $this->mobileRequestObject->bizid;
            $usage->csuse_item_type = 'other';
            $usage->csuse_source = 'geolocation';
            $usage->csuse_type = 'usage';

            $result = customerSubscriptionManager::useCustSubscription($usage);
            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function usedGeoPunchPass(){
        try{
            $usage = new customerPunchUsageObject();

            $usage->cpuse_cpp_id = $_REQUEST['cpp_id'];
            $usage->cpuse_cust_id = $this->mobileRequestObject->server_customer_id;
            $usage->cpuse_biz_id = $this->mobileRequestObject->bizid;
            $usage->cpuse_item_type = 'other';
            $usage->cpuse_source = 'geolocation';
            $usage->cpuse_type = 'usage';

            $result = customerSubscriptionManager::useCustPunchPass($usage);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*   POINTS SHOP                       */
    /************************************* */

    public function claimFromPointsShop(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $itemID = $_REQUEST['id'];

            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $itemResult = $bizModel->getPointsGrantByID($itemID);

            if($itemResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_item'));
            }
            $item = $itemResult->data;

            $result = $this->customersModel->claimFromPointsShop($customer,$item);
            customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function unclaimFromPointsShop(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $benefitID = $_REQUEST["id"];

            $result = $this->customersModel->unclaimFromPointsShop($customer,$benefitID);
            customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$this->mobileRequestObject->server_customer_id);
            customerManager::sendAsyncCustomerDataUpdateToDevice('badges',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function getPointsHistory(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $skip = isset($_REQUEST['skip']) ? $_REQUEST['skip'] : 0;
            $take = isset($_REQUEST['skip']) ? 10 : 0;

            $entriesResult = $this->customersModel->getPointsHistoryForCustomer($customer,$skip,$take);

            $history = array();

            if($entriesResult->code == 1){
                foreach ($entriesResult->data as $entry)
                {
                	$history[] = $entry->getAPIFormattedArray();
                }
            }

            return $this->returnAnswer(resultObject::withData(1,'',$history));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*           SCRATCH CARDS             */
    /************************************* */

    public function claimScratchReward(){

        $customerModel = new customerModel();
        $result = $customerModel->getScratchedReward($_REQUEST['cscard_id'],$this->mobileRequestObject->bizid);
        customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$this->mobileRequestObject->server_customer_id);
        return $this->returnAnswer($result);
    }

    public function getCustomerScratchCard(){

        $customerModel = new customerModel();
        $result = $customerModel->getCustomerScratchCard($_REQUEST['cscard_id']);

        return $this->returnAnswer($result);
    }

    /************************************* */
    /*   HELPER FUNCTIONS                  */
    /************************************* */

    public function setCustomerLocation(){

        $customerModel = new customerModel();
        $customerResponse = $customerModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);

        if($customerResponse->code == 1){
            $customerResponse->data->cust_longt = $_REQUEST["longtCust"];
            $customerResponse->data->cust_lati = $_REQUEST["latiCust"];
            $customerResponse->data->cust_location_updated = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
            $customerResponse = $customerModel->updateCustomer($customerResponse->data);
        }

        return $this->returnAnswer($customerResponse);
    }

    public function zeroCustomerBadge(){

        $badge_name = isset($_REQUEST['badge']) ? $_REQUEST['badge'] : '';

        if($badge_name != ''){
            customerManager::zeroBadgeTypeForCustomer($this->mobileRequestObject->server_customer_id,$badge_name);
            customerManager::sendAsyncCustomerDataUpdateToDevice('badges',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer(resultObject::withData(1,'ok'));
        }

        return $this->returnAnswer(resultObject::withData(0,'no_badge_name'));
    }

    public function subtractBadgeCounter(){

        $badge_name = isset($_REQUEST['badge']) ? $_REQUEST['badge'] : '';

        if($badge_name != ''){
            customerManager::subtractOneBadgeTypeForCustomer($this->mobileRequestObject->server_customer_id,$badge_name);
            customerManager::sendAsyncCustomerDataUpdateToDevice('badges',$this->mobileRequestObject->server_customer_id);
            return $this->returnAnswer(resultObject::withData(1,'ok'));
        }

        return $this->returnAnswer(resultObject::withData(0,'no_badge_name'));
    }

    public function sendPersonalDataAsync($custID,$deviceID,$is_android = ""){

        customerManager::sendAsyncCustomerDataUpdateToDevice('meetings',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('push',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('requests',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('documents',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('wishlist',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('friends',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('multiuseitems',$custID,$deviceID,$is_android);
        customerManager::sendAsyncCustomerDataUpdateToDevice('points',$custID,$deviceID,$is_android);
        return;
    }

	/************************************* */
    /*          PLIVO FUNCTIONS            */
    /************************************* */

    private function sendValidationCodeSMS($validCode,$number,$customerId){

        if(customerManager::isFraudulentCustomerID($this->mobileRequestObject->server_customer_id)){
            return;
        }

        if(smsManager::isFraudulentCountry($this->mobileRequestObject->bizid,$this->mobileRequestObject->country)){
            return;
        }

        if(smsManager::canSendCodeSms($number,$customerId)){

            $text = "$validCode - This is your validation code.";
            $smsHash="";
            $first4 = substr($number,0,4);
            if($this->mobileRequestObject->deviceID != "web" && $this->mobileRequestObject->OS == 'Android'){
                $smsHash = bizManager::getBizSmsHash($this->mobileRequestObject->bizid);
                $text = "<#>\n\nThis is your validation code: $validCode\n\n".$smsHash;
            }


            $fh = fopen("plivo_sms_log.txt", 'a');
            $date = new DateTime();
            $time = $date->format('Y-m-d H:i:s');
            fwrite($fh, $time." - Biz: ".$this->mobileRequestObject->bizid." | Number: ".$number." | CustID: ".$this->mobileRequestObject->server_customer_id." Request: ".print_r($_REQUEST,true)."\n");
            fclose($fh);

            smsManager::sendMessage($number,$text,"sendSMS");
            smsManager::addSendCodeSms($number);
        }


        return;
    }

    public function validationCodeCall(){
        
        try{
           
            if($this->mobileRequestObject->deviceID == "web" || customerManager::isDeviceIOS($this->mobileRequestObject->deviceID)){
               
                $this->executeCodeCall();
            }
            
            return $this->returnAnswer(resultObject::withData(0,'all_good'));
        }
        catch(Exception $e){
           
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            
            return $this->returnAnswer($result);
        }
    }

    public function validateCaptcha($token){
        try{          
            
            $body = array();
            $body['secret'] = "6Lft6McUAAAAALdXVvO5E8v5ZkLyajl9LI2NT6Xd";
            $body['response'] = $_REQUEST["token"];
            $captchaResponse = utilityManager::curl("https://www.google.com/recaptcha/api/siteverify",array(),$body,true);

            $result = json_decode($captchaResponse);
           
            $code = isset($result->success) && $result->success ? 1 : 0;
            
            return $this->returnAnswer(resultObject::withData($code,''));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function sendValidationCodeCallAfterCaptcha(){
        try{
            if(!isset($_REQUEST["token"])){
                return $this->returnAnswer(resultObject::withData(0,'all_good'));//psychological warfare
            }

            if(utilityManager::validateCaptcha($_REQUEST["token"])->code == 0){//failed captcha
                return $this->returnAnswer(resultObject::withData(0,'all_good'));//psychological warfare
            }

            $this->executeCodeCall();

            return $this->returnAnswer(resultObject::withData(0,'all_good'));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    private function executeCodeCall(){
        $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
        if($customerResult->code == 0){
            return;
        }

        $customer = $customerResult->data;
        $code = $customer->cust_valid_code;
        $to = trim($customer->cust_valid_phone);
        if(substr( $to, 0, 1 ) !== "+"){
            $to = "+".$to;
        }
        $lang = bizManager::getAppOwnerLang($this->mobileRequestObject->bizid);
        
        $fh = fopen("plivo_call_log.txt", 'a');
        $date = new DateTime();
        $time = $date->format('Y-m-d H:i:s');
        fwrite($fh, $time." - Biz: ".$this->mobileRequestObject->bizid." | Number: ".$to." | CustID: ".$this->mobileRequestObject->server_customer_id." Request: ".print_r($_REQUEST,true)."\n");
        fclose($fh);

        //Pseudo Code for validation
        //1. count 3 times per number.
        //2. every time a number exceeds 3 times it goes to black list with phone number + customer id.
        //3. every time a customer reaches 3 blocked numbers - the customer is blocked (marked as fraud)
        //4. every time a biz gets 3 fraud customers in a single country the country is banned from SMS/Calls in that country for the biz. -> if the owner is not from that country.

        if(customerManager::isFraudulentCustomerID($this->mobileRequestObject->server_customer_id)){
            return;//psychological warfare
        }

        if(smsManager::isFraudulentCountry($this->mobileRequestObject->bizid,$this->mobileRequestObject->country)){
            return;//psychological warfare
        }

        $canMakeCallResult = smsManager::addSendCodeVoiceCall($to,$this->mobileRequestObject->bizid,$this->mobileRequestObject->server_customer_id,$customer->cust_ip);

        if($canMakeCallResult->code == 0){
            return;//psychological warfare
        }

        smsManager::makeCodeCall($to,$code,$lang);

        return;
    }

    /************************************* */
    /*  WEB MEMBER                         */
    /************************************* */

    function setWebClient(){
        try{
            $ip = utilityManager::get_real_IP();

            $phone = utilityManager::getValidPhoneNumber(trim($_REQUEST['phone']));
            $name = isset($_REQUEST['name']) ? urldecode($_REQUEST['name']) : "";


            $existingCustResult = $this->customersModel->getCustIDByValidPhoneNumber($phone,$this->mobileRequestObject);

            $custID = 0;

            if($existingCustResult->code == 1 && $existingCustResult->data > 0){//customer exists with phone number
                $custID = $existingCustResult->data;
                $customerResult = $this->customersModel->getCustomerWithID($custID);
                $customer = $customerResult->data;

                $customer->cust_returning = 1;
                if( $customer->cust_mobile_serial == ""){
                    $time = time() % 1000000;
                    $customer->cust_mobile_serial = 'web_'.$time;
                    $customer->cust_last_mobile_serial = 'web_'.$time;
                }
                $this->customersModel->updateCustomer($customer);
                $existing = $customer->cust_status == 'member' ? 1 : 0;
            }
            else{
                $addResult = $this->customersModel->addWebCustomer($this->mobileRequestObject,$_REQUEST,$ip);

                $custID = $addResult->data;
                $existing = 0;
            }

            $validationCodeResult = $this->customersModel->setCustomerValidationCodeAndPhoneAndName($custID,"+".$phone,$name);

            if($validationCodeResult->code == 1){
                $validationCode = $validationCodeResult->data;
                //Send Sms with validation code
                $this->sendValidationCodeSMS($validationCode,"+".$phone,$custID);
            }


            $result = $this->customersModel->getCustomerWithID($custID);

            if($result->message == "" && $existing == 1){
                $result->message = "exists";
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getWebMemberClient(){
        try{
            $ip = utilityManager::get_real_IP();
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);

            if($customerResult->code == 1){
                $customer = $customerResult->data;
                $custID = $customer->cust_id;
                $customerResult->data->cust_last_seen = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
                $this->customersModel->updateCustomer($customer);
                customerManager::updateCustomerLocationByIP($custID,$ip);
                $this->sendPersonalDataAsync($this->mobileRequestObject->server_customer_id,$this->mobileRequestObject->deviceID);
                $customer->getCustomerUnseenSMS();

                //get rewards for client
                $rewards = array();
                //$entriesResult = $this->customersModel->getCustomerBenefitsForPersonalZone($customer);
                //if($entriesResult->code == 1){
                //    foreach ($entriesResult->data as $entry)
                //    {
                //        array_push($rewards,$entry->PersonalZoneAPIArray($this->mobileRequestObject));
                //    }
                //}

                $pushManager = new pushManager();

                $pushManager->setAllCustomerSMSToSeen($custID);
                $customerResult->data = $customer;
                eventManager::actionTrigger(enumCustomerActions::webVisit,$this->mobileRequestObject->server_customer_id, "visited_web",'',$this->mobileRequestObject->deviceID);

                $returnData = array();
                $returnData["custData"] = $customer;
                $returnData["rewards"] = $rewards;

                return $this->returnAnswer(resultObject::withData(1,'',$returnData));
            }
            else{
                return $this->returnAnswer($customerResult);
            }


        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function validateWebmemberClientByPhone(){
        try{
            $ip = utilityManager::get_real_IP();
            $phone = utilityManager::getValidPhoneNumber(trim($_REQUEST['phone']));
            $existingCustResult = $this->customersModel->getCustIDByValidPhoneNumber($phone,$this->mobileRequestObject);

            $custID = 0;

            if($existingCustResult->code == 1 && $existingCustResult->data > 0){//customer exists with phone number
                $custID = $existingCustResult->data;

                $result = $this->customersModel->getCustomerWithID($custID);

                $customer = $result->data;

                $bizExclusive = bizManager::isBizExclusive($this->mobileRequestObject->bizid);

                if($customer->cust_status != 'member' && $bizExclusive){

                    $result = resultObject::withData(0,"no_customer");
                }
                else{
                    customerManager::updateCustomerLocationByIP($custID,$ip);

                    $validationCodeResult = $this->customersModel->setCustomerValidationCodeAndPhoneAndName($custID,"+".$phone);

                    if($validationCodeResult->code == 1){
                        $validationCode = $validationCodeResult->data;
                        //Send Sms with validation code
                        $this->sendValidationCodeSMS($validationCode,"+".$phone,$custID);

                        $result->data->cust_valid_code = $validationCode;
                    }

                    $updateCustomer = false;

                    if($customer->cust_status == 'member'){
                        $customer->cust_returning = 1;
                        $updateCustomer = true;
                    }



                    $this->customersModel->updateCustomer($customer);

                    if(!isset($customer->cust_mobile_serial) || $customer->cust_mobile_serial == ''){

                        $time = time() % 1000000;
                        $customer->cust_mobile_serial = 'web_'.$time;
                        $customer->cust_last_mobile_serial = 'web_'.$time;
                        $updateCustomer = true;
                    }

                    if($updateCustomer){
                        $this->customersModel->updateCustomer($customer);
                    }

                }
            }
            else{

                $result = resultObject::withData(0,"no_customer");
            }


            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function validateWebPhoneCode(){
        try{

            $result = $this->customersModel->validateWebCustomer($this->mobileRequestObject->server_customer_id);

            if($result->code == 1){
                eventManager::actionTrigger(enumCustomerActions::webMemberValidate,$this->mobileRequestObject->server_customer_id,'web_validate','','web');
                $customer = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id)->data;
                if($customer->cust_status == 'member' && $customer->cust_returning == 1){
                    eventManager::actionTrigger(enumCustomerActions::welcomeBack,$customer->cust_id,'welcome_back','',$this->mobileRequestObject->deviceID);
                    $customer->cust_returning  = 0;
                    $this->customersModel->updateCustomer($customer);
                }

                if($customer->cust_status == 'member' && $customer->cust_inviter == 0 && isset($_REQUEST["invite_clipboard_code"]) && $_REQUEST["invite_clipboard_code"] != ""){

                    $customer->cust_inviter = $_REQUEST["invite_clipboard_code"];
                    $this->customersModel->updateCustomer($customer);
                    eventManager::actionTrigger(enumCustomerActions::invitedAFriend,$_REQUEST["invite_clipboard_code"],'invited_friend',$customer->cust_first_name,'',$customer->cust_id);
                }
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    function grantInitialWebPoints(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }
            $customer = $customerResult->data;

            $pointsGrant = new customerPointsHistoryObject();

            $pointsGrant->cph_biz_id = $this->mobileRequestObject->bizid;
            $pointsGrant->cph_cust_id = $this->mobileRequestObject->server_customer_id;
            $pointsGrant->cph_value = bizManager::getBizWelcomeGrant($this->mobileRequestObject->bizid);
            $pointsGrant->cph_source = 'web_register';

            $result = $this->customersModel->grantCustomerPoints($customer,$pointsGrant);

            if($result->code == 1){
                eventManager::actionTrigger(enumCustomerActions::webMemberClaimPoints,$this->mobileRequestObject->server_customer_id,'web_claim_points','','web');
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getWebMemberPointsData(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }
            $customer = $customerResult->data;

            $data = array();

            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $data['points_item'] = $bizModel->getBizPopularPointShopItem()->data;


            $data['friends'] = array();

            $friendsResult = $this->customersModel->getCustomerInvitedFriends($customer);

            if($friendsResult->code == 1){
                foreach ($friendsResult->data as $friend)
                {
                    $data['friends'][] = $friend->getArrayForFriendList();
                }

            }

            $entriesResult = $this->customersModel->getPointsHistoryForCustomer($customer,0,10);

            $data['history'] = array();

            if($entriesResult->code == 1){
                foreach ($entriesResult->data as $entry)
                {
                	$data['history'][] = $entry->getAPIFormattedArray();
                }
            }

            return $this->returnAnswer(resultObject::withData(1,'',$data));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getWebMorePointsHistory(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }
            $customer = $customerResult->data;

            $skip = isset($_REQUEST['skip']) ? $_REQUEST['skip'] : 0;

            $entriesResult = $this->customersModel->getPointsHistoryForCustomer($customer,$skip,10);

            $data = array();

            if($entriesResult->code == 1){
                foreach ($entriesResult->data as $entry)
                {
                    $data[] = $entry->getAPIFormattedArray();
                }
            }

            return $this->returnAnswer(resultObject::withData(1,'',$data));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getWebBenefits(){
        try{
            $customerResult = $this->customersModel->getCustomerWithID($this->mobileRequestObject->server_customer_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }
            $customer = $customerResult->data;

            $skip = isset($_REQUEST['skip']) ? $_REQUEST['skip'] : 0;

            $entriesResult = $this->customersModel->getCustomerBenefitsForPersonalZone($customer,$skip,10);

            customerManager::zeroBadgeTypeForCustomer($this->mobileRequestObject->server_customer_id,'benefits');

            $data = array();

            if($entriesResult->code == 1){
                foreach ($entriesResult->data as $entry)
                {
                    $data[] = $entry->PersonalZoneAPIArray($this->mobileRequestObject);
                }
            }

            return $this->returnAnswer(resultObject::withData(1,'',$data));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }
}
