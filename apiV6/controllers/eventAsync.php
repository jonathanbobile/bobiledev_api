<?php

/**
 * eventAsync short summary.
 *
 * eventAsync description.
 *
 * @version 1.0
 * @author JonathanM
 */

//require ROOT_PATH.'libs/Ratchet/vendor/autoload.php';
//use Ratchet\Server\IoServer;
//use Ratchet\Http\HttpServer;
//use Ratchet\WebSocket\WsServer;
//require_once 'Chat.php';



class eventAsync extends Controller
{
    function __construct()
    {
        parent::__construct(true);
        
    }

    public function triggerAction($cust_action_id){
       
        $eventManager = new eventManager();

        $eventManager->actionTriggered($cust_action_id);

    }

    public function triggeringAction(){
        $eventManager = new eventManager();
        
        $actionTriggerObject = actionTriggerObject::withData($_REQUEST);

        
        
        $eventManager->actionTriggering($actionTriggerObject);
    }

    public function triggerCustomerStageCheck($cust_id){

        $customersModel = new customerModel();
        $customersModel->checkCustomerStage($cust_id);
    }

    public function grantInviterPoints(){
        $friendID = $_REQUEST['friend_id'];
        $amount = $_REQUEST['amount'];
       
        $customerMod = new customerModel();

        $customerMod->grantInviterPointsExecute($friendID,$amount);
        
    } 
    
    public function sendInstantMessage(){
        
        $message = json_decode(urldecode(stripslashes($_POST['message'])));
       
        $target = $_POST['receiver'];
        $type = $_POST['message_type'];
        $postFallback = isset($_POST['fallback']) ? json_decode($_POST['fallback']) : null;
        
        $forAndroid = isset($_POST['for_android']) && $_POST['for_android'] == 1;

        if(isset($postFallback)){
            $fallback = new pushParamsObject();
            foreach($postFallback as $property => $value) { 
                $fallback->$property = $value; 
            } 
        }
        else{
            $fallback = null;
        }

        wsocketManager::sendIM($message,$target,$type,$fallback,$forAndroid);
    }

    public function sendInstantData(){
        
        $message = json_decode($_POST['message']);
      
        $target = $_POST['receiver'];
        $type = $_POST['message_type'];
        $postFallback = isset($_POST['fallback']) ? json_decode($_POST['fallback']) : null;
        
        $forAndroid = isset($_POST['for_android']) && $_POST['for_android'] == 1;

        if(isset($postFallback)){
            $fallback = new pushParamsObject();
            foreach($postFallback as $property => $value) { 
                $fallback->$property = $value; 
            } 
        }
        else{
            $fallback = null;
        }

        wsocketManager::sendIM($message,$target,$type,$fallback,$forAndroid);
    }

    public function sendInstantClientChat(){
        $chatID = $_POST['chatID'];
        $customerID = $_POST['customerID'];
        $deviceID = $_POST['deviceID'];
        $customerModel = new customerModel();

        $customerResult = $customerModel->getCustomerWithID($customerID);

        if($customerResult->code != 1){
            return;
        }
        $customer = $customerResult->data;

        $chatManager = new chatManager();

        $chatResult = $chatManager->getChatMessageByID($chatID);
        if($chatResult->code != 1){
            return;
        }

        $chat = $chatResult->data;
       
        $socketMessage = new stdClass();
        $socketMessage->chat_message = $chat->ch_message;
        $receiver = $chat->ch_user."_".$chat->ch_customer."_chat_listener";
        $wsendResult = wsocketManager::sendIMSynchronous($socketMessage,$receiver,enumSocketType::client_chat_message);
       
        $notifications = new notificationsManager();
        $params["customer_id"] = $customerID;
        $notifications->addNotification($customer->cust_biz_id,enumActions::newCatMessage,false,$params);

      
        $extraParams["chat_id"] = $chatID;
        $extraParams["customer_id"] = $customerID;
        $extraParams["customer_device_id"] = $deviceID;
          
        pushManager::sendPushAdmin($chat->ch_user,enumPushType::admin_chat,$chat->ch_message,$extraParams);
       
        $params["messID"] = $chatID;
        utilityManager::asyncTask(PUSH_SERVER."/admin/autojobs/sendpushChat",$params);

        $customer->cust_unread_chats = $customer->cust_unread_chats + 1;
        $customerModel->updateCustomer($customer);
    }

    public function logSocketConnection(){
        $identity = $_REQUEST['identity'];
        $connection_id = $_REQUEST['connection_id'];

        $wsManager = new wsocketManager();

        $log_id = $wsManager->logConnection($identity,$connection_id);

        header('Content-Type: application/json');
        utilityManager::echoJavaScriptResponseArray(1,'',$log_id);
    }

    public function deleteSocketConnection($socket_conn_id){
        

        $wsManager = new wsocketManager();

        $wsManager->deleteConnection($socket_conn_id);
    }

    public function updateCustomerAddressByLocation(){

        $geoData = utilityManager::getAddresByLongAndLati($_REQUEST["lati"],$_REQUEST["long"]);
        if(isset($geoData["country_id"]) || isset($geoData["state_id"]) || isset($geoData["city"])){
            $customerModel = new customerModel();
            $resultCustomer = $customerModel->getCustomerWithID($_REQUEST["custId"]);

            if($resultCustomer->code == 1){
                if(isset($geoData["country_id"])){
                    $resultCustomer->data->cust_country = $geoData["country_id"];
                }
                if(isset($geoData["state_id"])){
                    $resultCustomer->data->cust_state = $geoData["state_id"];
                }
                if(isset($geoData["city"])){
                    $resultCustomer->data->cust_city = $geoData["city"];
                }
                $customerModel->updateCustomer($resultCustomer->data);
            }
        }

    }

    public function updateCustomerAddressByIP(){

        $geoData = utilityManager::getUsersAddress($_REQUEST['ip']);
        
        if($geoData['statusCode'] == 'OK'){
            $customerModel = new customerModel();
            $resultCustomer = $customerModel->getCustomerWithID($_REQUEST["custID"]);
            
            if($resultCustomer->code == 1){
                $customer = $resultCustomer->data;
                $customer->cust_city = $geoData['cityName'];
                $customer->cust_country = utilityManager::getCountryId($geoData['countryCode']);
                $customer->cust_state = utilityManager::getStateIdFromName($geoData['regionName']);
                $customerModel->updateCustomer($customer);
            }
        }

    }

    public function getMobileRequestObjectByDeviceType($isAndroid=true, customerObject $customer){


       
        $retVal = new mobileRequestObject();
        $retVal->bizid = $customer->cust_biz_id;
        $retVal->server_customer_id = $customer->cust_id;
        $retVal->android = $isAndroid ? 1 : 0;

        return $retVal;
    }



    public function sendAsyncPersonalZoneData(){
        $type = $_REQUEST['type'];
        $custID = $_REQUEST['cust_id'];
        $deviceID = isset($_REQUEST['device_id']) ? $_REQUEST['device_id'] : "";
        $deviceType = isset($_REQUEST['device_type']) ? $_REQUEST['device_type'] : "";
        $this->customersModel = new customerModel();
        $customerResult = $this->customersModel->getCustomerWithID($custID);
        if($customerResult->code != 1){
            exit;
        }
        $customer = $customerResult->data;
        
        if($deviceID == ""){
            $deviceID = $customer->cust_last_mobile_serial != "" ? $customer->cust_last_mobile_serial : $customer->cust_mobile_serial;
        }

        if($deviceType == ""){
            $deviceType = $customer->cust_last_device != "" ? $customer->cust_last_device : $customer->cust_device;
        }
      
        $collectionToSend = $this->customersModel->getPersonalDataForcustomerFromType($customer,$type);
       
       
        $iOSMobileObjectRequest = $this->getMobileRequestObjectByDeviceType(false,$customer);
        $androidMobileRequestObject = $this->getMobileRequestObjectByDeviceType(true,$customer);

        

        $iOSPersonalZoneComponents = $this->getPersonalZoneReturnDataForMobileRequest($iOSMobileObjectRequest,$collectionToSend,$type);
        $androidPersonalZoneComponents = $this->getPersonalZoneReturnDataForMobileRequest($androidMobileRequestObject,$collectionToSend,$type);        

        if ($deviceType == 'Android'){
            $dataToSend = $androidPersonalZoneComponents["dataToSend"];
            $socketType = $androidPersonalZoneComponents["socketType"];
        }else{
            $dataToSend = $iOSPersonalZoneComponents["dataToSend"];
            $socketType = $iOSPersonalZoneComponents["socketType"];
        }

       

        $sendType = 'client'.$socketType.'Data';
        $target = $custID.'_'.$deviceID;
        
        $post = array();
        $post['receiver'] = $target;
        $post['message_type'] = $sendType;
        $post['message'] = urlencode(json_encode($dataToSend));
        
        $post['for_android'] = $deviceType == 'Android' ? 1 : 0;
        $url = "http://127.0.0.1/".MVC_NAME."/eventAsync/sendInstantData";
        
        utilityManager::asyncTask($url,$post,false);

        exit;
    }


    public function getPersonalZoneReturnDataForMobileRequest(mobileRequestObject $mobileRequestObject, array $collectionToSend, $type=""){

        $dataToSend = array();
        $socketType = "";

        $retVal = array();
        
        switch($type){
            case "meetings":
                if(bizManager::hasPersonalZoneElement('appointments',$mobileRequestObject->bizid)){
                    $socketType = 'Booking';                        
                    $dataToSend['featured'] = array();
                    foreach ($collectionToSend as $meeting){
                        $entry = $meeting->PersonalZoneAPIArray();
                        $dataToSend['featured'][] = $entry;
                    }  
                }

                break;
            case "push":
                if(bizManager::hasPersonalZoneElement('notifications',$mobileRequestObject->bizid)){
                    $socketType = 'Push';
                    $dataToSend['featured'] = array();
                    foreach ($collectionToSend as $push){
                        $dataToSend['featured'][] = $push->PersonalZoneAPIArray();
                    }
                }
                break;
            case "orders":
                if(bizManager::hasPersonalZoneElement('orders',$mobileRequestObject->bizid)){
                    $socketType = 'Orders';
                    foreach ($collectionToSend as $orderObj){
                        $order = $orderObj->PersonalZoneAPIArray();
                        $dataToSend[] = $order;
                    }
                }
                break;
            case "benefits":
                if(bizManager::hasPersonalZoneElement('benefits',$mobileRequestObject->bizid)){
                    $socketType = 'Benefits';
                    foreach ($collectionToSend as $benefit){                                    
                        $dataToSend[] = $benefit->PersonalZoneAPIArray($mobileRequestObject);
                    }
                }
                break;
            case "requests":
                if(bizManager::hasPersonalZoneElement('requests',$mobileRequestObject->bizid)){
                    $socketType = 'Requests';
                    foreach ($collectionToSend as $orderObj){
                        $order = $orderObj->PersonalZoneAPIArray();
                        $dataToSend[] = $order;
                    }
                }
                break;
            case "documents":
                if(bizManager::hasPersonalZoneElement('documents',$mobileRequestObject->bizid)){
                    $socketType = 'Documents';
                    foreach ($collectionToSend as $doc){
                        $dataToSend[] = $doc->PersonalZoneAPIArray($mobileRequestObject);
                    }
                }
                break;
            case "wishlist":
                $socketType = 'Wishlist';
                foreach ($collectionToSend as $wish){
                    $dataToSend[] = $wish->PersonalZoneAPIArray();
                }
                break;
            case "friends":
                if(bizManager::hasPersonalZoneElement('invitefriends',$mobileRequestObject->bizid)){
                    $socketType = 'Friends';
                    foreach ($collectionToSend as $friend){
                        $dataToSend[] = $friend->getArrayForFriendList();
                    }
                }
                break;
            case "subscriptions":
                if(bizManager::hasPersonalZoneElement('subscriptions',$mobileRequestObject->bizid)){
                    $socketType = 'Subscriptions';
                    $dataToSend = $collectionToSend;
                }
                break;
            case "punchpasses":
                if(bizManager::hasPersonalZoneElement('punchpass',$mobileRequestObject->bizid)){
                    $socketType = 'Punchpasses';
                    $dataToSend = $collectionToSend;
                }
                break;
            case "multiuseitems":
                $dataToSend = $collectionToSend;
                break;
            case "points":
                $socketType = 'PointsHistory';               
                foreach ($collectionToSend as $entry){
                    $dataToSend[] = $entry->getAPIFormattedArray();
                } 
                break;
            case "membership":
                $socketType = 'Membership';
                $dataToSend = $collectionToSend;
                break;
            case "badges":
                $socketType = 'Badges';
                $dataToSend['badges'] = $collectionToSend;
                break;
            case "default_payment":
                $socketType = 'DefaultPayment';
                $dataToSend = $collectionToSend;
                break;
        }

        $retVal["dataToSend"] =  $dataToSend;
        $retVal["socketType"] = $socketType;

        return $retVal;

    }

    public function processOrder(){
        

        customerOrderManager::processCustOrder($_REQUEST['order_id']);
    }

    public function processAdminOrder(){
        customerOrderManager::processAdminOrder($_REQUEST['order_id']);
    }
}
