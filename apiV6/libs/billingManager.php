<?php
require_once "mpdf60/mpdf.php";


require_once('../admin/libs/Composer/vendor/autoload.php');

/**
 * Methods for billing
 *
 * @version 1.0
 * @author Alex
 */
class billingManager extends Manager
{
    private $zoozUniqueId = "com.paptap.webSB";
    private $zoozAppKey = "3dfb00fc-3080-4a06-b485-8515fc12ea53";
    private $zoozIsSandbox = false;

    private $data = array();
    
    function __construct()
    {
        parent::__construct();
        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        \Stripe\Stripe::setApiVersion(getenv('STRIPE_API_VERSION'));
    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }

    public function getMonthlyPlansResellers($start_date, $reseller_id)
    {
        $resellerRow = $this->db->getRow("select * from tbl_resellers,tbl_plans where pl_id=reseller_plan and reseller_id=$reseller_id");
        $apps = $this->getResellerAppsCount($reseller_id);       
        
        $start_date = date('Y-m-01 00:00:00',strtotime($start_date));
        $end_date = date('Y-m-t 23:59:59',strtotime($start_date));
        $items = array();
        
        if($resellerRow["reseller_is_wl"] == "0"){ //not white label  
            $q="SELECT    tp.pl_name as name,
                        tp.pl_id as id,
                        '$start_date' as start_date, 
                        '$end_date' as end_date,
                        CEIL(SUM(TIMESTAMPDIFF(SECOND, '$start_date', '$end_date') / 86400)) as days,
                        tp.pl_plrice as monthly_price,
                        tp.pl_plrice * IF(MOD(12 * (YEAR('$end_date')- YEAR(ttf.tf_start_time)) + (MONTH('$end_date') - MONTH(ttf.tf_start_time)), 12 / tp.pl_anual_freq_times) , 0, 1) as price,
                        tp.pl_anual_freq_name as period,
                        tp.pl_billing_type,
                        tp.pl_interval,
                        tp.pl_entity_id,
                        tp.pl_anual_freq_times,
                        tp.pl_anual_freq_type,
                        ttf.tf_trial_date,
                        'plan' as type,
                        '1' as quantity
                             FROM tbl_transaction_feature ttf
                             INNER JOIN tbl_plans tp ON ttf.tf_entity_id = tp.pl_id
                             WHERE tp.pl_id = 30
                             and ttf.tf_reseller_id = $reseller_id
                             GROUP BY ttf.tf_entity_id";
            
            $items = $this->db->getTable($q);
        }
        
        $specTable = ($reseller_id == 80) ? "_$reseller_id" : "";
            
        $extraRows = $this->getAditionalAppsResellers($resellerRow,$apps,$start_date,$end_date,$specTable);        

        if(count($extraRows)>0){
            if($resellerRow["reseller_is_wl"] == "0"){ //not white label
                $items[0]["monthly_price"] = $items[0]["monthly_price"] + $extraRows[0]["monthly_price"];
            }else{
                $items = array_merge((array)$items,(array)$extraRows);
            }
        }
        return $items;
    }
    
    public function getResellerBilling($reseller_id){
        $sql = "SELECT * FROM tbl_resellers 
                         LEFT JOIN tbl_resellers_programs ON reseller_program = rp_id
                         LEFT JOIN tbl_reseller_connect_program ON reseller_id = rcp_reseller_id AND rcp_end_time IS NULL
                         WHERE reseller_id = $reseller_id";
        $result = $this->db->getRow($sql);
      
       
        $result['app_count'] = $this->getResellerAppsCount($reseller_id);           
        $result['markets_count'] = $this->getResellerMarketsCount($reseller_id);
        

        return $result;
    }
    
    public function getResellerOwnerId($reseller_id){
        $sql = "SELECT owner_id FROM tbl_owners WHERE owner_reseller_id = $reseller_id";        
        return $this->db->getVal($sql);
    }
    
    public function getAditionalAppsResellers($resellerRow,$apps,$start_date,$end_date,$specTable)
    {
        $extraRows = array();        
        if($resellerRow["reseller_is_wl"] == "0"){ //not white label           
            if($apps > $resellerRow["pl_start_apps"]){
                $priceForApp = $resellerRow["pl_aditional_app"];
                $extraApps = $apps - $resellerRow["pl_start_apps"];
                        
                $payMonthly = $priceForApp * $extraApps;
                
                $row["name"] = "Additional $extraApps apps";
                $row["id"] = "0";
                $row["start_date"] = $start_date;
                $row["end_date"] = $end_date;
                $row["days"] = "0";
                $row["monthly_price"] = $payMonthly;
                $row["price"] = $payMonthly;
                $row["period"] = "0";
                $row["pl_billing_type"] = "";
                $row["pl_interval"] = "0";
                $row["pl_entity_id"] = "0";
                $row["pl_anual_freq_times"] = "0";
                $row["pl_anual_freq_type"] = "0";
                $row["tf_trial_date"] = "";
                $row["type"] = "Custom";
                $row["quantity"] = $extraApps;
                array_push($extraRows,$row);
            }
        }
        else{     
            if($apps < $resellerRow["pl_start_apps"]) $apps = $resellerRow["pl_start_apps"];
            $cureProgramRow = $this->db->getRow("select * from tbl_resellers_programs$specTable where rp_number_apps >= $apps order by rp_number_apps limit 1");
            $programs = $this->db->getTable("select * from tbl_resellers_programs$specTable where rp_number_apps <= {$cureProgramRow["rp_number_apps"]} order by rp_number_apps");
            
            if(count($programs) > 0){
                $processedApps = 0;
                $tierNumber = 0;
                $extraApps = 0;
                $payMonthly = 0;
                
                foreach ($programs as $program)
                {
                    $tierNumber++;
                    $priceForApp = $program["rp_app_price"];
                    
                    if($tierNumber == count($programs)){
                        $extraApps = $apps - $processedApps;
                        $processedApps += $extraApps;
                        $payMonthly = $payMonthly + $extraApps * $priceForApp;
                    }else{
                        $extraApps = $program["rp_number_apps"] - $processedApps;
                        $payMonthly = $payMonthly + $extraApps * $priceForApp;
                        $processedApps += $extraApps;
                    }  
                }
       
                $row["name"] = "$processedApps ".$this->language->get('apps')." ".$this->language->get('tier')."$tierNumber";
                $row["id"] = "0";
                $row["start_date"] = $start_date;
                $row["end_date"] = $end_date;
                $row["days"] = "0";
                $row["monthly_price"] = $payMonthly;
                $row["price"] = $payMonthly;
                $row["period"] = "0";
                $row["pl_billing_type"] = "";
                $row["pl_interval"] = "0";
                $row["pl_entity_id"] = "0";
                $row["pl_anual_freq_times"] = "0";
                $row["pl_anual_freq_type"] = $resellerRow["pl_anual_freq_type"]; 
                $row["tf_trial_date"] = "";
                $row["type"] = "Custom";
                $row["quantity"] = $extraApps;
                array_push($extraRows,$row);
            }
        }
        return $extraRows;
    }

    public function getTotalBilledThisMonth($bizID){
        return $this->db->getVal("SELECT sum(tri_total_amount) FROM tbl_trans_invoice 
                                WHERE tri_biz_id = $bizID 
                                and tri_draft=0
                                and date_format(tri_time, '%Y-%m')=date_format(now(), '%Y-%m')");
    }
    
    public function getAllInvoices($biz_id)
    {
        $reselerID = $this->db->getVal("select owner_reseller_id from tbl_owners,tbl_biz where owner_id=biz_owner_id and biz_id = $biz_id");
        
        $sql = "SELECT * FROM tbl_trans_invoice left join tbl_transaction on tri_id=tr_invoice WHERE tri_biz_id = $biz_id and tri_draft=0 and tri_status not in ('authorized','voided') order by tri_time DESC";
        
        if($reselerID != 0){
            $sql = "SELECT * FROM tbl_trans_invoice left join tbl_transaction on tri_id=tr_invoice WHERE tri_reseller_id = $reselerID and tri_draft=0 and tri_status not in ('authorized','voided') order by tri_time DESC";
        }
        return $this->db->getTable($sql);
    }

    public function getAllInvoicesClient($biz_id)
    {
        $sql = "SELECT * FROM tbl_reseller_invoice LEFT JOIN tbl_reseller_transaction ON tri_id=tr_invoice 
                WHERE tri_biz_id = $biz_id 
                AND tri_draft=0 
                AND tri_status NOT IN ('authorized','voided','initiated') 
                ORDER BY tri_time DESC";
        return $this->db->getTable($sql);
    }

    public function getAllInvoicesReseller($reselerID)
    {
        $sql = "SELECT * FROM tbl_trans_invoice left join tbl_transaction on tri_id=tr_invoice WHERE tri_reseller_id = $reselerID and tri_draft=0 order by tri_time DESC";
        return $this->db->getTable($sql);
    }

    public function getAllInvoicesResellerClients($reselerID)
    {
        $sql = "
                SELECT *,ADDDATE(tri_time, INTERVAL 1 MONTH) due_date FROM tbl_biz,tbl_reseller_invoice 
                LEFT JOIN tbl_reseller_transaction ON tri_id=tr_invoice 
                WHERE biz_id=tri_biz_id
                AND tri_reseller_id = $reselerID 
                AND tri_status NOT IN('initiated','voided')
                ORDER BY tri_time DESC
                ";
        return $this->db->getTable($sql);
    }
    public function getPaidInvoicesResellerClients($reselerID)
    {
        $sql = "
                SELECT *,ADDDATE(tri_time, INTERVAL 1 MONTH) due_date FROM tbl_biz,tbl_reseller_invoice 
                LEFT JOIN tbl_reseller_transaction ON tri_id=tr_invoice 
                WHERE biz_id=tri_biz_id
                AND tri_reseller_id = $reselerID 
                AND tri_status='paid'
                ORDER BY tri_time DESC
                ";
        return $this->db->getTable($sql);
    }
    public function getDraftInvoicesResellerClients($reselerID)
    {
        $sql = "
                SELECT *,ADDDATE(tri_time, INTERVAL 1 MONTH) due_date FROM tbl_biz,tbl_reseller_invoice 
                LEFT JOIN tbl_reseller_transaction ON tri_id=tr_invoice 
                WHERE biz_id=tri_biz_id
                AND tri_reseller_id = $reselerID 
                AND (tri_status='pending' OR tri_draft =1)
                ORDER BY tri_time DESC
                ";
        return $this->db->getTable($sql);
    }
    public function getOutstandingInvoicesResellerClients($reselerID)
    {
        $sql = "
                SELECT *,ADDDATE(tri_time, INTERVAL 1 MONTH) due_date FROM tbl_biz,tbl_reseller_invoice 
                LEFT JOIN tbl_reseller_transaction ON tri_id=tr_invoice 
                WHERE biz_id=tri_biz_id
                AND tri_reseller_id = $reselerID 
                AND tri_status='outstanding'
                AND ADDDATE(tri_time, INTERVAL 1 MONTH) > NOW()
                ORDER BY tri_time DESC
                ";
        return $this->db->getTable($sql);
    }
    public function getPastDueInvoicesResellerClients($reselerID)
    {
        $sql = "
                SELECT *,ADDDATE(tri_time, INTERVAL 1 MONTH) due_date FROM tbl_biz,tbl_reseller_invoice 
                LEFT JOIN tbl_reseller_transaction ON tri_id=tr_invoice 
                WHERE biz_id=tri_biz_id
                AND tri_reseller_id = $reselerID 
                AND tri_status='outstanding'
                AND ADDDATE(tri_time, INTERVAL 1 MONTH) <= NOW()
                ORDER BY tri_time DESC
                ";
        return $this->db->getTable($sql);
    }

    public function getTransactionItems($tr_id_md5)
    {
        return $this->db->getTable("SELECT   tri_invoice_id,tri_name as name,
                                    tri_days as days,
                                    tri_monthly_price as monthly_price,
                                    tri_price as price,
                                    tri_quantity as quantity,
                                    tri_entity as entity
                                    FROM tbl_transaction_items tti 
                                    WHERE md5(tti.tri_invoice_id) = '" . addslashes($tr_id_md5) . "'");
    }

    public function getTransactionItemsClient($tr_id_md5)
    {
        return $this->db->getTable("SELECT tri_invoice_id,tri_name as name,
                                    tri_days as days,
                                    tri_monthly_price as monthly_price,
                                    tri_price as price,
                                    tri_quantity as quantity,
                                    tri_entity as entity
                                    FROM tbl_reseller_invoice_items tti 
                                    WHERE md5(tti.tri_invoice_id) = '" . addslashes($tr_id_md5) . "'");
    }

    public function getCustomersTransactionItems($tr_id_md5)
    {
        return $this->db->getTable("SELECT tri_name as name,
                                    tri_price as price,
                                    tri_quantity as quantity,
                                    tr_currency
                                    FROM tbl_cust_transaction_items,tbl_cust_transaction 
                                    WHERE tri_order_id=tr_order_id
                                    and md5(tri_order_id) = '$tr_id_md5'");
    }

    public function getCustInvoices($biz_id, $thisMonth, $startFrom=0)
    {
        
        if($thisMonth){
            $addToSql = "and date_format(cto_order_tyme, '%Y-%m')=date_format(now(), '%Y-%m')";
            $limit="";
        }
        else{
            $addToSql = "and cto_order_tyme < DATE_FORMAT(NOW() ,'%Y-%m-01')";
            $limit = "limit $startFrom, 8";
        }
        
        $q = "SELECT * FROM tbl_cust_transaction_order left join tbl_cust_transaction on cto_id=tr_order_id 
                                WHERE cto_biz_id = $biz_id 
                                and tr_id is not null 
                                and cto_order_type='eCommerce'
                                $addToSql 
                                order by cto_order_tyme DESC
                                $limit";
        return $this->db->getTable($q);
    }
    
    public function getOrderItems($tr_id_md5)
    {
        $q = "SELECT   tri_name as name,
                                    it_name as type,
                                    tri_quantity as quantity,
                                    tri_price as price
                                    FROM tbl_cust_transaction_items tti,tbl_item_type 
                                    WHERE tri_item_type=it_id and md5(tti.tri_order_id) = '" . addslashes($tr_id_md5) . "'";
        return $this->db->getTable($q);
    }
    
    public function getCustTotalInvoices($biz_id)
    {
       
        $q = "SELECT count(cto_id) FROM tbl_cust_transaction_order left join tbl_cust_transaction on cto_id=tr_order_id 
                                WHERE cto_biz_id = $biz_id 
                                and tr_id is not null 
                                and cto_order_tyme < DATE_FORMAT(NOW() ,'%Y-%m-01')
                                and cto_order_type='eCommerce'
                                order by cto_order_tyme DESC";
        return $this->db->getVal($q);
    }

    public function getPayPalHost($sandbox){
        if($sandbox)
            return "www.sandbox.paypal.com";
        else
            return "www.paypal.com";
    }
    
    public function getPayPalAuthToken($sandbox){
        if($sandbox)
            return "iA2cgcqT6JnMyAdtOw4BbbGeI_if00Z_qyavSaBK0h5jds-aPN0MoCKCZE4";
        else
            return "wYrdZfA8ZthghtS6C15OUZM79kIPgf9sLhLkZiwl1LxW82oyXk4DO4NjYti";
    }
    
    public function insertTransactionPayPal($p,$type='CHARGE'){
        
        $jsona = json_decode($p->ipn_data["custom"]);       
        
        $selectedID = $p->ipn_data['item_number'];
        $uid = $jsona->uid;
        $ipAddress = $jsona->ip;
        $country = $jsona->country;
        $phone = $jsona->phone;
        $paymentMethodType = "PayPal";
        $paymentMethodToken = $p->ipn_data['txn_id'];
        $captureCode = $p->ipn_data['verify_sign'];
        $amount = $p->ipn_data['payment_gross'];
        $reconciliationId = $p->ipn_data['payer_id'];
        $currency = $p->ipn_data['mc_currency'];
        
        $planRow = $this->getPlanRow($selectedID);
        $planType = $planRow['pl_type'];
        
        $transID = $this->db->execute("insert into tbl_transaction set
                           tr_plan_id = $selectedID,
                           tr_plan_type = $planType,
                           tr_biz_id = $uid,
                           tr_reference_Id = '$paymentMethodToken',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = '$type',
                           tr_amount=$amount,
                           tr_currency='$currency',
                           tr_pay_method = 'PP',
                           tr_paymentMethodToken='$paymentMethodToken',
                           tr_paymentMethodType='$paymentMethodType',
                           tr_cvv = '',
                           tr_ip = '$ipAddress',
                           tr_country = '$country',
                           tr_phone = '$phone'
                          ");

        foreach($jsona->item as $oneItem)
        {
            $itmname= addslashes(stripslashes($oneItem->itemName));
            $quantity=$oneItem->itemQuantity;
            $price=$oneItem->itemPrice;
            
            $this->db->execute("insert into tbl_transaction_items set
                                tri_tr_id=$transID,
                                tri_name='$itmname',
                                tri_quantity='$quantity',
                                tri_price=$price
                                ");
        }
    }
    
    public function insertTransactionCC($p,$authorizeResponseArray,$type='CHARGE'){
        
        $paymentMethodToken = NULL;
        $paymentMethodType = NULL;
        $cvvNumber = NULL;
        $ipAddress = NULL;
        $planType = NULL;
        $uid = NULL;
        $amount = NULL;
        $currency = NULL;
        $selectedID = NULL;
        $items= NULL;
        $ufullname= NULL;
        $uEmail= NULL;
        $country= NULL;
        $phone= NULL;
        $invoice=NULL;
        $updateTblBiz = NULL;
        $owner_id = NULL;
        $lastFourDigits = NULL;
        $paymentToken = NULL;

        foreach ($p as $key => $value) {
            switch ($key) {
                case 'paymentMethodToken' : $paymentMethodToken = $value;
                    break;
                case 'paymentMethodType' : $paymentMethodType = $value;
                    break;
                case 'cvvNumber' : $cvvNumber = $value;
                    break;
                case 'ipAddress' : $ipAddress = $value;
                    break;
                case 'planType' : $planType = $value;
                    break;
                case 'bizID' : $uid = $value;
                    break;
                case 'amount' : $amount = $value;
                    break;
                case 'currency' : $currency = $value;
                    break;
                case 'selectedID' : $selectedID = $value;
                    break;
                case 'items' : $items = $value;
                    break;
                case 'ufullname' : $ufullname = addslashes(stripslashes($value));
                    break;
                case 'uEmail' : $uEmail = $value;
                    break;
                case 'country' : $country = $value;
                    break;
                case 'phone' : $phone = $value;
                    break;
                case 'invoice' : $invoice = $value;
                    break; 
                case 'updateTblBiz' : $updateTblBiz = $value;
                    break; 
                case 'owner_id' : $owner_id = $value;
                    break; 
                case 'lastFourDigits' : $lastFourDigits = $value;
                    break;
                case 'paymentToken' : $paymentToken = $value;
                    break;
            }
        }
        
         
        
        $processorReferenceId = $authorizeResponseArray["responseObject"]["processorReferenceId"];
        $captureCode = $authorizeResponseArray["responseObject"]["captureCode"];
        $reconciliationId = (isset($authorizeResponseArray["responseObject"]["reconciliationId"])) ? $authorizeResponseArray["responseObject"]["reconciliationId"] : "";
        
        
        $planRow = $this->getPlanRow($selectedID);
        
        $isReseller = utilityManager::contains($uid,'-R');
        $rid = 0;
        if(!$isReseller){
            $bizRow =  $this->db->getRow("select * from tbl_biz,tbl_categories,tbl_sub_categories, tbl_owners 
				                     where biz_category_id=cat_id 
                                     and sub_sub_id=biz_sub_category_id 
                                     and biz_id=$uid 
                                     and biz_owner_id = owner_id");
            $aff_id = $bizRow["owner_aff_id"];
        }
        else{
            $aff_id = 0;
            $rid = str_replace('-R','',$uid);
            $uid = '0';
        }
        
        $transID = $this->db->execute("insert into tbl_transaction set
                           tr_plan_id = $selectedID,
                           tr_invoice = $invoice,
                           tr_plan_type = $planType,
                           tr_biz_id = $uid,
                           tr_reference_Id = '$processorReferenceId',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = '$type',
                           tr_amount=$amount,
                           tr_currency='$currency',
                           tr_pay_method = 'CC',
                           tr_paymentMethodToken='$paymentMethodToken',
                           tr_paymentMethodType='$paymentMethodType',
                           tr_cvv = '$cvvNumber',
                           tr_ip = '$ipAddress',
                           tr_country = '$country',
                           tr_phone = '$phone',
                           tr_aff_id = $aff_id,
                           tr_resellerid = $rid
                          ");
        
        $this->db->execute("update tbl_transaction_items set
                                tri_tr_id=$transID
                                where tri_invoice_id=$invoice
                                ");
        
        $this->updatePaydStatus($invoice,1);
        
        $biz_package_type = $planRow["pl_type"];
        $email_template = $planRow["pl_email_template"];
        
        if($updateTblBiz){
           
            if(!$isReseller){

                $updateSQL = "update tbl_biz set                                    
                                    biz_paymentMethodToken = '$paymentMethodToken',
                                    biz_paymentMethodType = '$paymentMethodType',
                                    biz_cvv = '$cvvNumber',
                                    biz_payment_name='$ufullname',
                                    biz_payment_email='$uEmail',
                                    biz_status=1,                                  
                                    biz_cc_last_digits = '$lastFourDigits'
                                    WHERE biz_id = $uid";
                
                appManager::addDeleteResellerMarket($uid);
            }
            else{
                $updateSQL = "update tbl_resellers set 
                                reseller_paymentMethodToken = '$paymentMethodToken',
                                reseller_paymentMethodType = '$paymentMethodType',
                                reseller_cvv = '$cvvNumber',
                                reseller_lastFourDigits = '$lastFourDigits',
                                reseller_name_on_ccard = '$ufullname',
                                reseller_is_demo = 0
                                WHERE reseller_id = $rid";
            }
            $this->db->execute($updateSQL); 
            
            if($biz_package_type == 3 || $biz_package_type == 4 || $biz_package_type == 8){
                $this->fillSubmitForm($uid,$bizRow);
            }
            
            if($aff_id != "0"){
                $this->setAffiliateCommission($aff_id,$invoice);
            }

            if($email_template != ""){ 
                emailManager::sendSystemMailApp($uid,$email_template,enumEmailType::SystemMailSGCare);
            }
        }
        
    }
    
    public function updatePaydStatus($invoice,$status){
        $this->db->execute("update tbl_trans_invoice set
                                tri_payd=$status,
                                tri_draft=0
                                where tri_id=$invoice
                                ");
    }

    public function duplicateChargeTransaction($paymentMethodToken){
        
        return $this->db->getVal("select count(tr_id) from tbl_transaction where tr_reference_Id='$paymentMethodToken' and tr_type='CHARGE'") != "0";
    }
    
    public function getTransactionByInvoiceId($invoiceID){
        
        return $this->db->getVal("select tr_id from tbl_transaction where tr_invoice=$invoiceID");
    }

    public function selfMail($repSubjectTEXT,$processorReferenceId,$uid,$appName,$amount,$currency,$errorCode,$errorDescription,$paymentToken,$methodToken="",$toMail="billing@bobile.com"){
        
        $repTEXT = "Reference ID: $processorReferenceId <br />
                            User ID: $uid <br />
                            App Name: $appName <br />
                            Transaction Type: $repSubjectTEXT<br />
                            Amount: $amount.$currency <br />
                            Error Code: $errorCode <br />
                            Error Description: $errorDescription <br />
                            Payment Token: $paymentToken <br />
                            Payment Method Token: $methodToken <br />";
        
        emailManager::sendCustomMail(emailManager::getDefaulProvider(),$toMail,$repSubjectTEXT,$repTEXT,"Admin");       
    }
    
    public function zoozErrorMail($uid,$errorCode,$errorDescription){
        $repSubjectTEXT = "ZOOZ Error";
        $repTEXT = "User ID: $uid <br />
                        Transaction Type: CHARGE <br />
                        Error Code: $errorCode <br />
                        Error Description: $errorDescription <br />";
        
        emailManager::sendCustomMail(emailManager::getDefaulProvider(),"billing@bobile.com",$repSubjectTEXT,$repTEXT,"Admin");
    }
    
    public function fillSubmitForm($user_id,$pBizRow)
    {
        $appURL = "https://bobile.com/app/".$user_id."/".strtolower(str_replace(" ","-",addslashes(stripslashes($pBizRow["biz_short_name"]))));
        $description = addslashes(stripslashes(strip_tags($pBizRow["biz_odot"], '<br><br/><br />')));
        $breaks = array("<br />","<br>","<br/>");  
        $description = str_ireplace($breaks, "\r\n", $description);
        
        $updateSQL = "update tbl_biz SET
                  biz_submit_mrkt_lang = biz_default_lng,
                  biz_copy_right = biz_short_name,
                  biz_submit_avl_date = curdate(),
                  biz_submit_desc = '$description',
                  biz_submit_keys = biz_short_name,
                  biz_submit_sprt_url = '$appURL'
                  where biz_id = $user_id";
        
        $this->db->execute($updateSQL);
    }
    
    public function updateToken($post,$uid){
        
        $paymentMethodToken = $post["paymentMethodToken"];

        $paymentMethodType = $post["paymentMethodType"];
        $cvvNumber = $post["cvvNumber"];
        $lastFourDigits = $post["lastFourDigits"];
        $ufullname = addslashes($post["ufullname"]);
        $email = $post["email"];
        
        if($_SESSION['rid'] > 0  && $_SESSION['clientid'] == 0){
            $updateSQL = "update tbl_resellers set 
                reseller_paymentMethodToken = '$paymentMethodToken',
                reseller_paymentMethodType = '$paymentMethodType',
                reseller_cvv = '$cvvNumber',
                reseller_lastFourDigits = '$lastFourDigits',
                reseller_name_on_ccard = '$ufullname'
                WHERE reseller_id = {$_SESSION['rid']}";
        }else{
            $updateSQL = "update tbl_biz set 
                biz_paymentMethodToken = '$paymentMethodToken',
                biz_paymentMethodType = '$paymentMethodType',
                biz_cvv = '$cvvNumber',
                biz_cc_last_digits = '$lastFourDigits',
                biz_payment_name = '$ufullname',
                biz_payment_email = '$email',
                biz_status=1
                WHERE biz_id = $uid";     
            
            appManager::addDeleteResellerMarket($uid);
        }
        $res = $this->db->execute($updateSQL);
        
        $message="";
        if($res != -1){
            if(!isset($_SESSION['rid']) || $_SESSION['rid'] == 0){
                $_SESSION["appData"]["biz_paymentMethodToken"] = $paymentMethodToken;
                $_SESSION["appData"]["biz_paymentMethodType"] = $paymentMethodType;
                $_SESSION["appData"]["biz_cvv"] = $cvvNumber;
                $_SESSION["appData"]["biz_cc_last_digits"] = $lastFourDigits;
                $_SESSION["appData"]["biz_payment_name"] = $ufullname;
                $_SESSION["appData"]["biz_payment_email"] = $email;                
            }
        }
        else{
            $message = "Oops Something went wrong.";
        }

        $clientSuccessResponse = new ClientSuccessResponse($message,$res);
        return $clientSuccessResponse->toJson();
    }
    
    public function updatePhoneNumber($accountId,$phone){
        
        $validPhone = $this->db->getVal("select ac_phone_valid from tbl_account where ac_id=$accountId");
        if($validPhone == "0"){
            $this->db->execute("update tbl_account set ac_phone='$phone' where ac_id=$accountId");
        }
    }
    
    public function deleteInitialInvoicesBiz($bizID){
        
        if($bizID != ""){
            $invoices = $this->db->getTable("SELECT tri_id FROM tbl_trans_invoice WHERE tri_biz_id = $bizID AND tri_status = 'initiated'");
            if (count($invoices) > 0){
                foreach ($invoices as $inv)
                {
                    $this->deleteInvoice($inv["tri_id"]);
                }
            }
        }
        unset($_SESSION["paymentToken"]);
        unset($_SESSION["invoiceID"]);
    }

    public function deletePendingInvoicesBiz($bizID){
        
        $invoices = $this->db->getTable("SELECT tri_id FROM tbl_trans_invoice WHERE tri_biz_id = $bizID AND tri_status = 'pending'");
        if (count($invoices) > 0){
            foreach ($invoices as $inv)
            {
            	$this->deleteInvoice($inv["tri_id"]);
            }
        }
    }

    public function voidPendingInvoicesBiz($bizID){
        
        $invoices = $this->db->getTable("SELECT tri_id FROM tbl_trans_invoice WHERE tri_biz_id = $bizID AND tri_status = 'pending'");
        if (count($invoices) > 0){
            foreach ($invoices as $inv)
            {
            	$this->voidInvoice($inv["tri_id"]);
            }
        }
    }

    public function existInitialInvoicesBiz($bizID){
        
        return $this->db->getVal("SELECT count(tri_id) FROM tbl_trans_invoice WHERE tri_biz_id = $bizID AND tri_status = 'initiated'") > 0;
       
    }
    
    public function deleteInitialInvoicesReseller($rID){
        
        $invoices = $this->db->getTable("SELECT tri_id FROM tbl_trans_invoice WHERE tri_reseller_id = $rID AND tri_status = 'initiated'");
        if (count($invoices) > 0){
            foreach ($invoices as $inv)
            {
            	$this->deleteInvoice($inv["tri_id"]);
            }
        }
        
        unset($_SESSION["paymentToken"]);
        unset($_SESSION["invoiceID"]);
    }
    
    public function deleteInvoice($invoice_id){     

        $this->stripe_closeInvoice($invoice_id);

        $this->db->execute("delete from tbl_trans_invoice where tri_id = $invoice_id");
        $this->db->execute("delete from tbl_transaction_items where tri_invoice_id = $invoice_id");
    }
    
    public function getEntityData($entity,$uid,$modid,$appName=''){
        $planInterval="0";
        $itemPic = "";
        $itemName = "";
        
        switch (strtolower($entity))
        {
            case "appstore":
                $approw = $this->db->getRow("select name,image from tbl_appstores where id=$modid");
                $itemName = $approw["name"];
                $itemPic = $approw["image"];
                break;
            case "function":
                $funcRow = $this->db->getRow("SELECT fu_default_label,fu_image FROM tbl_functions WHERE fu_id=$modid");
                $itemName = $funcRow["fu_default_label"];
                $itemPic = $funcRow["fu_image"];
                break;  
            case "module":
                $modRow = $this->db->getRow("SELECT REPLACE(msc_label,'#biz_name#','$appName') as label,mod_pic
                                    FROM tbl_modules tm 
                                    INNER JOIN tbl_mod_subcat tms ON tms.msc_mod_id = tm.mod_id
                                    INNER JOIN tbl_biz tb ON tms.msc_sub_id = tb.biz_sub_category_id
                                    WHERE mod_id = $modid
                                    AND tb.biz_id = $uid");
                $itemName = $modRow["label"];
                $itemPic = $modRow["mod_pic"];
                
                break; 
            case "plan":
                $planRow = $this->db->getRow("SELECT pl_name,pl_interval FROM tbl_plans where pl_id=$modid AND pl_entity = 'bundle' and pl_active=1");
                $itemName = $planRow["pl_name"];
                $planInterval = $planRow["pl_interval"];
                break; 
            case "deal":
                $planRow = $this->db->getRow("SELECT pl_name,pl_interval FROM tbl_plans where pl_id=$modid AND pl_entity = 'deal' and pl_active=1");
                $itemName = $planRow["pl_name"];
                $planInterval = $planRow["pl_interval"];
                break;                
        }
        
        return array('itemName' => $itemName, 'itemPic' => $itemPic, 'planInterval' => $planInterval);
    }
    
    public function getItemsByInvoiceID($invoceID){
        $jsonObject = new stdClass();
        $itemsDT = $this->db->getTable("select * from tbl_transaction_items where tri_invoice_id=$invoceID");    
        if(count($itemsDT)>0){
            $i=0;
            foreach ($itemsDT as $oneItem)
            {
                $jsonObject->item[$i]["itemName"] = $oneItem["tri_name"];
                $jsonObject->item[$i]["itemQuantity"] = $oneItem["tri_quantity"];
                $jsonObject->item[$i]["itemPrice"] = $oneItem["tri_price"];
                $i++;
            }
        }
        return $jsonObject;
    }
    
    public function getUserRowForInvoiceServer($invoiceID){
        
        $lang = isset($_SESSION['system_lang']) ? $_SESSION['system_lang'] : "en";
        
        return $this->db->getRow("SELECT biz_id,biz_short_name,biz_contact_name,cat_name_$lang,owner_password,owner_conf_code,sub_name_$lang,biz_paymentMethodToken,biz_addr_country_id,biz_office_tele,
                            biz_paymentMethodType,biz_cvv,biz_plan_id,biz_payment_name,biz_payment_price,biz_payment_email,biz_package_type,tri_id,tri_total_amount,tri_currency,tri_month,tri_year,tri_collection,owner_username 
            FROM tbl_biz, tbl_owners,tbl_categories,tbl_sub_categories,tbl_trans_invoice
            WHERE  tri_biz_id=biz_id
            and biz_owner_id = owner_id
            and biz_category_id=cat_id 
            and sub_sub_id=biz_sub_category_id
            and biz_paymentMethodToken <> 'free'
            and tri_payd=0
            and biz_paymentMethodType<>''
            and tri_id=$invoiceID
            ");
    }
    
    public function getUserForForm($uid){
        
        return $this->db->getRow("SELECT owner_name, owner_username, owner_system_lang, biz_short_name, biz_addr_country_id,biz_addr_state_id, 
                    biz_mobile_tele,biz_paymentMethodToken,biz_paymentMethodType,biz_cvv,biz_cc_last_digits,biz_payment_name,biz_payment_price,biz_plan_id,biz_payment_email
                    FROM tbl_owners, tbl_biz 
                    WHERE biz_id = $uid
                    AND biz_owner_id = owner_id");
    }
    
    public function getOpenInvoices($invoiceID){
 
        $lang = isset($_SESSION['system_lang']) ? $_SESSION['system_lang'] : "en";

        return $this->db->getRow("SELECT biz_id,biz_short_name,biz_contact_name,cat_name_$lang,owner_password,owner_conf_code,sub_name_$lang,biz_paymentMethodToken,biz_addr_country_id,biz_office_tele,
                            biz_paymentMethodType,biz_cvv,biz_plan_id,biz_payment_name,biz_payment_price,biz_payment_email,biz_package_type,tri_id,tri_total_amount,tri_currency,tri_month,tri_year,tri_collection,owner_username,
                            owner_aff_id,owner_aff_group,owner_id,owner_currency_code,owner_zooz_merchant_id,biz_zooz_program_id
            FROM tbl_biz, tbl_owners,tbl_categories,tbl_sub_categories,tbl_trans_invoice
            WHERE  tri_biz_id=biz_id
            AND biz_owner_id = owner_id
            AND biz_category_id=cat_id 
            AND sub_sub_id=biz_sub_category_id
            AND tri_id=$invoiceID");
        
    }
    
    public function getOpenInvoicesReseller($invoiceID){
        
        return $this->db->getRow("SELECT reseller_id,reseller_full_name,reseller_company_name,owner_password,owner_conf_code,reseller_paymentMethodToken,reseller_country_id,reseller_phone,
                            reseller_paymentMethodType,reseller_cvv,reseller_name_on_ccard,tri_id,tri_total_amount,tri_currency,tri_month,tri_year,tri_collection,owner_username,
                            owner_aff_id,owner_aff_group,owner_id,reseller_program reseller_plan,owner_currency_code
            FROM tbl_resellers, tbl_owners,tbl_trans_invoice
            WHERE  reseller_id=tri_reseller_id
            and reseller_id = owner_reseller_id
            and tri_id=$invoiceID");
    }

    public function getResellerInvoices($invoiceID=""){
        return $this->db->getRow("SELECT * FROM tbl_trans_invoice,tbl_resellers,tbl_owners
                                        WHERE tri_reseller_id = reseller_id
                                        AND reseller_id = owner_reseller_id
                                        AND tri_payd=0                                        
                                        AND tri_id=$invoiceID");
    }

    public function getResellerPayToken($resellerID){
        $sql = "SELECT reseller_paymentMethodToken FROM tbl_resellers WHERE reseller_id = $resellerID";

        return $this->db->getVal($sql);
    }

    public function getResellerLastDigits($resellerID){
        $sql = "SELECT reseller_lastFourDigits FROM tbl_resellers WHERE reseller_id = $resellerID";

        return $this->db->getVal($sql);
    }
    
    public function transactionBadRow($uid,$invoiceId){
        return $this->db->getRow("select (date_add( tri_time,INTERVAL 16 day) < now()) end_grace, (date_add( ttb_last_reminder,INTERVAL 7 day) <= now()) send_mail,ttb_id,ttb_last_err_code,ttb_last_err_desc,ttb_collection,ttb_invoice_id 
                                    from tbl_transaction_bad ,tbl_trans_invoice
                                    where tri_biz_id=ttb_biz_id
                                    and tri_payd=0
                                    and tri_draft=0
                                    and ttb_biz_id=$uid
                                    AND ttb_invoice_id=$invoiceId
                                    order by tri_id
                                    limit 1");
    }
    
    public function transactionBadSetCollection($uid,$invoiceId){
        return $this->db->execute("update tbl_transaction_bad set ttb_collection = 1 where ttb_biz_id=$uid AND ttb_invoice_id=$invoiceId");
    }
    
    public function transactionBadRowReseller($rid,$invoiceId){
        return $this->db->getRow("select (date_add( tri_time,INTERVAL 27 day) < now()) end_grace, (date_add( ttb_last_reminder,INTERVAL 7 day) <= now()) send_mail,ttb_id,ttb_last_err_code,ttb_last_err_desc,ttb_invoice_id 
                                    from tbl_transaction_bad ,tbl_trans_invoice
                                    where ttb_reseller_id=tri_reseller_id
                                    and tri_payd=0
                                    and tri_draft=0
                                    and ttb_reseller_id=$rid
                                    AND ttb_invoice_id=$invoiceId");
    }
    
    public function addTransactionBadRow($uid,$selectedID,$errorCode,$errorDescription,$invoiceId){
        $this->db->execute("insert into tbl_transaction_bad set 
                            ttb_biz_id=$uid,
                            ttb_plan_id=$selectedID,
                            ttb_last_err_code='$errorCode',
                            ttb_last_err_desc='$errorDescription',
                            ttb_invoice_id=$invoiceId");
    }

    public function lastChargeTransactionBadRow($id){
        $this->db->execute("update tbl_transaction_bad set 
                            ttb_last_charge=now()
                            where ttb_id = $id");
    }
    
    public function addTransactionBadRowReseller($rid,$selectedID,$errorCode,$errorDescription,$invoiceId){
        $this->db->execute("insert into tbl_transaction_bad set 
                            ttb_reseller_id=$rid,
                            ttb_plan_id=$selectedID,
                            ttb_last_err_code='$errorCode',
                            ttb_last_err_desc='$errorDescription',
                            ttb_invoice_id=$invoiceId");
    }
    
    public function updateTransactionBadRow($ttbID,$send_mail,$errorCode,$errorDescription){
        
        $addUpdate = ($send_mail == "1") ? ",ttb_last_reminder = now()" : "";
        
        $this->db->execute("update tbl_transaction_bad set                            
                                        ttb_last_err_code='$errorCode',
                                        ttb_last_err_desc='$errorDescription'
                                        $addUpdate
                                        where ttb_id=$ttbID");
    }
    
    public function updateTransactionBadRowUser($biz_id,$send_mail,$errorCode,$errorDescription,$invoiceId){
        
        $addUpdate = ($send_mail == "1") ? ",ttb_last_reminder = now()" : "";
        
        $this->db->execute("update tbl_transaction_bad set                            
                                        ttb_last_err_code='$errorCode',
                                        ttb_last_err_desc='$errorDescription'
                                        $addUpdate
                                        where ttb_biz_id=$biz_id
                                        AND ttb_invoice_id=$invoiceId");
    }
    
    public function endGrace($uid,$invoiceID,$trBadId,$selectedID,$errorCode,$errorDescription){
        
        $bizRow = $this->db->getRow("select biz_paymentMethodToken,biz_paymentMethodType,biz_cvv,biz_cc_last_digits from tbl_biz where biz_id=$uid");

        $this->db->execute("insert into tbl_transaction_cancel set 
                                ttc_biz_id=$uid,
                                ttc_plan_id=$selectedID,
                                ttc_err_code='$errorCode',
                                ttc_err_desc='$errorDescription',
                                ttc_paymentMethodToken='{$bizRow["biz_paymentMethodToken"]}',
                                ttc_paymentMethodType='{$bizRow["biz_paymentMethodType"]}',
                                ttc_cvv='{$bizRow["biz_cvv"]}',
                                ttc_cc_last_digits='{$bizRow["biz_cc_last_digits"]}',
                                ttc_invoice_id=$invoiceID
                                ");
        
        if($invoiceID == 0){
            $modules = $this->db->getTable("SELECT * FROM tbl_transaction_feature 
                                            WHERE tf_biz_id = $uid 
                                            and tf_end_time is null");
        }else{
            $this->db->execute("update tbl_trans_invoice set
                                    tri_collection=1
                                    where tri_id=$invoiceID
                                    ");
        
            //Get only the list of the items in the outstanding invoice. 
            $modules = $this->db->getTable("SELECT * FROM tbl_transaction_feature,tbl_transaction_items 
                                            WHERE tf_entity_id = tri_mod_id
                                            and tri_invoice_id=$invoiceID
                                            and tri_price > 0
                                            and tf_biz_id = $uid 
                                            and tf_end_time is null");
        }
        
        $entitiesManager = new entitiesManager();

        if(count($modules) > 0){

            foreach ($modules as $oneModule)
            {
                switch ($oneModule["tf_entity"]) {
                    case 'module' : 
                        $entitiesManager->removeModuleFromBiz($oneModule["tf_entity_id"], $uid);
                        break;
                    case 'appstore' : 
                        $entitiesManager->removeAppstoreFromBiz($oneModule["tf_entity_id"], $uid);
                        break;
                    case 'function' : 
                        $entitiesManager->removeFunctionFromBiz($oneModule["tf_entity_id"], $uid);
                        break;
                    case 'plan' :
                        $entitiesManager->cancelAccount($uid);
                        break;
                }   
            } 
            if(isset($_SESSION["appData"])){
                appManager::initEngagements($uid);
                appManager::initAppModules($uid);        
                appManager::initAppData($uid);
                appManager::initAppFunctions($uid);
                appManager::initAppStores($uid);
            }           
        }
        notificationsManager::addInfoBannerToBiz($uid,2);
        $this->db->execute("delete from tbl_transaction_bad WHERE ttb_id=$trBadId");
    }
    
    //Legacy
    public function endGraceCollection($uid,$invoiceID,$trBadId,$selectedID,$errorCode,$errorDescription){
        
        $bizRow = $this->db->getRow("select biz_paymentMethodToken,biz_paymentMethodType,biz_cvv,biz_cc_last_digits from tbl_biz where biz_id=$uid");

        $this->db->execute("insert into tbl_transaction_cancel set 
                                ttc_biz_id=$uid,
                                ttc_plan_id=$selectedID,
                                ttc_err_code='$errorCode',
                                ttc_err_desc='$errorDescription',
                                ttc_paymentMethodToken='{$bizRow["biz_paymentMethodToken"]}',
                                ttc_paymentMethodType='{$bizRow["biz_paymentMethodType"]}',
                                ttc_cvv='{$bizRow["biz_cvv"]}',
                                ttc_cc_last_digits='{$bizRow["biz_cc_last_digits"]}',
                                ttc_invoice_id=$invoiceID
                                ");
        
        $this->db->execute("update tbl_trans_invoice set
                                tri_collection=1
                                where tri_id=$invoiceID
                                ");
           
        $this->db->execute("delete from tbl_transaction_bad WHERE ttb_id=$trBadId");
    }
    
    public function deleteTransactionBadRow($uid,$invoiceId){
        
        $this->db->execute("delete from tbl_transaction_bad WHERE ttb_biz_id=$uid AND ttb_invoice_id=$invoiceId");
    }
    
    public function deleteTransactionBadRowReseller($rid,$invoiceId){
        
        $this->db->execute("delete from tbl_transaction_bad WHERE ttb_reseller_id=$rid AND ttb_invoice_id=$invoiceId");
    }
    
    public function getPlanRow($planId){
        return $this->db->getRow("select * from tbl_plans where pl_id=$planId");
    }

    public function lastInvoiceCreateDate(){
        $response = $this->db->getVal("SELECT COUNT(stam_invoice_time) FROM tbl_stam 
                                                     WHERE YEAR(stam_invoice_time) = YEAR(CURRENT_DATE)
                                                     AND MONTH(stam_invoice_time) = MONTH(CURRENT_DATE)
                                                     AND DAY(stam_invoice_time) = DAY(CURRENT_DATE)");
        if($response == "1"){
            $this->db->execute("UPDATE tbl_stam SET stam_invoice_time = DATE_ADD(stam_invoice_time, INTERVAL 1 MONTH)");
        }
        return $response > 0;
    }
    
    // ***************************************************************** CREATE INVOICES ***************************************************************** //

    public function createInvoices(){
 
        $bizs = $this->getAppsListForInvoicesStandart(); //returns all the apps that are live premium & not in trial.

        if(count($bizs)>0){        
            foreach ($bizs as $bizRow) {

                //issue invoice only if it's ammount bigger then 0
                if($bizRow["tf_price"] > 0){
                    $items = array();
                    $items[] = $this->preparePlanInvoiceItem($bizRow);
                    $invoiceId = $this->issueInvoice($bizRow['biz_id'],$items,"false",0,0,1);
                    $this->setPendingStatusToInvoice($invoiceId);
                }

                $interval = utilityManager::getNextBillingIntervalInMonths($bizRow);
                $this->setNextBillingDateForTransactionFeature($bizRow["tf_id"],$interval);
            }
        }
    }

    public function setNextBillingDateForTransactionFeature($transactionFeatureId,$intervalMonth){

        $sql = "UPDATE tbl_transaction_feature 
                    SET tf_next_billing_date=DATE_ADD(tf_next_billing_date, INTERVAL $intervalMonth MONTH) 
                    WHERE tf_id=$transactionFeatureId";
        $this->db->execute($sql);
    }

    public function addItemsFromArray($itemsArray,$entityArray){

        if(count($entityArray) > 0){
            foreach ($entityArray as $itemRow)
            {
            	array_push($itemsArray,$itemRow);  
            }            
        }

        return $itemsArray;
    }

    public function takeAppOutOfTrial($bizId){
        $this->db->execute("update tbl_transaction_feature SET tf_trial_date = NULL
                                                WHERE tf_biz_id=$bizId
                                                and tf_trial_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')");
    }

    public function takeAppOutOfTrialNoConditions($bizId){
        $this->db->execute("update tbl_transaction_feature SET tf_trial_date = NULL
                                                WHERE tf_biz_id=$bizId
                                                and tf_trial_date IS NOT NULL");

        $_SESSION["appData"]['biz_in_trial'] = false;
    }

    public function createInvoicesReseller(){
              
        $resellers = $this->getResellersForInvoices();

        if(count($resellers)>0){
            
            foreach ($resellers as $resellerRow) {             
                
                $items = $this->getMonthlyPlansPCAS($resellerRow);
                
                if (!empty($items)) {
                    $total = $this->getItemsTotal($items);
                    //issue invoice only if it's ammount bigger then 0
                    if($total > 0){
                        $invoiceId = $this->issueInvoice($resellerRow['reseller_id'],$items,"false",0,1,1);
                        $this->setPendingStatusToInvoice($invoiceId);
                    }
                }
            }
        }
    }

    public function createInvoicesResellerPackage(){
        
        $resellers = $this->getResellersForInvoicesPackage();

        if(count($resellers)>0){
            
            foreach ($resellers as $resellerRow) {             
                
                $items = $this->getPackageItems($resellerRow);
                
                if (!empty($items)) {
                    $total = $this->getItemsTotal($items);
                    //issue invoice only if it's ammount bigger then 0
                    if($total > 0){
                        $invoiceId = $this->issueInvoice($resellerRow['reseller_id'],$items,"false",0,1,1);
                        $this->setPendingStatusToInvoice($invoiceId);
                        $this->setNextInvoiseDateResellerPackage($resellerRow);
                    }
                }
            }
        }
    }
    public function setNextInvoiseDateResellerPackage($resellerRow){
        $addMonth = 12/$resellerRow["rp_charge_freq"];
        $this->db->execute("UPDATE tbl_reseller_connect_program 
                            SET rcp_next_invoice_date=DATE_ADD(NOW(), INTERVAL $addMonth MONTH)
                            WHERE rcp_id={$resellerRow["rcp_id"]}");        
    }

    public function createInvoicesStartReseller(){
        
        $resellers = $this->getResellersForOneTimeStartInvoices();//List of resellers that activated and have downpayment agreed for the first charge. reseller_billed_price > 0

        if(count($resellers)>0){
            
            foreach ($resellers as $resellerRow) {             
                
                $items = $this->getResellerStartItem($resellerRow);
                                
                if (!empty($items)) {                    
                    $total = $this->getItemsTotal($items);
                    //issue invoice only if it's ammount bigger then 0
                    if($total > 0){
                        $invoiceId = $this->issueInvoice($resellerRow['reseller_id'],$items,"false",0,1,1);
                        $this->setPendingStatusToInvoice($invoiceId);
                        $this->db->execute("UPDATE tbl_resellers SET reseller_billed=1 WHERE reseller_id = {$resellerRow['reseller_id']}");
                    }                    
                }
            }
        }
    }

    public function getNextBillingDate($biz_id=0){
        return $this->db->getVal("select max(billDate) from (
                                    SELECT DATE_ADD(LAST_DAY(NOW()), INTERVAL 1 DAY) billDate
                                    union
                                    SELECT min(tf_trial_date) billDate FROM tbl_transaction_feature WHERE tf_biz_id=$biz_id and tf_end_time is null
                                    ) t1");
    }

    public function getNextBillingDateForBiz($biz_id=0){
        return $this->db->getVal("SELECT tf_next_billing_date FROM tbl_transaction_feature WHERE tf_biz_id=$biz_id and tf_end_time is null AND tf_entity='plan'");
    }

    public function getCurrentBillingDate($biz_id=0){
        return $this->db->getVal("select max(billDate) from (
                                    SELECT DATE_SUB(LAST_DAY(NOW()),INTERVAL DAY(LAST_DAY(NOW()))- 1 DAY) billDate
                                    union
                                    SELECT min(tf_trial_date) billDate FROM tbl_transaction_feature WHERE tf_biz_id=$biz_id and tf_end_time is null
                                    ) t1");
    }

    public function getAppsListForInvoicesStandart(){

        $bizs = $this->db->getTable("SELECT * 
                                        FROM tbl_transaction_feature,tbl_biz,tbl_owners,tbl_plans 
                                        WHERE biz_id=tf_biz_id 
										AND biz_owner_id = owner_id
                                        AND tf_entity_id=pl_id
                                        AND biz_paymentMethodToken <> 'free'
                                        AND biz_paymentMethodToken <> ''
                                        AND tf_end_time IS NULL
                                        AND tf_not_for_invoice = 0
                                        AND tf_client_id = 0
										AND tf_next_billing_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')
										AND tf_entity = 'plan'");
        return $bizs;
    }

    public function getAppsListForInvoicesTrial(){

        $bizs = $this->db->getTable("SELECT distinct tf_biz_id as biz_id 
                                        FROM tbl_transaction_feature,tbl_biz,tbl_owners 
                                        WHERE biz_id=tf_biz_id 
										AND owner_id=biz_owner_id
										AND owner_account_id <> 0
                                        AND biz_paymentMethodToken <> 'free'
                                        AND tf_end_time IS NULL
                                        and tf_not_for_invoice = 0
                                        AND tf_trial_date <= DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 DAY),'%Y-%m-%d')");
        return $bizs;
    }

    public function getResellersForInvoices(){

        $resellers = $this->db->getTable("SELECT * 
                                        FROM tbl_resellers,tbl_resellers_programs 
                                        WHERE reseller_program = rp_id
                                        AND reseller_paymentMethodToken <> '' 
                                        AND reseller_paymentMethodToken <> 'free'
                                        AND reseller_program <> 0
                                        AND reseller_is_demo = 0
                                        AND reseller_deactivated = 0
                                        AND rp_model_type='pay_per_app'");
        return $resellers;
    }

    public function getResellersForInvoicesPackage(){

        $resellers = $this->db->getTable("SELECT * 
                                        FROM tbl_resellers,tbl_resellers_programs,tbl_reseller_connect_program 
                                        WHERE reseller_program = rp_id
										AND rcp_reseller_id = reseller_id
										AND rcp_end_time is NULL
                                        AND reseller_paymentMethodToken <> '' 
                                        AND reseller_paymentMethodToken <> 'free'
                                        AND reseller_program <> 0
                                        AND reseller_is_demo = 0
                                        AND reseller_deactivated = 0
                                        AND rp_model_type='package'
                                        AND (rcp_next_invoice_date IS NULL OR rcp_next_invoice_date <= DATE(NOW()))");
        return $resellers;
    }

    public function getResellersForOneTimeStartInvoices(){

        $resellers = $this->db->getTable("SELECT * 
                                        FROM tbl_resellers,tbl_resellers_programs 
                                        WHERE reseller_program = rp_id
                                        AND reseller_paymentMethodToken <> '' 
                                        AND reseller_paymentMethodToken <> 'free'
                                        AND reseller_program <> 0
                                        AND reseller_is_demo = 0
                                        AND reseller_deactivated = 0
                                        AND reseller_billed=0
                                        AND reseller_billed_price <> 0"); //reseller_billed_price -> the initial payment (down payment) of the account)
        return $resellers;
    }

    public function issueInvoice($bizID,$items,$markAsPayd="false",$draft=0,$isReseller = 0,$autoInvoice = 0){
        
        if($isReseller == 0){
            $invoice_id = $this->db->execute("INSERT INTO tbl_trans_invoice SET  tri_month = MONTH(CURRENT_DATE),
                                                                tri_year = YEAR(CURRENT_DATE),
                                                                tri_auto_invoice=$autoInvoice,
                                                                tri_biz_id= $bizID,
                                                                tri_reseller_id = 0");
            $ownerCurrency = appManager::getOwnerCurrency($bizID);
        }
        else{
            $invoice_id = $this->db->execute("INSERT INTO tbl_trans_invoice SET  tri_month = MONTH(CURRENT_DATE),
                                                                tri_year = YEAR(CURRENT_DATE),
                                                                tri_auto_invoice = $autoInvoice,
                                                                tri_reseller_id = $bizID,
                                                                tri_biz_id = 0");
            $ownerCurrency = appManager::getResellerCurrency($bizID);
        }
        
        $total = 0;

        foreach ($items as $item) {
            
            $days = ($item["days"] == "") ? 0 : $item["days"];
            $itmName = addslashes($item["name"]);
            
            $sql = "INSERT INTO tbl_transaction_items SET tri_invoice_id = $invoice_id,
                                                                 tri_name='$itmName',
                                                                 tri_mod_id={$item["id"]},
                                                                 tri_quantity='{$item["itemQuantity"]}',
																 tri_days = $days,
                                                                 tri_monthly_price={$item["monthly_price"]},
                                                                 tri_price={$item["price"]},
																 tri_image = '{$item["pic"]}',
                                                                 tri_entity = '{$item["entity"]}'";    
            
            
            $this->db->execute($sql);
            $quantity = ($item["itemQuantity"] == "" || $item["itemQuantity"] == "0") ? 1 : $item["itemQuantity"];

            $total += ($item['price'] * $quantity);
        }
        
        $payd = "0";
        if($total == 0 || $markAsPayd === "true")
        {
            $payd = "1";
        }

        $this->db->execute("UPDATE tbl_trans_invoice SET tri_currency='$ownerCurrency', tri_total_amount = $total,tri_payd=$payd,tri_draft=$draft WHERE tri_id=$invoice_id");        
        return $invoice_id;
    }

    public function createInvoicePDF($invoiceID){

        $mpdf = new mPDF();
        $invoice = $this->getRowForInvoiceClient($invoiceID);

        $invoice['ext_tri_id'] = $invoice['tri_id'] + 100000;

        $sql = "SELECT * FROM tbl_biz,tbl_owners,tbl_account
                    WHERE biz_id = {$invoice['tri_biz_id']}
                    AND biz_owner_id = owner_id
                    AND owner_account_id = ac_id";



        $inbizData = $this->db->getRow($sql);

        $ownerCurrency = appManager::getOwnerCurrencySymbol($invoice['tri_biz_id']);

        $image_storage = imageStorage::getStorageAdapter();

        $fname = "invoice_".$invoice['ext_tri_id'].".pdf";
        
        if($image_storage->exists("invoices/".$inbizData['biz_id']."/".$fname)){
            return IMAGE_STORAGE_PATH."invoices/".$inbizData['biz_id']."/".$fname;
            
        }

        $invoiceDetails = $this->db->getRow("SELECT * FROM tbl_trans_invoice,tbl_transaction 
                                            WHERE tri_id = tr_invoice
                                            AND tri_id = {$invoice["tri_id"]}
                                            AND tr_type IN ('CHARGE','RECURRING')");

      // $invoiceStatus = "";
       // $invoicePayment = "";
        //if (isset($invoiceDetails)){
            $invoiceStatus = strtoupper($invoiceDetails["tri_status"]);
            $invoicePayment = $invoiceDetails["tr_vendor"]." Ending: XX-".$invoiceDetails["tr_4_digits"];
        //}

        $sql = "SELECT * FROM tbl_transaction_items WHERE tri_invoice_id = $invoiceID";

        $invoiceItems = $this->db->getTable($sql);

        $itemssHtml = "";

        $total = 0;

        foreach ($invoiceItems as $entry)
        {
            $total += $entry['tri_quantity'] * $entry['tri_monthly_price'];
        	$itemssHtml .= "<div style=\"background-color:#fff;padding:15px 40px;width:90%;height:30px;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#333333;\">
                    <div style=\"float:left;width:60%;text-align:left;\">
                        {$entry['tri_name']}
                    </div>
                    <div style=\"float:left;width:39%;text-align:center;\">
                        <div style=\"float:left;width:33%;\">
                            {$entry['tri_quantity']}
                        </div>
                        <div style=\"float:left;width:33%;text-align:left;\">
                            $ownerCurrency{$entry['tri_monthly_price']}
                        </div>
                        <div style=\"float:left;width:33%;text-align:right;\">
                            $ownerCurrency$total
                        </div>
                    </div>
                </div>";
        }
        
        $signature = utilityManager::encryptIt($invoiceID.'_'.$inbizData['biz_id']);
        $now = utilityManager::getFormattedTime($invoice['tri_time'],false,false);

        $html = "<html style=\"width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:0;\">
     <head> 
  <meta charset=\"UTF-8\"> 
  <meta content=\"width=device-width, initial-scale=1\" name=\"viewport\"> 
  <meta name=\"x-apple-disable-message-reformatting\"> 
  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> 
  <meta content=\"telephone=no\" name=\"format-detection\"> 
  <title>PDF Invoice</title> 
  <!--[if (mso 16)]>
    <style type=\"text/css\">
    a {text-decoration: none;}
    </style>
    <![endif]--> 
  <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
  <!--[if !mso]><!-- --> 
  <link href=\"https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\" rel=\"stylesheet\"> 
  <!--<![endif]--> 
 
  <style>

#outlook a {
	padding: 0;
}
.ExternalClass {
	width: 100%;
}
.ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
	line-height: 100%;
}
.es-button {
	mso-style-priority: 100 !important;
	text-decoration: none !important;
}
a[x-apple-data-detectors] {
	color: inherit !important;
	text-decoration: none !important;
	font-size: inherit !important;
	font-family: inherit !important;
	font-weight: inherit !important;
	line-height: inherit !important;
}


</style> 
 </head>
    <body style=\"width:100%;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;padding:0;Margin:auto;text-align:center;background-color:#E7E7E7;\" >
        <div style=\"width:800px;margin:auto\">
            <div style=\"Margin:0;padding:5%;background-color:#F7F7F7;display:inline-block;width:90%;margin:auto;\">
                <div style=\"float:left;text-align:left;width:40%;\">
                    <a target=\"_blank\" style=\"-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:#000000;height:250px;\"> <img src=\"https://eklc.stripocdn.email/content/guids/CABINET_7ca0800c05226c8a3d783e8b173cb023/images/77181526214940560.png\" alt=\"\" style=\"display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;\" width=\"90\"> </a> 
                </div>
                <div style=\"float:right;text-align:right;width:40%\">
                    <p><strong>9 Parkside</strong></p><p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:6px;color:#000000;\"><strong>Stoneybatter Park, Wexford, Ireland</strong></p>
                    <p style=\"Margin-top:20px;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;color:#000000;\">support@bobile.com</p><p style=\"Margin-top:-10px;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;color:#000000;\">www.bobile.com</p><p style=\"Margin-top:-10px;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;color:#000000;\">514869973</p>
                </div>
            </div>
            <div style=\"Margin:0;padding:5%;background-color:#fff;display:inline-block;width:90%;margin:auto;\">
                <div style=\"float:left;text-align:left;width:33%;\">
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#939394;\">BILLED TO</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">{$inbizData['ac_name']}</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">{$inbizData['biz_short_name']}</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">{$inbizData['ac_username']}</p>
                </div>
                <div style=\"float:left;text-align:center;width:33%;\">
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#939394;\">INVOICE NUMBER</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">{$invoice['ext_tri_id']}</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">$invoiceStatus</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">$invoicePayment</p>

                </div>
                <div style=\"float:left;text-align:right;width:33%;\">
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#939394;\">DATE OF ISSUE</p>
                    <p style=\"Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">$now</p>

                </div>
            </div>
            <div style=\"background-color:#fff;padding-bottom:50px;\">
                <div style=\"Margin:0;padding:10px 5%;background-color:#1875d2;display:inline-block;width:90%;margin:auto;color:#fff;font-size:12px;\">
                    <div style=\"float:left;width:60%;text-align:left;\">
                        <strong>SERVICE</strong>
                    </div>
                    <div style=\"float:left;width:40%;text-align:center;\">
                        <div style=\"float:left;width:33%;\">
                            <strong>QTY</strong>
                        </div>
                        <div style=\"float:left;width:33%;text-align:left;\">
                            <strong>PRICE</strong>
                        </div>
                        <div style=\"float:left;width:33%;text-align:right;\">
                            <strong>SUBTOTAL</strong>
                        </div>
                    </div>
                </div>
                $itemssHtml
                <div>
                    <div style=\"Margin-top:50px;padding:10px 5%;background-color:#1875d2;width:30%;margin-left:69%;color:#fff;font-size:12px;float:right;\">
                        <div style=\"float:left;width:50%;text-align:left;\">
                            <strong>TOTAL</strong>
                        </div>
                         <div style=\"float:left;width:39%;text-align:right;\">
                            <strong>$ownerCurrency{$invoice['tri_total_amount']}</strong>
                        </div>
                    </div>

                </div>
                <div style=\"margin-top:50px;padding:0 40px;text-align:left;font-size:14px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">
                    THANK YOU FOR YOUR BUSINESS
                </div>
                <div style=\"margin-top:10px;padding:0 40px;text-align:left;font-size:12px;font-family:'open sans', 'helvetica neue', helvetica, arial, sans-serif;line-height:150%;color:#000000;\">
                    Signed by: $signature
                </div>
            </div>
        </div>        
    </body>
</html>";

        
        $mpdf->WriteHTML($html);
        $mpdf->Output($fname);
       
        


        $path = $image_storage->uploadFile(ROOT_PATH.$fname,"invoices/".$inbizData['biz_id']."/".$fname);

        unlink(ROOT_PATH.$fname);

        return $path;
    }

    public function preparePlanInvoiceItem($item){

        $item["pic"] = "";
        $item["days"] = 0; //legacy - used for refund calculation - not in use
        $item["type"] = 'plan';
        $item["entity"] = 'plan';
        $item["itemQuantity"] = 1;
        $item["id"] = $item["pl_id"];
        $item["name"] = $item["pl_name"];

        if(($item["id"] >=31 && $item["id"] <= 36) || ($item["id"] >=62 && $item["id"] <= 65)){                
            $discount = $this->getDiscount($item["owner_country"]);
            $discount = $discount == "" ? 0 : $discount;
            
            $devider = 1;
            if($item["pl_interval"] == "1 year") $devider = 12;
            
            $price = ceil($item["tf_price"] / $devider - $item["tf_price"] / $devider * $discount / 100) * $devider ;
            $monthlyPrice = ceil($item["tf_price"] / $devider - $item["tf_price"] / $devider * $discount / 100) * $devider;
        }else{
            $price = $item["tf_price"];
            $monthlyPrice = $item["tf_price"];
        }
        $item["monthly_price"] = $monthlyPrice;
        $item["price"] = $price;

        return $item;
    }

    public function getItemsTotal($items){

        $total = 0;

        if(count($items)>0){
            foreach ($items as $item) {
                $total += $item['price'];
            }
        }
        return $total;
    }

    public function getTrialDays($start_date){       

        $end_date = date('Y-m-t',strtotime($start_date));
        return $this->db->getVal("SELECT CEIL(SUM(TIMESTAMPDIFF(SECOND, '$start_date', '$end_date') / 86400)) as days");
    }

    public function getDiscount($country_id)
    {
        $discount = $this->db->getVal("SELECT country_discount FROM tbl_countries WHERE country_id = $country_id");
        return $discount == "" ? 0 : $discount;
    }

    public function getResellerAppsCount($reseller_id){
        return $this->db->getVal("SELECT count(biz_id) 
                                    FROM tbl_biz 
                                    WHERE biz_isdeleted=0 
                                    AND biz_pcas_demo=0 
                                    AND biz_owner_id = (SELECT owner_id FROM tbl_owners WHERE owner_reseller_id=$reseller_id)");
    }

    public function getResellerMarketsCount($reseller_id){

        return $this->db->getVal("select count(mrkt_id) from tbl_markets where mrkt_reseller_id=$reseller_id AND mrkt_is_deleted = 0");
    }

    // get all the features that were used by the buziness for some month
    // start_date is the first day of the needed month
    public function getMonthlyModules($start_date, $biz_id, $sub_cat_id,$trial=0)
    {
        $start_date = date('Y-m-01 00:00:00',strtotime($start_date));
        $end_date = date('Y-m-t 23:59:59',strtotime($start_date));
        
        $addToSql = "AND ttf.tf_trial_date IS NULL";
        if($trial == 1){
            $addToSql = "AND ttf.tf_trial_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')";
        }else if($trial == 2){
            $addToSql="";
        }

        $q ="SELECT tm.mod_id as id,
                    tms.msc_label as name, 
                    tm.mod_pic as pic,
                    '$start_date' as start_date, 
                    '$end_date' as end_date,
                    CEIL(SUM(TIMESTAMPDIFF(SECOND, '$start_date', '$end_date') / 86400)) as days,
                    ttf.tf_price as price,
                    ttf.tf_price as monthly_price,
                    ttf.tf_trial_date,
                    'module' as type,
                    'module' as entity,
                    '1' as itemQuantity
                FROM tbl_transaction_feature ttf 
                INNER JOIN tbl_modules tm ON ttf.tf_entity_id = tm.mod_id 
                INNER JOIN tbl_mod_subcat tms ON (tm.mod_id = tms.msc_mod_id AND tms.msc_sub_id = $sub_cat_id)
                WHERE ttf.tf_biz_id = $biz_id
                AND ttf.tf_end_time IS NULL
                AND ttf.tf_entity  = 'module'
				AND ttf.tf_start_time < '$start_date'
                $addToSql
                GROUP BY tm.mod_name";
        // echo "$end_date - $q";
        return $this->db->getTable($q);
    }

    // get all the functions that were used by the buziness for some month
    // start_date is the first day of the needed month
    public function getMonthlyFunctions($start_date, $biz_id,$trial=0)
    {
        $start_date = date('Y-m-01 00:00:00',strtotime($start_date));
        $end_date = date('Y-m-t 23:59:59',strtotime($start_date));
        
        $addToSql = "AND ttf.tf_trial_date IS NULL";
        if($trial == 1){
            $addToSql = "AND ttf.tf_trial_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')";
        }else if($trial == 2){
            $addToSql="";
        }

        $q ="SELECT tf.fu_id as id,
                    tf.fu_default_label as name, 
                    tf.fu_image as pic,
                    '$start_date' as start_date, 
                    '$end_date' as end_date,
                    CEIL(SUM(TIMESTAMPDIFF(SECOND, '$start_date', '$end_date') / 86400)) as days,
                    ttf.tf_price as price,
                    ttf.tf_price as monthly_price,
                    ttf.tf_trial_date,
                    'function' as type,
                    'function' as entity,
                    '1' as itemQuantity
                FROM tbl_transaction_feature ttf 
                INNER JOIN tbl_functions tf ON ttf.tf_entity_id = tf.fu_id
                WHERE ttf.tf_biz_id = $biz_id
                AND ttf.tf_end_time IS NULL
                AND ttf.tf_entity = 'function'
				AND ttf.tf_start_time < '$start_date'
                $addToSql
                GROUP BY tf.fu_id";
        
        return $this->db->getTable($q);
    }

    // get all the appstores that were used by the buziness for some month
    // start_date is the first day of the needed month
    public function getMonthlyAppstores($start_date, $biz_id,$trial=0)
    {
        $start_date = date('Y-m-01 00:00:00',strtotime($start_date));
        $end_date = date('Y-m-t 23:59:59',strtotime($start_date));
        
        $addToSql = "AND ttf.tf_trial_date IS NULL";
        if($trial == 1){
            $addToSql = "AND ttf.tf_trial_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')";
        }else if($trial == 2){
            $addToSql="";
        }

        $q ="SELECT ta.id,
                    ta.name, 
                    ta.image as pic,
                    '$start_date' as start_date, 
                    '$end_date' as end_date,
                    CEIL(SUM(TIMESTAMPDIFF(SECOND, '$start_date', '$end_date') / 86400)) as days,
                    ROUND(ttf.tf_price,2) as price,
                    ttf.tf_price as monthly_price,
                    ttf.tf_trial_date,
                    'appstore' as type,
                    'appstore' as entity,
                    '1' as itemQuantity
                FROM tbl_transaction_feature ttf 
                INNER JOIN tbl_appstores ta ON ttf.tf_entity_id = ta.id
                WHERE ttf.tf_biz_id = $biz_id
                AND ttf.tf_end_time IS NULL
                AND ttf.tf_entity = 'appstore'
				AND ttf.tf_start_time < '$start_date'
                $addToSql
                GROUP BY ta.id";

        return $this->db->getTable($q);
    }

    // get all plans that were used by the biz on some month
    public function getMonthlyPlans($start_date, $biz_id,$trial=0)
    {
        $start_date = date('Y-m-01 00:00:00',strtotime($start_date));
        $end_date = date('Y-m-t 23:59:59',strtotime($start_date));

        $addToSql = "AND ttf.tf_trial_date IS NULL";
        if($trial == 1){
            $addToSql = "AND ttf.tf_trial_date <= DATE_FORMAT(NOW(),'%Y-%m-%d')";
        }else if($trial == 2){
            $addToSql="";
        }
        
        $q = "select * from (SELECT tp.pl_name as name,
                        tp.pl_id as id,
                        '$start_date' as start_date, 
                        '$end_date' as end_date,
                        CEIL(SUM(TIMESTAMPDIFF(SECOND, '$start_date', '$end_date') / 86400)) as days,
                        tf_price as monthly_price,
                        tf_price as price,
                        tp.pl_anual_freq_name as period,
                        tp.pl_billing_type,
                        tp.pl_interval,
                        tp.pl_entity_id,
                        tp.pl_anual_freq_times,
                        tp.pl_anual_freq_type,
                        ttf.tf_trial_date,
                        'plan' as type,
                        'plan' as entity,
                        '1' as itemQuantity
                             FROM tbl_transaction_feature ttf
                             INNER JOIN tbl_plans tp ON ttf.tf_entity_id = tp.pl_id
                             WHERE ttf.tf_biz_id = $biz_id
                             AND ttf.tf_entity = 'plan'
                             AND tp.pl_entity = 'bundle'
                             AND ttf.tf_end_time IS NULL
                             $addToSql
                             GROUP BY ttf.tf_entity_id) t1 WHERE price > 0";

     

        return $this->db->getTable($q);
    }

    public function getMonthlyPlansPCAS($resellerRow){

        $apps = $this->getResellerAppsCount($resellerRow["reseller_id"]);       
        $markets = $this->getResellerMarketsCount($resellerRow["reseller_id"]);

        $items = array();
        $itemObject = array();

        if($apps > 0){
            $itemObject["price"] = $resellerRow["reseller_app_price"];
            $itemObject["monthly_price"] = $resellerRow["reseller_app_price"];
            $itemObject["name"] = $resellerRow["rp_name"];
            $itemObject["itemQuantity"] = $apps;
            $itemObject["id"] = $resellerRow["rp_id"];
            $itemObject["entity"] = "resseler-apps";
            $itemObject["days"] = "0";
            $itemObject["pic"] = "";
            array_push($items,$itemObject);
        }

        if($markets > 0 && $resellerRow["rp_is_pcas"] == "1"){
            $itemObject["price"] = $resellerRow["reseller_pcas_price"];
            $itemObject["monthly_price"] = $resellerRow["reseller_pcas_price"];
            $itemObject["name"] = $resellerRow["rp_name"]." Instances";
            $itemObject["itemQuantity"] = $markets;
            $itemObject["id"] = $resellerRow["rp_id"];
            $itemObject["entity"] = "resseler-market";
            $itemObject["days"] = "0";
            $itemObject["pic"] = "";
            array_push($items,$itemObject);
        }

        return $items;
    }

    public function getPackageItems($resellerRow){

        $items = array();
        $itemObject = array();

        $itemObject["price"] = $resellerRow["rcp_price"];
        $itemObject["monthly_price"] = $resellerRow["rcp_price"];
        $itemObject["name"] = $resellerRow["rp_name"]." - ".$resellerRow["rcp_app_amount"]." Apps";
        $itemObject["itemQuantity"] = 1;
        $itemObject["id"] = $resellerRow["rp_id"];
        $itemObject["entity"] = "resseler-apps";
        $itemObject["days"] = "0";
        $itemObject["pic"] = "";
        array_push($items,$itemObject);

        return $items;
    }

    public function getResellerStartItem($resellerRow){

        $items = array();
        $itemObject = array();

        $itemObject["price"] = $resellerRow["reseller_billed_price"];
        $itemObject["monthly_price"] = $resellerRow["reseller_billed_price"];
        $itemObject["name"] = "Initial payment";
        $itemObject["itemQuantity"] = 1;
        $itemObject["id"] = $resellerRow["rp_id"];
        $itemObject["entity"] = "resseler-start";
        $itemObject["days"] = "0";
        $itemObject["pic"] = "";
        array_push($items,$itemObject);

        return $items;
    }

    // *************************************************************************************************************************************************** //
  

    // ***************************************************************** RECURRING ********************************************************************** //

    public function lastRequringDate(){
        $response = $this->db->getVal("SELECT COUNT(stam_recuring_time) FROM tbl_stam 
                                                     WHERE YEAR(stam_recuring_time) = YEAR(CURRENT_DATE)
                                                     AND MONTH(stam_recuring_time) = MONTH(CURRENT_DATE)
                                                     AND DAY(stam_recuring_time) = DAY(CURRENT_DATE)");
        if($response == "1"){
            $this->db->execute("UPDATE tbl_stam SET stam_recuring_time = DATE_ADD(stam_recuring_time, INTERVAL 1 DAY)");
        }
        return $response == "1";
    }

    //notJ5 pending
    public function getOpenInvoicesIDs(){ 

        return $this->db->getTable("SELECT tri_id
                                        FROM tbl_biz, tbl_owners,tbl_trans_invoice,tbl_transaction_feature
                                        WHERE  tri_biz_id=biz_id
                                        and biz_owner_id = owner_id
                                        and tri_biz_id=tf_biz_id
                                        and tf_end_time is null
                                        and (tf_trial_date is null or tf_trial_date < now())
                                        and biz_paymentMethodToken <> 'free'
                                        AND owner_stripe_cust_id != ''
                                        AND EXISTS (SELECT ops_id vol FROM tbl_owner_payment_sources WHERE ops_owner_id = owner_id)
                                        and tri_payd=0
                                        and tri_draft=0
                                        and tri_collection=0
                                        and tri_total_amount >= 1
                                        and date(tri_last_try) <> date(now())
                                        and tri_status = 'pending'
                                        order by tri_id");
    }

    //notJ5 outstanding
    public function getOpenInvoicesIDsOutstanding(){ 

        return $this->db->getTable("SELECT tri_id
                                        FROM tbl_biz, tbl_owners,tbl_trans_invoice
                                        WHERE  tri_biz_id=biz_id
                                        and biz_owner_id = owner_id
                                        and biz_paymentMethodToken <> 'free'
                                        AND owner_stripe_cust_id != ''
                                        AND EXISTS (SELECT ops_id vol FROM tbl_owner_payment_sources WHERE ops_owner_id = owner_id)
                                        and tri_payd=0
                                        and tri_draft=0
                                        and tri_collection=0
                                        and tri_total_amount >= 1
                                        and date(tri_last_try) < DATE_ADD(NOW(), INTERVAL -5 DAY)
                                        and tri_status = 'outstanding'
                                        order by tri_id");
    }

    public function getOpenInvoicesResellerIDs(){

        return $this->db->getTable("SELECT tri_id
                                        FROM tbl_resellers,tbl_owners,tbl_trans_invoice
                                        WHERE  tri_reseller_id=reseller_id
                                        AND reseller_id = owner_reseller_id
                                        and reseller_paymentMethodToken <> 'free'
                                        and reseller_paymentMethodToken <> ''
                                        AND (reseller_deactivated=0 or (reseller_deactivated=1 AND DATE_ADD(reseller_deactivate_date, INTERVAL 30 DAY) > NOW()))
                                        AND owner_stripe_cust_id != ''
                                        AND EXISTS (SELECT ops_id vol FROM tbl_owner_payment_sources WHERE ops_owner_id = owner_id)
                                        and tri_payd=0
                                        and tri_draft=0
                                        and tri_collection=0
                                        and tri_total_amount >= 1
                                        and date(tri_last_try) <> date(now())
                                        and tri_status = 'pending'
                                        order by tri_id desc");
    }

    public function getOpenInvoicesResellerIDsOutstanding(){

        return $this->db->getTable("SELECT tri_id
                                        FROM tbl_resellers,tbl_owners,tbl_trans_invoice
                                        WHERE  tri_reseller_id=reseller_id
                                        AND reseller_id = owner_reseller_id
                                        and reseller_paymentMethodToken <> 'free'
                                        and reseller_paymentMethodToken <> ''
                                        AND (reseller_deactivated=0 or (reseller_deactivated=1 AND DATE_ADD(reseller_deactivate_date, INTERVAL 30 DAY) > NOW()))
                                        AND owner_stripe_cust_id != ''
                                        AND EXISTS (SELECT ops_id vol FROM tbl_owner_payment_sources WHERE ops_owner_id = owner_id)
                                        and tri_payd=0
                                        and tri_draft=0
                                        and tri_collection=0
                                        and tri_total_amount >= 1
                                        and date(tri_last_try) < DATE_ADD(NOW(), INTERVAL -5 DAY)
                                        and tri_status = 'outstanding'
                                        order by tri_id desc");
    }

    public function updateLastInvoiceTry($invoceID){
        $this->db->execute("UPDATE tbl_trans_invoice SET
                                tri_last_try=now()
                                WHERE tri_id=$invoceID");
    }

    public function getTrialBizIdsForRequringEmail(){ 

        return $this->db->getTable("SELECT DISTINCT biz_id
                                        FROM tbl_biz, tbl_owners,tbl_trans_invoice,tbl_transaction_feature
                                        WHERE tri_biz_id=biz_id
                                        and biz_owner_id = owner_id
                                        and tri_biz_id=tf_biz_id
                                        and tf_end_time is null
                                        and tf_trial_date is not null
                                        and date(tf_next_billing_date) = date_add(CURRENT_DATE() ,INTERVAL 2 DAY)
                                        and biz_paymentMethodToken <> 'free'
                                        AND owner_stripe_cust_id != ''
                                        AND EXISTS (SELECT ops_id vol FROM tbl_owner_payment_sources WHERE ops_owner_id = owner_id)
                                        and tri_payd=0
                                        and tri_draft=0
                                        and tri_collection=0
                                        and tri_total_amount >= 1
                                        and tri_status = 'pending'
                                        order by tri_id");
    } 

    public function getBizIdsForRequringEmail(){ 

        $bizTable = $this->db->getTable("SELECT DISTINCT biz_id
                                        FROM tbl_biz, tbl_owners,tbl_transaction_feature
                                        LEFT JOIN tbl_plans on pl_id = tf_entity_id and tf_entity='plan'
                                        WHERE tf_biz_id=biz_id
                                        and biz_owner_id = owner_id
                                        and tf_end_time is null
                                        and tf_trial_date is null
                                        and date(tf_next_billing_date) = date_add(CURRENT_DATE() ,INTERVAL 2 DAY)
                                        and biz_paymentMethodToken <> 'free'
                                        AND owner_stripe_cust_id != ''
                                        AND tf_not_for_invoice = 0
                                        AND EXISTS (SELECT ops_id vol FROM tbl_owner_payment_sources WHERE ops_owner_id = owner_id)");

        return $bizTable;
    }


    // *************************************************************************************************************************************************** //

    //dont delete - old invoice creation - not in use for now
    public function createInvoicesResellerOld(){
        
        $date_f = $this->getNextBillingDate();
        
        $bizs = $this->db->getTable("SELECT distinct tf_reseller_id as reseller_id 
                                        FROM tbl_transaction_feature,tbl_resellers 
                                        WHERE reseller_id=tf_reseller_id 
                                        AND reseller_paymentMethodToken <> '' AND reseller_paymentMethodToken <> 'free'
                                        AND tf_end_time IS NULL");
        if(count($bizs)>0){
            
            $entitiesManager = new entitiesManager();
            
            foreach ($bizs as $biz) {
                $bizRow = $this->db->getRow("SELECT * FROM tbl_resellers INNER JOIN tbl_owners ON owner_reseller_id = reseller_id WHERE reseller_id = {$biz['reseller_id']}");
                $biz_paymentMethodToken = $bizRow["reseller_paymentMethodToken"];              
                
                if($biz_paymentMethodToken != "free" && $biz_paymentMethodToken != ""){    

                    $items = $this->getMonthlyPlansResellers($date_f, $biz['reseller_id']);
                    
                    if (!empty($items)) {
                       
                        $invoice_id = $this->db->execute("INSERT INTO tbl_trans_invoice SET  tri_month = MONTH(CURRENT_DATE),
                                                                tri_year = YEAR(CURRENT_DATE),
                                                                tri_auto_invoice=1,
                                                                tri_reseller_id= " . $biz['reseller_id']);

                        $total = 0;

                        foreach ($items as $item) {
                            
                            $refund = 0;
                            $days = (!isset($item["days"]) || $item["days"] == "") ? 0 : $item["days"];
                            $pic = (isset($item["pic"])) ? $item["pic"] : "";
                            
                            $price = $item["price"];
                            $monthlyPrice = $item["monthly_price"];
                            
                            
                            if($item["type"] == "plan"){
                                
                                $discount = ""; //$this->getDiscount($bizRow['owner_country']);
                                $discount = ($discount == "") ? 0 : $discount;
                                
                                $devider = 1;
                                if($item["pl_interval"] == "1 year") $devider = 12;
                                
                                $price = ceil($item["price"] / $devider - $item["price"] / $devider * $discount / 100) * $devider ;
                                $monthlyPrice = ceil($item["monthly_price"] / $devider - $item["monthly_price"] / $devider * $discount / 100) * $devider;
                            }
                            
                            $this->db->execute("INSERT INTO tbl_transaction_items SET tri_invoice_id = $invoice_id,
                                                                 tri_name='{$item["name"]}',
                                                                 tri_mod_id={$item["id"]},
                                                                 tri_quantity='{$item["quantity"]}',
                                                                 tri_days = $days,
                                                                 tri_monthly_price=$monthlyPrice,
                                                                 tri_price=$price,
                                                                 tri_image = '$pic',
                                                                 tri_entity='{$item["type"]}'");                                                       

                            if($item["pl_anual_freq_times"] == "12"){
                                $prevMonthRow = $this->db->getRow("SELECT tri_days, TRUNCATE((DAY(LAST_DAY(tri_time)) - tri_days) * tri_monthly_price / DAY(LAST_DAY(tri_time)),2) as refund FROM tbl_trans_invoice,tbl_transaction_items
                                                WHERE tbl_trans_invoice.tri_id=tri_invoice_id
                                                AND tri_auto_invoice=0
                                                AND tri_reseller_id={$biz['reseller_id']}
                                                AND tri_entity = '{$item["type"]}'
                                                AND tri_mod_id = {$item["id"]}
                                                and tri_draft=0
                                                AND YEAR(tri_time) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
                                                AND MONTH(tri_time) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)");

                                
                                if($prevMonthRow["refund"] != ""){
                                    $refund = floatval($prevMonthRow["refund"]);
                                    
                                    if($refund>0){
                                        $refund = -1 * abs($refund);                                       
                                        $this->db->execute("INSERT INTO tbl_transaction_items SET tri_invoice_id = $invoice_id,
                                                                 tri_name='Refund for {$item["name"]}',
                                                                 tri_mod_id={$item["id"]},
                                                                 tri_quantity='1',
                                                                 tri_days = {$prevMonthRow["tri_days"]},
                                                                 tri_monthly_price=$refund,
                                                                 tri_price=$refund,
                                                                 tri_image = '$pic',
                                                                 tri_entity='{$item["type"]}'"); 
                                    }
                                }
                            }


                            $entitiesManager->addTransactionLog($item["type"],$item["id"],$biz['biz_id']);
                            $total += $price + $refund;
                        }
                        
                        $payd = "0";
                        if($total == 0)
                        {
                            $payd = "1";
                        }
                        $this->db->execute("UPDATE tbl_trans_invoice SET tri_currency='{$bizRow["owner_currency_code"]}', tri_total_amount = $total,tri_payd=$payd WHERE tri_id= $invoice_id");
                    }
                }
            }
        }
    }

    public function getTrialDate($biz_id){
        
        return $this->db->getVal("SELECT min(tf_trial_date) billDate FROM tbl_transaction_feature WHERE tf_biz_id=$biz_id AND tf_end_time is null");
    }
    
    public function chargedThisMonth($bizID,$entityType,$entityID){
        return $this->db->getVal("select count(ttl_id) 
                                    from tbl_transaction_log 
                                    where date_format(ttl_time, '%Y-%m')=date_format(now(), '%Y-%m')
                                    and ttl_biz_id=$bizID
                                    and ttl_entity='$entityType'
                                    and ttl_mod_id=$entityID");
    }

    public function hasOpenInvoicesBiz($bizId){
        return $this->db->getTable("select tri_id,tri_collection from tbl_trans_invoice
                                    where tri_biz_id=$bizId
                                    and tri_payd=0
                                    and tri_draft=0
                                    and tri_total_amount > 0
                                    and tri_status = 'outstanding'
                                    order by tri_id desc 
                                    ");
    }
    
    public function hasOpenInvoicesReseller($rId){
        return $this->db->getTable("select tri_id,tri_collection from tbl_trans_invoice
                                    where tri_reseller_id=$rId
                                    and tri_payd=0
                                    and tri_draft=0
                                    and tri_total_amount > 0
                                    and tri_status = 'outstanding'
                                    order by tri_id desc 
                                    ");
    }

    public function getInvoiceItems($invoiceID){
        return $this->db->getTable("SELECT *
                                    FROM tbl_transaction_items 
                                    WHERE tri_invoice_id = $invoiceID");
    }

    public function getInvoiceMD5($invoiceID){
        $tr_id_md5 = addslashes($invoiceID);
        $sql = "SELECT * FROM tbl_trans_invoice WHERE md5(tri_id) = '$tr_id_md5'";

        return $this->db->getRow($sql);
    }

    public function paypal($POST){

        $type = isset($POST["txn_type"]) ? $POST["txn_type"] : $POST["payment_status"];
        $planArray = explode("_",$POST["item_number"]);
        $selectedID = $planArray[0];
        $uid = $POST["custom"];

        $paymentMethodToken = $POST['subscr_id'];
        $planRow = $this->db->getRow("select * from tbl_plans where pl_id=$selectedID");    
        $planType = $planRow['pl_type'];
        if($planType == "") $planType = "0";
        $captureCode = isset($POST['verify_sign']) ? $POST['verify_sign'] : "";
        $reconciliationId = isset($POST['payer_id']) ? $POST['payer_id'] : "";
        $amount = isset($POST['payment_gross']) ? $POST['payment_gross'] : "0";
        $currency = isset($POST['mc_currency']) ? $POST['mc_currency'] : "USD";
        $paymentMethodType = "PayPal";
        $ipAddress = utilityManager::get_real_IP();

        switch ($type) {
            case "subscr_payment":
                if($POST["payment_status"] == "Completed"){

                    $duplicate = $this->db->getVal("select count(tr_id) from tbl_transaction where tr_reference_Id='$paymentMethodToken' and tr_type='CHARGE'");
                    if($duplicate == "0") //CHARGE
                    {
                        $transID = dbExecute("insert into tbl_transaction set
                           tr_plan_id = $selectedID,
                           tr_plan_type = $planType,
                           tr_biz_id = $uid,
                           tr_reference_Id = '$paymentMethodToken',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = 'CHARGE',
                           tr_amount=$amount,
                           tr_currency='$currency',
                           tr_pay_method = 'PP',
                           tr_paymentMethodToken='$paymentMethodToken',
                           tr_paymentMethodType='$paymentMethodType',
                           tr_cvv = '',
                           tr_ip = '$ipAddress'
                          ");
                       

                        //foreach($jsona->item as $oneItem)
                        //{
                        //    $itmname= addslashes(stripslashes($oneItem->itemName));
                        //    $quantity=$oneItem->itemQuantity;
                        //    $price=$oneItem->itemPrice;
                            
                        //    dbExecute("insert into tbl_transaction_items set
                        //        tri_tr_id=$transID,
                        //        tri_name='$itmname',
                        //        tri_quantity='$quantity',
                        //        tri_price=$price
                        //        ");
                        //}
                    } 

                }
                break;
        }

    }

    // ***************************************************************** ZOOZ ***************************************************************** //

    public function refundPayment($invoiceId,$amount = 0){
        
        $controller = new ZController($this->zoozUniqueId, $this->zoozAppKey, $this->zoozIsSandbox);
        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $response = array();
        if($amount == 0){
            $amount = $invoiceRow['tri_total_amount'];
        }
        if($amount > $invoiceRow["tri_total_amount"]){
            $response["responseStatus"] = "-3";
        }
        
        if(($amount +  $invoiceRow["tri_amount_refunded"]) > $invoiceRow["tri_total_amount"]){
            $response["responseStatus"] = "-4";
        }
        
        if($invoiceRow["tri_payment_token"] != ""){
            $refundPaymentRequest = new RefundRequest($invoiceRow["tri_payment_token"],$amount);
            $refundResponse = $controller->sendRequest($refundPaymentRequest);
            
            $refundResponseArray = json_decode($refundResponse, true);
            
            $refundResponseObject = $refundResponseArray["responseObject"];
            $refundResponseStatus = $refundResponseArray["responseStatus"];
            
            $response["responseStatus"] = $refundResponseStatus;
            
            if($refundResponseStatus == 0){
                $this->refundInvoice($invoiceId,$amount);
            }else{
                $response["errorDescription"] = $refundResponseObject["errorDescription"];
                $response["responseErrorCode"] = $refundResponseObject["responseErrorCode"];
                $response["errorMessage"] = $refundResponseObject["errorMessage"];
            }                        
        }else{
            $response["responseStatus"] = "-2";
        }
        
        return $response;
    }

    // ***************************************************************** END ZOOZ FUNCTIONS ***************************************************************** //
   
    function saveOpenPaymentToken($invoiceId,$token){
        $sql = "update tbl_trans_invoice set tri_payment_token='$token' where tri_id=$invoiceId";
        $this->db->execute($sql);
    }
    
    function setPaymentMethodBiz($bizId,$data){

        $name = addslashes($data["card"]["name"]);

        $sql = "UPDATE tbl_biz SET biz_paymentMethodToken='stripe',biz_paymentMethodType='CreditCard',
                biz_cvv='555',biz_cc_last_digits='5555',biz_payment_name='$name'
                WHERE biz_id=$bizId AND biz_paymentMethodToken=''";
        
        $response = $this->db->execute($sql);        
        appManager::initAppData($bizId);
        
        return $response;
    }
    
    function loadPaymentMethodBiz($bizId){
        $sql = "SELECT biz_id,biz_paymentMethodToken,biz_paymentMethodType,biz_cvv,biz_cc_last_digits,biz_payment_name,
                biz_card_subtype,biz_card_country_code,biz_card_vendor,biz_card_type,biz_card_issuer,biz_card_level,biz_card_bin
                FROM tbl_biz WHERE biz_id=$bizId";
        return $this->db->getRow($sql);
    }
    
    function setPaymentMethodReseller($resellerId,$data){

        $name = addslashes($data["card"]["name"]);

        $sql = "UPDATE tbl_resellers SET reseller_paymentMethodToken='stripe',reseller_paymentMethodType='cc',
                reseller_cvv='555',reseller_lastFourDigits='5555',reseller_name_on_ccard='$name'
                WHERE reseller_id=$resellerId";
        $response = $this->db->execute($sql); 
        
        appManager::initWithOwnerID($data["owner_id"]);
        return $response;
    }
    
    function loadPaymentMethodReseller($resellerId){
        $sql = "SELECT reseller_id,reseller_paymentMethodToken,reseller_paymentMethodType,reseller_cvv,reseller_lastFourDigits,reseller_name_on_ccard,
                reseller_card_subtype,reseller_card_country_code,reseller_card_vendor,reseller_card_type,reseller_card_issuer,reseller_card_level,reseller_card_bin
                FROM tbl_resellers WHERE reseller_id=$resellerId";
        return $this->db->getRow($sql);
    }
    
    function saveAuthorization($invoiceId,$authCode){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='authorized', tri_auth_code='$authCode' WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }

    function loadJ5AuthorizedInvoice($bizId){

        return $this->db->getRow("SELECT * FROM tbl_trans_invoice WHERE tri_status in ('authorized','pending') AND tri_biz_id = $bizId");
    }
    
    function closeInvoice($invoiceId){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='paid', tri_payd='1', tri_last_try=NOW(),tri_draft=0 WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }
    
    function voidInvoice($invoiceId){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='voided', tri_last_try=NOW(), tri_collection=1 WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }

    function commitFailInvoice($invoiceId){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='commit_failure', tri_last_try=NOW() WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }
    
    function outstandInvoice($invoiceId){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='outstanding', tri_last_try=NOW() WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }

    function refundInvoice($invoiceId,$amount){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='refunded', tri_last_try=NOW(),tri_amount_refunded = tri_amount_refunded + $amount WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }
    
    function addTransactionBiz($invoiceId,$processorReferenceId,$captureCode,$reconciliationId,$type){
        
        $bizRow = $this->db->getRow("SELECT * FROM tbl_biz,tbl_trans_invoice,tbl_owners
                                      WHERE biz_id = tri_biz_id
                                      AND biz_owner_id = owner_id
                                      AND tri_id=$invoiceId");

        $chargeData = $this->stripe_retreiveChargeResultForInvoice($invoiceId);
        $last4 = (isset($chargeData["source"]["last4"])) ? $chargeData["source"]["last4"] : "";
        $vendor = (isset($chargeData["source"]["brand"])) ? $chargeData["source"]["brand"] : "";
        
        $transID = $this->db->execute("insert into tbl_transaction set
                           tr_invoice = $invoiceId,
                           tr_biz_id = {$bizRow["biz_id"]},
                           tr_reference_Id = '$processorReferenceId',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = '$type',
                           tr_amount={$bizRow["tri_total_amount"]},
                           tr_currency='{$bizRow["tri_currency"]}',
                           tr_pay_method = 'CC',
                           tr_paymentMethodToken='{$bizRow["biz_paymentMethodToken"]}',
                           tr_paymentMethodType='{$bizRow["biz_paymentMethodType"]}',
                           tr_cvv = '{$bizRow["biz_cvv"]}',
                           tr_4_digits = '$last4',
                            tr_vendor = '$vendor',
                           tr_ip = '{$bizRow["owner_ip"]}',
                           tr_country = '{$bizRow["biz_addr_country_id"]}',
                           tr_phone = '{$bizRow["biz_mobile_tele"]}',
                           tr_aff_id = {$bizRow["owner_aff_id"]},
                           tr_resellerid = {$bizRow["owner_reseller_id"]}
                          ");
        
        $this->db->execute("update tbl_transaction_items set
                                tri_tr_id=$transID
                                where tri_invoice_id=$invoiceId
                                ");

        $this->setAffiliateCommission($bizRow["owner_aff_id"],$invoiceId);
    }
    
    function addTransactionReseller($invoiceId,$processorReferenceId,$captureCode,$reconciliationId,$type){
        
        $bizRow = $this->db->getRow("SELECT * FROM tbl_resellers,tbl_trans_invoice,tbl_owners
                                      WHERE reseller_id = tri_reseller_id
                                      AND owner_reseller_id = reseller_id
                                      AND tri_id=$invoiceId");
        
        $chargeData = $this->stripe_retreiveChargeResultForInvoice($invoiceId);
        $last4 = (isset($chargeData["source"]["last4"])) ? $chargeData["source"]["last4"] : "";
        $vendor = (isset($chargeData["source"]["brand"])) ? $chargeData["source"]["brand"] : "";

        $transID = $this->db->execute("insert into tbl_transaction set
                           tr_invoice = $invoiceId,
                           tr_biz_id = 0,
                           tr_reference_Id = '$processorReferenceId',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = '$type',
                           tr_amount={$bizRow["tri_total_amount"]},
                           tr_currency='{$bizRow["tri_currency"]}',
                           tr_pay_method = 'CC',
                           tr_paymentMethodToken='{$bizRow["reseller_paymentMethodToken"]}',
                           tr_paymentMethodType='{$bizRow["reseller_paymentMethodType"]}',
                           tr_cvv = '{$bizRow["reseller_cvv"]}',
                           tr_4_digits = '$last4',
                           tr_vendor = '$vendor',
                           tr_ip = '{$bizRow["owner_ip"]}',
                           tr_country = '{$bizRow["reseller_country_id"]}',
                           tr_phone = '{$bizRow["reseller_phone"]}',
                           tr_aff_id = {$bizRow["owner_aff_id"]},
                           tr_resellerid = {$bizRow["owner_reseller_id"]}
                          ");
        
        $this->db->execute("update tbl_transaction_items set
                                tri_tr_id=$transID
                                where tri_invoice_id=$invoiceId
                                ");
    }

    function addTransactionBiz3D($invoiceId,$chargeObject,$type){
        
        $bizRow = $this->db->getRow("SELECT * FROM tbl_biz,tbl_trans_invoice,tbl_owners
                                      WHERE biz_id = tri_biz_id
                                      AND biz_owner_id = owner_id
                                      AND tri_id=$invoiceId");
        $card = $chargeObject["payment_method_details"]["card"];
        $last4 = (isset($card["last4"])) ? $card["last4"] : "";
        $vendor = (isset($card["brand"])) ? $card["brand"] : "";
        
        $transID = $this->db->execute("insert into tbl_transaction set
                           tr_invoice = $invoiceId,
                           tr_biz_id = {$bizRow["biz_id"]},
                           tr_reference_Id = '{$chargeObject["payment_intent"]}',
                           tr_capture_code = '{$chargeObject["payment_method"]}',
                           tr_reconciliation_Id = '{$chargeObject["id"]}',
                           tr_type = '$type',
                           tr_amount={$bizRow["tri_total_amount"]},
                           tr_currency='{$bizRow["tri_currency"]}',
                           tr_pay_method = 'CC',
                           tr_paymentMethodToken='{$bizRow["biz_paymentMethodToken"]}',
                           tr_paymentMethodType='{$bizRow["biz_paymentMethodType"]}',
                           tr_cvv = '{$bizRow["biz_cvv"]}',
                           tr_4_digits = '$last4',
                            tr_vendor = '$vendor',
                           tr_ip = '{$bizRow["owner_ip"]}',
                           tr_country = '{$bizRow["biz_addr_country_id"]}',
                           tr_phone = '{$bizRow["biz_mobile_tele"]}',
                           tr_aff_id = {$bizRow["owner_aff_id"]},
                           tr_resellerid = {$bizRow["owner_reseller_id"]}
                          ");
        
        $this->db->execute("update tbl_transaction_items set
                                tri_tr_id=$transID
                                where tri_invoice_id=$invoiceId
                                ");

        $this->setAffiliateCommission($bizRow["owner_aff_id"],$invoiceId);
    }

    function addTransactionReseller3D($invoiceId,$chargeObject,$type){
        
        $bizRow = $this->db->getRow("SELECT * FROM tbl_resellers,tbl_trans_invoice,tbl_owners
                                      WHERE reseller_id = tri_reseller_id
                                      AND owner_reseller_id = reseller_id
                                      AND tri_id=$invoiceId");
        
        $card = $chargeObject["payment_method_details"]["card"];
        $last4 = (isset($card["last4"])) ? $card["last4"] : "";
        $vendor = (isset($card["brand"])) ? $card["brand"] : "";

        $transID = $this->db->execute("insert into tbl_transaction set
                           tr_invoice = $invoiceId,
                           tr_biz_id = 0,
                           tr_reference_Id = '{$chargeObject["payment_intent"]}',
                           tr_capture_code = '{$chargeObject["payment_method"]}',
                           tr_reconciliation_Id = '{$chargeObject["id"]}',
                           tr_type = '$type',
                           tr_amount={$bizRow["tri_total_amount"]},
                           tr_currency='{$bizRow["tri_currency"]}',
                           tr_pay_method = 'CC',
                           tr_paymentMethodToken='{$bizRow["reseller_paymentMethodToken"]}',
                           tr_paymentMethodType='{$bizRow["reseller_paymentMethodType"]}',
                           tr_cvv = '{$bizRow["reseller_cvv"]}',
                           tr_4_digits = '$last4',
                           tr_vendor = '$vendor',
                           tr_ip = '{$bizRow["owner_ip"]}',
                           tr_country = '{$bizRow["reseller_country_id"]}',
                           tr_phone = '{$bizRow["reseller_phone"]}',
                           tr_aff_id = {$bizRow["owner_aff_id"]},
                           tr_resellerid = {$bizRow["owner_reseller_id"]}
                          ");
        
        $this->db->execute("update tbl_transaction_items set
                                tri_tr_id=$transID
                                where tri_invoice_id=$invoiceId
                                ");
    }

    // ************************************************************************************************************************************************* //
    
    function getNextBillingDateBiz($bizId){
     
        $nextActualBillingData = "";
        $openFeatures = $this->getOpenFeaturesList($bizId);
        if(count($openFeatures) > 0){
            foreach ($openFeatures as $singleFeature)
            {
            	$candidate = $this->getNextBillingDateForFeature($singleFeature["tf_id"],$singleFeature["tf_entity"],$singleFeature["tf_entity_id"]);
                
                if($candidate != "" && ($nextActualBillingData == "" || strtotime($candidate) < strtotime($nextActualBillingData))){
                    $nextActualBillingData = $candidate;
                }
            }            
        }
        
        return $nextActualBillingData;
    }
    
    function getNextBillingDateForFeature($tfId,$type,$entityId){

        $firstCharge = $this->getFirstBillingDateBiz($tfId);
        $intervalMonths = 1;
        if ($type == "plan"){
            $entitiesManager = new entitiesManager();
            $planRow = $entitiesManager->getPlanById($entityId);
            $intervalMonths = 12 / $planRow["pl_anual_freq_times"];
        }
       
        $nextBillingData = "";
        for($i=1; $i<1000; $i++){
            $interval = $intervalMonths * $i;
            $nextBillingData = $this->db->getVal("SELECT IF(date_add('$firstCharge', INTERVAL $interval MONTH) >  LAST_DAY(CURRENT_DATE()),DATE_FORMAT(date_add('$firstCharge', INTERVAL $interval MONTH) ,'%Y-%m-01'),'')");
            if($nextBillingData != ""){
                break;
            }
        }
        
        return $nextBillingData;
    }

    function getFirstBillingDateBiz($tf_id){

        return $this->db->getVal("SELECT  IF(tf_trial_original_date IS NULL, tf_start_time,tf_trial_original_date) AS first_charge 
                                    FROM tbl_transaction_feature 
                                    WHERE tf_id=$tf_id ");
    }
    
    function getOpenFeaturesList($bizId){

        return $this->db->getTable("SELECT tf_id,tf_entity,tf_entity_id 
                                    FROM tbl_transaction_feature 
                                    WHERE tf_biz_id=$bizId 
                                    AND tf_end_time IS NULL");
    }

    function getOpenPlan($bizId){

        return $this->db->getRow("SELECT *,IFNULL(MONTH(tf_trial_date), MONTH(DATE_ADD(NOW(), INTERVAL 1 MONTH))) month ,IFNULL(YEAR(tf_trial_date), YEAR(DATE_ADD(NOW(), INTERVAL 1 MONTH))) year
                                    FROM tbl_transaction_feature,tbl_plans 
                                    WHERE tf_biz_id=$bizId 
                                    AND tf_end_time IS NULL
                                    AND tf_entity='plan'
                                    AND pl_id=tf_entity_id");
    }

    function getBizChargeItems($bizId){

        $response = array();

        $data = $this->db->getTable("SELECT *,MONTH(DATE_ADD(NOW(), INTERVAL 1 MONTH)) month ,YEAR(DATE_ADD(NOW(), INTERVAL 1 MONTH)) year
                                    FROM tbl_transaction_feature 
                                    WHERE tf_biz_id=$bizId 
                                    AND tf_end_time IS NULL");
        if(count($data)>0){
            foreach ($data as $itemRow)
            {
            	switch ($itemRow["tf_entity"]){
                    case "plan":
                        $itemRow["entity_name"] = $this->db->getVal("SELECT pl_name FROM tbl_plans WHERE pl_id = {$itemRow["tf_entity_id"]}");
                        array_push($response,$itemRow);
                        break;
                    case "module":
                        $itemRow["entity_name"] = $this->db->getVal("SELECT mod_name FROM tbl_modules WHERE mod_id = {$itemRow["tf_entity_id"]}");
                        array_push($response,$itemRow);
                        break;
                    case "function":
                        $itemRow["entity_name"] = $this->db->getVal("SELECT fu_default_label FROM tbl_functions WHERE fu_id = {$itemRow["tf_entity_id"]}");
                        array_push($response,$itemRow);
                        break;
                    case "appstore":
                        $itemRow["entity_name"] = $this->db->getVal("SELECT name FROM tbl_appstores WHERE id = {$itemRow["tf_entity_id"]}");
                        array_push($response,$itemRow);
                        break;
                }
            }
            
        }
        return $response;
    }
    
    function terminateInvoices($bizId){
        $this->db->execute("update tbl_trans_invoice set tri_collection=1 where tri_biz_id=$bizId and tri_payd=0");
        $this->db->execute("delete from tbl_transaction_bad where ttb_biz_id=$bizId");
    }
    
    function killPAPI($bizId){
        $zooz = new zoozPartnersManager();
        $userRow = $this->db->getRow("select owner_zooz_merchant_id,biz_zooz_program_id from tbl_biz,tbl_owners where owner_id=biz_owner_id and biz_id=$bizId");
        $zooz->removeProgram($userRow["owner_zooz_merchant_id"],$userRow["biz_zooz_program_id"],$bizId);
    }





    /*********************************************** Hip Hop Billing System - Bobile Methods ************************************************/

    function getRowForInvoiceClient($invoiceID){
        
        return $this->db->getRow("select * FROM tbl_trans_invoice WHERE tri_id=$invoiceID");
    }

    function addPaymentSourceToOwner($ownerId, $stripePaymentSourceTokenArray){

        $tokenData = $stripePaymentSourceTokenArray["token"];
        $name = addslashes($tokenData["billing_details"]["name"]);
        $phone = addslashes($tokenData["billing_details"]["phone"]);

        if (!isset($tokenData["id"]) || $tokenData["id"] == ""){
            return 0;
        }
        
        $customerId = $this->getStripeCustomerIdCreateIfMissing($ownerId);

        if ($tokenData["type"] == "card"){ //This is a credit card

            $response = $this->stripe_attachPaymentMethodToCustomer($customerId,$tokenData["id"]);

            if($response["code"] == 0){
                $ip = utilityManager::get_real_IP();
                $sql = "INSERT INTO tbl_owner_payment_sources SET
                      ops_owner_id = $ownerId,
                      ops_type = '{$tokenData["type"]}',
                      ops_ip = '$ip',
                      ops_token = '{$tokenData["id"]}',
                      ops_stipe_object_id = '{$tokenData["id"]}',
                      ops_card_brand = '{$tokenData["card"]["brand"]}',
                      ops_country = '{$tokenData["card"]["country"]}',
                      ops_exp_month = {$tokenData["card"]["exp_month"]},
                      ops_exp_year = {$tokenData["card"]["exp_year"]},
                      ops_funding = '{$tokenData["card"]["funding"]}',
                      ops_last4 = '{$tokenData["card"]["last4"]}',
                      ops_name = '$name',
                      ops_phone = '$phone'";

                $newId = $this->db->execute($sql);
                $this->setDefaultSourceForOwner($newId); //Stripe marks every new card as the default
            }

            return $response;

        }else{ //This is a Bank account or some other source type
            return array();
        }
    
    }

    function getStripeCustomerIdCreateIfMissing($ownerId){
        $customerId = $this->getStripeCustomerId($ownerId);
        if ($customerId == ""){
            $customerId = $this->stripe_createCustomer($ownerId);
        }

        return $customerId;
    }

    function removePaymentSourceFromOwner($ownerId,$paymentSourceId){
        $this->db->execute("DELETE FROM tbl_owner_payment_sources where ops_owner_id=$ownerId and ops_id=$paymentSourceId");
    }

    function setDefaultSourceForOwner($sourceId){

        $paymentSourceRow = $this->db->getRow("SELECT * FROM tbl_owner_payment_sources WHERE ops_id=$sourceId");

        $this->db->execute("UPDATE tbl_owner_payment_sources SET ops_default=0 WHERE ops_owner_id={$paymentSourceRow["ops_owner_id"]}");
        $sql = "UPDATE tbl_owner_payment_sources SET ops_default=1 WHERE ops_id=$sourceId";
        $this->db->execute($sql);
    }

    function invalidateDefaultSourceForOwner($ownerId){

        if ($ownerId == "" || $ownerId == 0) return;

        $this->db->execute("UPDATE tbl_owner_payment_sources SET ops_valid = 0 WHERE ops_owner_id=$ownerId AND ops_default=1");

    }

    function getPaymentSourceTokenBySourceId($paymentSourceId){
        return $this->db->getVal("SELECT ops_token from tbl_owner_payment_sources WHERE ops_id=$paymentSourceId");
    }

    function getStripeCustomerId($ownerId){
        $sql = "SELECT owner_stripe_cust_id FROM tbl_owners WHERE owner_id = $ownerId";
        return $this->db->getVal($sql);
    }

    function getOwnerIdWithStripeCustomerId($stripeCustomerId){
        $sql = "SELECT owner_id FROM tbl_owners WHERE owner_stripe_cust_id = '$stripeCustomerId'";
        return $this->db->getVal($sql);
    }

    function getStripeCustomerIdByBizId($bizId){
        $sql = "SELECT owner_stripe_cust_id FROM tbl_owners,tbl_biz WHERE biz_owner_id=owner_id AND biz_id = $bizId";
        return $this->db->getVal($sql);
    }

    function getStripeCustomerIdByResellerId($resellerId){
        $sql = "SELECT owner_stripe_cust_id FROM tbl_owners WHERE owner_reseller_id = $resellerId";
        return $this->db->getVal($sql);
    }

    function getAllPaymentSourcesForOwner($ownerId){
        $sql = "SELECT * FROM tbl_owner_payment_sources WHERE ops_owner_id = $ownerId ORDER BY ops_default DESC";
        return $this->db->getTable($sql);
    }

    function getAllPaymentSourcesForResellerClient($clientId){
        $sql = "SELECT * FROM tbl_reseller_client_payment_sources WHERE ops_owner_id = $clientId ORDER BY ops_default DESC";
        return $this->db->getTable($sql);
    }

    function stripePurchase($invoiceId){

        $shouldBeCreated = $this->checkInvoiceDoesntHaveStripeId($invoiceId); //If the the invoice DOESN'T have stripe_id - it means it needs to be created on stripe first.
        $openInvoiceResult = array();
        $openInvoiceResult["responseStatus"] = "0"; //Assuming that everything is fine.
        if ($shouldBeCreated){
            $openInvoiceResult = $this->stripe_createInvoice($invoiceId);
        }

        //Pay
        if (isset($openInvoiceResult["responseStatus"]) && $openInvoiceResult["responseStatus"] == "0") {
            return $this->stripe_payInvoice($invoiceId);
        }else{
            return $openInvoiceResult;
        }
    }

    function stripePurchaseReseller($invoiceId){

        $shouldBeCreated = $this->checkInvoiceDoesntHaveStripeId($invoiceId); //If the the invoice DOESN'T have stripe_id - it means it needs to be created on stripe first.
        $openInvoiceResult = array();
        $openInvoiceResult["responseStatus"] = "0"; //Assuming that everything is fine.
        if ($shouldBeCreated){
            $openInvoiceResult = $this->stripe_createInvoiceReseller($invoiceId);
        }

        //Pay
        if (isset($openInvoiceResult["responseStatus"]) && $openInvoiceResult["responseStatus"] == "0") {
            return $this->stripe_payInvoice($invoiceId);
        }else{
            return $openInvoiceResult;
        }
    }

    function getDefaultCardForOwner($ownerId){
        return $this->db->getRow("SELECT * FROM tbl_owner_payment_sources WHERE ops_owner_id = $ownerId and ops_valid=1 and ops_default=1 LIMIT 1");
    }

    function ownerHasDefaultSource($ownerId){
        return $this->db->getVal("SELECT count(ops_id) FROM tbl_owner_payment_sources WHERE ops_owner_id = $ownerId and ops_valid=1 and ops_default=1") > 0;
    }

    function setPendingStatusToInvoice($invoiceId){
        $sql = "UPDATE tbl_trans_invoice SET tri_status='pending' WHERE tri_id=$invoiceId";
        $this->db->execute($sql);
    }

    function checkInvoiceDoesntHaveStripeId($invoiceId){
        $checkSQL = "SELECT COUNT(tri_id) vol FROM tbl_trans_invoice WHERE tri_id = $invoiceId AND tri_stripe_invoice_id != ''";
        $has = $this->db->getVal($checkSQL);
        return $has == 0;
    }

    function getLocalInvoiceIdByStripeInvoiceId($stripeInvoiceId){

        return $this->db->getVal("SELECT tri_id FROM tbl_trans_invoice WHERE tri_stripe_invoice_id = '$stripeInvoiceId' AND tri_status != 'paid'");

    }

    function getLocalInvoiceIdByStripeChargeId($stripeChargeId){

        return $this->db->getVal("SELECT tri_id FROM tbl_trans_invoice WHERE tri_stripe_charge_id = '$stripeChargeId'");

    }

    function deleteTransactionBadRowByInvoiceId($invoiceId){
        
        $this->db->execute("delete from tbl_transaction_bad WHERE ttb_invoice_id=$invoiceId");
    }

    function updateRefundTransaction($transactionID){
        $this->db->execute("update tbl_transaction set
                                tr_date=now(),
                                tr_type='REFUND',
                                tr_amount = 0 - tr_amount
                                where tr_id=$transactionID
                                ");
    }

    function setAffiliateCommission($affiliateId, $invoiceId){

        if($affiliateId != "" && $affiliateId != "0"){
            $invoiceRow = $this->getRowForInvoiceClient($invoiceId);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,"https://affiliate.bobile.com/sale.php?profile=070470&idev_saleamt={$invoiceRow["tri_total_amount"]}&idev_ordernum=$invoiceId&affiliate_id=$affiliateId");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_exec($ch);
            curl_close($ch);
        }
    }

    function getAccountOwnerRow($ownerId){
        $sql = "SELECT * FROM tbl_owners,tbl_account
                    WHERE owner_id = $ownerId
                    AND owner_account_id = ac_id";
        return $this->db->getRow($sql);
    }

    //This Closes all the invoices for that Biz - bobile+stripe - Should be called only when the user cancels the account.
    function closeAllInvoicesForBiz($bizId){

        $openInvoices = $this->getAllPendingInvoicesForBiz($bizId);

        if (count($openInvoices)>0){
            foreach ($openInvoices as $oneInvoice){
                $invoiceID = $oneInvoice["tri_id"];
                $this->stripe_stopInvoiceCollection($invoiceID);
                if($oneInvoice["tri_status"] == "pending"){
                    $this->voidInvoice($invoiceID);
                }else{
                    $this->outstandInvoice($invoiceID);
                }
            } 
        }

        $this->terminateInvoices($bizId);
        //1. Get all pending / outstanding invoices per biz
        //2. For each invoice -> stripe_stopInvoiceCollection($invoiceId);
    }

    //This Closes all the outstanding invoices for that Biz - bobile+stripe - we don't leave open invoices for the same monthe that was billed by other invoice.
    function closeCurrentMonthOutstandingInvoice($bizId){

        $openInvoices = $this->getOutstandingInvoiceForThisMonth($bizId);

        if (count($openInvoices)>0){
            foreach ($openInvoices as $oneInvoice){
                $invoiceID = $oneInvoice["tri_id"];
                $this->stripe_stopInvoiceCollection($invoiceID);
                $this->voidInvoice($invoiceID);
            } 
        }

        $this->terminateInvoices($bizId);
        //1. Get all outstanding invoices per biz for this month
        //2. For each invoice -> stripe_stopInvoiceCollection($invoiceId);
    }

    function getAllPendingInvoicesForBiz($bizId){

        $sql = "SELECT * FROM tbl_trans_invoice
                WHERE tri_biz_id = $bizId
                AND tri_status IN ('outstanding','pending')
                AND tri_payd = 0";

        return $this->db->getTable($sql);

    }
   
    function getOutstandingInvoiceForThisMonth($bizId){

        $sql = "SELECT * FROM tbl_trans_invoice
                WHERE tri_month = MONTH(CURRENT_DATE())
                AND tri_year = YEAR(CURRENT_DATE())
                AND tri_biz_id = $bizId
                AND tri_status = 'outstanding'
                AND tri_payd = 0";

        return $this->db->getTable($sql);

    }
    
    
    /************************************************ Hip Hop Billing System - Stripe Methods ************************************************/

    // ------------------- STRIPE CUSTOMER ----------------------- //

    function stripe_createCustomer($ownerId){
        
        $ownerData = $this->getAccountOwnerRow($ownerId);

        if($ownerData["owner_stripe_cust_id"] == ""){
            try{

                $data = array();
                $data["email"] = $ownerData["ac_username"];
                $data["description"] = $ownerId;

                $customer = \Stripe\Customer::create(
                    $data
                );

                $this->db->execute("update tbl_owners set owner_stripe_cust_id='{$customer['id']}' where owner_id=$ownerId");
                return $customer['id'];
            }
            catch(Exception $e){
                return "0";
            }
        }else{
            return $ownerData["owner_stripe_cust_id"];
        }
    }

    function stripe_retreiveCustomer($stripeCustomerId){

        try{
            $customer = \Stripe\Customer::retrieve($stripeCustomerId);

            return $customer;
        }
        catch(Exception $e){
            return array();
        }
    }
    
    // ------------------- STRIPE CREDIT CARD --------------------- //

    function stripe_addCardToCustomer($stripeCustomerId,$token){

        $response["code"] = 0;
        $response["message"] = "success";

        try{

            $customer = \Stripe\Customer::retrieve($stripeCustomerId);
            $response["data"] = $customer->sources->create(["source" => $token]);
        }
        catch(Exception $e){
            $response["code"] = -1;
            $response["message"] = $e->getMessage();
        } 
        
        return $response;
    }

    function stripe_attachPaymentMethodToCustomer($stripeCustomerId,$paymentMethodId){

        $response["code"] = 0;
        $response["message"] = "success";

        try{

            $payMethod = \Stripe\PaymentMethod::retrieve($paymentMethodId);            
            $response["data"] = $payMethod->attach(['customer' => $stripeCustomerId]);

            \Stripe\Customer::update(
                $stripeCustomerId,
                [
                'invoice_settings' => ['default_payment_method' => $payMethod->id],
                ]
            );
        }
        catch(Exception $e){
            $response["code"] = -1;
            $response["message"] = $e->getMessage();
        } 
        
        return $response;
    }

    function stripe_removeCardFromCustomer($stripeCustomerId,$cardId){

        try{
            $customer = \Stripe\Customer::retrieve($stripeCustomerId);
            $customer->sources->retrieve($cardId)->delete();
        }catch(Exception $e){}
    }

    function stripe_retreiveCardsForCustomer($stripeCustomerId){

        try{
            $customer = \Stripe\Customer::retrieve($stripeCustomerId);
            $response = $customer->sources->all()["data"];
            return $response;
        }
        catch(Exception $e){
            return array();
        }
    }

    function stripe_setCardAsDefaultForCustomer($stripeCustomerId,$stripeCardId){

        $response["code"] = 0;
        $response["message"] = "success";
        try{
            $customer = \Stripe\Customer::retrieve($stripeCustomerId);
            $customer->default_source = $stripeCardId;
            $customer->save();

        }catch(Exception $e){
            $response["code"] = -1;
            $response["message"] = "failure";
        }

        return $response;
    }

    function stripe_retreiveSpecificCardForCustomer($stripeCustomerId,$stripeCardId){

        try{
            $customer = \Stripe\Customer::retrieve($stripeCustomerId);
            $response = $customer->sources->retrieve($stripeCardId);
            return $response;
        }
        catch(Exception $e){
            return array();
        }

    }



    // ------------------- STRIPE BANK ACCOUNT -------------------- //

    function stripe_addBankToCustomer($stripeCustomerId,$bankId){

    }

    function stripe_removeBankFromCustomer($stripeCustomerId,$bankId){

    }

    function stripe_retreiveBanksForCustomer($stripeCustomerId){

    }

    function stripe_setBankAsDefaultForCustomer($stripeCustomerId,$stripeTokenId){
        
    }


    // ------------------ STRIPE INVOICE ------------------------- //

    function stripe_createInvoice($invoiceId){

        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeCustomerId = $this->getStripeCustomerIdByBizId($invoiceRow["tri_biz_id"]);
        $invoiceItems = $this->db->getTable("SELECT * FROM tbl_transaction_items WHERE tri_invoice_id=$invoiceId");

        if(count($invoiceItems) > 0){
            foreach ($invoiceItems as $item)
            {
                $itemArray = array();
                $itemArray["amount"] = number_format((float)$item["tri_monthly_price"], 2, '', '');
                $itemArray["currency"] = $invoiceRow["tri_currency"];
                $itemArray["description"] = $item["tri_name"];
                $itemArray["quantity"] = $item["tri_quantity"] == 0 ? 1 : $item["tri_quantity"];

            	$resultItem = $this->stripe_createInvoiceItem($stripeCustomerId,$itemArray);

                if (isset($resultItem["responseStatus"])){
                    return $resultItem;
                }
            }            
        }else{

            $response["responseStatus"] = -1;
            $response["errorDescription"] = "No Items Created on Stripe";
            return $response;
        }

        try{

            $metadata = array();
            $metadata["Invoice ID"] = $invoiceId;
            $metadata["Biz ID"] = $invoiceRow["tri_biz_id"];
            $metadata["Entity"] = "app";

            $data = array();
            $data["customer"] = $stripeCustomerId;
            $data["billing"] = $invoiceRow["tri_stripe_billing_type"];
            $data["metadata"] = $metadata;
            $data["auto_advance"] = false;

            $invoice = \Stripe\Invoice::create(
                        $data
                    );
            
            $stripeInvoiceId = $invoice['id'];        
            $this->db->execute("UPDATE tbl_trans_invoice SET tri_stripe_invoice_id='$stripeInvoiceId' WHERE tri_id=$invoiceId");

            $response["responseStatus"] = 0;
            $response["errorDescription"] = "Invoice Created Successfully";
            return $response;

        }catch(Exception $e){

            //If the invoice creation was failed - it means that there are floating items in Stripe.
            //It is required to clean them. otherwise they will be added to the next invoice
            $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId);

            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();
            return $response;
        }

    }

    function stripe_createInvoiceReseller($invoiceId){

        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeCustomerId = $this->getStripeCustomerIdByResellerId($invoiceRow["tri_reseller_id"]);
        $invoiceItems = $this->db->getTable("SELECT * FROM tbl_transaction_items WHERE tri_invoice_id=$invoiceId");

        if(count($invoiceItems) > 0){
            foreach ($invoiceItems as $item)
            {
                $itemArray = array();
                $itemArray["amount"] = number_format((float)$item["tri_monthly_price"], 2, '', '');
                $itemArray["currency"] = $invoiceRow["tri_currency"];
                $itemArray["description"] = $item["tri_name"];
                $itemArray["quantity"] = $item["tri_quantity"] == 0 ? 1 : $item["tri_quantity"];

            	$resultItem = $this->stripe_createInvoiceItem($stripeCustomerId,$itemArray);

                if (isset($resultItem["responseStatus"])){
                    return $resultItem;
                }
            }            
        }else{

            $response["responseStatus"] = -1;
            $response["errorDescription"] = "No Items Created on Stripe";
            return $response;
        }

        try{

            $metadata = array();
            $metadata["Invoice ID"] = $invoiceId;
            $metadata["Reseller ID"] = $invoiceRow["tri_reseller_id"];
            $metadata["Entity"] = "reseller";

            $data = array();
            $data["customer"] = $stripeCustomerId;
            $data["billing"] = $invoiceRow["tri_stripe_billing_type"];
            $data["metadata"] = $metadata;
            $data["auto_advance"] = true;

            $invoice = \Stripe\Invoice::create(
                        $data
                    );
            
            $stripeInvoiceId = $invoice['id'];        
            $this->db->execute("UPDATE tbl_trans_invoice SET tri_stripe_invoice_id='$stripeInvoiceId' WHERE tri_id=$invoiceId");

            $response["responseStatus"] = 0;
            $response["errorDescription"] = "Invoice Created Successfully";
            return $response;

        }
        catch(Exception $e){

            //If the invoice creation was failed - it means that there are floating items in Stripe.
            //It is required to clean them. otherwise they will be added to the next invoice
            $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId);

            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();
            return $response;
        }

    }

    function stripe_payInvoice($invoiceId){
        
        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeInvoiceId = $invoiceRow["tri_stripe_invoice_id"];

        if($stripeInvoiceId == ""){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = "Missing invoice";
        }

        $response = array();

        try{
            $invoice = \Stripe\Invoice::retrieve($stripeInvoiceId);
            $invoiceObject = $invoice->pay();

            $this->db->execute("UPDATE tbl_trans_invoice SET tri_stripe_charge_id='{$invoiceObject["charge"]}' WHERE tri_id=$invoiceId");

            if($invoiceObject["paid"]){
                $response["responseStatus"] = 0;
                $response["errorDescription"] = "Success";            
                $this->closeInvoice($invoiceId);
                if($invoiceRow["tri_reseller_id"] > 0){
                    $this->addTransactionReseller($invoiceId,"","",$invoiceObject["charge"],"CHARGE");
                }else{
                    $this->addTransactionBiz($invoiceId,"","",$invoiceObject["charge"],"CHARGE");
                }
                $this->deleteTransactionBadRowByInvoiceId($invoiceId);
            }else{
                $chargeData = $this->stripe_retreiveChargeResultForInvoice($invoiceId);
                $response["responseStatus"] = -1;
                $response["errorDescription"] = $chargeData["outcome"]["seller_message"];
                $response["errorCode"] = $chargeData["failure_code"];
            }
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();
            $response["errorCode"] = "0";
        }

        return $response;
    }
    function stripe_payInvoice3D($invoiceId,$offline = true,$piId = ""){

        $response = array();
        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);

        $ownerId = "";
        if($invoiceRow["tri_biz_id"] != 0){
            $ownerId = appManager::getOwnerIdByBizId($invoiceRow["tri_biz_id"]);
        }else{
            $resellerData = appManager::getResellerOwnerByRid($invoiceRow["tri_reseller_id"]);
            $ownerId = $resellerData["owner_id"];
        }

        if($ownerId == ""){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = "Mising Owner";
            $response["errorCode"] = "0";
        }

        $defaultCard = $this->getDefaultCardForOwner($ownerId);
        if(!is_array($defaultCard)){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = "Mising Credit Card";
            $response["errorCode"] = "0";
        }

        $stripeCustomerId = $this->getStripeCustomerId($ownerId);
        if($stripeCustomerId == ""){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = "Mising Stripe Customer Id";
            $response["errorCode"] = "0";
        }

        $data = array();
        $data["amount"] = $invoiceRow["tri_total_amount"] * 100;
        $data["currency"] = $invoiceRow["tri_currency"];
        $data["customerId"] = $stripeCustomerId;
        $data["paymentMethodId"] = $defaultCard["ops_stipe_object_id"];

        try{
            
            if($piId == ""){
                if($offline){
                    $payIntent = stripeConnectManager::createPaymentIntent($data);
                }else{
                    $payIntent = stripeConnectManager::createPaymentIntentOnline($data);
                }
            }else{
                $payIntent = stripeConnectManager::retrievePaymentIntent($piId);                
            }
            $result = stripeConnectManager::confirmPaymentIntent($payIntent["data"]);

            if($result["code"] != 1){
                $response["responseStatus"] = -1;
                $response["errorDescription"] = $result["message"];
                $response["errorCode"] = $result["code"];
            }else{
                $chargeObject = $result["data"]["charges"]["data"][0];
                
                $this->db->execute("UPDATE tbl_trans_invoice SET tri_stripe_charge_id='{$chargeObject["id"]}' WHERE tri_id=$invoiceId");

                if($chargeObject["paid"] == 1){
                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Success";            
                    $this->closeInvoice($invoiceId);
                    if($invoiceRow["tri_reseller_id"] > 0){
                        $this->addTransactionReseller3D($invoiceId,$chargeObject,"CHARGE");
                    }else{
                        $this->addTransactionBiz3D($invoiceId,$chargeObject,"CHARGE");
                    }
                    $this->deleteTransactionBadRowByInvoiceId($invoiceId);
                }else{
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = "Not Paid";
                    $response["errorCode"] = "";
                    $response["data"] = $result["data"];
                }
            }
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();
            $response["errorCode"] = "0";
        }

        return $response;
    }
    function stripe_refundInvoice($invoiceId,$amount=0){

        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeChargeId = $invoiceRow["tri_stripe_charge_id"];
        $refundAmount = ($amount > 0) ? $amount : $invoiceRow["tri_total_amount"];

        if($amount > 0 && $refundAmount > $invoiceRow["tri_total_amount"]){
            $refundAmount = $invoiceRow["tri_total_amount"];
        }

        if($stripeChargeId == ""){
            return "-1";
        }

        try{

            $metadata = array();
            $metadata["Entity"] = "app";

            $data = array();
            $data["charge"] = $stripeChargeId;
            $data["amount"] = $amount;
            $data["metadata"] = $metadata;

            $response = \Stripe\Refund::create(
                $data
            );

            return $response;
        }catch(Exception $e){
            return array();
        }
    }

    function stripe_retreiveChargeResultForInvoice($invoiceId){
        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeChargeId = $invoiceRow["tri_stripe_charge_id"];

        try{
            $response = \Stripe\Charge::retrieve($stripeChargeId);
            return $response;
        }catch(Exception $e){
            return array();
        }
    }

    function stripe_closeInvoice($invoiceId){

        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeInvoiceId = $invoiceRow["tri_stripe_invoice_id"];

        if($stripeInvoiceId != ""){

            \Stripe\Invoice::update(
                    $stripeInvoiceId,
                    [
                        'auto_advance'   => false
                    ]
                );
        }
    }

    function stripe_stopInvoiceCollection($invoiceId){

        $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
        $stripeInvoiceId = $invoiceRow["tri_stripe_invoice_id"];

        if($stripeInvoiceId != ""){

            \Stripe\Invoice::update(
                    $stripeInvoiceId,
                    [
                        'auto_advance'   => false
                    ]
                );
        }

    }

    // ------------------ STRIPE WEBHOOKS ------------------------ //

    function stripe_webhookPaymentSucceeded($receivedData){

        $invoiceId = $this->getLocalInvoiceIdByStripeInvoiceId($receivedData->data->object->id);

        if($invoiceId != ""){
            $this->db->execute("UPDATE tbl_trans_invoice SET tri_stripe_charge_id='{$receivedData->data->object->charge}' WHERE tri_id=$invoiceId");
            $invoiceRow = $this->getRowForInvoiceClient($invoiceId);
            $this->closeInvoice($invoiceId);

            if($invoiceRow["tri_reseller_id"] > 0){
                $this->addTransactionReseller($invoiceId,"","",$receivedData->data->object->charge,"CHARGE");
            }else{
                $this->addTransactionBiz($invoiceId,"","",$receivedData->data->object->charge,"CHARGE");
            }

            $this->deleteTransactionBadRowByInvoiceId($invoiceId);
        }

    }

    function stripe_webhookChargeRefunded($receivedData){

        $invoiceId = $this->getLocalInvoiceIdByStripeChargeId($receivedData->data->object->id);

        $app_creator = new appCreatorManager();
        $transactionID =  $this->getTransactionByInvoiceId($invoiceId);
        if($transactionID != ""){
            $trans_id = $app_creator->DuplicateSQLRecord('tbl_transaction', 'tr_id', $transactionID);
            $this->updateRefundTransaction($trans_id);
        } 
    }

    // ------------------ STRIPE ITEMS ------------------------- //

    function stripe_createInvoiceItem($stripeCustomerId,$itemArray){

        try{

            $data = array();
            $data["customer"] = $stripeCustomerId;
            $data["unit_amount"] = $itemArray["amount"];
            $data["currency"] = $itemArray["currency"];
            $data["description"] = $itemArray["description"];
            $data["quantity"] = $itemArray["quantity"];

            $invoiceItem = \Stripe\InvoiceItem::create(
                $data
            );

            return $invoiceItem;
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();
            return $response;
        }

    }

    function stripe_deleteInvoiceItem($stripeItemId){

        try{
            $invoiceItem = \Stripe\InvoiceItem::retrieve($stripeItemId);
            $response = $invoiceItem->delete();

            return $response;
        }catch(Exception $e){
            return array();
        }

    }

    function stripe_deleteFloatingItemsForCustomer($stripeCustomerId){

        $allCustItems = $this->stripe_retreiveAllItemsForCustomer($stripeCustomerId);

        if (count($allCustItems["data"]) > 0){
            foreach ($allCustItems["data"] as $item){
                if( !isset($item["invoice"]) || $item["invoice"] == null || $item["invoice"] == "" ){
                    $this->stripe_deleteInvoiceItem($item["id"]);
                }
            }
        }

    }

    function stripe_retreiveAllItemsForCustomer($stripeCustomerId){

        try{

            $data = array();
            $data["customer"] = $stripeCustomerId;

            $response = \Stripe\InvoiceItem::all(
                $data
            );
         
            return $response;
        }catch(Exception $e){
            return array();
        }

    }

    /************************************* */
    /*   BASIC PLAN - PUBLIC           */
    /************************************* */

    /**
     * Insert new planObject to DB
     * Return Data = new planObject ID
     * @param planObject $planObj 
     * @return resultObject
     */
    public function addPlan(planObject $planObj){       
        try{
            $newId = $this->addPlanDB($planObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($planObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get plan from DB for provided ID
     * * Return Data = planObject
     * @param int $planId 
     * @return resultObject
     */
    public function getPlanByID($planId){
        
        try {
            $planData = $this->loadPlanFromDB($planId);
            
            $planObj = planObject::withData($planData);
            $result = resultObject::withData(1,'',$planObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$planId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param planObject $planObj 
     * @return resultObject
     */
    public function updatePlan(planObject $planObj){        
        try{
            $this->upatePlanDB($planObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($planObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete plan from DB
     * @param int $planID 
     * @return resultObject
     */
    public function deletePlanById($planID){
        try{
            $this->deletePlanByIdDB($planID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$planID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getPlansForBiz(bizObject $biz){
        $resellerID = bizManager::getResellerIDForBiz($biz->biz_id);
        $result = array();
        if($resellerID != 0){
            $plansRows = $this->getResllerPlans();
            $currentPlan = partnersManager::getPartnerCurrentPlan($resellerID);
            $resellerRow = bizManager::getOwnerOrResellerFromBizID($biz->biz_id);

            $result["biz_paymentMethodToken"] = $resellerRow["reseller_paymentMethodToken"];
            $result["biz_paymentMethodType"] = $resellerRow["reseller_paymentMethodType"];
            $result["biz_cvv"] = $resellerRow["reseller_cvv"];
            $result["biz_cc_last_digits"] = $resellerRow["reseller_lastFourDigits"];
            $result["biz_payment_name"] = $resellerRow["reseller_name_on_ccard"];
            $result["biz_payment_email"] = "";
        }
        else{
            $subCatType = bizManager::getBizSubcategoryType($biz->biz_id);

            $plansRows = $this->getPlansForSubCategoryType($subCatType);

            $currentPlan = bizManager::getBizCurrentPlan($biz->biz_id);

            $result["biz_paymentMethodToken"] = $biz->biz_paymentMethodToken;
            $result["biz_paymentMethodType"] = $biz->biz_paymentMethodType;
            $result["biz_cvv"] = $biz->biz_cvv;
            $result["biz_cc_last_digits"] = $biz->biz_cc_last_digits;
            $result["biz_payment_name"] = $biz->biz_payment_name;
            $result["biz_payment_email"] = $biz->biz_payment_email;
        }

        $result['plans'] = array();

        $standardPlan = false;
        foreach ($plansRows as $planRow){
            $plan = planObject::withData($planRow);
            $planArray = $plan->getArrayForAdmin();
            if($plan->pl_id == $currentPlan['pl_id']){
                $planArray['is_selected'] = 1;
                $standardPlan = true;
            }

            $result['plans'][] = $planArray;
        }

        $result['customPlan'] = array();
        if(!$standardPlan && isset($currentPlan['pl_id']) && $currentPlan['pl_id'] != ""){
            $plan = planObject::withData($currentPlan);
            $planArray = $plan->getArrayForAdmin();
            $planArray['is_selected'] = 1;
            $result['customPlan'][] = $planArray;
        }

        $result["current_plan_name"] = $currentPlan['pl_name'];
        $result["current_plan_price"] = $currentPlan['pl_plrice'];
        $result["current_plan_id"] = $currentPlan['pl_id'];
        $result["current_plan_group"] = $currentPlan['pl_type'];
        
        return $result;
    }

    /************************************* */
    /*   BASIC PLAN - DB METHODS           */
    /************************************* */

    private function addPlanDB(planObject $obj){

        if (!isset($obj)){
            throw new Exception("planObject value must be provided");             
        }

        $pl_expire_time = isset($obj->pl_expire_time) ? "'".$obj->pl_expire_time."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_plans SET 
                                        pl_group = {$obj->pl_group},
                                        pl_name = '".addslashes($obj->pl_name)."',
                                        pl_short_name = '".addslashes($obj->pl_short_name)."',
                                        pl_desc = '".addslashes($obj->pl_desc)."',
                                        pl_type = {$obj->pl_type},
                                        pl_entity = '".addslashes($obj->pl_entity)."',
                                        pl_entity_id = {$obj->pl_entity_id},
                                        pl_count = {$obj->pl_count},
                                        pl_int_remarks = '".addslashes($obj->pl_int_remarks)."',
                                        pl_billing_type = {$obj->pl_billing_type},
                                        pl_anual_freq_times = {$obj->pl_anual_freq_times},
                                        pl_anual_freq_type = '".addslashes($obj->pl_anual_freq_type)."',
                                        pl_anual_freq_name = '".addslashes($obj->pl_anual_freq_name)."',
                                        pl_interval = '".addslashes($obj->pl_interval)."',
                                        pl_plrice = {$obj->pl_plrice},
                                        pl_max_price = {$obj->pl_max_price},
                                        pl_currency = '".addslashes($obj->pl_currency)."',
                                        pl_mivtza = '".addslashes($obj->pl_mivtza)."',
                                        pl_selected = {$obj->pl_selected},
                                        pl_email_template = {$obj->pl_email_template},
                                        pl_grace = {$obj->pl_grace},
                                        pl_email_per = {$obj->pl_email_per},
                                        pl_paypal_id = '".addslashes($obj->pl_paypal_id)."',
                                        pl_active = {$obj->pl_active},
                                        pl_head1 = '".addslashes($obj->pl_head1)."',
                                        pl_head2 = '".addslashes($obj->pl_head2)."',
                                        pl_head3 = '".addslashes($obj->pl_head3)."',
                                        pl_body1 = '".addslashes($obj->pl_body1)."',
                                        pl_body2 = '".addslashes($obj->pl_body2)."',
                                        pl_premiuv_visible = '".addslashes($obj->pl_premiuv_visible)."',
                                        pl_tools_visible = '".addslashes($obj->pl_tools_visible)."',
                                        pl_go_url = '".addslashes($obj->pl_go_url)."',
                                        pl_go_text = '".addslashes($obj->pl_go_text)."',
                                        pl_specials_file = '".addslashes($obj->pl_specials_file)."',
                                        pl_is_submission = {$obj->pl_is_submission},
                                        pl_expire_time = '{$obj->pl_expire_time}',
                                        pl_ios_color = '".addslashes($obj->pl_ios_color)."',
                                        pl_aditional_app = {$obj->pl_aditional_app},
                                        pl_start_apps = {$obj->pl_start_apps},
                                        pl_belong_to_reseller = {$obj->pl_belong_to_reseller}
                                                 ");
        return $newId;
    }

    private function loadPlanFromDB($planID){

        if (!is_numeric($planID) || $planID <= 0){
            throw new Exception("Illegal value $planID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_plans WHERE pl_id = $planID");
    }

    private function upatePlanDB(planObject $obj){

        if (!isset($obj->pl_id) || !is_numeric($obj->pl_id) || $obj->pl_id <= 0){
            throw new Exception("planObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_plans SET 
                                pl_group = {$obj->pl_group},
                                pl_name = '".addslashes($obj->pl_name)."',
                                pl_short_name = '".addslashes($obj->pl_short_name)."',
                                pl_desc = '".addslashes($obj->pl_desc)."',
                                pl_type = {$obj->pl_type},
                                pl_entity = '".addslashes($obj->pl_entity)."',
                                pl_entity_id = {$obj->pl_entity_id},
                                pl_count = {$obj->pl_count},
                                pl_int_remarks = '".addslashes($obj->pl_int_remarks)."',
                                pl_billing_type = {$obj->pl_billing_type},
                                pl_anual_freq_times = {$obj->pl_anual_freq_times},
                                pl_anual_freq_type = '".addslashes($obj->pl_anual_freq_type)."',
                                pl_anual_freq_name = '".addslashes($obj->pl_anual_freq_name)."',
                                pl_interval = '".addslashes($obj->pl_interval)."',
                                pl_plrice = {$obj->pl_plrice},
                                pl_max_price = {$obj->pl_max_price},
                                pl_currency = '".addslashes($obj->pl_currency)."',
                                pl_mivtza = '".addslashes($obj->pl_mivtza)."',
                                pl_selected = {$obj->pl_selected},
                                pl_email_template = {$obj->pl_email_template},
                                pl_grace = {$obj->pl_grace},
                                pl_email_per = {$obj->pl_email_per},
                                pl_paypal_id = '".addslashes($obj->pl_paypal_id)."',
                                pl_active = {$obj->pl_active},
                                pl_head1 = '".addslashes($obj->pl_head1)."',
                                pl_head2 = '".addslashes($obj->pl_head2)."',
                                pl_head3 = '".addslashes($obj->pl_head3)."',
                                pl_body1 = '".addslashes($obj->pl_body1)."',
                                pl_body2 = '".addslashes($obj->pl_body2)."',
                                pl_premiuv_visible = '".addslashes($obj->pl_premiuv_visible)."',
                                pl_tools_visible = '".addslashes($obj->pl_tools_visible)."',
                                pl_go_url = '".addslashes($obj->pl_go_url)."',
                                pl_go_text = '".addslashes($obj->pl_go_text)."',
                                pl_specials_file = '".addslashes($obj->pl_specials_file)."',
                                pl_is_submission = {$obj->pl_is_submission},
                                pl_expire_time = '{$obj->pl_expire_time}',
                                pl_ios_color = '".addslashes($obj->pl_ios_color)."',
                                pl_aditional_app = {$obj->pl_aditional_app},
                                pl_start_apps = {$obj->pl_start_apps},
                                pl_belong_to_reseller = {$obj->pl_belong_to_reseller}
                                WHERE pl_id = {$obj->pl_id} 
                                         ");
    }

    private function deletePlanByIdDB($planID){

        if (!is_numeric($planID) || $planID <= 0){
            throw new Exception("Illegal value $planID");             
        }

        $this->db->execute("DELETE FROM tbl_plans WHERE pl_id = $planID");
    }

    private function getResllerPlans(){
        return $this->db->getTable("SELECT * FROM tbl_plans WHERE pl_type=21");
    }

    private function getPlansForSubCategoryType($subCatType){
        if($subCatType != "content" && $subCatType != ""){
            return $this->db->getTable("SELECT * FROM tbl_plans WHERE pl_id in (64,65,35,36,62,63) order by pl_group");
        }else{
            return $this->db->getTable("SELECT * FROM tbl_plans WHERE pl_id in (31,32,33,34) order by pl_group");
        }
    }
}
