<?php

/**
 * bizStatsManager short summary.
 *
 * bizStatsManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class bizStatsManager extends Manager
{
    public static function getInstalls($from,$to,$bizID){
        $instance = new self();
        $sql = "SELECT DATE_FORMAT(datestart, '%b/%Y') dDate,		
                        IF(Android_tot IS NOT NULL,Android_tot,0) Android,
                        IF(IOS_tot IS NOT NULL,IOS_tot,0) iOS,
                        MONTH(datestart) monthNo,
                        YEAR(datestart) yearNo
                FROM(
                select 
                DATE_FORMAT(m1 ,'%Y-%m-01') as datestart

                from
                (
                select 
                ($from) 
                +INTERVAL m MONTH as m1
                from
                (
                select @rownum:=@rownum+1 as m from
                (select 1 union select 2 union select 3 union select 4) t1,
                (select 1 union select 2 union select 3 union select 4) t2,
                (select 1 union select 2 union select 3 union select 4) t3,
                (select 1 union select 2 union select 3 union select 4) t4,
                (select @rownum:=-1) t0
                ) d1
                ) d2 
                where m1<=$to
                order by m1
                ) dates LEFT JOIN (
                SELECT SUM(IF(cust_device = 'IOS',1,0)) as IOS_tot,
		                SUM(IF(cust_device = 'Android',1,0)) as Android_tot,
                        abv_month,
                        abv_year,
                        DATE_FORMAT(STR_TO_DATE(CONCAT('1-',abv_month,'-',abv_year),'%d-%m-%Y'),'%Y-%m-01') as abv_date
                        FROM 
                            (SELECT cust_device,MONTH(cust_reg_date) as abv_month,YEAR(cust_reg_date) as abv_year FROM tbl_customers
                            WHERE cust_biz_id = $bizID
                            AND (cust_status = 'guest' OR cust_status = 'member')
                            AND cust_reg_date BETWEEN $from AND $to
                            group by cust_id) as abv
                            GROUP BY abv_month,abv_year
            ORDER BY abv_year,abv_month) totals ON dates.datestart = abv_date";

        

        return $instance->db->getTable($sql);
    }

    public static function getStatistics($from,$to,$bizID){
        $apiDB = new Database("api");

        $sql = "SELECT xAxis, Android, iOS
                    FROM (SELECT STR_TO_DATE(CONCAT('01/', SUBSTR( abv_month, 1, 3 ), '/', abv_year), '%d/%b/%Y') dDate, CONCAT( SUBSTR( abv_month, 1, 3 ) , '/', abv_year ) AS xAxis, SUM( abv_volume_andr ) AS Android, SUM( abv_volume_ios ) AS iOS, MONTH( STR_TO_DATE( SUBSTR( abv_month, 1, 3 ) , '%b' ) ) month_no, abv_month, abv_year
                    FROM analytics_biz_visits
                    WHERE abv_biz_id = $bizID
                    AND abv_date BETWEEN $from AND $to
                    GROUP BY abv_year, abv_month, month_no
                    ORDER BY abv_year, month_no) K";

        

        return $apiDB->getTable($sql);
    }

    public static function getPopularScreens($from,$to,$bizID){
        $apiDB = new Database("api");

        $sql = "SELECT amp_mod_name mod_name, SUM(amp_volume) vol,amp_mod_id mod_id FROM analytics_mod_profile 
                    WHERE amp_biz_id = $bizID
                    AND amp_date BETWEEN $from AND $to
                    GROUP BY amp_mod_id
                    ORDER BY vol DESC
                    LIMIT 5";        

        return $apiDB->getTable($sql);
    }

    public static function getInstallLocation($from,$to,$bizID){
        $apiDB = new Database("api");

        $sql = "SELECT
                amp_city_name city_name,
                SUM(amp_volume_android + amp_volume_ios) vol
                FROM analytics_location
                WHERE amp_biz_id = $bizID
                AND amp_city_name <> ''
                AND amp_date BETWEEN $from AND $to
                GROUP BY amp_city_name
                ORDER BY vol DESC
                LIMIT 5";        

        return $apiDB->getTable($sql);
    }
}