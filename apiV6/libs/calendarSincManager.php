<?php

require_once ROOT_PATH.'libs/google-api-client/src/Google/autoload.php';

class CalendarSincManager {

    private $data = array();
    private $iosServer = "https://p01-caldav.icloud.com";

    //outlook
    private $clientId = "eae0bc46-d92b-4ef7-a653-11fd69fba616";
    private $clientSecret = "ttZh3PRW6MpFfDwJwsEyVmo";
    private $authority = "https://login.microsoftonline.com";
    private $authorizeUrl = '/common/oauth2/v2.0/authorize?client_id=%1$s&redirect_uri=%2$s&response_type=code&scope=%3$s';
    private $tokenUrl = "/common/oauth2/v2.0/token";
    private $scopes = array("openid",
                            "offline_access",
                            "https://outlook.office.com/calendars.readwrite",
                            "https://outlook.office.com/contacts.read");
    private $outlookApiUrl = "https://outlook.office.com/api/v2.0";

    function __construct()
    {
        $this->db = new Database();

        if (!defined('APPLICATION_NAME')) define('APPLICATION_NAME', 'Google Calendar API');
        if (!defined('CLIENT_SECRET_PATH')) define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret_web.json');
        if (!defined('SCOPES')) define('SCOPES', implode(' ', array(Google_Service_Calendar::CALENDAR)));

    }

    public function __set($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->data)) {
            return $this->data[$name];
        }
        return null;
    }

    public function startGoogleCal($employeeID){

        $calRow = $this->getCalendarDB($employeeID);

        if($calRow["be_cal_type"] == "Google"){
            $client = $this->getClient($employeeID);

            $this->getCalendarsList($client,$employeeID);
            //echo $this->addEvent($calRow["be_cal_id"],"First Event","This is my first event","2017-01-04T18:00:00", "2017-01-04T19:30:00",$client,$calRow["be_event_color"],'RRULE:FREQ=DAILY;COUNT=2') . "<br><br>";

            //$this->createPush($calRow["be_cal_id"],$client,$employeeID);

            //$this->entInstances($calRow["be_cal_id"],"tj80hhq8o0rhcpvc8voqf95meg",$client,'delete');
            //$this->deleteEvent($calRow["be_cal_id"],"f261m5gs60tl8idvglr2o1nlcs",$client);
            //$this->updateEvent($calRow["be_cal_id"],"u5i21hndpqvbfmg2tvum788oj0","First Event update","This is my first event","2017-01-03T18:15:00", "2017-01-03T19:30:00",$client,$calRow["be_event_color"]);
            //$this->getEventsList($calRow["be_cal_id"],$client,$employeeID,$calRow["be_biz_id"]);

            //$this->disconectCalendar($employeeID);
        }
    }

    function getCalendarDB($employeeID){
        return $this->db->getRow("select * from tbl_employee where be_id=$employeeID");
    }

    //connect to google calendar and save the token
    function setupCalendar($code, $employeeID) {

        $client = new Google_Client();
        $client->revokeToken();
        $client->setClientId("707755455448-8kdle07n0mco9g07r1p11ljpapa3g6ib.apps.googleusercontent.com");
        $client->setClientSecret("WPlt7qVrd8eBnVXjAgu4zhxp");
        $client->setRedirectUri('postmessage');
        $client->setAccessType("offline");
        $client->setApprovalPrompt('force');
        $client->setScopes(SCOPES);

        // save access token to DB.
        $accessToken =  $client->authenticate($code);
        $this->db->execute("update tbl_employee
                            set be_token='$accessToken',
                            be_cal_type = 'Google'
                            where be_id=$employeeID");

        $newClient = $this->getClient($employeeID);
        return $this->getCalendarsList($newClient,$employeeID);
    }

    //after user select one of his callendar to sinc
    function setCalendarID($type,$calendarID, $employeeID,$cname) {

        $this->db->execute("update tbl_employee
                            set be_cal_type='$type',
                            be_cal_id='$calendarID',
                            be_cal_name = '".addslashes($cname)."'
                            where be_id=$employeeID");


        if($type == "Google"){
            $client = $this->getClient($employeeID);
            $this->createPush($calendarID,$client,$employeeID);
            $this->syncLocalEventsToGoogle($employeeID);
        }

        if($type == "IOS"){
            $this->syncLocalEventsToIOS($employeeID);
            $this->getEventsListFromIOS($employeeID,false);
        }

        if($type == "Outlook"){
            $this->createPushOutlook($employeeID);
            $this->getOutlookEvents($employeeID);
            $this->syncLocalEventsToOutlook($employeeID);
        }
    }

    function isConectedCalendarID($type,$calendarID,$biz_id = 0) {
        $checkbiz = '';
        if($biz_id != 0){
            $checkbiz = " AND be_biz_id = $biz_id";
        }
        return $this->db->getVal("select count(be_id) from tbl_employee where
                            be_cal_type='$type'
                            AND be_cal_id='$calendarID'
                            $checkbiz") > 0;
    }

    //when the user disconect calendar
    function disconectCalendar($employeeID){

        $calRow = $this->getCalendarDB($employeeID);

        $eventsTable = $this->db->getTable("select * from tbl_employee_meeting where em_source = 'Internal' and em_external_id<>'' and em_emp_id=$employeeID");

        $client = ($calRow["be_cal_type"] == "Google") ? $this->getClient($employeeID) : "";

        if(count($eventsTable)>0){

            foreach ($eventsTable as $eventRow)
            {
                //update befor delete on bobile
                $this->db->execute("update tbl_employee_meeting
                            set em_external_id=''
                            where em_id={$eventRow["em_id"]}
                            and em_emp_id=$employeeID");

                switch ($calRow["be_cal_type"])
                {
                    case "Google":
                        $this->deleteEvent($calRow["be_cal_id"],$eventRow["em_external_id"],$client);
                        break;
                    case "IOS":
                        $this->deleteEventIOS($calRow,$eventRow["em_external_id"]);
                        break;
                    case "Outlook":
                        $this->deleteEventOutlook($calRow["be_id"],$eventRow["em_external_id"]);
                        break;
                }

            }
        }

        $this->db->execute("delete from tbl_employee_meeting where em_source = 'External' and em_external_id<>'' and em_emp_id=$employeeID");
        $this->db->execute("update tbl_employee set be_cal_type='',
                                                    be_token='',
                                                    be_cal_id='',
                                                    be_cal_name='',
                                                    be_sync_token='',
                                                    be_ipn_expiration ='0000-00-00 00:00:00',
                                                    be_token_expiration ='0000-00-00 00:00:00',
                                                    be_ipn_push_id='',
                                                    be_outlook_user='',
                                                    be_apple_user='',
                                                    be_apple_pass=''
                                                    where be_id=$employeeID");

        return 1;

    }

    //get google client object
    function getClient($employeeID) {
       
        try{
            $client = new Google_Client();

            $client->setClientId("707755455448-8kdle07n0mco9g07r1p11ljpapa3g6ib.apps.googleusercontent.com");
            $client->setClientSecret("WPlt7qVrd8eBnVXjAgu4zhxp");
            $client->setRedirectUri('postmessage');
            $client->setAccessType("offline");
            $client->setApprovalPrompt('force');
            $client->setScopes(SCOPES);

            // get access token from DB.
            $accessToken = $this->db->getVal("select be_token from tbl_employee where be_id=$employeeID");

            $client->setAccessToken($accessToken);

            // Refresh the token if it's expired.
            if ($client->isAccessTokenExpired() && $client->getRefreshToken() != "") {

                $client->refreshToken($client->getRefreshToken());
                $accessToken =  $client->getAccessToken();
                $this->db->execute("update tbl_employee
                                set be_token='$accessToken'
                                where be_id=$employeeID");
            }
            
            return $client;
        }
        catch(Exception $ex){
            return array();
        }
    }

    //get events from google callendar
    function getEventsList($calendarID, $client, $employeeID,$bizID, $syncToken = ""){

        $service = new Google_Service_Calendar($client);

        // Print the next 10 events on the user's calendar.
        $calendarId = $calendarID;

        if($syncToken != ""){
            $optParams = array(
              'syncToken' => $syncToken
            );
        }
        else{
            $optParams = array(
              'singleEvents' => TRUE,
              'timeMin' => date('c'),
              'showDeleted' => true
            );
        }
        $results = $service->events->listEvents($calendarId, $optParams);

        //print_r($results);

        if (count($results->getItems()) == 0) {

        } else {




            while(true) {
                foreach ($results->getItems() as $event) {

                    if($event->getStatus() == "cancelled"){
                        //printf("ID: %s Deleted<br>", $event->getId());

                        $SQL = "delete from tbl_employee_meeting where em_external_id='{$event->getId()}' and em_emp_id=$employeeID";
                        $this->db->execute($SQL);

                    }else{
                        $start = $event->start->dateTime;
                        $start = (empty($start)) ? $start = $event->start->date : $start = substr($start,0,19);

                        $end = $event->end->dateTime;
                        $end = (empty($end)) ? $end = $event->end->date : $end = substr($end,0,19);

                        $fullStartDate = date('Y-m-d H:i:s', strtotime($start));
                        $startDate = date('Y-m-d', strtotime($start));

                        $fullEndDate = date('Y-m-d H:i:s', strtotime($end));
                        $endDate = date('Y-m-d', strtotime($end));


                        $startID = "0";
                        if (!empty($event->start->dateTime)) {
                            $startTime = date('H:i', strtotime($start));
                            $startID = $this->getTimeID($startTime,"start");
                        }

                        $endID = "0";
                        if (!empty($event->end->dateTime)) {
                            $endTime = date('H:i', strtotime($end));
                            $endID = $this->getTimeID($endTime,"end");
                        }

                        $allDayEvent = ($startID == "0" && $endID == "0") ? "1" : "0";

                        //printf("%s (%s - %s), ColorId: %s, ID: %s<br>", $event->getSummary(), $start, $end,$event->getColorId(),$event->getId());

                        $summery = addslashes($event->getSummary());
                        $description = addslashes($event->getDescription());

                        if($this->externalEventExist($event->getId(),$employeeID)){
                            $SQL = "update tbl_employee_meeting set
                                    em_name='$summery',
                                    em_cust_request = '$description',
                                    em_start_date = '$startDate',
                                    em_start = '$fullStartDate',
                                    em_start_time=$startID,
                                    em_end_date = '$endDate',
                                    em_end = '$fullEndDate',
                                    em_end_time=$endID,
                                    em_all_day_event=$allDayEvent
                                    where em_external_id='{$event->getId()}'
                                    and em_emp_id=$employeeID";
                            $this->db->execute($SQL);
                        }else{
                            $SQL = "insert into tbl_employee_meeting set
                                    em_emp_id=$employeeID,
                                    em_biz_id=$bizID,
                                    em_name='$summery',
                                    em_external_id='{$event->getId()}',
                                    em_cust_request = '$description',
                                    em_source='External',
                                    em_start_date = '$startDate',
                                    em_start = '$fullStartDate',
                                    em_start_time=$startID,
                                    em_end_date = '$endDate',
                                    em_end = '$fullEndDate',
                                    em_end_time=$endID,
                                    em_all_day_event=$allDayEvent";
                            $this->db->execute($SQL);
                        }
                    }
                }
                $pageToken = $results->getNextPageToken();
                if ($pageToken) {
                    $optParams = array('pageToken' => $pageToken);
                    $results = $service->events->listEvents($calendarId, $optParams);
                } else {
                    $this->db->execute("update tbl_employee
                            set be_sync_token='{$results->getNextSyncToken()}'
                            where be_id=$employeeID");
                    //echo $results->getNextSyncToken();
                    break;
                }
            }
        }
    }

    //set all local events to google
    function syncLocalEventsToGoogle($employeeID){

        $eventsTable = $this->db->getTable("select * from tbl_employee_meeting where em_start > now() and em_source = 'Internal' and em_emp_id=$employeeID");
        if(count($eventsTable)>0){
            $client = $this->getClient($employeeID);
            $calRow = $this->getCalendarDB($employeeID);
            foreach ($eventsTable as $eventRow)
            {
                $startTime = strtotime( $eventRow["em_start"] );
                $startdate = date( 'Y-m-d\TH:i:s', $startTime );

                $endTime = strtotime( $eventRow["em_end"] );
                $enddate = date( 'Y-m-d\TH:i:s', $endTime );

                if($eventRow["em_external_id"] == ""){
                    $extEventId = $this->addEvent($calRow["be_cal_id"],$eventRow["em_name"],$eventRow["em_cust_request"],$startdate, $enddate,$client,$calRow["be_event_color"]);
                    if($extEventId != ""){
                        $this->db->execute("update tbl_employee_meeting
                            set em_external_id='$extEventId'
                            where em_id={$eventRow["em_id"]}
                            and em_emp_id=$employeeID");
                    }
                }else{
                    $this->updateEvent($calRow["be_cal_id"],$eventRow["em_external_id"],$eventRow["em_name"],$eventRow["em_cust_request"],$startdate, $enddate,$client,$calRow["be_event_color"]);
                }
            }
        }

    }

    //get all users calendars
    function getCalendarsList($client,$employeeID){

        $service = new Google_Service_Calendar($client);

        $calendarList = $service->calendarList->listCalendarList();

        $calList = array();
        while(true) {
            foreach ($calendarList->getItems() as $calendarListEntry) {

                if(!utilityManager::contains($calendarListEntry->getId(),"v.calendar.google.com")){
                    $entry = array();
                    $entry['name'] = $calendarListEntry->getSummary();
                    $entry['cid'] = $calendarListEntry->getId();
                    array_push($calList,$entry);
                    //$calList .= "<div>{$calendarListEntry->getSummary()} <span class='connectCalendar' style='color:green; cursor:pointer;' data-ctype='Google' data-employee='$employeeID' data-cid='{$calendarListEntry->getId()}'>Connect</span></div><br>";
                }

            }
            $pageToken = $calendarList->getNextPageToken();
            if ($pageToken) {
                $optParams = array('pageToken' => $pageToken);
                $calendarList = $service->calendarList->listCalendarList($optParams);
            } else {
                break;
            }
        }
        return $calList;
    }

    function getCalendarTimeZone($calendarID,$client){

        $service = new Google_Service_Calendar($client);

        $calendar = $service->calendars->get($calendarID);

        return $calendar->getTimeZone();
    }

    function addEvent($calendarID,$summary,$description,$startDayTime, $endDayTime,$client,$color=10,$recurrence=""){
        $description = stripslashes($description);
        $summary = stripslashes($summary);
        $timeZone = $this->getCalendarTimeZone($calendarID,$client);

        $service = new Google_Service_Calendar($client);

        $event = new Google_Service_Calendar_Event(array(
          'summary' => stripslashes($summary),
          'description' => stripslashes($description),
          'start' => array(
            'dateTime' => $startDayTime,
            'timeZone' => $timeZone,
          ),
          'end' => array(
            'dateTime' => $endDayTime,
            'timeZone' => $timeZone,
          ),
          'recurrence' => array(),
          'attendees' => array(),
          'reminders' => array(
            'useDefault' => TRUE
          ),
        ));

        $createdEvent = $service->events->insert($calendarID, $event);
        return $createdEvent->getId();
    }

    function addEventFromArray($sincVals){
        $description = stripslashes($sincVals["notes"]);
        $summary = stripslashes($sincVals["eventName"]);
        $timeZone = $this->getCalendarTimeZone($sincVals["calendarId"],$sincVals["client"]);

        $service = new Google_Service_Calendar($sincVals["client"]);

        $event = new Google_Service_Calendar_Event(array(
          'summary' => stripslashes($summary),
          'description' => stripslashes($description),
          'start' => array(
            'dateTime' => $sincVals["startDate"],
            'timeZone' => $timeZone,
          ),
          'end' => array(
            'dateTime' => $sincVals["endDate"],
            'timeZone' => $timeZone,
          ),
          'recurrence' => array(),
          'attendees' => $sincVals["attendees"],
          'reminders' => array(
            'useDefault' => TRUE
          ),
        ));
       
        
        //$sincVals["attendees"]
        $createdEvent = $service->events->insert($sincVals["calendarId"], $event);
       
        return $createdEvent->getId();
    }

    function deleteEvent($calendarID,$eventID,$client){

        $service = new Google_Service_Calendar($client);
        $service->events->delete($calendarID, $eventID);
    }

    function deleteEventIOS($calRow,$eventID){

        $calendarId = $calRow["be_cal_id"];
        $appleUsername = utilityManager::decryptIt($calRow["be_apple_user"]);
        $applePassword = utilityManager::decryptIt($calRow["be_apple_pass"]);

        $url= $this->iosServer.$calendarId. $eventID . '.ics';

// !!!!!!!!!!!!! do not TAB the code bellow it must stay aligned to left
        $bodyDelete = "<<<__EOD
BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VEVENT
UID:$eventID
END:VEVENT
END:VCALENDAR
__EOD";

        $this->doRequest($appleUsername, $applePassword, $url, $bodyDelete,"DELETE","calendar");
    }

    function deleteEventOutlook($employeeID,$eventID){

        $calRow = $this->getCalendarDB($employeeID);

        $subscriptionUrl = $this->outlookApiUrl."/Me/events/$eventID";

        self::makeApiCall($this->getAccessToken($employeeID),$calRow["be_outlook_user"], "DELETE", $subscriptionUrl);
    }

    function updateEvent($calendarID,$eventID,$summary,$description,$startDayTime, $endDayTime,$client,$color=10){

        $timeZone = $this->getCalendarTimeZone($calendarID,$client);

        $service = new Google_Service_Calendar($client);
        $event = $service->events->get($calendarID, $eventID);

        $start = new Google_Service_Calendar_EventDateTime();
        $start->setDateTime($startDayTime);
        $start->setTimeZone($timeZone);

        $end = new Google_Service_Calendar_EventDateTime();
        $end->setDateTime($endDayTime);
        $end->setTimeZone($timeZone);
        
        $event->setSummary(stripslashes($summary));
        $event->setDescription(stripslashes($description));
        $event->setStart($start);
        $event->setEnd($end);

        $event->setColorId($color);

        $service->events->update($calendarID, $event->getId(), $event);
    }

    function updateEventFromArray($sincVals){

        $timeZone = $this->getCalendarTimeZone($sincVals["calendarId"],$sincVals["client"]);

        $service = new Google_Service_Calendar($sincVals["client"]);
        $event = $service->events->get($sincVals["calendarId"], $sincVals["event_id"]);

        $start = new Google_Service_Calendar_EventDateTime();
        $start->setDateTime($sincVals["startDate"]);
        $start->setTimeZone($timeZone);

        $end = new Google_Service_Calendar_EventDateTime();
        $end->setDateTime($sincVals["endDate"]);
        $end->setTimeZone($timeZone);
        
        $event->setSummary(stripslashes($sincVals["eventName"]));
        $event->setDescription(stripslashes($sincVals["notes"]));
        $event->setStart($start);
        $event->setEnd($end);
        $event->setAttendees($sincVals["attendees"]);
      
        $updatedEvent = $service->events->update($sincVals["calendarId"], $event->getId(), $event);
       
    }

    function entInstances($calendarID,$eventID,$client,$action = "list"){

        $service = new Google_Service_Calendar($client);

        $events = $service->events->instances($calendarID, $eventID);

        while(true) {
            foreach ($events->getItems() as $event) {

                if($action=="list"){
                    //echo $event->getId()."<br>";
                }
                if($action=="delete"){
                    $this->deleteEvent($calendarID,$event->getId(),$client);
                }
            }
            $pageToken = $events->getNextPageToken();
            if ($pageToken) {
                $optParams = array('pageToken' => $pageToken);
                $events = $service->events->instances('primary', "eventId",
                    $optParams);
            } else {
                break;
            }
        }
    }

    //need to be called when creating sync to google
    function createPush($calendarID,$client,$employeeID){

        $service = new Google_Service_Calendar($client);
        $channel =  new Google_Service_Calendar_Channel($client);
        $uuid = utilityManager::gen_uuid();
        $channel->setId($uuid);
        $channel->setType('web_hook');

        $address = URL . "autojobs/googleCalendarIPN/$employeeID";
        $channel->setAddress($address);
        $watchEvent = $service->events->watch($calendarID, $channel);

        $expireDate = date('Y-m-d H:i:s', $watchEvent->expiration /1000);
        $this->db->execute("update tbl_employee
                            set be_ipn_push_id='{$watchEvent->id}',
                            be_ipn_expiration= DATE_SUB('$expireDate', INTERVAL 1 DAY)
                            where be_id=$employeeID");
    }

    //cron to run once a hour
    function updatePushExpiration(){
        $pushTableGoogle = $this->db->getTable("select * from tbl_employee where now() > be_ipn_expiration and be_cal_type = 'Google'");
        if(count($pushTableGoogle)>0){
            foreach ($pushTableGoogle as $calRow)
            {
                $client = $this->getClient($calRow["be_id"]);
            	$this->createPush($calRow["be_cal_id"],$client,$calRow["be_id"]);
            }
        }

        $pushTableOutlook = $this->db->getTable("select * from tbl_employee where now() > be_ipn_expiration and be_cal_type = 'Outlook'");
        if(count($pushTableOutlook)>0){
            foreach ($pushTableOutlook as $calRow)
            {
                $client = $this->getClient($calRow["be_id"]);
            	$this->createPushOutlook($calRow["be_id"]);
            }
        }
    }

    //called by google IPN
    function syncEvents($pushID,$employeeID){

        $exist = $this->db->getVal("select count(be_id) from tbl_employee where be_id=$employeeID and be_ipn_push_id='$pushID'") > 0;
        if($exist){

            $calRow = $this->getCalendarDB($employeeID);

            if($calRow["be_cal_type"] == "Google"){
                $client = $this->getClient($calRow["be_id"]);

                $this->getEventsList($calRow["be_cal_id"], $client, $employeeID,$calRow["be_biz_id"], $calRow["be_sync_token"]);
            }
        }

    }

    //check if event exist by google event ID
    function externalEventExist($eventID,$employeeID){
        return $this->db->getVal("select count(em_id) from tbl_employee_meeting where em_emp_id=$employeeID and em_external_id='$eventID'") > 0;
    }


    //login To Apple Calendar
    function loginToApple($user,$pass,$employeeID){

        $principal_request="<A:propfind xmlns:A='DAV:'>
								<A:prop>
									<A:current-user-principal/>
								</A:prop>
							</A:propfind>";

		$response=simplexml_load_string($this->doRequest($user, $pass, $this->iosServer, $principal_request));
        $resp = array();
       
        if(is_object($response) && is_object($response->response[0]) && utilityManager::contains( (string) $response->response[0]->propstat[0]->status[0],"200")){

            $principal_url=$response->response[0]->propstat[0]->prop[0]->{'current-user-principal'}->href;
            $userID=explode("/", $principal_url);
            $userID= $userID[1];

            $appleUsername = utilityManager::encryptIt($user);
            $applePassword = utilityManager::encryptIt($pass);
            $this->db->execute("update tbl_employee set be_apple_user='$appleUsername',be_apple_pass='$applePassword',be_token='$userID' where be_id=$employeeID");

            $resp["code"] = 1;
            $resp["message"]="";
            $resp["data"]="";
        }else{
            $resp["code"] = 0;
            if(is_object($response) && is_object($response->body[0])){
                
                $resp["message"] = (string) $response->body[0]->h1[0];
                $resp["data"] = (string) $response->body[0]->p[0];
            }
            else{
                $resp["message"] = "no_access_to_ical";
            }
        }
        return $resp;

    }

    //get all calendars from apple
    function calendarListApple($employeeID){
        $calRow = $this->getCalendarDB($employeeID);

        $appleUsername = utilityManager::decryptIt($calRow["be_apple_user"]);
        $applePassword = utilityManager::decryptIt($calRow["be_apple_pass"]);
        $userID = $calRow["be_token"];

        $calendars_request="<A:propfind xmlns:A='DAV:'>
								<A:prop>
									<A:displayname/>
								</A:prop>
							</A:propfind>";
		$url=$this->iosServer."/".$userID."/calendars/";
		$response=simplexml_load_string($this->doRequest($appleUsername, $applePassword, $url, $calendars_request));

        $calList = array();



		foreach($response->response as $cal)
		{
			$entry["href"]=$cal->href;
			if(isset($cal->propstat[0]->prop[0]->displayname)) {
				$calName = $cal->propstat[0]->prop[0]->displayname;
                $urlParts = explode("/",$cal->href);

                if($calName != "" && count($urlParts) > 4 && $urlParts[3] != "notification" && $urlParts[3] != "tasks" && $urlParts[3] != "inbox" && $urlParts[3] != "outbox"){
                    $entry = array();
                    $entry['name'] = (string)$calName;
                    $entry['cid'] = (string)$cal->href;

                    array_push($calList,$entry);
                    //$calList .= "<div>$calName <span class='connectCalendar' style='color:green; cursor:pointer;' data-ctype='IOS' data-employee='$employeeID' data-cid='{$cal->href}'>Connect</span></div><br>";
                }
			}
		}

        return $calList;
    }

    //set all local events to IOS
    function syncLocalEventsToIOS($employeeID){

        $eventsTable = $this->db->getTable("select * from tbl_employee_meeting where em_start > now() and em_source = 'Internal' and em_emp_id=$employeeID");
        if(count($eventsTable)>0){

            $calRow = $this->getCalendarDB($employeeID);
            $appleUsername = utilityManager::decryptIt($calRow["be_apple_user"]);
            $applePassword = utilityManager::decryptIt($calRow["be_apple_pass"]);

            foreach ($eventsTable as $eventRow)
            {
                $startTime = strtotime( $eventRow["em_start"] );
                $startdate = date( 'Y-m-d\TH:i:s', $startTime );

                $endTime = strtotime( $eventRow["em_end"] );
                $enddate = date( 'Y-m-d\TH:i:s', $endTime );
                $eventRow["em_name"] = stripslashes($eventRow["em_name"]);
                $eventRow["em_cust_request"] = stripslashes($eventRow["em_cust_request"]);
                $this->addEventToIOS($appleUsername,$applePassword,$calRow["be_cal_id"],$eventRow["em_id"],$eventRow["em_name"],$eventRow["em_cust_request"],$startdate, $enddate,$calRow["be_event_color"]);
                $this->db->execute("update tbl_employee_meeting
                            set em_external_id='{$eventRow["em_id"]}'
                            where em_id={$eventRow["em_id"]}
                            and em_emp_id=$employeeID");
            }
        }
    }

    //get events from IOS callendar
    function getEventsListFromIOS($employeeID, $cron){

        $calRow = $this->getCalendarDB($employeeID);
        $calendarId = $calRow["be_cal_id"];
        $appleUsername = utilityManager::decryptIt($calRow["be_apple_user"]);
        $applePassword = utilityManager::decryptIt($calRow["be_apple_pass"]);

        $startdate = date( 'Ymd\THis\Z', strtotime("-1 month"));
        if($calRow["be_ipn_expiration"] != "0000-00-00 00:00:00"){
            $startdate = date( 'Ymd\THis\Z',strtotime($calRow["be_ipn_expiration"] . " - 1 month"));
        }
        $enddate = date( 'Ymd\THis\Z', strtotime("+1 year") );

        $url= $this->iosServer.$calendarId;

        $cal_events = '<C:calendar-query xmlns:D="DAV:" xmlns:C="urn:ietf:params:xml:ns:caldav">
                            <D:prop>
                                <C:calendar-data />
                            </D:prop>
                            <C:filter>
                                <C:comp-filter name="VCALENDAR">
                                    <C:comp-filter name="VEVENT">
                                        <C:time-range start="'.$startdate.'" end="'.$enddate.'"/>
                                    </C:comp-filter>
                                </C:comp-filter>
                            </C:filter>
                        </C:calendar-query>';

        //echo "time-range start='$startdate' end='$enddate'<br>";

        $data=simplexml_load_string($this->doRequest($appleUsername, $applePassword, $url, $cal_events,"REPORT"));

        $events = array();
        $timezone = null;
        foreach($data->response as $response) {
            foreach($response->propstat as $propstat) {
                $caldata = $propstat->prop->{'calendar-data'};
                if (strlen($caldata) == 0) { continue; }
                $accum_timezone = array();
                $in_timezone = false;
                $oneEvent = array();
                $in_event = false;
                foreach(explode("\n", $caldata) as $line) {
                    if ($line == "BEGIN:VTIMEZONE" && is_null($timezone)) { $in_timezone = true; }
                    if ($line == "BEGIN:VEVENT") {
                        $in_event = true;
                        $oneEvent = array();
                    }
                    if ($in_timezone) { $accum_timezone[] = $line; }
                    if ($line == "END:VTIMEZONE") {
                        $in_timezone = false;
                        if (count($accum_timezone) > 0) {
                            $timezone = join("\n", $accum_timezone);
                        }
                    }
                    if($in_event){

                        $parts = explode(":",$line);
                        if(count($parts)>1){
                            $key = $parts[0];
                            $val = $parts[1];

                            if(utilityManager::contains($key,"TZID") || utilityManager::contains($key,"VALUE=DATE")){

                                if(utilityManager::contains($key,"DTSTART;VALUE=DATE")){
                                    $oneEvent["ONEDAYEVENT"] = "1";
                                }

                                //$oneEvent[$key] = $val;
                                $keyParts = explode(";",$key);
                                $oneEvent[$keyParts[0]] = $val;

                                $tzParts = explode("=",$keyParts[1]);
                                $oneEvent[$keyParts[0]."-TZID"] = $tzParts[1] == "DATE" ? "" : $tzParts[1];
                            }
                            else{

                                $oneEvent[$key] = $val;
                                if(utilityManager::contains($key,"DTSTART") || utilityManager::contains($key,"DTEND")){
                                    $oneEvent[$key."-"."TZID"] = "";
                                }
                            }
                        }
                    }
                    if ($line == "END:VEVENT") {
                        $in_event = false;
                        $events[] = $oneEvent;
                    }
                }
            }
        }

        $this->db->execute("update tbl_employee set be_ipn_expiration=now() where be_id=$employeeID");


        //echo print_r($events,true);
        //return;

        if (count($events) == 0) {

        } else {




            $arr = array();
            foreach ($events as $event) {

                array_push ($arr, $event["UID"]);

                $start = $event["DTSTART"];
                $end = $event["DTEND"];

                $fullStartDate = date('Y-m-d H:i:s', strtotime($start));
                $startDate = date('Y-m-d', strtotime($start));

                $fullEndDate = date('Y-m-d H:i:s', strtotime($end));
                $endDate = date('Y-m-d', strtotime($end));


                $startID = "0";
                if (!isset($event["ONEDAYEVENT"])) {
                    $startTime = date('H:i', strtotime($start));
                    $startID = $this->getTimeID($startTime,"start");
                }

                $endID = "0";
                if (!isset($event["ONEDAYEVENT"])) {
                    $endTime = date('H:i', strtotime($end));
                    $endID = $this->getTimeID($endTime,"end");
                }


                //printf("%s (%s - %s), ID: %s, TimeZone: %s<br>", $event["SUMMARY"], $start, $end, $event["UID"], $event["DTSTART-TZID"]);

                $summery = addslashes($event["SUMMARY"]);
                $description = isset($event["DESCRIPTION"]) ? $event["DESCRIPTION"] : "";
                $description = str_replace('\n','<br>',$description);
                $description = addslashes($description);
                $allDayEvent = isset($event["ONEDAYEVENT"]) && $event["ONEDAYEVENT"] == "1" ? addslashes($event["ONEDAYEVENT"]) : "0";

                $fh = fopen("debugCalSinc.txt", 'a');
                if($event["UID"] == 11633){
                    fwrite($fh, print_r($event,true)."\n");
                }
                if($this->externalEventExist($event["UID"],$employeeID)){
                    $SQL = "update tbl_employee_meeting set
                                    em_name='$summery',
                                    em_cust_request = '$description',
                                    em_start_date = '$startDate',
                                    em_start = '$fullStartDate',
                                    em_start_time=$startID,
                                    em_end_date = '$endDate',
                                    em_end = '$fullEndDate',
                                    em_end_time=$endID,
                                    em_all_day_event=$allDayEvent
                                    where em_external_id='{$event["UID"]}'
                                    and em_emp_id=$employeeID";


                    if($event["UID"] == 11633){
                        fwrite($fh, $SQL."\n");
                    }


                    $this->db->execute($SQL);
                }else{
                    $SQL = "insert into tbl_employee_meeting set
                                    em_emp_id=$employeeID,
                                    em_biz_id={$calRow["be_biz_id"]},
                                    em_name='$summery',
                                    em_external_id='{$event["UID"]}',
                                    em_cust_request = '$description',
                                    em_source='External',
                                    em_start_date = '$startDate',
                                    em_start = '$fullStartDate',
                                    em_start_time=$startID,
                                    em_end_date = '$endDate',
                                    em_end = '$fullEndDate',
                                    em_end_time=$endID,
                                    em_all_day_event=$allDayEvent";
                    $this->db->execute($SQL);
                }
                fclose($fh);
            }

            //delete from bobile events that was deleted on IOS
            if($cron){

                $events = $this->db->getTable("select em_id,em_external_id from tbl_employee_meeting where em_emp_id=$employeeID and em_source='External'");
                if(count($events)>0){
                    foreach ($events as $event)
                    {
                    	if(!in_array($event["em_external_id"], $arr)){
                            $this->db->execute("delete from tbl_employee_meeting where em_id={$event["em_id"]}");
                        }
                    }
                }
            }
        }
    }

    // add one event to IOS
    function addEventToIOS($user, $pw,$calendarID,$eventID,$summary,$description,$startDayTime, $endDayTime,$color=10,$recurrence="",$attendees = array()){

        $url= $this->iosServer.$calendarID. $eventID . '.ics';
        $tstamp = gmdate("Ymd\THis\Z");
        $cleanDescription = stripSlashes($description);
        $cleanSummary = stripSlashes($summary);

        
        
// !!!!!!!!!!!!! do not TAB the code bellow it must stay aligned to left
    $bodyAdd = "<<<__EOD
BEGIN:VCALENDAR
VERSION:2.0
BEGIN:VEVENT";
foreach ($attendees as $attendee)
{
    if($attendee['organizer']){
        $bodyAdd .="
ORGANIZER;CN=\"".$attendee['displayName']."\":MAILTO:".$attendee['email']."
";
    }
    else{
        $bodyAdd .="
ATTENDEE;CN=\"".$attendee['displayName']."\";RSVP=FALSE;MAILTO:".$attendee['email']."
";
    }
}

$bodyAdd .= "
DTSTAMP:$tstamp
DTSTART:$startDayTime
DTEND:$endDayTime
UID:$eventID
DESCRIPTION:$cleanDescription
SUMMARY:$cleanSummary
END:VEVENT
END:VCALENDAR
__EOD";


        $this->doRequest($user, $pw, $url, $bodyAdd,"PUT","calendar");
        return $eventID;
    }

    //get our time ID acording real time HH:mm
    function getTimeID($time, $type){
        return $this->db->getVal("select hour_id from calendar_hours where hour_$type='$time'");
    }

    //getEmployees to sync with IOS
    function getIOSEmployees(){
        return $this->db->getTable("select be_id from tbl_employee where be_cal_type='IOS' and be_cal_id <> '' and be_token <> ''");
    }

    //getOutlookLoginUrl
    function outlookLoginUrl($redirectUri){

        $scopestr = implode(" ", $this->scopes);

        $loginUrl = $this->authority.sprintf($this->authorizeUrl, $this->clientId, urlencode($redirectUri), urlencode($scopestr));

        return $loginUrl;

    }

    //get outlook token
    function getTokenFromAuthCode($authCode, $redirectUri) {
      return $this->getToken("authorization_code", $authCode, $redirectUri);
    }
    function getToken($grantType, $code, $redirectUri) {
        $parameter_name = $grantType;
        if (strcmp($parameter_name, 'authorization_code') == 0) {
            $parameter_name = 'code';
        }

        // Build the form data to post to the OAuth2 token endpoint
        $token_request_data = array(
          "grant_type" => $grantType,
          $parameter_name => $code,
          "redirect_uri" => $redirectUri,
          "scope" => implode(" ", $this->scopes),
          "client_id" => $this->clientId,
          "client_secret" => $this->clientSecret
        );

        // Calling http_build_query is important to get the data
        // formatted as expected.
        $token_request_body = http_build_query($token_request_data);

        $curl = curl_init($this->authority.$this->tokenUrl);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $token_request_body);

        $response = curl_exec($curl);

        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($httpCode >= 400) {
            return array('errorNumber' => $httpCode,
                         'error' => 'Token request returned HTTP error '.$httpCode);
        }

        // Check error
        $curl_errno = curl_errno($curl);
        $curl_err = curl_error($curl);
        if ($curl_errno) {
            $msg = $curl_errno.": ".$curl_err;
            return array('errorNumber' => $curl_errno,
                         'error' => $msg);
        }

        curl_close($curl);

        // The response is a JSON payload, so decode it into
        // an array.
        $json_vals = json_decode($response, true);


        return $json_vals;
    }
    function getAccessToken($employeeID) {

        $redirectUri = URL.'calendar/outlookAutorize';

        $calendarRow = $this->getCalendarDB($employeeID);

        // Is there an access token in the session?
        $current_token = $calendarRow['be_token'];
        $expiration = strtotime($calendarRow['be_token_expiration']);
        if ($expiration < time()) {
            // Token expired, refresh
            $refresh_token = $calendarRow['be_sync_token'];
            $new_tokens = $this->getTokenFromRefreshToken($refresh_token, $redirectUri);

            $access_token = $new_tokens["access_token"];
            $refresh_token = $new_tokens["refresh_token"];
            $expiration = time() + $new_tokens['expires_in'] - 300;

            $mysqltime = date ("Y-m-d H:i:s", $expiration);

            if($access_token != ""){
                $this->db->execute("update tbl_employee
                            set be_token='$access_token',
                            be_sync_token='$refresh_token',
                            be_token_expiration='$mysqltime'
                            where be_id=$employeeID");
                }

            // Return new token
            return $access_token;
        }
        else {
            // Token is still valid, return it
            return $current_token;
        }
    }
    function getTokenFromRefreshToken($refreshToken, $redirectUri) {
        return $this->getToken("refresh_token", $refreshToken, $redirectUri);
    }

    // create outlook web hook
    function createPushOutlook($employeeID){

        $calRow = $this->getCalendarDB($employeeID);
        $subscriptionUrl = $this->outlookApiUrl."/Me/subscriptions";

        $post = array(
         "@odata.type" => "#Microsoft.OutlookServices.PushSubscription",
         "Resource" => "https://outlook.office.com/api/v2.0/me/calendars/{$calRow["be_cal_id"]}/events",
         "ChangeType" => "Created,Deleted,Updated",
         "NotificationURL" => URL . "autojobs/outlookCalendarIPN/$employeeID"
       );

        $response = self::makeApiCall($this->getAccessToken($employeeID),$calRow["be_outlook_user"], "POST", $subscriptionUrl, json_encode($post));

        $this->db->execute("update tbl_employee
                            set be_ipn_push_id='{$response["Id"]}',
                            be_ipn_expiration= DATE_SUB('{$response["SubscriptionExpirationDateTime"]}', INTERVAL 1 DAY)
                            where be_id=$employeeID");
    }

    //get outlook user
    function getUser($access_token) {
        $getUserParameters = array (
          // Only return the user's display name and email address
          "\$select" => "DisplayName,EmailAddress"
        );

        $getUserUrl = $this->outlookApiUrl."/Me?".http_build_query($getUserParameters);

        return $this->makeApiCall($access_token, "", "GET", $getUserUrl);
    }

    function setOutlook($data,$employeeID){

        $access_token = $data["access_token"];
        $refresh_token = $data["refresh_token"];
        $token_expires = $data["token_expires"];
        $user_email = $data["user_email"];

        $mysqltime = date ("Y-m-d H:i:s", $token_expires);

        $this->db->execute("update tbl_employee
                            set be_token='$access_token',
                            be_outlook_user='$user_email',
                            be_sync_token='$refresh_token',
                            be_token_expiration='$mysqltime',
                            be_cal_type = 'Outlook'
                            where be_id=$employeeID");
    }

    function getOutlookCalendars($employeeID){

        $calendarRow = $this->getCalendarDB($employeeID);

        $getEventsUrl = $this->outlookApiUrl."/Me/calendars";

        $response = $this->makeApiCall($this->getAccessToken($employeeID), $calendarRow["be_outlook_user"], "GET", $getEventsUrl);

        $calList = array();
        foreach($response["value"] as $cal)
        {
            $entry = array();
            $calName = $cal["Name"];
            $entry['name'] = (string)$cal["Name"];
            $entry['cid'] = (string)$cal["Id"];

            if($cal["Name"] != "" && !utilityManager::contains($calName,"holidays") && !utilityManager::contains($calName,"Birthdays")){
                array_push($calList,$entry);
                //$calList .= "<div>$calName <span class='connectCalendar' style='color:green; cursor:pointer;' data-ctype='Outlook' data-employee='$employeeID' data-cid='{$cal["Id"]}'>Connect</span></div><br>";
            }
        }

        return $calList;
    }

    //called by outlook IPN
    function syncEventsOutlook($pushData,$employeeID){

        if(count($pushData) > 0){
                foreach ($pushData as $subscription)
                {
                    $exist = $this->db->getVal("select count(be_id) from tbl_employee where be_id=$employeeID and be_ipn_push_id='{$subscription->SubscriptionId}'") > 0;
                    if($exist){

                        switch($subscription->ChangeType){
                            case "Created":
                                $existEvent = $this->db->getVal("select count(em_id) from tbl_employee_meeting where em_external_id='{$subscription->ResourceData->Id}' and em_emp_id=$employeeID");
                                if($existEvent == 0){
                                    $event = $this->getOutlookSingleEvents($employeeID,$subscription->ResourceData->Id);
                                    $this->inupOutlookSingleEvent($employeeID,$event,true);
                                }
                                break;
                            case "Updated":
                                $event = $this->getOutlookSingleEvents($employeeID,$subscription->ResourceData->Id);
                                if($event["Id"] != ""){
                                    $this->inupOutlookSingleEvent($employeeID,$event);
                                }
                                break;
                            case "Deleted":
                                $SQL = "delete from tbl_employee_meeting where em_external_id='{$subscription->ResourceData->Id}' and em_emp_id=$employeeID";
                                $this->db->execute($SQL);
                                break;
                        }
                    }

                }
            }
    }

    function getOutlookSingleEvents($employeeID,$eventID){

        $calendarRow = $this->getCalendarDB($employeeID);

        $getEventsUrl = $this->outlookApiUrl."/Me/events/$eventID";

        return $this->makeApiCall($this->getAccessToken($employeeID), $calendarRow["be_outlook_user"], "GET", $getEventsUrl);
    }

    //insert or update local event from Outlook IPN
    function inupOutlookSingleEvent($employeeID,$event,$isInsert=false){
        $calRow = $this->getCalendarDB($employeeID);
        $bizTimeZone = $this->db->getVal("select biz_time_zone from tbl_biz where biz_id={$calRow["be_biz_id"]}");
        $bizTimeZone = ($bizTimeZone >= 0) ? "+$bizTimeZone hour" : "$bizTimeZone hour";

        $summery = addslashes($event["Subject"]);
        $description = addslashes($event["BodyPreview"]);

        $start = $event["Start"]["DateTime"]." ".$bizTimeZone;
        //$start = (empty($start)) ? $start = $event->start->date : $start = substr($start,0,19);

        $end = $event["End"]["DateTime"]." ".$bizTimeZone;;
        //$end = (empty($end)) ? $end = $event->end->date : $end = substr($end,0,19);

        $fullStartDate = date('Y-m-d H:i:s', strtotime($start));
        $startDate = date('Y-m-d', strtotime($start));

        $fullEndDate = date('Y-m-d H:i:s', strtotime($end));
        $endDate = date('Y-m-d', strtotime($end));


        $startID = "0";
        if (!empty($event["Start"]["DateTime"])) {
            $startTime = date('H:i', strtotime($start));
            $startID = $this->getTimeID($startTime,"start");
        }

        $endID = "0";
        if (!empty($event["End"]["DateTime"])) {
            $endTime = date('H:i', strtotime($end));
            $endID = $this->getTimeID($endTime,"end");
        }

        $allDayEvent = ($event["IsAllDay"]=="1") ? "1" : "0";


        if($isInsert){
            $SQL = "insert into tbl_employee_meeting set
                                    em_emp_id=$employeeID,
                                    em_biz_id={$calRow["be_biz_id"]},
                                    em_name='$summery',
                                    em_external_id='{$event["Id"]}',
                                    em_cust_request = '$description',
                                    em_source='External',
                                    em_start_date = '$startDate',
                                    em_start = '$fullStartDate',
                                    em_start_time=$startID,
                                    em_end_date = '$endDate',
                                    em_end = '$fullEndDate',
                                    em_end_time=$endID,
                                    em_all_day_event=$allDayEvent";
        }else{
            $SQL = "update tbl_employee_meeting set
                                    em_name='$summery',
                                    em_cust_request = '$description',
                                    em_start_date = '$startDate',
                                    em_start = '$fullStartDate',
                                    em_start_time=$startID,
                                    em_end_date = '$endDate',
                                    em_end = '$fullEndDate',
                                    em_end_time=$endID,
                                    em_all_day_event=$allDayEvent
                                    where em_external_id='{$event["Id"]}'
                                    and em_emp_id=$employeeID";;
            $this->db->execute($SQL);
        }

        //$fh = fopen("debugOutlook.txt", 'a');
        //fwrite($fh, print_r($event,true)."\n");
        //fclose($fh);

        $this->db->execute($SQL);
    }

    //set all local events to outlook
    function syncLocalEventsToOutlook($employeeID){

        $eventsTable = $this->db->getTable("select * from tbl_employee_meeting where em_start > now() and em_source = 'Internal' and em_emp_id=$employeeID");
        if(count($eventsTable)>0){

            foreach ($eventsTable as $eventRow)
            {
                $startTime = strtotime( $eventRow["em_start"] );
                $startdate = date( 'Y-m-d\TH:i:s', $startTime );

                $endTime = strtotime( $eventRow["em_end"] );
                $enddate = date( 'Y-m-d\TH:i:s', $endTime );
                $eventRow["em_name"] = stripslashes($eventRow["em_name"]);
                $eventRow["em_cust_request"] = stripslashes($eventRow["em_cust_request"]);
                $externalID = $this->addEventOutlook($employeeID,$eventRow["em_name"],$eventRow["em_cust_request"],$startdate, $enddate);
                $this->db->execute("update tbl_employee_meeting
                            set em_external_id='$externalID'
                            where em_id={$eventRow["em_id"]}");
            }
        }
    }

    //set all outlook events to local db
    function getOutlookEvents($employeeID){

        $calendarRow = $this->getCalendarDB($employeeID);

        $startdate = date( 'Y-m-d\TH:i:s', strtotime("-1 month"));
        $enddate = date( 'Y-m-d\TH:i:s', strtotime("+1 year") );

        $getEventsUrl = $this->outlookApiUrl."/Me/calendars/{$calendarRow["be_cal_id"]}/calendarview?startDateTime=$startdate&endDateTime=$enddate";

        $response = $this->makeApiCall($this->getAccessToken($employeeID), $calendarRow["be_outlook_user"], "GET", $getEventsUrl);

        foreach($response["value"] as $event)
        {
            $this->inupOutlookSingleEvent($employeeID,$event,true);
        }
    }

    function addEventOutlook($employeeID,$summary,$description,$startDayTime, $endDayTime,$color=10,$recurrence=""){

        $calRow = $this->getCalendarDB($employeeID);
        $bizTimeZone = $this->db->getVal("select tz_zone from tbl_time_zone,tbl_biz where biz_time_zone=tz_id and biz_id={$calRow["be_biz_id"]}");
        $description = stripslashes(stripslashes($description));
        $summary = stripslashes($summary);
        $subscriptionUrl = $this->outlookApiUrl."/Me/calendars/{$calRow["be_cal_id"]}/events";

        $event = array(
         "Subject" => $summary,
         "Start" => array("DateTime" => $startDayTime, "TimeZone" => $bizTimeZone),
         "End" => array("DateTime" => $endDayTime, "TimeZone" => $bizTimeZone),
         "Body" => array("ContentType" => "HTML", "Content" => $description)
       );

        $response = self::makeApiCall($this->getAccessToken($employeeID),$calRow["be_outlook_user"], "POST", $subscriptionUrl, json_encode($event));

        $id = isset($response["Id"]) ? $response["Id"] : 0;

        return $id;
    }

    function updateEventOutlook($employeeID,$eventID,$summary,$description,$startDayTime, $endDayTime,$color=10){

        $calRow = $this->getCalendarDB($employeeID);
        $bizTimeZone = $this->db->getVal("select tz_name from tbl_time_zone,tbl_biz where biz_time_zone=tz_id and biz_id={$calRow["be_biz_id"]}");

        $subscriptionUrl = $this->outlookApiUrl."/Me/events/$eventID";
        $description = stripslashes($description);
        $summary = stripslashes($summary);
        $event = array(
         "Subject" => $summary,
         "Start" => array("DateTime" => $startDayTime, "TimeZone" => $bizTimeZone),
         "End" => array("DateTime" => $endDayTime, "TimeZone" => $bizTimeZone),
         "Body" => array("ContentType" => "HTML", "Content" => $description)
       );

        self::makeApiCall($this->getAccessToken($employeeID),$calRow["be_outlook_user"], "PATCH", $subscriptionUrl, json_encode($event));
    }

    //outlook API call
    function makeApiCall($access_token, $user_email, $method, $url, $payload = NULL) {
        // Generate the list of headers to always send.
        $headers = array(
          "User-Agent: php-tutorial/1.0",         // Sending a User-Agent header is a best practice.
          "Authorization: Bearer ".$access_token, // Always need our auth token!
          "Accept: application/json",             // Always accept JSON response.
          "client-request-id: ".$this->makeGuid(), // Stamp each new request with a new GUID.
          "return-client-request-id: true"       // Tell the server to include our request-id GUID in the response. "X-AnchorMailbox: ".$user_email         // Provider user's email to optimize routing of API call
        );

        $curl = curl_init($url);

        switch(strtoupper($method)) {
            case "GET":
                // Nothing to do, GET is the default and needs no
                // extra headers.
                break;
            case "POST":
                // Add a Content-Type header (IMPORTANT!)
                $headers[] = "Content-Type: application/json";
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
                break;
            case "PATCH":
                // Add a Content-Type header (IMPORTANT!)
                $headers[] = "Content-Type: application/json";
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PATCH");
                curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);
                break;
            case "DELETE":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                exit;
        }

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        //if ($httpCode >= 400) {
        //    return array('errorNumber' => $httpCode,
        //                 'error' => 'Request returned HTTP error '.$httpCode);
        //}

        $curl_errno = curl_errno($curl);
        $curl_err = curl_error($curl);

        if ($curl_errno) {
            $msg = $curl_errno.": ".$curl_err;
            curl_close($curl);
            return array('errorNumber' => $curl_errno,
                         'error' => $msg);
        }
        else {
            curl_close($curl);
            return json_decode($response, true);
        }
    }

    // This function generates a random GUID.
    function makeGuid(){
        if (function_exists('com_create_guid')) {
            error_log("Using 'com_create_guid'.");
            return strtolower(trim(com_create_guid(), '{}'));
        }
        else {
            error_log("Using custom GUID code.");
            $charid = strtolower(md5(uniqid(rand(), true)));
            $hyphen = chr(45);
            $uuid = substr($charid, 0, 8).$hyphen
                   .substr($charid, 8, 4).$hyphen
                   .substr($charid, 12, 4).$hyphen
                   .substr($charid, 16, 4).$hyphen
                   .substr($charid, 20, 12);

            return $uuid;
        }
    }

    function doRequest($user, $pw, $url, $xml,$requestType="PROPFIND",$contentType="xml")
	{

        $headers = array(
                "Depth: 1",
                "Content-Type: text/$contentType; charset='UTF-8'",
                "User-Agent: DAVKit/4.0.1 (730); CalendarStore/4.0.1 (973); iCal/4.0.1 (1374); Mac OS X/10.6.2 (10C540)"
            );


		//Init cURL
		$c=curl_init($url);
		//Set headers
		curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($c, CURLOPT_HEADER, 0);
        curl_setopt($c, CURLOPT_USERAGENT, 'curl/7.35.0');
		//Set SSL
		curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($c, CURLOPT_SSL_VERIFYPEER, true);
		//Set HTTP Auth
		curl_setopt($c, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($c, CURLOPT_USERPWD, $user.":".$pw);
		//Set request and XML
		curl_setopt($c, CURLOPT_CUSTOMREQUEST, $requestType);
		curl_setopt($c, CURLOPT_POSTFIELDS, $xml);
		curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		//Execute
		$data=curl_exec($c);
		//Close cURL
		curl_close($c);

        //print_r($data);
        //exit;

       


		return $data;
	}
}
?>