<?php

/**
 * ecommerceManager short summary.
 *
 * @version 1.0
 * @author Dany
 */
class ecommerceManager extends Manager
{
    function __construct()
    {
        parent::__construct();
    }

    /************************************* */
    /*   BASIC CASHDRAWER - PUBLIC           */
    /************************************* */

    /**
     * Insert new cashDrawerObject to DB
     * Return Data = new cashDrawerObject ID
     * @param cashDrawerObject $cashDrawerObj 
     * @return resultObject
     */
    public static function addCashDrawer(cashDrawerObject $cashDrawerObj){       
        try{
            $instance = new self();
            $newId = $instance->addCashDrawerDB($cashDrawerObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($cashDrawerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get cashDrawer from DB for provided ID
     * * Return Data = cashDrawerObject
     * @param int $cashDrawerId 
     * @return resultObject
     */
    public static function getCashDrawerByID($cashDrawerId){
        
        try {
            $instance = new self();
            $cashDrawerData = $instance->loadCashDrawerFromDB($cashDrawerId);
            
            $cashDrawerObj = cashDrawerObject::withData($cashDrawerData);
            $result = resultObject::withData(1,'',$cashDrawerObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cashDrawerId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param cashDrawerObject $cashDrawerObj 
     * @return resultObject
     */
    public static function updateCashDrawer(cashDrawerObject $cashDrawerObj){        
        try{
            $instance = new self();
            $instance->upateCashDrawerDB($cashDrawerObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($cashDrawerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete cashDrawer from DB
     * @param int $cashDrawerID 
     * @return resultObject
     */
    public static function deleteCashDrawerById($cashDrawerID){
        try{
            $instance = new self();
            $instance->deleteCashDrawerByIdDB($cashDrawerID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cashDrawerID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Summary of startNewCashDrawer
     * @param mixed $bizID 
     * @param mixed $deviceID 
     * @param mixed $accountID 
     * @param mixed $startingAmount 
     * @return resultObject
     */
    public static function startNewCashDrawer($bizID,$deviceID,$accountID,$startingAmount){

        $cashDrawerObject = new cashDrawerObject();
        $cashDrawerObject->cd_biz_id = $bizID;
        $cashDrawerObject->cd_device_id = $deviceID;
        $cashDrawerObject->cd_opener_account_id = $accountID;
        $cashDrawerObject->cd_start_cash = $startingAmount;
        $cashDrawerObject->cd_expected_total = $startingAmount;
        $cashDrawerObject->cd_actual_total = $startingAmount;

        return self::addCashDrawer($cashDrawerObject);
    }

    /**
     * Summary of closeCashDrawer
     * @param mixed $drawerID 
     * @param mixed $actualCash 
     * @param mixed $notes 
     * @return resultObject
     */
    public static function closeCashDrawer($drawerID,$actualCash,$notes){

        $result = self::getCashDrawerByID($drawerID);

        if($result->code == 1){
            $cashDrawerObject = $result->data;
            $cashDrawerObject->cd_actual_total = $actualCash;
            $cashDrawerObject->cd_notes = $notes;
            $cashDrawerObject->cd_end = "NOW()";

            $cashDrawerObject->closeDrawer();

            $result = self::updateCashDrawer($cashDrawerObject);

            if($result->code == 1){
                $result->data = $cashDrawerObject->AdminAPIArray();
                return $result;
            }else{
                return $result;
            }
        }else{
            return $result;
        }
    }

    /**
     * Summary of getCashDrawerHistory
     * @param mixed $bizID 
     * @param mixed $from 
     * @param mixed $to 
     * @param mixed $accountID 
     * @param mixed $existing 
     * @param mixed $take 
     * @return resultObject
     */
    public static function getCashDrawerHistory($bizID,$accountID = 0,$existing = 0,$take = 25){

        $instance = new self();
        $resultData = array();

        $addAccountSQL = ($accountID > 0) ? " AND (cd_opener_account_id = $accountID OR cd_closer_account_id = $accountID) " : "";
        $addLimits = ($take == 0) ? "" : " LIMIT $existing,$take ";

        $draws = $instance->db->getTable("SELECT * FROM tbl_cash_drawer 
                                            WHERE cd_biz_id = $bizID
                                            $addAccountSQL
                                            ORDER BY cd_start DESC
                                            $addLimits");

        if(count($draws) > 0){
            foreach ($draws as $drawer)
            {
                $drawerObject = cashDrawerObject::withData($drawer);
            	array_push($resultData,$drawerObject->AdminAPIArray());
            }            
        }

        return resultObject::withData(1,"",$resultData);
    }


    /************************************* */
    /*   BASIC CASHDRAWER - DB METHODS           */
    /************************************* */

    private function addCashDrawerDB(cashDrawerObject $obj){

        if (!isset($obj)){
            throw new Exception("cashDrawerObject value must be provided");             
        }

        $cd_endDate = isset($obj->cd_end) ? "'".$obj->cd_end."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_cash_drawer SET 
                                        cd_biz_id = {$obj->cd_biz_id},
                                        cd_device_id = '".addslashes($obj->cd_device_id)."',
                                        cd_opener_account_id = {$obj->cd_opener_account_id},
                                        cd_closer_account_id = {$obj->cd_closer_account_id},
                                        cd_end = $cd_endDate,
                                        cd_start_cash = {$obj->cd_start_cash},
                                        cd_cash_sales_total = {$obj->cd_cash_sales_total},
                                        cd_refunds_total = {$obj->cd_refunds_total},
                                        cd_non_sale_total = {$obj->cd_non_sale_total},
                                        cd_expected_total = {$obj->cd_expected_total},
                                        cd_actual_total = {$obj->cd_actual_total},
                                        cd_notes = '".addslashes($obj->cd_notes)."'
         ");
        return $newId;
    }

    private function loadCashDrawerFromDB($cashDrawerID){

        if (!is_numeric($cashDrawerID) || $cashDrawerID <= 0){
            throw new Exception("Illegal value $cashDrawerID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cash_drawer WHERE cd_id = $cashDrawerID");
    }

    private function upateCashDrawerDB(cashDrawerObject $obj){

        if (!isset($obj->cd_id) || !is_numeric($obj->cd_id) || $obj->cd_id <= 0){
            throw new Exception("cashDrawerObject value must be provided");             
        }

        $cd_startDate = isset($obj->cd_start) ? "'".$obj->cd_start."'" : "null";

        $cd_endDate = isset($obj->cd_end) ? $obj->cd_end : "null";

        $this->db->execute("UPDATE tbl_cash_drawer SET 
                                cd_biz_id = {$obj->cd_biz_id},
                                cd_device_id = '".addslashes($obj->cd_device_id)."',
                                cd_opener_account_id = {$obj->cd_opener_account_id},
                                cd_closer_account_id = {$obj->cd_closer_account_id},
                                cd_start = $cd_startDate,
                                cd_end = $cd_endDate,
                                cd_start_cash = {$obj->cd_start_cash},
                                cd_cash_sales_total = {$obj->cd_cash_sales_total},
                                cd_refunds_total = {$obj->cd_refunds_total},
                                cd_non_sale_total = {$obj->cd_non_sale_total},
                                cd_expected_total = {$obj->cd_expected_total},
                                cd_actual_total = {$obj->cd_actual_total},
                                cd_notes = '".addslashes($obj->cd_notes)."'
                                WHERE cd_id = {$obj->cd_id} 
         ");
    }

    private function deleteCashDrawerByIdDB($cashDrawerID){

        if (!is_numeric($cashDrawerID) || $cashDrawerID <= 0){
            throw new Exception("Illegal value $cashDrawerID");             
        }

        $this->db->execute("DELETE FROM tbl_cash_drawer WHERE cd_id = $cashDrawerID");
    }

    /************************************* */
    /*   BASIC CASHDRAWERENTRY - PUBLIC           */
    /************************************* */

    /**
     * Insert new cashDrawerEntryObject to DB
     * Return Data = new cashDrawerEntryObject ID
     * @param cashDrawerEntryObject $cashDrawerEntryObj 
     * @return resultObject
     */
    public static function addCashDrawerEntry(cashDrawerEntryObject $cashDrawerEntry){       
        try{
            $instance = new self();

            $newId = $instance->addCashDrawerEntryDB($cashDrawerEntry);         
            $cashDrawerEntry->cde_id = $newId;

            $result = self::getCashDrawerByID($cashDrawerEntry->cde_drawer_id);
            if($result->code == 1){
                $cashDrawer = $result->data;

                switch($cashDrawerEntry->cde_entry_type){
                    case "sale":
                        $cashDrawer->cd_cash_sales_total += $cashDrawerEntry->cde_total;
                        $cashDrawer->cd_expected_total += $cashDrawerEntry->cde_total;
                        break;
                    case "refund":
                        $cashDrawer->cd_refunds_total += $cashDrawerEntry->cde_total;
                        $cashDrawer->cd_expected_total += $cashDrawerEntry->cde_total;
                        break;
                    case "cash_in":
                    case "cash_out":
                        $cashDrawer->cd_non_sale_total += $cashDrawerEntry->cde_total;
                        $cashDrawer->cd_expected_total += $cashDrawerEntry->cde_total;
                        break;
                }

                $result = self::updateCashDrawer($cashDrawer);
                if($result->code == 1){
                    $result->data = $cashDrawerEntry->AdminAPIArray();
                    return $result;
                }else{
                    return $result;
                }
            }else{
                return $result;
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($cashDrawerEntry));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get cashDrawerEntry from DB for provided ID
     * * Return Data = cashDrawerEntryObject
     * @param int $cashDrawerEntryId 
     * @return resultObject
     */
    public static function getCashDrawerEntryByID($cashDrawerEntryId){
        
        try {
            $instance = new self();
            $cashDrawerEntryData = $instance->loadCashDrawerEntryFromDB($cashDrawerEntryId);
            
            $cashDrawerEntryObj = cashDrawerEntryObject::withData($cashDrawerEntryData);
            $result = resultObject::withData(1,'',$cashDrawerEntryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cashDrawerEntryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param cashDrawerEntryObject $cashDrawerEntryObj 
     * @return resultObject
     */
    public static function updateCashDrawerEntry(cashDrawerEntryObject $cashDrawerEntryObj){        
        try{
            $instance = new self();
            $instance->upateCashDrawerEntryDB($cashDrawerEntryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($cashDrawerEntryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete cashDrawerEntry from DB
     * @param int $cashDrawerEntryID 
     * @return resultObject
     */
    public static function deleteCashDrawerEntryById($cashDrawerEntryID){
        try{
            $instance = new self();
            $instance->deleteCashDrawerEntryByIdDB($cashDrawerEntryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cashDrawerEntryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Summary of getCashDrawerSales
     * @param mixed $drawerID 
     * @param mixed $existing 
     * @param mixed $take 
     * @return resultObject
     */
    public static function getCashDrawerSales($drawerID,$existing = 0,$take = 25){

        $instance = new self();
        $resultData = array();

        $addLimits = ($take == 0) ? "" : " LIMIT $existing,$take ";

        $draws = $instance->db->getTable("SELECT * FROM tbl_cash_drawer_entry LEFT JOIN tbl_cust_transaction_order ON cde_order_id = cto_id
                                            WHERE cde_drawer_id = $drawerID
                                            AND cde_entry_type='sale'
                                            ORDER BY cde_time DESC
                                            $addLimits");

        if(count($draws) > 0){
            foreach ($draws as $drawerEntry)
            {
                $drawerEntryObject = cashDrawerEntryObject::withData($drawerEntry);
            	array_push($resultData,$drawerEntryObject->AdminAPIArray());
            }            
        }

        return resultObject::withData(1,"",$resultData);

    }

    /**
     * Summary of getCashDrawerRefunds
     * @param mixed $drawerID 
     * @param mixed $existing 
     * @param mixed $take 
     * @return resultObject
     */
    public static function getCashDrawerRefunds($drawerID,$existing = 0,$take = 25){

        $instance = new self();
        $resultData = array();

        $addLimits = ($take == 0) ? "" : " LIMIT $existing,$take ";

        $draws = $instance->db->getTable("SELECT * FROM tbl_cash_drawer_entry LEFT JOIN tbl_cust_transaction_order ON cde_order_id = cto_id 
                                            WHERE cde_drawer_id = $drawerID
                                            AND cde_entry_type='refund'
                                            ORDER BY cde_time DESC
                                            $addLimits");

        if(count($draws) > 0){
            foreach ($draws as $drawerEntry)
            {
                $drawerEntryObject = cashDrawerEntryObject::withData($drawerEntry);
            	array_push($resultData,$drawerEntryObject->AdminAPIArray());
            }            
        }

        return resultObject::withData(1,"",$resultData);
    }

    /**
     * Summary of getCashDrawerNonSales
     * @param mixed $drawerID 
     * @param mixed $existing 
     * @param mixed $take 
     * @return resultObject
     */
    public static function getCashDrawerNonSales($drawerID,$existing = 0,$take = 25){

        $instance = new self();
        $resultData = array();

        $addLimits = ($take == 0) ? "" : " LIMIT $existing,$take ";

        $draws = $instance->db->getTable("SELECT * FROM tbl_cash_drawer_entry LEFT JOIN tbl_cust_transaction_order ON cde_order_id = cto_id 
                                            WHERE cde_drawer_id = $drawerID
                                            AND (cde_entry_type='cash_in' OR cde_entry_type='cash_out')
                                            ORDER BY cde_time DESC
                                            $addLimits");

        if(count($draws) > 0){
            foreach ($draws as $drawerEntry)
            {
                $drawerEntryObject = cashDrawerEntryObject::withData($drawerEntry);
            	array_push($resultData,$drawerEntryObject->AdminAPIArray());
            }            
        }

        return resultObject::withData(1,"",$resultData);
    }

    /************************************* */
    /*   BASIC CASHDRAWERENTRY - DB METHODS           */
    /************************************* */

    private function addCashDrawerEntryDB(cashDrawerEntryObject $obj){

        if (!isset($obj)){
            throw new Exception("cashDrawerEntryObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_cash_drawer_entry SET 
                                        cde_drawer_id = {$obj->cde_drawer_id},
                                        cde_account_id = {$obj->cde_account_id},
                                        cde_entry_type = '{$obj->cde_entry_type}',
                                        cde_order_id = {$obj->cde_order_id},
                                        cde_reason = '".addslashes($obj->cde_reason)."',
                                        cde_total = {$obj->cde_total}
         ");
        return $newId;
    }

    private function loadCashDrawerEntryFromDB($cashDrawerEntryID){

        if (!is_numeric($cashDrawerEntryID) || $cashDrawerEntryID <= 0){
            throw new Exception("Illegal value $cashDrawerEntryID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cash_drawer_entry LEFT JOIN tbl_cust_transaction_order ON cde_order_id = cto_id WHERE cde_id = $cashDrawerEntryID");
    }

    private function upateCashDrawerEntryDB(cashDrawerEntryObject $obj){

        if (!isset($obj->cde_id) || !is_numeric($obj->cde_id) || $obj->cde_id <= 0){
            throw new Exception("cashDrawerEntryObject value must be provided");             
        }

        $cde_timeDate = isset($obj->cde_time) ? "'".$obj->cde_time."'" : "null";

        $this->db->execute("UPDATE tbl_cash_drawer_entry SET 
                                cde_drawer_id = {$obj->cde_drawer_id},
                                cde_account_id = {$obj->cde_account_id},
                                cde_entry_type = '{$obj->cde_entry_type}',
                                cde_order_id = {$obj->cde_order_id},
                                cde_reason = '".addslashes($obj->cde_reason)."',
                                cde_time = $cde_timeDate,
                                cde_total = {$obj->cde_total}
                                WHERE cde_id = {$obj->cde_id} 
         ");
    }

    private function deleteCashDrawerEntryByIdDB($cashDrawerEntryID){

        if (!is_numeric($cashDrawerEntryID) || $cashDrawerEntryID <= 0){
            throw new Exception("Illegal value $cashDrawerEntryID");             
        }

        $this->db->execute("DELETE FROM tbl_cash_drawer_entry WHERE cde_id = $cashDrawerEntryID");
    }
}