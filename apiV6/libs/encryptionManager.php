<?php

/**
 * encryptionManager short summary.
 *
 * encryptionManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class encryptionManager extends Manager
{
    public static function createJWTToken($payload,$secret = JWT_SECRET){
        // Create token header as a JSON string
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);

        // Create token payload as a JSON string
        $encodedPayload = json_encode($payload);

        // Encode Header to Base64Url String
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        // Encode Payload to Base64Url String
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($encodedPayload));

        // Create Signature Hash
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, $secret, true);
        
        // Encode Signature to Base64Url String
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        // Create JWT
        $jwt = $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

        return $jwt;
    }

    public static function isTokenSignatureValid($token,$secret = JWT_SECRET){
        $tokenParts = explode('.',$token);

        $signature = $tokenParts[2];
       
        $verifiedSignature = hash_hmac('sha256', $tokenParts[0] . "." . $tokenParts[1], $secret, true);
        $base64VerifiedSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($verifiedSignature));
        return $signature == $base64VerifiedSignature;
    }

    public static function getJWTPayload($token){
        $tokenParts = explode('.',$token);

        $payload = str_replace(['-', '_', ''],['+', '/', '='], base64_decode($tokenParts[1]));

        return json_decode($payload);
    }

    public static function getJWTPayloadDirect($token){
        $tokenParts = explode('.',$token);

        $payload = base64_decode($tokenParts[1]);

        return json_decode($payload);
    }
}