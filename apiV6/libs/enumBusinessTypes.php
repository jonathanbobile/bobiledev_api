<?php

/**
 * enumBusinessTypes short summary.
 *
 * enumBusinessTypes description.
 *
 * @version 1.0
 * @author Jonathan
 */
class enumBusinessTypes extends Enum
{
    const service_providers = 1;

    const retail = 2;

    const dining = 3;
}