<?php

/**
 * errorManager short summary.
 *
 * errorManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class errorManager extends Manager
{
    function __construct()
    {
        parent::__construct();        
    }
    
    /**
     * saves Error to DB
     * can recive array with keys:
     * error - server error
     * location - where the error happened
     * data - ARRAY - the data that was sent to server
     * response - responce from the server if JS
     */
    public function seveErrorLog($POST = array()){
        
        $error = (isset($POST["error"])) ? addslashes(stripslashes($POST["error"])) : "";
        $location = (isset($POST["location"])) ? addslashes(stripslashes($POST["location"])) : "";
        $data = (isset($POST["data"]) && is_array($POST["data"])) ? addslashes(stripslashes(print_r($POST["data"],true))) : "";
        $response = (isset($POST["response"])) ? addslashes(stripslashes($POST["response"])) : "";
        $bizID = (isset($_SESSION["appData"]["biz_id"])) ? $_SESSION["appData"]["biz_id"] : 0;
        
        ob_start();
        debug_print_backtrace();
        $stack = addslashes(ob_get_clean());
        ob_end_flush(); 
        
        $sql = "insert into tbl_errors set 
                err_biz_id=$bizID,
                err_error='$error',
                err_location='$location',
                err_data='$data',
                err_response='$response',
                err_stack='$stack'";

        $this->db->execute($sql);
    }
    
    public static function addErrorLog($location,$error,$data="",$response=""){
        $stack = '';
        $instance = new self();
        
        $error = ($error != "") ? addslashes(stripslashes($error)) : "";
        $location = ($location != "") ? addslashes(stripslashes($location)) : "";
        $data = ($data != "") ? addslashes(stripslashes($data)) : "";
        $response = ($response != "") ? addslashes(stripslashes($response)) : "";
        $bizID = (isset($_SESSION["appData"]["biz_id"])) ? $_SESSION["appData"]["biz_id"] : 0;
        error_reporting(E_ERROR);
        try{
            ob_start();
            debug_print_backtrace();
            $stack = addslashes(ob_get_clean());
            ob_end_flush();       
        }
        catch(Exception $ex){
        } 
        $sql = "insert into tbl_errors set 
                err_biz_id=$bizID,
                err_error='$error',
                err_location='$location',
                err_data='$data',
                err_response='$response',
                err_stack='$stack'
                ";

        $instance->db->execute($sql);
    }

    public static function addAPIErrorLog($location,$error,$data="",$response="",$bizID = 0){
        $stack = '';
        $instance = new self();
        
        $error = ($error != "") ? addslashes(stripslashes($error)) : "";
        $location = ($location != "") ? addslashes(stripslashes($location)) : "";
        $data = ($data != "") ? addslashes(stripslashes($data)) : "";
        $response = ($response != "") ? addslashes(stripslashes($response)) : "";
        
        error_reporting(E_ERROR);
        try{
            ob_start();
            debug_print_backtrace();
            $stack = addslashes(ob_get_clean());
            ob_end_flush();       
        }
        catch(Exception $ex){
        } 
        $stringifedData = json_encode($data);

        $sql = "insert into tbl_errors_api set 
                apierr_biz_id=$bizID,
                apierr_error='$error',
                apierr_location='$location',
                apierr_data='$stringifedData',
                apierr_response='$response',
                apierr_stack='$stack'
                ";

        $instance->db->execute($sql);
    }
}
