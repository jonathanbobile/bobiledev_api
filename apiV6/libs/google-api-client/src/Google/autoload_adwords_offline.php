<?php
/*
 * Copyright 2014 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

spl_autoload_register(
    function ($className) {
        set_include_path(dirname(__FILE__)."/../"); 

        $clasParts = explode("\\",$className);
        $className = end($clasParts);

        $filePath = dirname(__FILE__) . "/AdsApi/Common/$className.php";
        echo $filePath."<br>";
        if (file_exists($filePath)) {
            require_once($filePath);
        }else{

            $filePath = dirname(__FILE__) . "/AdsApi/AdWords/$className.php";
            echo $filePath."<br>";
            if (file_exists($filePath)) {
                require_once($filePath);
            }else{
                $filePath = dirname(__FILE__) . "/AdsApi/AdWords/v201809/$className.php";
                echo $filePath."<br>";
                if (file_exists($filePath)) {
                    require_once($filePath);
                }else{
                    $filePath = dirname(__FILE__) . "/AdsApi/Common/Util/$className.php";
                    echo $filePath."<br>";
                    if (file_exists($filePath)) {
                        require_once($filePath);
                    }
                }
            }
        }
    }
);
