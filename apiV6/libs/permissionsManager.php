<?php

/**
 * permissionsManager short summary.
 *
 * permissionsManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class permissionsManager
{
    protected $db;    
   
    
    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }  
    }

    public function getBizRoles($bizID,$show_manager,$show_reseller = false){
        $instance = new self();
        $appManagerSql = $show_manager ? "" : "AND rb_id <> 1";

        $supportSQL = $show_reseller ? "" : "AND rb_reseller_only = 0";
        
        $sql = "SELECT * FROM tbl_role_biz WHERE rb_biz_id = $bizID OR (rb_biz_id = 0 $appManagerSql $supportSQL)";

        $result = $instance->db->getTable($sql);

        return $result;
    }

    public function getRole($biz_id,$role_id){
        $role = array();

        $sql = "SELECT * FROM tbl_role_biz WHERE rb_id = $role_id AND (rb_biz_id = $biz_id OR rb_biz_id = 0)";
        
        $role = $this->db->getRow($sql);

        $role['permissions'] = $this->getPermissionTree($biz_id,$role_id);

        return $role;
    }

    public function getPermissionTree($biz_id,$role_id,$for_account = 0){
        //flat is not really necessary. Keeping it for now
        $tree = $this->getPermissionBranch($biz_id,$role_id,0,$for_account);

        return $tree;
    }

    public function getPermissionBranch($biz_id,$role_id,$branch_id,$for_account = 0){

        $innerSQL = "SELECT * FROM tbl_biz_role_permission 
                            WHERE brp_biz_id = $biz_id AND brp_role_id = $role_id";

        if($for_account != 0){
            $innerSQL = "SELECT * FROM tbl_biz_role_permission,tbl_account_role_biz 
                           WHERE brp_biz_id = arb_biz_id 
                           AND brp_role_id = arb_role_id 
                           AND arb_account_id = $for_account AND arb_biz_id = $biz_id";
        }

        $sql = "SELECT * FROM (SELECT tbl_permission_tree.*,
                        CASE 
                            WHEN brp_permission_type is NULL THEN 0
                            WHEN brp_permission_type = 'blocked' THEN 0
                            WHEN brp_permission_type = 'selfonly' THEN 1
                            ELSE 2
                        END as role_permission
                FROM tbl_permission_tree
                LEFT JOIN ($innerSQL) s 
                            ON brp_area_id = pa_id
                WHERE pa_parent_id = $branch_id
                AND pa_permission_type <> 'obsolete'
                GROUP BY pa_id) s
                ORDER BY pa_index ASC, role_permission DESC";

        $branch = $this->db->getTable($sql);

        if(count($branch) == 0){
            return 0;
        }

        $result = array();
        
        foreach ($branch as $key => $leaf)
        {
        	$inner = $this->getPermissionBranch($biz_id,$role_id,$leaf['pa_id'],$for_account);

            $result[$leaf['pa_system_name']] = $leaf;
            if(is_array($inner)){
                $result[$leaf['pa_system_name']]['inner'] = $inner;
            }
           
            

        }

       

        return $result;
        
    }

    public function updateRole($data,$biz_id){
        try{
            $role_id = $data['roleid'];
            

            if($role_id > 2){
                $sql = "UPDATE tbl_role_biz SET
                    rb_role_name = '".addslashes($data['name'])."',
                    rb_role_description = '".addslashes($data['desc'])."'
                WHERE rb_id = $role_id AND rb_biz_id = $biz_id";

                $this->db->execute($sql);
            }

            $sql = "DELETE FROM tbl_biz_role_permission WHERE brp_biz_id = $biz_id AND brp_role_id = $role_id";

            $this->db->execute($sql);

            if(isset($data['permissions']) && count($data['permissions']) > 0){
                foreach ($data['permissions'] as $permission)
                {
                    $area = $permission['area'];
                    $type = $permission['set'];

                    $sql = "INSERT INTO tbl_biz_role_permission SET
                        brp_biz_id = $biz_id,
                        brp_role_id = $role_id,
                        brp_area_id = $area,
                        brp_permission_type = '$type'";

                    $this->db->execute($sql);
                }
            }
            
            return 1;
        }
        catch(Error $er){
            return 0;
        }

    }

    public function addBizRole($biz_id,$data){
        try{
            $result = array();
            $sql = "SELECT COUNT(*) FROM tbl_role_biz WHERE rb_biz_id = $biz_id AND rb_role_name = '".addslashes($data['role_name'])."'";

            $exists = $this->db->getVal($sql);

            if($exists > 0){
                $result['code'] = -1;
                return $result;
            }

            $sql = "INSERT INTO tbl_role_biz SET
                    rb_role_name = '".addslashes($data['role_name'])."',
                    rb_role_description = '".addslashes($data['role_desc'])."',
                    rb_biz_id = $biz_id";

            $roleid = $this->db->execute($sql);

            $sql = "INSERT INTO tbl_biz_role_permission SET
                        brp_biz_id = $biz_id,
                        brp_role_id = $roleid,
                        brp_area_id = 2,
                        brp_permission_type = 'full'";

            $this->db->execute($sql);

            $result['code'] = 1;
            $result['role_id'] = $roleid;

            return $result;

        }
        catch(Error $er){
            $result = array();

            $result['code'] = 0;
            

            return $result;
        }
    }

    public function deleteRole($role_id,$biz_id){

        $sql = "SELECT COUNT(*) FROM tbl_account_role_biz WHERE arb_role_id = $role_id AND arb_biz_id = $biz_id";

        $connected = $this->db->getVal($sql);

        if($connected > 0){
            return 0;
        }

        $sql = "DELETE FROM tbl_account_role_biz WHERE arb_role_id = $role_id AND arb_biz_id = $biz_id";

        $this->db->execute($sql);

        $sql = "DELETE FROM tbl_role_biz WHERE rb_id = $role_id AND rb_biz_id = $biz_id";

        $this->db->execute($sql);

        $sql = "DELETE FROM tbl_biz_role_permission WHERE brp_role_id = $role_id AND brp_biz_id = $biz_id";

        $this->db->execute($sql);

        return 1;
    }

    public function getBizAccounts($biz_id,$is_owner){
        $ownerSQL = "";
        if($is_owner){
            $ownerSQL = "SELECT tbl_account.* FROM tbl_account,tbl_reseller_clients,tbl_client_biz 
                    WHERE ac_id = reseller_client_account_id 
                    AND reseller_client_id = client_biz_client_id 
                    AND client_biz_biz_id = $biz_id
                    AND (client_biz_active = 1 OR (client_biz_active = 0 AND client_biz_isActivated = 0))
                UNION";
        }
        $sql = "$ownerSQL
                SELECT tbl_account.* FROM tbl_account,tbl_guests
				    WHERE ac_id = gu_account_id
                    AND gu_biz_id = $biz_id
                   
                    AND gu_isrevoked = 0";

        $result = $this->db->getTable($sql);

        return $result;
    }

    public function getAccountsForRole($role_id,$biz_id){
        $sql = "SELECT tbl_account.*,
                    CASE 
                        WHEN arb_account_id IS NULL THEN 0
                        ELSE 1
                    END as is_connected
                    FROM tbl_account
                    INNER JOIN tbl_guests ON gu_account_id = ac_id
                    LEFT JOIN (SELECT * FROM tbl_account_role_biz WHERE arb_role_id = $role_id AND arb_biz_id = $biz_id) s 
                    ON arb_account_id = ac_id
				    WHERE ac_id = gu_account_id
                    AND gu_biz_id = $biz_id
                    AND gu_isrevoked = 0";

        $accounts = $this->db->getTable($sql);

        return $accounts;
    }

    public function getAccountPermissions($biz_id,$account_id,$isReseller = false){
        $sql = "SELECT * FROM tbl_account WHERE ac_id = $account_id";

        $account = $this->db->getRow($sql);

        $managerSQL = "rb_biz_id = $biz_id OR (rb_biz_id = 0 AND rb_id <> 1)";

        if($account['ac_type'] == 'reseller_client'){
            $managerSQL = "rb_id = 1";
        }

        $resellerSql = "";
        if(!$isReseller){
            $resellerSql = " AND rb_reseller_only = 0";
        }

        $sql = "SELECT tbl_role_biz.*,
                    CASE 
                        WHEN arb_account_id IS NULL THEN 0
                        ELSE 1
                    END as is_connected
                FROM tbl_role_biz 
                LEFT JOIN (SELECT * FROM tbl_account_role_biz WHERE arb_account_id = $account_id AND arb_biz_id = $biz_id) s 
                ON rb_id = arb_role_id 
                WHERE $managerSQL $resellerSql";

        $account['roles'] = $this->db->getTable($sql);

        $account['permission_tree'] = $this->getPermissionTree($biz_id,0,$account_id);

        return $account;
    }

    public function updateAccountRole($biz_id,$account_id,$role_id,$to_state,$connected_to = ""){
        try{
            
            $SQLwhere = "rb_id = $role_id";

            if($connected_to != ""){
                $SQLwhere = "rb_connected_to = '$connected_to'";
            }

            $sql = "SELECT * FROM tbl_role_biz WHERE $SQLwhere";

           
            $role = $this->db->getRow($sql);

            $sql = "";

            if($to_state == 0){
                $sql = "DELETE FROM tbl_account_role_biz WHERE arb_account_id = $account_id AND arb_role_id = {$role['rb_id']} AND arb_biz_id = $biz_id";
            }
            else{
                $sql = "SELECT COUNT(*) FROM tbl_account_role_biz WHERE arb_account_id = $account_id AND arb_role_id = {$role['rb_id']} AND arb_biz_id = $biz_id";
                $cnt = $this->db->getVal($sql);

                if($cnt == 0){
                    $sql = "INSERT INTO tbl_account_role_biz SET
                            arb_account_id = $account_id,
                            arb_role_id = {$role['rb_id']},
                            arb_biz_id = $biz_id";
                }
                else{
                    $result = array();
                    $result['code'] = 1;
                    return $result;
                }
            }

            $this->db->execute($sql);
            $result = array();
            if(isset($role['rb_connected_to']) && $role['rb_connected_to'] != ""){
                $result['connect_to'] = $role['rb_connected_to'];
                
            }

            
            $result['code'] = 1;
            return $result;
        }
        catch(Error $er){
            $result = array();
            $result['code'] = 0;
            return $result;
        }
    }

    public function removeAccount($account_id,$biz_id){
        $sql = "SELECT * FROM tbl_account WHERE ac_id = $account_id";

        $account = $this->db->getRow($sql);
        
        $result = array();
        $result['code'] = 1;
        if($account['ac_type'] == 'guest'){
            $sql = "UPDATE tbl_guests SET gu_isrevoked = 1 WHERE gu_account_id = $account_id AND gu_biz_id = $biz_id";
        }
        else{
            $sql = "DELETE FROM tbl_account_role_biz WHERE arb_account_id = $account_id AND arb_role_id = 1 AND arb_biz_id = $biz_id";

            $this->db->execute($sql);
            
            $sql = "UPDATE tbl_client_biz,tbl_reseller_clients 
                        SET 
                            client_biz_active = 0,
                            client_biz_isActivated = 1
                        WHERE reseller_client_id = client_biz_client_id 
                        AND reseller_client_account_id = $account_id 
                        AND client_biz_biz_id = $biz_id";
            $result['code'] = 2;
        }

        $this->db->execute($sql);

        
        $result['message'] = "";

        return $result;
    }

    public function bizHasManager($biz_id){
        $sql = "SELECT COUNT(*) FROM tbl_client_biz WHERE client_biz_biz_id = $biz_id AND (client_biz_active = 1 OR (client_biz_active = 0 AND client_biz_isActivated = 0))";

        $result = $this->db->getVal($sql);

        return $result > 0;
    }

    public function checkEmailinBiz($email,$biz_id){
        $sql = "SELECT ac_username FROM tbl_account,tbl_owners,tbl_biz WHERE ac_id = owner_account_id AND biz_owner_id = owner_id AND biz_id = $biz_id";
        $count = 0;
        
        $owner_name = $this->db->getVal($sql);
        if($owner_name == $email){
            $count++;
        }

        $sql = "SELECT COUNT(*) FROM tbl_account,tbl_guests
                    WHERE gu_account_id = ac_id
                    AND gu_biz_id = $biz_id
                    AND LOWER(ac_username) = LOWER('$email')
                    AND gu_isrevoked = 0";

        $count += $this->db->getVal($sql);

        $sql = "SELECT COUNT(*) FROM tbl_account,tbl_reseller_clients,tbl_client_biz
                    WHERE ac_id = reseller_client_account_id
                    AND client_biz_client_id = reseller_client_id
                    AND client_biz_biz_id = $biz_id
                    AND LOWER(ac_username) = LOWER('$email')
                    AND client_biz_active = 1";

        $count += $this->db->getVal($sql);

        return $count > 0;
    }

    public function getPermissionsForUser($biz_id,$username){
        
        //Check if usere is the owner of the app
        $owner_id = $this->db->getVal("SELECT biz_owner_id FROM tbl_biz WHERE biz_id = $biz_id");
        $account_owner = $this->db->getRow("SELECT owner_id,owner_reseller_id FROM tbl_account,tbl_owners WHERE owner_account_id = ac_id AND ac_username = '$username' AND ac_type='account_owner'");
        
        $sql = "SELECT ac_id,arb_role_id
                FROM tbl_account,tbl_account_role_biz 
                    WHERE arb_account_id = ac_id
                    AND ac_username = '$username'
                    AND arb_biz_id = $biz_id
                GROUP BY ac_id
                ORDER BY arb_role_id asc";

        

        $ass_accounts = $this->db->getTable($sql);
        $result = array();
        if($owner_id == $account_owner['owner_id'] || count($ass_accounts) == 0){
            $tree = array();
            
        }
        else{
           

            
            $tree = $this->getPermissionTree($biz_id,0,$ass_accounts[0]['ac_id']);

            foreach ($ass_accounts as $ass_account)
            {
            	if($ass_account['arb_role_id'] == 183){
                    $result['isSupport'] = true;
                }
            }
            
        }
        
        $result['tree'] = $tree;

        $result['isAppManager'] = (isset($ass_accounts[0]['arb_role_id']) && $ass_accounts[0]['arb_role_id'] == 1) || ($owner_id == $account_owner['owner_id'] && $account_owner['owner_reseller_id'] == 0);
        return $result;

    }

    public function addAppManagerPermissions($biz_id){
        $sql ="INSERT IGNORE INTO tbl_biz_role_permission(brp_biz_id,brp_role_id,brp_area_id,brp_permission_type)
                        SELECT $biz_id,1,pa_id,'full' FROM tbl_permission_tree";

        $this->db->execute($sql);
    }

    public function addBasePermissions($biz_id){
        $sql ="INSERT IGNORE INTO tbl_biz_role_permission(brp_biz_id,brp_role_id,brp_area_id,brp_permission_type)
                        SELECT $biz_id,2,pa_id,'full' FROM tbl_permission_tree WHERE pa_id <> 33 AND pa_default_state = 1";

        $this->db->execute($sql);

        $sql ="INSERT IGNORE INTO tbl_biz_role_permission(brp_biz_id,brp_role_id,brp_area_id,brp_permission_type)
                        SELECT $biz_id,38,pa_id,'full' FROM tbl_permission_tree WHERE pa_id <> 33 AND pa_default_state = 1";

        $this->db->execute($sql);

        $sql ="INSERT IGNORE INTO tbl_biz_role_permission(brp_biz_id,brp_role_id,brp_area_id,brp_permission_type)
                        SELECT $biz_id,138,pa_id,'full' FROM tbl_permission_tree WHERE pa_id <> 33 AND pa_default_state = 1";

        $this->db->execute($sql);
    }

    public function getMobileBizPermissions($account_id,$biz_id,$type = ""){

        $permissionArray = array();

        if($type != ""){
            $globalPermissions = $this->db->getTable("SELECT per_key, pu_value
                            FROM tbl_permission_user,tbl_permission
                            WHERE per_id = pu_per_id
                            AND pu_user_type='$type'
                            AND per_type = 'owner'
                            AND pu_value <> 'full'");

            if(count($globalPermissions) > 0){
                foreach ($globalPermissions as $entry){
                    $permissionObject["name"] = $entry["per_key"];
                    $permissionObject["value"] = $entry["pu_value"];
                    $permissionObject["module"] = $entry["per_module"];
                    array_push($permissionArray,$permissionObject);
                }
            }
        }

        $permissionData = $this->getPermissionData($account_id,$biz_id);

        $bizPermissions = $this->db->getTable("SELECT *
                            FROM tbl_permission
                            WHERE per_type = 'biz'
                            AND per_master_key <> 'section_button'");

        $displayPromote = false;
        $displayEdit = false;
        $displayEngage = false;
        
        if(count($bizPermissions) > 0){
            foreach ($bizPermissions as $entry){
                $value = $this->checkForMobilePermission($permissionData,$entry["per_master_key"]);
                if($value != ""){
                    $permissionObject["name"] = $entry["per_key"];
                    $permissionObject["value"] = $value;
                    $permissionObject["module"] = $entry["per_module"];
                    array_push($permissionArray,$permissionObject);

                    if($entry["per_master"] != 0 && $value == "self"){
                        switch($entry["per_master"]){
                            case 8:
                                $displayPromote = true;
                                break;
                            case 10:
                                $displayEdit = true;
                                break;
                            case 11:
                                $displayEngage = true;
                                break;
                        }
                    }
                }else{
                    if($entry["per_master"] != 0){
                        switch($entry["per_master"]){
                            case 8:
                                $displayPromote = true;
                                break;
                            case 10:
                                $displayEdit = true;
                                break;
                            case 11:
                                $displayEngage = true;
                                break;
                        }
                    }
                }
            }
        }
        
        if(!$displayEngage){
            $permissionObject["name"] = "engageButton";
            $permissionObject["value"] = "none";
            $permissionObject["module"] = "0";
            array_push($permissionArray,$permissionObject);
        }
        
        if(!$displayEdit){
            $permissionObject["name"] = "editButton";
            $permissionObject["value"] = "none";
            $permissionObject["module"] = "0";
            array_push($permissionArray,$permissionObject);
        }
        
        if(!$displayPromote){
            $permissionObject["name"] = "promoteButton";
            $permissionObject["value"] = "none";
            $permissionObject["module"] = "0";
            array_push($permissionArray,$permissionObject);
        }

        return $permissionArray;

    }

    private function checkForMobilePermission($permissionData,$pKey){

        $retValue = "";
        foreach ($permissionData as $entry)
        {
            if($entry['role_permission'] != 'full'){
                
                if($entry['end_name'] == "$pKey"){
                    $retValue = $entry['role_permission'];
                }
            }
        }
        return $retValue;
    }

    public function getMobileAccountPermissions($account_id,$biz_id){
        
        $permissions = $this->getPermissionData($account_id,$biz_id);
        $result = array();
        foreach ($permissions as $entry)
        {
        	if($entry['role_permission'] != 'none'){
                $name = $entry['pa_system_name'];
                if($entry['parent_name'] != ""){
                    $name = $entry['parent_name']."_".$entry['pa_system_name'];
                }
                $permissionRole = array();
                $permissionRole["name"] = $name;
                $permissionRole["value"] = $entry['role_permission'];
                $permissionRole["module"] = $entry['pa_mod_id'];
                array_push($result,$permissionRole);
            }
        }
        return $result;
    }

    private function getPermissionData($account_id,$biz_id){

        //Check if usere is the owner of the app
        $owner_id = $this->db->getVal("SELECT biz_owner_id FROM tbl_biz WHERE biz_id = $biz_id");
        $account_owner_id = $this->db->getVal("SELECT owner_id FROM tbl_account,tbl_owners WHERE owner_account_id = ac_id AND ac_id = $account_id AND ac_type='account_owner'");

        
        if($owner_id == $account_owner_id){
            $oldsql = "SELECT base.*,'full' as role_permission,parent.pa_system_name as parent_name,'full' as parent_permission_type,
                        CASE WHEN base.pa_parent_id = 0 THEN base.pa_system_name
						ELSE CONCAT(parent.pa_system_name,'_',base.pa_system_name) 
						END as end_name
                        FROM tbl_permission_tree as base
                        LEFT JOIN tbl_permission_tree as parent ON base.pa_parent_id = parent.pa_id
                        ORDER BY pa_parent_id asc";

            $sql = "SELECT base.*,'full' as role_permission,pa_adminapp_key as end_name
                        FROM tbl_permission_tree as base                        
                        ORDER BY pa_parent_id asc";
        }
        else{

            $oldsql = "SELECT * FROM (SELECT base.*,
						CASE
						WHEN parent.pa_id is not NULL THEN
							CASE
							WHEN parent.brp_permission_type is not NULL THEN
								CASE 
									WHEN s.brp_permission_type is NULL THEN 'none'
									WHEN s.brp_permission_type = 'selfonly' THEN 'self'
									ELSE 'full'
								END 
							ELSE
								'none'
							END
						ELSE
						CASE
								WHEN s.brp_permission_type is NULL THEN 'none'
								WHEN s.brp_permission_type = 'selfonly' THEN 'self'
								ELSE 'full'
							END
						END as role_permission,
						CASE WHEN base.pa_parent_id = 0 THEN base.pa_system_name
						ELSE CONCAT(parent.pa_system_name,'_',base.pa_system_name) 
						END as end_name					
                FROM tbl_permission_tree as base
                LEFT JOIN (SELECT * FROM tbl_biz_role_permission,tbl_account_role_biz 
                           WHERE brp_biz_id = arb_biz_id 
                           AND brp_role_id = arb_role_id 
                           AND arb_account_id = $account_id AND arb_biz_id = $biz_id) s 
                            ON brp_area_id = pa_id
                LEFT JOIN (SELECT * FROM tbl_permission_tree
							LEFT JOIN (SELECT * FROM tbl_biz_role_permission,tbl_account_role_biz
										WHERE brp_biz_id = arb_biz_id 
										AND brp_role_id = arb_role_id) temp
						     ON brp_area_id = pa_id
                           AND arb_account_id = $account_id AND arb_biz_id = $biz_id) as parent ON base.pa_parent_id = parent.pa_id
                GROUP BY pa_id) s
                ORDER BY role_permission desc";

            $sql = "SELECT * FROM (SELECT base.*,
						CASE
						WHEN parent.pa_id is not NULL THEN
							CASE
							WHEN parent.brp_permission_type is not NULL THEN
								CASE 
									WHEN s.brp_permission_type is NULL THEN 'none'
									WHEN s.brp_permission_type = 'selfonly' THEN 'self'
									ELSE 'full'
								END 
							ELSE
								'none'
							END
						ELSE
						CASE
								WHEN s.brp_permission_type is NULL THEN 'none'
								WHEN s.brp_permission_type = 'selfonly' THEN 'self'
								ELSE 'full'
							END
						END as role_permission,
						base.pa_adminapp_key as end_name					
                FROM tbl_permission_tree as base
                LEFT JOIN (SELECT * FROM tbl_biz_role_permission,tbl_account_role_biz 
                           WHERE brp_biz_id = arb_biz_id 
                           AND brp_role_id = arb_role_id 
                           AND arb_account_id = $account_id AND arb_biz_id = $biz_id) s 
                            ON brp_area_id = pa_id
                LEFT JOIN (SELECT * FROM tbl_permission_tree
							LEFT JOIN (SELECT * FROM tbl_biz_role_permission,tbl_account_role_biz
										WHERE brp_biz_id = arb_biz_id 
										AND brp_role_id = arb_role_id) temp
						     ON brp_area_id = pa_id
                           AND arb_account_id = $account_id AND arb_biz_id = $biz_id) as parent ON base.pa_parent_id = parent.pa_id
                GROUP BY pa_id) s
                ORDER BY role_permission desc";
        }

        return $this->db->getTable($sql);
    }
    
    public function accountHasPermission($account_id,$biz_id,$area_sys_name,$area_id = 0){
        $sql = "SELECT owner_account_id FROM tbl_biz,tbl_owners 
                        WHERE owner_id = biz_owner_id
                        AND biz_id = $biz_id";

        $bizOwnerAccount = $this->db->getVal($sql);

        if($bizOwnerAccount == $account_id){
            return true;
        }

        $sqlWhere = $area_id == 0 ? "pa_system_name = '$area_sys_name'" : "pa_id = $area_id";

        $sql = "SELECT * FROM tbl_permission_tree WHERE $sqlWhere";

        $area = $this->db->getRow($sql);

        $depth = 0;

        $flag = true;

        while(isset($area['pa_id']) && $depth < 5 && $flag){
            $depth++;

            $sql = "SELECT pa_id,pa_parent_id,pa_name FROM tbl_account_role_biz 
                        LEFT JOIN tbl_biz_role_permission ON arb_role_id = brp_role_id and brp_biz_id=$biz_id
                        LEFT JOIN tbl_permission_tree ON brp_area_id = pa_id
                        WHERE arb_biz_id = $biz_id
                        AND arb_account_id = $account_id
                        AND pa_id = {$area['pa_id']}";

            $perm = $this->db->getRow($sql);

            if($perm["pa_id"] == ""){
                $flag = false;
            }else{
                if($area['pa_parent_id'] != 0){
                    $sql = "SELECT * FROM tbl_permission_tree WHERE pa_id = {$area['pa_parent_id']}";

                    $area = $this->db->getRow($sql);
                }
                else{
                    $area = array();
                }
            }
        }

        return $flag;
    }
    
    public function accountIsOwner($account_id,$biz_id){
        $sql = "SELECT owner_account_id FROM tbl_biz,tbl_owners 
                        WHERE owner_id = biz_owner_id
                        AND biz_id = $biz_id";

        $bizOwnerAccount = $this->db->getVal($sql);

        if($bizOwnerAccount == $account_id){
            return true;
        }
        
        return false;
    }

    public function accountIsSupport(){
    }

    
}
