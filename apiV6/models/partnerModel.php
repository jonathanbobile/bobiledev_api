<?php

require_once(LIBS_PATH.'Composer/vendor/autoload.php');

use Zenstruck\JWT\Token;
use Zenstruck\JWT\Signer\HMAC\HS256;
use Zenstruck\JWT\Validator\ExpiresAtValidator;
use Zenstruck\JWT\Exception\MalformedToken;
use Zenstruck\JWT\Exception\UnverifiedToken;
use Zenstruck\JWT\Exception\Validation\ExpiredToken;

class partnerModel extends Model
{    
    public function __construct()
    {
        parent::__construct(); 
    }

    //************************* A1 ***************************//

    /**
     * Get apiKey from A1
     * need to be sent in every request to A1
     * @return array
     */
    public function getA1ApiConfig(){

        $response["code"] = 1;

        try {
            $apiConfig = new \Connect\Config([
                'apiKey' => A1_API_KEY,
                'apiEndpoint' => 'https://api.connect.cloud.im/public/v1'
            ]); 
            $response["data"] = $apiConfig;
        }
        catch (Exception $e) {
            $response["code"] = 0;
            $response["data"] = "Error processing requests:" . $e->getMessage();
        }

        return $response;
    }

    /**
     * Get A1 asset ID from encoded JWT string
     * * Return Data = A1 asset ID
     * @param mixed $jwtKey 
     * @return resultObject
     */
    public function getAssetId($jwtKey){

        $decodedToken = "";

        try {
            $decodedToken = Token::fromString($jwtKey);
        }
        catch (MalformedToken $e) {
            return resultObject::withData(0,$e->getMessage());
        }

        try {
            $decodedToken->verify(new HS256(), A1_JWT_KEY);
        }
        catch (UnverifiedToken $e) {
            return resultObject::withData(0,$e->getMessage());
        }

        try {
            $decodedToken->validate(new ExpiresAtValidator());
        }
        catch (ExpiredToken $e) {
            return resultObject::withData(0,$e->getMessage());
        }

        return resultObject::withData(1,"Ok",$decodedToken->get('asset_id'));
    }

    /**
     * Get license object for provided asset ID
     * * Return Data = licenceObject
     * @param mixed $assetId 
     * @return resultObject
     */
    public function getA1LicenseByAsset($assetId){

        try {
            $licenseData = $this->getA1LicenseByAssetDB($assetId);
            $licenceObject = a1LicenseObject::withData($licenseData);
            return resultObject::withData(1,"Ok",$licenceObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$assetId);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Insert new licence to DB
     * * Return Data = new license DB ID
     * @param a1LicenseObject $licenseObject 
     * @return resultObject
     */
    public function addA1License(a1LicenseObject $licenseObject){

        try{
            $newId = $this->addA1LicenseDB($licenseObject);
            return resultObject::withData(1,"Ok",$newId);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Insert new history licence to DB
     * * Returns Data = new history license DB ID
     * @param a1HistoryObject $historyObject 
     * @return resultObject
     */
    public function addA1History(a1HistoryObject $historyObject){

        try{
            $newId = $this->addA1HistoryDB($historyObject);
            return resultObject::withData(1,"Ok",$newId);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }        
    }

    /**
     * Updates A1 licens in DB
     * @param a1LicenseObject $licenseObject 
     * @return resultObject
     */
    public function updateA1License(a1LicenseObject $licenseObject){

        try{
            $this->updateA1LicenseDB($licenseObject);
            return resultObject::withData(1,"Ok");
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Suspend biz
     * Mark the biz to status=0 and deleted=1
     * @param int $bizId 
     */
    public function suspendA1Biz($bizId){

        $bizModel = new bizModel($bizId);

        $responseBiz = $bizModel->getBiz();
        if($responseBiz->code == 1){

            $bizObj = $responseBiz->data;
            $bizObj->biz_status=0;
            $bizObj->biz_isdeleted=1;

            $responseUpdateBiz = $bizModel->updateBiz($bizObj);
            return $responseUpdateBiz;
        }else{
            return $responseBiz;
        }
    }

    /**
     * Activate biz
     * Mark the biz to status=1 and deleted=0
     * @param int $bizId 
     */
    public function restoreA1Biz($bizId){

        $bizModel = new bizModel($bizId);

        $responseBiz = $bizModel->getBiz();
        if($responseBiz->code == 1){

            $bizObj = $responseBiz->data;
            $bizObj->biz_status=1;
            $bizObj->biz_isdeleted=0;

            $responseUpdateBiz = $bizModel->updateBiz($bizObj);
            return $responseUpdateBiz;
        }else{
            return $responseBiz;
        }
    }

    /*------------------------- A1 PRIVATE -----------------------------*/

    private function getA1LicenseByAssetDB($assetId = ""){

        if ($assetId == ""){
            throw new Exception("assetId value must be provided");             
        }

        return $this->db->getRow("SELECT * FROM tbl_a1_license WHERE a1_asset_id='$assetId'");
    }

    private function addA1LicenseDB(a1LicenseObject $licenseObject){

        if (!isset($licenseObject)){
            throw new Exception("licenseObject value must be provided");             
        }

        $sql = "
                INSERT INTO tbl_a1_license SET
                    a1_request_id = '".addslashes($licenseObject->a1_request_id)."',
                    a1_asset_id = '".addslashes($licenseObject->a1_asset_id)."',
                    a1_biz_id = {$licenseObject->a1_biz_id},
                    a1_request_type = '{$licenseObject->a1_request_type}',
                    a1_plan_id = {$licenseObject->a1_plan_id},
                    a1_customer_id = '".addslashes($licenseObject->a1_customer_id)."',
                    a1_customer_organization = '".addslashes($licenseObject->a1_customer_organization)."',
                    a1_customer_first_name = '".addslashes($licenseObject->a1_customer_first_name)."',
                    a1_customer_last_name = '".addslashes($licenseObject->a1_customer_last_name)."',
                    a1_customer_email = '".addslashes($licenseObject->a1_customer_email)."',
                    a1_customer_phone = '".addslashes($licenseObject->a1_customer_phone)."',
                    a1_environment = '".addslashes($licenseObject->a1_environment)."'
        ";

        return $this->db->execute($sql);
    }

    private function updateA1LicenseDB(a1LicenseObject $licenseObject){

        if (!isset($licenseObject->a1_id) || !is_numeric($licenseObject->a1_id) || $licenseObject->a1_id <= 0){
            throw new Exception("licenseObject value must be provided");             
        }

        $sql = "
                UPDATE tbl_a1_license SET
                    a1_request_id = '".addslashes($licenseObject->a1_request_id)."',
                    a1_asset_id = '".addslashes($licenseObject->a1_asset_id)."',
                    a1_biz_id = {$licenseObject->a1_biz_id},
                    a1_request_type = '{$licenseObject->a1_request_type}',
                    a1_plan_id = {$licenseObject->a1_plan_id},
                    a1_customer_id = '".addslashes($licenseObject->a1_customer_id)."',
                    a1_customer_organization = '".addslashes($licenseObject->a1_customer_organization)."',
                    a1_customer_first_name = '".addslashes($licenseObject->a1_customer_first_name)."',
                    a1_customer_last_name = '".addslashes($licenseObject->a1_customer_last_name)."',
                    a1_customer_email = '".addslashes($licenseObject->a1_customer_email)."',
                    a1_customer_phone = '".addslashes($licenseObject->a1_customer_phone)."',
                    a1_date = '{$licenseObject->a1_date}'
                    WHERE a1_id = {$licenseObject->a1_id}
        ";

        $this->db->execute($sql);
    }

    private function addA1HistoryDB(a1HistoryObject $historyObject){

        if (!isset($historyObject)){
            throw new Exception("licenseObject value must be provided");             
        }

        $sql = "
                INSERT INTO tbl_a1_history SET
                a1h_license_id = {$historyObject->a1h_license_id},
                a1h_request_id = '".addslashes($historyObject->a1h_request_id)."',
                a1h_request_type = '{$historyObject->a1h_request_type}',
                a1h_plan_id = {$historyObject->a1h_plan_id}
        ";

        return $this->db->execute($sql);
        
    }


    //************************* BOBILE API ***************************//

    /**********************************/
    /*         PUBLIC FUNCTIONS        */
    /**********************************/

    /**
     * Get reseller object for provided API key
     * * Return Data = resellerObject
     * @param mixed $apiKey 
     * @return resultObject 
     */
    public function getResellerByApiKey($apiKey){

        try {
            $resellerData = $this->getResellerByApiKeyDB($apiKey);
            $resellerObject = resellerObject::withData($resellerData);
            return resultObject::withData(1,"Ok",$resellerObject);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Get generated client token from API key and E-mail
     * * Return Data = Client Token string
     * @param mixed $apiKey 
     * @param mixed $email 
     * @return resultObject
     */
    public function getClientTokenByEmail($apiKey,$email){

        if(isset($email)){

            if(filter_var($email, FILTER_VALIDATE_EMAIL)){

                try{
                    $responseReseller = $this->getResellerByApiKey($apiKey);

                    if($responseReseller->code == 1){

                        $resellerObject = $responseReseller->data;

                        $accountManager = new accountManager();
                        $responseAccount = $accountManager->getAccountByPartnerIdAndEmail($resellerObject->reseller_id,$email);

                        if($responseAccount->code == 1){
                            $accountObject = $responseAccount->data;
                            $generatedPassword = $accountObject->ac_password;
                        }else{
                            $generatedPassword = rand(10000000,99999999);
                        }
                            
                        $generatedToken = $email."^".$resellerObject->reseller_id."^".$generatedPassword;

                        $data = utilityManager::encodeToHASH($generatedToken,true);
                        return resultObject::withData(1,"Ok",$data);
                    }
                    else{
                        return resultObject::withData(0,"No account found","CANT_FIND_PARTNER");
                    }
                }
                catch(Exception $e) {
                    errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
                    return resultObject::withData(0,$e->getMessage());
                }

            }else{
                return resultObject::withData(0,"Wrong client's email","WRONG_EMAIL");
            }

        }else{
            return resultObject::withData(0,"Missing email","MISSING_EMAIL");
        }
    }

    /**
     * Get all apps belong to partner
     * * Return data = array of limited app data
     * --- clientToken (if exist)
     * --- appName
     * --- appToken
     * --- appCreationDate
     * --- appisActive (Yes/No)
     * 
     * @param mixed $apiKey 
     * @return resultObject
     */
    public function getPartnerApps($apiKey){

        try{
            $responseReseller = $this->getResellerByApiKey($apiKey);

            if($responseReseller->code == 1){

                $resellerObject = $responseReseller->data;

                $ownerManager = new ownerManager($resellerObject->resellers_owner->owner_id);
                $ownerAppsResult = $ownerManager->getBizListByOwnerId();

                if($ownerAppsResult->code == 1){
                    $appsArray = array();
                    if(count($ownerAppsResult->data)>0){

                        $resellerClientModel = new resellerClientModel();

                        foreach ($ownerAppsResult->data as $oneApp)
                        {
                            $responseReselerClientData = $resellerClientModel->getResellerClientByBizId($oneApp->biz_id);
                            if($responseReselerClientData->code == 1){
                                
                                $appObject = array();
                                
                                $clientToken = "";
                                if(isset($responseReselerClientData->data->reseller_client_id)){
                                    $clientObject = $responseReselerClientData->data;
                                    $genPass = substr(utilityManager::encodeToHASH($clientObject->reseller_client_username),0,8);
                                    $generated = $clientObject->reseller_client_username."^".$resellerObject->reseller_id."^".$genPass;
                                    $clientToken = utilityManager::encodeToHASH($generated,true);
                                }
                                
                                $appObject["clientToken"] = $clientToken;
                                $appObject["businessName"] = stripslashes($oneApp->biz_short_name);
                                $appObject["businessToken"] = utilityManager::encodeToHASH($oneApp->biz_id);
                                $appObject["businessCreationDate"] = $oneApp->biz_start_date;
                                $appObject["businessIsActive"] = $oneApp->biz_isdeleted == "0" ? "Yes" : "No";
                                array_push($appsArray,$appObject);
                            }
                        }                   
                    }

                    if(count($appsArray) > 0){
                        return resultObject::withData(1,"Ok",$appsArray);
                    }else{
                        return resultObject::withData(0,"No apps listed","NO_APPS");
                    }
                }
                else{
                    return resultObject::withData(0,$ownerAppsResult->message);
                }
            }
            else{
                return resultObject::withData(0,"No account found","CANT_FIND_PARTNER");
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getClientsApps($apiKey,$clientToken){

        try{
            $responseReseller = $this->getResellerByApiKey($apiKey);
            $tokenArray = $this->returnClientTokenObject($clientToken);

            if($responseReseller->code == 1){

                $resellerObject = $responseReseller->data;

                $resellerClientModel = new resellerClientModel();
                $responseClient = $resellerClientModel->getResellerClientByUsernameAndOwnerId($tokenArray[0],$resellerObject->resellers_owner->owner_id);
       
                if($responseClient->code != 1 || $responseClient->data->reseller_client_id == 0){
                    return resultObject::withData(0,"No clients listed","NO_CLIENT");
                }

                $appsData = bizManager::getBizListForClient($responseClient->data->reseller_client_id);

                if(is_array($appsData) && count($appsData)>0){
                    $appsArray = array();
                    foreach ($appsData as $app)
                    {
                        $oneApp = bizObject::withData($app);
                        $appObject = array();
                        
                        $appObject["clientToken"] = $clientToken;
                        $appObject["businessName"] = stripslashes($oneApp->biz_short_name);
                        $appObject["businessToken"] = utilityManager::encodeToHASH($oneApp->biz_id);
                        $appObject["businessCreationDate"] = $oneApp->biz_start_date;
                        $appObject["businessIsActive"] = $oneApp->biz_isdeleted == "0" ? "Yes" : "No";
                        array_push($appsArray,$appObject);
                    } 

                    return resultObject::withData(1,"Ok",$appsArray);
                }
                else{
                    return resultObject::withData(0,"No apps listed","NO_APPS");
                }
            }
            else{
                return resultObject::withData(0,"No account found","CANT_FIND_PARTNER");
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Deactivete biz.
     * Set the biz_isdeleted = 1
     * @param mixed $appToken 
     * @return resultObject
     */
    public function deactivatePartnerApp($appToken){

        $biz_id = utilityManager::decodeFromHASH($appToken);

        $bizManager = new bizModel($biz_id);
        $bizDataResponse = $bizManager->getBiz($biz_id);
        if($bizDataResponse->code == 1){
            $bizDataResponse->data->biz_isdeleted = 1;
            $updateBizResponse = $bizManager->updateBiz($bizDataResponse->data);
            if($updateBizResponse->code == 1){
                return resultObject::withData(1,"Ok");
            }else{
                return $updateBizResponse;
            }
        }else{
            return $bizDataResponse;
        } 
    }

    /**
     * Activete biz.
     * Set the biz_isdeleted = 0
     * @param mixed $appToken 
     * @return resultObject
     */
    public function activatePartnerApp($appToken){

        $biz_id = utilityManager::decodeFromHASH($appToken);

        $bizManager = new bizModel($biz_id);
        $bizDataResponse = $bizManager->getBiz($biz_id);
        if($bizDataResponse->code == 1){
            $bizDataResponse->data->biz_isdeleted = 0;
            return $bizManager->updateBiz($bizDataResponse->data);
        }else{
            return $bizDataResponse;
        } 
    }

    /**
     * Summary of getAppDetails
     * @param mixed $apiKey 
     * @param mixed $appToken 
     * @return resultObject
     */
    public function getAppDetails($apiKey,$appToken){

        try{
            $responseReseller = $this->getResellerByApiKey($apiKey);

            if($responseReseller->code == 1){

                $resellerObject = $responseReseller->data;

                $bizId = utilityManager::decodeFromHASH($appToken);
                $bizModel = new bizModel($bizId);

                $bizDataResponse = $bizModel->getBiz();

                if($bizDataResponse->code == 1){

                    $bizObject = $bizDataResponse->data;
                    if($bizObject->biz_id != "0"){

                        $resellerClientModel = new resellerClientModel();

                        $responseReselerClientData = $resellerClientModel->getResellerClientByBizId($bizObject->biz_id);
                        if($responseReselerClientData->code == 1){
                            
                            $appObject = array();

                            $clientToken = "";
                            if(is_array($responseReselerClientData->data)){
                                $clientObject = $responseReselerClientData->data;
                                $genPass = substr(utilityManager::encodeToHASH($clientObject->reseller_client_username),0,8);
                                $generated = $clientObject->reseller_client_username."^".$resellerObject->reseller_id."^".$genPass;
                                $clientToken = utilityManager::encodeToHASH($generated);
                            }
                            
                            $appObject["clientToken"] = $clientToken;
                            $appObject["businessName"] = stripslashes($bizObject->biz_short_name);
                            $appObject["businessToken"] = utilityManager::encodeToHASH($bizObject->biz_id);
                            $appObject["businessCreationDate"] = $bizObject->biz_start_date;
                            $appObject["businessIsActive"] = $bizObject->biz_isdeleted == "0" ? "Yes" : "No";

                            $bizModel = new bizModel($bizObject->biz_id);
                            $marketsData = $bizModel->getBizAppstoresAndStatus(); 

                            $appObject["appleAppStoreStatus"] = $marketsData->data[0]->stage->bms_home_title;
                            $appObject["googleAppStoreStatus"] = $marketsData->data[1]->stage->bms_home_title;
                            $appObject["marketStatus"] = $marketsData->data[3]->stage->bms_home_title;
                            
                            $appObject["appleAppId"] = $bizObject->biz_sbmt_apl_bundle_suffix; 
                            $appObject["googleAppId"] = $bizObject->biz_sbmt_goo_pack_id;
                            

                            $moduleDataManager = new moduleDataManager();
                            $modules = $moduleDataManager->loadAllModulesForBizFromDb($bizObject->biz_id);
                            $modulesArray = array();
                            if(count($modules)>0){
                                foreach ($modules as $module)
                                {
                                    $moduleObject = array();
                                    $moduleObject["featureName"] = ($module["biz_mod_mod_id"] == "") ? $module["mod_name"] : $module["biz_mod_mobile_name"];
                                    $moduleObject["featureActive"] = $module["biz_mod_active"] == "0" || $module["biz_mod_active"] == "" ? "No" : "Yes";
                                    array_push($modulesArray,$moduleObject);
                                }
                                
                            }
                            $appObject["businessFeatures"] = $modulesArray;

                        
                            return resultObject::withData(1,"Ok",$appObject);
                        }else{
                            return resultObject::withData(0,"No client listed","NO_CLIENT");
                        }
                        
                    }else{
                        return resultObject::withData(0,"No apps listed","NO_APPS");
                    }
                }
                else{
                    return $bizDataResponse;
                }
            }
            else{
                return resultObject::withData(0,"No account found","CANT_FIND_PARTNER");
            }
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Summary of getCustomersList
     * @param mixed $apiKey 
     * @param mixed $appToken 
     * @param mixed $type 
     * @return resultObject
     */
    public function getCustomersList($apiKey,$appToken,$type){

        try{
            $biz_id = utilityManager::decodeFromHASH($appToken);
            $customerModel = new customerModel();
            $customersList = $customerModel->getCustomersByTypeDB($biz_id,$type);
            if(count($customersList) > 0){
                
                $customersArray = array();
                foreach ($customersList as $customer)
                {
                    $oneCustomer = customerObject::withData($customer);
                    
                    $customerObject['customer_token']=utilityManager::encodeToHASH($oneCustomer->cust_id);
                    $customerObject['customer_name']=($oneCustomer->cust_first_name == null) ? "" : $oneCustomer->cust_first_name;
                    $customerObject['customer_registration_date']=($oneCustomer->cust_reg_date == null) ? "" : $oneCustomer->cust_reg_date;
                    $customerObject['customer_email']=($oneCustomer->cust_email == null) ? "" : $oneCustomer->cust_email;
                    $customerObject['customer_phone1']=($oneCustomer->cust_phone1 == null) ? "" : $oneCustomer->cust_phone1;
                    $customerObject['customer_phone2']=($oneCustomer->cust_phone2 == null) ? "" : $oneCustomer->cust_phone2;
                    $customerObject['customer_address']=($oneCustomer->cust_address == null) ? "" : $oneCustomer->cust_address;           
                    $customerObject['customer_city']=($oneCustomer->cust_city == null) ? "" : $oneCustomer->cust_city;                    
                    $customerObject['customer_zip']=($oneCustomer->cust_zip == null) ? "" : $oneCustomer->cust_zip;
                    $customerObject['customer_country']=($oneCustomer->cust_country == null) ? "0" : $oneCustomer->cust_country;
                    $customerObject['customer_state_code']=($oneCustomer->cust_state == null) ? "0" : $oneCustomer->cust_state;
                    $customerObject['customer_state']=($customer["state_code"] == null || $customer["state_code"] == "N/A") ? "" : $customer["state_code"];
                    $customerObject['customer_country_code']=($oneCustomer->cust_country == null) ? "0" : $oneCustomer->cust_country;
                    $customerObject['customer_country']=($customer["country_name_eng"] == null || $customer["country_name_eng"] == "N/A") ? "" : $customer["country_name_eng"];

                    $gender=($oneCustomer->cust_gender == null || $oneCustomer->cust_gender == "0") ? "" : $oneCustomer->cust_gender;
                    if($gender != ""){
                        $gender = $oneCustomer->cust_gender == 1 ? "M" : "F";
                    }
                    $customerObject['customer_gender']=$gender;
                    $customerObject['customer_birth_date']=($oneCustomer->cust_birth_date == null) ? "" : $oneCustomer->cust_birth_date;
                    $customerObject['customer_wedding_date']=($oneCustomer->cust_weding_date == null) ? "" : $oneCustomer->cust_weding_date;  
                    $customerObject['customer_pic']=($oneCustomer->cust_big_pic == null) ? "" : $oneCustomer->cust_big_pic;
                    $customerObject['customer_last_seen'] = ($oneCustomer->cust_last_seen == null) ? "" : $oneCustomer->cust_last_seen;
                    $customerObject['customer_status'] = ($oneCustomer->cust_status == null) ? "" : $oneCustomer->cust_status;
                    $customerObject['valid_phone'] = ($oneCustomer->cust_phone1 == $oneCustomer->cust_valid_phone  && $oneCustomer->cust_phone1 != "") ? "1" : "0";
                    
                    array_push($customersArray,$customerObject);
                }         
                return resultObject::withData(1,"Ok",$customersArray);

            }else{ 
                return resultObject::withData(0,"No customers listed","NO_CUSTOMERS");
            }

        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
            return resultObject::withData(0,$e->getMessage());
        }

    }

    /**
     * Summary of getCustomersList
     * @param mixed $apiKey 
     * @param mixed $appToken 
     * @param mixed $type 
     * @return resultObject
     */
    public function getCustomer($apiKey,$customerToken){

        try{
            $customer_id = utilityManager::decodeFromHASH($customerToken);

            $customerModel = new customerModel();
            $customer = $customerModel->getSingleCustomerByIdForPartnersApiDB($customer_id);
            
            if(is_array($customer)){
                
                $oneCustomer = customerObject::withData($customer);

                $customerObject['customer_token']=utilityManager::encodeToHASH($oneCustomer->cust_id);
                $customerObject['customer_name']=($oneCustomer->cust_first_name == null) ? "" : $oneCustomer->cust_first_name;
                $customerObject['customer_registration_date']=($oneCustomer->cust_reg_date == null) ? "" : $oneCustomer->cust_reg_date;
                $customerObject['customer_email']=($oneCustomer->cust_email == null) ? "" : $oneCustomer->cust_email;
                $customerObject['customer_phone1']=($oneCustomer->cust_phone1 == null) ? "" : $oneCustomer->cust_phone1;
                $customerObject['customer_phone2']=($oneCustomer->cust_phone2 == null) ? "" : $oneCustomer->cust_phone2;
                $customerObject['customer_address']=($oneCustomer->cust_address == null) ? "" : $oneCustomer->cust_address;           
                $customerObject['customer_city']=($oneCustomer->cust_city == null) ? "" : $oneCustomer->cust_city;                    
                $customerObject['customer_zip']=($oneCustomer->cust_zip == null) ? "" : $oneCustomer->cust_zip;
                $customerObject['customer_country']=($oneCustomer->cust_country == null) ? "0" : $oneCustomer->cust_country;
                $customerObject['customer_state_code']=($oneCustomer->cust_state == null) ? "0" : $oneCustomer->cust_state;
                $customerObject['customer_state']=($customer["state_code"] == null || $customer["state_code"] == "N/A") ? "" : $customer["state_code"];
                $customerObject['customer_country_code']=($oneCustomer->cust_country == null) ? "0" : $oneCustomer->cust_country;
                $customerObject['customer_country']=($customer["country_name_eng"] == null || $customer["country_name_eng"] == "N/A") ? "" : $customer["country_name_eng"];

                $gender=($oneCustomer->cust_gender == null || $oneCustomer->cust_gender == "0") ? "" : $oneCustomer->cust_gender;
                if($gender != ""){
                    $gender = $oneCustomer->cust_gender == 1 ? "M" : "F";
                }
                $customerObject['customer_gender']=$gender;
                $customerObject['customer_birth_date']=($oneCustomer->cust_birth_date == null) ? "" : $oneCustomer->cust_birth_date;
                $customerObject['customer_wedding_date']=($oneCustomer->cust_weding_date == null) ? "" : $oneCustomer->cust_weding_date;  
                $customerObject['customer_pic']=($oneCustomer->cust_big_pic == null) ? "" : $oneCustomer->cust_big_pic;
                $customerObject['customer_last_seen'] = ($oneCustomer->cust_last_seen == null) ? "" : $oneCustomer->cust_last_seen;
                $customerObject['customer_status'] = ($oneCustomer->cust_status == null) ? "" : $oneCustomer->cust_status;
                $customerObject['valid_phone'] = ($oneCustomer->cust_phone1 == $oneCustomer->cust_valid_phone  && $oneCustomer->cust_phone1 != "") ? "1" : "0";
                
                return resultObject::withData(1,"Ok",$customerObject);

            }else{ 
                return resultObject::withData(0,"No customer found","NO_CUSTOMERS");
            }

        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$apiKey);
            return resultObject::withData(0,$e->getMessage());
        }

    }

    /**
     * Summary of addCustomer
     * @param mixed $data 
     * @return resultObject
     */
    public function addCustomer($data = array()){

        $biz_id = utilityManager::decodeFromHASH($data['businessToken']);
        
        if(isset($data['customer_phone1']) && $data['customer_phone1'] != ""){
            $checkCustomer = customerManager::isOtherCustomerExistByStatusAndPhone($biz_id,0,'invited',$data['customer_phone1']);

            if($checkCustomer){
                return resultObject::withData(0,"Customer with this phone number already exist","CUSTOMER_EXIST");
            }

            if(!filter_var($data['customer_phone1'], FILTER_SANITIZE_NUMBER_INT)){
                return resultObject::withData(0,"Wrong phone format","WRONG_PHONE_NUMBER");
            }
        }

        if(isset($data['customer_email']) && $data['customer_email'] != ""){

            $checkCustomer = customerManager::isOtherCustomerExistByStatusAndEmail($biz_id,0,'invited',$data['customer_email']);

            if($checkCustomer){
                return resultObject::withData(0,"Customer with this email already exist","CUSTOMER_EXIST");
            }

            if(!filter_var($data['customer_email'], FILTER_VALIDATE_EMAIL)){
                return resultObject::withData(0,"Wrong email format","WRONG_EMAIL");
            }
        }

        $customer_first_name = isset($data['customer_name']) && $data['customer_name'] != "" ? $data['customer_name'] : "";
        $customer_email = isset($data['customer_email']) && $data['customer_email'] != "" ? $data['customer_email'] : "";
        $customer_phone1 = isset($data['customer_phone1']) && $data['customer_phone1'] != "" ? $data['customer_phone1'] : "";

        if($customer_first_name == ""){
            return resultObject::withData(0,"Customers name is mandatory","MISSING_FIELD");
        }

        if($customer_email == "" && $customer_phone1 == ""){
            return resultObject::withData(0,"Customer must have email or phone number","MISSING_FIELD");
        }

        $warrnings = array();
        
        $customer_phone2 = isset($data['customer_phone2']) ? $data['customer_phone2'] : "";
        $customer_address = isset($data['customer_address']) ? $data['customer_address'] : "";           
        $customer_city = isset($data['customer_city']) ? $data['customer_city'] : "";                    
        $customer_zip = isset($data['customer_zip']) ? $data['customer_zip'] : "";

        $customer_state = 'NULL';
        if(isset($data['customer_state']) && $data['customer_state'] != ""){
            $upperCaseState = strtoupper($data['customer_state']);
            $customerState = utilityManager::getStateId($upperCaseState);
            if($customerState != ""){
                $customer_state = $customerState;
            }else{
                $warrnings[] = "Wrong state code, please use 2 letters state code";
            }
        }

        $customer_country = 'NULL';
        if(isset($data['customer_country']) && $data['customer_country'] != ""){
            $upperCaseCountry = strtoupper($data['customer_country']);
            $customerCountry = utilityManager::getCountryId($upperCaseCountry);
            if($customerCountry != ""){
                $customer_country = $customerCountry;
            }else{
                $warrnings[] = "Wrong country code, please use 2 letters country code";
            }
        }


        $customer_gender = isset($data['customer_gender']) ? $data['customer_gender'] : "NULL";
        if(isset($data['customer_gender']) && $customer_gender != ""){
            $customer_gender = (strtolower($data['customer_gender']) == "m") ? 1 : 2;
        }

        $customer_birth_date = isset($data['customer_birth_date']) && $data['customer_birth_date'] != "" ? "{$data['customer_birth_date']}" : "NULL";
        $customer_wedding_date = isset($data['customer_wedding_date']) && $data['customer_wedding_date'] != "" ? "{$data['customer_wedding_date']}" : "NULL";  

        $custObject = new customerObject();
        $custObject->cust_first_name=$customer_first_name;
        $custObject->cust_email=$customer_email;
        $custObject->cust_phone1=$customer_phone1;
        $custObject->cust_phone2=$customer_phone2;
        $custObject->cust_address=$customer_address;
        $custObject->cust_city=$customer_city;
        $custObject->cust_zip=$customer_zip;
        $custObject->cust_state=$customer_state;
        $custObject->cust_country=$customer_country;
        $custObject->cust_gender=$customer_gender;
        $custObject->cust_birth_date=$customer_birth_date;
        $custObject->cust_weding_date=$customer_wedding_date;
        $custObject->cust_status='invited';
        $custObject->cust_biz_id=$biz_id;

        $customerModel = new customerModel();
        $customerResponse = $customerModel->addNewCustomer($custObject);

        if($customerResponse->code == 1){
            $response = array();
            $response["warrnings"] = $warrnings;
            $response['customer_token']=utilityManager::encodeToHASH($customerResponse->data);

            return resultObject::withData(1,"Ok",$response);
        }else{
            return $customerResponse;
        }
    }

    /**
     * Summary of updateCustomer
     * @param mixed $data 
     * @return resultObject
     */
    public function updateCustomer($data = array()){

        $response = array();
        $warrnings = array();

        $customerId = utilityManager::decodeFromHASH($data['customerToken']);

        $customerModel = new customerModel();
        $customerResult = $customerModel->getCustomerWithID($customerId);

        if($customerResult->code == 1 && $customerResult->data->cust_id > 0){

            $oneCustomer = $customerResult->data;
            $biz_id = $oneCustomer->cust_biz_id;
            $valid_phone = ($oneCustomer->cust_phone1 == $oneCustomer->cust_valid_phone  && $oneCustomer->cust_phone1 != "");


            if(isset($data['customer_email']) && $data['customer_email'] != ""){

                $checkCustomer = customerManager::isOtherCustomerExistByStatusAndEmail($biz_id,$oneCustomer->cust_id,$oneCustomer->cust_status,$data['customer_email']);

                if($checkCustomer){
                    return resultObject::withData(0,"Customer with this email already exist","CUSTOMER_EXIST");
                }

                if(!filter_var($data['customer_email'], FILTER_VALIDATE_EMAIL)){
                    return resultObject::withData(0,"Wrong email format","WRONG_EMAIL");
                }
            }

            $customer_first_name = isset($data['customer_name']) ? $data['customer_name'] : $oneCustomer->cust_first_name;
            $customer_email = isset($data['customer_email']) ? $data['customer_email'] : $oneCustomer->cust_email;

            $customer_phone2 = isset($data['customer_phone2']) ? $data['customer_phone2'] : $oneCustomer->cust_phone2;
            $customer_address = isset($data['customer_address']) ? $data['customer_address'] : $oneCustomer->cust_address;           
            $customer_city = isset($data['customer_city']) ? $data['customer_city'] : $oneCustomer->cust_city;                    
            $customer_zip = isset($data['customer_zip']) ? $data['customer_zip'] : $oneCustomer->cust_zip;

            if($customer_first_name == ""){
                return resultObject::withData(0,"Customers name is mandatory","MISSING_FIELD");
            }

            $customer_state = $oneCustomer->cust_state != "" ? $oneCustomer->cust_state : "NULL";
            if(isset($data['customer_state']) && $data['customer_state'] != ""){
                $upperCaseState = strtoupper($data['customer_state']);
                $customerState = utilityManager::getStateId($upperCaseState);
                if($customerState != ""){
                    $customer_state = $customerState;
                }else{
                    $warrnings[] = "Wrong state code, please use 2 letters state code";
                }
            }

            $customer_country = $oneCustomer->cust_country != "" ? $oneCustomer->cust_country : "NULL";
            if(isset($data['customer_country']) && $data['customer_country'] != ""){
                $upperCaseCountry = strtoupper($data['customer_country']);
                $customerCountry = utilityManager::getCountryId($upperCaseCountry);
                if($customerCountry != ""){
                    $customer_country = $customerCountry;
                }else{
                    $warrnings[] = "Wrong country code, please use 2 letters country code";
                }
            }


            $customer_gender = $oneCustomer->cust_gender != "" ? $oneCustomer->cust_gender : "NULL";
            $customer_gender = isset($data['customer_gender']) ? $data['customer_gender'] : $customer_gender;
            if(isset($data['customer_gender']) && $customer_gender != ""){
                $customer_gender = (strtolower($data['customer_gender']) == "m") ? 1 : 2;
            }

            $existingBirthDay = $oneCustomer->cust_birth_date == "" ? "NULL" : "{$oneCustomer->cust_birth_date}";
            $existingWeddingDay = $oneCustomer->cust_birth_date == "" ? "NULL" : "{$oneCustomer->cust_weding_date}";

            $customer_birth_date = isset($data['customer_birth_date']) && $data['customer_birth_date'] != "" ? "{$data['customer_birth_date']}" : $existingBirthDay;
            $customer_wedding_date = isset($data['customer_wedding_date']) && $data['customer_birth_date'] != "" ? "{$data['customer_wedding_date']}" : $existingWeddingDay;  

            $oneCustomer->cust_first_name=$customer_first_name;
            $oneCustomer->cust_email=$customer_email;
            $oneCustomer->cust_phone2=$customer_phone2;
            $oneCustomer->cust_address=$customer_address;
            $oneCustomer->cust_city=$customer_city;
            $oneCustomer->cust_zip=$customer_zip;
            $oneCustomer->cust_state=$customer_state;
            $oneCustomer->cust_country=$customer_country;
            $oneCustomer->cust_gender=$customer_gender;
            $oneCustomer->cust_birth_date=$customer_birth_date;
            $oneCustomer->cust_weding_date=$customer_wedding_date;

            $customerResponse = $customerModel->updateCustomer($oneCustomer);

            if($customerResponse->code == 1){
                $response = array();
                $response["warrnings"] = $warrnings;

                return resultObject::withData(1,"Ok",$response);
            }else{
                return $customerResponse;
            }
        }else{   
            return resultObject::withData(0,"No customer listed","NO_CUSTOMER");
        }
    }

    /**
     * Summary of addgetDefaultLevelData
     * @param mixed $data 
     * @return resultObject
     */
    public function addDataElement($data){

        switch ($data["module_id"])
        {
            case 0:
                return $this->getMainMenuLevelData();
            case 3:
                return $this->getNewsLevelData();
            case 6:
                return $this->getLoyaltyLevelData();
            case 9:
                return $this->getProductLevelData();
            case 26:
                return $this->getCouponLevelData();
            case 10:
            case 11:
                return $this->getSubscriptionPunchPassLevelData();
            case 35:
                return $this->getWebBrowsersLevelData();
        	default:
                return $this->addgetDefaultLevelData($data);
        } 
    }
    public function checkLevelDataInsertRequest($data){

        if(!isset($data["module_id"]) || !is_numeric($data["module_id"])){
            $response = resultObject::withData(0,"Module number is mandatory","MISSING_FIELD");

            echo json_encode($response);
            exit;
        }

        if(!isset($data["parent_id"]) || !is_numeric($data["parent_id"])){
            $response = resultObject::withData(0,"Parent id is mandatory","MISSING_FIELD");

            echo json_encode($response);
            exit;
        }

        if(!isset($data["level_number"]) || !is_numeric($data["level_number"]) || $data["level_number"] <= 0){
            $response = resultObject::withData(0,"Level number is mandatory and must be greater than 0","MISSING_FIELD");

            echo json_encode($response);
            exit;
        }

        if(!isset($data["element_id"]) || !is_numeric($data["element_id"]) || $data["element_id"] <= 0){
            $response = resultObject::withData(0,"Element id is mandatory and must be greater than 0","MISSING_FIELD");

            echo json_encode($response);
            exit;
        }
    }
    /**
     * Summary of addgetDefaultLevelData
     * @param mixed $data 
     * @return resultObject
     */
    public function addgetDefaultLevelData($data){

        $biz_id = utilityManager::decodeFromHASH($data["businessToken"]);

        $obj = "levelData{$data["module_id"]}Object";
        $dataObject = new $obj();

        $dataObject->md_mod_id = $data["module_id"];
        $dataObject->md_biz_id = $biz_id;
        $dataObject->md_parent = $data["parent_id"];
        $dataObject->md_level_no = $data["level_number"];
        $dataObject->md_element = $data["element_id"];

        if(isset($data["position"]) && is_numeric($data["position"]) && $data["position"] >= 0){
            $dataObject->md_index = $data["position"];
        }else{
            $dataObject->md_index = levelDataManager::getNextIndex($dataObject);
            $dataObject->md_index = $dataObject->md_index == "" ? 1 : $dataObject->md_index;
        }

        $dataObject->md_desc = isset($data["title"]) ? $data["title"] : $dataObject->md_desc;
        $dataObject->md_info = isset($data["description"]) ? $data["description"] : $dataObject->md_info;
        $dataObject->md_info = isset($data["video_url"]) ? $data["video_url"] : $dataObject->md_info;
        $dataObject->md_info = isset($data["audio_url"]) ? $data["audio_url"] : $dataObject->md_info;

        if($data["element_id"] == 8){
            if(utilityManager::startsWith($dataObject->md_info,"audio")){
                $dataObject->md_bool2 = 1;
            }else{
                if(utilityManager::isAudioFile($dataObject->md_info)){
                    $dataObject->md_info = "audio:".$dataObject->md_info;
                    $dataObject->md_bool2 = 1;
                }
            }
        }

        $levelDataManager = new levelDataManager($data["module_id"]);
        $insertResponse = $levelDataManager->addLevelData($dataObject);

        if($insertResponse->code == 1){

            if(isset($data["image"]) && $data["image"] != ""){
                $dataObject->md_pic = $data["image"];
                $levelDataManager->addImageToQueue($dataObject,$insertResponse->data,"md_pic");
            }

            for($i = 1; $i<=10; $i++){
                if(isset($data["slider_image_$i"]) && $data["slider_image_$i"] != ""){
                    $img = "md_pic$i";
                    $dataObject->$img = $data["slider_image_$i"];
                    $levelDataManager->addImageToQueue($dataObject,$insertResponse->data,"md_pic$i");
                }
            }

            return resultObject::withData(1,"Ok");
        }else{
            return $insertResponse;
        } 
    }
    /**********************************/
    /*         SECURITY FUNCTIONS        */
    /**********************************/

    /**
     * Summary of getApiKeyFromHeader
     * @param mixed Request Header 
     * @return string
     */
    public function getApiKeyFromHeader($REQUEST){
        $apiKeyArray = explode(" ",$REQUEST["Authorization"]);
        return $apiKeyArray[1];
    }

    /**
     * Stops the script if the request is not autorized
     * Must call this function at the beginning of evry API call
     * @param mixed $REQUEST 
     */
    public function preventNotAuthorizedRequest($REQUEST){

        if(!isset($REQUEST["Authorization"]) || !utilityManager::contains($REQUEST["Authorization"],"Bearer")){
            $response = resultObject::withData(0,"Missing Api Key","MISSING_API_KEY");
            echo json_encode($response);
            exit;
        }

        $apiKey = $this->getApiKeyFromHeader($REQUEST);

        if($apiKey == ""){
            $response = resultObject::withData(0,"Missing Api Key","MISSING_API_KEY");
            echo json_encode($response);
            exit;
        }

        $responseReseller = $this->getResellerByApiKey($apiKey);
        if($responseReseller->code == 0 || $responseReseller->data->reseller_id == 0){
            $response = resultObject::withData(0,"Invalid apiKey","INVALID_API_KEY");
            echo json_encode($response);
            exit;
        }

        return $responseReseller->data->reseller_id;
    }

    /**
     * Stops the script if the clients token is invalid or missing
     * The clientToken can be empty, but the parameter must exist
     * Must call this function at the beginning of evry API call that contain clientToken parameter
     * @param mixed $REQUEST 
     */
    public function preventNotAuthorizedClientToken($REQUEST,$apiKey){
        
        if(!isset($REQUEST["clientToken"])){
            
            $response = resultObject::withData(0,"Missing client token","MISSING_CLIENT_TOKEN");

            echo json_encode($response);
            exit;          
        }

        if($REQUEST["clientToken"] != ""){

            $tokenParts = $this->returnClientTokenObject($REQUEST["clientToken"]);
         
            if(count($tokenParts) != 3 || !filter_var($tokenParts[0], FILTER_VALIDATE_EMAIL) || $REQUEST["reseller_id"] != $tokenParts[1]){
                
                $response = resultObject::withData(0,"Invalid client token","INVALID_CLIENT_TOKEN");

                echo json_encode($response);
                exit;
            }
            
            $responseReseller = $this->getResellerByApiKey($apiKey);
            $resellerObject = $responseReseller->data;

            $ownerObject = $resellerObject->resellers_owner;

            $resellerClientModel = new resellerClientModel();
            $responseReselerClientData = $resellerClientModel->getResellerClientByUsernameAndOwnerId($tokenParts[0],$ownerObject->owner_id);
            
            if($responseReselerClientData->code == 0){

                $response = resultObject::withData(0,"Wrong client's email","WRONG_EMAIL");

                echo json_encode($response);
                exit;
            }
        }  
    }

    /**
     * Stops the script if the request missing app token
     * Must call this function at the beginning of evry API call that handle APP
     * @param mixed $REQUEST 
     */
    public function preventNotAuthorizedAppToken($REQUEST){

        if(!isset($REQUEST['businessToken']) || $REQUEST['businessToken'] == ''){

            $response = resultObject::withData(0,"Missing business token","MISSING_APP_TOKEN");

            echo json_encode($response);
            exit;
        }  
        
        $biz_id = utilityManager::decodeFromHASH($REQUEST['businessToken']);

        $bizModel = new bizModel($biz_id);
        $bizObjectResponse = $bizModel->getBiz();
        if($bizObjectResponse->code == 0 || $bizObjectResponse->data->biz_id == 0){

            $response = resultObject::withData(0,"Wrong business token","WRONG_APP_TOKEN");

            echo json_encode($response);
            exit;
        }

        return $bizObjectResponse->data->biz_id;
    }

    /**
     * Stops the script if the request missing customer token
     * Must call this function at the beginning of evry API call that handle Customer
     * @param mixed $REQUEST 
     */
    public function preventNotAuthorizedCustomerToken($REQUEST){

        if(!isset($REQUEST['customerToken']) || $REQUEST['customerToken'] == ''){

            $response = resultObject::withData(0,"Missing customer token","MISSING_CUSTOMER_TOKEN");

            echo json_encode($response);
            exit;
        }  
        
        $customerId = utilityManager::decodeFromHASH($REQUEST['customerToken']);

        $customerModel = new customerModel();
        $customerObjectResponse = $customerModel->getCustomerWithID($customerId);
        if($customerObjectResponse->code == 0 || $customerObjectResponse->data->cust_id == 0){

            $response = resultObject::withData(0,"Wrong customer token","WRONG_CUSTOMER_TOKEN");

            echo json_encode($response);
            exit;
        }
    }

    /**********************************/
    /*         PRIVATE FUNCTIONS        */
    /**********************************/   

    private function getResellerByApiKeyDB($apiKey = ""){

        if ($apiKey == ""){
            throw new Exception("API Key value must be provided");             
        }

        return $this->db->getRow("SELECT * FROM tbl_resellers,tbl_account,tbl_owners 
                                    WHERE reseller_id = owner_reseller_id
                                    AND owner_account_id = ac_id
                                    AND reseller_wl_apikey LIKE '$apiKey'");
    }

    /************************************* */
    /*   BASIC RESELLER - PUBLIC           */
    /************************************* */

    /**
    * Insert new resellerObject to DB
    * Return Data = new resellerObject ID
    * @param resellerObject $resellerObj 
    * @return resultObject
    */
    public function addReseller(resellerObject $resellerObj){       
        try{
            $newId = $this->addResellerDB($resellerObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($resellerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get reseller from DB for provided ID
    * * Return Data = resellerObject
    * @param int $resellerId 
    * @return resultObject
    */
    public function getResellerByID($resellerId){
        
        try {
            $resellerData = $this->loadResellerFromDB($resellerId);
            
            $resellerObj = resellerObject::withData($resellerData);
            $result = resultObject::withData(1,'',$resellerObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$resellerId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update reseller in DB
    * @param resellerObject $resellerObj 
    * @return resultObject
    */
    public function updateReseller(resellerObject $resellerObj){        
        try{
            $this->upateResellerDB($resellerObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($resellerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete reseller from DB
    * @param int $resellerID 
    * @return resultObject
    */
    public function deleteResellerById($resellerID){
            try{
                $this->deleteResellerByIdDB($resellerID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$resellerID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC RESELLER - DB METHODS           */
    /************************************* */

    private function addResellerDB(resellerObject $obj){

        if (!isset($obj)){
            throw new Exception("resellerObject value must be provided");             
        }

        $reseller_agreed_onDate = isset($obj->reseller_agreed_on) ? "'".$obj->reseller_agreed_on."'" : "null";

        $reseller_deactivate_dateDate = isset($obj->reseller_deactivate_date) ? "'".$obj->reseller_deactivate_date."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_resellers SET 
                                        reseller_company_name = '".addslashes($obj->reseller_company_name)."',
                                        reseller_job_title = '".addslashes($obj->reseller_job_title)."',
                                        reseller_full_name = '".addslashes($obj->reseller_full_name)."',
                                        reseller_phone = '".addslashes($obj->reseller_phone)."',
                                        reseller_type = '{$obj->reseller_type}',
                                        reseller_employee_number = {$obj->reseller_employee_number},
                                        reseller_info = '".addslashes($obj->reseller_info)."',
                                        resller_website = '".addslashes($obj->resller_website)."',
                                        reseller_country_id = {$obj->reseller_country_id},
                                        reseller_city = '".addslashes($obj->reseller_city)."',
                                        reseller_zipcode = '".addslashes($obj->reseller_zipcode)."',
                                        reseller_state_id = {$obj->reseller_state_id},
                                        reseller_street = '".addslashes($obj->reseller_street)."',
                                        reseller_street_number = {$obj->reseller_street_number},
                                        reseller_token = '".addslashes($obj->reseller_token)."',
                                        reseller_paymentMethodToken = '".addslashes($obj->reseller_paymentMethodToken)."',
                                        reseller_paymentMethodType = '".addslashes($obj->reseller_paymentMethodType)."',
                                        reseller_cvv = '".addslashes($obj->reseller_cvv)."',
                                        reseller_lastFourDigits = '".addslashes($obj->reseller_lastFourDigits)."',
                                        reseller_name_on_ccard = '".addslashes($obj->reseller_name_on_ccard)."',
                                        reseller_card_subtype = '".addslashes($obj->reseller_card_subtype)."',
                                        reseller_card_country_code = '".addslashes($obj->reseller_card_country_code)."',
                                        reseller_card_vendor = '".addslashes($obj->reseller_card_vendor)."',
                                        reseller_card_type = '".addslashes($obj->reseller_card_type)."',
                                        reseller_card_issuer = '".addslashes($obj->reseller_card_issuer)."',
                                        reseller_card_level = '".addslashes($obj->reseller_card_level)."',
                                        reseller_card_bin = '".addslashes($obj->reseller_card_bin)."',
                                        reseller_billed = {$obj->reseller_billed},
                                        reseller_billed_price = {$obj->reseller_billed_price},
                                        reseller_plan = {$obj->reseller_plan},
                                        reseller_is_demo = {$obj->reseller_is_demo},
                                        reseller_is_wl = {$obj->reseller_is_wl},
                                        reseller_wl_brand_name = '".addslashes($obj->reseller_wl_brand_name)."',
                                        reseller_wl_brand_logo = '".addslashes($obj->reseller_wl_brand_logo)."',
                                        reseller_wl_client_back = '".addslashes($obj->reseller_wl_client_back)."',
                                        reseller_wl_favicon = '".addslashes($obj->reseller_wl_favicon)."',
                                        reseller_wl_protocol = '".addslashes($obj->reseller_wl_protocol)."',
                                        reseller_wl_domain_main = '".addslashes($obj->reseller_wl_domain_main)."',
                                        reseller_wl_domain = '".addslashes($obj->reseller_wl_domain)."',
                                        reseller_wl_market_domain = '".addslashes($obj->reseller_wl_market_domain)."',
                                        reseller_wl_lang = '".addslashes($obj->reseller_wl_lang)."',
                                        reseller_wl_url = '".addslashes($obj->reseller_wl_url)."',
                                        reseller_wl_color_1 = '".addslashes($obj->reseller_wl_color_1)."',
                                        reseller_wl_color_2 = '".addslashes($obj->reseller_wl_color_2)."',
                                        reseller_wl_color_3 = '".addslashes($obj->reseller_wl_color_3)."',
                                        reseller_wl_color_4 = '".addslashes($obj->reseller_wl_color_4)."',
                                        reseller_wl_color_5 = '".addslashes($obj->reseller_wl_color_5)."',
                                        reseller_wl_color_6 = '".addslashes($obj->reseller_wl_color_6)."',
                                        reseller_wl_support_email = '".addslashes($obj->reseller_wl_support_email)."',
                                        reseller_wl_smtp_server = '".addslashes($obj->reseller_wl_smtp_server)."',
                                        reseller_wl_smtp_user = '".addslashes($obj->reseller_wl_smtp_user)."',
                                        reseller_wl_smtp_pass = '".addslashes($obj->reseller_wl_smtp_pass)."',
                                        reseller_wl_include_editor = {$obj->reseller_wl_include_editor},
                                        reseller_redirect_country = '".addslashes($obj->reseller_redirect_country)."',
                                        reseller_wl_email_type = {$obj->reseller_wl_email_type},
                                        reseller_wl_apikey = '".addslashes($obj->reseller_wl_apikey)."',
                                        reseller_public_rsa_key = '". addslashes($obj->reseller_public_rsa_key)."',
                                        reseller_agreed = {$obj->reseller_agreed},
                                        reseller_agreed_on = $reseller_agreed_onDate,
                                        reseller_tier = {$obj->reseller_tier},
                                        reseller_program = {$obj->reseller_program},
                                        reseller_app_price = {$obj->reseller_app_price},
                                        reseller_pcas_price = {$obj->reseller_pcas_price},
                                        reseller_wl_set = {$obj->reseller_wl_set},
                                        reseller_deactivated = {$obj->reseller_deactivated},
                                        reseller_deactivate_date = $reseller_deactivate_dateDate,
                                        reseller_internal = {$obj->reseller_internal},
                                        reseller_has_manager = {$obj->reseller_has_manager},
                                        reseller_has_sso = {$obj->reseller_has_sso},
                                        reseller_app_ios_skin = '".addslashes($obj->reseller_app_ios_skin)."',
                                        reseller_app_android_skin = '".addslashes($obj->reseller_app_android_skin)."',
                                        reseller_app_ios_sufix = '".addslashes($obj->reseller_app_ios_sufix)."',
                                        reseller_app_name = '".addslashes($obj->reseller_app_name)."',
                                        reseller_app_color = '".addslashes($obj->reseller_app_color)."',
                                        reseller_app_icon = '".addslashes($obj->reseller_app_icon)."',
                                        reseller_app_splash = '".addslashes($obj->reseller_app_splash)."',
                                        reseller_app_desc = '".addslashes($obj->reseller_app_desc)."',
                                        reseller_app_support_url = '".addslashes($obj->reseller_app_support_url)."',
                                        reseller_app_marketing_url = '".addslashes($obj->reseller_app_marketing_url)."',
                                        reseller_app_privacy_url = '".addslashes($obj->reseller_app_privacy_url)."',
                                        reseller_app_keywords = '".addslashes($obj->reseller_app_keywords)."',
                                        reseller_app_logo = '".addslashes($obj->reseller_app_logo)."',
                                        reseller_web_hook = '".addslashes($obj->reseller_web_hook)."',
                                        reseller_app_welcome_text = '".addslashes($obj->reseller_app_welcome_text)."',
                                        reseller_show_preview = {$obj->reseller_show_preview},
                                        reseller_group = '{$obj->reseller_group}',
                                        reseller_display_tooltips = {$obj->reseller_display_tooltips},
                                        reseller_billing_model = '{$obj->reseller_billing_model}',
                                        reseller_billing_trial_days = {$obj->reseller_billing_trial_days}
                                                 ");
        return $newId;
    }

    private function loadResellerFromDB($resellerID){

        if (!is_numeric($resellerID) || $resellerID <= 0){
            throw new Exception("Illegal value resellerID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_resellers,tbl_owners,tbl_account
                                    WHERE  owner_reseller_id = reseller_id
                                    AND owner_account_id = ac_id
                                    AND reseller_id = $resellerID");
    }

    private function upateResellerDB(resellerObject $obj){

        if (!isset($obj->reseller_id) || !is_numeric($obj->reseller_id) || $obj->reseller_id <= 0){
            throw new Exception("resellerObject value must be provided");             
        }

        $reseller_agreed_onDate = isset($obj->reseller_agreed_on) ? "'".$obj->reseller_agreed_on."'" : "null";

        $reseller_deactivate_dateDate = isset($obj->reseller_deactivate_date) ? "'".$obj->reseller_deactivate_date."'" : "null";

        $this->db->execute("UPDATE tbl_resellers SET 
                                reseller_company_name = '".addslashes($obj->reseller_company_name)."',
                                reseller_job_title = '".addslashes($obj->reseller_job_title)."',
                                reseller_full_name = '".addslashes($obj->reseller_full_name)."',
                                reseller_phone = '".addslashes($obj->reseller_phone)."',
                                reseller_type = '{$obj->reseller_type}',
                                reseller_employee_number = {$obj->reseller_employee_number},
                                reseller_info = '".addslashes($obj->reseller_info)."',
                                resller_website = '".addslashes($obj->resller_website)."',
                                reseller_country_id = {$obj->reseller_country_id},
                                reseller_city = '".addslashes($obj->reseller_city)."',
                                reseller_zipcode = '".addslashes($obj->reseller_zipcode)."',
                                reseller_state_id = {$obj->reseller_state_id},
                                reseller_street = '".addslashes($obj->reseller_street)."',
                                reseller_street_number = {$obj->reseller_street_number},
                                reseller_token = '".addslashes($obj->reseller_token)."',
                                reseller_paymentMethodToken = '".addslashes($obj->reseller_paymentMethodToken)."',
                                reseller_paymentMethodType = '".addslashes($obj->reseller_paymentMethodType)."',
                                reseller_cvv = '".addslashes($obj->reseller_cvv)."',
                                reseller_lastFourDigits = '".addslashes($obj->reseller_lastFourDigits)."',
                                reseller_name_on_ccard = '".addslashes($obj->reseller_name_on_ccard)."',
                                reseller_card_subtype = '".addslashes($obj->reseller_card_subtype)."',
                                reseller_card_country_code = '".addslashes($obj->reseller_card_country_code)."',
                                reseller_card_vendor = '".addslashes($obj->reseller_card_vendor)."',
                                reseller_card_type = '".addslashes($obj->reseller_card_type)."',
                                reseller_card_issuer = '".addslashes($obj->reseller_card_issuer)."',
                                reseller_card_level = '".addslashes($obj->reseller_card_level)."',
                                reseller_card_bin = '".addslashes($obj->reseller_card_bin)."',
                                reseller_billed = {$obj->reseller_billed},
                                reseller_billed_price = {$obj->reseller_billed_price},
                                reseller_plan = {$obj->reseller_plan},
                                reseller_is_demo = {$obj->reseller_is_demo},
                                reseller_is_wl = {$obj->reseller_is_wl},
                                reseller_wl_brand_name = '".addslashes($obj->reseller_wl_brand_name)."',
                                reseller_wl_brand_logo = '".addslashes($obj->reseller_wl_brand_logo)."',
                                reseller_wl_client_back = '".addslashes($obj->reseller_wl_client_back)."',
                                reseller_wl_favicon = '".addslashes($obj->reseller_wl_favicon)."',
                                reseller_wl_protocol = '".addslashes($obj->reseller_wl_protocol)."',
                                reseller_wl_domain_main = '".addslashes($obj->reseller_wl_domain_main)."',
                                reseller_wl_domain = '".addslashes($obj->reseller_wl_domain)."',
                                reseller_wl_market_domain = '".addslashes($obj->reseller_wl_market_domain)."',
                                reseller_wl_lang = '".addslashes($obj->reseller_wl_lang)."',
                                reseller_wl_url = '".addslashes($obj->reseller_wl_url)."',
                                reseller_wl_color_1 = '".addslashes($obj->reseller_wl_color_1)."',
                                reseller_wl_color_2 = '".addslashes($obj->reseller_wl_color_2)."',
                                reseller_wl_color_3 = '".addslashes($obj->reseller_wl_color_3)."',
                                reseller_wl_color_4 = '".addslashes($obj->reseller_wl_color_4)."',
                                reseller_wl_color_5 = '".addslashes($obj->reseller_wl_color_5)."',
                                reseller_wl_color_6 = '".addslashes($obj->reseller_wl_color_6)."',
                                reseller_wl_support_email = '".addslashes($obj->reseller_wl_support_email)."',
                                reseller_wl_smtp_server = '".addslashes($obj->reseller_wl_smtp_server)."',
                                reseller_wl_smtp_user = '".addslashes($obj->reseller_wl_smtp_user)."',
                                reseller_wl_smtp_pass = '".addslashes($obj->reseller_wl_smtp_pass)."',
                                reseller_wl_include_editor = {$obj->reseller_wl_include_editor},
                                reseller_redirect_country = '".addslashes($obj->reseller_redirect_country)."',
                                reseller_wl_email_type = {$obj->reseller_wl_email_type},
                                reseller_wl_apikey = '".addslashes($obj->reseller_wl_apikey)."',
                                reseller_public_rsa_key = '". addslashes($obj->reseller_public_rsa_key)."',
                                reseller_agreed = {$obj->reseller_agreed},
                                reseller_agreed_on = $reseller_agreed_onDate,
                                reseller_tier = {$obj->reseller_tier},
                                reseller_program = {$obj->reseller_program},
                                reseller_app_price = {$obj->reseller_app_price},
                                reseller_pcas_price = {$obj->reseller_pcas_price},
                                reseller_wl_set = {$obj->reseller_wl_set},
                                reseller_deactivated = {$obj->reseller_deactivated},
                                reseller_deactivate_date = $reseller_deactivate_dateDate,
                                reseller_internal = {$obj->reseller_internal},
                                reseller_has_manager = {$obj->reseller_has_manager},
                                reseller_has_sso = {$obj->reseller_has_sso},
                                reseller_app_ios_skin = '".addslashes($obj->reseller_app_ios_skin)."',
                                reseller_app_android_skin = '".addslashes($obj->reseller_app_android_skin)."',
                                reseller_app_ios_sufix = '".addslashes($obj->reseller_app_ios_sufix)."',
                                reseller_app_name = '".addslashes($obj->reseller_app_name)."',
                                reseller_app_color = '".addslashes($obj->reseller_app_color)."',
                                reseller_app_icon = '".addslashes($obj->reseller_app_icon)."',
                                reseller_app_splash = '".addslashes($obj->reseller_app_splash)."',
                                reseller_app_desc = '".addslashes($obj->reseller_app_desc)."',
                                reseller_app_support_url = '".addslashes($obj->reseller_app_support_url)."',
                                reseller_app_marketing_url = '".addslashes($obj->reseller_app_marketing_url)."',
                                reseller_app_privacy_url = '".addslashes($obj->reseller_app_privacy_url)."',
                                reseller_app_keywords = '".addslashes($obj->reseller_app_keywords)."',
                                reseller_app_logo = '".addslashes($obj->reseller_app_logo)."',
                                reseller_web_hook = '".addslashes($obj->reseller_web_hook)."',
                                reseller_app_welcome_text = '".addslashes($obj->reseller_app_welcome_text)."',
                                reseller_show_preview = {$obj->reseller_show_preview},
                                reseller_group = '{$obj->reseller_group}',
                                reseller_display_tooltips = {$obj->reseller_display_tooltips},
                                reseller_billing_model = '{$obj->reseller_billing_model}',
                                reseller_billing_trial_days = {$obj->reseller_billing_trial_days}
                                WHERE reseller_id = {$obj->reseller_id} 
                                         ");
        }

    private function deleteResellerByIdDB($resellerID){

        if (!is_numeric($resellerID) || $resellerID <= 0){
            throw new Exception("Illegal value resellerID");             
        }

        $this->db->execute("DELETE FROM tbl_resellers WHERE reseller_id = $resellerID");
    }

    /************************************* */
    /*   BASIC RESELLERCLIENTS - PUBLIC           */
    /************************************* */

    /**
    * Insert new resellerClientsObject to DB
    * Return Data = new resellerClientsObject ID
    * @param resellerClientsObject $resellerClientsObj 
    * @return resultObject
    */
    public function addResellerClients(resellerClientsObject $resellerClientsObj){       
        try{
            $newId = $this->addResellerClientsDB($resellerClientsObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($resellerClientsObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get resellerClients from DB for provided ID
    * * Return Data = resellerClientsObject
    * @param int $resellerClientsId 
    * @return resultObject
    */
    public function getResellerClientsByID($resellerClientsId){
        
        try {
            $resellerClientsData = $this->loadResellerClientsFromDB($resellerClientsId);
            
            $resellerClientsObj = resellerClientsObject::withData($resellerClientsData);
            $result = resultObject::withData(1,'',$resellerClientsObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$resellerClientsId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update a1History in DB
    * @param resellerClientsObject $resellerClientsObj 
    * @return resultObject
    */
    public function updateResellerClients(resellerClientsObject $resellerClientsObj){        
        try{
            $this->upateResellerClientsDB($resellerClientsObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($resellerClientsObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete resellerClients from DB
    * @param int $resellerClientsID 
    * @return resultObject
    */
    public function deleteResellerClientsById($resellerClientsID){
        try{
            $this->deleteResellerClientsByIdDB($resellerClientsID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$resellerClientsID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC RESELLERCLIENTS - DB METHODS           */
    /************************************* */

    private function addResellerClientsDB(resellerClientsObject $obj){

        if (!isset($obj)){
            throw new Exception("resellerClientsObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_reseller_clients SET 
        reseller_client_owner_id = {$obj->reseller_client_owner_id},
        reseller_client_username = '".addslashes($obj->reseller_client_username)."',
        reseller_client_password = '".addslashes($obj->reseller_client_password)."',
        reseller_client_lang = '".addslashes($obj->reseller_client_lang)."',
        reseller_client_isactive = {$obj->reseller_client_isactive},
        reseller_client_account_id = {$obj->reseller_client_account_id},
        reseller_client_stripe_cust_id = '".addslashes($obj->reseller_client_stripe_cust_id)."',
        reseller_client_country = {$obj->reseller_client_country}
                 ");
            return $newId;
    }

    private function loadResellerClientsFromDB($resellerClientsID){

        if (!is_numeric($resellerClientsID) || $resellerClientsID <= 0){
            throw new Exception("Illegal value resellerClientsID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_reseller_clients WHERE reseller_client_id = $resellerClientsID");
    }

    private function upateResellerClientsDB(resellerClientsObject $obj){

        if (!isset($obj->reseller_client_id) || !is_numeric($obj->reseller_client_id) || $obj->reseller_client_id <= 0){
            throw new Exception("resellerClientsObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_reseller_clients SET 
                                reseller_client_owner_id = {$obj->reseller_client_owner_id},
                                reseller_client_username = '".addslashes($obj->reseller_client_username)."',
                                reseller_client_password = '".addslashes($obj->reseller_client_password)."',
                                reseller_client_lang = '".addslashes($obj->reseller_client_lang)."',
                                reseller_client_isactive = {$obj->reseller_client_isactive},
                                reseller_client_account_id = {$obj->reseller_client_account_id},
                                reseller_client_stripe_cust_id = '".addslashes($obj->reseller_client_stripe_cust_id)."',
                                reseller_client_country = {$obj->reseller_client_country}
                                WHERE reseller_client_id = {$obj->reseller_client_id} 
                                         ");
        }

    private function deleteResellerClientsByIdDB($resellerClientsID){

        if (!is_numeric($resellerClientsID) || $resellerClientsID <= 0){
            throw new Exception("Illegal value resellerClientsID");             
        }

        $this->db->execute("DELETE FROM tbl_reseller_clients WHERE reseller_client_id = $resellerClientsID");
    }

    /************************************* */
    /*   BASIC RESELLERCLIENTPAYMENTSOURCE - PUBLIC           */
    /************************************* */

    /**
    * Insert new resellerClientPaymentSourceObject to DB
    * Return Data = new resellerClientPaymentSourceObject ID
    * @param resellerClientPaymentSourceObject $resellerClientPaymentSourceObj 
    * @return resultObject
    */
    public function addResellerClientPaymentSource(resellerClientPaymentSourceObject $resellerClientPaymentSourceObj){       
        try{
            $newId = $this->addResellerClientPaymentSourceDB($resellerClientPaymentSourceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($resellerClientPaymentSourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get resellerClientPaymentSource from DB for provided ID
    * * Return Data = resellerClientPaymentSourceObject
    * @param int $resellerClientPaymentSourceId 
    * @return resultObject
    */
    public function getResellerClientPaymentSourceByID($resellerClientPaymentSourceId){
        
        try {
            $resellerClientPaymentSourceData = $this->loadResellerClientPaymentSourceFromDB($resellerClientPaymentSourceId);
            
            $resellerClientPaymentSourceObj = resellerClientPaymentSourceObject::withData($resellerClientPaymentSourceData);
            $result = resultObject::withData(1,'',$resellerClientPaymentSourceObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$resellerClientPaymentSourceId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update resellerClientPaymentSourceObject in DB
    * @param resellerClientPaymentSourceObject $resellerClientPaymentSourceObj 
    * @return resultObject
    */
    public function updateResellerClientPaymentSource(resellerClientPaymentSourceObject $resellerClientPaymentSourceObj){        
        try{
            $this->upateResellerClientPaymentSourceDB($resellerClientPaymentSourceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($resellerClientPaymentSourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete resellerClientPaymentSource from DB
    * @param int $resellerClientPaymentSourceID 
    * @return resultObject
    */
    public function deleteResellerClientPaymentSourceById($resellerClientPaymentSourceID){
        try{
            $this->deleteResellerClientPaymentSourceByIdDB($resellerClientPaymentSourceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$resellerClientPaymentSourceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC RESELLERCLIENTPAYMENTSOURCE - DB METHODS           */
    /************************************* */

    private function addResellerClientPaymentSourceDB(resellerClientPaymentSourceObject $obj){

        if (!isset($obj)){
            throw new Exception("resellerClientPaymentSourceObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_reseller_client_payment_sources SET 
                                        ops_owner_id = {$obj->ops_owner_id},
                                        ops_type = '".addslashes($obj->ops_type)."',
                                        ops_ip = '".addslashes($obj->ops_ip)."',
                                        ops_token = '".addslashes($obj->ops_token)."',
                                        ops_stipe_object_id = '".addslashes($obj->ops_stipe_object_id)."',
                                        ops_card_brand = '".addslashes($obj->ops_card_brand)."',
                                        ops_country = '".addslashes($obj->ops_country)."',
                                        ops_exp_month = {$obj->ops_exp_month},
                                        ops_exp_year = {$obj->ops_exp_year},
                                        ops_funding = '".addslashes($obj->ops_funding)."',
                                        ops_last4 = '".addslashes($obj->ops_last4)."',
                                        ops_name = '".addslashes($obj->ops_name)."',
                                        ops_phone = '".addslashes($obj->ops_phone)."',
                                        ops_valid = {$obj->ops_valid},
                                        ops_default = {$obj->ops_default}
                                                 ");
        return $newId;
    }

    private function loadResellerClientPaymentSourceFromDB($resellerClientPaymentSourceID){

        if (!is_numeric($resellerClientPaymentSourceID) || $resellerClientPaymentSourceID <= 0){
            throw new Exception("Illegal value resellerClientPaymentSourceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_reseller_client_payment_sources WHERE ops_id = $resellerClientPaymentSourceID");
    }

    private function upateResellerClientPaymentSourceDB(resellerClientPaymentSourceObject $obj){

        if (!isset($obj->ops_id) || !is_numeric($obj->ops_id) || $obj->ops_id <= 0){
            throw new Exception("resellerClientPaymentSourceObject value must be provided");             
        }

        $ops_createdDate = isset($obj->ops_created) ? "'".$obj->ops_created."'" : "null";

        $this->db->execute("UPDATE tbl_reseller_client_payment_sources SET 
        ops_owner_id = {$obj->ops_owner_id},
        ops_type = '".addslashes($obj->ops_type)."',
        ops_created = $ops_createdDate,
        ops_ip = '".addslashes($obj->ops_ip)."',
        ops_token = '".addslashes($obj->ops_token)."',
        ops_stipe_object_id = '".addslashes($obj->ops_stipe_object_id)."',
        ops_card_brand = '".addslashes($obj->ops_card_brand)."',
        ops_country = '".addslashes($obj->ops_country)."',
        ops_exp_month = {$obj->ops_exp_month},
        ops_exp_year = {$obj->ops_exp_year},
        ops_funding = '".addslashes($obj->ops_funding)."',
        ops_last4 = '".addslashes($obj->ops_last4)."',
        ops_name = '".addslashes($obj->ops_name)."',
        ops_phone = '".addslashes($obj->ops_phone)."',
        ops_valid = {$obj->ops_valid},
        ops_default = {$obj->ops_default}
        WHERE ops_id = {$obj->ops_id} 
                    ");
    }

    private function deleteResellerClientPaymentSourceByIdDB($resellerClientPaymentSourceID){

        if (!is_numeric($resellerClientPaymentSourceID) || $resellerClientPaymentSourceID <= 0){
            throw new Exception("Illegal value resellerClientPaymentSourceID");             
        }

        $this->db->execute("DELETE FROM tbl_reseller_client_payment_sources WHERE ops_id = $resellerClientPaymentSourceID");
    }

    /************************************* */
    /*              HELPERS                */
    /************************************* */

    /**
     * Summary of returnClientTokenObject
     * - email
     * - reseller id
     * - password
     * @param mixed $clientToken 
     * @return array
     */
    private function returnClientTokenObject($clientToken){
        $clientTokenString = utilityManager::decodeFromHASH($clientToken,true);
        return explode("^",$clientTokenString);
    }
}
