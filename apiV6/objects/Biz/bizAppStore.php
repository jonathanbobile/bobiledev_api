<?php

/**
 * bizAppStore short summary.
 *
 * bizAppStore description.
 *
 * @version 1.0
 * @author Dany
 */
class bizAppStore extends bobileObject
{
    public $id = "0";
    public $name;
    public $short_description;
    public $description;
    public $image;
    public $price = "0";
    public $connectedToBiz = false;
    public $stage = array();

    function __construct(){
       
    }

    public static function withData($data){
        if(!isset($data["id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        $instance->id = $data["id"];
        $instance->name = $data["name"];
        $instance->short_description = $data["short_description"];
        $instance->description = $data["description"];
        $instance->image = $data["image"];
        $instance->price = $data["price"];

        return $instance;
    }
}
