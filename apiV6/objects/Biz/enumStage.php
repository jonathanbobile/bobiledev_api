<?php

/**
 * Enum for appstore stage types
 *
 * @version 1.0
 * @author PapTap
 */

class enumStage extends Enum {

    const NOT_PUBLISHED = 1; 


    const PENDING_YOUR_APPROVAL = 2;       


    const IN_SUBMISSION = 3;      


    const WAITING_FOR_REVIEW = 4; 
    

    const APPROVED_LIVE = 5;


    const REJECTED = 6;
    

    const DEVELOPER_ACCOUNT_REQUIRED = 7;

}
?>