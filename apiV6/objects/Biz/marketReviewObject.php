<?php 

class marketReviewObject extends bobileObject { 

    public $rate_id = "0";
    public $rate_biz_id = "0";
    public $rate_deviceid;
    public $rate_nickname;
    public $rate_date_time = "";
    public $rate_score = "0";
    public $rate_review_text;
    public $rate_despute = "0";
    public $rate_market = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["rate_id"])){
              throw new Exception("marketReviewObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->rate_id = $data["rate_id"];
        $instance->rate_biz_id = $data["rate_biz_id"];
        $instance->rate_deviceid = $data["rate_deviceid"];
        $instance->rate_nickname = $data["rate_nickname"];
        $instance->rate_date_time = $data["rate_date_time"];
        $instance->rate_score = $data["rate_score"];
        $instance->rate_review_text = $data["rate_review_text"];
        $instance->rate_despute = $data["rate_despute"];
        $instance->rate_market = $data["rate_market"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


