<?php

/**
 * membershipBenefitObject short summary.
 *
 * membershipBenefitObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class membershipBenefitObject extends bobileObject
{
    /* Identifiers */
    public $mben_id;
    public $mben_mb_id;
    public $mben_biz_id;

    /* Details */
    public $mben_text;
    public $mben_image;
    public $mben_title;

    /* Settings */
    public $mben_type;
    public $mben_entity_id;    
    public $mben_amount;

    /* Connected object */
    public $connected_object = null;

    function __construct(){
        
    }

    public static function withData($membershipBenefitData){
        if(!isset($membershipBenefitData["mben_id"])){
            throw new Exception("Data incorrect - membership benefit");
        }

        $instance = new self();

        /* Identifiers */
        $instance->mben_id = isset($membershipBenefitData["mben_id"]) && $membershipBenefitData["mben_id"] > 0 ? $membershipBenefitData["mben_id"] : 0;
        $instance->mben_mb_id = isset($membershipBenefitData["mben_mb_id"]) && $membershipBenefitData["mben_mb_id"] > 0 ? $membershipBenefitData["mben_mb_id"] : 0;
        $instance->mben_biz_id = isset($membershipBenefitData["mben_biz_id"]) && $membershipBenefitData["mben_biz_id"] > 0 ? $membershipBenefitData["mben_biz_id"] : 0;

        /* Details */
        $instance->mben_text = $membershipBenefitData["mben_text"];
        $instance->mben_image = $membershipBenefitData["mben_image"];
        $instance->mben_title = $membershipBenefitData["mben_title"];

        /* Settings */
        $instance->mben_type = $membershipBenefitData["mben_type"];
        $instance->mben_entity_id = $membershipBenefitData["mben_entity_id"];
        $instance->mben_amount = $membershipBenefitData["mben_amount"];

        return $instance;
    }
}
