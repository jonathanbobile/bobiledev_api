<?php 

class moduleStructureObject extends bobileObject { 

    public $ms_mod_id = "0";
    public $ms_level_no = "0";
    public $ms_friendly_name;
    public $ms_view_type;
    public $ms_view_type_android;
    public $ms_view_end;
    public $ms_view_end_android;
    public $ms_nib_name;
    public $ms_nib_name_android;
    public $ms_template = "0";
    public $ms_font = "arial";
    public $ms_font_color = "#000000";
    public $ms_btn_image;
    public $ms_html_site;
    public $ms_html_element;
    public $ms_icon;
    public $ms_img1_w = "1";
    public $ms_img2_w = "1";
    public $ms_img3_w = "1";
    public $ms_img4_w = "1";
    public $ms_img5_w = "1";
    public $ms_img6_w = "1";
    public $ms_img7_w = "1";
    public $ms_img8_w = "1";
    public $ms_img9_w = "1";
    public $ms_img10_w = "1";
    public $ms_img1_default = "no_icon_levels.png";
    public $ms_html;
    public $ms_tooltip_id = "0";
    public $ms_good_for;//enum('','content','all','bobilex')
    public $ms_req_mods;
    public $ms_mainpage_cat = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["ms_mod_id"])){
              throw new Exception("moduleStructureObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->ms_mod_id = $data["ms_mod_id"];
        $instance->ms_level_no = $data["ms_level_no"];
        $instance->ms_friendly_name = $data["ms_friendly_name"];
        $instance->ms_view_type = $data["ms_view_type"];
        $instance->ms_view_type_android = $data["ms_view_type_android"];
        $instance->ms_view_end = $data["ms_view_end"];
        $instance->ms_view_end_android = $data["ms_view_end_android"];
        $instance->ms_nib_name = $data["ms_nib_name"];
        $instance->ms_nib_name_android = $data["ms_nib_name_android"];
        $instance->ms_template = $data["ms_template"];
        $instance->ms_font = $data["ms_font"];
        $instance->ms_font_color = $data["ms_font_color"];
        $instance->ms_btn_image = $data["ms_btn_image"];
        $instance->ms_html_site = $data["ms_html_site"];
        $instance->ms_html_element = $data["ms_html_element"];
        $instance->ms_icon = $data["ms_icon"];
        $instance->ms_img1_w = $data["ms_img1_w"];
        $instance->ms_img2_w = $data["ms_img2_w"];
        $instance->ms_img3_w = $data["ms_img3_w"];
        $instance->ms_img4_w = $data["ms_img4_w"];
        $instance->ms_img5_w = $data["ms_img5_w"];
        $instance->ms_img6_w = $data["ms_img6_w"];
        $instance->ms_img7_w = $data["ms_img7_w"];
        $instance->ms_img8_w = $data["ms_img8_w"];
        $instance->ms_img9_w = $data["ms_img9_w"];
        $instance->ms_img10_w = $data["ms_img10_w"];
        $instance->ms_img1_default = $data["ms_img1_default"];
        $instance->ms_html = $data["ms_html"];
        $instance->ms_tooltip_id = $data["ms_tooltip_id"];
        $instance->ms_good_for = $data["ms_good_for"];
        $instance->ms_req_mods = $data["ms_req_mods"];
        $instance->ms_mainpage_cat = $data["ms_mainpage_cat"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


