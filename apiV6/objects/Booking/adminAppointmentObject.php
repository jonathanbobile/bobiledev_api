<?php

/**
 * adminAppointmentObject - The object for admin appointments
 *
 * adminAppointmentObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class adminAppointmentObject
{
    public $appoint_id;
    public $source_id;
    public $label;
    public $start_time;
    public $start_time_unix;
    public $end_time;
    public $end_time_unix;
    public $emp_name;
    public $emp_img;
    public $emp_id;
    public $color = "";
    public $type;
    
    public static function withdata($data){
        $instance = new self();

        $instance->appoint_id = $data["appoint_id"];
        $instance->source_id = $data["source_id"];
        $instance->label = $data["label"];
        $instance->start_time = $data["start_time"];
        $instance->start_time_unix = strtotime($data["start_time"]);
        $instance->end_time = $data["end_time"];
        $instance->end_time_unix = strtotime($data["end_time"]);
        $instance->emp_name = $data["emp_name"];
        $instance->emp_img = $data["emp_img"];
        $instance->emp_id = $data["emp_id"];
        $instance->type = $data["type"];
        $instance->color = $data["color"];

        return $instance;
    }

    public static function withMeeting(employeeMeetingObject $meeting){
        $instance = new self();

        $instance->appoint_id = $meeting->em_id;
        $instance->source_id = $meeting->em_meet_type;
        $instance->label = $meeting->em_name;
        $instance->start_time = $meeting->em_start;
        $instance->start_time_unix = strtotime($meeting->em_start);
        $instance->end_time = $meeting->em_end;
        $instance->end_time_unix = strtotime($meeting->em_end);
        $instance->emp_name = isset($meeting->employee) ? $meeting->employee->be_name : "";
        $instance->emp_img = isset($meeting->employee) ? $meeting->employee->be_pic : "";
        $instance->emp_id = $meeting->em_emp_id;
        $instance->type = 'meeting';

        $serviceResult = bookingManager::getServiceByID($meeting->em_meet_type);
        if($serviceResult->code == 1){
            $instance->color = $serviceResult->data->bmt_color;
        }

        

        return $instance;
    }

    public static function withClassDate(classObject $class,classDateObject $classDate){
        $instance = new self();

        $instance->appoint_id = $classDate->ccd_id;
        $instance->source_id = $classDate->ccd_class_id;
        $instance->label = $class->calc_name;
        $instance->start_time = date("Y-m-d H:i:s",$classDate->ccd_start_unix);
        $instance->start_time_unix = $classDate->ccd_start_unix;
        $instance->end_time = date("Y-m-d H:i:s",$classDate->ccd_end_unix);
        $instance->end_time_unix = $classDate->ccd_end_unix;
        $instance->emp_name = isset($class->employee) ? $class->employee->be_name : "";
        $instance->emp_img = isset($class->employee) ? $class->employee->be_pic : "";
        $instance->emp_id = $class->employee->be_id;
        $instance->type = 'class';

        return $instance;
    }
}