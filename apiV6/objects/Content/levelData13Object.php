<?php 

class levelData13Object extends levelDataObject { 

    public $md_mod_id = "13";

    public $md_win_ratio = "0";
    public $md_prize_type;//enum('product','service','other','points')
    public $md_prize_id = "0";
    public $md_prize_value = "0";
    public $md_prize_text;
    public $md_prize_amount = "0";
    public $md_prize_current_amount = "0";
    public $md_back_type;//enum('image','color')
    public $md_back_data;
    public $md_card_expire;
    public $md_consolation = "0";
    public $md_consolation_text;
    public $card_won;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData13Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        $instance->md_win_ratio = $data["md_win_ratio"];
        $instance->md_prize_type = $data["md_prize_type"];
        $instance->md_prize_id = $data["md_prize_id"];
        $instance->md_prize_value = $data["md_prize_value"];
        $instance->md_prize_text = $data["md_prize_text"];
        $instance->md_prize_amount = $data["md_prize_amount"];
        $instance->md_prize_current_amount = $data["md_prize_current_amount"];
        $instance->md_back_type = $data["md_back_type"];
        $instance->md_back_data = $data["md_back_data"];
        $instance->md_card_expire = $data["md_card_expire"];
        $instance->md_consolation = $data["md_consolation"];
        $instance->md_consolation_text = $data["md_consolation_text"];

        return $instance;
    }
}
?>


