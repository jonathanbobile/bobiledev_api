<?php

/**
 * customerSubsciptionObject short summary.
 *
 * customerSubsciptionObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerSubsciptionObject extends bobileObject
{
    /* Identifiers */
    public $csu_id = "0";
    public $csu_cust_id = "0";
    public $csu_sub_id = "0";
    public $csu_biz_id = "0";

    /* Settings */
    public $csu_price = "0";
    public $csu_usage_rule;//enum('unlimited','n_times','n_times_period')
    public $csu_usage_period_unit;//enum('day','month','year','week')
    public $csu_billing_type;//enum('once','auto_renew')
    public $csu_billing_period_unit;//enum('month','year')

    /* Status */
    public $csu_status = "active";//enum('active','frozen','cancelled','ended','user_canceled','expired')
    public $csu_is_primary = "0";
    public $csu_purchase_time;
    public $csu_usage_start_time = "0000-00-00 00:00:00";
    public $csu_usage_end_time;    
    public $csu_biling_payments_total = "0";
    public $csu_billing_payments_left = "0";
    public $csu_billing_next_date;    
    public $csu_usage_capacity = "0";
    public $csu_usage_uses_left = "0";
    public $csu_usage_next_renew;
    public $csu_qr_url = "";

    /* For IOS */
    public $csu_usage_start_time_unix;
    public $csu_usage_next_renew_unix;
    public $csu_usage_end_time_unix;

    /* Subscription */
    public $subscription;
    public $usages = array();

    public static function withData($customerSubscriptionData,$withInnerSubscription = true){
        if(!isset($customerSubscriptionData["csu_id"])){
            throw new Exception("Data incorrect - subscription");
        }

        $instance = new self();

        /* Identifiers */
        $instance->csu_id = isset($customerSubscriptionData["csu_id"]) ? $customerSubscriptionData["csu_id"] : 0;
        $instance->csu_cust_id = isset($customerSubscriptionData["csu_cust_id"]) ? $customerSubscriptionData["csu_cust_id"] : 0;
        $instance->csu_sub_id = isset($customerSubscriptionData["csu_sub_id"]) ? $customerSubscriptionData["csu_sub_id"] : 0;
        $instance->csu_biz_id = isset($customerSubscriptionData["csu_biz_id"]) ? $customerSubscriptionData["csu_biz_id"] : 0;

        /* Settings */
        $instance->csu_price = $customerSubscriptionData["csu_price"];
        $instance->csu_usage_rule = $customerSubscriptionData["csu_usage_rule"];
        $instance->csu_usage_period_unit = $customerSubscriptionData["csu_usage_period_unit"];
        $instance->csu_billing_type = $customerSubscriptionData["csu_billing_type"];
        $instance->csu_billing_period_unit = $customerSubscriptionData["csu_billing_period_unit"];

        /* Status */
        $instance->csu_status = $customerSubscriptionData["csu_status"];
        $instance->csu_is_primary = $customerSubscriptionData["csu_is_primary"];
        $instance->csu_purchase_time = $customerSubscriptionData["csu_purchase_time"];
        $instance->csu_usage_start_time = $customerSubscriptionData["csu_usage_start_time"];
        $instance->csu_usage_end_time = $customerSubscriptionData["csu_usage_end_time"];        
        $instance->csu_biling_payments_total = $customerSubscriptionData["csu_biling_payments_total"];
        $instance->csu_billing_payments_left = $customerSubscriptionData["csu_billing_payments_left"];
        $instance->csu_billing_next_date = $customerSubscriptionData["csu_billing_next_date"];        
        $instance->csu_usage_capacity = $customerSubscriptionData["csu_usage_capacity"];
        $instance->csu_usage_uses_left = $customerSubscriptionData["csu_usage_uses_left"];
        $instance->csu_usage_next_renew = $customerSubscriptionData["csu_usage_next_renew"];

        $instance->csu_usage_end_time_unix = isset($customerSubscriptionData["csu_usage_end_time"]) ? strtotime($customerSubscriptionData["csu_usage_end_time"]) : 0;
        $instance->csu_usage_next_renew_unix = isset($customerSubscriptionData["csu_usage_next_renew"]) ? strtotime($customerSubscriptionData["csu_usage_next_renew"]) : 0;
        $instance->csu_usage_start_time_unix = isset($customerSubscriptionData["csu_usage_start_time"]) ? strtotime($customerSubscriptionData["csu_usage_start_time"]) : 0;

        if($withInnerSubscription && isset($customerSubscriptionData['md_row_id'])){
            $instance->subscription = levelData10Object::withData($customerSubscriptionData);
        }

        if(isset($instance->csu_id) && $instance->csu_id != '' && customerSubscriptionManager::isPunchpassMechAllowed($instance->csu_id,'scan')){
            $instance->csu_qr_url = "https://".API_SERVER.'/admin/eventAsync/subscriptionUsage?uid='.utilityManager::encryptIt($instance->csu_id,true);
        }

        return $instance;
    }

    public function setSubscription(levelData10Object $subscription){
        $this->subscription = $subscription;
    }

    public function getSubscription(){
        return $this->subscription;
    }

    public function _isValid(){
        return isset($this->csu_id) && $this->csu_id > 0;
    }
}
