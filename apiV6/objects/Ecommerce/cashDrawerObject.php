<?php 

class cashDrawerObject extends bobileObject { 

    public $cd_id = "0";
    public $cd_biz_id = "0";
    public $cd_device_id;
    public $cd_opener_account_id = "0";
    public $cd_closer_account_id = "0";
    public $cd_start = "";
    public $cd_start_unix = 0;
    public $cd_end;
    public $cd_end_unix = 0;
    public $cd_start_cash = "0";
    public $cd_cash_sales_total = "0";
    public $cd_refunds_total = "0";
    public $cd_non_sale_total = "0";
    public $cd_expected_total = "0";
    public $cd_actual_total = "0";
    public $cd_notes;
    public $cd_balance = "0";
    public $cd_close_status = "open";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cd_id"])){
            throw new Exception("cashDrawerObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cd_id = $data["cd_id"];
        $instance->cd_biz_id = $data["cd_biz_id"];
        $instance->cd_device_id = $data["cd_device_id"];
        $instance->cd_opener_account_id = $data["cd_opener_account_id"];
        $instance->cd_closer_account_id = $data["cd_closer_account_id"];
        $instance->cd_start = $data["cd_start"];
        $instance->cd_start_unix = strtotime($data["cd_start"]);
        $instance->cd_end = $data["cd_end"];
        $instance->cd_end_unix = isset($data["cd_end"]) ? strtotime($data["cd_end"]) : 0;
        $instance->cd_start_cash = $data["cd_start_cash"];
        $instance->cd_cash_sales_total = $data["cd_cash_sales_total"];
        $instance->cd_refunds_total = $data["cd_refunds_total"];
        $instance->cd_non_sale_total = $data["cd_non_sale_total"];
        $instance->cd_expected_total = $data["cd_expected_total"];
        $instance->cd_actual_total = $data["cd_actual_total"];
        $instance->cd_notes = $data["cd_notes"];
        $instance->cd_balance = $data["cd_expected_total"] - $data["cd_actual_total"];

        if($data["cd_end"] == ""){
            $instance->cd_close_status = "open";
        }else if($instance->cd_balance > 0){
            $instance->cd_close_status = "extra";
        }else if($instance->cd_balance < 0){
            $instance->cd_close_status = "missing";
        }else if($instance->cd_balance == 0){
            $instance->cd_close_status = "balanced";
        }

        return $instance;
    }

    public function closeDrawer(){

        $this->cd_balance = $this->cd_expected_total - $this->cd_actual_total;

        if($this->cd_balance > 0){
            $this->cd_close_status = "extra";
        }else if($this->cd_balance < 0){
            $this->cd_close_status = "missing";
        }else if($this->cd_balance == 0){
            $this->cd_close_status = "balanced";
        }
    }

    public function AdminAPIArray(){
        $result = array();

        $result = (array) $this;

        $result['cd_opener_account_id'] = utilityManager::encodeToHASH($this->cd_opener_account_id);
        $result['cd_closer_account_id'] = utilityManager::encodeToHASH($this->cd_closer_account_id);

        $acountManager = new accountManager();
        $openerAccount = $acountManager->getAccountById($this->cd_opener_account_id);
        $result['cd_opener_name'] = $openerAccount->data->ac_name;

        $result['cd_closer_name'] = "";
        if($this->cd_end_unix > 0 && $this->cd_closer_account_id > 0){
            $closerAccount = $acountManager->getAccountById($this->cd_closer_account_id);
            $result['cd_closer_name'] = $closerAccount->data->ac_name;
        }

        return $result;
    }

    function _isValid(){
        return true;
    }
}
?>


