<?php

/**
 * categoryObject short summary.
 *
 * categoryObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class categoryObject extends levelData999Object
{

    /* Products list */
    public $productIDs = array();
    public $categoryModID = 9; //the module the category belongs to

    public static function withData($data){
        $instance = new self();

        $instance->fillLevelData($data);

        return $instance;
    }

    public function setProductIDs($productsList){
        if(!is_array($productsList)){
            throw new Exception('Products list is not an array');
        }

        $this->productIDs = $productsList;
    }

    public function AdminAPIArray(){
        $data = array();

        $data['id'] = $this->md_row_id;
        $data['modID'] = $this->categoryModID;
        $data['label'] = $this->md_head;
        $data['items'] = $this->productIDs;

        return $data;
    }
}
