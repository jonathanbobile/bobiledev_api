<?php 

class customerInvoiceObject extends bobileObject { 

    public $cin_id = "0";
    public $cin_long_id;
    public $cin_biz_id = "0";
    public $cin_cust_id = "0";
    public $cin_unique_id;
    public $cin_is_credit_note = "0";
    public $cin_stripe_invoice_id;
    public $cin_stripe_payintent_id;
    public $cin_stripe_charge_id;
    public $cin_create_date = null;
    public $cin_due_date = null;
    public $cin_sub_total = "0";
    public $cin_tax = "0";
    public $cin_total = "0";
    public $cin_currency;
    public $cin_status = "draft";//enum('paid','pending','outstanding','refunded','voided','draft','closed')
    public $cin_finalized = "0";
    public $cin_stripe_billing_type = "charge_automatically";//enum('','charge_automatically','send_invoice')

    /* Connected items */

    public $invoice_orders = array();
    public $invoice_transactions = array();
    
    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cin_id"])){
              throw new Exception("customerInvoiceObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cin_id = $data["cin_id"];
        $instance->cin_long_id = $data["cin_long_id"];
        $instance->cin_biz_id = $data["cin_biz_id"];
        $instance->cin_cust_id = $data["cin_cust_id"];
        $instance->cin_unique_id = $data["cin_unique_id"];
        $instance->cin_is_credit_note = $data["cin_is_credit_note"];
        $instance->cin_stripe_invoice_id = $data["cin_stripe_invoice_id"];
        $instance->cin_stripe_payintent_id = $data["cin_stripe_payintent_id"];
        $instance->cin_stripe_charge_id = $data["cin_stripe_charge_id"];
        $instance->cin_create_date = $data["cin_create_date"];
        $instance->cin_due_date = $data["cin_due_date"];
        $instance->cin_sub_total = $data["cin_sub_total"];
        $instance->cin_tax = $data["cin_tax"];
        $instance->cin_total = $data["cin_total"];
        $instance->cin_currency = $data["cin_currency"];
        $instance->cin_status = $data["cin_status"];
        $instance->cin_finalized = $data["cin_finalized"];
        $instance->cin_stripe_billing_type = $data["cin_stripe_billing_type"];

        $instance->fillTransactionsForInvoice();

        return $instance;
    }

    public function AdminAPIArray(){
        $result = array();

        $result["cin_id"] = $this->cin_id;
        $result["cin_long_id"] = $this->cin_long_id;
        $result["cin_biz_id"] = $this->cin_biz_id;
        $result["cin_cust_id"] = $this->cin_cust_id;
        $result["cin_unique_id"] = $this->cin_unique_id;
        $result["cin_stripe_invoice_id"] = $this->cin_stripe_invoice_id;
        $result["cin_stripe_payintent_id"] = $this->cin_stripe_payintent_id;
        $result["cin_stripe_charge_id"] = $this->cin_stripe_charge_id;
        $result["cin_create_date"] = $this->cin_create_date;
        $result["cin_create_date_unix"] = time($this->cin_create_date);
        $result["cin_due_date"] = $this->cin_due_date;
        $result["cin_due_date_unix"] = time($this->cin_due_date);
        $result["cin_sub_total"] = $this->cin_sub_total;
        $result["cin_tax"] = $this->cin_tax;
        $result["cin_total"] = $this->cin_total;
        $result["cin_currency"] = $this->cin_currency;
        $result["cin_status"] = $this->cin_status;
        $result["cin_finalized"] = $this->cin_finalized;
        $result["cin_finalized_unix"] = time($this->cin_finalized);
        $result["cin_stripe_billing_type"] = $this->cin_stripe_billing_type;

        $this->fillTransactionsForInvoice();

        $result['transactions'] = $this->invoice_transactions;

        $result['cin_payment_method'] = "";

        if(count($this->invoice_transactions) > 0){
            $result['cin_payment_method'] = $this->invoice_transactions[0]->tr_paymentMethodType;
        }
        $result["cin_last_attempt"] = 0;
        $customerBilling = new customerBillingManager();
        $result['history'] = $customerBilling->getCustInvoiceHistory($this->cin_id);

        if($this->cin_finalized == 1){
            $finalizedHistoryEntry = $customerBilling->getCustInvoiceFinalizedHistoryEntry($this->cin_id);
            $result["cin_last_attempt"] = strtotime($finalizedHistoryEntry['cinh_time']);
        }

        $result["orders"] = array();

        $connectedOrders = $customerBilling->getConnectedOrdersForCustInvoice($this->cin_id);

        foreach ($connectedOrders as $orderRow)
        {
        	$order = customerOrderObject::withData($orderRow);
            $result["orders"][] = $order->AdminAPIArray();
        }
        

        return $result;
    }

    function _isValid(){
          return true;
    }

    public function setInvoiceOrders($orders){
        if(!is_array($orders)){
            throw new Exception('Orders not an array');
        }

        $this->invoice_orders = $orders;
    }

    public function fillTransactionsForInvoice(){
        $this->invoice_transactions = customerBillingManager::getTransactionsByInvoiceId($this->cin_id);
    }

    public function setInvoiceTransactions($transactions){
        if(!is_array($transactions)){
            throw new Exception('Transactions not an array');
        }

        $this->invoice_transactions = $transactions;
    }
}
?>


