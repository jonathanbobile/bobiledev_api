<?php

/**
 * defaultPaySrcObject short summary.
 *
 * defaultPaySrcObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class defaultPaySrcObject extends bobileObject
{
    public $vendor;
    public $type;
    public $last4;
    public $stripe_card_id;
    public $card_inner_id;
    public $is_default;

    function _isValid(){
        return $this->vendor != '';
    }

    public static function withData($data){
        if(!isset($data["cps_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        $instance->vendor = $data['cps_card_brand'];
        $instance->type = $data['cps_type'];
        $instance->last4 =  $data['cps_last4'];            
        $instance->stripe_card_id = $data['cps_stipe_object_id'];
        $instance->card_inner_id = $data['cps_id'];
        $instance->is_default = $data['cps_default'];

        return $instance;
    }
}