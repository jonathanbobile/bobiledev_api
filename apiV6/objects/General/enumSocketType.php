<?php

/**
 * enumSocketType - the types of messages that the socket sends to the clients.
 * 
 *
 * @version 1.0
 * @author JonathanM
 */
class enumSocketType extends Enum{

    const point = 'point';
   
    const scratch = 'scratch';

    const scratch_empty = 'scratch_empty';

    const points_from_friend = 'points_from_friend';

    const membership = 'membership';

    const server_request = 'server_request';

    const chat_message = 'chat_message';

    const client_chat_message = 'client_chat_message';
    
}

    

