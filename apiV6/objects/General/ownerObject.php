<?php

/**
 * ownerObject short summary.
 *
 * ownerObject description.
 *
 * @version 1.0
 * @author Dany
 */
class ownerObject extends bobileObject
{
    public $owner_id = "0";
    public $owner_ip;
    public $owner_name;
    public $owner_username;
    public $owner_password;
    public $owner_system_lang = "english";
    public $owner_short_lang = "en";
    public $owner_conf_code;
    public $owner_conf_mail = "0";
    public $owner_last_app_id = "0";
    public $owner_bad_email = "0";
    public $owner_hide_hint = "0";
    public $owner_unsub = "0";
    public $owner_aff_id = "0";
    public $owner_aff_group = "0";
    public $owner_start_date = "";
    public $owner_zooz_merchant_id;
    public $owner_facebook_id;
    public $owner_facebook_token;
    public $owner_reseller_id = "0";
    public $owner_visitor_id = "0";
    public $owner_country = "0";
    public $owner_device_id;
    public $owner_device_type;//enum('','Android','IOS')
    public $owner_isreal = "0";
    public $owner_account_id = "0";
    public $owner_currency_code = "USD";
    public $owner_currency_symbol = "$";
    public $owner_environment = "DEV";//enum('PROD','DEV','','')
    public $owner_stripe_cust_id;
    public $owner_gclid;

    function __construct(){
        
    }

    public static function withData($data){

        if (!isset($data["owner_id"])){
            throw new Exception("ownerObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->owner_id = $data["owner_id"];
        $instance->owner_ip = $data["owner_ip"];
        $instance->owner_name = $data["owner_name"];
        $instance->owner_username = $data["owner_username"];
        $instance->owner_password = $data["owner_password"];
        $instance->owner_system_lang = $data["owner_system_lang"];
        $instance->owner_short_lang = $data["owner_short_lang"];
        $instance->owner_conf_code = $data["owner_conf_code"];
        $instance->owner_conf_mail = $data["owner_conf_mail"];
        $instance->owner_last_app_id = $data["owner_last_app_id"];
        $instance->owner_bad_email = $data["owner_bad_email"];
        $instance->owner_hide_hint = $data["owner_hide_hint"];
        $instance->owner_unsub = $data["owner_unsub"];
        $instance->owner_aff_id = $data["owner_aff_id"];
        $instance->owner_aff_group = $data["owner_aff_group"];
        $instance->owner_start_date = $data["owner_start_date"];
        $instance->owner_zooz_merchant_id = $data["owner_zooz_merchant_id"];
        $instance->owner_facebook_id = $data["owner_facebook_id"];
        $instance->owner_facebook_token = $data["owner_facebook_token"];
        $instance->owner_reseller_id = $data["owner_reseller_id"];
        $instance->owner_visitor_id = $data["owner_visitor_id"];
        $instance->owner_country = $data["owner_country"];
        $instance->owner_device_id = $data["owner_device_id"];
        $instance->owner_device_type = $data["owner_device_type"];
        $instance->owner_isreal = $data["owner_isreal"];
        $instance->owner_account_id = $data["owner_account_id"];
        $instance->owner_currency_code = $data["owner_currency_code"];
        $instance->owner_currency_symbol = $data["owner_currency_symbol"];
        $instance->owner_environment = $data["owner_environment"];
        $instance->owner_stripe_cust_id = $data["owner_stripe_cust_id"];
        $instance->owner_gclid = $data["owner_gclid"];

        return $instance;
    }

    function _isValid(){
        return $this->biz_id > 0;
    }
}
