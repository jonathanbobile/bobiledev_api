<?php

/**
 * partnerRequestObject short summary.
 *
 * partnerRequestObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class partnerRequestObject
{
    public $authToken;
    public $partnerAccountID = "";
    public $partner;

    public static function withData($data){
        $instance = new self();

        $header = apache_request_headers();
        
        if(isset($header['Authorization'])){
            $instance->authToken = $header['Authorization'];
        }
        else if(isset($header['authorization'])){
            $instance->authToken = $header['authorization'];
        }
        return $instance;
    }

    public function isAuthorizedRequest(){
        
        if(!isset($this->authToken) || $this->authToken == ""){            
            return false;
        }
        
        $tokenPayload = encryptionManager::getJWTPayloadDirect($this->authToken);

        $this->partnerAccountID = $tokenPayload->iss;

        $resellerResult = partnersManager::getPartnerByPublicUniqueID($this->partnerAccountID);

        if($resellerResult->code != 1){
            return false;
        }

        $this->partner = $resellerResult->data;

        $validSignature = partnersManager::validatePartnerJWTToken($this->partnerAccountID,$this->authToken);

        $publicKeyValid = preg_replace( "/\r|\n/", "",$this->partner->reseller_public_rsa_key) == preg_replace( "/\r|\n/", "",$tokenPayload->kid);
        
        return $validSignature && $publicKeyValid;
    }
}