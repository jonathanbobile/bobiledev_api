<?php

class resellerClientsObject extends bobileObject {

    public $reseller_client_id = "0";
    public $reseller_client_owner_id = "0";
    public $reseller_client_username;
    public $reseller_client_password;
    public $reseller_client_lang = "en";
    public $reseller_client_isactive = "0";
    public $reseller_client_account_id = "0";
    public $reseller_client_stripe_cust_id;
    public $reseller_client_country = "0";

    function __construct(){}

    public static function withData($data){

        if (!isset($data["reseller_client_id"])){
            throw new Exception("resellerClientsObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->reseller_client_id = $data["reseller_client_id"];
        $instance->reseller_client_owner_id = $data["reseller_client_owner_id"];
        $instance->reseller_client_username = $data["reseller_client_username"];
        $instance->reseller_client_password = $data["reseller_client_password"];
        $instance->reseller_client_lang = $data["reseller_client_lang"];
        $instance->reseller_client_isactive = $data["reseller_client_isactive"];
        $instance->reseller_client_account_id = $data["reseller_client_account_id"];
        $instance->reseller_client_stripe_cust_id = $data["reseller_client_stripe_cust_id"];
        $instance->reseller_client_country = $data["reseller_client_country"];
      
        return $instance;
    }

    function _isValid(){       
        return true;
    }
}
?>