<?

if($this->controller != "creator" && $this->controller != "checkout" && $this->controller != "credential" && $this->controller != "account" && $this->controller != "offers" && $this->controller != 'wizard' && $this->controller != 'addons'){    
     
     if(isset($_GET["SES14"])){
         appManager::setTrial();
     }
     
     $redirectLocation = appManager::shouldGoToTrial();
     if($redirectLocation == "reseller_client"){
         Header( "HTTP/1.1 301 Moved Permanently" );
         Header( "Location: " . URL . "builder/trial/client-subscription" );
         return;
     }

     if($redirectLocation == "regular_app"){
         $_SESSION['appCreationSource'] = "addApp";
         Header( "HTTP/1.1 301 Moved Permanently" );
         Header( "Location: " . URL . "builder/trial/subscription" );
         return;
     }
     
     if($_SESSION['ac_wiz_status'] != 'finished' && $_SESSION['rid'] == 0 && $_SESSION['ac_type'] == 'account_owner'){
         Header( "HTTP/1.1 301 Moved Permanently" );
         if($_SESSION['ac_wiz_status'] == 'none'){
             Header( "Location: " . URL . "wizard/wizards" );
         }
         if($_SESSION['ac_wiz_status'] == 'first'){
             Header( "Location: " . URL . "wizard/wizard/2" );
         }
     }

}

$lastModJS = utilityManager::dirmodtime('../admin/scripts');
$lastModCSS = utilityManager::dirmodtime('../admin/styles');

$showTripWire = false;
if(isset($_SESSION['appData'])){
    $showTripWire = appManager::isJ5Country($_SESSION["appData"]["biz_addr_country_id"]) &&  $this->controller == "creator" && $this->action == "preview" && appManager::getAppsCount($_SESSION['appData']['biz_owner_id']) == 1;
}

$detect = new Mobile_Detect;
?>

<html>
  <head>
    <?if($detect->isMobile() && !$detect->isTablet()){?>
    <meta name="viewport" content="width=1024">
    <?}else{?>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <?}?>
    <title><?=$_SESSION["wl_brand_name"]?> WorkSpace</title>
      <? 
      if($_SESSION["wl_domain"] == "bobile.com")
      {
      ?>
      <script>
          if (window.location.protocol == "http:") {
              window.location.href = window.location.href.replace("http", "https");
          }
    </script>
      <? 
      }
      ?>
    <meta charset="utf-8">
      <!--test-now-here-->
    <!--<meta name="viewport" content="width=device-width; initial-scale=1; minimal-ui">-->
    <meta name="keywords" content="<?=$_SESSION["wl_brand_name"]?>, create, app, business, tools, native, native app, free app builder, app builder, friendly app builder, no coding, app template, apps for business, mobile schedule, mobile chat, mobile ecommerce, mobile shop, mobile payments, create app, app creator" />
    <meta name="author" content="<?=$_SESSION["wl_brand_name"]?>" />
    <meta http-equiv="content-type" content="text/html;charset=UTF-8">
    
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 21 Jan 2017 0:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <meta name="author" content="<?=$_SESSION["wl_brand_name"]?>">
    
    <link rel="canonical" href="http://<?=$_SESSION["wl_domain"]?>/" />
      <?if(!utilityManager::inBobileDomain()){?>
      <link rel="icon"  href="<?= $_SESSION["wl_favicon"] != "" ? $_SESSION["wl_favicon"] : '/admin/img/favicon/favicon_inner.ico'?>"/>
      <?}?>

       <!-- Google Tag Manager -->
        <script>(function (w, d, s, l, i) {
            w[l] = w[l] || []; w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            }); var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WF5BKBN');</script>
        <!-- End Google Tag Manager -->

    <link rel="stylesheet" type="text/css" href="/admin/scripts/fancybox/jquery.fancybox-1.3.4.css" />
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.tagsinput.css" />

    <!-- Bootstrap -->
    <link href="/admin/styles/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/admin/styles/bootstrap/animate.css" rel="stylesheet">


    <link href="/admin/styles/ajaxloader/ajaxloader.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="/admin/styles/global.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/topMenu.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/header.css?v=<?= $lastModCSS?>">
      <link rel="stylesheet" type="text/css" href="/admin/styles/dropzone.css?v=<?= $lastModCSS?>">

      <link rel="stylesheet" type="text/css" href="/admin/styles/hirepro.css?v=<?= $lastModCSS?>">
    
    <link rel="stylesheet" type="text/css" href="/admin/styles/sideMenu.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/sidePreview.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/account.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/rangeslider.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/scripts/jquery.qtip.custom/jquery.qtip.min.css">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.paptap.itemSlider.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.paptap.modal.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.paptap.confirmField.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.paptap.popover.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.bobile.slideCanvas.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.bobile.popup.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.bobile.pane.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jplayer.skin.css">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.bobile.video.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/jquery.paptap.switch.css?v=<?= $lastModCSS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/select2/css/select2.css">
    <link rel="stylesheet" type="text/css" href="/admin/scripts/colorpicker/css/colorpicker.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/scripts/clockpicker/src/clockpicker.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="/admin/scripts/colorpicker/css/layout.css" />
    <link rel="stylesheet" type="text/css" href="/admin/scripts/Jcrop/css/jquery.Jcrop.css" />
    <link rel="stylesheet" type="text/css" href="/admin/scripts/scrollbar/jquery.custom-scrollbar.css" />
    <link rel="stylesheet" type="text/css" href="/admin/scripts/intl-tel-input-master/build/css/intlTelInput.css" />
    <link rel="stylesheet" type="text/css" href="/admin/styles/cssgram/cssgram.min.css" />
       
    <?if($this->controller == 'push'){?>
    <link rel="stylesheet" type="text/css" href="/admin/styles/greeting_fonts.css" />
    <!--<link rel="stylesheet" type="text/css" href="https://storage.googleapis.com/bobile/greetingsCSS/greeting_fonts.css" />-->
    <?}?>
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="/admin/styles/explorer.css">
<![endif]-->


    <?php 
    
    $system_lang = (isset($_SESSION['system_lang'])) ? $_SESSION['system_lang'] : "en"; 
    
    foreach ($this->styles as $style) { ?>
    <link rel="stylesheet" type="text/css" href="<?php echo $style;?>?v=<?= $lastModCSS?>" />
    <?php } ?>
    <script src="https://www.gstatic.com/firebasejs/5.5.3/firebase.js"></script>
    <script>
        // Initialize Firebase
        // TODO: Replace with your project's customized code snippet
        var config = {
            apiKey: '<?= FIREBASE_FIRESTORE_APIKEY?>',
            authDomain: '<?= FIREBASE_FIRESTORE_AUTHDOMAIN?>',
            projectId: '<?= FIREBASE_FIRESTORE_PROJECTID?>'
        };
        firebase.initializeApp(config);

        const firestore = firebase.firestore();
        const settings = {timestampsInSnapshots: true};
        firestore.settings(settings);

        const db = firebase.firestore();
        <?if(isset($_SESSION['appData'])){?>
        var bizDoc = db.collection('db').doc('<?=$_SESSION['appData']['biz_id']?>');
        var bizPhotos = bizDoc.collection('photos');
        var bizPhotoGroups = bizDoc.collection('photo_groups');
       
        <?}?>
    </script>
    
    <script>
        var URL = '<?php echo URL; ?>'; 
        var ROOT_URL = '<?php echo ROOT_URL; ?>';
      
    </script>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
    <script src="//s.picdn.net/editor/image/assets/integration.js"></script>
    <!-- Strip JS V3 -->
    <script src="https://js.stripe.com/v3/"></script> 
    <? if (utilityManager::contains($_SERVER['HTTP_HOST'],"sandbox.") || utilityManager::contains($_SERVER['HTTP_HOST'],"dev.")){?>
        <script>var stripe = Stripe('pk_test_ZY1E5gmgGdtuLrQUoPFre6Yt'); var elements = stripe.elements();</script>
    <?}else{?>
        <script>var stripe = Stripe('pk_live_dM2aPr8qyKgNlqWn5QctYjyJ'); var elements = stripe.elements();</script>
    <?}?>

    <script src="/admin/scripts/side_menu.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/top_menu.js?v=<?= $lastModJS?>"></script>
   <script src="/admin/scripts/dropzone.min.js?v=<?= $lastModJS?>"></script>

    <script src="/admin/scripts/fileuploader.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/fileupload/vendor/jquery.ui.widget.js"></script>
    <script src="/admin/scripts/fileupload/jquery.iframe-transport.js"></script>
    <script src="/admin/scripts/fileupload/jquery.fileupload.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/bootstrap/modernizr.custom.js"></script>
    <script src="/admin/scripts/jquery.qtip.custom/jquery.qtip.min.js"></script>
    <script src="/admin/scripts/bootstrap/waypoints.min.js"></script>
    <script src="/admin/scripts/bootstrap/bootstrap.min.js"></script>
    
    <script src="/admin/scripts/ajaxloader/jquery.ajaxloader.1.4.js"></script>
    <script src="/admin/scripts/particles.js"></script>
    <script src="/admin/scripts/jquery.paptap.itemSlider.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.modal.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.popover.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.bobile.pane.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.bobile.video.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.bobile.popup.js?v=<?= $lastModJS?>"></script>
    <script type="text/javascript" src="https://dme0ih8comzn4.cloudfront.net/imaging/v3/editor.js"></script>
    <script src="/admin/scripts/jquery.bobile.imageeditor.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.bobile.slideCanvas.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.jplayer.js"></script>
    <script src="/admin/scripts/jquery.paptap.confirmField.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.infiniteScroll.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.frameSlider.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.switch.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.bobile.select.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.bobile.bbSocket.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.pusher.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/sidePreview.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/phoneBox.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.stickyElements.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.paptap.animations.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/jquery.tagsinput.js"></script>
    <!--<script src="/admin/scripts/tiny_mce3.5.6/tiny_mce.js"></script>-->
    <script src="/admin/scripts/tinymce/js/tinymce/tinymce.min.js"></script>
    <script src="/admin/scripts/select2/dist/js/select2.full.js"></script>
    <script src="/admin/scripts/jquery.paptap.confirmField.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    <script src="/admin/scripts/localize-datepicker/datepicker-de.js"></script>
    <script src="/admin/scripts/intl-tel-input-master/build/js/intlTelInput.js"></script>
    <script src="/admin/scripts/localize-datepicker/datepicker-he.js"></script>
    <script src="/admin/scripts/localize-datepicker/datepicker-es.js"></script>
    <script src="/admin/scripts/localize-datepicker/datepicker-hu.js"></script>
    <script src="/admin/scripts/localize-datepicker/datepicker-pb.js"></script>
    <script src="/admin/scripts/colorpicker/js/colorpicker.js" type="text/javascript"></script>
    <script src="/admin/scripts/clockpicker/src/clockpicker.js" type="text/javascript"></script>
    <script src="/admin/scripts/jQuery.print.js" type="text/javascript"></script>
    <script src="/admin/scripts/FB.js" type="text/javascript"></script>
    <script src="/admin/scripts/scrollbar/jquery.custom-scrollbar.js"></script>
     <script src="/admin/scripts/rangeSlider/rangeslider.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="/admin/scripts/CamanJS-4.1.1/dist/caman.full.js"></script>
    <script src="/admin/scripts/Jcrop/js/jquery.Jcrop.min.js"></script>
    <script src="/admin/scripts/filterous2/filterous-2-master/demo-browser/filterous2.js"></script>
    <script src="/admin/scripts/bbFireDB.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/photo_manager.js?v=<?= $lastModJS?>"></script>
    <script src="/admin/scripts/global.js?v=<?= $lastModJS?>"></script>
    <?php foreach ($this->scripts as $script) { ?>
    <script src="<?php echo $script;?>?v=<?= $lastModJS?>"></script>
    <?php } ?>

    <?
        $lang_js = isset($_SESSION['system_lang']) ? $_SESSION['system_lang'] : 'en';
        $rand = rand(1,1000000);
        ?>
    <script src="https://storage.googleapis.com/bobile/languages/<?= $lang_js?>/jsLanguage.js?ran=<?=$rand?>"></script>
      <script>
          
          $.datepicker.setDefaults($.datepicker.regional['<?=$lang_js?>']);

          <?if(isset($_SESSION['appData'])){?>
          var biz_data = {
              shortname: '<?= $_SESSION['appData']['biz_short_name']?>'
          };
          <?}?>
      </script>

    

    <!-- <script src="/language/<?php echo $system_lang;?>/jsLanguage.js"></script> -->

    
    <link rel="stylesheet" type="text/css" href="/admin/styles/spinner.css?v=<?= $lastModJS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/responsive.css?v=<?= $lastModJS?>">
    <link rel="stylesheet" type="text/css" href="/admin/styles/whitelabel.php">
  </head>
  <?
    $specialClass = "";
    if($this->controller == "home"){
        if($this->action == 'festivus'){
            $specialClass = 'festivus';
        }
        
        if(isset($this->firstTime) && $this->firstTime == 1){
            $specialClass = 'tour';
        }

    }
        ?>
  <body class="<?= $specialClass?>">

      <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WF5BKBN"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

      <?
      $countryId = isset($_SESSION["appData"]['biz_addr_country_id']) ? $_SESSION["appData"]['biz_addr_country_id'] : "0";
      if($countryId == "0"){
          $countryId = isset($_SESSION['country']) ? $_SESSION['country'] : "1";
      }
      ?>
      <?if($showTripWire){?>
      <div class="trip-wire"></div>
      <div class="hidden-tag fancy-content on-leave">
          <div class="screen-cover">

              
          </div>

      </div>
      <?}?>
      
      <input id="dateFormat" type="hidden" value="<?php echo  $countryId == "1" ? "mm/dd/yy" : "dd/mm/yy";?>" />
      <input id="timeUS" type="hidden" value="<?php echo  $countryId == "1" ? "true" : "false";?>" />
      <input id="calStartDay" type="hidden" value="<?php echo  $countryId == "2" ? "0" : "1";?>" />

      <input id="currencyBiz" type="hidden" value="<?= isset($_SESSION['appData']["owner_currency_symbol"]) ? $_SESSION['appData']["owner_currency_symbol"] : '$' ?>" />
    
      <?if(isset($_REQUEST['opentab'])){?>
      <input type="hidden" id="tabtoopen" value="<?= $_REQUEST['opentab']?>" />
      <?}?>
  <div class="main-all">
      
    <div class="cover-all" style="display:none;"></div>
      <canvas id="canvas"></canvas>
  <?php

  
  $firstTimeComp = '';
  
  if($this->action != "reseller_bundles" && $this->action != "myapps" && $this->action != 'reseller_programs' && $this->controller != "markets" && (!isset($this->isUnbound) || !$this->isUnbound)){
      $iconimg = "";
      $credits = 0;
      $_SESSION['rid'] = isset($_SESSION['rid']) ? $_SESSION['rid'] : 0;
      if($_SESSION['rid'] == 0 && isset($_SESSION["appData"])){
          $iconimg = (substr( $_SESSION["appData"]["biz_icon"], 0, 4 ) === "http") ? utilityManager::getImageThumb($_SESSION["appData"]["biz_icon"]) : "/bizImages/".$_SESSION["appData"]["biz_icon"];
          $credits = $_SESSION['appData']['biz_credits'];
      }
      
      if(isset($this->firstTime) && $this->firstTime == 1){
          $firstTimeComp = 'data-tour="2" data-tooltip="[data-tourstage=2]" data-tooltip-my="top right" data-tooltip-at="bottom left" data-tooltip-offx="500" data-tooltip-stay="1" data-tooltip-style="tourtooltip"';
      }
      require VIEWS_PATH . '_templates/side_menu.php';
  }

  $noleft = false;
  if((isset($this->isUnbound) && $this->isUnbound) || $this->action == "reseller_bundles" || $this->action == 'reseller_programs' || $this->action == "bundles" || $this->action == "myapps" || $this->controller == "markets"){
      $noleft = true;
  }
  ?>
      <div class="header-all <? echo $this->controller == "home"  || $this->action == "preview" || $this->action == "postPreview" || strtolower($this->action) == "mymarkets" || $this->action == "postpreview" ? $this->controller : "" ?> <?= $this->action == 'bundles' ? 'billing' : ''?>">
        <div class="row">
        <?if($this->controller != "creator" && $this->controller != "account" && $this->controller != "wizard" && $this->action != "reseller_bundles" && $this->action != 'reseller_programs' || ($this->controller == "wizard" && $_SESSION['ac_wiz_status'] == 'finished') ){?>
            <div class="col-md-8 text-right <?=isset($this->firstTime) && $this->firstTime == 1 ? 'tour-tip-2' : ''?>" <?=$firstTimeComp?> style="min-width:500px; float:right; " >
                
	        	<div class="account-box paptap-popup" data-targetelem="options-list-orig" data-align="right" data-height="140" data-width="190"  data-leftoffset="20" data-topoffset="5" data-customclass="appoptions">
	          		
                    <img src="/admin/img/header/settings.png" />
	        	</div>

                <?if(isset($_SESSION['oid']) && $_SESSION['oid'] != 0 && $this->controller != 'markets'){?>
	        	<div class="notifi-box paptap-popup pointer <? appManager::hideViewIfBizIsFree()?>" data-callback="notification_action" data-block="true" data-target-url="/admin/account/notifications" data-height="360" data-align="right" data-title="<?= $this->language->get('notifications')?>" data-tip_id="82" data-customclass="notification-modal" data-leftoffset="47" data-topoffset="10">
                    <div class="counter-holder counterHolder hidden-tag"><!--add zero to class when empty-->
                        <span class="newNotifications"></span>
                    </div>
                    <img src="/admin/img/header/notifications.png" />
	        	</div>
                <?}?>
               
                <?if(appManager::isBobileDomain() && isset($_SESSION['oid']) && $_SESSION['oid'] != 0 && $this->controller != 'markets'){?>
                <div class="app-help">
                    <a href="https://bobile.com/helpcenter/" target="_blank">
                    <div>
                        <img class="help-btn" src="/admin/img/header/help.png"/>
                    </div>
                        </a>
                </div>
                <?}?>
                
                
                <div>
                
                    
                    
                    
                </div>
                
               
                <?if($this->controller == 'markets' && strtolower($this->action) != 'mymarkets'){?>
                <div class="market-header">
                    <div class="market-icon">
                        <img src="<?= $_SESSION['marketData']['mrkt_icon']?>" />
                    </div>
                    <div class="market-name">
                        <?= $_SESSION['marketData']['mrkt_name']?>
                    </div>
                </div>
                <?}?>
	      </div>
          <?}?>
            <?if($this->controller == "account" || $this->action == "reseller_bundles" || $this->action == 'reseller_programs'){?>
            <div class="col-md-2 col-md-offset-10 text-right">
                <div class="logout-holder">                    
                    <div class="float-right">
                        <div class="secondary-text">
                            <?= isset($_SESSION["username"]) ? $_SESSION["username"] : $_SESSION["appData"]["owner_username"]?>
                        </div>
                        <div class="option-holder logOut float-left">
                            <span><?=$this->language->get('Log_Out')?></span>
                        </div>
                    </div>
                    <div class="account-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 24">
                        <path fill-rule="evenodd"  fill="rgb(217, 217, 217)"
                         d="M25.000,22.653 C25.000,23.458 24.571,23.959 23.611,23.959 L12.514,23.959 C12.511,23.954 12.509,23.953 12.505,23.948 C12.502,23.953 12.506,23.950 12.500,23.959 C12.494,23.950 12.497,23.953 12.494,23.948 C12.490,23.953 12.489,23.954 12.485,23.959 L1.389,23.959 C0.429,23.959 -0.000,23.458 -0.000,22.653 L-0.001,20.189 C-0.001,19.829 0.224,19.507 0.560,19.379 C4.530,17.878 8.935,15.528 9.304,14.369 L9.304,13.637 C8.481,12.813 7.831,11.663 7.448,10.318 C6.937,9.966 6.601,9.375 6.601,8.705 C6.601,8.228 6.778,7.797 7.060,7.458 L7.060,4.731 C7.060,1.917 8.757,-0.010 12.323,-0.010 C12.386,-0.010 12.438,0.001 12.500,0.002 C12.562,0.001 12.613,-0.010 12.676,-0.010 C16.243,-0.010 17.940,1.917 17.940,4.731 L17.940,7.458 C18.222,7.797 18.399,8.228 18.399,8.705 C18.399,9.375 18.063,9.966 17.552,10.318 C17.168,11.663 16.519,12.813 15.696,13.637 L15.696,14.369 C16.064,15.528 20.470,17.878 24.439,19.379 C24.775,19.507 25.000,19.829 25.000,20.189 L25.000,22.653 ZM12.500,23.959 L12.514,23.959 C12.532,23.981 12.544,24.000 12.500,23.959 ZM12.485,23.959 L12.500,23.959 C12.456,24.000 12.468,23.981 12.485,23.959 Z"/>
                        </svg>
                    </div>
                </div>
            </div>
            <?}?>
        </div>
      </div>
      <?if ($this->controller == "creator" && $this->action != "postPreview"  ){?>
            <div class="second-btn workspace header-btn" data-href="/admin/home/welcome" data-issola="<?=isset($_SESSION["solaID"]) ? $_SESSION["solaID"] : ""?>">
                <span><?=$this->language->get('i_want')?></span>
                <img src="/admin/img/wizard/arrow-white.png">
            </div>
      <?}?>
      <?
        $tSystems = "";
        if($_SESSION["rid"] > 0 || (isset($_SESSION["clientid"]) && $_SESSION["clientid"] == "1")){
            $tSystems = "t-systems";
        }
      ?>
      <div class="logo-holder <?php echo isset($_COOKIE['menu_minimized']) || $noleft ? "maximized" : ""?>">
          <div class="row">
	         <div class="text-left title-menu <?=$tSystems?>" >
                 <?if(!isset($this->isUnbound) || !$this->isUnbound){
                       if($this->action != 'specials'){?>

                            <span><?
                                  echo $_SESSION["wl_brand_name"];
                              ?>
                              </span>
                            <?include('../admin/img/svg/title_svg/'.$this->controller.'.svg')?>
                            <span><?=$this->language->get('title_'.$this->controller)?></span>

	        	
                 <?}else{
                 
                       if($this->action == 'bundles' || $this->action == 'reseller_bundles' || $this->action == 'reseller_programs' ){?>
                     <span><?
                           echo $_SESSION["wl_brand_name"];
                           ?>
                              </span>
                                <?include('../admin/img/svg/title_svg/plans.svg')?>
                                <span><?=$this->language->get('title_plans')?></span>
                     <?}else{?>
                            <span><?
                           echo $_SESSION["wl_brand_name"];
                                  ?>
                              </span>
                            <?include('../admin/img/svg/title_svg/'.$this->controller.'.svg')?>
                            <span><?=$this->language->get('title_'.$this->controller)?></span>
                       <?}?>
                 <?}
                   }
                   else{
                       if($this->action == 'bundles' || $this->action == 'reseller_bundles' || $this->action == 'reseller_programs' ){?>
                         <span><?
                                  echo $_SESSION["wl_brand_name"];
                              ?>
                              </span>
                                    <?include('../admin/img/svg/title_svg/plans.svg')?>
                                    <span><?=$this->language->get('title_plans')?></span>
                         <?}else{?>
                            <span><?
                               echo $_SESSION["wl_brand_name"];
                            ?>
                              </span>
                            <?include('../admin/img/svg/title_svg/'.$this->controller.'.svg')?>
                            <span><?=$this->language->get('title_'.$this->controller)?></span>
                        <?}?>
                 <?}?>
	      	 </div> 	
          </div>
      </div>
     
      <?if($this->controller != 'wizard'){
            if($this->controller != "account" && $this->controller != 'creator' && $this->action != "reseller_bundles" && $this->action != 'reseller_programs' ){?>
     <div class="header-small <? echo $this->controller == "home" || $this->action == "preview" || $this->action == "postPreview" || strtolower($this->action) == "mymarkets" || $this->action == "postpreview" ? $this->controller : "" ?>">
       
         <div class="small-menu-toggle">
             <img class="dots" src="/admin/img/vertical_dots_grey.png" />
             <img class="dots-close" src="/admin/img/x.png" />
         </div>
         <div class="small-menu-holder">
             <?if($this->controller != "creator"){?>
                
	        	<div class="account-box paptap-popup on-left" data-targetelem="options-list-orig" data-align="top" data-height="140" data-width="190"  data-leftoffset="20" data-customclass="appoptions">
	          		
                    <img src="/admin/img/header/settings.png" />
	        	</div>

                <?if(isset($_SESSION['appData']['biz_id']) && isset($_SESSION['oid']) && $_SESSION['oid'] != 0){?>
	        	<div class="notifi-box paptap-popup pointer on-left <? appManager::hideViewIfBizIsFree()?>" data-callback="notification_action" data-block="true" data-target-url="/admin/account/notifications" data-height="360" data-align="top" data-title="<?= $this->language->get('notifications')?>" data-tip_id="82" data-customclass="notification-modal" data-leftoffset="20" data-topoffset="10">
                    <div class="counter-holder counterHolder hidden-tag"><!--add zero to class when empty-->
                        <span class="newNotifications"></span>
                    </div>
                    <img src="/admin/img/header/notifications.png" />
	        	</div>
                <?}?>
                <?if(appManager::isBobileDomain() && isset($_SESSION['oid']) && $_SESSION['oid'] != 0){?>
                <div class="app-help">
                    <a href="https://bobile.com/helpcenter/" target="_blank">
                        <div>
                            <img class="help-btn" src="/admin/img/header/help.png"/>
                        </div>
                    </a>
                </div>
                <?}?>
             <?}?>
         </div>
      </div>
      <?}else if($this->controller == 'creator'  && $this->action != "postPreview"){?>
      <div class="header-small">
          <div class="second-btn workspace header-btn" data-href="/admin/home/welcome" data-issola="<?=isset($_SESSION["solaID"]) ? $_SESSION["solaID"] : ""?>">
              <span><?=$this->language->get('i_want')?></span>
              <img src="/admin/img/wizard/arrow-white.png">
          </div>
      </div>
      <?}else{
                if($this->action != "postPreview"){?>
      <div class="header-small col-md-2 col-md-offset-10 text-right">
            <div class="logout-holder">                    
                <div class="float-right">
                    <div class="secondary-text">
                        <?= isset($_SESSION["username"]) ? $_SESSION["username"] : $_SESSION["appData"]["owner_username"]?>
                    </div>
                    <div class="option-holder logOut float-left">
                        <span><?=$this->language->get('Log_Out')?></span>
                    </div>
                </div>
                <div class="account-icon">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 24">
                    <path fill-rule="evenodd"  fill="rgb(217, 217, 217)"
                        d="M25.000,22.653 C25.000,23.458 24.571,23.959 23.611,23.959 L12.514,23.959 C12.511,23.954 12.509,23.953 12.505,23.948 C12.502,23.953 12.506,23.950 12.500,23.959 C12.494,23.950 12.497,23.953 12.494,23.948 C12.490,23.953 12.489,23.954 12.485,23.959 L1.389,23.959 C0.429,23.959 -0.000,23.458 -0.000,22.653 L-0.001,20.189 C-0.001,19.829 0.224,19.507 0.560,19.379 C4.530,17.878 8.935,15.528 9.304,14.369 L9.304,13.637 C8.481,12.813 7.831,11.663 7.448,10.318 C6.937,9.966 6.601,9.375 6.601,8.705 C6.601,8.228 6.778,7.797 7.060,7.458 L7.060,4.731 C7.060,1.917 8.757,-0.010 12.323,-0.010 C12.386,-0.010 12.438,0.001 12.500,0.002 C12.562,0.001 12.613,-0.010 12.676,-0.010 C16.243,-0.010 17.940,1.917 17.940,4.731 L17.940,7.458 C18.222,7.797 18.399,8.228 18.399,8.705 C18.399,9.375 18.063,9.966 17.552,10.318 C17.168,11.663 16.519,12.813 15.696,13.637 L15.696,14.369 C16.064,15.528 20.470,17.878 24.439,19.379 C24.775,19.507 25.000,19.829 25.000,20.189 L25.000,22.653 ZM12.500,23.959 L12.514,23.959 C12.532,23.981 12.544,24.000 12.500,23.959 ZM12.485,23.959 L12.500,23.959 C12.456,24.000 12.468,23.981 12.485,23.959 Z"/>
                    </svg>
                </div>
            </div>
        </div>
      <?}
            }
        }?>
      <div class="main-right mainRight  <?php echo isset($_COOKIE['menu_minimized']) ? "maximized" : ""?> <? echo $this->controller == "home" ? "home" : "" ?> <?= $noleft ? "no-left-menu" : ""?>">
        
        <div class="info-display-holder infoBannerHolder">
        <?  
            if(isset($_SESSION['reseller_banners'])){
                foreach ($_SESSION['reseller_banners'] as $this->bannerInfo)
                {
                	include('info_banner.php');
                }
                
            }

            if($this->controller != "account" && isset($_SESSION['appData']['biz_info_banners'])){
                foreach ($_SESSION['appData']['biz_info_banners'] as $this->bannerInfo)
                {
                	include('info_banner.php');
                }
                
            }
        ?>
        </div>
        
  		
            
      

  

