<?php
/**
 * This is the "base controller class". All other "real" controllers extend this class.
 * Whenever a controller is created, we also
 * 1. initialize a session
 * 2. check if the user is not logged in anymore (session timeout) but has a cookie
 * 3. create a database connection (that will be passed to all models that need a database connection)
 * 4. create a view object
 */
class Controller
{
    public $debugMode = false;
    public $mobileRequestObject;
    protected $echo_answer;

    function __construct($echo_answer = true)
    { 
        $this->echo_answer = $echo_answer;
        Session::init();

    }

    /**
     * loads the model with the given name.
     * @param $model string name of the model
     */
    public function loadModel($model)
    {
        $path = MODELS_PATH . $model . '.php';

        if (file_exists($path)) {
            require_once MODELS_PATH . $model . '.php';

            return new $model();
        }
        else return 0;
    }

    function returnAnswer(resultObject $answer){
        $output = outputObject::fromResult($answer);
        if($this->echo_answer){
            echo json_encode($output);
            return "";
        }
        else{
            
            return $output;
        }

    }

    function returnAnswerWithCaller(resultObject $answer,$caller = ''){
        $output = outputObject::fromResult($answer);
        $output->caller = $caller;
        if($this->echo_answer){
            echo json_encode($output);
            return "";
        }
        else{
            
            return $output;
        }

    }

}
