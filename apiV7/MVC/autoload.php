<?php

/**
 * the auto-loading function, which will be called every time a file "is missing"
 * NOTE: don't get confused, this is not "__autoload", the now deprecated function
 * The PHP Framework Interoperability Group (@see https://github.com/php-fig/fig-standards) recommends using a
 * standardized auto-loader https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-0.md, so we do:
 */
function autoload($class) {
    // if file does not exist in LIBS_PATH folder [set it in config/config.php]
    if (file_exists(LIBS_PATH . $class . ".php")) {
        require_once(LIBS_PATH . $class . ".php");
    } else if (file_exists(MODELS_PATH . $class . ".php")) {
        require_once(MODELS_PATH . $class . ".php");
    }

    listFolderFiles(OBJ_PATH,$class);

    if (file_exists(MVC_PATH . $class . ".php")) {
        require_once(MVC_PATH . $class . ".php");
    }  
}

function listFolderFiles($dir,$class){
    $ffs = scandir($dir);

    unset($ffs[array_search('.', $ffs, true)]);
    unset($ffs[array_search('..', $ffs, true)]);

    // prevent empty ordered elements
    if (count($ffs) < 1)
        return;

    foreach($ffs as $ff){

        if(is_dir($dir.$ff)){
            listFolderFiles($dir.$ff.'/',$class);
        }else{
            if (file_exists($dir . $class . ".php")) {
                require_once($dir . $class . ".php");
            }
        }
    }
}

// spl_autoload_register defines the function that is called every time a file is missing. as we created this
// function above, every time a file is needed, autoload(THENEEDEDCLASS) is called
spl_autoload_register("autoload");


// https://api.bobile.com/api/biz/get_active/?
// https://api.bobile.com/api/customer/
// https://api.bobile.com/api/admin/
// https://api.bobile.com/api/market/

