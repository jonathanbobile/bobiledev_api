<?php

class Error extends Controller
{
    function __construct()
    {
        parent::__construct(); 
    }
    
    function index()
    {
        echo json_encode(resultObject::withData(0,"Wrong URL"));
        //$this->view = new View();
        //header("HTTP/1.0 404 Not Found");
        //$this->view->render('error/404', 0, 0);
    }
    
    function errorLog(){
        $errorManager = new errorManager();
        $errorManager->seveErrorLog($_POST);
    }
    
    function errorPage($message)
    {
        $this->view = new View();
        $this->view->message = utilityManager::decryptIt($message,true);
        $this->view->render('_templates/error',true);
    }
}
