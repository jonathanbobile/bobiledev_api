<?php

class Partner extends Controller{

    public $partnerModel;
    public $partnerRequest;

    public function __construct(){

        header("access-control-allow-origin: https://affiliate.bobile.com");

        parent::__construct();
        set_time_limit(0);

        $this->partnerModel = new partnerModel();
        $this->partnerRequest = partnerRequestObject::withData($_REQUEST);

    }
    
    //************************* A1 ***************************//
    public function a1WebHook(){
        
        $input = @file_get_contents('php://input');
        $event_json = json_decode($input);
        
        $fh = fopen("debug.txt", 'a');
        fwrite($fh, print_r($event_json,true)."\n");
        fwrite($fh, print_r($_REQUEST,true)."\n");
        fclose($fh);

        $apiConfig = $this->partnerModel->getA1ApiConfig();

        if($apiConfig["code"] == 1){
            $requests = new a1ProductRequests($apiConfig["data"]);
            $requests->templteId = "TL-028-148-034";
            $requests->process();
        }else{
            print_r($apiConfig);
        }
    }

    public function a1_manager(){
        
        $jwtKey = $_GET["jwt"];
        
        $responseAssetId = $this->partnerModel->getAssetId($jwtKey);
        if($responseAssetId->code == 0){

            Header( "HTTP/1.1 301 Moved Permanently" ); 
            Header( "Location: ".URL."/error/errorPage/" . utilityManager::encryptIt($responseAssetId->message,true));

        }else{

            $responseLicense = $this->partnerModel->getA1LicenseByAsset($responseAssetId->data);

            if($responseLicense->code == 1){

                $licenseObject = $responseLicense->data;
                $a1ApiKey = "G6qSF3eSoIjGbjCzTpzQt2d2RJXmCNHqw0uIECkvNb4=";

                $responseToken = $this->partnerModel->getClientTokenByEmail($a1ApiKey,$licenseObject->a1_customer_email);

                if($responseToken->code == 1){
                    Header( "HTTP/1.1 301 Moved Permanently" ); 
                    Header( "Location: https://workspace.bobile.at/admin/builder/clientLanguagRedirect/" . $responseToken->data . "/de");
                }else{
                    Header( "HTTP/1.1 301 Moved Permanently" ); 
                    Header( "Location: ".URL."/error/errorPage/" . utilityManager::encryptIt($responseToken->message,true));
                }

            }else{
                Header( "HTTP/1.1 301 Moved Permanently" ); 
                Header( "Location: ".URL."/error/errorPage/" . utilityManager::encryptIt("Wrong asset ID",true));
            }
        }
    }

    //************************* BOBILE API's ***************************//

    /**
     * Return generated clientToken from resellers API key and E-mail
     * to use for create/login to SSO
     */
    public function getClientToken(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        
        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $apiKey = $this->partnerRequest->partner->reseller_wl_apikey;
        
        $response = $this->partnerModel->getClientTokenByEmail($apiKey,$data["email"]);
        echo json_encode($response);
    }

    /**
     * Return a list of all apps that belongs to partner
     */
    public function getPartnerBusinesses(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        
        $apiKey = $this->partnerRequest->partner->reseller_wl_apikey;

        $response = $this->partnerModel->getPartnerApps($apiKey);
        echo json_encode($response);
    }

    public function getClientBusinesses(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $apiKey = $this->partnerRequest->partner->reseller_wl_apikey;
        $resellerId = $this->partnerRequest->partner->reseller_id;
       
        $data["reseller_id"] = $resellerId;
        $this->partnerModel->preventNotAuthorizedClientToken($data,$apiKey);

        $response = $this->partnerModel->getClientsApps($apiKey,$data["clientToken"]);
        echo json_encode($response);
    }

    public function deactivatePartnerBusiness(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        //$apiKey = $this->partnerRequest->partner->reseller_wl_apikey;
        $resellerId = $this->partnerRequest->partner->reseller_id;

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $data["reseller_id"] = $resellerId;
        $this->partnerModel->preventNotAuthorizedAppToken($data);
       
        $response =  $this->partnerModel->deactivatePartnerApp($data["businessToken"]);
        echo json_encode($response);
    }

    public function activatePartnerBusiness(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        
        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $this->partnerModel->preventNotAuthorizedAppToken($data);

        $response =  $this->partnerModel->activatePartnerApp($data["businessToken"]);
        echo json_encode($response);
    }

    public function getBusinessDetails(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $apiKey = $this->partnerRequest->partner->reseller_wl_apikey;
        $this->partnerModel->preventNotAuthorizedAppToken($data);

        $response =  $this->partnerModel->getAppDetails($apiKey,$data["businessToken"]);
        echo json_encode($response);
    }

    public function getCustomersList(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $apiKey = $this->partnerRequest->partner->reseller_wl_apikey;
        $this->partnerModel->preventNotAuthorizedAppToken($data);

        $type = isset($data['type']) && $data['type'] != "" ? $data['type'] : "";

        $response =  $this->partnerModel->getCustomersList($apiKey,$data["businessToken"],$type);
        echo json_encode($response);
    }

    public function getCustomer(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $apiKey = $this->partnerRequest->partner->reseller_wl_apikey;
        $this->partnerModel->preventNotAuthorizedCustomerToken($data);

        $response =  $this->partnerModel->getCustomer($apiKey,$data["customerToken"]);
        echo json_encode($response);
    }

    public function addCustomer(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $this->partnerModel->preventNotAuthorizedAppToken($data);

        $response =  $this->partnerModel->addCustomer($data);
        echo json_encode($response);
    }
    
    public function updateCustomer(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $this->partnerModel->preventNotAuthorizedCustomerToken($data);

        $response =  $this->partnerModel->updateCustomer($data);
        echo json_encode($response);
    }

    public function addDataElementWithDelete(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
       

        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;
        
        $this->partnerModel->preventNotAuthorizedAppToken($data);

        if(count($data["data"]) > 0){

            $biz_id = utilityManager::decodeFromHASH($data['appToken']); 

            $levelDataManager = new levelDataManager($data["module_id"]);
            $levelDataManager->deleteLevelDataElements($biz_id,$data["level_number"],$data["parent_id"]);

            foreach ($data["data"] as $element)
            {      
                $element["appToken"] = $data["appToken"];
                $element["module_id"] = $data["module_id"];
                $element["parent_id"] = $data["parent_id"];
                $element["level_number"] = $data["level_number"];
               
                $this->partnerModel->checkLevelDataInsertRequest($element);
                
            	$responseAddElement =  $this->partnerModel->addDataElement($element);
                if($responseAddElement->code != 1){
                    echo json_encode($responseAddElement);
                    exit;
                }
            }            
        }

        echo json_encode(resultObject::withData(1,"Ok"));
    }

    public function addProduct(){
        if(!$this->partnerRequest->isAuthorizedRequest()){
            $response = resultObject::withData(0,"Not authorized","UNAUTHORIZED_API_KEY");
            echo json_encode($response);
            exit;
        }
        
        $input = @file_get_contents('php://input');
        $event_json = json_decode($input,true);
        $data = is_array($event_json) ? $event_json : $_REQUEST;

        $bizId = $this->partnerModel->preventNotAuthorizedAppToken($data);

        $response =  shopManager::addProducts($bizId,$data["products"]);
        echo json_encode($response);
    }

    public function getJWT($rid){

        $partner = partnersManager::getPartnerByPublicUniqueID($rid)->data;
        
        $payload = array();

        

        $payload['iss'] = $rid;
        $payload['kid'] = $partner->reseller_public_rsa_key;
        $payload['iat'] = time();

        $bucket = RESELLER_KEYS_BUCKET;

        $secret = file_get_contents("https://storage.googleapis.com/$bucket/resellers/keys/$rid.pem");
        
       

        echo encryptionManager::createJWTToken($payload,$secret);
    }
}
