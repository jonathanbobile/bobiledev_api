<?php

/**
 * enumOrderStatus - the order's statuses
 * 
 *
 * @version 1.0
 * @author JonathanM
 */
class enumOrderItemSource extends Enum{

    const source_coupon = "coupon";
   
    const source_product = "product";

    const source_meeting = "meeting";

    const source_appointment = "appointment";

    const source_class = "class";

    const source_subscription = "subscription";

    const source_punch_pass = "punch_pass";

    const source_workshop = "workshop";

    const source_event = "event";

    const source_giftcard = "giftcard"; 
    
    const source_other = "other"; 
    
}

    

