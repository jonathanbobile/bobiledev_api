<?php

/**
 * enumOrderStatus - the order's statuses
 * 
 *
 * @version 1.0
 * @author JonathanM
 */
class enumOrderStatus extends Enum{

    const received = 1;
   
    const in_process = 2;

    const packaging = 3;

    const shipped = 4;

    const delivered = 5;

    const NA = 6;

    const on_hold = 7;

    const cancelled = 8;
    
    const undelivered = 9;
}

    

