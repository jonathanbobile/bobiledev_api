<?php
class generalManager{

    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }
        $this->bizID = isset($_SESSION['appData']['biz_id']) ? $_SESSION['appData']['biz_id'] : 0;

        try {
            $this->apiDB = new Database('api');
        }
        catch (PDOException $e) {
            die('API Database connection could not be established.');
        }
    }

    public function getCountries(){
        return $this->db->getTable("SELECT * FROM tbl_countries WHERE country_id NOT IN (244,0) ORDER BY country_name_eng");
    }

    public function getState($country = 1){
        return $this->db->getTable("SELECT * FROM tbl_states WHERE state_country_id=$country ORDER BY state_name_eng");
    }

    public function getTimeZones(){
        return $this->db->getTable("SELECT * FROM tbl_time_zone");
    }

    public function getGenders(){
        return $this->db->getTable("select * from tbl_gender");
    }

    public static function getGender($genderID){
        $instance = new self();
        return $instance->db->getVal("select gen_name from tbl_gender WHERE gen_id = $genderID");
    }

    public static function needUpdateApp($bizID = 0)
    {
        $bizID = ($bizID == 0 && isset($_SESSION["appData"])) ? $_SESSION["appData"]["biz_id"] : $bizID;
        if($bizID != 0){
            $instance = new self();
            $instance->db->execute("update tbl_users_biz set usrbiz_need_update=1 where usrbiz_biz_id=$bizID");
        }
    }

    public function reCalcRank()
    {
        $apps = $this->db->getTable("SELECT biz_id FROM tbl_biz");
        if (count($apps) > 0){
            foreach ($apps as $app) {

                $appId = $app["biz_id"];

                //Calculation of the Rank is based on 3 factors:
                //1. Content of the app
                //2. Usage of the platform
                //3. App Users Engagement

                $contentRank = 0;
                $usageRank = 0;
                $engagementRank = 0;


                /* Content Rank */
                $hasKeyFeature = $this->db->getVal("SELECT COUNT(biz_mod_id) has FROM tbl_biz_mod
                                                    WHERE biz_mod_biz_id = $appId
                                                    AND (biz_mod_mod_id = 10 OR biz_mod_mod_id = 17)
                                                    AND biz_mod_active = 1");
                if ($hasKeyFeature > 0){
                    $contentRank +=8;
                }

                $howManyAppStores = $this->db->getVal("SELECT CASE WHEN has = 0 THEN 0
                                                              WHEN has = 1 THEN 4
                                                              ELSE 9 END as appStores FROM (
                                                                SELECT COUNT(id) has FROM tbl_biz_appstores
                                                                WHERE biz_id = $appId
                                                                AND appstore_id <> 4 ) stores");

                $contentRank += $howManyAppStores;

                $screensScore = 0;
                $screens = $this->db->getTable("SELECT * FROM tbl_biz_mod WHERE biz_mod_biz_id = $appId AND biz_mod_active = 1
                                                AND biz_mod_mod_id IN (1,2,3,4,19,23,24,25,26)");

                if (count($screens)>0){
                     foreach ($screens as $screen) {
                        $screenFactor = 1;
                        $rows = $this->db->getVal("SELECT COUNT(md_row_id) ro FROM tbl_mod_data{$screen["biz_mod_mod_id"]}
                                                    WHERE md_biz_id = $appId AND md_mod_id = {$screen["biz_mod_mod_id"]}
                                                    AND md_element NOT IN (10,12)");

                        if ($rows > 3){$screenFactor = 2;}

                        $screensScore += $screenFactor;
                     }
                }

                $contentRank += min($screensScore,18);


                /* Usage Rank */
                $notificationsSent = $this->db->getVal("SELECT COUNT(pu_id) pushes FROM tbl_push WHERE pu_biz_id = $appId AND pu_num_sent > 0");
                $usageRank += min(3*$notificationsSent,12);


                $orders = $this->db->getVal("SELECT COUNT(cto_id) ord FROM tbl_cust_transaction_order WHERE cto_biz_id = $appId and cto_order_type='eCommerce'");
                if ($orders > 0){
                    $usageRank += min($orders,12);
                }else{
                    $meetings = $this->db->getVal("SELECT COUNT(em_id) meets FROM tbl_employee_meeting WHERE em_biz_id = $appId");
                    $usageRank +=min($meetings,12);
                }


                $logins = $this->db->getVal("SELECT COUNT(his_id) histor FROM tbl_history
                                            WHERE his_biz_id = $appId
                                            AND his_action = 'Login'
                                            AND his_time >= DATE_SUB(CURDATE(), INTERVAL 30 DAY) ");

                $usageRank += min(1.5*$logins,12);



                /*Engagement Rank */
                $downloads = $this->db->getVal("SELECT COUNT(DISTINCT(usrbiz_user_id)) downloads FROM tbl_users_biz WHERE usrbiz_biz_id = $appId");

                $entries = $this->apiDB->getTable("SELECT (SUM(abv_volume_andr) + SUM(abv_volume_ios)) entr FROM analytics_biz_visits
                                                    WHERE abv_biz_id = $appId");

                if ($downloads > 0){
                    $ratio1 = intval($entries)/intval($downloads);
                    $ratio2 = intval($entries)/100;
                    $engagementRank += min(18*$ratio1*$ratio2,18);
                }


                $this->db->execute("UPDATE tbl_biz SET biz_app_rank = ($contentRank + $usageRank + $engagementRank) WHERE biz_id = $appId");


            }
        }
    }

    public function apiStats()
    {
        $db = new Database("api");
        $cureDate = "CURDATE()";
        //$cureDate = "'2017-05-15 13:06:37'";

        $db->execute("
             INSERT INTO analytics_biz_visits( abv_biz_id, abv_month, abv_year,abv_volume_andr,abv_volume_ios,abv_date)
             select
             ap_biz_id,
             MONTHNAME(ap_date) mon,
             YEAR(ap_date) yr,
             (select count(ap_id) from api_get_biz WHERE ap_system_name = 'Android' and ap_biz_id = t_main.ap_biz_id and DATE_FORMAT(ap_date,'%Y-%m-%d') = DATE_FORMAT(t_main.ap_date,'%Y-%m-%d')) Android,
             (select count(ap_id) from api_get_biz WHERE ap_system_name = 'iOS' and ap_biz_id = t_main.ap_biz_id and DATE_FORMAT(ap_date,'%Y-%m-%d') = DATE_FORMAT(t_main.ap_date,'%Y-%m-%d')) iOS,
             DATE_FORMAT(ap_date,'%Y-%m-%d') analiticsDate
             from api_get_biz t_main
             WHERE ap_date > (select api_run from analytics_run_time where api_name='analytics_biz_visits')
             GROUP BY ap_biz_id,DATE_FORMAT(ap_date,'%Y-%m-%d')
             ON DUPLICATE KEY UPDATE
             abv_volume_andr = (select count(ap_id) from api_get_biz WHERE ap_system_name = 'Android' and ap_biz_id = abv_biz_id and DATE_FORMAT(ap_date,'%Y-%m-%d')=DATE_FORMAT(abv_date,'%Y-%m-%d')),
             abv_volume_ios = (select count(ap_id) from api_get_biz WHERE ap_system_name = 'iOS' and ap_biz_id = abv_biz_id and DATE_FORMAT(ap_date,'%Y-%m-%d')=DATE_FORMAT(abv_date,'%Y-%m-%d'))
            ");

        $db->execute("update analytics_run_time set api_run=NOW() where api_name='analytics_biz_visits'");



        $db->execute("
                     INSERT INTO analytics_mod_profile(amp_biz_id, amp_mod_id, amp_mod_name, amp_volume,amp_month,amp_year,amp_date)
                     select ap_biz_id,ap_mod_id,mod_name, count(ap_id) volume, MONTHNAME(ap_date) mon, YEAR(ap_date) yer ,DATE_FORMAT(ap_date,'%Y-%m-%d') analiticsDate
                     from api_get_levels A,federated_tbl_modules Mods
                     WHERE ap_level_id = 1
                     AND ap_date > (select api_run from analytics_run_time where api_name='analytics_mod_profile')
                     and ap_mod_id = mod_id
                     GROUP BY ap_biz_id, ap_mod_id,DATE_FORMAT(ap_date,'%Y-%m-%d')
                     ON DUPLICATE KEY UPDATE
                     amp_volume = (select count(ap_id) from api_get_levels WHERE ap_level_id = 1 and ap_biz_id = amp_biz_id and DATE_FORMAT(ap_date,'%Y-%m-%d') = DATE_FORMAT(amp_date,'%Y-%m-%d') and ap_mod_id=amp_mod_id)
                    ");

        $db->execute("update analytics_run_time set api_run=NOW() where api_name='analytics_mod_profile'");



        // *********** installs *************
        for($i=0; $i < 3; $i++){
            $thisMonthAndroid = $this->db->getTable("SELECT cust_biz_id gsm_appid,count(cust_id) vol,DATE_FORMAT(cust_reg_date,'%Y-%m-%d') analiticsDate
                                                    FROM tbl_customers
                                                    WHERE DATE_FORMAT(cust_reg_date,'%Y-%m-%d') = DATE_FORMAT(DATE_SUB($cureDate, INTERVAL $i DAY),'%Y-%m-%d')
                                                    AND cust_device='Android'
                                                    group by cust_biz_id");

            $thisMonthIOS = $this->db->getTable("SELECT cust_biz_id appid,count(cust_id) vol,DATE_FORMAT(cust_reg_date,'%Y-%m-%d') analiticsDate
                                                    FROM tbl_customers
                                                    WHERE DATE_FORMAT(cust_reg_date,'%Y-%m-%d') = DATE_FORMAT(DATE_SUB($cureDate, INTERVAL $i DAY),'%Y-%m-%d')
                                                    AND cust_device='IOS'
                                                    group by cust_biz_id");



            if(count($thisMonthAndroid) > 0){
                foreach ($thisMonthAndroid as $install)
                {
                    $db->execute("
                            INSERT INTO analytics_app_installs( abv_biz_id, abv_month, abv_year,abv_volume_andr,abv_date)
                            values (
                            {$install["gsm_appid"]},
                            MONTHNAME('{$install["analiticsDate"]}'),
                            YEAR('{$install["analiticsDate"]}'),
                            {$install["vol"]},
                            '{$install["analiticsDate"]}')
                            ON DUPLICATE KEY UPDATE
                            abv_volume_andr = {$install["vol"]}
                        ");
                }

            }


            if(count($thisMonthIOS) > 0){
                foreach ($thisMonthIOS as $install)
                {
                    $db->execute("
                                INSERT INTO analytics_app_installs( abv_biz_id, abv_month, abv_year,abv_volume_ios,abv_date)
                                values (
                                {$install["appid"]},
                                MONTHNAME('{$install["analiticsDate"]}'),
                                YEAR('{$install["analiticsDate"]}'),
                                {$install["vol"]},
                                '{$install["analiticsDate"]}')
                                ON DUPLICATE KEY UPDATE
                                abv_volume_ios = {$install["vol"]}
                            ");
                }

            }




            // *********** location *************
            $thisMonthAndroid = $this->db->getTable("SELECT cust_biz_id gsm_appid,if(cust_city is null,'N/A',cust_city) gsm_city,count(cust_id) vol,DATE_FORMAT(cust_reg_date,'%Y-%m-%d') analiticsDate
                                                FROM tbl_customers
                                                WHERE DATE_FORMAT(cust_reg_date,'%Y-%m-%d') = DATE_FORMAT(DATE_SUB($cureDate, INTERVAL $i DAY),'%Y-%m-%d')
                                                AND cust_device='Android'
                                                group by cust_biz_id,cust_city");

            $thisMonthIOS = $this->db->getTable("SELECT cust_biz_id appid,if(cust_city is null,'N/A',cust_city) apns_city, count(cust_id) vol,DATE_FORMAT(cust_reg_date,'%Y-%m-%d') analiticsDate
                                                FROM tbl_customers
                                                WHERE DATE_FORMAT(cust_reg_date,'%Y-%m-%d') = DATE_FORMAT(DATE_SUB($cureDate, INTERVAL $i DAY),'%Y-%m-%d')
                                                AND cust_device='IOS'
                                                group by cust_biz_id,cust_city");


            if(count($thisMonthAndroid) > 0){
                foreach ($thisMonthAndroid as $install)
                {
                    $city = addslashes(stripcslashes($install["gsm_city"]));

                    $db->execute("
                                INSERT INTO analytics_location( amp_biz_id,amp_city_name, amp_month, amp_year,amp_volume_android,amp_date)
                                values (
                                {$install["gsm_appid"]},
                                '$city',
                                MONTHNAME('{$install["analiticsDate"]}'),
                                YEAR('{$install["analiticsDate"]}'),
                                {$install["vol"]},
                                '{$install["analiticsDate"]}')
                                ON DUPLICATE KEY UPDATE
                                amp_volume_android = {$install["vol"]}
                            ");
                }

            }

            if(count($thisMonthIOS) > 0){
                foreach ($thisMonthIOS as $install)
                {

                    $city = addslashes(stripcslashes($install["apns_city"]));

                    $db->execute("
                                INSERT INTO analytics_location(amp_biz_id,amp_city_name, amp_month, amp_year,amp_volume_ios,amp_date)
                                values (
                                {$install["appid"]},
                                '$city',
                                MONTHNAME('{$install["analiticsDate"]}'),
                                YEAR('{$install["analiticsDate"]}'),
                                {$install["vol"]},
                                '{$install["analiticsDate"]}')
                                ON DUPLICATE KEY UPDATE
                                amp_volume_ios = {$install["vol"]}
                            ");
                }

            }
        }

    }

    public function PCASapiStats(){

        $db = new Database("api");
        $cureDate = "CURDATE()";

        for($i=0; $i < 3; $i++){
            // *********** PCAS Installs *************

            $thisMonthAndroid = $this->db->getTable("SELECT usr_marketid,count(usr_id) vol,DATE_FORMAT(usr_start_date,'%Y-%m-%d') analiticsDate
                                                    FROM tbl_users
                                                    WHERE DATE_FORMAT(usr_start_date,'%Y-%m-%d') = DATE_FORMAT(DATE_SUB($cureDate, INTERVAL $i DAY),'%Y-%m-%d')
                                                    AND usr_os='Android'
                                                    AND usr_marketid > 0
                                                    group by usr_marketid");

            $thisMonthIOS = $this->db->getTable("SELECT usr_marketid,count(usr_id) vol,DATE_FORMAT(usr_start_date,'%Y-%m-%d') analiticsDate
                                                    FROM tbl_users
                                                    WHERE DATE_FORMAT(usr_start_date,'%Y-%m-%d') = DATE_FORMAT(DATE_SUB($cureDate, INTERVAL $i DAY),'%Y-%m-%d')
                                                    AND usr_os='iOS'
                                                    AND usr_marketid > 0
                                                    group by usr_marketid");



            if(count($thisMonthAndroid) > 0){
                foreach ($thisMonthAndroid as $install)
                {
                    $db->execute("
                            INSERT INTO analytics_pcas_installs( apv_market_id, apv_month, apv_year,apv_volume_andr,apv_date)
                            values (
                            {$install["usr_marketid"]},
                            MONTHNAME('{$install["analiticsDate"]}'),
                            YEAR('{$install["analiticsDate"]}'),
                            {$install["vol"]},
                            '{$install["analiticsDate"]}')
                            ON DUPLICATE KEY UPDATE
                            apv_volume_andr = {$install["vol"]}
                        ");
                }

            }


            if(count($thisMonthIOS) > 0){
                foreach ($thisMonthIOS as $install)
                {
                    $db->execute("
                                INSERT INTO analytics_pcas_installs( apv_market_id, apv_month, apv_year,apv_volume_ios,apv_date)
                                values (
                                {$install["usr_marketid"]},
                                MONTHNAME('{$install["analiticsDate"]}'),
                                YEAR('{$install["analiticsDate"]}'),
                                {$install["vol"]},
                                '{$install["analiticsDate"]}')
                                ON DUPLICATE KEY UPDATE
                                apv_volume_ios = {$install["vol"]}
                            ");
                }

            }
        }
    }

    public static function getConnectedElementsErrorMessage($type,$item_id,$biz_id){
        $instance = new self();
        $response = array();
        $response['code'] = 0;

        $response['connections'] = array();

        $modM = new moduleManager();


        $connectionsObject = array();

        $connectionsObject['inlayout'] = $modM->isElementInActiveLayout($type,$item_id);
        $connectionsObject['connected_elements'] =  $modM->getElementsConnectedToItem($type,$item_id);
        $connectionsObject['in_mods'] = $modM->getItemLinkedModules($type,$item_id,$biz_id);
        $connectionsObject['future_meeting'] = false;

        if($type == 'employee'){
            $sql = "SELECT Count(*) FROM tbl_employee_meeting WHERE em_emp_id = $item_id AND em_start > NOW()";
            $connectionsObject['future_meeting'] = $instance->db->getVal($sql) > 0;
        }

        $isConnected = $connectionsObject['inlayout'] || $connectionsObject['connected_elements']['connected'] || $connectionsObject['in_mods']['code'] == 1 || $connectionsObject['future_meeting'];

        if($isConnected){
            $response['code'] = 1;
            $response['connections'] = array();
            if($connectionsObject['inlayout']){
                $response['code'] = 2;//cannot delete/hide item

                $response['connections'][] = array(
                        'area' => 'layout'
                    );
            }

            if($connectionsObject['future_meeting']){
                $response['code'] = 2;//cannot delete/hide item

                $response['connections'][] = array(
                        'area' => 'future_meeting'
                    );
            }



            if(count($connectionsObject['connected_elements']['classes']) > 0){
                $response['code'] = 2;//cannot delete/hide item

                $connectionList = array();
                $connectionList['area'] = 'classes';
                $connectionList['inner'] = array();
                foreach ($connectionsObject['connected_elements']['classes'] as $class)
                {

                    $connectionList['inner'][] = $class['label'];
                }

                $response['connections'][] = $connectionList;


            }



            if(count($connectionsObject['connected_elements']['subscriptions']) > 0){

                $response['code'] = 2;//cannot delete/hide item
                $connectionList = array();
                $connectionList['area'] = 'subscriptions';
                $connectionList['inner'] = array();
                foreach ($connectionsObject['connected_elements']['subscriptions'] as $sub)
                {

                    $connectionList['inner'][] = $sub['label'];
                }
                $response['connections'][] = $connectionList;
            }
            if(count($connectionsObject['connected_elements']['punch_passes']) > 0){
                $response['code'] = 2;//cannot delete/hide item
                $connectionList = array();
                $connectionList['area'] = 'punch_passes';
                $connectionList['inner'] = array();


                foreach ($connectionsObject['connected_elements']['punch_passes'] as $pass)
                {

                    $connectionList['inner'][] = $pass['label'];
                }
                $response['connections'][] = $connectionList;
            }

            if($connectionsObject['in_mods']['code'] == 1){

                foreach ($connectionsObject['in_mods']['data'] as $entry)
                {



                    $connectionList = array();
                    $connectionList['area'] = $entry['mod_name'];
                    $connectionList['type'] = 'module';
                    $connectionList['inner'] = array();

                    foreach ($entry['pages'] as $page)
                    {

                        $connectionList['inner'][] = $page['page'];
                    }
                    $response['connections'][] = $connectionList;
                }


            }

        }
        return $response;
    }

    public static function getPlatformDeafultTrialLength(){
        $db = new Database();

        return $db->getVal("SELECT MAX(stam_platorm_trial_days) FROM tbl_stam");
    }

    public static function saveLoyaltySettings($data){

        $instance = new self();

        $sql = "UPDATE ecommerce_store_settings SET
                            ess_points_expire_period = '{$data["period"]}',
                            ess_points_expire_number = {$data["number"]}
                WHERE ess_biz_id = " . $data["biz_id"];

        $instance->db->execute($sql);

        if($data["retroactively"] == 1){
            customersManager::setExpiredDateForPoints($data["biz_id"],$data["number"],$data["period"]);
            utilityManager::asyncTask(AUTOJOBS_SERVER."/biz/autojobs/removeExpiredPoints/".$data["biz_id"]);
        }

        return 1;

    }

    public static function saveGeneralSettings($post,$bizID=0){

        $instance = new self();

        $country = $post['biz_addr_country_id'];
        $state = ($post['biz_addr_country_id'] == "1") ? $post['biz_addr_state_id'] : "0";
        $time_zone = $post['time_zone'];
        $text_dir = $post['text_dir'];


        $storeSettings = appManager::getBizStoreObjectForBiz($bizID);

        $storeSettings->ess_currency = $post['currency'];
        $storeSettings->ess_currency_symbol = $post['currency_symbol'];
        $storeSettings->ess_include_tax = $post['include_tax'];
        $storeSettings->ess_tax_rate = $post['tax_rate'];
        $storeSettings->ess_finalize_amount = $post['finalize_amount'];
        $storeSettings->ess_finalize_by_amount= $post['finalize_by_amount'];
        $storeSettings->ess_finalize_by_time = $post['finalize_by_time'];
        $storeSettings->ess_finalize_period = $post['finalize_period'];
        $storeSettings->ess_finalize_period_nmber = $post['finalize_number'];
        $storeSettings->ess_auto_reminder = $post['auto_reminder'];
        $storeSettings->ess_reminder_after = $post['reminder_after'];
        $storeSettings->ess_reminder_before = $post['reminder_before'];
        $storeSettings->ess_require_cash_drawer = $post['require_drawer'];
        $storeSettings->ess_allow_employee_refund = $post['employee_refund'];
        $storeSettings->ess_handle_unpaid_orders = $post['handle_unpaid'];
        $storeSettings->ess_evaluate_revenue = $post['revenue_calc'];
        $storeSettings->ess_set_cash_as_paid = $post['cash_as_paid'];
        $storeSettings->ess_weight_units = $post['weight_units'];
        $storeSettings->ess_refund_policy = $post['refund_policy'];
        $storeSettings->ess_custom_policy = $post['custom_policy'];
        $storeSettings->ess_refund_policy_on = $post['refund_on'];
        $storeSettings->ess_custom_policy_on = $post['custom_on'];
        $storeSettings->ess_phone = $post['phone'];
        $storeSettings->ess_email = $post['email'];
        $storeSettings->ess_checkout_comment_title = $post['cart_comment'];

        appManager::updateBizStoreSettingsFromBizStoreObject($storeSettings);

        appManager::setCurrencyToModData($bizID,$post['currency']);

        $q2 = "UPDATE tbl_biz SET
                            biz_addr_country = '" . addslashes($post['biz_addr_country']) . "',
                            biz_addr_country_id = $country,
                            biz_addr_state = '" . addslashes($post['biz_addr_state']) . "',
                            biz_addr_state_id = $state,
                            biz_addr_town = '" . addslashes($post['biz_addr_town']) . "',
                            biz_addr_street = '" . addslashes($post['biz_addr_street']) . "',
                            biz_addr_no = '" . addslashes($post['biz_addr_no']) . "',
                            biz_postal_code = '" . addslashes($post['biz_postal_code']) . "',
                            biz_office_tele = '" . addslashes($post['phone']) ."',
                            biz_e_mail = '" . addslashes($post['email']) ."',
                            biz_time_zone = $time_zone,
                            biz_dir = '$text_dir'
                   WHERE biz_id = $bizID" ;


        $res = $instance->db->execute($q2);

        utilityManager::getCoordinatesByAddress("","","","","",$bizID);


        if($res != -1){
            $response["code"] = 1;
            $response["message"] = "";
        }
        else{
            $response["code"] = $res;
            $response["message"] = "";
        }
        return $response;

    }

    public static function saveShippingSettings($values,$bizID){
        $instance = new self();

        $q1 = "UPDATE ecommerce_store_settings SET
                            ess_shipping_policy_on = " . intval($values['shipping_on']) . ",
                            ess_shipping_policy = '" . addslashes($values['shipping_policy']) . "',
                            ess_require_shipping = {$values['shipping_req']}
                   WHERE ess_biz_id = $bizID";
        $instance->db->execute($q1);

        return 1;
    }

    public static function saveShopSettings($values,$bizID)
    {
        $instance = new self();

        $q1 = "UPDATE ecommerce_store_settings SET
                            ess_shipping_policy_on = " . intval($values['shipping_on']) . ",
                            ess_shipping_policy = '" . addslashes($values['shipping_policy']) . "',
                            ess_require_shipping = {$values['shipping_req']}
                   WHERE ess_biz_id = $bizID";
        $instance->db->execute($q1);

        $storeSettings = appManager::getBizStoreObjectForBiz($bizID);
        $storeSettings->ess_handle_warn_before_due_date = $values['warn_before'];
        $storeSettings->ess_track_stock = $values['track_stock'];
        $storeSettings->ess_hide_out_of_stock = $values['hide_out_of_stock'];
        $storeSettings->ess_product_order = $values['product_order'];
        appManager::updateBizStoreSettingsFromBizStoreObject($storeSettings);

        return 1;
    }

    public static function savePersonalZoneData($tabs){

        $instance = new self();
        $i = 1;
        foreach($tabs as $tab){
            $sql = "UPDATE tbl_biz_personal SET
                                        tbpz_index = $i,
                                        tbpz_label = '{$tab['text']}'
                                    WHERE tbpz_id = {$tab['id']}";

            $instance->db->execute($sql);
            $i++;
        }

    }

    public static function updateHolidays($source,$scheme){
        $instance = new self();
        $response = file_get_contents("https://www.googleapis.com/calendar/v3/calendars/en.$scheme%23holiday@group.v.calendar.google.com/events?key=AIzaSyCaemOfU6K5Ipd0q07kn3srCAFEXaB49uQ");

        $holidays = json_decode($response,true);

        foreach ($holidays['items'] as $holiday)
        {
        	$start = strtotime($holiday["start"]["date"]);

            if($start < time() || $start > strtotime("+ 1 year")){//event in the past or too far ahead
                continue;
            }

            $existing = $instance->getHolidayByName($holiday["summary"],$scheme);

            if(isset($existing['hol_id']) && $existing['hol_id'] > 0){//holidays exists, update time
                $instance->db->execute("UPDATE tbl_holidays SET
                            hol_start = '{$holiday["start"]["date"]}',
                            hol_end = '{$holiday["end"]["date"]}',
                            hol_external_id = '{$holiday["id"]}'
                        WHERE hol_id = {$existing['hol_id']}");
            }
            else{//new holiday, add row
                $instance->db->execute("INSERT INTO tbl_holidays SET
                            hol_source = '{$source}',
                            hol_source_scheme = '{$scheme}',
                            hol_name = '{$holiday["summary"]}',
                            hol_start = '{$holiday["start"]["date"]}',
                            hol_end = '{$holiday["end"]["date"]}',
                            hol_external_id = '{$holiday["id"]}'");
            }
        }

    }

    private function getHolidayByName($name,$scheme){
        return $this->db->getRow("SELECT * FROM tbl_holidays
                                    WHERE hol_source_scheme = '$scheme'
                                    AND hol_name = '$name'");
    }

    public static function sendRequestToLog($bizID = 0, $custID = 0,$data = array()){
        if(count($data) == 0){
            $data = $_REQUEST;
        }
        $post = array();
        $post['data'] = json_encode($data);
        $post['bizID'] = $bizID;
        $post['custID'] = $custID;
        $url = "http://127.0.0.1/".MVC_NAME."/eventAsync/logAPIRequest";
        utilityManager::asyncTask($url,$post);
    }

    public static function addAPIRequestLog($data,$bizID = 0, $custID = 0){
        $instance = new self();
        $version = API_VERSION;
        $request = addslashes($data);

        $instance->db->execute("INSERT INTO tbl_api_requests_log SET
                    arl_version = '$version',
                    arl_request = '$request',
                    arl_biz_id = $bizID,
                    arl_customer_id = $custID");
    }
}
?>