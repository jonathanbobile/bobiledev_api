<?php

/**
 * A factory class that creates a class of image storage according to the storage provider defined in config (i.e AWS, Google Storage)
 *
 *
 * @version 1.0
 * @author Alex
 */
class ImageStorage
{
    static function getStorageAdapter()
    {
        $adapter = NULL;
        switch(IMAGE_STORAGE_PROVIDER) {
            case 'google':
                $adapter = new imageStorageGoogle();
                break;
        }
        return $adapter;
    }
}
