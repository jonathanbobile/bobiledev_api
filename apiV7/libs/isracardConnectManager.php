<?php
/**
 * Methods for isracard360 connect
 *
 * @version 1.0
 * @author PapTap
 */


class isracardConnectManager {  
    
    protected $db;    
    private $language;

    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }  

        $system_lang = (isset($_SESSION['system_lang'])) ? $_SESSION['system_lang'] : "en";
        $this->language = new languageManager( $system_lang );
        $this->language->load('builder');
    }

    /**
     * Summary of generateSale
     * @param mixed $data 
     * @return resultObject
     */
    public static function generateSale($data){

        $result = self::isracardApiCall($data,"generate-sale");

        if($result["status_code"] == 0){
            $response = resultObject::withData(1,"",$result);
        }else{
            $response = resultObject::withData(-1,$result["status_error_details"],$result);
        }

        return $response;
    }

    public static function getApiKeyForBiz($bizId){
        $instance = new self();

        $apiKey = $instance->db->getVal("SELECT bp_wallet_id FROM tbl_biz_processors WHERE bp_vendor = 'isracard360' AND bp_biz_id = $bizId");

        return $apiKey == "" ? ISRACARD_BOBILE_API_KEY : $apiKey;
    }
    

    private static function isracardApiCall($data,$endPoint){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, ISRACARD_URL. $endPoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);

        curl_setopt($ch, CURLOPT_POST, TRUE);

        $postData = json_encode($data);

        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          "Content-Type: application/json"
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode($response, true);

    }
}


?>
