<?php

/**
 * languageManager short summary.
 *
 * languageManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class languageManager {
	private $default = 'en';
	private $directory;
	private $data = array();
    private $wl_brand_name = "";
    
	public function __construct($language = '',$bizID = 0) {
        if(isset($language) && $language != ''){
            $this->default = $language;
        }
        if($bizID != 0){
            $ownerInfo = bizManager::getOwnerOrResellerFromBizID($bizID);

            $this->wl_brand_name = isset($ownerInfo['reseller_wl_brand_name']) ? $ownerInfo['reseller_wl_brand_name'] : '';
        }

        $this->load('system');
	}

    public function testArray(){
        return $this->data;
    }
	
    public function get($key,$htmlSafe = false) {  
        
        $placeHolder = '#'. str_replace('_',' ',$key) . '#';       
        
        $text = $placeHolder;
        if(isset($this->data[$key])){
            $text = $this->data[$key];
        }
        else{
            $fixKey = str_replace(' ','_',$key);
            if(isset($this->data[$fixKey])){
                $text = $this->data[$fixKey];
            }

        }

        if(!$this->contains(strtolower($_SERVER['HTTP_HOST']),"bobile.com") && $this->wl_brand_name != ""){
            $pattern = '/bobile/';
            preg_match_all($pattern, $text, $matches);
            $value = $this->wl_brand_name;
            $i = 0;
            foreach ($matches[0] as $match) {
                $text = str_replace($matches[0][$i],$value,$text);
                $i++;
            }
        }
        if($htmlSafe){
            $text = htmlspecialchars($text);
        }
        return $text;
    }

    
	
    public function replace($content,$replaceEmpty = true) {
        $pattern = '#\#(.*?)\##s';
        preg_match_all($pattern, $content, $matches);
        
        $i=0;
        foreach ($matches[0] as $match) {
            $value = $this->get($matches[1][$i]);
            if($value == "")
            {
                if($replaceEmpty)
                {
                    $content = str_replace($matches[0][$i],$value,$content);
                }
            }
            else
            {
                $content = str_replace($matches[0][$i],$value,$content);
            }
            $i++;
        }
        
        return $content;
    }
    
	public function load($filename) {
		
        $lang = $this->default;
        if(strlen($lang) > 2 || $lang == -1){
            $lang = 'en';
        }
        $rand = rand(0,100000000);
        $file = "https://storage.googleapis.com/bobile/languages/".$lang."/". $filename .".php?rand=".$rand;
        require($file);
		
        $this->data = array_merge($this->data, $_);
        
        return $this->data;

        
    }

    private function contains($haystack, $needle,$isCaseSensetive = false)
    {
        if(!$isCaseSensetive){
            $haystack = strtolower($haystack);
            $needle = strtolower($needle);
        }
        $pos = strrpos($haystack, $needle);
        if($pos !== false) {
            return true;
        }
        
        return false;
    }
}
