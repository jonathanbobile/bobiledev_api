<?php

/**
 * loyaltyEngineManager short summary.
 *
 * loyaltyEngineManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class loyaltyEngineManager extends Manager
{


    public function __construct()
    {
        try {
            $this->db = new Database();


        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }
    }

    public static function updateLoyaltySentTime($trackingID){
        $params = array();
        $params["tracking_id"] = $trackingID;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setSendTime",$params);
    }

    public static function updateLoyaltyDeliveredTime($trackingID){
        $params = array();
        $params["tracking_id"] = $trackingID;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setDeliverTime",$params);
    }

    public static function updateLoyaltyOpenTime($trackingID){
        $params = array();
        $params["tracking_id"] = $trackingID;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setOpenTime",$params);
    }

    public static function getCurrentWeekWorkPlan($bizID){
        try{
            $instance = new self();

            $currentWeekRow = $instance->getCurrentWeekDB($bizID);

            $weekPlan = new weeklyPlanObject();

            $weekPlan->week = strtotime($currentWeekRow["bwpc_start_date"]);
            $weekPlan->week_id = $currentWeekRow["bwpc_id"];
            $weekPlan->week_no = $currentWeekRow["bwpc_week_no"];
            $weekPlan->week_year = $currentWeekRow["bwpc_year"];

            $startDate = new DateTime(date("Y-m-d",$weekPlan->week));
            for ($i = 0; $i <= 6; $i++)
            {
                $day = $instance->getDailyItemsForWeek($bizID,$startDate->format("Y-m-d"));

                $weekPlan->daily_plans[] = $day;

                $startDate->add(new DateInterval("P1D"));
            }



            return resultObject::withData(1,'',$weekPlan);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getWorkPlanHistory($bizID,$year = '',$skip = 0){
        try{
            $instance = new self();

            $result = array();

            if($year == ""){
                $year = date("Y");
            }

            $weekStartDay = bizManager::getBizWeekStartDay($bizID);

            $startDayOffset = $weekStartDay == "monday" ? 1 : 0;

            $workPlanRows = $instance->getPlanHistoryDB($bizID,$year,$skip);

            foreach ($workPlanRows as $workPlanRow)
            {
            	$weekStartDate = new DateTime();
                $weekStartDate->setISODate($workPlanRow["bwpc_year"],$workPlanRow["bwpc_week_no"],$startDayOffset);

                $weekPlan = new weeklyPlanObject();
                $weekPlan->week_id = $workPlanRow["bwpc_id"];
                $weekPlan->week_no = $workPlanRow["bwpc_week_no"];
                $weekPlan->week_year = $workPlanRow["bwpc_year"];
                $weekPlan->week = strtotime($weekStartDate->format("Y-m-d"));

                $result[] = $weekPlan;
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getPastWeekPlan($bizID,$weekID){
        try{
            $instance = new self();
            $weekPlan = new weeklyPlanObject();

            $weekRow = $instance->getPastWeek($weekID);

            $weekPlan->week = strtotime($weekRow["bwpc_start_date"]);
            $weekPlan->week_id = $weekRow["bwpc_id"];
            $weekPlan->week_no = $weekRow["bwpc_week_no"];
            $weekPlan->week_year = $weekRow["bwpc_year"];

            $startDate = new DateTime($weekRow["bwpc_start_date"]);
            for ($i = 0; $i < 6; $i++)
            {
                $day = $instance->getDailyItemsForWeek($bizID,$startDate->format("Y-m-d"));

                $weekPlan->daily_plans[] = $day;

                $startDate->add(new DateInterval("P1D"));
            }



            return resultObject::withData(1,'',$weekPlan);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getWorkItem($bizID,$group,$day){
        try{
            $instance = new self();
            $workItem = $instance->getDayItemByDayAndEventGroup($bizID,$group,$day);

            $today = strtotime("today");

            $dayUnix = strtotime($day);

            if($dayUnix > $today){
                $customers = $instance->getPendingCustomersListForEventGroup($bizID,$group,$day);
            }
            else{
                $customers = $instance->getSentCustomersListForEventGroup($bizID,$group,$day);
            }

            $workItem->customers = $customers;

            return resultObject::withData(1,'',$workItem);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function fillWeeklyPlanForBiz($bizID){
        $params = array();
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/entry/fillWeeklyQueueForBiz/".$bizID,$params);
    }

    public static function setLoyaltySentChannel($trackingID,$channel){
        $params = array();
        $params["tracking_id"];
        $params["channel"] = $channel;
        utilityManager::asyncTask("https://".LOYALTY_SERVER."/loyalty/tracker/setChannel",$params);
    }

    public static function getDemoPlan($bizID){
        $instance = new self();

        $startDay = bizManager::getBizWeekStartDay($bizID);

        $nextWeekStartDateUnix = strtotime("next $startDay");

        $prevWeekMinDayToCalc = strtotime("next $startDay -3 days");

        $currentUnix = strtotime("today");

        $weekPlan = new weeklyPlanObject();

        if($currentUnix >= $prevWeekMinDayToCalc){
            $weekPlan->week = $nextWeekStartDateUnix;
        }
        else{
            $weekPlan->week = strtotime("previous $startDay");
        }

        $weekPlan->week_year = date('Y',$weekPlan->week);

        $weekPlan->week_no = date('W',$weekPlan->week);

        if($startDay == "sunday"){
            $weekPlan->week_no += 1;
        }

        $startDate = new DateTime(date("Y-m-d",$weekPlan->week));
        for ($i = 0; $i < 6; $i++)
        {
            $day = $instance->getDailyItemsForWeek($bizID,$startDate->format("Y-m-d"));

            $weekPlan->daily_plans[] = $day;

            $startDate->add(new DateInterval("P1D"));
        }

        return $weekPlan;
    }

    /************************************* */
    /*   LOYALTY ENGINE - PRIVATE          */
    /************************************* */

    private function getCurrentWeekDB($bizID){
        $sql = "SELECT * FROM tbl_biz_week_plan_calculated
                    WHERE bwpc_biz_id = $bizID
                ORDER BY bwpc_week_no DESC
                LIMIT 1";

        return $this->db->getRow($sql);
    }

    private function getDailyItemsForWeek($bizID,$dayDate){
        $langauge = new languageManager(bizManager::getAppOwnerLang($bizID));
        $langauge->load("home");
        $day = new dailyPlanObject();
        $day->day = strtotime($dayDate);

        $sql = "SELECT tbl_events.*,tbl_biz_event.* FROM tbl_events,tbl_biz_event
                    WHERE bevent_event_id = event_id
                    AND bevent_biz_id = $bizID
                GROUP BY event_group,event_triggered_by";

        $dailyLogs = $this->db->getTable($sql);

        $sectionID = 1;

        $tempSections = array();
        foreach ($dailyLogs as $logEntry)
        {
            $eventFullObject = bizEventObject::withData($logEntry);

            if(!isset($tempSections[$eventFullObject->event_purpose])){
                $section = new planSectionObject();
                $section->section_id = $sectionID;
                $section->section_name = $langauge->get("work_section_".$eventFullObject->event_purpose);
                $sectionID++;

                $tempSections[$eventFullObject->event_purpose] = $section;
            }

            $workItem = new workItemObject();

            $workItem->item_source = 'event';
            $workItem->item_source_id = $eventFullObject->event_id;
            $workItem->item_planning_action = $eventFullObject->event_triggered_by == "system" ? "planned" : "passive";
            $workItem->item_id = $eventFullObject->event_group;
            $workItem->item_name = $langauge->get("work_item_name_".$workItem->item_source."_".$eventFullObject->event_group);

            if($day->day > strtotime('today')){
                $workItem->item_customers_count = $this->getPendingCustomersCountForDailyEventGroup($dayDate,$eventFullObject->event_group,$bizID);
                $workItem->item_status = "pending";
            }
            else{
                $workItem->item_customers_count = $this->getSentCustomersCountForDailyEventGroup($dayDate,$eventFullObject->event_group,$bizID);
                $workItem->item_status = "sent";
            }



            $tempSections[$eventFullObject->event_purpose]->work_items[] = $workItem;
        }

        foreach ($tempSections as $tempSection)
        {
        	$day->plan_sections[] = $tempSection;
        }


        return $day;
    }

    private function getPendingCustomersCountForDailyEvent($day,$eventID,$bizID){
        return $this->db->getVal("SELECT COUNT(*) FROM tbl_loyalty_queue_log
                        WHERE DATE(clql_send_time) = '$day'
                        AND clql_event_id = $eventID
                        AND clql_biz_id = $bizID");
    }

    private function getPendingCustomersCountForDailyEventGroup($day,$group,$bizID){
        return $this->db->getVal("SELECT COUNT(*) FROM tbl_loyalty_queue_log
                        WHERE DATE(clql_send_time) = '$day'
                        AND clql_event_id IN (SELECT event_id FROm tbl_events WHERE event_group = '$group')
                        AND clql_biz_id = $bizID");
    }

    private function getSentCustomersCountForDailyEvent($day,$eventID,$bizID){
        return $this->db->getVal("SELECT COUNT(*) FROM tbl_customer_events,tbl_cust_loyalty_sent,tbl_loyalty_queue_log
                        LEFT JOIN tbl_cust_history ON ch_id = clql_cust_history_id
                        WHERE cevent_queue_log_id = clql_id
                        AND cls_cevent_id = cevent_id
                        AND DATE(clql_send_time) = '$day'
                        AND clql_event_id = $eventID
                        AND clql_biz_id = $bizID");
    }

    private function getSentCustomersCountForDailyEventGroup($day,$group,$bizID){
        return $this->db->getVal("SELECT COUNT(*) FROM tbl_customer_events,tbl_cust_loyalty_sent,tbl_loyalty_queue_log
                        LEFT JOIN tbl_cust_history ON ch_id = clql_cust_history_id
                        WHERE cevent_queue_log_id = clql_id
                        AND cls_cevent_id = cevent_id
                        AND DATE(clql_send_time) = '$day'
                        AND clql_event_id IN (SELECT event_id FROm tbl_events WHERE event_group = '$group')
                        AND clql_biz_id = $bizID");
    }

    private function getDayItemByDayAndEventID($bizID,$eventID,$day){
        $sql = "SELECT tbl_events.*,tbl_biz_event.* FROM tbl_events,tbl_biz_event
                WHERE bevent_event_id = event_id
                AND bevent_biz_id = $bizID
                AND event_id = $eventID";

        $workItemRow = $this->db->getRow($sql);

        $eventFullObject = bizEventObject::withData($workItemRow);

        $workItem = new workItemObject();

        $workItem->item_source = 'event';
        $workItem->item_source_id = $eventFullObject->event_id;
        $workItem->item_planning_action = $eventFullObject->event_triggered_by == "system" ? "planned" : "passive";
        $workItem->item_id = $eventFullObject->event_id;

        if(strtotime($day) > strtotime('today')){
            $workItem->item_customers_count = $this->getPendingCustomersCountForDailyEvent($day,$eventFullObject->event_id,$bizID);
            $workItem->item_status = "pending";
        }
        else{
            $workItem->item_customers_count = $this->getSentCustomersCountForDailyEvent($day,$eventFullObject->event_id,$bizID);
            $workItem->item_status = "sent";
        }

        return $workItem;
    }

    private function getDayItemByDayAndEventGroup($bizID,$group,$day){
        $langauge = new languageManager(bizManager::getAppOwnerLang($bizID));
        $langauge->load("home");
        $sql = "SELECT tbl_events.*,tbl_biz_event.* FROM tbl_events,tbl_biz_event
                WHERE bevent_event_id = event_id
                AND bevent_biz_id = $bizID
                AND event_id IN (SELECT event_id FROm tbl_events WHERE event_group = '$group')
                GROUP BY event_group";

        $workItemRow = $this->db->getRow($sql);

        $eventFullObject = bizEventObject::withData($workItemRow);

        $workItem = new workItemObject();

        $workItem->item_source = 'event';
        $workItem->item_source_id = $eventFullObject->event_id;
        $workItem->item_planning_action = $eventFullObject->event_triggered_by == "system" ? "planned" : "passive";
        $workItem->item_id = $eventFullObject->event_group;
        $workItem->item_name = $langauge->get("work_item_name_".$workItem->item_source."_".$eventFullObject->event_group);
        if(strtotime($day) > strtotime('today')){
            $workItem->item_customers_count = $this->getPendingCustomersCountForDailyEventGroup($day,$eventFullObject->event_group,$bizID);
            $workItem->item_status = "pending";
        }
        else{
            $workItem->item_customers_count = $this->getSentCustomersCountForDailyEventGroup($day,$eventFullObject->event_group,$bizID);
            $workItem->item_status = "sent";
        }

        return $workItem;
    }

    private function getPlanHistoryDB($bizID,$year,$skip = 0){
        $skip += 1;

        return $this->db->getTable("SELECT * FROM tbl_biz_week_plan_calculated
                WHERE bwpc_biz_id = $bizID
                AND bwpc_year = $year
                ORDER BY bwpc_week_no DESC
                LIMIT $skip,20");
    }

    private function getPastWeek($weekID){
        return $this->db->getRow("SELECT * FROM tbl_biz_week_plan_calculated
                                    WHERE bwpc_id = $weekID");
    }

    private function getPendingCustomersListForWorkItem($bizID,$eventID,$day){
        $langauge = new languageManager(bizManager::getAppOwnerLang($bizID));
        $langauge->load("home");
        $sql = "SELECT * FROM tbl_loyalty_queue_log
                        WHERE DATE(clql_send_time) = '$day'
                        AND clql_event_id = $eventID
                        AND clql_biz_id = $bizID";

        $logsList = $this->db->getTable($sql);

        $result = array();

        foreach ($logsList as $logEntry)
        {
        	$log = loyaltyQueueLogObject::withData($logEntry);

            $customerEntry = new workItemCustomerObject();

            $customerEntry->cust_id = $log->clql_cust_id;
            $customerEntry->recevied_entity = $log->clql_grant_entity;

            switch($log->clql_grant_entity){
                case "points":
                    $customerEntry->received_label = $log->clql_grant_point_amount;
                    break;
                case "coupon":
                    $levelDataManager = new levelDataManager(26);
                    $couponRow = $levelDataManager->getLevelDataDirectByID($log->clql_grant_entity_id)->data;
                    $couponRow = $couponRow["md_head"];
                    break;
                case "custom":
                    $customerEntry->received_label = $log->clql_grant_title;
                    break;
            }

            $customerEntry->status = 0;
            $customerEntry->granted_for_label = $log->clql_event_id;
            $customerEntry->granted_source = "event";
            $customerEntry->time = strtotime($log->clql_send_time);
            $result[] = $customerEntry;
        }


        return $result;
    }

    private function getPendingCustomersListForEventGroup($bizID,$group,$day){
        $langauge = new languageManager(bizManager::getAppOwnerLang($bizID));
        $langauge->load("loyalty");
        $sql = "SELECT * FROM tbl_loyalty_queue_log
                        WHERE DATE(clql_send_time) = '$day'
                        AND clql_event_id IN (SELECT event_id FROm tbl_events WHERE event_group = '$group')
                        AND clql_biz_id = $bizID";

        $logsList = $this->db->getTable($sql);

        $result = array();
        $customerModel = new customerModel();
        foreach ($logsList as $logEntry)
        {
        	$log = loyaltyQueueLogObject::withData($logEntry);

            $customerEntry = new workItemCustomerObject();

            $customerEntry->cust_id = $log->clql_cust_id;
            $customerEntry->recevied_entity = $log->clql_grant_entity;

            switch($log->clql_grant_entity){
                case "points":
                    $customerEntry->received_label = $log->clql_grant_point_amount;
                    break;
                case "coupon":
                    $levelDataManager = new levelDataManager(26);
                    $couponRow = $levelDataManager->getLevelDataDirectByID($log->clql_grant_entity_id)->data;
                    $couponRow = $couponRow["md_head"];
                    break;
                case "custom":
                    $customerEntry->received_label = $log->clql_grant_title;
                    break;
            }

            $customerEntry->status = 0;
            $customerEntry->granted_for_label = $langauge->get('event_entry_'.$log->clql_event_id);
            $customerEntry->granted_source = "event";
            $customerEntry->time = $log->clql_send_time;
            $customerEntry->time_unix = strtotime($log->clql_send_time);
            $result[] = $customerEntry;
        }


        return $result;
    }

    private function getSentCustomersListForWorkItem($bizID,$eventID,$day){
        $sql = "SELECT * FROM tbl_customer_events,tbl_cust_loyalty_sent,tbl_loyalty_queue_log
                        LEFT JOIN tbl_cust_history ON ch_id = clql_cust_history_id
                        WHERE cevent_queue_log_id = clql_id
                        AND cls_cevent_id = cevent_id
                        AND DATE(clql_send_time) = '$day'
                        AND clql_event_id = $eventID
                        AND clql_biz_id = $bizID";

        $logsList = $this->db->getTable($sql);

        $result = array();
        $customerModel = new customerModel();
        foreach ($logsList as $logEntry)
        {
        	$log = loyaltyQueueLogObject::withData($logEntry);
            $sent = custLoyaltySentObject::withData($logEntry);
            $custEvent = customerEventObject::withData($logEntry);


            $customerEntry = new workItemCustomerObject();

            $customerEntry->cust_id = $log->clql_cust_id;
            $customerEntry->recevied_entity = $sent->cls_granted_entity;

            switch($customerEntry->recevied_entity){
                case "points":

                    $pointsHistoryRow = $customerModel->getPointsHistoryEntryForCustomerID($customerEntry->cust_id,$sent->cls_granted_entity_id)->data;
                    $customerEntry->received_label = $pointsHistoryRow->cph_value;
                    break;
                case "coupon":
                    $levelDataManager = new levelDataManager(26);
                    $couponRow = $levelDataManager->getLevelDataDirectByID($log->clql_grant_entity_id)->data;
                    $couponRow = $couponRow["md_head"];
                    break;
                case "custom":
                    $customerEntry->received_label = $log->clql_grant_title;
                    break;
            }

            $customerEntry->status = 0;
            $customerEntry->time = $log->clql_send_time;
            if(isset($sent->cls_channel_sent_on)){
                $customerEntry->status = 1;
                $customerEntry->time = $sent->cls_channel_sent_on;
            }
            if(isset($sent->cls_channel_deliverd_on)){
                $customerEntry->status = 2;
                $customerEntry->time = $sent->cls_channel_deliverd_on;
            }
            if(isset($sent->cls_channel_opened_on)){
                $customerEntry->status = 3;
                $customerEntry->time = $sent->cls_channel_opened_on;
            }

            $customerEntry->granted_for_label = $log->clql_event_id;
            $customerEntry->granted_source = "event";

            if(isset($logEntry["ch_external_type"]) && $logEntry["ch_external_type"] == 'meeting'){
                $customerEntry->granted_source = "meeting";
                $customerEntry->granted_for_label = $this->db->getVal("SELECT em_name FROM tbl_employee_meeting WHERE em_id = ".$logEntry["ch_external_id"]);
            }

            $result[] = $customerEntry;
        }


        return $result;
    }

    private function getSentCustomersListForEventGroup($bizID,$group,$day){
        $langauge = new languageManager(bizManager::getAppOwnerLang($bizID));
        $langauge->load("loyalty");
        $sql = "SELECT * FROM tbl_customer_events,tbl_cust_loyalty_sent,tbl_loyalty_queue_log
                        LEFT JOIN tbl_cust_history ON ch_id = clql_cust_history_id
                        WHERE cevent_queue_log_id = clql_id
                        AND cls_cevent_id = cevent_id
                        AND DATE(clql_send_time) = '$day'
                        AND clql_event_id IN (SELECT event_id FROm tbl_events WHERE event_group = '$group')
                        AND clql_biz_id = $bizID";

        $logsList = $this->db->getTable($sql);

        $result = array();
        $customerModel = new customerModel();
        foreach ($logsList as $logEntry)
        {
        	$log = loyaltyQueueLogObject::withData($logEntry);
            $sent = custLoyaltySentObject::withData($logEntry);
            $custEvent = customerEventObject::withData($logEntry);


            $customerEntry = new workItemCustomerObject();

            $customerEntry->cust_id = $log->clql_cust_id;
            $customerEntry->recevied_entity = $sent->cls_granted_entity;

            switch($customerEntry->recevied_entity){
                case "points":

                    $pointsHistoryRow = $customerModel->getPointsHistoryEntryForCustomerID($customerEntry->cust_id,$sent->cls_granted_entity_id)->data;
                    $customerEntry->received_label = $pointsHistoryRow->cph_value;
                    break;
                case "coupon":
                    $levelDataManager = new levelDataManager(26);
                    $couponRow = $levelDataManager->getLevelDataDirectByID($log->clql_grant_entity_id)->data;
                    $couponRow = $couponRow["md_head"];
                    break;
                case "custom":
                    $customerEntry->received_label = $log->clql_grant_title;
                    break;
            }

            $customerEntry->status = 0;
            $customerEntry->time = $log->clql_send_time;
            $customerEntry->time_unix = strtotime($log->clql_send_time);
            if(isset($sent->cls_channel_sent_on)){
                $customerEntry->status = 1;
                $customerEntry->time = $sent->cls_channel_sent_on;
                $customerEntry->time_unix = strtotime($sent->cls_channel_sent_on);
            }
            if(isset($sent->cls_channel_deliverd_on)){
                $customerEntry->status = 2;
                $customerEntry->time = $sent->cls_channel_deliverd_on;
                $customerEntry->time_unix = strtotime($sent->cls_channel_deliverd_on);
            }
            if(isset($sent->cls_channel_opened_on)){
                $customerEntry->status = 3;
                $customerEntry->time = $sent->cls_channel_opened_on;
                $customerEntry->time_unix = strtotime($sent->cls_channel_opened_on);
            }

            $customerEntry->granted_for_label = $log->clql_event_id;
            $customerEntry->granted_source = "event";

            if(isset($logEntry["ch_external_type"]) && $logEntry["ch_external_type"] == 'meeting'){
                $customerEntry->granted_source = "meeting";
                $customerEntry->granted_for_label = $this->db->getVal("SELECT em_name FROM tbl_employee_meeting WHERE em_id = ".$logEntry["ch_external_id"]);
            }
            else{
                $customerEntry->granted_for_label = $langauge->get('event_entry_'.$log->clql_event_id);
                $customerEntry->granted_source = "event";
            }



            $result[] = $customerEntry;
        }


        return $result;
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUE - PUBLIC           */
    /************************************* */

    /**
     * Insert new loyaltyQueueObject to DB
     * Return Data = new loyaltyQueueObject ID
     * @param loyaltyQueueObject $loyaltyQueueObj
     * @return resultObject
     */
    public function addLoyaltyQueue(loyaltyQueueObject $loyaltyQueueObj){
        try{
            $newId = $this->addLoyaltyQueueDB($loyaltyQueueObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get loyaltyQueue from DB for provided ID
     * * Return Data = loyaltyQueueObject
     * @param int $loyaltyQueueId
     * @return resultObject
     */
    public function getLoyaltyQueueByID($loyaltyQueueId){

        try {
            $loyaltyQueueData = $this->loadLoyaltyQueueFromDB($loyaltyQueueId);

            $loyaltyQueueObj = loyaltyQueueObject::withData($loyaltyQueueData);
            $result = resultObject::withData(1,'',$loyaltyQueueObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param loyaltyQueueObject $loyaltyQueueObj
     * @return resultObject
     */
    public function updateLoyaltyQueue(loyaltyQueueObject $loyaltyQueueObj){
        try{
            $this->upateLoyaltyQueueDB($loyaltyQueueObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete loyaltyQueue from DB
     * @param int $loyaltyQueueID
     * @return resultObject
     */
    public function deleteLoyaltyQueueById($loyaltyQueueID){
        try{
            $this->deleteLoyaltyQueueByIdDB($loyaltyQueueID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUE - DB METHODS           */
    /************************************* */

    private function addLoyaltyQueueDB(loyaltyQueueObject $obj){

        if (!isset($obj)){
            throw new Exception("loyaltyQueueObject value must be provided");
        }

        $clq_grant_valid_fromDate = isset($obj->clq_grant_valid_from) ? "'".$obj->clq_grant_valid_from."'" : "null";

        $clq_grant_valid_toDate = isset($obj->clq_grant_valid_to) ? "'".$obj->clq_grant_valid_to."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_loyalty_queue SET
                                        clq_biz_id = {$obj->clq_biz_id},
                                        clq_cust_id = {$obj->clq_cust_id},
                                        clq_event_id = {$obj->clq_event_id},
                                        clq_priority = {$obj->clq_priority},
                                        clq_cust_history_id = {$obj->clq_cust_history_id},
                                        clq_grant_entity = '{$obj->clq_grant_entity}',
                                        clq_grant_point_amount = {$obj->clq_grant_point_amount},
                                        clq_grant_entity_id = {$obj->clq_grant_entity_id},
                                        clq_grant_extra_data = '".addslashes($obj->clq_grant_extra_data)."',
                                        clq_grant_title = '".addslashes($obj->clq_grant_title)."',
                                        clq_grant_description = '".addslashes($obj->clq_grant_description)."',
                                        clq_grant_valid_from = $clq_grant_valid_fromDate,
                                        clq_grant_valid_to = $clq_grant_valid_toDate,
                                        clq_message = '".addslashes($obj->clq_message)."'
                                                 ");
        return $newId;
    }

    private function loadLoyaltyQueueFromDB($loyaltyQueueID){

        if (!is_numeric($loyaltyQueueID) || $loyaltyQueueID <= 0){
            throw new Exception("Illegal value $loyaltyQueueID");
        }

        return $this->db->getRow("SELECT * FROM tbl_loyalty_queue WHERE clq_id = $loyaltyQueueID");
    }

    private function upateLoyaltyQueueDB(loyaltyQueueObject $obj){

        if (!isset($obj->clq_id) || !is_numeric($obj->clq_id) || $obj->clq_id <= 0){
            throw new Exception("loyaltyQueueObject value must be provided");
        }

        $clq_send_timeDate = isset($obj->clq_send_time) ? "'".$obj->clq_send_time."'" : "null";

        $clq_grant_valid_fromDate = isset($obj->clq_grant_valid_from) ? "'".$obj->clq_grant_valid_from."'" : "null";

        $clq_grant_valid_toDate = isset($obj->clq_grant_valid_to) ? "'".$obj->clq_grant_valid_to."'" : "null";

        $this->db->execute("UPDATE tbl_loyalty_queue SET
                                clq_biz_id = {$obj->clq_biz_id},
                                clq_cust_id = {$obj->clq_cust_id},
                                clq_send_time = $clq_send_timeDate,
                                clq_event_id = {$obj->clq_event_id},
                                clq_priority = {$obj->clq_priority},
                                clq_cust_history_id = {$obj->clq_cust_history_id},
                                clq_grant_entity = '{$obj->clq_grant_entity}',
                                clq_grant_point_amount = {$obj->clq_grant_point_amount},
                                clq_grant_entity_id = {$obj->clq_grant_entity_id},
                                clq_grant_extra_data = '".addslashes($obj->clq_grant_extra_data)."',
                                clq_grant_title = '".addslashes($obj->clq_grant_title)."',
                                clq_grant_description = '".addslashes($obj->clq_grant_description)."',
                                clq_grant_valid_from = $clq_grant_valid_fromDate,
                                clq_grant_valid_to = $clq_grant_valid_toDate,
                                clq_message = '".addslashes($obj->clq_message)."'
                                WHERE clq_id = {$obj->clq_id}
                                         ");
    }

    private function deleteLoyaltyQueueByIdDB($loyaltyQueueID){

        if (!is_numeric($loyaltyQueueID) || $loyaltyQueueID <= 0){
            throw new Exception("Illegal value $loyaltyQueueID");
        }

        $this->db->execute("DELETE FROM tbl_loyalty_queue WHERE clq_id = $loyaltyQueueID");
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUELOG - PUBLIC           */
    /************************************* */

    /**
     * Insert new loyaltyQueueLogObject to DB
     * Return Data = new loyaltyQueueLogObject ID
     * @param loyaltyQueueLogObject $loyaltyQueueLogObj
     * @return resultObject
     */
    public function addLoyaltyQueueLog(loyaltyQueueLogObject $loyaltyQueueLogObj){
        try{
            $newId = $this->addLoyaltyQueueLogDB($loyaltyQueueLogObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueLogObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get loyaltyQueueLog from DB for provided ID
     * * Return Data = loyaltyQueueLogObject
     * @param int $loyaltyQueueLogId
     * @return resultObject
     */
    public function getLoyaltyQueueLogByID($loyaltyQueueLogId){

        try {
            $loyaltyQueueLogData = $this->loadLoyaltyQueueLogFromDB($loyaltyQueueLogId);

            $loyaltyQueueLogObj = loyaltyQueueLogObject::withData($loyaltyQueueLogData);
            $result = resultObject::withData(1,'',$loyaltyQueueLogObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueLogId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param loyaltyQueueLogObject $loyaltyQueueLogObj
     * @return resultObject
     */
    public function updateLoyaltyQueueLog(loyaltyQueueLogObject $loyaltyQueueLogObj){
        try{
            $this->upateLoyaltyQueueLogDB($loyaltyQueueLogObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($loyaltyQueueLogObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete loyaltyQueueLog from DB
     * @param int $loyaltyQueueLogID
     * @return resultObject
     */
    public function deleteLoyaltyQueueLogById($loyaltyQueueLogID){
        try{
            $this->deleteLoyaltyQueueLogByIdDB($loyaltyQueueLogID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$loyaltyQueueLogID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC LOYALTYQUEUELOG - DB METHODS           */
    /************************************* */

    private function addLoyaltyQueueLogDB(loyaltyQueueLogObject $obj){

        if (!isset($obj)){
            throw new Exception("loyaltyQueueLogObject value must be provided");
        }

        $clql_send_timeDate = isset($obj->clql_send_time) ? "'".$obj->clql_send_time."'" : "NOW()";

        $clql_grant_valid_fromDate = isset($obj->clql_grant_valid_from) ? "'".$obj->clql_grant_valid_from."'" : "null";

        $clql_grant_valid_toDate = isset($obj->clql_grant_valid_to) ? "'".$obj->clql_grant_valid_to."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_loyalty_queue_log SET
                                        clql_biz_id = {$obj->clql_biz_id},
                                        clql_cust_id = {$obj->clql_cust_id},
                                        clql_send_time = $clql_send_timeDate,
                                        clql_event_id = {$obj->clql_event_id},
                                        clql_priority = {$obj->clql_priority},
                                        clql_cust_history_id = {$obj->clql_cust_history_id},
                                        clql_grant_entity = '{$obj->clql_grant_entity}',
                                        clql_grant_point_amount = {$obj->clql_grant_point_amount},
                                        clql_grant_entity_id = {$obj->clql_grant_entity_id},
                                        clql_grant_extra_data = '".addslashes($obj->clql_grant_extra_data)."',
                                        clql_grant_title = '".addslashes($obj->clql_grant_title)."',
                                        clql_grant_description = '".addslashes($obj->clql_grant_description)."',
                                        clql_grant_valid_from = $clql_grant_valid_fromDate,
                                        clql_grant_valid_to = $clql_grant_valid_toDate,
                                        clql_message = '".addslashes($obj->clql_message)."'
                                                 ");
        return $newId;
    }

    private function loadLoyaltyQueueLogFromDB($loyaltyQueueLogID){

        if (!is_numeric($loyaltyQueueLogID) || $loyaltyQueueLogID <= 0){
            throw new Exception("Illegal value $loyaltyQueueLogID");
        }

        return $this->db->getRow("SELECT * FROM tbl_loyalty_queue_log WHERE clql_id = $loyaltyQueueLogID");
    }

    private function upateLoyaltyQueueLogDB(loyaltyQueueLogObject $obj){

        if (!isset($obj->clql_id) || !is_numeric($obj->clql_id) || $obj->clql_id <= 0){
            throw new Exception("loyaltyQueueLogObject value must be provided");
        }

        $clql_send_timeDate = isset($obj->clql_send_time) ? "'".$obj->clql_send_time."'" : "null";

        $clql_grant_valid_fromDate = isset($obj->clql_grant_valid_from) ? "'".$obj->clql_grant_valid_from."'" : "null";

        $clql_grant_valid_toDate = isset($obj->clql_grant_valid_to) ? "'".$obj->clql_grant_valid_to."'" : "null";

        $this->db->execute("UPDATE tbl_loyalty_queue_log SET
                                clql_biz_id = {$obj->clql_biz_id},
                                clql_cust_id = {$obj->clql_cust_id},
                                clql_send_time = $clql_send_timeDate,
                                clql_event_id = {$obj->clql_event_id},
                                clql_priority = {$obj->clql_priority},
                                clql_cust_history_id = {$obj->clql_cust_history_id},
                                clql_grant_entity = '{$obj->clql_grant_entity}',
                                clql_grant_point_amount = {$obj->clql_grant_point_amount},
                                clql_grant_entity_id = {$obj->clql_grant_entity_id},
                                clql_grant_extra_data = '".addslashes($obj->clql_grant_extra_data)."',
                                clql_grant_title = '".addslashes($obj->clql_grant_title)."',
                                clql_grant_description = '".addslashes($obj->clql_grant_description)."',
                                clql_grant_valid_from = $clql_grant_valid_fromDate,
                                clql_grant_valid_to = $clql_grant_valid_toDate,
                                clql_message = '".addslashes($obj->clql_message)."'
                                WHERE clql_id = {$obj->clql_id}
                                         ");
    }

    private function deleteLoyaltyQueueLogByIdDB($loyaltyQueueLogID){

        if (!is_numeric($loyaltyQueueLogID) || $loyaltyQueueLogID <= 0){
            throw new Exception("Illegal value $loyaltyQueueLogID");
        }

        $this->db->execute("DELETE FROM tbl_loyalty_queue_log WHERE clql_id = $loyaltyQueueLogID");
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYKILL - PUBLIC           */
    /************************************* */

    /**
     * Insert new custLoyaltyKillObject to DB
     * Return Data = new custLoyaltyKillObject ID
     * @param custLoyaltyKillObject $custLoyaltyKillObj
     * @return resultObject
     */
    public function addCustLoyaltyKill(custLoyaltyKillObject $custLoyaltyKillObj){
        try{
            $newId = $this->addCustLoyaltyKillDB($custLoyaltyKillObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltyKillObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get custLoyaltyKill from DB for provided ID
     * * Return Data = custLoyaltyKillObject
     * @param int $custLoyaltyKillId
     * @return resultObject
     */
    public function getCustLoyaltyKillByID($custLoyaltyKillId){

        try {
            $custLoyaltyKillData = $this->loadCustLoyaltyKillFromDB($custLoyaltyKillId);

            $custLoyaltyKillObj = custLoyaltyKillObject::withData($custLoyaltyKillData);
            $result = resultObject::withData(1,'',$custLoyaltyKillObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltyKillId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param custLoyaltyKillObject $custLoyaltyKillObj
     * @return resultObject
     */
    public function updateCustLoyaltyKill(custLoyaltyKillObject $custLoyaltyKillObj){
        try{
            $this->upateCustLoyaltyKillDB($custLoyaltyKillObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltyKillObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete custLoyaltyKill from DB
     * @param int $custLoyaltyKillID
     * @return resultObject
     */
    public function deleteCustLoyaltyKillById($custLoyaltyKillID){
        try{
            $this->deleteCustLoyaltyKillByIdDB($custLoyaltyKillID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltyKillID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYKILL - DB METHODS           */
    /************************************* */

    private function addCustLoyaltyKillDB(custLoyaltyKillObject $obj){

        if (!isset($obj)){
            throw new Exception("custLoyaltyKillObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_loyalty_killed SET
                                        clk_biz_id = {$obj->clk_biz_id},
                                        clk_cust_id = {$obj->clk_cust_id},
                                        clk_cevent_id = {$obj->clk_cevent_id},
                                        clk_kill_reason = '{$obj->clk_kill_reason}'
                                                 ");
        return $newId;
    }

    private function loadCustLoyaltyKillFromDB($custLoyaltyKillID){

        if (!is_numeric($custLoyaltyKillID) || $custLoyaltyKillID <= 0){
            throw new Exception("Illegal value $custLoyaltyKillID");
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_loyalty_killed WHERE clk_id = $custLoyaltyKillID");
    }

    private function upateCustLoyaltyKillDB(custLoyaltyKillObject $obj){

        if (!isset($obj->clk_id) || !is_numeric($obj->clk_id) || $obj->clk_id <= 0){
            throw new Exception("custLoyaltyKillObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_cust_loyalty_killed SET
                                clk_biz_id = {$obj->clk_biz_id},
                                clk_cust_id = {$obj->clk_cust_id},
                                clk_cevent_id = {$obj->clk_cevent_id},
                                clk_kill_reason = '{$obj->clk_kill_reason}'
                                WHERE clk_id = {$obj->clk_id}
                                         ");
    }

    private function deleteCustLoyaltyKillByIdDB($custLoyaltyKillID){

        if (!is_numeric($custLoyaltyKillID) || $custLoyaltyKillID <= 0){
            throw new Exception("Illegal value $custLoyaltyKillID");
        }

        $this->db->execute("DELETE FROM tbl_cust_loyalty_killed WHERE clk_id = $custLoyaltyKillID");
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYSENT - PUBLIC           */
    /************************************* */

    /**
     * Insert new custLoyaltySentObject to DB
     * Return Data = new custLoyaltySentObject ID
     * @param custLoyaltySentObject $custLoyaltySentObj
     * @return resultObject
     */
    public function addCustLoyaltySent(custLoyaltySentObject $custLoyaltySentObj){
        try{
            $newId = $this->addCustLoyaltySentDB($custLoyaltySentObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltySentObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get custLoyaltySent from DB for provided ID
     * * Return Data = custLoyaltySentObject
     * @param int $custLoyaltySentId
     * @return resultObject
     */
    public function getCustLoyaltySentByID($custLoyaltySentId){

        try {
            $custLoyaltySentData = $this->loadCustLoyaltySentFromDB($custLoyaltySentId);

            $custLoyaltySentObj = custLoyaltySentObject::withData($custLoyaltySentData);
            $result = resultObject::withData(1,'',$custLoyaltySentObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltySentId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param custLoyaltySentObject $custLoyaltySentObj
     * @return resultObject
     */
    public function updateCustLoyaltySent(custLoyaltySentObject $custLoyaltySentObj){
        try{
            $this->upateCustLoyaltySentDB($custLoyaltySentObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custLoyaltySentObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete custLoyaltySent from DB
     * @param int $custLoyaltySentID
     * @return resultObject
     */
    public function deleteCustLoyaltySentById($custLoyaltySentID){
        try{
            $this->deleteCustLoyaltySentByIdDB($custLoyaltySentID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custLoyaltySentID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTLOYALTYSENT - DB METHODS           */
    /************************************* */

    private function addCustLoyaltySentDB(custLoyaltySentObject $obj){

        if (!isset($obj)){
            throw new Exception("custLoyaltySentObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_loyalty_sent SET
                                        cls_biz_id = {$obj->cls_biz_id},
                                        cls_cust_id = {$obj->cls_cust_id},
                                        cls_cevent_id = {$obj->cls_cevent_id},
                                        cls_granted_entity = '{$obj->cls_granted_entity}',
                                        cls_granted_entity_id = {$obj->cls_granted_entity_id},
                                        cls_channel_sent = '{$obj->cls_channel_sent}',
                                        cls_channel_send_id = {$obj->cls_channel_send_id}
                                                 ");
        return $newId;
    }

    private function loadCustLoyaltySentFromDB($custLoyaltySentID){

        if (!is_numeric($custLoyaltySentID) || $custLoyaltySentID <= 0){
            throw new Exception("Illegal value $custLoyaltySentID");
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_loyalty_sent WHERE cls_id = $custLoyaltySentID");
    }

    private function upateCustLoyaltySentDB(custLoyaltySentObject $obj){

        if (!isset($obj->cls_id) || !is_numeric($obj->cls_id) || $obj->cls_id <= 0){
            throw new Exception("custLoyaltySentObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_cust_loyalty_sent SET
                                cls_biz_id = {$obj->cls_biz_id},
                                cls_cust_id = {$obj->cls_cust_id},
                                cls_cevent_id = {$obj->cls_cevent_id},
                                cls_granted_entity = '{$obj->cls_granted_entity}',
                                cls_granted_entity_id = {$obj->cls_granted_entity_id},
                                cls_channel_sent = '{$obj->cls_channel_sent}',
                                cls_channel_send_id = {$obj->cls_channel_send_id}
                                WHERE cls_id = {$obj->cls_id}
                                         ");
    }

    private function deleteCustLoyaltySentByIdDB($custLoyaltySentID){

        if (!is_numeric($custLoyaltySentID) || $custLoyaltySentID <= 0){
            throw new Exception("Illegal value $custLoyaltySentID");
        }

        $this->db->execute("DELETE FROM tbl_cust_loyalty_sent WHERE cls_id = $custLoyaltySentID");
    }
}