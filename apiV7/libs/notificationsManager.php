<?php
/**
 * Methods for App notifications
 *
 * @version 1.0
 * @author PapTap
 */

class notificationsManager {

    protected $db;

    public function __construct()
    {

        try {
            $this->db = new Database();
            $this->dbAPI = new Database("api");
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }
    }

    public function getNotificationData($bizID)
    {
        $data = $this->db->getTable("select * from tbl_biz_notification
                                                INNER JOIN tbl_biz_notif_params ON noti_param_id=ntp_id
                                                LEFT JOIN tbl_customers ON cust_id = noti_cust_id
                                                where noti_biz_id = $bizID
                                                and noti_new = 1
                                                order by noti_id DESC");

        $this->db->execute("update tbl_biz_notification ,tbl_biz_notif_params
                                                set noti_new=0
                                                where noti_param_id=ntp_id
                                                and noti_biz_id = $bizID
                                                and noti_new = 1
                                                and ntp_type='view'");

        return $data;
    }

    public function clearNotificationByType($bizID,$type){
        $this->db->execute("update tbl_biz_notification
                                                set noti_new=0
                                                where noti_param_id=$type
                                                and noti_biz_id = $bizID
                                                and noti_cust_id=0
                                                and noti_new = 1");
    }

    public static function clearAllNotificationsByTypeForBiz($bizID,$type){
        $instance = new self();
        $instance->db->execute("update tbl_biz_notification
                                                set noti_new=0
                                                where noti_param_id=$type
                                                and noti_biz_id = $bizID
                                                and noti_new = 1");
    }

    public function clearNotificationByOwner($ownerID,$type){

        $this->db->execute("update tbl_biz_notification
                                set noti_new=0
                                where noti_param_id=$type
                                and noti_new = 1
                                and noti_biz_id in (select biz_id from tbl_biz where biz_owner_id=$ownerID)
                                ");
    }

    public static function clearBizNotificationsByTypeAndCustomer($biz_id,$type_id,$customer_id){
        $instance = new self();

        $sql = "UPDATE tbl_biz_notification SET noti_new = 0
                WHERE noti_biz_id = $biz_id
                AND noti_param_id = $type_id
                AND noti_cust_id = $customer_id";

        $instance->db->execute($sql);
    }

    public function getNotificationCount($bizID)
    {
        return $this->db->getVal("select count(noti_id) from tbl_biz_notification
                                    where noti_biz_id = $bizID
                                    and noti_new = 1");
    }

    public function getUnseenNotificationList($bizID)
    {
        return $this->db->getTable("select * from tbl_biz_notification
                                                INNER JOIN tbl_biz_notif_params ON noti_param_id=ntp_id
                                                LEFT JOIN tbl_customers ON cust_id = noti_cust_id
                                                where noti_biz_id = $bizID
                                                and noti_new = 1");
    }

    /**
     * Adding notification to biz
     * $bizID - Id of the app
     * $notiPearamId - enumActions value
     */
    public function addNotification($bizID,$notiPearamId,$unique = false,$params=array(),$sendPush = true){

        $text_translate = $this->db->getRow("select * from tbl_ml_labels where mll_key like 'notification_{$notiPearamId}_text'");
        $owner_lang = $this->db->getRow("SELECT biz_owner_id,owner_id,ac_lang,owner_short_lang FROM tbl_biz
                                            LEFT JOIN tbl_owners ON biz_owner_id = owner_id
                                            LEFT JOIN tbl_account ON owner_account_id = ac_id
                                            WHERE biz_id = $bizID");
        $eManager = new emailManager();
        $notiParams = $eManager->getCustomersSinglePushFromBiz($notiPearamId,$bizID,$owner_lang['owner_id']);

        if(isset($owner_lang['owner_short_lang']) && isset($text_translate['mll_value_'.$owner_lang['owner_short_lang']]) && $text_translate['mll_value_'.$owner_lang['owner_short_lang']] != ''){
            $notiParams["ntp_noti_text"] = $text_translate['mll_value_'.$owner_lang['owner_short_lang']];
        }
        else{
            $notiParams["ntp_noti_text"] = $text_translate['mll_value_en'];
        }


        $notiText = $notiParams["ntp_noti_text"];
        $customerId = isset($params["customer_id"]) ? $params["customer_id"] : 0;
        $external_id = isset($params["external_id"]) ? $params["external_id"] : 0;
        if(isset($params["cust_first_name"]))  $notiText = str_replace ( '#cust_name#' , stripslashes($params["cust_first_name"]) , $notiText);
        if(isset($params["total_billed"]))  $notiText = str_replace ( '#total_billed#' , stripslashes($params["total_billed"]) , $notiText);
        if(isset($params["coupon_header"]))  $notiText = str_replace ( '#coupon_header#' , stripslashes($params["coupon_header"]) , $notiText);
        $notiText = addslashes(stripslashes($notiText));

        $noti_params_csv = "";
        if(count($params) > 0){
            foreach ($params as $key => $value)
            {
            	if($noti_params_csv != ''){
                    $noti_params_csv .= ",";
                }
                $key = addslashes($key);
                $value = addslashes($value);
                $noti_params_csv .= "$key|$value";
            }

        }

        if($notiParams["ntp_act_type"] != "push_only"){
            if($unique){
                $exist = $this->db->getVal("select noti_id
                                        from tbl_biz_notification
                                        where noti_biz_id = $bizID
                                        and noti_param_id = $notiPearamId
                                        and noti_ext_id = $external_id");
                if($exist != "")
                {
                    $this->db->execute("update tbl_biz_notification set
                                    noti_new = 1,
                                    noti_date = now(),
                                    noti_params = '$noti_params_csv',
                                    noti_cust_id = $customerId,
                                    noti_ext_id = $external_id
                                    where noti_id = $exist
                                    ");
                }
                else{
                    $this->db->execute("insert into tbl_biz_notification set
                                    noti_biz_id = $bizID,
                                    noti_param_id = $notiPearamId,
                                    noti_text = '$notiText',
                                    noti_params = '$noti_params_csv',
                                    noti_cust_id = $customerId,
                                    noti_ext_id = $external_id,
                                    noti_old_format = 0
                                    ");
                }
            }
            else{
                $this->db->execute("insert into tbl_biz_notification set
                                    noti_biz_id = $bizID,
                                    noti_param_id = $notiPearamId,
                                    noti_text = '$notiText',
                                    noti_params = '$noti_params_csv',
                                    noti_cust_id = $customerId,
                                    noti_ext_id = $external_id,
                                    noti_old_format = 0
                                    ");
            }
        }


        if($notiParams["ntp_email_comp"] != 0) emailManager::sendSystemMailApp($bizID,$notiParams["ntp_email_comp"],enumEmailType::SystemMailSGCare,$params);

        if($notiParams["ntp_push_text"] != ""){

            $bizModel = new bizModel($bizID);

            $bizObject = $bizModel->getBiz()->data;

            if($notiParams["ntpo_push_text"] != ""){
                $repTEXT =$notiParams['ntpo_push_text'];
            }else{
                $push_translate = $this->db->getRow("select * from tbl_ml_labels where mll_key = 'notification_{$notiPearamId}_push'");
                $repTEXT = $notiParams["ntp_push_text"];
                if($owner_lang['ac_lang'] != "" && $push_translate['mll_value_'.$owner_lang['ac_lang']] != ""){
                    $repTEXT = $push_translate['mll_value_'.$owner_lang['ac_lang']];
                }else{
                    if($push_translate['mll_value_en'] != ""){
                        $repTEXT =$push_translate['mll_value_en'];
                    }
                }
            }

            if(isset($params["cust_first_name"]))  $repTEXT = str_replace ( '#cust_name#' , stripslashes($params["cust_first_name"]) , $repTEXT);
            if(isset($params["total_billed"]))  $repTEXT = str_replace ( '#total_billed#' , stripslashes($params["total_billed"]) , $repTEXT);
            if(isset($params["coupon_header"]))  $repTEXT = str_replace ( '#coupon_header#' , stripslashes($params["coupon_header"]) , $repTEXT);

            foreach ($params as $key => $value)
            {
                $repTEXT = str_replace ( "#$key#" , stripslashes($value) , $repTEXT);
            }

            if($sendPush){
                switch ($notiPearamId)
                {
                    case enumActions::newInstallation:
                    case enumActions::confirmMail:
                    case enumActions::accountBilled:
                    case enumActions::wrongPaymentDetails:
                    case enumActions::productStockLow:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_messageOnly,$repTEXT,0,$params);
                        break;
                    case enumActions::newCustomer:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_newClubMember,$repTEXT,0,$params);
                        break;
                    case enumActions::newOrder:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_newOrder,$repTEXT,0,$params);
                        break;
                    case enumActions::orderHandlerChanged:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_newOrderAssign,$repTEXT,$params["direct_account_id"],$params);
                        break;
                    case enumActions::newCatMessage:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_chat,$repTEXT,0,$params);
                        break;
                    case enumActions::newMeeting:
                        $pushType = enumPushType::admin_newMeeting;
                        if($bizObject->getBizBusinessType() != enumBusinessTypes::service_providers){
                            $pushType = enumPushType::admin_messageOnly;
                        }

                        pushManager::sendAdminPositionsPush($bizID,$pushType,$repTEXT,$params["direct_account_id"],$params);
                        break;
                    case enumActions::updatedMeeting:
                        $pushType = enumPushType::admin_updateMeeting;
                        if($bizObject->getBizBusinessType() != enumBusinessTypes::service_providers){
                            $pushType = enumPushType::admin_messageOnly;
                        }

                        pushManager::sendAdminPositionsPush($bizID,$pushType,$repTEXT,$params["direct_account_id"],$params);
                        break;
                    case enumActions::cancelledMeeting:
                        $pushType = enumPushType::admin_cencelMeeting;
                        if($bizObject->getBizBusinessType() != enumBusinessTypes::service_providers){
                            $pushType = enumPushType::admin_messageOnly;
                        }

                        pushManager::sendAdminPositionsPush($bizID,$pushType,$repTEXT,$params["direct_account_id"],$params);
                        break;
                    case enumActions::newCustomForm:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_newFormFilled,$repTEXT,0,$params);
                        break;
                    case enumActions::newDocument:
                        pushManager::sendAdminPositionsPush($bizID,enumPushType::admin_newDocument,$repTEXT,0,$params);
                        break;
                }
            }

        }
    }

    public function confirm_mail($post){

        emailManager::sendSystemMailApp($_SESSION["appData"]["biz_id"],127,enumEmailType::SystemMailSGCare);
    }

    public function generateRules(){
        $data = $this->db->getTable("select * from tbl_email_rules where rule_active=1");
        if(count($data>0)){

            $billing = new billingManager();

            foreach ($data as $oneRule)
            {
            	$users = $this->db->getTable($oneRule["rule_conditions_sql"]);
                if(count($users>0)){
                    foreach ($users as $oneUser)
                    {
                        if($oneRule["rule_receiver_type"] == "owner"){
                            if($oneRule["rule_send_type"] == "email"){
                                emailManager::sendSystemMailOwner($oneUser["owner_id"],$oneRule["rule_campaign_id"],enumEmailType::SystemMailSGCare);
                                $this->addToBizRules($oneUser["owner_id"],$oneRule["rule_id"]);
                            }
                        }

                        if($oneRule["rule_receiver_type"] == "biz"){
                            if($oneRule["rule_send_type"] == "email"){

                                $nextBillingDate = $billing->getNextBillingDate($oneUser["biz_id"]);
                                $data = $billing->getMonthlyPlans($nextBillingDate, $oneUser["biz_id"],2);
                                $plansName = "";
                                if(count($data)>0){
                                    $plansName = $data[0]['name'];
                                }
                                $extraParams["plan"] = $plansName;
                                emailManager::sendSystemMailApp($oneUser["biz_id"],$oneRule["rule_campaign_id"],enumEmailType::SystemMailSGCare,$extraParams);
                                $this->addToBizRules($oneUser["biz_id"],$oneRule["rule_id"]);
                            }
                        }
                    }
                }
            }

        }
    }

    private function addToBizRules($resiverID,$ruleID){
        $this->db->execute("insert into tbl_biz_rules set bru_received_id=$resiverID,bru_rule_id=$ruleID");
    }

    /** Info banners  **/

    /* biz banner */
    public static function addInfoBannerToBiz($biz_id,$banner_id,$redirect_params = "",$label_params = ""){
        //Priority high - 3, medium - 2, low - 1
        $instance = new self();

        $sql = "SELECT ibanner_single_show FROM tbl_info_banner WHERE ibanner_id = $banner_id";

        $onlySingle = $instance->db->getVal($sql);

        $result = array();
        if($onlySingle == 1 && $instance::hasBizBannerOfType($biz_id,$banner_id)){
            $result['code'] = 0;
            return $result;
        }

        if($label_params != ''){
            $label_params = addslashes($label_params);
        }

        $sql = "INSERT INTO tbl_biz_info_banner SET
                    bibanner_biz_id = $biz_id,
                    bibanner_banner_id = $banner_id,
                    bibanner_redirect_params = '$redirect_params',
                    bibanner_label_params = '$label_params'";

        $instance->db->execute($sql);



        $result['code'] = 1;

        if(isset($_SESSION['appData']['biz_id']) && $_SESSION['appData']['biz_id'] == $biz_id){
            $banners = self::getAllBizInfoBanners($_SESSION['appData']['biz_id']);

            if($banners['code'] == 1){
                $_SESSION['appData']['biz_info_banners'] = $banners['data'];
            }
        }

        return $result;
    }

    public static function getFirstBizInfoBanner($biz_id){
        $instance = new self();
        $sql = "SELECT *,bibanner_id as banner_id,bibanner_redirect_params as redirect_param,'biz' as banner_source FROM tbl_biz_info_banner,tbl_info_banner
                WHERE bibanner_banner_id = ibanner_id
                AND bibanner_biz_id = $biz_id
                AND bibanner_status = 'unread'
                ORDER BY ibanner_priority,bibanner_date DESC
                LIMIT 1";

        $banner = $instance->db->getRow($sql);
        $result = array();

        if(isset($banner['bibanner_id'])){
            $result['code'] = 1;
            $result['data'] = $banner;
        }
        else{
            $result['code'] = 0;
        }
        return $result;
    }

    public static function getAllBizInfoBanners($biz_id){
        $instance = new self();
        $sql = "SELECT *,bibanner_id as banner_id,bibanner_redirect_params as redirect_param,'biz' as banner_source FROM tbl_biz_info_banner,tbl_info_banner
                WHERE bibanner_banner_id = ibanner_id
                AND bibanner_biz_id = $biz_id
                AND bibanner_status = 'unread'
                ORDER BY ibanner_priority,bibanner_date DESC";

        $banners = $instance->db->getTable($sql);
        $result = array();

        if(count($banners) > 0){
            $result['code'] = 1;
            $result['data'] = $banners;
        }
        else{
            $result['code'] = 0;
        }
        return $result;
    }

    public static function setBizInfoBannerStatus($bib_id,$status){
        $instance = new self();
        $sql = "UPDATE tbl_biz_info_banner SET
                    bibanner_status = '$status',
                    bibanner_status_date = NOW()
                WHERE bibanner_id = $bib_id";

        $instance->db->execute($sql);

        $result = array();

        $result['code'] = 1;

        return $result;
    }

    public static function removeBizInfoBannerByID($bib_id){
        $instance = new self();
        $sql = "DELETE FROM tbl_biz_info_banner WHERE bibanner_id = $bib_id";

        $instance->db->execute($sql);

        $result = array();
        $result['code'] = 1;

        return $result;
    }

    public static function removeBizInfoBannerByBannerTypeID($biz_id,$banner_id){
        $instance = new self();
        $sql = "DELETE FROM tbl_biz_info_banner
                WHERE bibanner_banner_id = $banner_id
                AND bibanner_biz_id = $biz_id";

        $instance->db->execute($sql);

        if(isset($_SESSION['appData']['biz_info_banners'])){
            foreach ($_SESSION['appData']['biz_info_banners'] as $key => $value)
            {
            	if($value['ibanner_id'] == $banner_id){
                    unset($_SESSION['appData']['biz_info_banners'][$key]);
                }
            }

        }

        $result = array();
        $result['code'] = 1;

        return $result;
    }

     public static function hasBizBannerOfType($biz_id,$banner_id){
        $instance = new self();

        $sql = "SELECT COUNT(*) FROM tbl_biz_info_banner
                WHERE bibanner_banner_id = $banner_id
                AND bibanner_biz_id = $biz_id";

        $count = $instance->db->getVal($sql);

        return $count > 0;
    }

    /* reseller banners */

    public static function addInfoBannerToReseller($reseller_id,$banner_id,$redirect_params = ""){
        //Priority high - 3, medium - 2, low - 1
        $instance = new self();
        $sql = "INSERT INTO tbl_reseller_info_banner SET
                    ribanner_reseller_id = $reseller_id,
                    ribanner_banner_id = $banner_id,
                    ribanner_redirect_params = '$redirect_params'";

        $instance->db->execute($sql);

        $result = array();

        $result['code'] = 1;

        if(isset($_SESSION['rid']) && $_SESSION['rid'] == $biz_id){
            $banners = self::getAllResellerInfoBanner($_SESSION['rid']);

            if($banners['code'] == 1){
                $_SESSION['reseller_banners'] = $banners['data'];
            }
        }

        return $result;
    }

    public static function getFirstResellerInfoBanner($reseller_id){
        $instance = new self();
        $sql = "SELECT *,ribanner_id as banner_id,ribanner_redirect_params as redirect_param,'reseller' as banner_source FROM tbl_reseller_info_banner,tbl_info_banner
                WHERE ribanner_banner_id = ibanner_id
                AND ribanner_reseller_id = $reseller_id
                AND ribanner_status = 'unread'
                ORDER BY ibanner_priority,ribanner_date DESC
                LIMIT 1";

        $banner = $instance->db->getRow($sql);
        $result = array();

        if(isset($banner['ribanner_id'])){
            $result['code'] = 1;
            $result['data'] = $banner;
        }
        else{
            $result['code'] = 0;
        }
        return $result;
    }

    public static function getAllResellerInfoBanner($reseller_id){
        $instance = new self();
        $sql = "SELECT *,ribanner_id as banner_id,ribanner_redirect_params as redirect_param,'reseller' as banner_source FROM tbl_reseller_info_banner,tbl_info_banner
                WHERE ribanner_banner_id = ibanner_id
                AND ribanner_reseller_id = $reseller_id
                AND ribanner_status = 'unread'
                ORDER BY ibanner_priority,ribanner_date DESC";

        $banners = $instance->db->getTable($sql);
        $result = array();

        if(count($banners) > 0){
            $result['code'] = 1;
            $result['data'] = $banners;
        }
        else{
            $result['code'] = 0;
        }
        return $result;
    }

    public static function setResellerInfoBannerStatus($rib_id,$status){
        $instance = new self();
        $sql = "UPDATE tbl_reseller_info_banner SET
                    ribanner_status = '$status',
                    ribanner_status_date = NOW()
                WHERE ribanner_id = $rib_id";

        $instance->db->execute($sql);

        if(isset($_SESSION['reseller_banners'])){
            foreach ($_SESSION['reseller_banners'] as $key => $value)
            {
            	if($value['ib_banner_id'] == $banner_id){
                    unset($_SESSION['reseller_banners'][$key]);
                }
            }

        }

        $result = array();

        $result['code'] = 1;

        return $result;
    }

    public static function removeResellerInfoBannerByID($rib_id){
        $instance = new self();
        $sql = "DELETE FROM tbl_reseller_info_banner WHERE ribanner_id = $rib_id";

        $instance->db->execute($sql);

        $result = array();
        $result['code'] = 1;

        return $result;
    }

    public static function removeResellerInfoBannerByBannerTypeID($reseller_id,$banner_id){
        $instance = new self();
        $sql = "DELETE FROM tbl_reseller_info_banner
                WHERE ribanner_banner_id = $banner_id
                AND ribanner_reseller_id = $reseller_id";

        $instance->db->execute($sql);

        if(isset($_SESSION['reseller_banners'])){
            foreach ($_SESSION['reseller_banners'] as $key => $value)
            {
            	if($value['ibanner_id'] == $banner_id){
                    unset($_SESSION['reseller_banners'][$key]);
                }
            }

        }

        $result = array();
        $result['code'] = 1;

        return $result;
    }

    public static function hasResellerBannerOfType($reseller_id,$banner_id){
        $instance = new self();

        $sql = "SELECT COUNT(*) FROM tbl_reseller_info_banner
                WHERE ribanner_banner_id = $banner_id
                AND ribanner_reseller_id = $reseller_id";

        $count = $instance->db->getVal($sql);

        return $count > 0;
    }

}

?>
