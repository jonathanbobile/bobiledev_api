<?php

/**
 * partnersManager short summary.
 *
 * partnersManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class partnersManager extends Manager
{
    public static function getPartnerCurrentPlan($resellerID){
        $instance = new self();

        return $instance->db->getRow("SELECT * FROM tbl_transaction_feature,tbl_plans
                                        WHERE tf_entity_id = pl_id
                                        AND tf_reseller_id=$resellerID
                                        AND tf_entity='plan'
                                        AND tf_end_time IS NULL");
    }

    public static function getPartnerByPublicUniqueID($puID){
        try{
            $instance = new self();

            $partnerRow = $instance->db->getRow("SELECT * FROM tbl_account,tbl_owners,tbl_resellers
                                        WHERE ac_id = owner_account_id
                                        AND owner_reseller_id = reseller_id
                                        AND ac_public_unique_id = '$puID'");

            $partner = resellerObject::withData($partnerRow);


            return resultObject::withData(1,'',$partner);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$puID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getPartnersPrivateKey(resellerObject $partner){
        try{
            $acUID = $partner->resellers_account->ac_public_unique_id;

            $bucket = RESELLER_KEYS_BUCKET;

            $result = file_get_contents("https://storage.googleapis.com/$bucket/resellers/keys/$acUID.pem");

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($partner));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function validatePartnerJWTToken($accountUniqueID,$token){
        try{
            $bucket = RESELLER_KEYS_BUCKET;

            $privateKey = file_get_contents("https://storage.googleapis.com/$bucket/resellers/keys/$accountUniqueID.pem");

            return encryptionManager::isTokenSignatureValid($token,$privateKey);
        }
        catch(Exception $e){
            $data = array();
            $data['accountUniqueID'] = $accountUniqueID;
            $data['token'] = $token;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function issuePartnerJWTToken(resellerObject $partner){
        try{
            $acUID = $partner->resellers_account->ac_public_unique_id;

            $bucket = RESELLER_KEYS_BUCKET;

            $privateKey = file_get_contents("https://storage.googleapis.com/$bucket/resellers/keys/$acUID.pem");

            $payload = array();

            $payload['iss'] = $acUID;
            $payload['kid'] = $partner->reseller_public_rsa_key;
            $payload['iat'] = time();

            $token = encryptionManager::createJWTToken($payload,$privateKey);

            return resultObject::withData(1,'',$token);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($partner));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function checkIfVerifiedMarketDomain($domain){
        $instance = new self();

        if(utilityManager::contains($domain,"bobile.com")){
            return true;
        }

        return $instance->db->getVal("SELECT COUNT(*) FROM tbl_resellers
                                WHERE reseller_wl_market_domain LIKE '%$domain%'") > 0;
    }
}