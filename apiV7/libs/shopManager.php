<?php
/**
 * Methods for Utility Functions
 *
 * @version 1.0
 * @author PapTap
 */

class shopManager extends Manager
{ 

    function __construct()
    {
        parent::__construct();        
    }   

    /* Products */

    public function addProduct(productObject $product){
        try{
             $levelManager = new levelDataManager(9);
             $result = $levelManager->addLevelData($product);
             //To do - add attributes
             //To do - add attribute values
             //To do - add variations
             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($product));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getProductByProductID($productID){
        try{
            $levelManager = new levelDataManager(9);
            $productData = $levelManager->getLevelDataDirectByID($productID);
            
            $prdocutObj = productObject::withData($productData->data);

            //To do - get attributes
             //To do - get attribute values
             //To do - get variations

            return resultObject::withData(1,'',$prdocutObj);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$productID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updateProduct(productObject $product){
        try{
             $levelManager = new levelDataManager(9);
             $result = $levelManager->updateLevelData($product);
             //To do - update attributes
             //To do - update attribute values
             //To do - update variations
             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($product));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteProduct($productID){
        try{
             $levelManager = new levelDataManager(9);

             //To do - delete attributes
             //To do - delete attribute values
             //To do - delete variations

             $result = $levelManager->deleteLevelDataById($productID);
             
             return resultObject::withData(1);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$productID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function reduceProductStock($productID,$qty){
        try{
            $levelManager = new levelDataManager(9);
            $productData = $levelManager->getLevelDataDirectByID($productID);

            $prdocutObj = productObject::withData($productData->data);

            $prdocutObj->md_qty_instock -= $qty;

            $levelManager->updateLevelData($prdocutObj);

            if($prdocutObj->md_qty_instock <= $prdocutObj->md_safety_stock){
                $this->sendLowStockAlert($prdocutObj);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['productID'] = $productID;
            $data['qty'] = $qty;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function sendLowStockAlert(levelData9Object $product){

        $extraParams = array();
        $extraParams["product_name"] = $product->md_head;
        $extraParams["product_current_qty"] = $product->md_qty_instock;
        $extraParams["product_safe_qty"] = $product->md_safety_stock;
        $extraParams["product_id"] = $product->md_row_id;
        $extraParams["external_id"] = $product->md_row_id;

        $notificationsManager = new notificationsManager();

        $notificationsManager->addNotification($product->md_biz_id,enumActions::productStockLow,true,$extraParams);

        return;
    }

     /* Categories - CRUD */

    public function addCategory(categoryObject $category){
        try{
             $levelManager = new levelDataManager(999);
             $result = $levelManager->addLevelData($category);
            
             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($category));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getCategoryByCategoryID($categoryID){
        try{
            $levelManager = new levelDataManager(999);
            $categoryData = $levelManager->getLevelDataDirectByID($categoryID);
            
            $categoryObj = categoryObject::withData($categoryData->data);

            return resultObject::withData(1,'',$categoryObj);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$categoryID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function updateCategory(categoryObject $category){
        try{
             $levelManager = new levelDataManager(999);
             $result = $levelManager->updateLevelData($category);

             return $result;
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($category));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function deleteCategory($categoryID){
        try{
             $levelManager = new levelDataManager(999);

             $result = $levelManager->deleteLevelDataById($categoryID);
             
             return resultObject::withData(1);
        }
        catch(Exception $e){
             errorManager::addAPIErrorLog('API Model',$e->getMessage(),$categoryID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

     /* Categories - advanced */

    /**
     * Summary of getCategoriesForBizByBizID
     * @param mixed $bizID 
     * @return resultObject
     */
    public function getCategoriesForBizByBizID($bizID){
        try{
            $categoriesRows = $this->getCategoryRowsByBizID($bizID);

            $categories = array();
            foreach ($categoriesRows as $entry)
            {
            	$category = categoryObject::withData($entry);

                $categoryProductIDs = $this->getProductIDsinCategoryByCategoryID($category->md_row_id);

                $category->setProductIDs($categoryProductIDs);
                
                $categories[] = $category;
            }


            return resultObject::withData(1,'',$categories);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getProductsInCategoryByCategoryID($categoryID){
        try{
            $productsList = $this->getProductIDsinCategoryByCategoryID($categoryID);

            return resultObject::withData(1,'',$productsList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getCategoriesForProduct($productID){
        try{
            $instance = new self();
            $categoriesList = $instance->getCategoryIDsForProductID($productID);

            return resultObject::withData(1,'',$categoriesList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    private function getCategoryRowsByBizID($bizID){
        $sql = "SELECT * FROM tbl_mod_data999
                WHERE md_biz_id = $bizID
                ORDER BY md_index ASC";

        return $this->db->getTable($sql);
    }

    private function getProductIDsinCategoryByCategoryID($categoryID){
        $sql = "SELECT product_id FROM ecommerce_categories_products
                WHERE category_id = $categoryID
                ORDER BY product_order ASC";

        $productsIDs = $this->db->getTable($sql);

        $result = array();

        foreach ($productsIDs as $entry)
        {
            $result[] = $entry['product_id'];
        }

        return $result;
    }

    private function getCategoryIDsForProductID($productID){
        $sql = "SELECT category_id FROM ecommerce_categories_products
                WHERE product_id = $productID
                ORDER BY product_order ASC";

        $categoriesIDs = $this->db->getTable($sql);

        $result = array();

        foreach ($categoriesIDs as $entry)
        {
            $result[] = $entry['category_id'];
        }

        return $result;
    }

    /************************************* */
    /*   BASIC SHIPPINGMETHOD - PUBLIC           */
    /************************************* */

    /**
    * Insert new shippingMethodObject to DB
    * Return Data = new shippingMethodObject ID
    * @param shippingMethodObject $shippingMethodObj 
    * @return resultObject
    */
    public function addShippingMethod(shippingMethodObject $shippingMethodObj){       
        try{
            $newId = $this->addShippingMethodDB($shippingMethodObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($shippingMethodObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get shippingMethod from DB for provided ID
    * * Return Data = shippingMethodObject
    * @param int $shippingMethodId 
    * @return resultObject
    */
    public function getShippingMethodByID($shippingMethodId){
        
        try {
            $shippingMethodData = $this->loadShippingMethodFromDB($shippingMethodId);
            
            $shippingMethodObj = shippingMethodObject::withData($shippingMethodData);
            $result = resultObject::withData(1,'',$shippingMethodObj);
            return $result;
          }
          //catch exception
          catch(Exception $e) {
              errorManager::addAPIErrorLog('API Model',$e->getMessage(),$shippingMethodId);
              $result = resultObject::withData(0,$e->getMessage());
              return $result;
          }
    }

    /**
    * Update shippingMethod in DB
    * @param shippingMethodObject $shippingMethodObj 
    * @return resultObject
    */
    public function updateShippingMethod(shippingMethodObject $shippingMethodObj){        
        try{
            $this->upateShippingMethodDB($shippingMethodObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($shippingMethodObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete shippingMethod from DB
    * @param int $shippingMethodID 
    * @return resultObject
    */
    public function deleteShippingMethodById($shippingMethodID){
        try{
            $this->deleteShippingMethodByIdDB($shippingMethodID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$shippingMethodID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC SHIPPINGMETHOD - DB METHODS */
    /************************************* */

    private function addShippingMethodDB(shippingMethodObject $obj){

        if (!isset($obj)){
            throw new Exception("shippingMethodObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_ship_method SET 
        ship_method_region_id = {$obj->ship_method_region_id},
        ship_method_name = '".addslashes($obj->ship_method_name)."',
        ship_method_rate = {$obj->ship_method_rate},
        ship_method_type = '{$obj->ship_method_type}',
        ship_method_time = '".addslashes($obj->ship_method_time)."'
                 ");
         return $newId;
    }

    private function loadShippingMethodFromDB($shippingMethodID){

        if (!is_numeric($shippingMethodID) || $shippingMethodID <= 0){
            throw new Exception("Illegal value shippingMethodID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_ship_method WHERE ship_method_id = $shippingMethodID");
    }

    private function upateShippingMethodDB(shippingMethodObject $obj){

        if (!isset($obj->ship_method_id) || !is_numeric($obj->ship_method_id) || $obj->ship_method_id <= 0){
            throw new Exception("shippingMethodObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_ship_method SET 
        ship_method_region_id = {$obj->ship_method_region_id},
        ship_method_name = '".addslashes($obj->ship_method_name)."',
        ship_method_rate = {$obj->ship_method_rate},
        ship_method_type = '{$obj->ship_method_type}',
        ship_method_time = '".addslashes($obj->ship_method_time)."'
        WHERE ship_method_id = {$obj->ship_method_id} 
                 ");
    }

    private function deleteShippingMethodByIdDB($shippingMethodID){

        if (!is_numeric($shippingMethodID) || $shippingMethodID <= 0){
            throw new Exception("Illegal value shippingMethodID");             
        }

        $this->db->execute("DELETE FROM tbl_ship_method WHERE ship_method_id = $shippingMethodID");
    }

    private function getShippingMethodsByRegionID($regionID){
        $sql = "select * from tbl_ship_method WHERE ship_method_region_id = $regionID";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*           IMPORT FUNCTIONS          */
    /************************************* */

    public static function addProducts($bizId,$data = array()){

        $levelDataManager = new levelDataManager(9);

        if(count($data) > 0){
            foreach ($data as $product)
            {
            	$prodObject = self::getProductObjectFromArray($product);
                $prodObject->md_biz_id=$bizId;

                $result = $levelDataManager->addLevelData($prodObject);
                if($result->code == 1){

                    $prodObject->md_row_id = $result->data;

                    if(count($product["images"]) > 0){
                        for($i = 0; $i<count($product["images"]);$i++){
                            if($i == 0){
                                $prodObject->md_icon = $product["images"][$i]["src"];
                                $levelDataManager::addImageToQueue($prodObject,$result->data,"md_icon");
                            }else{
                                $field = "md_pic$i";
                                $prodObject->$field = $product["images"][$i]["src"];
                                $levelDataManager::addImageToQueue($prodObject,$result->data,$field);
                            }
                        }
                    }else{
                        $bizModel = new bizModel($bizId);
                        $bizResult = $bizModel->getBiz();
                        $prodObject->md_icon = $bizResult->data->biz_logo;
                        $levelDataManager->updateLevelData($prodObject);
                    }

                    self::addCategoriesToProduct($product["categories"],$prodObject);
                }
            }
            
            $result = resultObject::withData(1,"Ok");
            return $result;

        }else{
            $result = resultObject::withData(0,"No products collection provided","MISSING_PRODUCTS");
            return $result;
        }
    }

    /**
     * Summary of getProductObjectFromArray
     * @param mixed $product 
     * @return levelData9Object
     */
    private static function getProductObjectFromArray($product){

        $prodObject = new levelData9Object();

        $prodObject->md_external_id = $product["id"];
        $prodObject->md_head = $product["name"];

        $description = $product["short_description"]."\n".$product["description"];
        $description = strip_tags($description);
        $prodObject->md_desc = $description;
        $prodObject->md_element = "23";
        $prodObject->md_info1 = $product["sku"];
        $prodObject->md_weight = trim($product["weight"]) == "" || !is_numeric(trim($product["weight"])) ? 0 : trim($product["weight"]);
        $prodObject->md_original_price = trim($product["regular_price"]) == "" ? 0 : trim($product["regular_price"]);
        $prodObject->md_price = trim($product["price"]) == "" ? 0 : trim($product["price"]);
        if($prodObject->md_original_price != $prodObject->md_price){
            $prodObject->md_discount_amount = $prodObject->md_original_price - $prodObject->md_price;
            $prodObject->md_has_discount = 1;
        }
        $prodObject->md_bool2 = 0;
        $prodObject->md_featured = $product["featured"] ? 1 : 0;
        $prodObject->md_visible_element = $product["catalog_visibility"] == "visible" ? 1 : 0;
        $prodObject->md_discount_type = "currency";
        return $prodObject;
    }

    private static function addCategoriesToProduct($categories,levelData9Object $product){

        if(count($categories)>0){
            foreach ($categories as $category)
            {
                if($category["name"] != ""){
                    $categoryId = self::getCategoryByName($category["name"],$product);
                    self::connectProductToCategory($product->md_row_id,$categoryId);
                }
            }        
        }
    }

    private static function getCategoryByName($categoryName,levelData9Object $product){

        $instance = new self();
        $dbCategoryName = addslashes(trim($categoryName));

        $categoryId = $instance->db->getVal("SELECT md_row_id FROM tbl_mod_data999 
                                        WHERE md_biz_id={$product->md_biz_id} 
                                        AND md_head='$dbCategoryName'");

        if($categoryId == ""){
            $categoryId = self::createCategory($dbCategoryName,$product->md_biz_id);
        }

        return $categoryId;
    }

    private static function createCategory($categoryName,$bizId){

        $instance = new self();

        return $instance->db->execute("
            INSERT INTO tbl_mod_data999 SET
                   md_head = '$categoryName', 
                   md_biz_id = $bizId,
                   md_element = 24
            ");
    }

    private static function connectProductToCategory($productId,$categoryId){

        $instance = new self();

        $instance->db->execute("
            INSERT IGNORE INTO ecommerce_categories_products SET
                   category_id = $categoryId, 
                   product_id = $productId
            ");
    }

    /************************************* */
    /*         NON OBJECT FUNCTIONS        */
    /************************************* */

    public function getItemTypeByModId($modId){
        return $this->db->getRow("SELECT it_id,it_need_shipment FROM tbl_item_type WHERE it_mod_id = $modId");
    }

    public function getActiveVariationsForProduct($product_id) {

        $q = "SELECT * FROM ecommerce_variations WHERE attributes_values_ids <> '' and product_id = $product_id AND active = 1";
        $data = $this->db->getTable($q);
        $variations = array();
        if(count($data) > 0){
            $i=0;
            foreach ($data as $variation) {
                $variations[$i]['id'] = $variation['id'];
                $variations[$i]['surcharge'] = $variation['surcharge'];
                $variations[$i]['sku' ] = $variation['sku'];
                $variations[$i]['active'] = $variation['active'];
                $variations[$i]['values'] = $variation['attributes_values'];
                $variations[$i]['values_ids'] = $variation['attributes_values_ids'];
                $variations[$i]['attributes'] = $variation['attributes_ids'];
                $variations[$i]['product_id'] = $variation['product_id'];
                $i++;
            }
        }
        return $variations;
    }

    public function getVariationByID($variationID){
        $q = "SELECT * FROM ecommerce_variations WHERE id = $variationID";
        
        return productVariationObject::withData($this->db->getRow($q));
    }

    /**
     * Returns variations for the biz app (not the admin app!)
     * @param mixed $product_id 
     * @return array[]
     */
    public function getAllVariationsForProduct($product_id) {

        $q = "SELECT * FROM ecommerce_variations WHERE attributes_values_ids <> '' and product_id = $product_id AND active = 1";

        $data = $this->db->getTable($q);
        $variations = array();
        if(count($data) > 0){
            $i=0;
            foreach ($data as $variation) {
                $variations[$i]['id'] = $variation['id'];
                $variations[$i]['surcharge'] = $variation['surcharge'];
                $variations[$i]['sku' ] = $variation['sku'];
                $variations[$i]['active'] = $variation['active'];
                $variations[$i]['values'] = $variation['attributes_values'];
                $variations[$i]['values_ids'] = $variation['attributes_values_ids'];
                $variations[$i]['attributes'] = $variation['attributes_ids'];
                $variations[$i]['product_id'] = $variation['product_id'];
                $i++;
            }
        }
        return $variations;
    }

    public function getAttributeValues($attrID, $attrPos) {
        $values = $this->db->getTable("SELECT * FROM ecommerce_attributes_values WHERE attribute_id = $attrID");
        $index = 0;
        $attribute = Array();
        if(count($values) > 0){
            foreach ($values as $oneValue)
            {
                $attribute[$index]['id'] = $oneValue['id'];
                $attribute[$index]['name'] = $oneValue['name'];
                $attribute[$index]['data'] = $oneValue['data'];
                $attribute[$index]['pos'] = $attrPos;
                $index++;
            }
        }
        return $attribute;
    }

    public function calculateShipping($bizID,$country,$tax, $items){
        $orderTotal = 0;
        $weightTotal = 0;
        $returnArray = array();
        foreach($items as $oneObject){
            
            $price = $oneObject->totalPrice;        //Change
            $quantity = $oneObject->quantity;
            
            $totalRow = $quantity * $price;
            $orderTotal += $totalRow;
            
            $weight = $this->db->getVal("SELECT md_weight FROM tbl_mod_data9 WHERE md_row_id = " . $oneObject->recordId);
            if($weight == "") $weight = 0;
            $weightTotal += $quantity * $weight;
        }
        
        $orderTotal += $tax;
        
        $regions = $this->db->getTable("select * from tbl_ship_region WHERE ship_region_biz_id = $bizID");
        
        $index=0;
        $haveRegion = false;
        if(count($regions) > 0){
            
            foreach ($regions as $oneRegion)
            {               
            	$countries = explode(",", $oneRegion["ship_region_dest"]);
                if(in_array($country, $countries)) $haveRegion = true;
            }

            if(!$haveRegion) $country = -1;

            foreach ($regions as $oneRegion)
            {               
            	$countries = explode(",", $oneRegion["ship_region_dest"]);
                if(in_array($country, $countries)){
                    
                    $regionID = $oneRegion["ship_region_id"];        
                    $methods = $this->getShippingMethodsByRegionID($regionID);
                    
                    if(count($methods)>0){
                        
                        foreach ($methods as $oneMethod)
                        {
                            switch($oneMethod["ship_method_type"]){
                                case "free":
                                case "fixed":
                                    $returnArray[$index] = $this->shippingType($oneMethod["ship_method_id"],$oneMethod["ship_method_name"],$oneMethod["ship_method_rate"],$oneMethod["ship_method_time"]);
                                    $index++;
                                    break;
                                case "by_weight":
                                    $ranges = $this->db->getTable("select * from tbl_ship_method_range WHERE ship_method_range_method_id = {$oneMethod["ship_method_id"]}");
                                    if(count($ranges)>0){
                                        foreach ($ranges as $oneRange)
                                        {
                                            $maxValue = ($oneRange["ship_method_range_to"] == "") ? 9999999999 : $oneRange["ship_method_range_to"];
                                            if($weightTotal >= $oneRange["ship_method_range_from"] && $weightTotal < $maxValue){
                                                $returnArray[$index] = $this->shippingType($oneMethod["ship_method_id"],$oneMethod["ship_method_name"],$oneRange["ship_method_range_price"],$oneMethod["ship_method_time"]);
                                                $index++;
                                            }
                                        }
                                    }
                                    break;
                                case "by_price":
                                    $ranges = $this->db->getTable("select * from tbl_ship_method_range WHERE ship_method_range_method_id = {$oneMethod["ship_method_id"]}");
                                    if(count($ranges)>0){
                                        foreach ($ranges as $oneRange)
                                        {
                                            $maxValue = ($oneRange["ship_method_range_to"] == "") ? 9999999999 : $oneRange["ship_method_range_to"];
                                            if($orderTotal >= $oneRange["ship_method_range_from"] && $orderTotal < $maxValue){
                                                $returnArray[$index] = $this->shippingType($oneMethod["ship_method_id"],$oneMethod["ship_method_name"],$oneRange["ship_method_range_price"],$oneMethod["ship_method_time"]);
                                                $index++;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            
            return resultObject::withData(1,'',$returnArray);
            
        }
        else{          
            return resultObject::withData(1,'',$returnArray);
        }
    }

    private function shippingType($id=0,$name="",$price=0,$time=""){
        
        $returnArray["id"] = $id;
        $returnArray["name"] = $name;
        $returnArray["price"] = $price;
        $returnArray["time"] = $time;
        
        return $returnArray;
    }
}
