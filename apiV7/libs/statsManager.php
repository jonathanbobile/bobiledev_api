<?php
/**
 * Methods for Statistics
 *
 * @version 1.0
 * @author Bobile
 */

class statsManager {

    protected $db;

    public function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }
    }

    public static function lifetimeRevenuesPerMember($bizId,$startDate,$endDate){

        $instance = new self();
        $baseField = self::getStatsBaseField($bizId);
        $totalRevenue = $instance->db->getVal("SELECT SUM($baseField) FROM tbl_daily_customer_activity WHERE dca_biz_id=$bizId AND dca_activity_date BETWEEN '$startDate' AND '$endDate'");

        return number_format($totalRevenue, 2,".","");
    }

    public static function estimatedTotalClubValue($bizId){

        $instance = new self();
        $baseField = self::getStatsBaseField($bizId);
        $weightedAvg = 0;

        $last3MonthsData = $instance->db->getTable("
                SELECT
                biz_id,
                date_range_groups,
                SUM(invoice_sum) AS invoice_sum,
                COUNT(distinct cust_id) AS unique_users_count
                FROM(
                    SELECT
                    dca_activity_date AS activity_date,
                    datediff(curdate(),dca_activity_date) AS days_since_activity,
                    CASE
                        WHEN datediff(curdate(),`dca_activity_date`)<=30 THEN '1to30'
                        WHEN datediff(curdate(),`dca_activity_date`)>=31 AND datediff(curdate(),`dca_activity_date`)<=60 THEN '31to60'
                        WHEN datediff(curdate(),`dca_activity_date`)>=61 AND datediff(curdate(),`dca_activity_date`)<=90 THEN '61to90'
                        ELSE null END AS date_range_groups,
                    dca_cust_id AS cust_id,
                    dca_biz_id AS biz_id,
                    $baseField AS invoice_sum
                    FROM tbl_daily_customer_activity
                    WHERE dca_biz_id = $bizId
                    AND datediff(curdate(),dca_activity_date) <= 90
                    ) x
                GROUP BY biz_id, date_range_groups
                ORDER BY biz_id ASC, date_range_groups ASC 
                ");

        $totalUsersCount = 0;
        foreach($last3MonthsData as $rangeGroup){
            $totalUsersCount += $rangeGroup["unique_users_count"];
        }

        foreach($last3MonthsData as $rangeGroup){
            $weightedAvg += ($rangeGroup["unique_users_count"]/$totalUsersCount) * $rangeGroup["invoice_sum"];
        }

        return number_format($weightedAvg, 2,".","");
    }

    public static function growthRate($bizId,$start_date,$end_date){
        
        $instance = new self();

        $bizStartDate = $instance->db->getVal("SELECT biz_start_date FROM tbl_biz WHERE biz_id=$bizId");

        if (strtotime($end_date) <= strtotime($bizStartDate)){
            return 0;
        }

        $beginDate = $start_date;
        if (strtotime($start_date) <= strtotime($bizStartDate)){
            $beginDate = $bizStartDate;
        }

        $daysInRang = utilityManager::dateDiffInDays($beginDate, $end_date);
        if ($daysInRang <= 0){
            return 0;
        }

        $customersForMtbf = self::getTotalMembersByStatusInPeriod($bizId,"member",$beginDate,$end_date);
        
        return $customersForMtbf == 0 ? 0 : number_format($customersForMtbf / $daysInRang,2,".","");
    }

    public static function glueGost($bizId,$start_date,$end_date){        
        $instance = new self();
        $cost = $instance->db->getVal("SELECT AVG(cust_conversion_cost) FROM tbl_customers 
                                        WHERE cust_biz_id = $bizId
                                        AND cust_status = 'member'
                                        AND cust_reg_date >= STR_TO_DATE('$start_date','%Y-%m-%d')
                                        AND cust_reg_date <= STR_TO_DATE('$end_date','%Y-%m-%d')");

        return number_format($cost,2,".","");
    }

    public static function monthRPM($bizId,$start_date,$end_date){
        
        $instance = new self();
        $baseField = self::getStatsBaseField($bizId);
        return number_format($instance->db->getVal("SELECT avg(val) from (
                                                        SELECT avg($baseField) val,EXTRACT(YEAR_MONTH FROM dca_activity_date),dca_cust_id 
                                                            FROM tbl_daily_customer_activity,tbl_customers 
                                                        WHERE cust_id=dca_cust_id
                                                        AND cust_status='member'
                                                        AND dca_biz_id = $bizId
                                                        AND dca_activity_date BETWEEN '$start_date' AND '$end_date'
                                                        GROUP BY EXTRACT(YEAR_MONTH FROM dca_activity_date),dca_cust_id
                                                            ) t1
                                                    "),2,".","");

    }

    public static function purchaseVolume($bizId,$startDate,$endDate){
        $instance = new self();
        return number_format($instance->db->getVal("SELECT avg(val) from (
                                                        SELECT avg(dca_order_count) val,EXTRACT(YEAR_MONTH FROM dca_activity_date),dca_cust_id 
                                                            FROM tbl_daily_customer_activity,tbl_customers 
                                                        WHERE cust_id=dca_cust_id
                                                        AND cust_status='member'
                                                        AND dca_biz_id = $bizId
                                                        AND dca_activity_date BETWEEN '$startDate' AND '$endDate'
                                                        GROUP BY EXTRACT(YEAR_MONTH FROM dca_activity_date),dca_cust_id
                                                            ) t1
                                                    "),2,".","");
    }

    public static function totalRevenueNewMembers($bizId,$startDate,$endDate){

        $instance = new self();
        $baseField = self::getStatsBaseField($bizId);
        return number_format($instance->db->getVal("SELECT IFNULL(SUM($baseField), 0)
                                        FROM tbl_daily_customer_activity,tbl_customers 
                                        WHERE cust_id=dca_cust_id
                                        AND dca_biz_id = $bizId
                                        AND dca_activity_date BETWEEN '$startDate' AND '$endDate'
                                        AND cust_reg_date BETWEEN DATE_SUB(dca_activity_date,INTERVAL 90 DAY) AND dca_activity_date
                                        "),2,".","");

    }

    public static function totalRevenueReturningMembers($bizId,$startDate,$endDate){

        $instance = new self();
        $baseField = self::getStatsBaseField($bizId);
        return number_format($instance->db->getVal("SELECT IFNULL(SUM($baseField), 0)
                                        FROM tbl_daily_customer_activity,tbl_customers 
                                        WHERE cust_id=dca_cust_id
                                        AND dca_biz_id = $bizId
                                        AND dca_activity_date BETWEEN '$startDate' AND '$endDate'
                                        AND cust_reg_date < DATE_SUB(dca_activity_date,INTERVAL 90 DAY)
                                        "),2,".","");

    }

    public static function averageRevenuePerMembers($bizId,$startDate,$endDate){

        $instance = new self();
        $baseField = self::getStatsBaseField($bizId);
        $data =  $instance->db->getTable("SELECT IFNULL(dca_membership_tier,0) member_tier, TRUNCATE(IFNULL(SUM($baseField), 0),2) sum, count(DISTINCT dca_cust_id) count
                                        FROM tbl_daily_customer_activity 
                                        WHERE dca_biz_id = $bizId
                                        AND dca_activity_date BETWEEN '$startDate' AND '$endDate'
                                        GROUP BY dca_membership_tier
                                        ");

        $returnArray[0] = 0;
        $returnArray[1] = 0;
        $returnArray[2] = 0;
        $returnArray[3] = 0;

        $platinumCount = 0;

        if(count($data) > 0){
            foreach ($data as $value)
            {
            	if($value["member_tier"] >= 3){
                    $returnArray[3] += $value["sum"];
                    $platinumCount += $value["count"];
                }else{
                    $returnArray[$value["member_tier"]] = $value["count"] == 0 ? 0 : number_format($value["sum"] / $value["count"],2,".","");
                }
            }
            $returnArray[3] = $platinumCount == 0 ? 0 : number_format($returnArray[3] / $platinumCount,2,".","");
        }

        return $returnArray;
    }

    public static function totalMembersPerTier($bizId,$startDate,$endDate){

        $instance = new self();

        $data =  $instance->db->getTable("SELECT tier, count(cust_id) count FROM (
                                            SELECT cust_id, IFNULL(MAX(cmth_tier),0) tier FROM tbl_customers LEFT JOIN tbl_customer_membership_tier_history ON cust_id = cmth_cust_id
                                            WHERE cust_biz_id = $bizId
                                            AND cust_status = 'member'
                                            AND (cmth_start BETWEEN '$startDate' AND '$endDate' OR cmth_start IS NULL)
                                            AND cust_reg_date <= '$endDate'
                                            GROUP BY cust_id) t1
                                            GROUP BY tier");

        $returnArray[0] = 0;
        $returnArray[1] = 0;
        $returnArray[2] = 0;
        $returnArray[3] = 0;

        if(count($data) > 0){
            foreach ($data as $value)
            {
            	if($value["tier"] >= 3){
                    $returnArray[3] += $value["count"];
                }else{
                    $returnArray[$value["tier"]] = $value["count"];
                }
            }            
        }

        return $returnArray;
    }

    public static function purchaseVsVisits($bizId){

        $instance = new self();

        return   $instance->db->getTable("SELECT yearMonth, IFNULL(val,0) val from 
                                            (SELECT distinct EXTRACT(YEAR_MONTH FROM datefield) yearMonth FROM calendar
                                            WHERE datefield between DATE_SUB(NOW(),INTERVAL 11 MONTH) AND NOW()) calendar 
                                            LEFT JOIN 
                                            (SELECT 
                                            EXTRACT(YEAR_MONTH FROM dca_activity_date) calcDate,sum(dca_invoice_count), sum(dca_visits_count), TRUNCATE(IFNULL(sum(dca_visits_count) / sum(dca_invoice_count), 0),2) val 
                                            FROM 
                                            tbl_daily_customer_activity
                                            WHERE dca_biz_id = $bizId
                                            and dca_activity_date between DATE_SUB(NOW(),INTERVAL 1 MONTH) AND NOW()
                                            GROUP BY EXTRACT(YEAR_MONTH FROM dca_activity_date)) t1
                                            ON yearMonth = calcDate");
    }

    private static function getStatsBaseField($bizId){
        $storeSettings = bizManager::getBizStoreObjectForBiz($bizId);

        switch($storeSettings->ess_evaluate_revenue){
            case "orders":
                return "dca_order_sum";
            case "transactions":
                return "dca_transaction_sum";
            default:
                return "dca_invoice_sum";
        }
    }

    private static function getMembersForLastDays($bizId,$days){

        $instance = new self();
        return $instance->db->getVal("SELECT COUNT(cust_id) FROM tbl_customers
                                                    WHERE cust_biz_id=$bizId
                                                    AND cust_status='member'
                                                    AND IFNULL(TIMESTAMPDIFF(DAY,cust_reg_date,NOW()),0) <= $days");
    }

    private static function getMembersForDaysSinceSpecificDate($bizId,$days,$startDate){

        $instance = new self();
        return $instance->db->getVal("SELECT COUNT(cust_id) FROM tbl_customers
                                                    WHERE cust_biz_id=$bizId
                                                    AND cust_status='member'
                                                    AND cust_reg_date BETWEEN '$startDate' AND DATE_ADD('$startDate', INTERVAL $days DAY)
                                                    ");


    }

    public static function getTotalMembersByStatusInPeriod($bizID,$status,$startDate,$endDate){

        $instance = new self();
        return $instance->db->getVal("SELECT count(cust_id) FROM tbl_customers 
                                        WHERE cust_biz_id=$bizID 
                                        AND cust_status='$status'
                                        AND cust_reg_date >= STR_TO_DATE('$startDate','%Y-%m-%d')
                                        AND cust_reg_date <= STR_TO_DATE('$endDate','%Y-%m-%d')");
    }


}

?>
