<?php 

class bizCalendarHoursObject extends bobileObject { 

    public $bch_id = "0";
    public $bch_biz_id = "0";
    public $bch_day_no = "0";
    public $bch_day_name;
    public $bch_begin_hour = "109";
    public $bch_finish_hour = "105";
    public $bch_working_day = "1";

    public $day_start_hour;
    public $day_finish_hour;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bch_id"])){
              throw new Exception("bizCalendarHoursObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bch_id = $data["bch_id"];
        $instance->bch_biz_id = $data["bch_biz_id"];
        $instance->bch_day_no = $data["bch_day_no"];
        $instance->bch_day_name = $data["bch_day_name"];
        $instance->bch_begin_hour = $data["bch_begin_hour"];
        $instance->bch_finish_hour = $data["bch_finish_hour"];
        $instance->bch_working_day = $data["bch_working_day"];

        $instance->day_start_hour = isset($data["day_start_hour"]) ? $data["day_start_hour"] : "";
        $instance->day_finish_hour = isset($data["day_finish_hour"]) ? $data["day_finish_hour"] : "";
        return $instance;
    }

    public function getArrayForBizAdmin(){
        $result = array();

        $result["day_no"] = $this->bch_day_no;
        $result["day_name"] = $this->bch_day_name;
        $result["day_start_hour"] = $this->day_start_hour;
        $result["day_end_hour"] = $this->day_finish_hour;
        $result["day_is_active"] = $this->bch_working_day;

        return $result;
    }

    function _isValid(){
          return true;
    }
}
?>


