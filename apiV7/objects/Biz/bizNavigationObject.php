<?php 

class bizNavigationObject extends bobileObject { 

    public $bn_id = "0";
    public $bn_biz_id = "0";
    public $bn_entity;//enum('module','function','menu','')
    public $bn_entity_id = "0";
    public $bn_external_id = "0";
    public $bn_index = "0";
    public $bn_label;
    public $bn_icon;
    public $bn_selector_ios;
    public $bn_selector_and;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bn_id"])){
              throw new Exception("bizNavigationObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bn_id = $data["bn_id"];
        $instance->bn_biz_id = $data["bn_biz_id"];
        $instance->bn_entity = $data["bn_entity"];
        $instance->bn_entity_id = $data["bn_entity_id"];
        $instance->bn_external_id = $data["bn_external_id"];
        $instance->bn_index = $data["bn_index"];
        $instance->bn_label = $data["bn_label"];
        $instance->bn_icon = $data["bn_icon"];
        $instance->bn_selector_ios = $data["bn_selector_ios"];
        $instance->bn_selector_and = $data["bn_selector_and"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


