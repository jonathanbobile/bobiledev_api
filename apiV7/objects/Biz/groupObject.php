<?php 

class groupObject extends bobileObject { 

    public $cg_id = "0";
    public $cg_biz_id = "0";
    public $cg_name;
    public $cg_description;
    public $cg_type = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cg_id"])){
              throw new Exception("groupObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cg_id = $data["cg_id"];
        $instance->cg_biz_id = $data["cg_biz_id"];
        $instance->cg_name = $data["cg_name"];
        $instance->cg_description = $data["cg_description"];
        $instance->cg_type = $data["cg_type"];

        return $instance;
    }

    function _isValid(){
          return true;
    }

    public function AdminAPIArray(){

        $result = array();
        $result = (array) $this;

        $result['cg_biz_id'] = utilityManager::encodeToHASH($this->cg_biz_id);

        return $result;
    }
}
?>


