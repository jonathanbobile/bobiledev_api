<?php 

class pushHistoryLogObject extends bobileObject { 

    public $puh_id = "0";
    public $puh_biz_id = "0";
    public $puh_cust_id = "0";
    public $puh_push_id = "0";
    public $puh_device_id;
    public $puh_short_mess;
    public $puh_type;//enum('Pre-Scheduled','Send Now','Greeting','Reminder','Geo','Benefit','Campaign','Subscription','Punch pass')
    public $pu_groups;
    public $puh_created = "";
    public $puh_sent = "";
    public $puh_external_id = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["puh_id"])){
              throw new Exception("pushHistoryLogObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->puh_id = $data["puh_id"];
        $instance->puh_biz_id = $data["puh_biz_id"];
        $instance->puh_cust_id = $data["puh_cust_id"];
        $instance->puh_push_id = $data["puh_push_id"];
        $instance->puh_device_id = $data["puh_device_id"];
        $instance->puh_short_mess = $data["puh_short_mess"];
        $instance->puh_type = $data["puh_type"];
        $instance->pu_groups = $data["pu_groups"];
        $instance->puh_created = $data["puh_created"];
        $instance->puh_sent = $data["puh_sent"];
        $instance->puh_external_id = $data["puh_external_id"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


