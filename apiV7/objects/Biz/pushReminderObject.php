<?php 

class pushReminderObject extends bobileObject { 

    public $rem_id = "0";
    public $rem_biz_id = "0";
    public $rem_created = "";
    public $rem_text;
    public $pu_target = "all";//enum('all','guests','members','groups')
    public $pu_groups;
    public $pu_cust_id = "0";
    public $rem_entity_type = "reminder";//enum('reminder','meeting')
    public $rem_entity_id = "0";
    public $rem_repeat = "Monthly";//enum('Daily','Weekly','Monthly','Yearly','Once')
    public $rem_active = "0";
    public $rem_time;
    public $rem_next_send = "0000-00-00 00:00:00";

    function __construct(){} 

    public static function withData($data){

    if (!isset($data["rem_id"])){
          throw new Exception("pushReminderObject constructor requies data array provided!");
    }

    $instance = new self();

    $instance->rem_id = $data["rem_id"];
    $instance->rem_biz_id = $data["rem_biz_id"];
    $instance->rem_created = $data["rem_created"];
    $instance->rem_text = $data["rem_text"];
    $instance->pu_target = $data["pu_target"];
    $instance->pu_groups = $data["pu_groups"];
    $instance->pu_cust_id = $data["pu_cust_id"];
    $instance->rem_entity_type = $data["rem_entity_type"];
    $instance->rem_entity_id = $data["rem_entity_id"];
    $instance->rem_repeat = $data["rem_repeat"];
    $instance->rem_active = $data["rem_active"];
    $instance->rem_time = $data["rem_time"];
    $instance->rem_next_send = $data["rem_next_send"];

    return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


