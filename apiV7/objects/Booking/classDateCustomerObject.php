<?php

/**
 * classDateCustomerObject short summary.
 *
 * classDateCustomerObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class classDateCustomerObject extends customerPersonalObject
{
    /* Identifiers */
    public $cdc_id = "0";
    public $cdc_biz_id = "0";
    public $cdc_class_id = "0";
    public $cdc_class_date_id = "0";
    public $cdc_customer_id = "0";

    /* Details */
    public $cdc_signup;
    public $cdc_comments;
    public $cdc_scheduled_from = "External";//enum('Internal','External')

    /* Settings */
    public $cdc_payment_source = "none";//enum('none','order','subscription','punch_pass')
    public $cdc_payment_source_id = "0";
    public $cdc_payment_source_inner_id = "0";


    public static function withData($classdateCustomerData){
        if(!isset($classdateCustomerData["cdc_id"])){
            throw new Exception("Data incorrect - class date customer");
        }

        $instance = new self();

        /* Identifiers */
        $instance->cdc_id = $classdateCustomerData["cdc_id"];
        $instance->cdc_biz_id = $classdateCustomerData["cdc_biz_id"];
        $instance->cdc_class_id = $classdateCustomerData["cdc_class_id"];
        $instance->cdc_class_date_id = $classdateCustomerData["cdc_class_date_id"];
        $instance->cdc_customer_id = $classdateCustomerData["cdc_customer_id"];

        /* Details */
        $instance->cdc_signup = $classdateCustomerData["cdc_signup"];
        $instance->cdc_comments = $classdateCustomerData["cdc_comments"];
        $instance->cdc_scheduled_from = $classdateCustomerData["cdc_scheduled_from"];

        /* Settings */
        $instance->cdc_payment_source = $classdateCustomerData["cdc_payment_source"];
        $instance->cdc_payment_source_id = $classdateCustomerData["cdc_payment_source_id"];
        $instance->cdc_payment_source_inner_id = $classdateCustomerData["cdc_payment_source_inner_id"];

        return $instance;
    }

    

    public function PersonalZoneAPIArray(mobileRequestObject $request = null){
        $result = array();

        $classResult = bookingManager::getClassByID($this->cdc_class_id);
        if($classResult->code == 0){
            return $result;
        }

        $class = $classResult->data;

        $classDateResult = bookingManager::getClassDateByID($this->cdc_class_date_id);
        if($classDateResult->code == 0){
            return $result;
        }

        $classDate = $classDateResult->data;

        $startStamp = strtotime( $classDate->ccd_start_date.' '.$classDate->start_hour );
        $endStamp = strtotime( $classDate->ccd_end_date.' '.$classDate->end_hour );

        $result["meet_id"] = $this->cdc_class_id;
        $result["meet_secondary_id"] = $this->cdc_class_date_id;
        $result["meet_emp_id"] = isset($class->employee->be_id) ? $class->employee->be_id : '';
        $result["meet_emp_name"] = isset($class->employee->be_name) ? $class->employee->be_name : '';
        $result["meet_emp_pic"] = isset($class->employee->be_pic) ? $class->employee->be_pic : '';
        $result["meet_type_id"] = $class->calc_service;
        $result["meet_type_name"] = $class->calc_name;
        $result["meet_start_timestamp"] = $startStamp;
        $result["meet_end_timestamp"] = $endStamp;

        return $result;
    }

    public function adminAPIArray(){
        $result = (array)$this;

        $result["paid"] = 0;
        if($this->cdc_payment_source == "none"){
            $result["paid"] = 0;
        }
        else{
            if($this->cdc_payment_source == "order"){
                $customerBilling = new customerBillingManager();

                $result['invoices'] = $customerBilling->getInvoicesListForOrderByOrderID($this->cdc_payment_source_id)->data;

                $orderInvoice = end($result)[0];
                
                $result["paid"] = $orderInvoice->cin_status == 'paid' || $orderInvoice->cin_status == 'refunded' ? 1 : 0;
            }
            else{
                $result["paid"] = 1;
            }
        }


        return $result;
    }

    function _getDBArray(){
        $data = array();

        $data = parent::_getDBArray();

        $data["cdc_id"] = $this->cdc_id;
        $data["cdc_class_id"] = $this->cdc_class_id;
        $data["cdc_biz_id"] = $this->cdc_biz_id;
        $data["cdc_class_date_id"] = $this->cdc_class_date_id; 
        $data["cdc_customer_id"] = $this->cdc_customer_id;
        $data["cdc_signup"] = $this->cdc_signup;
        $data["cdc_comments"] = $this->cdc_comments;
        $data["cdc_payment_source"] = $this->cdc_payment_source;
        $data["cdc_payment_source_id"] = $this->cdc_payment_source_id;
        $data["cdc_payment_source_inner_id"] = $this->cdc_payment_source_inner_id;

        return $data;
    }
}
