<?php

/**
 * classObject short summary.
 *
 * classObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class classObject extends bobileObject
{
    /* Identifiers */
    public $calc_id;
    public $calc_biz_id;

    /* Details */
    public $calc_name;
    public $calc_desc;

    /* Settings */    
    public $calc_max_cust = 1;
    public $calc_min_cust = 1;
    public $calc_duration_by_service;
    public $calc_service;
    public $calc_price;
    public $calc_duration;
    public $calc_duration_size;
    public $calc_isvisible = 1;
    public $calc_isdeleted = 0;

    public $employee_id = 0;
    public $dates = array();
    public $employee;
    public $service;

    function __construct(){
        
    }

    public static function withData($classData){
        if(!isset($classData["calc_id"])){
            throw new Exception("Data incorrect - class");
        }
       
        $instance = new self();

        /* Identifiers */
        $instance->calc_id = isset($classData["calc_id"]) && $classData["calc_id"] > 0 ? $classData["calc_id"] : 0;
        $instance->calc_biz_id = isset($classData["calc_biz_id"]) && $classData["calc_biz_id"] > 0 ? $classData["calc_biz_id"] : 0;

        /* Details */
        $instance->calc_name = $classData["calc_name"];
        $instance->calc_desc = $classData["calc_desc"];

        /* Settings */   
        $instance->calc_max_cust = $classData["calc_max_cust"];
        $instance->calc_min_cust = $classData["calc_min_cust"];
        $instance->calc_duration_by_service = $classData["calc_duration_by_service"];
        $instance->calc_service = $classData["calc_service"];
        $instance->calc_price = $classData["calc_price"];
        $instance->calc_duration = $classData["calc_duration"];
        $instance->calc_duration_size = $classData["calc_duration_size"];
        $instance->calc_isvisible = $classData["calc_isvisible"];
        $instance->calc_isdeleted = $classData["calc_isdeleted"]; 
        
        $instance->employee_id = isset($classData["ce_employee_id"]) ? $classData["ce_employee_id"] : 0;

        $instance->updateDuration();
        
        return $instance;
    }

    public function getAPIFormattedArray(mobileRequestObject $requestObj){
        $result = array();

        $duration = $this->calc_duration;
        $durationUnits = $this->calc_duration_size;
        $price = 0;
        if($this->calc_service != "0" && $this->calc_duration_by_service == 1 && isset($this->service)){
            
            $duration = $this->service->bmt_duration;
            $durationUnits = $this->service->bmt_size;
            $price = $this->service->bmt_price;
        }else{
            $price = $this->calc_price;
        }

        $result['recordID']=$this->calc_id;
        $result['class_name']=$this->calc_name;
        $result['class_description']=$this->calc_desc;
        $result['class_total_positions']=$this->calc_max_cust;
        $result['based_on_service']=$this->calc_duration_by_service;
        $result['service_id']=$this->calc_service;
        $result['service_price']=$price;
        $result['class_duration']=$duration;
        $result['duration_units']=$durationUnits;
        $result['employee_id']=isset($this->employee->be_id) ? $this->employee->be_id : 0;
        $result['employee_name']=isset($this->employee->be_name) ? $this->employee->be_name : "";
        $result['employee_email']=isset($this->employee->be_mail) ? $this->employee->be_mail : "";
        $result['employee_pic']=isset($this->employee->be_pic) ? $this->employee->be_pic : IMAGE_STORAGE_PATH."system/no_employee_image_square.png";
        $result['in_sub'] = customerSubscriptionManager::isItemInCustomerSubscription($requestObj->server_customer_id,'class',$this->calc_id) ? "1" : "0";
        
        

        return $result;
    } 

    public function updateDuration(){
        $duration = $this->getDuration();

        $this->calc_duration = $duration['duration'];
        $this->calc_duration_size = $duration['durationUnits'];

        
    }

    
    
    public function getDuration(){
        
        $result = array();

        $result['duration'] = $this->calc_duration;
        $result['durationUnits'] = $this->calc_duration_size;
        if($this->calc_service != "0" && $this->calc_duration_by_service == 1){
            if(!isset($this->service)){
                $this->service =  bookingManager::getServiceByID($this->calc_service)->data;
            }
            $result['duration'] = $this->service->bmt_duration;
            $result['durationUnits'] = $this->service->bmt_size;
        }

        return $result;
    }

    public function setEmployee(employeeObject $employee){
        $this->employee = $employee;
    }

    public function getEmployee(){
        return $this->employee;
    }

    public function setService(serviceObject $service){
        $this->service = $service;
    }

    public function getService(){
        return $this->service;
    }

    public function setDates($datesList){
        $this->dates = $datesList;
    }

    public function getDates(){
        return $this->dates;
    }
}
