<?php

/**
 * serviceObject short summary.
 *
 * serviceObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class serviceObject extends bobileObject
{
    /* Identifiers */
    public $bmt_id = 0;
    public $bmt_biz_id = 0;

    /* Details */
    public $bmt_name;
    public $bmt_desc;
    public $bmt_pic;
    public $bmt_color;

    /* Settings */   
    public $bmt_size;
    public $bmt_duration;
    public $bmt_active;
    public $bmt_mobile_active;
    public $bmt_capacity;
    public $bmt_price;
    
    /* Assigned employee */
    public $employee; 
    public $connected_employees_id = array();

    public static function withData($serviceData){
        if(!isset($serviceData["bmt_id"])){
            throw new Exception("Data incorrect -service");
        }
        $instance = new self();



        /* Identifiers */
        $instance->bmt_id = isset($serviceData["bmt_id"]) && $serviceData["bmt_id"] > 0 ? $serviceData["bmt_id"] : 0;
        $instance->bmt_biz_id = isset($serviceData["bmt_biz_id"]) && $serviceData["bmt_biz_id"] > 0 ? $serviceData["bmt_biz_id"] : 0;

        /* Details */
        $instance->bmt_name = $serviceData["bmt_name"];
        $instance->bmt_desc = $serviceData["bmt_desc"];
        $instance->bmt_color = $serviceData["bmt_color"];
        $instance->bmt_pic = $serviceData["bmt_pic"];

        /* Settings */
        $instance->bmt_size = $serviceData["bmt_size"];
        $instance->bmt_duration = $serviceData["bmt_duration"];
        $instance->bmt_active = $serviceData["bmt_active"];
        $instance->bmt_mobile_active = $serviceData["bmt_mobile_active"];
        $instance->bmt_capacity = $serviceData["bmt_capacity"];           
        $instance->bmt_price = $serviceData["bmt_price"];

        return $instance;
    }

    public function getAPIFormattedArray(mobileRequestObject $requestObject){
        $result = array();

        $result["type_id"] = $this->bmt_id;
        $result["type_name"] = $this->bmt_name;
        $result["in_sub"] = customerSubscriptionManager::isItemInCustomerSubscription($requestObject->server_customer_id,'service',$this->bmt_id);

        return $result;
    }

    public function fillConnectedEmployees(){

        $employeesResult = bookingManager::getEmployeesIDsForService($this->bmt_id);

        foreach ($employeesResult as $employee)
        {
            $this->connected_employees_id[] = $employee["emt_emp_id"];
        }
    }

    public function setEmployee(employeeObject $employee){
        $this->employee = $employee;
    }

    public function getAssignedEmployee(){
        return $this->employee;
    }

}
