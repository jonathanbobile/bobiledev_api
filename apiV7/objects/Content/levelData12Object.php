<?php 

class levelData12Object extends levelDataObject {

    public $md_mod_id = "12";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
              throw new Exception("levelData12Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        return $instance;
    }
}
?>


