<?php 

class levelData23Object extends levelDataObject { 

    public $md_mod_id = "23";   

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData23Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        return $instance;
    }
}
?>


