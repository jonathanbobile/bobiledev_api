<?php 

class customerClaimObject extends bobileObject { 

    public $cc_id = "0";
    public $cc_biz_id = "0";
    public $cc_cust_id = "0";
    public $cc_item_source;//enum('product','appointment','class','coupon','subscription','punchpass','workshop','event','giftcard','other')
    public $cc_item_source_id = "0";
    public $cc_item_type = "0";
    public $cc_order_id = "0";
    public $cc_order_item_id = "0";
    public $cc_was_fulfilled = "0";
    public $cc_fulfilled_on;
    public $cc_fulfillment_row = 0;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cc_id"])){
            throw new Exception("customerClaimObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cc_id = $data["cc_id"];
        $instance->cc_biz_id = $data["cc_biz_id"];
        $instance->cc_cust_id = $data["cc_cust_id"];
        $instance->cc_item_source = $data["cc_item_source"];
        $instance->cc_item_source_id = $data["cc_item_source_id"];
        $instance->cc_item_type = $data["cc_item_type"];
        $instance->cc_order_id = $data["cc_order_id"];
        $instance->cc_order_item_id = $data["cc_order_item_id"];
        $instance->cc_was_fulfilled = $data["cc_was_fulfilled"];
        $instance->cc_fulfilled_on = $data["cc_fulfilled_on"];
        $instance->cc_fulfillment_row = $data["cc_fulfillment_row"];

        return $instance;
    }

    public function getAdminFormattedArray(){
        $result = array();

        $result["cc_id"] = $this->cc_id;
        $result["cc_biz_id"] = $this->cc_biz_id;
        $result["cc_cust_id"] = $this->cc_cust_id;
        $result["cc_item_source"] = $this->cc_item_source;
        $result["cc_item_source_id"] = $this->cc_item_source_id;
        $result["cc_item_type"] = $this->cc_item_type;
        $result["cc_order_id"] = $this->cc_order_id;
        $result["cc_order_item_id"] = $this->cc_order_item_id;
        $result["cc_was_fulfilled"] = $this->cc_was_fulfilled;
        $result["cc_fulfilled_on"] = isset($this->cc_fulfilled_on) ? strtotime($this->cc_fulfilled_on) : 0;
        $result["cc_fulfillment_row"] = $this->cc_fulfillment_row;
        $result["cc_item_title"] = $this->getClaimTitle();

        $orderDate = 0;
        if($this->cc_order_id > 0){  
            $order = customerOrderManager::getCustomerOrderByID($this->cc_order_id);
            if($order->code == 1){
                $orderDate = $order->data->cto_order_tyme == "" ? 0 : $order->data->cto_order_tyme;
                $orderDate = strtotime($orderDate);
            }
        }

        $result["order_date"] = $orderDate;

        return $result;
    }

    private function getClaimItem(){
        $result = resultObject::withData(0);
        switch($this->cc_item_source){
            case "product":
                $levelManager = new levelDataManager(9);
                $result = $levelManager->getAdminProduct($this->cc_item_source_id);
                break;
            case "appointment":
                $result = bookingManager::getServiceByID($this->cc_item_source_id);
                break;
            case "class":
                $result = bookingManager::getClassByID($this->cc_item_source_id);
                break;
            case "coupon":
                $levelManager = new levelDataManager(26);
                $result = $levelManager->getLevelDataByID($this->cc_item_source_id);
                break;
            case "subscription":
                $levelManager = new levelDataManager(10);
                $result = $levelManager->getLevelDataByID($this->cc_item_source_id);
                break;
            case "punchpass":
                $levelManager = new levelDataManager(11);
                $result = $levelManager->getLevelDataByID($this->cc_item_source_id);
                break;
            case "workshop":
                break;
            case "event":
                break;
            case "giftcard":
                break;
            case "other":
                break;
        }

        return $result;
    }
    
    private function getClaimTitle(){
        $title = "";
        try{
            switch($this->cc_item_source){
                case "product":
                    $levelManager = new levelDataManager(9);
                    $item = $levelManager->getAdminProduct($this->cc_item_source_id)->data;
                    $title = $item->md_head;
                    break;
                case "appointment":
                    $item = bookingManager::getServiceByID($this->cc_item_source_id)->data;
                    $title = $item->bmt_name;
                    break;
                case "class":
                    $item = bookingManager::getClassByID($this->cc_item_source_id)->data;
                    $title = $item->calc_name;
                    break;
                case "coupon":
                    $levelManager = new levelDataManager(26);
                    $item = $levelManager->getLevelDataByID($this->cc_item_source_id)->data;
                    $title = $item->md_head;
                    break;
                case "subscription":
                    $levelManager = new levelDataManager(10);
                    $item = $levelManager->getLevelDataByID($this->cc_item_source_id)->data;
                    $title = $item->md_head;
                    break;
                case "punchpass":
                    $levelManager = new levelDataManager(11);
                    $item = $levelManager->getLevelDataByID($this->cc_item_source_id)->data;
                    $title = $item->md_head;
                    break;
                case "workshop":
                    break;
                case "event":
                    break;
                case "giftcard":
                    break;
                case "other":
                    break;
            }
        }
        catch(Exception $e){
        }

        return $title;
    }

    function _isValid(){
        return true;
    }
}
?>


