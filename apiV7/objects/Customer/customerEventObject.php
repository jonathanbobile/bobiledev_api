<?php 

class customerEventObject extends bobileObject { 

    public $cevent_id = "0";
    public $cevent_cust_id = "0";
    public $cevent_biz_id = "0";
    public $cevent_cust_history_id = "0";
    public $cevent_event_id = "0";
    public $cevent_time = "";
    public $cevent_queue_log_id = 0;
    public $cevent_status;//enum('killed','sent')

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cevent_id"])){
            throw new Exception("customerEventObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cevent_id = $data["cevent_id"];
        $instance->cevent_cust_id = $data["cevent_cust_id"];
        $instance->cevent_biz_id = $data["cevent_biz_id"];
        $instance->cevent_cust_history_id = $data["cevent_cust_history_id"];
        $instance->cevent_event_id = $data["cevent_event_id"];
        $instance->cevent_time = $data["cevent_time"];
        $instance->cevent_status = $data["cevent_status"];
        $instance->cevent_queue_log_id = $data["cevent_queue_log_id"];

        return $instance;
    }

    function _isValid(){
        return true;
    }
}
?>


