<?php 

class customerPointsRedeemHistoryObject extends bobileObject { 

    public $cprh_id = "0";
    public $cprh_cust_id = "0";
    public $cprh_biz_id = "0";
    public $cprh_cph_id = "0";
    public $cprh_amount = "0";
    public $cprh_points_source_entry_id = "0";
    public $cprh_psr_id = "0";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cprh_id"])){
              throw new Exception("customerPointsRedeemHistoryObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cprh_id = $data["cprh_id"];
        $instance->cprh_cust_id = $data["cprh_cust_id"];
        $instance->cprh_biz_id = $data["cprh_biz_id"];
        $instance->cprh_cph_id = $data["cprh_cph_id"];
        $instance->cprh_amount = $data["cprh_amount"];
        $instance->cprh_points_source_entry_id = $data["cprh_points_source_entry_id"];
        $instance->cprh_psr_id = $data["cprh_psr_id"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


