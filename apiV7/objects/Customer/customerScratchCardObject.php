<?php

/**
 * custScratchCardObject short summary.
 *
 * custScratchCardObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerScratchCardObject extends bobileObject
{
    /* Identifiers */
    public $cscard_id = "0";
    public $cscard_biz_id = "0";
    public $cscard_cust_id = "0";
    public $cscard_card_id = "0";

    /* Settings */
    public $cscard_win = "0";
    public $cscard_date;
    public $cscard_status = "new";//enum('new','scratched')
    public $cscard_scratched_on;
    public $cscard_event_id = "0";
    public $cscard_cevent_id = "0";
    public $cscard_prize_type;//enum('product','service','other','points')
    public $cscard_prize_id = "0";
    public $cscard_prize_value = "0";
    public $cscard_prize_text;
    public $cscard_expire;
    public $cscard_consolation = "0";
    public $cscard_consolation_text;
    public $cscard_prize_title;

    /* Connected scratch card */

    private $scratchCard;

    public static function withData($data){
        $instance = new self();

        $instance->cscard_id = $data["cscard_id"];
        $instance->cscard_biz_id = $data["cscard_biz_id"];
        $instance->cscard_cust_id = $data["cscard_cust_id"];
        $instance->cscard_card_id = $data["cscard_card_id"];
        $instance->cscard_win = $data["cscard_win"];
        $instance->cscard_date = $data["cscard_date"];
        $instance->cscard_status = $data["cscard_status"];
        $instance->cscard_scratched_on = $data["cscard_scratched_on"];
        $instance->cscard_event_id = $data["cscard_event_id"];
        $instance->cscard_cevent_id = $data["cscard_cevent_id"];
        $instance->cscard_prize_type = $data["cscard_prize_type"];
        $instance->cscard_prize_id = $data["cscard_prize_id"];
        $instance->cscard_prize_value = $data["cscard_prize_value"];
        $instance->cscard_prize_text = $data["cscard_prize_text"];
        $instance->cscard_expire = $data["cscard_expire"];
        $instance->cscard_consolation = $data["cscard_consolation"];
        $instance->cscard_consolation_text = $data["cscard_consolation_text"];
        $instance->cscard_prize_title = $data["cscard_prize_title"];

        return $instance;
    }

    public function setScratchCard(scratchCardObject $scratchCard){
        if(!$scratchCard->_isValid()){
            throw new Exception('Invalid scratch card');
        }

        $this->scratchCard = $scratchCard;
    }

    public function getScratchCard(){
        return $this->scratchCard;
    }
}
