<?php

/**
 * punchpassPZObject short summary.
 *
 * punchpassPZObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class punchpassPZObject extends levelData11Object
{
    public $cust_pass_data;

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData11Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        $instance->md_usage_total_capacity = $data["md_usage_total_capacity"];
        $instance->md_usage_period = $data["md_usage_period"];
        $instance->md_usage_period_unit = $data["md_usage_period_unit"];
        $instance->md_usage_period_number = $data["md_usage_period_number"];
        $instance->md_usage_period_start = $data["md_usage_period_start"];
        $instance->md_usage_period_end = $data["md_usage_period_end"];
        $instance->md_usage_rule_type = $data["md_usage_rule_type"];
        $instance->md_usage_rule_capacity = $data["md_usage_rule_capacity"];
        $instance->md_usage_rule_period_unit = $data["md_usage_rule_period_unit"];
        $instance->md_color = $data["md_color"];

        $instance->md_next_view = "PunchPass_Info";

        $instance->extra_data = levelDataManager::getPunchPassExtraData($instance);
        $instance->extra_data["connected_items"] = levelDataManager::getPunchPassConnectedItems($instance);
        if(isset($data['cpp_id'])){
            $custPpassResult = customerSubscriptionManager::getCustomerPunchpassByID($data['cpp_id'],false);
            if($custPpassResult->code == 1){
                $instance->cust_pass_data = $custPpassResult->data;
            }
        }
        $instance->md_deleted = $data["md_deleted"];

        return $instance;
    }
}