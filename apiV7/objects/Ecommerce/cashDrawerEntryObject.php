<?php 

class cashDrawerEntryObject extends bobileObject { 

    public $cde_id = "0";
    public $cde_drawer_id = "0";
    public $cde_account_id = "0";
    public $cde_entry_type;//enum('sale','refund','cash_in','cash_out')
    public $cde_order_id = "0";
    public $cde_reason;
    public $cde_time = "";
    public $cde_total = "0";
    public $cde_unix_time = "0";
    public $cde_sale_id = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cde_id"])){
            throw new Exception("cashDrawerEntryObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cde_id = $data["cde_id"];
        $instance->cde_drawer_id = $data["cde_drawer_id"];
        $instance->cde_account_id = $data["cde_account_id"];
        $instance->cde_entry_type = $data["cde_entry_type"];
        $instance->cde_order_id = $data["cde_order_id"];
        $instance->cde_reason = $data["cde_reason"];
        $instance->cde_time = $data["cde_time"];
        $instance->cde_total = $data["cde_total"];
        $instance->cde_unix_time = strtotime($data["cde_time"]);
        $instance->cde_sale_id = $data["cto_long_order_id"];

        return $instance;
    }

    function AdminAPIArray(){
        $result = (array)$this;

        $result["cde_account_id"] = utilityManager::encodeToHASH($this->cde_account_id);

        $accountManager = new accountManager();

        $account = $accountManager->getAccountById($this->cde_account_id);
        $result["cde_account_name"] = $account->data->ac_name;

        if($this->cde_order_id > 0){
            
            $orderResult = customerOrderManager::getCustomerOrderByID($this->cde_order_id);

            if($orderResult->code == 1){
                $result["cde_order_id"] = $orderResult->data->cto_long_order_id;
            }
        }

        return $result;
    }

    function _isValid(){
        return true;
    }
}
?>


