<?php 

class levelData999Object extends levelDataObject { 

    public $md_mod_id = "999";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData999Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        return $instance;
    }
}
?>


