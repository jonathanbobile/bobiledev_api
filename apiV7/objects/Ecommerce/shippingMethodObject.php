<?php 

class shippingMethodObject extends bobileObject { 

    public $ship_method_id = "0";
    public $ship_method_region_id = "0";
    public $ship_method_name;
    public $ship_method_rate = "0";
    public $ship_method_type;//enum('free','fixed','by_weight','by_price')
    public $ship_method_time;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["ship_method_id"])){
              throw new Exception("shippingMethodObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->ship_method_id = $data["ship_method_id"];
        $instance->ship_method_region_id = $data["ship_method_region_id"];
        $instance->ship_method_name = $data["ship_method_name"];
        $instance->ship_method_rate = $data["ship_method_rate"];
        $instance->ship_method_type = $data["ship_method_type"];
        $instance->ship_method_time = $data["ship_method_time"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


