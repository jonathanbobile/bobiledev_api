<?php

/**
 * enumPeriod short summary.
 *
 * enumPeriod description.
 *
 * @version 1.0
 * @author JonathanM
 */
class enumPeriod extends Enum
{
    const hour = 'hour';

    const day = 'day';

    const week = 'week';
    
    const month = 'month';

    const year = 'year';
}
