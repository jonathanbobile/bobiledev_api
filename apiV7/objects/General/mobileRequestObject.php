<?php

class mobileRequestObject extends bobileObject {

    public $bizid;
    
    public $android;
    public $deviceID;
    public $iphone;
	public $lang;
    public $deviceType;
	public $deviceModel;
    public $deviceOrient;
    public $OS;
    public $OSVersion;
    public $papTapVersion;
    public $bobileVersionNumber;
    public $long;
    public $lati;
    public $phoneNumber;	
    public $state;
    public $country;	
    public $category;
    public $subcategory;
    public $mod_id;
    public $level_no;
    public $parent;
    public $in_favorites;
    public $lastid;
    public $custid;
    public $searchterm;
    public $market;
    public $customerID;
    public $appid;
    public $base;
    public $validPhone;
    public $isMarket;
    public $marketId;
    public $resellerId;
    public $server_customer_id = 0;
    public $is_appetize;
    public $languageM;
    public $bundleId;
    public $OSType;
    public $accountID;

    function __construct(){}

    public static function withData($GET){

        if (!isset($GET["deviceID"])){
            throw new Exception("mobileRequestObject constructor requies valie mobile request data array provided!");
        }

        $instance = new self();

        $instance->android = isset($GET["android"]) ? $GET["android"] : 0;
        $instance->deviceID = $GET["deviceID"];
        if($instance->deviceID == "") $instance->deviceID = $GET["user"];
        $instance->iphone = isset($GET["iphone"]) ? $GET["iphone"] : 0;
        $instance->lang = isset($GET["lang"]) ? $GET["lang"] : "eng";
        $instance->deviceType = isset($GET["deviceType"]) ? $GET["deviceType"] : "";
        $instance->deviceModel = isset($GET["deviceModel"]) ? $GET["deviceModel"] : "";
        $instance->deviceOrient = isset($GET["deviceOrient"]) ? $GET["deviceOrient"] : "";
        $instance->OS = isset($GET["OS"]) ? $GET["OS"] : "Android";
        $instance->OSVersion = isset($GET["OSVersion"]) ? $GET["OSVersion"] : "";
        $instance->papTapVersion = isset($GET["papTapVersion"]) ? $GET["papTapVersion"] : "";
        $instance->bobileVersionNumber = isset($GET["bobileVersionNumber"]) ? $GET["bobileVersionNumber"] : 0;
        $instance->long = isset($GET["long"]) ? $GET["long"] : "";
        $instance->lati = isset($GET["lati"]) ? $GET["lati"] : "";
        $instance->phoneNumber = isset($GET["phoneNumber"]) ? $GET["phoneNumber"] : "";
        $instance->bizid = isset($GET["bizid"]) && $GET["bizid"] != "" ? $GET["bizid"] : "0";
        $instance->state = isset($GET["state"]) ? utilityManager::getStateId($GET["state"]) : "0";
        $instance->country = isset($GET["country"]) ? utilityManager::getCountryId($GET["country"]) : "0";
        $instance->category = isset($GET["category"]) && $GET["category"] != "" ? $GET["category"] : "0";
        $instance->subcategory = isset($GET["subcategory"]) && $GET["subcategory"] != "" ? $GET["subcategory"] : "0";
        $instance->mod_id = isset($GET["mod_id"]) && $GET["mod_id"] != "" ? $GET["mod_id"] : "0";
        $instance->level_no = isset($GET["level_no"]) && $GET["level_no"] != "" && $GET["level_no"] != "0" ? $GET["level_no"] : "1";
        $instance->parent = isset($GET["parent"]) && $GET["parent"] != "" ? $GET["parent"] : "0";
        $instance->in_favorites = isset($GET["in_favorites"]) && $GET["in_favorites"] != "" ? $GET["in_favorites"] : "0";
        $instance->lastid = isset($GET["lastid"])  ? $GET["lastid"] : 0;
        $instance->custid = isset($GET["custid"]) ? $GET["custid"] : '';
        $instance->searchterm = "";
        $instance->market = isset($GET["market"]) ? $GET["market"] : '';
        $instance->customerID = (!isset($GET["customerID"]) || $GET["customerID"] == "(null)" || $GET["customerID"] == 'null') ? $instance->deviceID : $GET["customerID"];
        $instance->appid = (!isset($GET["appid"]) || $GET["appid"] == 'null' || $GET["appid"] == '') ? $instance->bizid : $GET["appid"];
        $instance->base = isset($GET["image"]) ? $GET["image"] : '';
        $instance->validPhone = isset($GET["valid_phone"]) ? $GET["valid_phone"] : "";
        $instance->isMarket = (isset($GET["appid"]) && $GET["appid"] == 0) ? true : false;
        $instance->server_customer_id = isset($GET["server_customer_id"]) && $GET["server_customer_id"] != "" ? $GET["server_customer_id"] : "0";
        $instance->is_appetize = isset($GET["is_appetize"]) ? $GET["is_appetize"] : "0";
        $instance->bundleId = isset($GET["bundle_id"]) ? $GET["bundle_id"] : "";

        if(isset($GET['marketId']) && $GET['marketId'] != "" && $_REQUEST["marketId"] != 'null'){
            $instance->marketId = ($instance->appid != 0) ? "-1" : $GET['marketId'];
        }else{
            if($instance->appid == 0){
                $instance->marketId = "0";
            }else{
                $instance->marketId = "-1";
            }
        }

        if(isset($GET['resellerId'])){
            $instance->resellerId = ($instance->appid > 2) ? "-1" : $GET['resellerId'];
        }else{
            if($instance->appid == 0){
                $instance->resellerId = "0";
            }else{
                $instance->resellerId = "-1";
            }
        }

        if(isset($_REQUEST["android"])){
            $instance->OSType = (isset($_REQUEST["android"]) && $_REQUEST["android"] == "1") ? "Android" : "IOS";
        }else{
            $instance->OSType = "IOS";
        }

        if($instance->bizid == 0 && $instance->appid > 2){
            $instance->bizid = $instance->appid;
        }       
        
        if($instance->custid == "" && $instance->deviceID != "")
        {
            $instance->custid = $instance->deviceID;
        }
        
        $instance->accountID = isset($_REQUEST['accountId']) ? $_REQUEST['accountId'] : 0;
      
        return $instance;
    }

    function _isValid(){       
        return true;
    }
}
?>