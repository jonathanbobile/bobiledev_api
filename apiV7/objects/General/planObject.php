<?php 

class planObject extends bobileObject { 

    public $pl_id = "0";
    public $pl_group = "0";
    public $pl_name;
    public $pl_short_name;
    public $pl_desc;
    public $pl_type = "0";
    public $pl_entity;
    public $pl_entity_id = "0";
    public $pl_count = "0";
    public $pl_int_remarks;
    public $pl_billing_type = "0";
    public $pl_anual_freq_times = "0";
    public $pl_anual_freq_type;
    public $pl_anual_freq_name;
    public $pl_interval;
    public $pl_plrice = "0";
    public $pl_max_price = "0";
    public $pl_currency;
    public $pl_mivtza;
    public $pl_selected = "0";
    public $pl_email_template = "0";
    public $pl_grace = "30";
    public $pl_email_per = "5";
    public $pl_paypal_id;
    public $pl_active = "0";
    public $pl_head1;
    public $pl_head2;
    public $pl_head3;
    public $pl_body1;
    public $pl_body2;
    public $pl_premiuv_visible = "none";
    public $pl_tools_visible = "none";
    public $pl_go_url = "http://paptap.com/admin.php";
    public $pl_go_text = "GO TO MY APP";
    public $pl_specials_file;
    public $pl_is_submission = "0";
    public $pl_expire_time;
    public $pl_ios_color;
    public $pl_aditional_app = "0";
    public $pl_start_apps = "0";
    public $pl_belong_to_reseller = "0";

     function __construct(){} 

    public static function withData($data){

        if (!isset($data["pl_id"])){
              throw new Exception("planObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->pl_id = $data["pl_id"];
        $instance->pl_group = $data["pl_group"];
        $instance->pl_name = $data["pl_name"];
        $instance->pl_short_name = $data["pl_short_name"];
        $instance->pl_desc = $data["pl_desc"];
        $instance->pl_type = $data["pl_type"];
        $instance->pl_entity = $data["pl_entity"];
        $instance->pl_entity_id = $data["pl_entity_id"];
        $instance->pl_count = $data["pl_count"];
        $instance->pl_int_remarks = $data["pl_int_remarks"];
        $instance->pl_billing_type = $data["pl_billing_type"];
        $instance->pl_anual_freq_times = $data["pl_anual_freq_times"];
        $instance->pl_anual_freq_type = $data["pl_anual_freq_type"];
        $instance->pl_anual_freq_name = $data["pl_anual_freq_name"];
        $instance->pl_interval = $data["pl_interval"];
        $instance->pl_plrice = $data["pl_plrice"];
        $instance->pl_max_price = $data["pl_max_price"];
        $instance->pl_currency = $data["pl_currency"];
        $instance->pl_mivtza = $data["pl_mivtza"];
        $instance->pl_selected = $data["pl_selected"];
        $instance->pl_email_template = $data["pl_email_template"];
        $instance->pl_grace = $data["pl_grace"];
        $instance->pl_email_per = $data["pl_email_per"];
        $instance->pl_paypal_id = $data["pl_paypal_id"];
        $instance->pl_active = $data["pl_active"];
        $instance->pl_head1 = $data["pl_head1"];
        $instance->pl_head2 = $data["pl_head2"];
        $instance->pl_head3 = $data["pl_head3"];
        $instance->pl_body1 = $data["pl_body1"];
        $instance->pl_body2 = $data["pl_body2"];
        $instance->pl_premiuv_visible = $data["pl_premiuv_visible"];
        $instance->pl_tools_visible = $data["pl_tools_visible"];
        $instance->pl_go_url = $data["pl_go_url"];
        $instance->pl_go_text = $data["pl_go_text"];
        $instance->pl_specials_file = $data["pl_specials_file"];
        $instance->pl_is_submission = $data["pl_is_submission"];
        $instance->pl_expire_time = $data["pl_expire_time"];
        $instance->pl_ios_color = $data["pl_ios_color"];
        $instance->pl_aditional_app = $data["pl_aditional_app"];
        $instance->pl_start_apps = $data["pl_start_apps"];
        $instance->pl_belong_to_reseller = $data["pl_belong_to_reseller"];

        return $instance;
    }

    function getArrayForAdmin(){
        $planData = array();

        $planData["plan_id"] = $this->pl_id;
        $planData["plan_group"] = $this->pl_type;
        $planData["plan_name"] = $this->pl_name;
        $planData["plan_price"] = $this->pl_plrice;
        $planData["plan_color"] = $this->pl_ios_color;
        $planData["is_annual"] = ($this->pl_anual_freq_times) == "1" ? "1" : "0";
        $planData["is_selected"] = "0";

        return $planData;
    }

    function _isValid(){
          return true;
    }
}
?>


