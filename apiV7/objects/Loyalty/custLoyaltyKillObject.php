<?php 

class custLoyaltyKillObject extends bobileObject { 

    public $clk_id = "0";
    public $clk_biz_id = "0";
    public $clk_cust_id = "0";
    public $clk_cevent_id = "0";
    public $cls_killed_on;
    public $clk_kill_reason;//enum('low_priority','expired')

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["clk_id"])){
              throw new Exception("custLoyaltyKillObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->clk_id = $data["clk_id"];
        $instance->clk_biz_id = $data["clk_biz_id"];
        $instance->clk_cust_id = $data["clk_cust_id"];
        $instance->clk_cevent_id = $data["clk_cevent_id"];
        $instance->cls_killed_on = $data["cls_killed_on"];
        $instance->clk_kill_reason = $data["clk_kill_reason"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


