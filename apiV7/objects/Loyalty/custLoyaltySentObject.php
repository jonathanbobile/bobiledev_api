<?php 

class custLoyaltySentObject extends bobileObject { 

    public $cls_id = "0";
    public $cls_biz_id = "0";
    public $cls_cust_id = "0";
    public $cls_cevent_id = "0";
    public $cls_sent_on;
    public $cls_granted_entity;//enum('points','reward','push')
    public $cls_granted_entity_id = "0";
    public $cls_channel_sent;//enum('push','sms','email','whatsapp','messenger')
    public $cls_channel_send_tracking_id = "";
    public $cls_channel_sent_on;
    public $cls_channel_deliverd_on;
    public $cls_channel_opened_on;

    function __construct(){}

    public static function withData($data){

        if (!isset($data["cls_id"])){
            throw new Exception("custLoyaltySentObject. constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cls_id = $data["cls_id"];
        $instance->cls_biz_id = $data["cls_biz_id"];
        $instance->cls_cust_id = $data["cls_cust_id"];
        $instance->cls_cevent_id = $data["cls_cevent_id"];
        $instance->cls_sent_on = $data["cls_sent_on"];
        $instance->cls_granted_entity = $data["cls_granted_entity"];
        $instance->cls_granted_entity_id = $data["cls_granted_entity_id"];
        $instance->cls_channel_send_tracking_id = $data["cls_channel_send_tracking_id"];
        $instance->cls_channel_sent_on = $data["cls_channel_sent_on"];
        $instance->cls_channel_deliverd_on = $data["cls_channel_deliverd_on"];
        $instance->cls_channel_opened_on = $data["cls_channel_opened_on"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


