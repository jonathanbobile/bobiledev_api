<?php

class loyaltyQueueLogObject extends bobileObject {

    public $clql_id = "0";
    public $clql_original_clq_id = 0;
    public $clql_biz_id = "0";
    public $clql_cust_id = "0";
    public $clql_create_time;
    public $clql_send_time;
    public $clql_event_id = "0";
    public $clql_priority = "0";
    public $clql_cust_history_id = "0";
    public $clql_grant_entity;//enum('points','coupon','custom','push')
    public $clql_grant_point_amount = "0";
    public $clql_grant_entity_id = "0";
    public $clql_grant_extra_data;
    public $clql_grant_title;
    public $clql_grant_description;
    public $clql_grant_valid_from;
    public $clql_grant_valid_to;
    public $clql_message;

    function __construct(){}

    public static function withData($data){

        if (!isset($data["clql_id"])){
            throw new Exception("loyaltyQueueLogObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->clql_id = $data["clql_id"];
        $instance->clql_biz_id = $data["clql_biz_id"];
        $instance->clql_cust_id = $data["clql_cust_id"];
        $instance->clql_create_time = $data["clql_create_time"];
        $instance->clql_send_time = $data["clql_send_time"];
        $instance->clql_event_id = $data["clql_event_id"];
        $instance->clql_priority = $data["clql_priority"];
        $instance->clql_cust_history_id = $data["clql_cust_history_id"];
        $instance->clql_grant_entity = $data["clql_grant_entity"];
        $instance->clql_grant_point_amount = $data["clql_grant_point_amount"];
        $instance->clql_grant_entity_id = $data["clql_grant_entity_id"];
        $instance->clql_grant_extra_data = $data["clql_grant_extra_data"];
        $instance->clql_grant_title = $data["clql_grant_title"];
        $instance->clql_grant_description = $data["clql_grant_description"];
        $instance->clql_grant_valid_from = $data["clql_grant_valid_from"];
        $instance->clql_grant_valid_to = $data["clql_grant_valid_to"];
        $instance->clql_message = $data["clql_message"];

        return $instance;
    }

    public static function fromLoyatyQueue(loyaltyQueueObject $original){
        $instance = new self();


        $instance->clql_biz_id = $original->clq_biz_id;
        $instance->clql_original_clq_id = $original->clq_id;
        $instance->clql_cust_id = $original->clq_cust_id;
        $instance->clql_create_time = $original->clq_create_time;
        $instance->clql_send_time = $original->clq_send_time;
        $instance->clql_event_id = $original->clq_event_id;
        $instance->clql_priority = $original->clq_priority;
        $instance->clql_cust_history_id = $original->clq_cust_history_id;
        $instance->clql_grant_entity = $original->clq_grant_entity;
        $instance->clql_grant_point_amount = $original->clq_grant_point_amount;
        $instance->clql_grant_entity_id = $original->clq_grant_entity_id;
        $instance->clql_grant_extra_data = $original->clq_grant_extra_data;
        $instance->clql_grant_title = $original->clq_grant_title;
        $instance->clql_grant_description = $original->clq_grant_description;
        $instance->clql_grant_valid_from = $original->clq_grant_valid_from;
        $instance->clql_grant_valid_to = $original->clq_grant_valid_to;
        $instance->clql_message = $original->clq_message;

        return $instance;
    }

    function _isValid(){
        return true;
    }
}
?>


