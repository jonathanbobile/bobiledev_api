<?php 

class loyaltyQueueObject extends bobileObject { 

    public $clq_id = "0";
    public $clq_biz_id = "0";
    public $clq_cust_id = "0";
    public $clq_create_time;
    public $clq_send_time;
    public $clq_event_id = "0";
    public $clq_priority = "0";
    public $clq_cust_history_id = "0";
    public $clq_grant_entity;//enum('points','coupon','custom','push')
    public $clq_grant_point_amount = "0";
    public $clq_grant_entity_id = "0";
    public $clq_grant_extra_data;
    public $clq_grant_title;
    public $clq_grant_description;
    public $clq_grant_valid_from;
    public $clq_grant_valid_to;
    public $clq_message;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["clq_id"])){
            throw new Exception("loyaltyQueueObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->clq_id = $data["clq_id"];
        $instance->clq_biz_id = $data["clq_biz_id"];
        $instance->clq_cust_id = $data["clq_cust_id"];
        $instance->clq_send_time = $data["clq_send_time"];
        $instance->clq_event_id = $data["clq_event_id"];
        $instance->clq_priority = $data["clq_priority"];
        $instance->clq_cust_history_id = $data["clq_cust_history_id"];
        $instance->clq_grant_entity = $data["clq_grant_entity"];
        $instance->clq_grant_point_amount = $data["clq_grant_point_amount"];
        $instance->clq_grant_entity_id = $data["clq_grant_entity_id"];
        $instance->clq_grant_extra_data = $data["clq_grant_extra_data"];
        $instance->clq_grant_title = $data["clq_grant_title"];
        $instance->clq_grant_description = $data["clq_grant_description"];
        $instance->clq_grant_valid_from = $data["clq_grant_valid_from"];
        $instance->clq_grant_valid_to = $data["clq_grant_valid_to"];
        $instance->clq_message = $data["clq_message"];

        return $instance;
    }

    function _isValid(){
        return true;
    }
}
?>


