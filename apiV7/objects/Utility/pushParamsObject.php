<?php

/**
 * pushParamsObject short summary.
 *
 * pushParamsObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class pushParamsObject extends bobileObject
{
    public $biz_id;
    public $cust_id;
    public $push_type;
    public $message = '';
    public $extra_params = array();
    public $for = "customer";

    public static function withParams($bizID,$custID,$type,$message = '',$extraParams = array(),$for = "customer"){
        $instance= new self();

        $instance->biz_id = $bizID;
        $instance->cust_id = $custID;
        $instance->push_type = $type;
        $instance->message = $message;
        $instance->extra_params = $extraParams;
        $instance->for = $for;

        return $instance;
    }
}