<?php 

class accountActiveDeviceObject extends bobileObject { 

    public $aad_id = "0";
    public $aad_ac_id = "0";
    public $aad_device_id;
    public $aad_device_os;//enum('Android','iOS')
    public $aad_active_biz_id = "0";
    public $aad_active_position = "0";
    public $aad_logged_on = "";
    public $aad_logged_off;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["aad_id"])){
              throw new Exception("accountActiveDeviceObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->aad_id = $data["aad_id"];
        $instance->aad_ac_id = $data["aad_ac_id"];
        $instance->aad_device_id = $data["aad_device_id"];
        $instance->aad_device_os = $data["aad_device_os"];
        $instance->aad_active_biz_id = $data["aad_active_biz_id"];
        $instance->aad_active_position = $data["aad_active_position"];
        $instance->aad_logged_on = $data["aad_logged_on"];
        $instance->aad_logged_off = $data["aad_logged_off"];

        return $instance;
    }

    public function getSocketName(){
        return "admin_".utilityManager::encodeToHASH($this->aad_ac_id);
    }

    function _isValid(){
          return true;
    }
}
?>


