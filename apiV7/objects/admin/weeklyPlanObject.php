<?php

/**
 * weeklyPlanObject short summary.
 *
 * weeklyPlanObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class weeklyPlanObject extends bobileObject
{
    public $week;
    public $week_id = 0;
    public $week_no;
    public $week_year;
    public $daily_plans = array();

    public function __construct(){
        $this->week = time();
    }
}