<?php

/**
 * workItemObject short summary.
 *
 * workItemObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class workItemObject extends bobileObject
{
    public $item_id = 0;
    public $item_name = "Default value for daily action - need to change on server ";
    public $item_planning_action = "planned";
    public $item_status = "pending";
    public $item_customers_count = 0;
    public $item_source = "event";
    public $item_source_id = 1;

    public $customers = array();
}