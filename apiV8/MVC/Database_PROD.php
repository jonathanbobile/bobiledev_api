<?php

/**
 * Class Database
 * Creates a PDO database connection. This connection will be passed into the models (so we use
 * the same connection for all models and prevent to open multiple connections at once)
 */
class Database extends PDO
{
    public $queries;
    private $cureType="";
    /**
     * Construct this Database object, extending the PDO object
     * By the way, the PDO object is built into PHP by default
     */
    public function __construct($type="")
    {
        /**
         * set the (optional) options of the PDO connection. in this case, we set the fetch mode to
         * "objects", which means all results will be objects, like this: $result->user_name !
         * For example, fetch mode FETCH_ASSOC would return results like this: $result["user_name] !
         * @see http://www.php.net/manual/en/pdostatement.fetch.php
         */
        $options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);

        /**
         * Generate a database connection, using the PDO connector
         * @see http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
         * Also important: We include the charset, as leaving it out seems to be a security issue:
         * @see http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers#Connecting_to_MySQL says:
         * "Adding the charset to the DSN is very important for security reasons,
         * most examples you'll see around leave it out. MAKE SURE TO INCLUDE THE CHARSET!"
         */
        switch($type){
            case "api":
                    parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME_API . ';charset=utf8', DB_USER, DB_PASS, $options);
                break;
            case "sola":
                    parent::__construct(DB_TYPE . ':host=10.128.0.6;dbname=sola;charset=utf8', "root", "Pa55w0rd4rfv", $options);
                break;
            case "com":
                    parent::__construct(DB_TYPE . ':host=10.128.0.6;dbname=com;charset=utf8', "root", "Pa55w0rd4rfv", $options);
                break;
            case "wp":
                    parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=wp;charset=utf8', DB_USER, DB_PASS, $options);
                break;
            case "dash":
                    parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=dashboard;charset=utf8', DB_USER, DB_PASS, $options);
                break;
            case "mainForWP":
                parent::__construct('mysql:host=' . DB_HOST . ';dbname=paptap_main;charset=utf8', "root_prod", "ZRZi5Mt5Mpdw99Dg", $options);
                break;
            case "forms":
                parent::__construct('mysql:host=' . DB_HOST . ';dbname=paptap_forms;charset=utf8', "root_prod", "ZRZi5Mt5Mpdw99Dg", $options);
                break;
            case "prod":
                    parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=paptap_main;charset=utf8', DB_USER, DB_PASS, $options);
                break;
            case "backup":
                parent::__construct(DB_TYPE . ':host=35.226.154.192;dbname=paptap_main;charset=utf8', DB_USER, DB_PASS, $options);
                break;
            case "affiliate":
                parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=paptap_idevaffiliate;charset=utf8', "admin", "av1dad0n", $options);
                break;
            default:
                    parent::__construct(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=utf8', DB_USER, DB_PASS, $options);
                break;
        }    
        $this->cureType = $type;
    }


    // helper functions 

    public function query($sql)
    {
        try
        {
            $msc = microtime(true);

            $result = parent::query($sql);
            
            $msc = microtime(true) - $msc;

        }
        catch(PDOException $e)
        {
            try{
                errorManager::addErrorLog("API Vx DB",$e->getMessage(),$sql,$this->cureType);
            }
            catch (Exception $ex) {}
            //$myFile = "error-PDO.txt";
            //$myError = 'SQL Error: ' . $e->getMessage() . "\n".'Whole query: ' . $sql . "\n\n";
            //$fh = fopen($myFile, 'a');
            //fwrite($fh, $myError);
            //fclose($fh);
            
            return -1;
        }
        return $result;
    }

    public function execute($sql)
    {
        $res = $this->query($sql);
        if ($res instanceof PDOStatement) {
            return $this->lastInsertId();
        } else {
            return -1;
        }
    }

    public function getVal($sql)
    {
        $res = $this->query($sql);
        if ($res instanceof PDOStatement) {
            return $res->fetchColumn();
        } else {
            return -1;
        }
    }

    public function getRow($sql)
    {
        $res = $this->query($sql);
        if ($res instanceof PDOStatement) {
            return $res->fetch();
        } else {
            return -1;
        }
    }

    public function getTable($sql)
    {
        $res = $this->query($sql);
        if ($res instanceof PDOStatement) {
            return $res->fetchAll();
        } else {
            return -1;
        }
    }
 
}