<?php

class Admin extends Controller{

    public $mobileRequestObject = array();
    public $adminModel;

    public function __construct(){
        parent::__construct();
        set_time_limit(0);
        $this->mobileRequestObject = adminRequestObject::withData($_REQUEST);
        $this->adminModel = new adminModel($this->mobileRequestObject);
    }

    /************************************* */
    /*          ACCOUNT FUNCTIONS          */
    /************************************* */

    function getAdminVersion(){
        try{
            $result = array(
            "version" => ADMIN_VERSION,
            "url" => ADMIN_VERSION_URL
                      );

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function login(){
        try{

            $username = utilityManager::decryptWithPrivateKey($_REQUEST["username"]);
            $password = utilityManager::decryptWithPrivateKey($_REQUEST["password"]);

            if($this->mobileRequestObject->resellerId > 0){
                $partnersModel = new partnerModel();
                $resellerResult = $partnersModel->getResellerByID($this->mobileRequestObject->resellerId);

                if($resellerResult->code == 1){

                    $reseller = $resellerResult->data;
                    if($reseller->reseller_deactivated == 1){
                        return $this->returnAnswer(resultObject::withData(0,'reseller_deactivated'));
                    }

                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'no_reseller_found'));
                }
            }

            $accountManager = new accountManager();

            $accountResult = $accountManager->getAccountForLogin($username,$password,$this->mobileRequestObject->resellerId);
            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }

            $account = $accountResult->data;
            
            adminManager::logAccountOnDevice($account->ac_id,$this->mobileRequestObject->deviceID,$this->mobileRequestObject->OS);
            $this->adminModel->markAllBizsForAdmin($account->ac_id);

            $response = array();

            $response['ac_id'] = utilityManager::encodeToHASH($account->ac_id);
            $response["ac_name"]  = $account->ac_name;
            $response["ac_image"]  = $account->ac_image;
            $response["ac_last_biz"]  = utilityManager::encodeToHASH($account->ac_last_bizid);
            $response["ac_biz_count"]  = $this->adminModel->getBizCountForAccount($account->ac_id);
            $response["ac_country"] = $account->ac_country;
            $response["ac_on_other_device"] = adminManager::isAccountLoggedInOnOtherDevices($account,$this->mobileRequestObject->deviceID) ? 1 : 0;
            $tokenPayload = array();
            $tokenPayload['iss'] = JWT_ISSUER;
            $tokenPayload['iat'] = time();
            $tokenPayload['ac'] = utilityManager::encodeToHASH($account->ac_id);
            $response["token"] = encryptionManager::createJWTToken($tokenPayload);

            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function useOnThisDevice(){
        try{
            
            adminManager::logAccountOffAllOtherDevices($account->ac_id,$this->mobileRequestObject->deviceID);
            $message = array(
                "keep_device" => $this->mobileRequestObject->deviceID
                );
            adminManager::sendUpdateToAllAccountDevice($account->ac_id,"account.logout",$message);

            return $this->returnAnswer(resultObject::withData(1));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }


    function logout(){
        try{
            $response = $this->adminModel->logoutUser();
            return $this->returnAnswer($response);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function forgotPassword(){
        try{
            $accountManger = new accountManager();
            $result = $accountManger->sendForgotPasswordEmail(utilityManager::decryptWithPrivateKey($_REQUEST['email']),$this->mobileRequestObject->resellerId);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function checkToken(){
        $valid = $this->mobileRequestObject->isAuthorizedRequest();

        $code = $valid ? 1 : 0;
        $message = $valid  ? "true" : "false";


        return $this->returnAnswer(resultObject::withData($code,$message));
    }

    function getUpdateForAccount(){
        try{

            $accountManager = new accountManager();

            $accountResult = $accountManager->getAccountById($this->mobileRequestObject->acID);
            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }

            $account = $accountResult->data;
            $response = array();

            adminManager::logAccountOffDevice($account->ac_id,$this->mobileRequestObject->deviceID);
            adminManager::logAccountOnDevice($account->ac_id,$this->mobileRequestObject->deviceID,$this->mobileRequestObject->OS);

            $response['ac_id'] = utilityManager::encodeToHASH($account->ac_id);
            $response["ac_name"]  = $account->ac_name;
            $response["ac_image"]  = $account->ac_image;
            $response["ac_last_biz"]  = utilityManager::encodeToHASH($account->ac_last_bizid);
            $response["ac_biz_count"]  = $this->adminModel->getBizCountForAccount($account->ac_id);
            $response["ac_country"] = $account->ac_country;
            $response["ac_on_other_device"] = adminManager::isAccountLoggedInOnOtherDevices($account,$this->mobileRequestObject->deviceID) ? 1 : 0;
            $tokenPayload = array();
            $tokenPayload['iss'] = JWT_ISSUER;
            $tokenPayload['iat'] = time();
            $tokenPayload['ac'] = utilityManager::encodeToHASH($account->ac_id);
            $response["token"] = encryptionManager::createJWTToken($tokenPayload);

            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizListForAccount($bizEncoded = ''){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $skip = isset($_REQUEST['current_count']) ? $_REQUEST['current_count'] : 0;
            $keyword = isset($_REQUEST['name']) ? $_REQUEST['name'] : '';
            $take = 20;

            $bizID = 0;
            if($bizEncoded != ''){
                $bizID = utilityManager::decodeFromHASH($bizEncoded);
            }

            $bizList = $this->adminModel->getFullBizListForRequest($this->mobileRequestObject,$bizID,$skip,$take,$keyword);

            return $this->returnAnswer(resultObject::withData(1,'',$bizList));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function myPosition(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $position = $_REQUEST['position'];

            $bizModel = new bizModel($this->mobileRequestObject->bizID);

            $bizResult = $bizModel->getBiz();

            if($bizResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_biz'));
            }

            $accountDeviceResult = adminManager::getActiveDeviceForAccountID($this->mobileRequestObject->acID);

            if($accountDeviceResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_active_device'));
            }

            $accountDevice = $accountDeviceResult->data;

            $accountDevice->aad_active_biz_id = $this->mobileRequestObject->bizID;
            $accountDevice->aad_active_position = $position;

            adminManager::updateAccountActiveDevice($accountDevice);

            $accountManager = new accountManager();
            $accountResult = $accountManager->getAccountById($this->mobileRequestObject->acID);
            if($accountResult->code != 1){
                return $this->returnAnswer($accountResult);
            }

            $account = $accountResult->data;

            bizManager::setAccountLoggedToBiz($account,$this->mobileRequestObject->bizID);

            $biz = $bizResult->data;

            $result = array();
            switch($biz->getBizBusinessType()){
                case enumBusinessTypes::service_providers:
                case enumBusinessTypes::dining:
                case enumBusinessTypes::retail:
                    switch($position){
                        case 1:
                        case 4:
                        case 7://front desk
                            $offeringSections = $bizModel->getPOSOfferingSections()->data;

                            //getBizOffering(section) x10 --> Mobile should go to this API Async

                            //metaDataAPIs:
                            $metaDataAPIs = array();
                            foreach ($offeringSections as $section)
                            {
                                $entry = array();
                                if($section['baos_section_items_type'] != 'all'){
                                    $entry['dataAPI'] = 'getBizOfferings';
                                }
                                else{
                                    $entry['dataAPI'] = 'getFavoriteOfferings';
                                }
                                $entry['modID'] = $section['baos_section_mod_id'];
                                $entry['done'] = 0;
                                $metaDataAPIs[] = $entry;
                            }
                            //shop categories
                            $entry = array();
                            $entry['dataAPI'] = 'getBizShopCategories';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //biz groups
                            $entry = array();
                            $entry['dataAPI'] = 'getBizGroups';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizMembersList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizMembersList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveEmployeesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveEmployeesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveResourcesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveResourcesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizServicesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizServicesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveClassList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveClassList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveEventsList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveEventsList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveWorkshopList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveWorkshopList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            $result['offerings_section'] = $offeringSections;
                            $result['required_data'] = $metaDataAPIs;
                            break;
                        case 2:
                        case 8:// service giver
                            $offeringSections = $bizModel->getPOSOfferingSections()->data;

                            //getBizOffering(section) x10 --> Mobile should go to this API Async

                            //metaDataAPIs:
                            $metaDataAPIs = array();
                            foreach ($offeringSections as $section)
                            {
                                $entry = array();
                                if($section['baos_section_items_type'] != 'all'){
                                    $entry['dataAPI'] = 'getBizOfferings';
                                }
                                else{
                                    $entry['dataAPI'] = 'getFavoriteOfferings';
                                }
                                $entry['modID'] = $section['baos_section_mod_id'];
                                $entry['done'] = 0;
                                $metaDataAPIs[] = $entry;
                            }
                            //getBizMembersList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizMembersList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveEmployeesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveEmployeesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveResourcesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveResourcesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizServicesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizServicesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveClassList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveClassList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveEventsList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveEventsList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveWorkshopList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveWorkshopList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            $result['offerings_section'] = $offeringSections;
                            $result['required_data'] = $metaDataAPIs;
                            break;
                        case 3:
                        case 6:
                        case 11://back office
                            break;
                        case 9: //kitchen
                        case 5://order handling
                            $offeringSections = $bizModel->getPOSOfferingSections()->data;

                            //getBizOffering(section) x10 --> Mobile should go to this API Async

                            //metaDataAPIs:
                            $metaDataAPIs = array();
                            foreach ($offeringSections as $section)
                            {
                                $entry = array();
                                if($section['baos_section_items_type'] != 'all'){
                                    $entry['dataAPI'] = 'getBizOfferings';
                                }
                                else{
                                    $entry['dataAPI'] = 'getFavoriteOfferings';
                                }
                                $entry['modID'] = $section['baos_section_mod_id'];
                                $entry['done'] = 0;
                                $metaDataAPIs[] = $entry;

                            }
                            //getBizMembersList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizMembersList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            //getBizActiveEmployeesList
                            $entry = array();
                            $entry['dataAPI'] = 'getBizActiveEmployeesList';
                            $entry['modID'] = 0;
                            $entry['done'] = 0;
                            $metaDataAPIs[] = $entry;
                            $result['offerings_section'] = $offeringSections;
                            $result['required_data'] = $metaDataAPIs;
                            break;
                        case 10: //delivery
                            break;
                    }
                    break;
                //case enumBusinessTypes::dining:
                //    break;
                //case enumBusinessTypes::retail:
                //    break;
            }

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    /************************************* */
    /*          FRONT DESK FUNCTIONS       */
    /************************************* */

    function getBizOfferings($modID = 0){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;


            $result = $bizModel->getBizOfferings($modID,$skip);
            return $this->returnAnswerWithCaller($result,$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizShopCategories(){
        try{
            $shopManager = new shopManager();

            $categories = $shopManager->getCategoriesForBizByBizID($this->mobileRequestObject->bizID);

            $result = array();

            foreach ($categories->data as $category)
            {
            	$result[] = $category->AdminAPIArray();
            }

            return $this->returnAnswerWithCaller(resultObject::withData(1,'',$result),$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizGroups(){
        try{

            $result = array();
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $groupsResult = $bizModel->getGroupsForBizByBizID();

            if($groupsResult->code == 1){

                foreach ($groupsResult->data as $group)
                {
                    $result[] = $group->AdminAPIArray();
                }

                return $this->returnAnswerWithCaller(resultObject::withData(1,'',$result),$this->mobileRequestObject->caller);
            }else{
                return $this->returnAnswer($groupsResult);
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getFavoriteOfferings(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $result = $bizModel->getBizFavoriteOfferings();
            return $this->returnAnswerWithCaller($result,$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizActiveEmployeesList(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;

            $accountID = isset($_REQUEST["account_only"]) && $_REQUEST["account_only"] == 1 ? $this->mobileRequestObject->acID : 0;

            $result = $bizModel->getAdminEmployeesList($skip,$accountID);

            return $this->returnAnswerWithCaller($result,$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizServicesList(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;

            $result = $bizModel->getAdminServicesList($skip);

            return $this->returnAnswerWithCaller($result,$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizActiveClassList(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;

            $result =$bizModel->getAdminClassesList($skip);

            return $this->returnAnswerWithCaller($result,$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizActiveResourcesList(){//Not ready yet
        $this->returnAnswerWithCaller(resultObject::withData(1,'not_ready',array()),$this->mobileRequestObject->caller);
    }

    function getBizActiveEventsList(){//Not ready yet
        $this->returnAnswerWithCaller(resultObject::withData(1,'not_ready',array()),$this->mobileRequestObject->caller);
    }

    function getBizActiveWorkshopList(){//Not ready yet
        $this->returnAnswerWithCaller(resultObject::withData(1,'not_ready',array()),$this->mobileRequestObject->caller);
    }


    /************************************* */
    /*          POS FUNCTIONS              */
    /************************************* */

    function getShopItemData(){
        try{
            $itemID = $_REQUEST['itemID'];

            $levelManager = new levelDataManager(9);

            $product = $levelManager->getAdminProduct($itemID);

            return $this->returnAnswer($product);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getClientRewards(){
        try{
            $clientID = $_REQUEST['clientID'];
            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($clientID);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $benefitsResult = $customerModel->getCustomerActiveBenefitsForAdmin($customer,0,0);

            if($benefitsResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_rewards'));
            }

            $outData = array();
            $outData['clientID'] = $_REQUEST['clientID'];
            $outData['rewards'] = $benefitsResult->data;

            return $this->returnAnswer($benefitsResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function calculateShipping(){
        try{
            $shopManager = new shopManager();
            $itemsList = json_decode(stripslashes(str_replace('\n','',$_REQUEST["items"])));

            $country = $_REQUEST["shipCountry"];
            $tax = $_REQUEST["tax"];
            $shipableItems = array();

            foreach ($itemsList as $item)//only products can be shipped
            {
            	if($item->item_type == 2){
                    $shipableItems[] = $item;
                }
            }

            $result = $shopManager->calculateShipping($this->mobileRequestObject->bizID,$country,$tax,$shipableItems);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function checkCouponAvailabilty(){
        try{
            $couponID = $_REQUEST['couponID'];

            $couponLimit = couponsManager::getCouponLimitations($couponID);
            $couponTotalLimit = couponsManager::getCouponTotalLimitations($couponID);

            if($couponLimit == 0 && $couponTotalLimit == 0){//coupon has no limits
                return $this->returnAnswer(resultObject::withData(1));
            }
            $memberID = $_REQUEST['memberID'];
            $customerClaimedCount = couponsManager::getClaimedCoupoonsForCustomerByCoupon($couponID,$memberID);

            if($couponLimit > 0 && $customerClaimedCount >= $couponLimit){
                return $this->returnAnswer(resultObject::withData(1,'exceeded_limit'));
            }

            $totalCouponClaimedCount = couponsManager::getClaimedCouponsByItem($couponID);

            if($couponTotalLimit > 0 && $totalCouponClaimedCount >= $couponTotalLimit){
                return $this->returnAnswer(resultObject::withData(1,'exceeded_limit'));
            }

            return $this->returnAnswer(resultObject::withData(1));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setClientOrder(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['custID'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            $ipAddress = utilityManager::get_real_IP();

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $customerBilling = new customerBillingManager();

            $response = array();

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_account_id = $this->mobileRequestObject->acID;

            //Set due date
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettingsResult = $bizModel->getBizStoreSettings();
            $storeSettings = $storeSettingsResult->data;

            $orderTime = new DateTime($orderObject->cto_order_tyme);
            $addHours = ($storeSettings->ess_shipping_time * 24);
            $orderTime->add(new DateInterval("PT{$addHours}H"));
            $orderObject->cto_due_date = $orderTime->format('Y-m-d H:i:s');

            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }

            //update customer details
            $buyName = addslashes(urldecode($_REQUEST["name"]));

            $customer->cust_first_name = $buyName;
            $customer->cust_email = $_REQUEST['email'];
            if(isset($_REQUEST['phone']) && $_REQUEST['phone'] != ''){
                $customer->cust_phone2 = "+".utilityManager::getValidPhoneNumber($_REQUEST['phone']);
            }

            $customerModel->updateCustomer($customer);

            //check validity
            if(!$orderObject->_isValid()){
                return $this->returnAnswer(resultObject::withData(-1,"order_invalid"));
            }

            //create order
            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,"order_not_created"));
            }

            $orderObject = $orderResult->data;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();


            foreach ($jarray as $item)
            {

                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustSubscription($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustPunchPass($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_punchpass_usage'));
                        }
                    }



                }

                $itemObjectsList[] = $itemObject;
            }

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount <= 0){
                utilityManager::asyncProcessAdminOrder($orderObject->cto_id);
                eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'','',$orderObject->cto_id);

                $response['code'] = 1;
                $response["order_id"] = $orderObject->cto_id;
                $response["long_order_id"] = $orderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($orderObject->cto_id,$invoiceID);

            $_REQUEST["ipAddress"] = $ipAddress;
            $transaction = customerTransactionObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $transaction->tr_cust_id = $customer->cust_id;
            $transaction->tr_biz_id = $customer->cust_biz_id;
            $transaction->tr_invoice_id = $invoiceID;
            $transaction->tr_country = bizManager::getBizCountryID($this->mobileRequestObject->bizID);

            if($transaction->tr_paymentMethodType != "Cash"){
                $chargeAttempt = $customerBilling->addDirectSuccesfulCustomerTransaction($transaction);
            }
            else{
                $customerBilling->setCustInvoicePendingByInvoiceID($invoiceID);
                $customerBilling->settleInvoiceWithCashTransaction($invoiceID,$transaction);
                $chargeAttempt = resultObject::withData(1);
            }

            if($chargeAttempt->code == 1){
                utilityManager::asyncProcessAdminOrder($orderObject->cto_id);
                eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'','',$orderObject->cto_id);
                $response['code'] = 1;
                $response["order_id"] = $orderObject->cto_id;
                $response["long_order_id"] = $orderObject->cto_long_order_id;
                $response['charge_code'] = $chargeAttempt->code;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }
            else{
                $response['code'] = 0;
                $response['charge_code'] = $chargeAttempt->code;
                $response['charge_message'] = $chargeAttempt->message;
                return $this->returnAnswer(resultObject::withData(0,'',$response));
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    public function setClientOrderStripe(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $fh = fopen("debugProcessRequest.txt", 'w');
            fwrite($fh, print_r($_REQUEST,true)."\n");
            fclose($fh);

            $customersModel = new customerModel();
            $cust_id = $_REQUEST['custID'];
            $customerResult = $customersModel->getCustomerWithID($cust_id);

            $ipAddress = utilityManager::get_real_IP();

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);

            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_account_id = $this->mobileRequestObject->acID;

            //Set due date
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettingsResult = $bizModel->getBizStoreSettings();
            $storeSettings = $storeSettingsResult->data;

            $orderTime = new DateTime($orderObject->cto_order_tyme);
            $addHours = ($storeSettings->ess_shipping_time * 24);
            $orderTime->add(new DateInterval("PT{$addHours}H"));
            $orderObject->cto_due_date = $orderTime->format('Y-m-d H:i:s');

            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'cart_coupon_redeemed_already'));
                }
            }



            //check validity
            if(!$orderObject->_isValid()){
                return $this->returnAnswer(resultObject::withData(0,'order_invalid'));
            }

            $customerBilling = new customerBillingManager();
            $customerBilling->setBillingBizID($customer->cust_biz_id);


            if($customer->cust_stripe_cust_id == ''){//need to create customer account in stripe
                $stripeAddResult = $customerBilling->addCustomerToStripe($customer);
                if($stripeAddResult->code == 1){
                    if($customer->cust_stripe_cust_id == ""){
                        $customer->cust_stripe_cust_id = $stripeAddResult->data;

                        $customersModel->updateCustomer($customer);
                    }
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,"cant_add_to_stripe"));
                }
            }



            $paymentMethodId = "";
            if(isset($_REQUEST['payment_method_id']) && $_REQUEST['payment_method_id'] != ""){//new payment method - add to DB
                $payMethodResult = $customerBilling->getPaymentMethodDetails($customer,$_REQUEST['payment_method_id']);

                if($payMethodResult->code == 1){
                    $payMethod = $payMethodResult->data;

                    $customerBilling->attachPaymentMethodToCustomer($customer,$payMethod);

                    $paymentMethodId = $_REQUEST['payment_method_id'];

                    customerManager::sendAsyncCustomerDataUpdateToDevice('default_payment',$customer->cust_id);
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,"Error",$payMethodResult));
                }

            }
            else{
                $defaultPaySourceResult = $customerBilling->getCustomerDefaultPaymentSource($customer->cust_id);

                if($defaultPaySourceResult->code == 1){
                    $paymentMethodId = $defaultPaySourceResult->data->stripe_card_id;
                }
            }

            if($paymentMethodId == "" && $orderObject->amount > 0){
                return $this->returnAnswer(resultObject::withData(0,$this->language->get('no_pay_source_selected')));
            }

            $response = array();
            $isRecurring = false;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();

            foreach ($jarray as $item)
            {
                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                //Check if subscription or punchpass uses are valid
                if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';

                        $subUseValid = customerSubscriptionManager::checkCustSubscriptionUsageIsValid($usageObject);

                        if($subUseValid->code != 1){
                            return $this->returnAnswer(resultObject::withData(0,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';


                        $punchUseValid = customerSubscriptionManager::checkCustPunchpassusageIsvalid($usageObject);

                        if($punchUseValid->code != 1){
                            return $this->returnAnswer(resultObject::withData(0,'no_punchpass_usage'));
                        }
                    }


                }

                if($itemObject->tri_item_type == 4){//is a subscription - recurring payment
                    $isRecurring = true;
                }

                $itemObjectsList[] = $itemObject;
            }

            $orderObject->items = $itemObjectsList;




            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount <= 0){
                //create order
                $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

                if($orderResult->code == 0){
                    return $this->returnAnswer(resultObject::withData(0,''));
                }

                $orderObject = $orderResult->data;

                //connect items to order
                customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

                utilityManager::asyncProcessAdminOrder($orderObject->cto_id);
                eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$orderObject->cto_id);
                $response['code'] = 1;
                $response["order_id"] = $orderObject->cto_id;
                $response["long_order_id"] = $orderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            if(!isset($_REQUEST['pay_intent_id']) || $_REQUEST['pay_intent_id'] == ""){

                $stripePayIntentResult = $customerBilling->createPaymentIntent($customer,$orderObject,$paymentMethodId);

                if($stripePayIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(0,"Error",$stripePayIntentResult));
                }

                $payIntentResult = $customerBilling->getStripePaymentIntent($stripePayIntentResult->data['intentId']);

                if($payIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(0,"Error",$payIntentResult));
                }
                $payIntent = $payIntentResult->data;

                if($isRecurring){
                    $customerBilling->updateStripePaymentMethodToRecurring($payIntent->id);
                }

            }
            else{
                $stripeIntentResult = $customerBilling->getStripePaymentIntent($_REQUEST['pay_intent_id']);
                if($stripeIntentResult->code == 1){
                    $payIntent = $stripeIntentResult->data;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'error_pay_intent_id'));
                }
            }



            $stripeConfrimResult = $customerBilling->confirmPaymentIntent($payIntent);

            if($stripeConfrimResult->code == 0){

                $response['code'] = 0;
                $response['charge_code'] = 0;
                $response['charge_message'] = "charge_message";
                $response['pay_intent_id'] = $stripeConfrimResult->data["intentId"];
                $response['client_secret'] = $stripeConfrimResult->data["clientSecret"];
                $response['is_action_required'] = 1;
                return $this->returnAnswer(resultObject::withData(-1,'',$response));

            }else if($stripeConfrimResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,"Error",$stripeConfrimResult));
            }else{
                $_REQUEST['pay_intent_id'] = $payIntent->id;
            }
            //Paymnet was successful - create DB rows
            //create order

            //Add any subscription or punch pass uses
            foreach ($orderObject->items as $key => $itemObject)
            {
            	if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';

                        $subUse = customerSubscriptionManager::useCustSubscription($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(0,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';


                        $subUse = customerSubscriptionManager::useCustPunchPass($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(0,'no_punchpass_usage'));
                        }
                    }

                    $orderObject->items[$key] = $itemObject;

                }

            }

            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,''));
            }

            $orderObject = $orderResult->data;

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($orderObject->cto_id,$invoiceID);

            //Mark invoice as paid
            $chargCode = $customerBilling->markCustomerInvoicePaidFromPaymentIntent($invoiceID,$payIntent,$this->mobileRequestObject->deviceID);

            //Process order and
            utilityManager::asyncProcessAdminOrder($orderObject->cto_id);


            //End purchase successfully
            eventManager::actionTrigger(enumCustomerActions::completedPurchase,$cust_id, "order",'',$this->mobileRequestObject->deviceID,$orderObject->cto_id);
            $response['code'] = 1;
            $response["order_id"] = $orderObject->cto_id;
            $response["long_order_id"] = $orderObject->cto_long_order_id;
            $response['charge_code'] = $chargCode->code;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function memberHasMultiuseForOfferingItem(){
        try{
            $custID = $_REQUEST['custID'];
            $offeringID = $_REQUEST['itemID'];

            $bizModel = new bizModel($this->mobileRequestObject->bizID);

            $offeringResult = $bizModel->getBizOfferingByID($offeringID);

            if($offeringResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_item'));

            }

            $offering = $offeringResult->data;

            $type = $offering->bao_source;

            if($type == "appointment"){
                $type = "service";
            }

            $result = customerSubscriptionManager::getAllActiveCustomerSubscriptionsForItem($custID,$type,$offering->bao_source_row_id);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*    MEMBERS FUNCTIONS                */
    /************************************* */

    function addMember(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $customerModel = new customerModel();

            $data = array();

            $data['phone'] = $_REQUEST['phone'];
            $data['name'] = $_REQUEST['name'];
            $data['email'] = $_REQUEST['email'];

            $existingCustResult = $customerModel->getCustIDByValidPhoneNumberForAdmin($data['phone'],$this->mobileRequestObject);

            $custID = 0;
            $existed = false;
            $result = array();
            if($existingCustResult->code == 1 && $existingCustResult->data > 0){//customer exists with phone number
                $custID = $existingCustResult->data;
                $existed = true;
            }
            else{
                $newMemberResult = $customerModel->addMemberFromAdminApp($this->mobileRequestObject,$data);
                if($newMemberResult->code == 1){
                    $custID = $newMemberResult->data;
                }
            }

            if($custID == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_member_found_or_created'));
            }


            $memberResult = $customerModel->getMemberForAdminCustID($custID);
            $result['member'] = $memberResult->data;
            $result['existed'] = $existed ? 1 : 0;

            $members = array();
            $members[] = $memberResult->data;
            adminManager::sendUpdateToBizDevices($this->mobileRequestObject->bizID,"members.new",$members);

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getBizMembersList(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;

            $members = $bizModel->getBizMembers($skip);

            return $this->returnAnswerWithCaller($members,$this->mobileRequestObject->caller);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*    CUSTOMER CLAIMS FUNCTIONS        */
    /************************************* */

    function getCustomerOpenClaims(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custID = $_REQUEST['custID'];

            $claims = customerManager::getCustomerOpenClaimsbyCustID($custID);

            $result = array();

            foreach ($claims as $claim)
            {
            	$result[] = $claim->getAdminFormattedArray();
            }

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getCustomerOpenClaimsFromOrder(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custID = $_REQUEST['custID'];
            $orderLongID = $_REQUEST['orderID'];
            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                 return $this->returnAnswer(resultObject::withData(0,'no_order'));
            }
            $order = $orderResult->data;
            $claims = customerManager::getCustomerOpenClaimsbyCustIDAndOrderID($custID,$order->cto_id);

            $result = array();

            foreach ($claims as $claim)
            {
            	$result[] = $claim->getAdminFormattedArray();
            }

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getCustomerOpenClaimsForItem(){
        try{
            $itemSource = $_REQUEST['source'];
            $itemID = $_REQUEST['itemID'];
            $custID = $_REQUEST['custID'];

            $claims = customerManager::getCustomerOpenClaimsbyCustIDAndItem($custID,$itemSource,$itemID);

            $result = array();

            foreach ($claims as $claim)
            {
            	$result[] = $claim->getAdminFormattedArray();
            }

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setCustomerClaimUsed(){
        try{
            $claimID = $_REQUEST['claimID'];
            $fulfillRow = isset($_REQUEST['fulfillID']) ? $_REQUEST['fulfillID'] : 0;

            $claimResult = customerManager::getCustomerClaimByID($claimID);

            if($claimResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_claim'));
            }

            $claim = $claimResult->data;

            $claim->cc_was_fulfilled = 1;

            $now = new DateTime();
            $claim->cc_fulfilled_on = $now->format('Y-m-d H:i:s');
            $claim->cc_fulfillment_row = $fulfillRow;

            customerManager::updateCustomerClaim($claim);

            return $this->returnAnswer(resultObject::withData(1,'',$claim->getAdminFormattedArray()));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*          ORDERS FUNCTIONS           */
    /************************************* */

    function getOrders(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;
            $from = $_REQUEST["from"];
            $to = $_REQUEST["to"];

            $filter = isset($_REQUEST['filter']) && $_REQUEST['filter'] != "" ? $_REQUEST['filter'] : "";

            $result = customerOrderManager::getOrdersForAdminForBiz($this->mobileRequestObject->bizID,$from,$to,$filter,$skip);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getOrdersForHandling(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;
            $from = $_REQUEST["from"];
            $to = $_REQUEST["to"];

            $filter = isset($_REQUEST['filter']) && $_REQUEST['filter'] != "" ? $_REQUEST['filter'] : "";

            $result = customerOrderManager::getOrdersForAdminHandlingForBiz($this->mobileRequestObject->bizID,$from,$to,$filter,$skip);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getOrder(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            $result = $order->AdminSingleOrderAPIArray();

            return $this->returnAnswer(resultObject::withData(1,'',$result));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function updateOrderDeliveryStatus(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;
            $status = $_REQUEST['status'];
            $notes = isset($_REQUEST['notes']) ? $_REQUEST['notes'] : "";
            $accountID = $this->mobileRequestObject->acID;

            $order->cto_status = $status;

            if($status == 4){//order was shipped - update delivery notes
                $order->cto_delivery_details = $notes;
            }

            $updateOrderResult = customerOrderManager::updateCustomerOrder($order);

            if($updateOrderResult->code != 1){
                return $this->returnAnswer($updateOrderResult);
            }

            $statusHistory = new customerOrderStatusHistory();

            $statusHistory->ctoh_order_id = $order->cto_id;
            $statusHistory->ctoh_status_account_id = $accountID;
            $statusHistory->ctoh_status_id = $status;
            $statusHistory->ctoh_status_notes = $notes;

            $statusResult = customerOrderManager::addCustomerOrderStatusHistory($statusHistory);

            if($statusResult->code != 1){
                return $this->returnAnswer($statusResult);
            }

            $result = customerOrderManager::getCustomerOrderStatusHistoryByID($statusResult->data);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setOrderAsCancelled(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;
            $notes = isset($_REQUEST['notes']) ? $_REQUEST['notes'] : "";
            $accountID = $this->mobileRequestObject->acID;

            $result = customerOrderManager::updateOrderDeliveryStatus($order,enumOrderStatus::cancelled,$accountID,$notes);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function addOrderDeliveryStatusNote(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;
            $notes = isset($_REQUEST['notes']) ? $_REQUEST['notes'] : "";
            $accountID = $this->mobileRequestObject->acID;

            $statusHistory = new customerOrderStatusHistory();

            $statusHistory->ctoh_order_id = $order->cto_id;
            $statusHistory->ctoh_status_id = $order->cto_status;
            $statusHistory->ctoh_status_notes = $notes;
            $statusHistory->ctoh_status_account_id = $accountID;

            $statusResult = customerOrderManager::addCustomerOrderStatusHistory($statusHistory);

            if($statusResult->code != 1){
                return $this->returnAnswer($statusResult);
            }

            $result = customerOrderManager::getCustomerOrderStatusHistoryByID($statusResult->data);

            if($result->code == 1){
                $log = $result->data->AdminAPIArray();
                return $this->returnAnswer(resultObject::withData($result->code,$result->message,$log));
            }
            else{
                return $this->returnAnswer($result);
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function editOrderDeliveryInfo(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;
            $notes = isset($_REQUEST['notes']) ? $_REQUEST['notes'] : "";
            $accountID = $this->mobileRequestObject->acID;

            $statusHistory = new customerOrderStatusHistory();

            $statusHistory->ctoh_order_id = $order->cto_id;
            $statusHistory->ctoh_status_id = $order->cto_status;
            $statusHistory->ctoh_status_notes = $notes;
            $statusHistory->ctoh_status_account_id = $accountID;

            $order->cto_delivery_details = $notes;

            customerOrderManager::addCustomerOrderStatusHistory($statusHistory);

            $result = customerOrderManager::updateCustomerOrder($order);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function editOrderTrackingID(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            $tracking_id = isset($_REQUEST['tracking_id']) ? $_REQUEST['tracking_id'] : "";

            $order->cto_delivery_tracking_id = $tracking_id;

            $result = customerOrderManager::updateCustomerOrder($order);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function sendOrderInvoiceEmail(){
        try{
            $orderLongID = $_REQUEST['orderID'];
            $email = urldecode($_REQUEST['email']);

            $result = customerOrderManager::sendCustomerOrderEmailByOrderLongID($orderLongID,$email);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function sendSingleInvoicePDFEmail(){
        try{
            $invoiceLongID = $_REQUEST['invoiceID'];
            $email = $_REQUEST['email'];

            $customerBilling = new customerBillingManager();
            $result = $customerBilling->sendCustomerInvoicePDFEmail($invoiceLongID,$email);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getOrderForHandling(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['orderID'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            $result = $order->AdminAPIOrderHandlingArray();

            return $this->returnAnswer(resultObject::withData(1,'',$result));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getOrdersStatusesDistribution(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $result = customerOrderManager::getOrdersDistribuationByStatusForBizID($this->mobileRequestObject->bizID);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function searchOrders(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $skip = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;
            $from = $_REQUEST["from"];
            $to = $_REQUEST["to"];

            $key = isset($_REQUEST['key']) && $_REQUEST['key'] != "" ? $_REQUEST['key'] : "";

            $result = customerOrderManager::getOrdersSearchForAdminForBiz($this->mobileRequestObject->bizID,$from,$to,$key,$skip);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*          INVOICE FUNCTIONS          */
    /************************************* */

    function getInvoice(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $invoiceLongID = $_REQUEST['invoiceID'];

            $customerBilling = new customerBillingManager();

            $invoiceResult = $customerBilling->getCustomerInvoiceByLongID($invoiceLongID);

            if($invoiceResult->code != 1){
                return $this->returnAnswer($invoiceResult);
            }

            $invoice = $invoiceResult->data;

            $result = $invoice->AdminAPIArray();

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function changeInvoiceStatus(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $invoiceLongID = $_REQUEST['invoiceID'];
            $invoiceStatus = $_REQUEST['status'];
            $notes = isset($_REQUEST['notes']) ? $_REQUEST['notes'] : "";

            $customerBilling = new customerBillingManager();
            $invoiceResult = $customerBilling->getCustomerInvoiceByLongID($invoiceLongID);

            if($invoiceResult->code != 1){
                return $this->returnAnswer($invoiceResult);
            }

            $invoice = $invoiceResult->data;
            $invoice->cin_status = $invoiceStatus;
            $result = $customerBilling->updateCustomerInvoice($invoice);

            if($result->code == 1){
                $customerBilling->addManualCustInvoiceHistoryEntry($invoice->cin_id,$invoiceStatus,$notes);
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function refundInvoice(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $invoiceLongID = $_REQUEST['invoiceID'];
            $ammount = isset($_REQUEST['amount']) && $_REQUEST['amount'] != "" ? $_REQUEST['amount'] : 0;

            $customerBilling = new customerBillingManager();
            $invoiceResult = $customerBilling->getCustomerInvoiceByLongID($invoiceLongID);

            if($invoiceResult->code != 1){
                return $this->returnAnswer($invoiceResult);
            }

            $invoice = $invoiceResult->data;

            $result = $customerBilling->refundCustomerInvoice($invoice,$ammount);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*          BOOKINGS FUNCTIONS         */
    /************************************* */

    function getSchedules(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $from = $_REQUEST["from"];
            $to = $_REQUEST["to"];
            $filter = isset($_REQUEST['filter']) && $_REQUEST['filter'] != "" ? json_decode($_REQUEST['filter']) : array();

            $result = bookingManager::getAdminSchedule($this->mobileRequestObject->bizID,$from,$to,$filter);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getSchedulerList(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $from = $_REQUEST["from"];
            $existing = isset($_REQUEST["existing"]) ? $_REQUEST["existing"] : 0;
            $firstLoad = isset($_REQUEST["first_load"]) ? $_REQUEST["first_load"] == 1 : false;
            $direction = isset($_REQUEST["direction"]) ? $_REQUEST["direction"] : "forward";
            $filter = isset($_REQUEST['filter']) && $_REQUEST['filter'] != "" ? json_decode($_REQUEST['filter']) : array();

            if($firstLoad){
                $backResult = bookingManager::getAdminSchedulerList($this->mobileRequestObject->bizID,$from,"back");
                $forwardResult = bookingManager::getAdminSchedulerList($this->mobileRequestObject->bizID,$from,"forward");

                $fullList = array_merge($backResult->data,$forwardResult->data);

                $result = resultObject::withData(1,'',$fullList);
            }
            else{
                $result = bookingManager::getAdminSchedulerList($this->mobileRequestObject->bizID,$from,$direction,$existing,$filter);
            }

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function searchSchedulerList(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $from = $_REQUEST["from"];
            $existing = $_REQUEST["existing"];
            $key = $_REQUEST['key'];
            $result = array();
            $appointmentResult = bookingManager::searchAdminSchedulerList($this->mobileRequestObject->bizID,$from,$existing,$key);
            $result['appointments'] = $appointmentResult->data;

            $classResult = bookingManager::searchBizClassesList($this->mobileRequestObject->bizID,$key);
            $result['classes'] = $classResult->data;

            return $this->returnAnswer(resultObject::withData(1,'',$result));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getEmployeesSchedules(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $date = $_REQUEST['date'];
            $filter = isset($_REQUEST['filter']) && $_REQUEST['filter'] != "" ? json_decode($_REQUEST['filter']) : array();

            $result = bookingManager::getAdminEmployeesSchedule($this->mobileRequestObject->bizID,$date,$filter);

            return $this->returnAnswer($result);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function verifyTimeIsAvailableForMeeting(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            date_default_timezone_set("UTC");

            $customerModel = new customerModel();
            $meeting = new employeeMeetingObject();

            $meeting->em_emp_id = $_REQUEST["emp_id"];
            $meeting->em_meet_type = $_REQUEST["meet_type"];
            $meeting->em_name = addslashes($_REQUEST["meet_name"]);
            $meeting->em_start_time_stamp = $_REQUEST["start_time"];
            $meeting->em_end_time_stamp = $_REQUEST["end_time"];
            $meeting->em_cust_request = addslashes($_REQUEST["custom_text"]);
            $meeting->em_location = addslashes($_REQUEST["location"]);
            $meeting->em_payment_source = isset($_REQUEST['payment_source']) && $_REQUEST['payment_source'] != '' ? $_REQUEST['payment_source'] : 'none';
            $meeting->em_payment_source_id = isset($_REQUEST['payment_source_id']) && $_REQUEST['payment_source_id'] != '' ? $_REQUEST['payment_source_id'] : 0;

            if($meeting->em_payment_source_id == 0){
                $meeting->em_payment_source = "none";
            }

            $meeting->em_biz_id = $this->mobileRequestObject->bizID;
            $meeting->em_from_mobile = 0;
            $meeting->em_cust_id = isset($_REQUEST["cust_id"]) ? $_REQUEST["cust_id"] : 0;
            $meeting->em_is_recurring = $_REQUEST['recurring'];
            $meeting->em_recurring_limit_method = isset($_REQUEST['recurring_method']) ? $_REQUEST['recurring_method'] : "automatic";
            $meeting->em_recurring_amount = isset($_REQUEST['recurring_amount']) ? $_REQUEST['recurring_amount'] : 0;
            $meeting->em_recurring_end_date_stamp = isset($_REQUEST['recurring_end']) ? $_REQUEST['recurring_end'] : 0;

            $responce = new stdClass();

            if ($this->mobileRequestObject->bizID == "0"){
                $responce->result["code"] = "1";
                $responce->result["value"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No bizid";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_emp_id == "0"){
                $responce->result["code"] = "2";
                $responce->result["value"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Emplyee";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_meet_type == "0"){
                $responce->result["code"] = "3";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Meeting Type";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_name == ""){
                $responce->result["code"] = "4";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Meeting Name";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_start_time_stamp == "" || $meeting->em_start_time_stamp == "(null)" || $meeting->em_start_time_stamp == "0"){
                $responce->result["code"] = "5";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Start Time";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_end_time_stamp == "" || $meeting->em_end_time_stamp == "(null)" || $meeting->em_end_time_stamp == "0"){
                $responce->result["code"] = "6";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No End Time";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }


            if($meeting->em_cust_id > 0){
                $customerResult = $customerModel->getCustomerWithID($meeting->em_cust_id);

                if ($customerResult->code == 0){
                    $responce->result["code"] = "7";
                    $responce->result["meeting_id"] = 0;
                    $responce->result["localized_no"] = "201";
                    $responce->result["internal_desc"] = "No Customer Found";
                    return $this->returnAnswer(resultObject::withData(0,'',$responce));
                }

                $meeting->setCustomer($customerResult->data);
            }

            $result = bookingManager::verifyMeetingTimeAvailable($meeting);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getMeeting(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }



            $meetID = $_REQUEST['meetingID'];

            $meetingResult = bookingManager::getEmployeeMeetingByID($meetID);

            if($meetingResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_meeting'));
            }

            $meeting = $meetingResult->data;

            return $this->returnAnswer(resultObject::withData(1,'',$meeting->adminAPIArray()));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setMeeting(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            date_default_timezone_set("UTC");

            $customerModel = new customerModel();
            $meeting = new employeeMeetingObject();

            $meeting->em_emp_id = $_REQUEST["emp_id"];
            $meeting->em_meet_type = $_REQUEST["meet_type"];
            $meeting->em_name = addslashes($_REQUEST["meet_name"]);
            $meeting->em_start_time_stamp = $_REQUEST["start_time"];
            $meeting->em_end_time_stamp = $_REQUEST["end_time"];
            $meeting->em_cust_request = addslashes($_REQUEST["custom_text"]);
            $meeting->em_location = addslashes($_REQUEST["location"]);
            $meeting->em_payment_source = isset($_REQUEST['payment_source']) && $_REQUEST['payment_source'] != '' ? $_REQUEST['payment_source'] : 'none';
            $meeting->em_payment_source_id = isset($_REQUEST['payment_source_id']) && $_REQUEST['payment_source_id'] != '' ? $_REQUEST['payment_source_id'] : 0;

            if($meeting->em_payment_source_id == 0){
                $meeting->em_payment_source = "none";
            }

            $meeting->em_biz_id = $this->mobileRequestObject->bizID;
            $meeting->em_from_mobile = 0;
            $meeting->em_cust_id = isset($_REQUEST["cust_id"]) ? $_REQUEST["cust_id"] : 0;
            $meeting->em_is_recurring = $_REQUEST['recurring'];
            $meeting->em_recurring_limit_method = isset($_REQUEST['recurring_method']) ? $_REQUEST['recurring_method'] : "automatic";
            $meeting->em_recurring_amount = isset($_REQUEST['recurring_amount']) ? $_REQUEST['recurring_amount'] : 0;
            $meeting->em_recurring_end_date_stamp = isset($_REQUEST['recurring_end']) ? $_REQUEST['recurring_end'] : 0;

            $responce = new stdClass();

            if ($this->mobileRequestObject->bizID == "0"){
                $responce->result["code"] = "1";
                $responce->result["value"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No bizid";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }



            if ($meeting->em_meet_type == "0"){
                $responce->result["code"] = "3";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Meeting Type";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_name == ""){
                $responce->result["code"] = "4";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Meeting Name";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_start_time_stamp == "" || $meeting->em_start_time_stamp == "(null)" || $meeting->em_start_time_stamp == "0"){
                $responce->result["code"] = "5";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No Start Time";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }

            if ($meeting->em_end_time_stamp == "" || $meeting->em_end_time_stamp == "(null)" || $meeting->em_end_time_stamp == "0"){
                $responce->result["code"] = "6";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "201";
                $responce->result["internal_desc"] = "No End Time";
                return $this->returnAnswer(resultObject::withData(0,'',$responce));
            }


            if($meeting->em_cust_id > 0){
                $customerResult = $customerModel->getCustomerWithID($meeting->em_cust_id);

                if ($customerResult->code == 0){
                    $responce->result["code"] = "7";
                    $responce->result["meeting_id"] = 0;
                    $responce->result["localized_no"] = "201";
                    $responce->result["internal_desc"] = "No Customer Found";
                    return $this->returnAnswer(resultObject::withData(0,'',$responce));
                }

                $meeting->setCustomer($customerResult->data);
            }

            $result = bookingManager::setMeetingFromAdmin($meeting);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function editMeeting(){
        try{


            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            date_default_timezone_set("UTC");

            $meetingResult = bookingManager::getEmployeeMeetingByID($_REQUEST['meet_id']);

            if($meetingResult->code != 1){
                return $this->returnAnswer($meetingResult);
            }

            $originalMeeting = $meetingResult->data;

            $meeting = new employeeMeetingObject();

            $meeting->em_emp_id = $_REQUEST["emp_id"];
            $meeting->em_meet_type = $_REQUEST["meet_type"];
            $meeting->em_name = addslashes($_REQUEST["meet_name"]);
            $meeting->em_start_time_stamp = $_REQUEST["start_time"];
            $meeting->em_end_time_stamp = $_REQUEST["end_time"];
            $meeting->em_cust_request = addslashes($_REQUEST["custom_text"]);
            $meeting->em_location = addslashes($_REQUEST["location"]);
            $meeting->em_payment_source = isset($_REQUEST['payment_source']) && $_REQUEST['payment_source'] != '' ? $_REQUEST['payment_source'] : 'none';
            $meeting->em_payment_source_id = isset($_REQUEST['payment_source_id']) && $_REQUEST['payment_source_id'] != '' ? $_REQUEST['payment_source_id'] : 0;
            $meeting->em_biz_id = $this->mobileRequestObject->bizID;
            $meeting->em_from_mobile = 0;
            $meeting->em_cust_id = isset($_REQUEST["cust_id"]) ? $_REQUEST["cust_id"] : 0;
            $meeting->em_is_recurring = $_REQUEST['recurring'];

            $payChangeConfirmed = isset($_REQUEST["pay_change_confirmed"]) && $_REQUEST["pay_change_confirmed"] == 1;

            $responce = new stdClass();
            if($meeting->em_cust_id > 0){
                $customerModel = new customerModel();
                $customerResult = $customerModel->getCustomerWithID($meeting->em_cust_id);

                if ($customerResult->code == 0){
                    $responce->result["code"] = "7";
                    $responce->result["meeting_id"] = 0;
                    $responce->result["localized_no"] = "201";
                    $responce->result["internal_desc"] = "No Customer Found";
                    return $this->returnAnswer(resultObject::withData(1,'',$responce));
                }

                $meeting->setCustomer($customerResult->data);
            }

            if($meeting->em_emp_id > 0){
                $employeeResult = bookingManager::getEmployeeByID($meeting->em_emp_id);

                if ($employeeResult->code == 0){
                    $responce->result["code"] = "7";
                    $responce->result["meeting_id"] = 0;
                    $responce->result["localized_no"] = "201";
                    $responce->result["internal_desc"] = "No employee Found";
                    return $this->returnAnswer(resultObject::withData(1,'',$responce));
                }

                $meeting->setEmployee($employeeResult->data);
            }


            $result = bookingManager::updateMeetingFromAdmin($meeting,$originalMeeting,$payChangeConfirmed);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function cancelMeeting(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $meetingResult = bookingManager::getEmployeeMeetingByID($_REQUEST['meetingID']);

            if($meetingResult->code != 1){
                return $this->returnAnswer($meetingResult);
            }

            $meeting = $meetingResult->data;

            if($meeting->em_cust_id > 0){
                $customerModel = new customerModel();
                $customerResult = $customerModel->getCustomerWithID($meeting->em_cust_id);

                if ($customerResult->code == 0){
                    return $this->returnAnswer(resultObject::withData(0,"No Customer Found"));
                }

                $meeting->setCustomer($customerResult->data);
            }

            if($meeting->em_emp_id > 0){
                $employeeResult = bookingManager::getEmployeeByID($meeting->em_emp_id);

                if ($employeeResult->code == 0){

                    return $this->returnAnswer(resultObject::withData(0,"No employee Found"));
                }

                $meeting->setEmployee($employeeResult->data);
            }

            $refundAction = isset($_REQUEST['refund_action']) ? $_REQUEST['refund_action'] : "reset_claim";

            $result = resultObject::withData(1);

            if(isset($_REQUEST['delete_reccuring']) && $_REQUEST['delete_reccuring'] == 1){
                bookingManager::deleteMeetingFromAdminRecurring($meeting,$refundAction);
            }
            else{
                $appointments = array();
                $appointments[] = adminAppointmentObject::withMeeting($meeting);

                adminManager::sendUpdateToBizDevices($meeting->em_biz_id,"appointments.delete",$appointments);
                $result = bookingManager::deleteMeetingFromAdmin($meeting,$refundAction);
            }

            return $this->returnAnswer($result);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function addCashChargeForMeeting(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $meetID = $_REQUEST["meetingID"];
            $chargeText = $_REQUEST["charge_text"];
            $amount = $_REQUEST["amount"];
            $tax = $_REQUEST["tax"];
            $amounTotal = $_REQUEST["amount_total"];

            $meetingResult = bookingManager::getEmployeeMeetingByID($meetID);

            if($meetingResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_meeting'));
            }

            $meeting = $meetingResult->data;

            $customerBilling = new customerBillingManager();

            $cust_id = $_REQUEST['custID'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            if($meeting->em_payment_source == "order"){
                $orderResult = customerOrderManager::getCustomerOrderByID($meeting->em_payment_source_id);
            }
            else{
                return $this->returnAnswer(resultObject::withData(0,'no_order_found'));
            }

            $orderObject = $orderResult->data;

            //create invoice
            $invoiceCreateResult = $customerBilling->initiateCustomerInvoice($cust_id);

            if($invoiceCreateResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            $invoice = $customerBilling->getCustomerInvoiceByID($invoiceID)->data;

            //connect order to invoice
            $customerBilling->connectInvoiceToOrderDirectly($orderObject->cto_id,$invoiceID);

            $invoice->cin_sub_total = $amount;
            $invoice->cin_tax = $tax;
            $invoice->cin_total = $amounTotal;


            $customerBilling->setCustInvoiceFinalized($invoiceID);

            $orderItem = new customerOrderItemObject();

            //add item to order
            $orderItem->tri_item_type = 0;
            $orderItem->tri_item_ext_type = "other";
            $orderItem->tri_name = $chargeText;
            $orderItem->tri_price = $amounTotal;
            $orderItem->tri_without_vat = $amount;
            $orderItem->tri_vat = $tax;

            $items = array();
            $items[] = $orderItem;

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$items);

            $_REQUEST["ipAddress"] = utilityManager::get_real_IP();
            $transaction = customerTransactionObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $transaction->tr_cust_id = $customer->cust_id;
            $transaction->tr_biz_id = $customer->cust_biz_id;
            $transaction->tr_invoice_id = $invoiceID;
            $transaction->tr_country = bizManager::getBizCountryID($this->mobileRequestObject->bizID);

            $customerBilling->setCustInvoicePendingByInvoiceID($invoiceID);
            $customerBilling->settleInvoiceWithCashTransaction($invoiceID,$transaction);


            return $this->returnAnswer(resultObject::withData(1));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getTimeSlotsForServiceAndEmployee(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $emp_id = $_REQUEST["employeeID"] != '' ? $_REQUEST["employeeID"] : 0;
            $service_id = $_REQUEST["serviceID"] != '' ? $_REQUEST["serviceID"] : 0;
            $start_date = mktime(0, 0, 0);
            if($_REQUEST["fromDate"] != ""){
                $qure_date = mktime(0, 0, 0);
                $day = date('d',$_REQUEST["fromDate"]);
                $month = date('m',$_REQUEST["fromDate"]);
                $year = date('Y',$_REQUEST["fromDate"]);
                $start_date = mktime(0, 0, 0, $month, $day, $year);

                if($start_date <= $qure_date) {

                    $start_date = $qure_date;
                }
            }
            $offset = $_REQUEST["offset"] != "" ? $_REQUEST["offset"] : 0;



            $result = bookingManager::getMeetingsList($emp_id,$service_id,$start_date,$offset);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*           CLASS FUNCTIONS           */
    /************************************* */

    function addClass(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $serviceId = $_REQUEST['service'] == "" ? "0" : $_REQUEST['service'];

            $calculatedDuration = bookingManager::getCalculatedDuration($_REQUEST);

            $classObject = new classObject();
            $classObject->calc_biz_id = $this->mobileRequestObject->bizID;
            $classObject->calc_name = $_REQUEST["class_name"];
            $classObject->calc_desc = $_REQUEST["class_desc"];
            $classObject->calc_price = $_REQUEST["calc_price"];
            $classObject->calc_max_cust = $_REQUEST["class_cap"];
            $classObject->calc_duration_by_service = $_REQUEST["dur_by_serv"];
            $classObject->calc_service = $serviceId;
            $classObject->calc_duration = $calculatedDuration['format'];
            $classObject->calc_duration_size = $calculatedDuration['size'];

            if(isset($_REQUEST["class_visible"])){
                $classObject->calc_isvisible = $_REQUEST["class_visible"];
            }

            $addClassResult = bookingManager::addClass($classObject);

            if($addClassResult->code != 1) return $this->returnAnswer($addClassResult);

            $classObject->calc_id = $addClassResult->data;

            $employees = explode(',', $_REQUEST['employees']);

            foreach($employees as $employeeid){
                bookingManager::connectEmployeeToClass($this->mobileRequestObject->bizID,$addClassResult->data,$employeeid);
            }

            $dates = $_REQUEST["class_dates"];
            $dates = str_replace('\n','',$dates);
            $datesArray = json_decode($dates);
            if(count($datesArray) > 0){
                foreach ($datesArray as $classDate)
                {
                    $classDateObject = new classDateObject();
                    $classDateObject->ccd_biz_id = $this->mobileRequestObject->bizID;
                    $classDateObject->ccd_class_id = $classObject->calc_id;
                    $classDateObject->ccd_isvisible = $classDate->isVisible;
                    $classDateObject->ccd_start_date = $classDate->date;
                    $classDateObject->ccd_start_time = bookingManager::getStartHourID($classDate->time);
                    $classDateObject->ccd_end_date = bookingManager::getEndDateForClassDate($classDateObject->ccd_start_date,$classDateObject->ccd_start_time,$calculatedDuration["duration"]);
                    $classDateObject->ccd_end_time = bookingManager::getEndTimeIdForClassDate($classDateObject->ccd_start_time,$calculatedDuration["duration"]);
                    $classDateObject->ccd_recurring = isset($classDate->recurring) ? $classDate->recurring : "none";
                    $classDateObject->ccd_recurring_limit_method = isset($classDate->recurring_method) ? $classDate->recurring_method : "automatic";
                    $classDateObject->ccd_recurring_end_date = isset($classDate->recurring_end_date) ? date('Y-m-d',$classDate->recurring_end_date) : null;
                    $classDateObject->ccd_recurring_amount = isset($classDate->recurring_amount) ? $classDate->recurring_amount : 0;

                    $addClassDateResult = bookingManager::setClassDate($classObject,$classDateObject);

                }
            }

            return $this->returnAnswer($addClassResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function editClass(){
        try{

            $classResult = bookingManager::getClassByID($_REQUEST["class_id"]);

            if($classResult->code != 1){
                return $classResult;
            }

            $classObject = $classResult->data;

            $serviceId = $_REQUEST['service'] == "" ? "0" : $_REQUEST['service'];

            $calculatedDuration = bookingManager::getCalculatedDuration($_REQUEST);


            $classObject->calc_biz_id = $this->mobileRequestObject->bizID;
            $classObject->calc_name = $_REQUEST["class_name"];
            $classObject->calc_desc = $_REQUEST["class_desc"];
            $classObject->calc_price = $_REQUEST["calc_price"];
            $classObject->calc_max_cust = $_REQUEST["class_cap"];
            $classObject->calc_duration_by_service = $_REQUEST["dur_by_serv"];
            $classObject->calc_service = $serviceId;
            $classObject->calc_duration = $calculatedDuration['format'];
            $classObject->calc_duration_size = $calculatedDuration['size'];

            if(isset($_REQUEST["class_visible"])){
                $classObject->calc_isvisible = $_REQUEST["class_visible"];
            }

            $result = bookingManager::updateClass($classObject);

            if($result->code != 1){
                return $result;
            }

            $employees = explode(',', $_REQUEST['employees']);

            foreach($employees as $employeeid){
                bookingManager::connectEmployeeToClass($this->mobileRequestObject->bizID,$classObject->calc_id,$employeeid);
            }

            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getClass(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $classID = $_REQUEST['classID'];
            $classDateID = isset($_REQUEST['classDateID']) ? $_REQUEST['classDateID'] : 0;

            $classResult = bookingManager::getClassByID($classID);

            if($classResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_class'));
            }

            $datesArray = array();
            if($classDateID != 0){
                $dateObjectResult = bookingManager::getClassDateByID($classDateID);
                if($dateObjectResult->code != 1){
                    return $this->returnAnswer($dateObjectResult);
                }
                $dateCustomersResult = bookingManager::getAllCustomersForClassDate($dateObjectResult->data->ccd_id);
                $dateObjectResult->data->setCustomers($dateCustomersResult->data);

                array_push($datesArray,$dateObjectResult->data);
                $classResult->data->dates = $datesArray;
            }else{
                $datesData = bookingManager::getClassDates($classID);
                if($datesData->code == 1){
                    $classResult->data->dates = $datesData->data;
                }else{
                    return $this->returnAnswer($datesData);
                }
            }
            return $this->returnAnswer($classResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function addClassDate(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $classResult = bookingManager::getClassByID($_REQUEST["classID"]);

            if($classResult->code != 1) return $this->returnAnswer($classResult);

            $classObject = $classResult->data;

            $calculatedDuration = $classObject->getDuration();

            $time = $_REQUEST['time'];

            $classDateObject = new classDateObject();
            $classDateObject->ccd_biz_id = $this->mobileRequestObject->bizID;
            $classDateObject->ccd_class_id = $_REQUEST["classID"];
            $classDateObject->ccd_isvisible = 1;
            $classDateObject->ccd_start_date = date("Y-m-d",$time);
            $classDateObject->ccd_start_time = bookingManager::getStartHourID(date("H:i",$time));
            $classDateObject->ccd_end_date = bookingManager::getEndDateForClassDate($classDateObject->ccd_start_date,$classDateObject->ccd_start_time,$calculatedDuration["durationUnits"]);
            $classDateObject->ccd_end_time = bookingManager::getEndTimeIdForClassDate($classDateObject->ccd_start_time,$calculatedDuration["durationUnits"]);
            $classDateObject->ccd_recurring = isset($_REQUEST["recurring"]) ? $_REQUEST["recurring"] : "none";
            $classDateObject->ccd_recurring_limit_method = isset($_REQUEST["recurring_method"]) ? $_REQUEST["recurring_method"] : "automatic";
            $classDateObject->ccd_recurring_end_date = isset($_REQUEST["recurring_end_date"]) ? date('Y-m-d',$_REQUEST["recurring_end_date"]) : null;
            $classDateObject->ccd_recurring_amount = isset($_REQUEST["recurring_amount"]) ? $_REQUEST["recurring_amount"] : 0;

            $addClassDateResult = bookingManager::setClassDate($classObject,$classDateObject);

            return $this->returnAnswer($addClassDateResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getClassDates(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $classID = $_REQUEST['classID'];
            $existing = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $datesData = bookingManager::getClassDates($classID,$existing,0);
            return $this->returnAnswer($datesData);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function moveClassDate(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }



            $classDateResult = bookingManager::getClassDateByID($_REQUEST["classDateID"]);

            if($classDateResult->code != 1){
                return $this->returnAnswer($classDateResult);
            }

            $classDateObject = $classDateResult->data;

            $classResult = bookingManager::getClassByID($classDateObject->ccd_class_id);

            if($classResult->code != 1) return $this->returnAnswer($classResult);

            $classObject = $classResult->data;

            $calculatedDuration = $classObject->getDuration();

            $time = $_REQUEST['time'];

            $classDateObject->ccd_start_date = date("Y-m-d",$time);
            $classDateObject->ccd_start_time = bookingManager::getStartHourID(date("H:i",$time));
            $classDateObject->ccd_end_date = bookingManager::getEndDateForClassDate($classDateObject->ccd_start_date,$classDateObject->ccd_start_time,$calculatedDuration["durationUnits"]);
            $classDateObject->ccd_end_time = bookingManager::getEndTimeIdForClassDate($classDateObject->ccd_start_time,$calculatedDuration["durationUnits"]);



            $moveClassResult = bookingManager::moveClassDate($classDateObject);

            return $this->returnAnswer($moveClassResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function removeClassDate(){

        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $classDateId = $_REQUEST["classDateID"];

            $classDateResult = bookingManager::getClassDateByID($classDateId);

            if($classDateResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_class_date'));
            }

            $classDate = $classDateResult->data;

            $classResult = bookingManager::getClassByClassDateID($classDateId);

            if($classResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_class'));
            }

            $class = $classResult->data;

            $employeeMeetings = bookingManager::getEmployeeMeetingsForClassDate($classDateId);
            if(count($employeeMeetings) > 0){
                foreach ($employeeMeetings as $meeting)
                {
                    $meetingResult = bookingManager::getEmployeeMeetingByID($meeting["em_id"]);
                	bookingManager::deleteMeetingFromAdmin($meetingResult->data);
                }
            }

            $refundAction = isset($_REQUEST['refund_action']) ? $_REQUEST['refund_action'] : "reset_claim";

            if(bookingManager::isClassFutureDate($classDateId)){
                $customers = bookingManager::getCustomersForClassDate($classDateId);
                if(count($customers) > 0){
                    foreach ($customers as $customer)
                    {
                        bookingManager::deleteClassDateCustomerById($classDateId,$customer["cust_id"],$refundAction);
                    }
                }
            }

            $appointments = array();
            $appointments[] = adminAppointmentObject::withClassDate($class,$classDate);

            adminManager::sendUpdateToBizDevices($class->calc_biz_id,"appointments.delete",$appointments);

            $result = bookingManager::deleteClassDateById($classDateId);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function signupMemberToClassDate(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $classID = $_REQUEST['classID'];
            $dateID = $_REQUEST["dateId"];
            $custID = isset($_REQUEST["cust_id"]) ? $_REQUEST["cust_id"] : 0;
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($custID);
            if ($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'"no_customer"'));
            }
            $customer = $customerResult->data;

            $classResult = bookingManager::getClassByID($classID);
            if($classResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_class'));
            }
            $class = $classResult->data;

            $classDateResult = bookingManager::getClassDateByID($dateID);
            if($classDateResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_class_date'));
            }
            $classDate = $classDateResult->data;

            $existsInDate = bookingManager::isCustomerInClassDate($custID,$classDate);

            if($existsInDate){
                return $this->returnAnswer(resultObject::withData(0,'already_exist'));
            }

            if($classDate->booked_count >= $class->calc_max_cust){
                return $this->returnAnswer(resultObject::withData(0,'no_place'));
            }

            $customerDate = new classDateCustomerObject();
            $customerDate->cdc_biz_id = $classDate->ccd_biz_id;
            $customerDate->cdc_class_id = $classID;
            $customerDate->cdc_class_date_id = $dateID;
            $customerDate->cdc_customer_id = $custID;
            $customerDate->cdc_payment_source = isset($_REQUEST['payment_source']) && $_REQUEST['payment_source'] != '' ? $_REQUEST['payment_source'] : 'none';
            $customerDate->cdc_payment_source_id = isset($_REQUEST['payment_source_id']) && $_REQUEST['payment_source_id'] != '' ? $_REQUEST['payment_source_id'] : 0;
            $customerDate->cdc_comments = $_REQUEST["comments"];

            $result = bookingManager::signUpCustomerToClassdate($customer,$customerDate);

            if($result->code == 1){
                bizManager::addBizOfferingUsage($this->mobileRequestObject->bizid,'class',$classID);
                eventManager::actionTrigger(enumCustomerActions::classsignup,$customer->cust_id,"class",'','',$dateID);
                customerManager::addBadgeToCustomer($custID,enumBadges::appointments);
                bizManager::needUpdateApp($this->mobileRequestObject->bizid);
            }
            customerManager::sendAsyncCustomerDataUpdateToDevice('meetings',$custID);
            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function removeMemberFromClassDate(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }
            $refundAction = isset($_REQUEST['refund_action']) ? $_REQUEST['refund_action'] : "reset_claim";
            $dateID = $_REQUEST["dateId"];
            $custID = isset($_REQUEST["cust_id"]) ? $_REQUEST["cust_id"] : 0;
            $deleteResult = bookingManager::deleteClassDateCustomerById($dateID,$custID,$refundAction);

            if($deleteResult->code != 1){
                return $this->returnAnswer($deleteResult);
            }

            $dateCustomersResult = bookingManager::getAllCustomersForClassDate($dateID);
            $result = resultObject::withData(1,'',$dateCustomersResult);
            return $this->returnAnswer($result);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setMembersInClassdate(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $classID = $_REQUEST['classID'];
            $dateID = $_REQUEST["dateId"];
            $members = str_replace('\n','',$_REQUEST["members"]);
            $membersArray = (array)json_decode($members);

            $customersModel = new customerModel();
            $refundAction = isset($_REQUEST['refund_action']) ? $_REQUEST['refund_action'] : "reset_claim";
            if(count($membersArray) > 0){

                $classDateResult = bookingManager::getClassDateByID($dateID);
                $classDate = $classDateResult->data;

                foreach ($membersArray as $member)
                {
                    if($member->action == "delete"){
                        bookingManager::deleteClassDateCustomerById($dateID,$member->cust_id,$refundAction);
                    }else{
                        $existsInDate = bookingManager::isCustomerInClassDate($member->cust_id,$classDate);
                        if(!$existsInDate){

                            $customerResult = $customersModel->getCustomerWithID($member->cust_id);
                            if ($customerResult->code == 1){

                                $customer = $customerResult->data;

                                $customerDate = new classDateCustomerObject();
                                $customerDate->cdc_biz_id = $classDate->ccd_biz_id;
                                $customerDate->cdc_class_id = $classID;
                                $customerDate->cdc_class_date_id = $dateID;
                                $customerDate->cdc_customer_id = $member->cust_id;
                                $customerDate->cdc_payment_source = 'none';
                                $customerDate->cdc_payment_source_id = 0;
                                $customerDate->cdc_comments = '';
                                $customerDate->cdc_scheduled_from = "Internal";

                                $result = bookingManager::signUpCustomerToClassdate($customer,$customerDate);
                            }
                        }
                    }
                }
            }
            $dateCustomersResult = bookingManager::getAllCustomersForClassDate($dateID);
            $result = resultObject::withData(1,'',$dateCustomersResult);
            return $this->returnAnswer($result);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setPaymentForClassDate(){
       
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $classDateCustomerResult = bookingManager::getClassDateCustomerByID($_REQUEST["class_date_customerID"]);

            if($classDateCustomerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no customer date found'));
            }

            $classDateCustomer = $classDateCustomerResult->data;

            $classDateCustomer->cdc_payment_source = $_REQUEST["payment_source"];
            $classDateCustomer->cdc_payment_source_id = $_REQUEST["payment_source_id"];

            $result = bookingManager::processCustomerClassDatePayment($classDateCustomer);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*          SETTINGS FUNCTIONS         */
    /************************************* */

    function getSettings(){
        try{
            $bizModel = new bizModel($this->mobileRequestObject->bizid);

            $bizSettingsResult = $bizModel->getBizSettingsForAdmin();

            return $this->returnAnswer($bizSettingsResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function setSettings(){
        try{

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /************************************* */
    /*              CASH DRAWER            */
    /************************************* */

    function startCashDrawer(){

        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            if($this->mobileRequestObject->deviceType == 'TABLET' && isset($_REQUEST['fleet_name']) && $_REQUEST['fleet_name'] != ""){
                adminManager::setBizFleetName($this->mobileRequestObject,$_REQUEST['fleet_name']);
            }

            $startingAmount = $_REQUEST["startCash"];

            $cashDrawerObject = new cashDrawerObject();
            $cashDrawerObject->cd_biz_id = $this->mobileRequestObject->bizID;
            $cashDrawerObject->cd_device_id = $this->mobileRequestObject->deviceID;
            $cashDrawerObject->cd_opener_account_id = $this->mobileRequestObject->acID;
            $cashDrawerObject->cd_start_cash = $startingAmount;
            $cashDrawerObject->cd_expected_total = $startingAmount;
            $cashDrawerObject->cd_actual_total = $startingAmount;
            $cashDrawerObject->cd_notes = $_REQUEST["notes"];

            $drawerResult = ecommerceManager::addCashDrawer($cashDrawerObject);

            if($drawerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_drawer_added'));
            }

            $drawer = ecommerceManager::getCashDrawerByID($drawerResult->data);

            return $this->returnAnswer($drawer);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    function endCashDrawer(){

        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $drawerResult = ecommerceManager::closeCashDrawer($_REQUEST["drawerID"],$_REQUEST["actualCash"],$_REQUEST["note"]);
            return $this->returnAnswer($drawerResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    function addCashDrawerEntry(){

        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cashDrawerEntryObject = new cashDrawerEntryObject();

            $cashDrawerEntryObject->cde_drawer_id = $_REQUEST["drawerID"];
            $cashDrawerEntryObject->cde_entry_type = $_REQUEST["type"];
            $cashDrawerEntryObject->cde_total = $_REQUEST["amount"];

            $cashDrawerEntryObject->cde_order_id = 0;

            if($_REQUEST["type"] == 'sale'){
                $order = customerOrderManager::getCustomerOrderByLongID($_REQUEST["saleID"])->data;

                $cashDrawerEntryObject->cde_order_id = $order->cto_id;
            }


            if($_REQUEST["type"] == 'refund'){
                $customerBilling = new customerBillingManager();

                $invoice = $customerBilling->getCustomerInvoiceByLongID($_REQUEST["saleID"])->data;

                $cashDrawerEntryObject->cde_order_id = $customerBilling->getConnectedOrderIDForCustInvoice($invoice->cin_id);
            }

            $cashDrawerEntryObject->cde_reason = $_REQUEST["reason"];
            $cashDrawerEntryObject->cde_account_id = $this->mobileRequestObject->acID;

            if(($_REQUEST["type"] == 'refund' || $_REQUEST["type"] == 'cash_out') && $_REQUEST["amount"] > 0){
                $cashDrawerEntryObject->cde_total = 0-$_REQUEST["amount"];
            }

            if(isset($_REQUEST["time"]) && $_REQUEST["time"] != ""){
                $cashDrawerEntryObject->cde_time = $_REQUEST["time"];
            }

            $drawerResult = ecommerceManager::addCashDrawerEntry($cashDrawerEntryObject);
            return $this->returnAnswer($drawerResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    function getCashDrawerHistory(){

        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $paramAccountId = isset($_REQUEST["accountID"]) && $_REQUEST["accountID"] != "" && $_REQUEST["accountID"] != "0" ? utilityManager::decodeFromHASH($_REQUEST["accountID"]) : "0";
            $paramExisting = isset($_REQUEST["existing"]) && $_REQUEST["existing"] != "" ? $_REQUEST["existing"] : 0;

            $drawerResult = ecommerceManager::getCashDrawerHistory($this->mobileRequestObject->bizID,$paramAccountId,$paramExisting);
            return $this->returnAnswer($drawerResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    function getCashDrawerDetails(){

        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $drawerResult = ecommerceManager::getCashDrawerByID($_REQUEST["drawerID"]);

            $data["drawerData"] = $drawerResult->data->AdminAPIArray();
            $data["drawerData"]["cash_sales"] = ecommerceManager::getCashDrawerSales($_REQUEST["drawerID"],0,0)->data;
            $data["drawerData"]["cash_refunds"] = ecommerceManager::getCashDrawerRefunds($_REQUEST["drawerID"],0,0)->data;
            $data["drawerData"]["cash_non_sales"] = ecommerceManager::getCashDrawerNonSales($_REQUEST["drawerID"],0,0)->data;

            $drawerResult->data = $data;
            return $this->returnAnswer($drawerResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    /************************************* */
    /*              Members                */
    /************************************* */

    /**
     * Add members from list
     * Return list of success/failure
     */
    function addMembers(){
       try{
           if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

           $members = $_REQUEST["members"];
           $members = str_replace('\n','',$members);
           $jarray = json_decode($members);

           $results = array();
           $added = array();

           $customerModel = new customerModel();
           $i = 0;
           foreach ($jarray as $member)
           {
               $data = (array)$member;
               $cCode = isset($_REQUEST['countryCode']) ? str_replace("+","",$_REQUEST['countryCode']) : "" ;
               $data['phone'] = utilityManager::getValidPhoneNumber(trim($data['phone']),$cCode);
               $existingCustResult = $customerModel->getCustIDByValidPhoneNumberForAdmin($data['phone'],$this->mobileRequestObject);

               $custID = 0;
               $existed = false;
               $result = array();

               if($existingCustResult->code == 1 && $existingCustResult->data > 0){//customer exists with phone number
                   $custID = $existingCustResult->data;
                   $existed = true;
               }
               else{
                   $newMemberResult = $customerModel->addMemberFromAdminApp($this->mobileRequestObject,$data);
                   if($newMemberResult->code == 1){
                       $custID = $newMemberResult->data;
                   }
               }

               $result['index'] = isset($data['index']) ? $data['index'] : $i;
               $result['phone'] = "+".$data['phone'];
               $result['name'] = $data['name'];
               if($custID <= 0){
                   $result['added'] = 0;
               }
               else{

                   $result['added'] = 1;
                   $memberResult = $customerModel->getMemberForAdminCustID($custID);

                   $result['member'] = $memberResult->data->AdminAPIArray();
                   $result['existed'] = $existed ? 1 : 0;
                   $added = $memberResult->data;
               }



               $results[] = $result;
               $i++;
           }

           adminManager::sendUpdateToBizDevices($this->mobileRequestObject->bizID,"members.new",$added);

           return $this->returnAnswer(resultObject::withData(1,'',$results));

       }
       catch(Exception $e){
           errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
           $result = resultObject::withData(0,$e->getMessage());
           return $this->returnAnswer($result);
       }
    }

    /**
     * Returns orders list for member
     */
    function getMemberOrders(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];
            $skip = $_REQUEST["existing"];


            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;
            $result = customerOrderManager::getCustomerAdminOrders($customer,"eCommerce",$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member details */

    function updateMemberData(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }



            $cust_id = $_REQUEST['cust_id'];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(0,'no_customer_found'));
            }

            $customer = $customerResult->data;


            if(isset($_REQUEST['email'])){
                $customer->cust_email = $_REQUEST['email'];
            }

            if(isset($_REQUEST['gender'])){
                $customer->cust_gender = $_REQUEST['gender'];
            }

            if(isset($_REQUEST['birthday'])){
                $customer->cust_birth_date = $_REQUEST['birthday'];
            }

            if(isset($_REQUEST['anniversary'])){
                $customer->cust_weding_date = $_REQUEST['anniversary'];
            }

            if(isset($_REQUEST['comments'])){
                $customer->cust_comments = $_REQUEST['comments'];
            }

            if(isset($_REQUEST['address'])){
                $customer->cust_address = $_REQUEST['address'];
            }

            if(isset($_REQUEST['city'])){
                $customer->cust_city = $_REQUEST['city'];
            }

            if(isset($_REQUEST['country'])){
                $customer->cust_country = $_REQUEST['country'];
            }

            if(isset($_REQUEST['zip'])){
                $customer->cust_zip = $_REQUEST['zip'];
            }

            if(isset($_REQUEST['phone2'])){
                $customer->cust_phone2 = $_REQUEST['phone2'];
            }

            if(isset($_REQUEST['state'])){
                $customer->cust_state = $_REQUEST['state'];
            }

            $result = $customerModel->updateCustomer($customer);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function updateMemberImage(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $innerFileName = str_replace('.','_',$_REQUEST['fileName']);
            $file = $_FILES[$innerFileName];
            $bizId = $this->mobileRequestObject->bizID;

            $fileName = $file['name'];

            $fileExtention = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

            $fileType = "";
            switch ($fileExtention){
                case "png":
                case "jpg":
                case "jpeg":
                    $fileType="img";
                    break;
            }

            if($fileType == ""){
                return $this->returnAnswer(resultObject::withData(0,'not_image_file'));
            }
            $newFileName = $cust_id."_".time().'.'.$fileExtention;
            $image_storage = imageStorage::getStorageAdapter();


            $urlFile = $image_storage->directUpload($file['tmp_name'], $bizId.'/'.$newFileName);

            $customer->cust_pic = $urlFile;

            $result = $customerModel->updateCustomer($customer);

            return $this->returnAnswer(resultObject::withData($result->code,'',$urlFile));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member groups */

    /**
     * Returns list of groups of biz and status of member in group
     */
    function getMemberGroups(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $result = $customerModel->getGroupsForCustomer($customer);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Sets the status of member in list of groups
     */
    function setMemberGroups(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $groups = $_REQUEST["groups"];
            $groups = str_replace('\n','',$groups);
            $jarray = json_decode($groups);

            $addTo = array();
            $removeFrom = array();
            foreach ($jarray as $group)
            {
            	if($group->assign == 1){
                    $addTo[] = $group->id;
                }
                else{
                    $removeFrom[] = $group->id;
                }
            }

            $customerModel->addCustomerToGroups($customer,$addTo);
            $customerModel->removeCustomerFromGroups($customer,$removeFrom);

            return $this->returnAnswer(resultObject::withData(1));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Sets the status of member in list of groups
     */
    function setMemberInGroup(){
        try{

            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $groupID = $_REQUEST["group_id"];
            $groups = array();
            $groups[] = $groupID;
            $assign = $_REQUEST["assign"];
            if($assign == 1){
                $customerModel->addCustomerToGroups($customer,$groups);
            }
            else{
                $customerModel->removeCustomerFromGroups($customer,$groups);
            }

            return $this->returnAnswer(resultObject::withData(1));

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* member rewards */

    /**
     * Returns rewards list for member
     */
    function getMemberRewards(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $result = $customerModel->getCustomerBenefitsForAdmin($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Adds reward to member
     * Returns success/faliure and reward (if success)
     */
    function grantMemberReward(){

        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }



            $cust_id = $_REQUEST["cust_id"];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $benefit = new customerBenefitObject();

            $benefit->cb_cust_id = $cust_id;
            $benefit->cb_title = $_REQUEST['title'];
            $benefit->cb_description = $_REQUEST['description'];
            $benefit->cb_biz_id = $this->mobileRequestObject->bizID;
            $benefit->cb_type = 'personal';
            if(isset($_REQUEST["valid_start"]) && $_REQUEST["valid_start"] != 0){
                $benefit->cb_valid_start = date("Y-m-d H:i:s",$_REQUEST["valid_start"]);
            }
            if(isset($_REQUEST["valid_end"]) && $_REQUEST["valid_end"] != 0){
                $benefit->cb_valid_end = date("Y-m-d H:i:s",$_REQUEST["valid_end"]);
            }
            $benefit->cb_code = $_REQUEST['code'];

            $result = customerManager::addCustomerBenefit($benefit);

            if($result->code != 1){
                return $this->returnAnswer($result);
            }

            $createdBenefit = customerManager::getCustomerBenefit($result->data);

            $fallback = new pushParamsObject();
            $fallback->biz_id = $this->mobileRequestObject->bizID;
            $fallback->cust_id = $cust_id;
            $fallback->push_type = enumPushType::biz_personalRewards;
            $fallback->message = pushManager::getSystemAlertPushMessageText(27,$this->mobileRequestObject->bizID);
            $extraParams = array();
            
            $fallback->extra_params = $extraParams;

            wsocketManager::sendIMAsync($fallback->message,$customer->getSocketIdentifier(),enumSocketType::point,$fallback,$customer->cust_last_device == "Android");

            return $this->returnAnswer($createdBenefit);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Submit redeem code for reward
     */
    function submitRewardCode(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $code = $_REQUEST['code'];

            $result = customerManager::redeemBenefitWithCode($code);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Ad hoc Payments */
    function getMemberPaymentRequests(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];
            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;


            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $result = customerOrderManager::getCustomerAdminpaymentRequests($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getMemberOpenPaymentDraft(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST["cust_id"];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);
            if($customerResult->code != 1){
                return $this->returnAnswer(resultObject::withData(0,'no_customer'));
            }

            $customer = $customerResult->data;

            $result = customerOrderManager::getCustomerOpenDraftForAdmin($customer);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Add payment request for member
     */
    function addMemberPaymentRequest(){

        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $openDraftResult = customerOrderManager::customerHasOpenDraft($customer);

            if($openDraftResult->code == 1 || $openDraftResult->data){
                return $this->returnAnswer(resultObject::withData(0,'draft_already_open'));
            }

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_account_id = $this->mobileRequestObject->acID;
            $orderObject->cto_order_type = "paymentRequest";
            $orderObject->cto_original_type = "paymentRequest";
            $orderObject->cto_title = $_REQUEST['title'];
            $orderObject->cto_order_tyme = date('Y-m-d H:i:s',time());
            $orderObject->cto_due_date = isset($_REQUEST['due_date']) ? date("Y-m-d",$_REQUEST['due_date']) : null;
            $orderObject->cto_request_status = "draft";
            $orderObject->cto_status = enumOrderStatus::NA;

            //check validity
            if(!$orderObject->_isValid()){
                return $this->returnAnswer(resultObject::withData(-1,"order_invalid"));
            }

            //create order
            $orderResult = customerOrderManager::initiateEmptyOrderForCustomer($cust_id,$orderObject);

            if($orderResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,"order_not_created"));
            }

            $orderObject = $orderResult->data;

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();


            foreach ($jarray as $item)
            {

                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                $itemObjectsList[] = $itemObject;
            }

            //connect items to order
            customerOrderManager::addCustItemsToOrder($orderObject->cto_id,$itemObjectsList);

            //reminder
            if(isset($_REQUEST['reminder']) && $_REQUEST['reminder'] != ""){
                $orderObject->cto_reminder_type = $_REQUEST['reminder'];
                $orderObject->cto_next_reminder = isset($_REQUEST['next_reminder']) && $_REQUEST['next_reminder'] != 0 ? date("Y-m-d H:i:s",$_REQUEST['next_reminder']) : date("Y-m-d H:i:s");
                $orderObject->cto_reminder_time = isset($_REQUEST['next_reminder']) && $_REQUEST['next_reminder'] != 0 ? date("H:i:s",$_REQUEST['next_reminder']) : date("H:i:s");
                $orderObject->cto_reminder_text = $_REQUEST["reminder_text"];
            }


            $finalize = isset($_REQUEST['finalize']) ? $_REQUEST['finalize'] == 1 : false;
            $send = $finalize && isset($_REQUEST['send']) ? $_REQUEST['send'] == 1 : false;

            if($finalize){
                $orderObject->cto_request_status = "finalized";
                $orderObject->cto_finalized_on = date("Y-m-d H:i:s");


            }

            if($orderObject->cto_request_status == "finalized" && $send && $orderObject->cto_request_sent != 1){
                $orderObject->cto_request_sent = 1;
                $orderObject->cto_request_sent_on = date("Y-m-d H:i:s");

                customerOrderManager::sendPaymentRequestToCustomer($orderObject);
            }

            customerOrderManager::updateCustomerOrder($orderObject);

            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettings = $bizModel->getBizStoreSettings()->data;

            if($finalize && $storeSettings->ess_handle_unpaid_orders == 1){
                utilityManager::asyncHandlePaymentRequestOrder($orderObject->cto_id);
            }

            return $this->returnAnswer(resultObject::withData(1,'',$orderObject->AdminPayRequestAPIArray()));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Update payment request
     */
    function editPaymentRequest(){

        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['order_id'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            if($order->cto_request_status == "finalized"){
                return $this->returnAnswer(resultObject::withData(0,'request_finalized'));
            }

            $finalize = isset($_REQUEST['finalize']) ? $_REQUEST['finalize'] == 1 : false;
            $send = $finalize && isset($_REQUEST['send']) ? $_REQUEST['send'] == 1 : false;

            //Order Fields - Purchase Details
            $order->cto_amount = $_REQUEST["amount"];      //Becomes Total
            $order->cto_without_vat = $_REQUEST["subTotal"];        //To Add
            $order->cto_vat = $_REQUEST["tax"];            //To Add

            customerOrderManager::emptyCustomerOrderFromItems($order->cto_id);

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();


            foreach ($jarray as $item)
            {

                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                $itemObjectsList[] = $itemObject;
            }

            //connect items to order
            customerOrderManager::addCustItemsToOrder($order->cto_id,$itemObjectsList);

            if($finalize){
                $order->cto_request_status = "finalized";
                $order->cto_finalized_on = date("Y-m-d H:i:s");
            }

            if($order->cto_request_status == "finalized" && $send && $order->cto_request_sent != 1){
                $order->cto_request_sent = 1;
                $order->cto_request_sent_on = date("Y-m-d H:i:s");

                customerOrderManager::sendPaymentRequestToCustomer($order);
            }

            customerOrderManager::updateCustomerOrder($order);

            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettings = $bizModel->getBizStoreSettings()->data;

            if($finalize && $storeSettings->ess_handle_unpaid_orders == 1){
                utilityManager::asyncHandlePaymentRequestOrder($orderObject->cto_id);
            }

            return $this->returnAnswer(resultObject::withData(1,'',$order->AdminAPIArray()));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function finalizePaymentRequest(){

        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['order_id'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            $order->cto_request_status = "finalized";
            $order->cto_finalized_on = date("Y-m-d H:i:s");



            $send = isset($_REQUEST['send']) ? $_REQUEST['send'] == 1 : false;

            if($send && $order->cto_request_sent != 1){
                $order->cto_request_sent = 1;
                $order->cto_request_sent_on = date("Y-m-d H:i:s");

                customerOrderManager::sendPaymentRequestToCustomer($order);
            }

            customerOrderManager::updateCustomerOrder($order);

            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettings = $bizModel->getBizStoreSettings()->data;

            if($storeSettings->ess_handle_unpaid_orders == 1){
                utilityManager::asyncHandlePaymentRequestOrder($order->cto_id);
            }

            return $this->returnAnswer(resultObject::withData(1,'',$order->AdminAPIArray()));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function sendPaymentRequest(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['order_id'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            if($order->cto_request_sent != 1){
                $order->cto_request_sent = 1;
                $order->cto_request_sent_on = date("Y-m-d H:i:s");

                customerOrderManager::updateCustomerOrder($order);
            }

            customerOrderManager::sendPaymentRequestToCustomer($order);

            return $this->returnAnswer(resultObject::withData(1,'',$order->AdminAPIArray()));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function collectPaymentRequest(){

        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['order_id'];

            $existingOrderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($existingOrderResult->code != 1){
                //return failure
                return $this->returnAnswer(resultObject::withData(-1));
            }

            $existingOrderObject = $existingOrderResult->data;

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($existingOrderObject->cto_cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_biz_id = $customer->cust_biz_id;

            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }



            //fill existing order data with the one received
            $existingOrderObject->fillPaymentRequestOrder($orderObject);

            //create items
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();

            foreach ($jarray as $item)
            {

                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustSubscription($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustPunchPass($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_punchpass_usage'));
                        }
                    }
                }

                $itemObjectsList[] = $itemObject;
            }



            $customerBilling = new customerBillingManager();
            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettings = $bizModel->getBizStoreSettings()->data;

            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount <= 0){
                //Empty previous items from order
                customerOrderManager::emptyCustomerOrderFromItems($existingOrderObject->cto_id);
                //connect items to order
                customerOrderManager::addCustItemsToOrder($existingOrderObject->cto_id,$itemObjectsList);
                customerOrderManager::updateCustomerOrder($existingOrderObject);


                if($storeSettings->ess_handle_unpaid_orders == 0){
                    utilityManager::asyncHandlePaymentRequestOrder($orderObject->cto_id);
                }
                utilityManager::asyncProcessAdminPaymentRequest($existingOrderObject->cto_id);

                $response['code'] = 1;
                $response["order_id"] = $existingOrderObject->cto_id;
                $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($existingOrderObject->cto_cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($existingOrderObject->cto_id,$invoiceID);

            $ipAddress = utilityManager::get_real_IP();

            $_REQUEST["ipAddress"] = $ipAddress;
            $transaction = customerTransactionObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $transaction->tr_cust_id = $customer->cust_id;
            $transaction->tr_biz_id = $customer->cust_biz_id;
            $transaction->tr_invoice_id = $invoiceID;
            $transaction->tr_country = bizManager::getBizCountryID($this->mobileRequestObject->bizID);

            if($transaction->tr_paymentMethodType != "Cash"){
                $chargeAttempt = $customerBilling->addDirectSuccesfulCustomerTransaction($transaction);
            }
            else{
                $customerBilling->setCustInvoicePendingByInvoiceID($invoiceID);
                $customerBilling->settleInvoiceWithCashTransaction($invoiceID,$transaction);
                $chargeAttempt = resultObject::withData(1);
            }

            if($chargeAttempt->code == 1){
                //Empty previous items from order
                customerOrderManager::emptyCustomerOrderFromItems($existingOrderObject->cto_id);
                //connect items to order
                customerOrderManager::addCustItemsToOrder($existingOrderObject->cto_id,$itemObjectsList);

                customerOrderManager::updateCustomerOrder($existingOrderObject);

                if($storeSettings->ess_handle_unpaid_orders == 0){
                    utilityManager::asyncHandlePaymentRequestOrder($orderObject->cto_id);
                }
                utilityManager::asyncProcessAdminPaymentRequest($existingOrderObject->cto_id);
                eventManager::actionTrigger(enumCustomerActions::paidPayment,$existingOrderObject->cto_cust_id, "order",'','',$existingOrderObject->cto_id);
                $response['code'] = 1;
                $response["order_id"] = $existingOrderObject->cto_id;
                $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
                $response['charge_code'] = $chargeAttempt->code;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }
            else{
                $response['code'] = 0;
                $response['charge_code'] = $chargeAttempt->code;
                $response['charge_message'] = $chargeAttempt->message;
                return $this->returnAnswer(resultObject::withData(-1,'',$response));
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function collectPaymentRequestStripe(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['order_id'];

            $existingOrderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($existingOrderResult->code != 1){
                //return failure
                return $this->returnAnswer(resultObject::withData(-1));
            }

            $existingOrderObject = $existingOrderResult->data;

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($existingOrderObject->cto_cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $orderObject = customerOrderObject::fillFromRequest($_REQUEST,$this->mobileRequestObject->deviceID);
            $orderObject->cto_cust_id = $customer->cust_id;
            $orderObject->cto_biz_id = $customer->cust_biz_id;

            if(isset($_REQUEST["cartBenefitRedeem"]) && $_REQUEST["cartBenefitRedeem"] != ''){
                $cartBenefit = customerManager::getBenefitByCode($_REQUEST["cartBenefitRedeem"]);

                if($cartBenefit->code == 1 && $cartBenefit->data->cb_redeemed == 0){
                    $orderObject->cto_benefit_id = $cartBenefit->data->cb_id;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,'cart_coupon_redeemed_already'));
                }
            }

            $customerBilling = new customerBillingManager();
            $customerBilling->setBillingBizID($customer->cust_biz_id);


            if($customer->cust_stripe_cust_id == ''){//need to create customer account in stripe
                $stripeAddResult = $customerBilling->addCustomerToStripe($customer);
                if($stripeAddResult->code == 1){
                    if($customer->cust_stripe_cust_id == ""){
                        $customer->cust_stripe_cust_id = $stripeAddResult->data;

                        $customersModel = new customerModel();
                        $customersModel->updateCustomer($customer);
                    }
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,"cant_add_to_stripe"));
                }
            }

            $paymentMethodId = "";
            if(isset($_REQUEST['payment_method_id']) && $_REQUEST['payment_method_id'] != ""){//new payment method - add to DB
                $payMethodResult = $customerBilling->getPaymentMethodDetails($customer,$_REQUEST['payment_method_id']);

                if($payMethodResult->code == 1){
                    $payMethod = $payMethodResult->data;

                    $customerBilling->attachPaymentMethodToCustomer($customer,$payMethod);

                    $paymentMethodId = $_REQUEST['payment_method_id'];

                    customerManager::sendAsyncCustomerDataUpdateToDevice('default_payment',$customer->cust_id);
                }
                else{
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$payMethodResult));
                }

            }
            else{
                $defaultPaySourceResult = $customerBilling->getCustomerDefaultPaymentSource($customer->cust_id);

                if($defaultPaySourceResult->code == 1){
                    $paymentMethodId = $defaultPaySourceResult->data->stripe_card_id;
                }
            }

            if($paymentMethodId == "" && $orderObject->amount > 0){
                return $this->returnAnswer(resultObject::withData(-1,$this->language->get('no_pay_source_selected')));
            }

            //fill existing order data with the one received
            $existingOrderObject->fillPaymentRequestOrder($orderObject);

            //create items list (user might have used rwards/subscriptions/punchpasses)
            $items = $_REQUEST["items"];
            $items = str_replace('\n','',$items);
            $jarray = json_decode($items);
            $itemObjectsList = array();

            $isRecurring = false;

            foreach ($jarray as $item)
            {

                $itemObject = customerOrderItemObject::fillFromRequest((array)$item);

                if($itemObject->tri_multiuse_src_type != ""){
                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'subscription'){

                        $usageObject = new customerSubscriptionUsageObject();

                        $usageObject->csuse_cust_id = $customer->cust_id;
                        $usageObject->csuse_biz_id = $customer->cust_biz_id;
                        $usageObject->csuse_csu_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->csuse_item_type = 'product';
                        $usageObject->csuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->csuse_source = 'auto';
                        $usageObject->csuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustSubscription($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_subscription_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 2 && $itemObject->tri_multiuse_src_type == 'punch_pass'){

                        $usageObject = new customerPunchUsageObject();

                        $usageObject->cpuse_cust_id =  $customer->cust_id;
                        $usageObject->cpuse_biz_id = $customer->cust_biz_id;
                        $usageObject->cpuse_cpp_id = $itemObject->tri_multiuse_src_usage_id;
                        $usageObject->cpuse_item_type = 'product';
                        $usageObject->cpuse_item_id = $itemObject->tri_itm_row_id;
                        $usageObject->cpuse_source = 'auto';
                        $usageObject->cpuse_type = 'usage';
                        $subUse = customerSubscriptionManager::useCustPunchPass($usageObject);

                        if($subUse->code == 1){
                            $itemObject->tri_multiuse_src_usage_id = $subUse->data;
                        }
                        else{
                            return $this->returnAnswer(resultObject::withData(-1,'no_punchpass_usage'));
                        }
                    }

                    if($itemObject->tri_item_type == 4){//is a subscription - recurring payment
                        $isRecurring = true;
                    }

                }

                $itemObjectsList[] = $itemObject;
            }

            $bizModel = new bizModel($this->mobileRequestObject->bizID);
            $storeSettings = $bizModel->getBizStoreSettings()->data;

            //if is free purchase - then do not attempt to charge and do not create invoice
            if($orderObject->cto_amount <= 0){
                //Empty previous items from order
                customerOrderManager::emptyCustomerOrderFromItems($existingOrderObject->cto_id);
                //connect items to order
                customerOrderManager::addCustItemsToOrder($existingOrderObject->cto_id,$itemObjectsList);
                customerOrderManager::updateCustomerOrder($existingOrderObject);

                if($storeSettings->ess_handle_unpaid_orders == 0){
                    utilityManager::asyncHandlePaymentRequestOrder($orderObject->cto_id);
                }
                utilityManager::asyncProcessAdminPaymentRequest($existingOrderObject->cto_id);

                $response['code'] = 1;
                $response["order_id"] = $existingOrderObject->cto_id;
                $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
                $response['charge_code'] = 1;
                return $this->returnAnswer(resultObject::withData(1,'',$response));
            }

            if(!isset($_REQUEST['pay_intent_id']) || $_REQUEST['pay_intent_id'] == ""){

                $stripePayIntentResult = $customerBilling->createPaymentIntent($customer,$orderObject,$paymentMethodId);

                if($stripePayIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$stripePayIntentResult));
                }

                $payIntentResult = $customerBilling->getStripePaymentIntent($stripePayIntentResult->data['intentId']);

                if($payIntentResult->code != 1){
                    return $this->returnAnswer(resultObject::withData(-1,"Error",$payIntentResult));
                }
                $payIntent = $payIntentResult->data;

                if($isRecurring){
                    $customerBilling->updateStripePaymentMethodToRecurring($payIntent->id);
                }

            }
            else{
                $stripeIntentResult = $customerBilling->getStripePaymentIntent($_REQUEST['pay_intent_id']);
                if($stripeIntentResult->code == 1){
                    $payIntent = $stripeIntentResult->data;
                }
                else{
                    return $this->returnAnswer(resultObject::withData(0,'error_pay_intent_id'));
                }
            }

            //create invoice
            $invoiceResult = $customerBilling->initiateCustomerInvoice($existingOrderObject->cto_cust_id);

            if($invoiceResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_invoice_created'));
            }

            $invoiceID = $invoiceResult->data;

            //connect order to invoice
            $customerBilling->connectCustInvoiceToCustOrder($existingOrderObject->cto_id,$invoiceID);

            //Mark invoice as paid
            $chargCode = $customerBilling->markCustomerInvoicePaidFromPaymentIntent($invoiceID,$payIntent,$this->mobileRequestObject->deviceID);

            //Process order and
            if($storeSettings->ess_handle_unpaid_orders == 0){
                utilityManager::asyncHandlePaymentRequestOrder($orderObject->cto_id);
            }
            utilityManager::asyncProcessAdminPaymentRequest($existingOrderObject->cto_id);

            //End purchase successfully
            eventManager::actionTrigger(enumCustomerActions::completedPurchase,$existingOrderObject->cto_cust_id, "order",'',$this->mobileRequestObject->deviceID,$existingOrderObject->cto_id);
            $response['code'] = 1;
            $response["order_id"] = $existingOrderObject->cto_id;
            $response["long_order_id"] = $existingOrderObject->cto_long_order_id;
            $response['charge_code'] = $chargCode->code;
            return $this->returnAnswer(resultObject::withData(1,'',$response));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Deletes payment request
     */
    function deletePaymentRequest(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $orderLongID = $_REQUEST['order_id'];

            $orderResult = customerOrderManager::getCustomerOrderByLongID($orderLongID);

            if($orderResult->code != 1){
                return $this->returnAnswer($orderResult);
            }

            $order = $orderResult->data;

            $result = customerOrderManager::deleteCustomerOrderById($order->cto_id);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* member Subscriptions */

    /**
     * Returns list of subscription for member
     */
    function getMemberSubscriptions(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $result = $customerModel->getCustomerSubscriptionsForAdmin($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getAllMemberActiveSubscriptions(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;


            $result = $customerModel->getActiveCustomerSubscriptionsForAdmin($customer);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Set the status of member's subscription
     */
    function setMemberSubscriptionStatus(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custSubID = $_REQUEST['cust_sub_id'];

            $state = $_REQUEST["state"];

            $custSubResult = customerSubscriptionManager::getCustomerSubsciptionByID($custSubID);

            if($custSubResult->code != 1){
                return $this->returnAnswer($custSubResult);
            }

            $custSub = $custSubResult->data;

            $custSub->csu_status = $state;

            customerSubscriptionManager::updateCustomerSubsciption($custSub);

            $entry = new customerSubscriptionUsageObject();

            $type = 'resume';

            switch($state){
                case 'active':
                    $type = 'resume';
                    break;
                case 'frozen':
                    $type = 'freeze';
                    break;
                case 'cancelled':
                    $type = 'cancel';
                    break;
            }

            $entry->csuse_cust_id = $custSub->csu_cust_id;
            $entry->csuse_csu_id = $custSub->csu_id;
            $entry->csuse_biz_id = $custSub->csu_biz_id;
            $entry->csuse_type = $type;
            $entry->csuse_source = 'manual';
            $entry->csuse_source_id = $this->mobileRequestObject->acID;

            customerSubscriptionManager::addCustomerSubscriptionUsage($entry);

            return $this->returnAnswer(resultObject::withData(1,'',$custSub));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Add usage entry to member's subscription
     */
    function addMemberSubscriptionEntry(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custSubID = $_REQUEST['cust_sub_id'];

            $custSubResult = customerSubscriptionManager::getCustomerSubsciptionByID($custSubID);

            if($custSubResult->code != 1){
                return $this->returnAnswer($custSubResult);
            }

            $custSub = $custSubResult->data;

            $entry = new customerSubscriptionUsageObject();

            $entry->csuse_cust_id = $custSub->csu_cust_id;
            $entry->csuse_csu_id = $custSub->csu_id;
            $entry->csuse_biz_id = $custSub->csu_biz_id;
            $entry->csuse_item_type = 'other';
            $entry->csuse_type = 'usage';
            $entry->csuse_source = 'manual';
            $entry->csuse_source_id = $this->mobileRequestObject->acID;

            $result = customerSubscriptionManager::useCustSubscription($entry);

            if($result->code != 1){
                return $this->returnAnswer($result);
            }

            $custSubResult = customerSubscriptionManager::getCustomerSubsciptionByID($custSubID);

            return $this->returnAnswer($custSubResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* member punch passes */

    /**
     * Returns list of pucnh passes for member
     */
    function getMemberPunchpasses(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $result = $customerModel->getCustomerPunchpassesForAdmin($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getAllMemberActivePunchpasses(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $result = $customerModel->getActiveCustomerPunchpassesForAdmin($customer);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Set the status of member's punch pass
     */
    function setMemberPunchpassesStatus(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custPpassID = $_REQUEST['cust_ppass_id'];

            $state = $_REQUEST["state"];

            $custPpassResult = customerSubscriptionManager::getCustomerPunchpassByID($custPpassID);

            if($custPpassResult->code != 1){
                return $this->returnAnswer($custPpassResult);
            }

            $custPpass = $custPpassResult->data;

            $custPpass->cpp_status = $state;

            customerSubscriptionManager::updateCustomerPunchpass($custPpass);

            $entry = new customerPunchUsageObject();

            $type = 'resume';

            switch($state){
                case 'active':
                    $type = 'resume';
                    break;
                case 'frozen':
                    $type = 'freeze';
                    break;
                case 'cancelled':
                    $type = 'cancel';
                    break;
            }

            $entry->cpuse_cust_id = $custPpass->cpp_cust_id;
            $entry->cpuse_cpp_id = $custPpass->cpp_id;
            $entry->cpuse_biz_id = $custPpass->cpp_biz_id;
            $entry->cpuse_type = $type;
            $entry->cpuse_source = 'manual';
            $entry->cpuse_source_id = $this->mobileRequestObject->acID;

            customerSubscriptionManager::addCustomerPunchUsage($entry);

            return $this->returnAnswer(resultObject::withData(1,'',$custPpass));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Adds usage entry to member's punch pass
     */
    function addMemberPunchpassesnEntry(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custPpassID = $_REQUEST['cust_ppass_id'];

            $custPpassResult = customerSubscriptionManager::getCustomerPunchpassByID($custPpassID);

            if($custPpassResult->code != 1){
                return $this->returnAnswer($custPpassResult);
            }

            $custPpass = $custPpassResult->data;

            $entry = new customerPunchUsageObject();

            $entry->cpuse_cust_id = $custPpass->cpp_cust_id;
            $entry->cpuse_cpp_id = $custPpass->cpp_id;
            $entry->cpuse_biz_id = $custPpass->cpp_biz_id;
            $entry->cpuse_item_type = 'other';
            $entry->cpuse_type = 'usage';
            $entry->cpuse_source = 'manual';
            $entry->cpuse_source_id = $this->mobileRequestObject->acID;

            $result = customerSubscriptionManager::useCustPunchPass($entry);

            if($result->code != 1){
                return $this->returnAnswer($result);
            }

            $custPpassResult = customerSubscriptionManager::getCustomerPunchpassByID($custPpassID);

            return $this->returnAnswer($custPpassResult);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member  files and forms */

    /**
     * Returns list of files and forms for member
     */
    function getMemberFiles(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $result = $customerModel->getCustomerDocuments($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Send file to member
     */
    function sendFileToMember(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $document = new customerDocumentObject();
            $innerFileName = str_replace('.','_',$_REQUEST['fileName']);
            $innerFileName = str_replace(' ','_',$innerFileName);
            $file = $_FILES[$innerFileName];
            $bizId = $this->mobileRequestObject->bizID;

            $fileName = $file['name'];
            $fileData = utilityManager::file_get_contents_pt($file['tmp_name']);
            $title = urldecode($_REQUEST['title']);

            $fileExtention = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $newFileName = str_replace(" ","_",pathinfo($fileName, PATHINFO_FILENAME))."_".str_replace(".","",microtime(true)).".".pathinfo($fileName, PATHINFO_EXTENSION);
            $fileType = "";
            switch ($fileExtention){
                case "xlsx":
                case "xls":
                case "csv":
                    $fileType="exl";
                    break;
                case "doc":
                case "docx":
                    $fileType="doc";
                    break;
                case "pdf":
                    $fileType="pdf";
                    break;
                case "gif":
                case "png":
                case "jpg":
                case "jpeg":
                    $fileType="img";
                    break;
            }


            $image_storage = imageStorage::getStorageAdapter();


            $urlFile = $image_storage->directUpload($file['tmp_name'], $bizId.'/docs/'.$newFileName);

            $document->do_biz_id = $this->mobileRequestObject->bizID;
            $document->do_cust_id = $cust_id;
            $document->do_uploaded_by = 'admin';
            $document->do_uploader_id = $this->mobileRequestObject->acID;
            $document->do_url = $urlFile;
            $document->do_title = $title;
            $document->do_type = $fileType;

            $result = $customerModel->addCustomerDocument($document);

            if($result->code != 1){
                return $this->returnAnswer($result);
            }

            $document->do_id = $result->data;

            return $this->returnAnswer(resultObject::withData($result->code,$result->message,$document));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function sendFileURLToMember(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $document = new customerDocumentObject();
            $title = urldecode($_REQUEST['title']);

            $fileExtention = strtolower(pathinfo($_REQUEST['url'], PATHINFO_EXTENSION));

            $fileType = "";
            switch ($fileExtention){
                case "xlsx":
                case "xls":
                case "csv":
                    $fileType="exl";
                    break;
                case "doc":
                case "docx":
                    $fileType="doc";
                    break;
                case "pdf":
                    $fileType="pdf";
                    break;
                case "gif":
                case "png":
                case "jpg":
                case "jpeg":
                    $fileType="img";
                    break;
            }

            $urlFile = $_REQUEST['url'];

            $document->do_biz_id = $this->mobileRequestObject->bizID;
            $document->do_cust_id = $cust_id;
            $document->do_uploaded_by = 'admin';
            $document->do_uploader_id = $this->mobileRequestObject->acID;
            $document->do_url = $urlFile;
            $document->do_title = $title;
            $document->do_type = $fileType;

            $result = $customerModel->addCustomerDocument($document);

            if($result->code != 1){
                return $this->returnAnswer($result);
            }

            $document->do_id = $result->data;

            return $this->returnAnswer(resultObject::withData($result->code,$result->message,$document));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getMemberFilledForm(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $docID = $_REQUEST['doc_id'];

            $docResult = $customerModel->getCustomerDocumentByID($docID);

            if($docResult->code != 1){
                return $this->returnAnswer(resultObject::withData(-1,'no_document_found'));
            }

            $doc = $docResult->data;

            $formManager = new customFormManager();

            $data = $formManager->getFormFields($doc->do_external_id,$doc->do_form_id);

            return $this->returnAnswer(resultObject::withData(1,'',$data));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }

    }

    /* Member loyalty cards */

    /**
     * Returns list of loyatly cards
     */
    function getMemberLoyaltyCards(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $result = customerManager::getCustomerLoyaltyCardsByCustomerForAdmin($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    function getAllMemberLoyaltyCards(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;


            $result = customerManager::getCustomerLoyaltyCardsByCustomerForAdmin($customer,0,0);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Start a new loyalty card for member
     */
    function startNewLoyaltyCardForMember(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }


            $cust_id = $_REQUEST['cust_id'];
            $loyaltyID = $_REQUEST['loyalty_id'];
            $stampCount = isset($_REQUEST['stamp_count']) ? $_REQUEST['stamp_count'] : 1;
            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $result = customerManager::startNewLoyalyCardForCustomerFromAdmin($customer,$loyaltyID,$this->mobileRequestObject->acID,$stampCount);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Add stamp to member's loyalty member
     */
    function addStampToMemberLoyaltyCard(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $custloyaltyID = $_REQUEST['cust_loyalty_id'];
            $stampCount = isset($_REQUEST['stamp_count']) ? $_REQUEST['stamp_count'] : 1;
            $custLoyaltyResult = customerManager::getLoyaltyCardByCardID($custloyaltyID);

            if($custLoyaltyResult->code != 1){
                return $this->returnAnswer(resultObject::withData(-1,'no_cust_loyalty_found'));
            }

            $custLoyalty = $custLoyaltyResult->data;

            $generalResult = resultObject::withData(1);
            $result = resultObject::withData(0,'not_stamps_added');
            for ($i = 0; $i < $stampCount; $i++)
            {

                $result = customerManager::addPunchStampToCustomerLoyaltyCard($custLoyalty,'manual',$this->mobileRequestObject->acID);

                if($result->code != 1){
                    $generalResult = $result;
                    break;
                }
                else{
                    $custLoyalty = $result->data;
                }


            }

            $generalResult = $result;
            return $this->returnAnswer($generalResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member appointments */

    /**
     * Returns appointments for member
     */
    function getMemberAppointments(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];
            $skip = $_REQUEST['existing'];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;


            $result = $customerModel->getAllCustomersUpcomingAppointmentsForAdmin($customer,$skip,25);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member points */

    /**
     * Returns points history for member
     */
    function getMemberPointsHistory(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];
            $skip = $_REQUEST['existing'];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $entriesResult = $customerModel->getPointsHistoryForCustomer($customer,$skip,25);

            $list = array();
            if($entriesResult->code == 1){
                foreach ($entriesResult->data as $entry)
                {
                    $list[] = $entry->getAPIFormattedArray();
                }
            }

            return $this->returnAnswer(resultObject::withData(1,'',$list));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Grant points to member
     */
    function grantPointsToMember(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $pointsHistoryObject = new customerPointsHistoryObject();

            $pointsHistoryObject->cph_biz_id = $this->mobileRequestObject->bizID;
            $pointsHistoryObject->cph_cust_id = $cust_id;
            $pointsHistoryObject->cph_external_id = $this->mobileRequestObject->acID;
            $pointsHistoryObject->cph_value = $_REQUEST['amount'];
            $pointsHistoryObject->cph_reason_text = $_REQUEST['reason'];
            $pointsHistoryObject->cph_source = 'personal_grant';

            $result = $customerModel->grantCustomerPoints($customer,$pointsHistoryObject);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member history */

    /**
     * Returns list of member's clients history
     */
    function getMemberActivityHistory(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];
            $skip = $_REQUEST['existing'];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $result = $customerModel->getCustomerHistoryEntriesByCustID($customer->cust_id,$skip,40);

            return $this->returnAnswer($result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Add entry to member's activities log
     */
    function addMemberActivityEntry(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $historyEntry = new customerHistoryObject();

            $historyEntry->ch_cust_id = $customer->cust_id;
            $historyEntry->ch_creator_account_id = $this->mobileRequestObject->acID;
            $historyEntry->ch_status = $_REQUEST['action'];
            $historyEntry->ch_short_mess = isset($_REQUEST['note']) ? $_REQUEST['note'] : '';
            $historyEntry->ch_auto = 0;
            $historyEntry->ch_device_id = $this->mobileRequestObject->deviceID;

            $entryResult = $customerModel->addCustomerHistory($historyEntry);

            if($entryResult->code == 1){
                $result = $customerModel->getCustomerHistoryByID($entryResult->data);

                return $this->returnAnswer($result);
            }

            return $this->returnAnswer($entryResult);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Referals */

    /**
     * Get list of member's referals
     */
    function getMemberReferals(){
        try{
            if(!$this->mobileRequestObject->isAuthorizedRequest()){
                return $this->returnAnswer(resultObject::withData(0,'token_error'));
            }

            $cust_id = $_REQUEST['cust_id'];
            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $friendsResult = $customerModel->getCustomerInvitedFriendsFromAdmin($customer,$skip,40);

            $list = array();
            if($friendsResult->code == 1){
                foreach ($friendsResult->data as $friend)
                {
                    $list[] = $friend->getArrayForFriendList();
                }

            }

            return $this->returnAnswer(resultObject::withData(1,'',$list));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member chat */

    /**
     * Returns list of chat entries with member
     */
    function getChatWithMember(){
        try{
            $chatManager = new chatManager();
            $customerModel = new customerModel();
            $cust_id = $_REQUEST['cust_id'];

            $skip = isset($_REQUEST['existing']) ? $_REQUEST['existing'] : 0;

            $customerModel = new customerModel();
            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $customer->cust_unread_chats = 0;
            $customerModel->updateCustomer($customer);

            chatManager::setChatMessagesAsReadByAdmin($cust_id);

            $chatMessages = $chatManager->getLatestMessagesFromCustomerToAdmin($cust_id,$this->mobileRequestObject->bizID,$skip,20);
            return $this->returnAnswer($chatMessages);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /**
     * Sends chat message to member
     */
    function sendChatMessageToMember(){
        try{
            $message = $_REQUEST["message"];
            $cust_id = $_REQUEST['cust_id'];

            $chatManager = new chatManager();

            $customerModel = new customerModel();
            $chatMessageObject = new chatMessageObject();

            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $chatMessageObject->ch_user = $this->mobileRequestObject->bizID;
            $chatMessageObject->ch_customer = $cust_id;
            $chatMessageObject->ch_account = $this->mobileRequestObject->acID;
            $chatMessageObject->ch_message = $message;
            $chatMessageObject->ch_dir = 1;
            $chatMessageObject->ch_new = 1;

            $resultChat = $chatManager->sendChatFromAdmin($chatMessageObject,$customer);

            if($resultChat->code != 1){
                return $this->returnAnswer($resultChat);
            }

            $chatMessages = $chatManager->getLatestMessagesFromCustomerToAdmin($cust_id,$this->mobileRequestObject->bizID,0,1);
            return $this->returnAnswer($chatMessages);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    /* Member pre-paid */

    /**
     * Get List of member's claims
     */
    function getMemberClaims(){
        try{
            $cust_id = $_REQUEST['cust_id'];
            $skip = $_REQUEST['existing'];
            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($cust_id);

            if($customerResult->code == 0){
                return $this->returnAnswer(resultObject::withData(-1,'no_customer_found'));
            }

            $customer = $customerResult->data;

            $claims = customerManager::getAllCustomerClaimsbyCustID($cust_id,$skip,40);

            return $this->returnAnswer(resultObject::withData(1,'',$claims));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }

    //function temp(){

    //    $pm = new partnerModel();

    //    $resellerData = $pm->getResellerByID(1392);
    //    //print_r($resellerData->data);
    //    //$resellerObj = resellerObject::withData($resellerData->data);

    //    print_r(partnersManager::issuePartnerJWTToken($resellerData->data));
    //}
}
