<?php

class Market extends Controller{

    function __construct(){
        parent::__construct();
    }

    function verifyDomain(){
        try{
            $domain = $_REQUEST['domain'];

            $code =  partnersManager::checkIfVerifiedMarketDomain($domain) ? 1 : 0;

            return $this->returnAnswer(resultObject::withData($code));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($_REQUEST));
            $result = resultObject::withData(0,$e->getMessage());
            return $this->returnAnswer($result);
        }
    }



}
