<?php

/**
 * adminManager short summary.
 *
 * adminManager description.
 *
 * @version 1.0
 * @author Jonathan
 */
class adminManager extends Manager
{
    
    public static function logAccountOnDevice($acId,$deviceID,$os){
        $acdevice = new accountActiveDeviceObject();
        $acdevice->aad_ac_id = $acId;
        $acdevice->aad_device_id = $deviceID;
        $acdevice->aad_device_os = $os;
        
        return self::addAccountActiveDevice($acdevice);        
    }

    public static function logAccountOffDevice($acId,$deviceID){
        $instance = new self();

        $instance->markAccountAsLoggedOffDevice($acId,$deviceID);

        return;
    }

    public static function logAccountOffAllDevice($acId){
        $instance = new self();

        $instance->markAccountAsLoggedOffAllDevices($acId);

        return;
    }

    public static function logAccountOffAllOtherDevices($acId,$keep_deviceId){
        $instance = new self();
        
        $allDevices = $instance->getAllActiveDeviceForAccountID_DB($acId);

        foreach ($allDevices as $device)
        {
        	if($device["aad_device_id"] != $keep_deviceId){
                $instance->markAccountAsLoggedOffDevice($acId,$device["aad_device_id"]);
            }
        }
        
        return;
    }

    public static function getActiveDeviceForAccountID($acId){
        try{
            $instance = new self();

            $accountActiveDeviceData = $instance->getActiveDeviceForAccountID_DB($acId);

            $accountActiveDeviceObj = accountActiveDeviceObject::withData($accountActiveDeviceData);
            $result = resultObject::withData(1,'',$accountActiveDeviceObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$acId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAccountBizPositions(accountObject $account,bizObject $biz){
        $instance = new self();
        if($account->ac_type == "reseller_client" || $account->ac_type == "account_owner"){
            $positions = $instance->db->getTable("SELECT pos_id FROM tbl_positions WHERE pos_business_type = {$biz->getBizBusinessType()}");            
        }
        else{
            $positions = $instance->db->getTable("SELECT abp_pos_id as pos_id FROM tbl_account_biz_positions 
                                                WHERE abp_ac_id = {$account->ac_id} 
                                                AND abp_biz_id = {$biz->biz_id}");
        }

        return $positions;
    }

    public static function isAccountLoggedInOnOtherDevices(accountObject $account,$deviceID){
        $instance = new self();

        $otherDevicesCount = $instance->db->getVal("SELECT COUNT(*) FROM tbl_account_active_devices
                WHERE aad_ac_id = {$account->ac_id}
                AND aad_device_id <> '$deviceID'
                AND aad_logged_off IS NULL");

        return $otherDevicesCount > 0;
    }

    public static function sendUpdateToAccountDevice($acId,$message_type,$message){
        $instance = new self();

        $accountActiveDeviceData = $instance->getActiveDeviceForAccountID_DB($acId);

        if(!isset($accountActiveDeviceData['aad_id']) || $accountActiveDeviceData['aad_id'] == ""){//no active device found
            return;
        }

        $accountActiveDeviceObj = accountActiveDeviceObject::withData($accountActiveDeviceData);

        $receiver = utilityManager::encodeToHASH($acId);

        wsocketManager::sendIMAsync($message,$receiver,$message_type);

        return;
    }

    public static function sendUpdateToAllAccountDevice($acId,$message_type,$message){
        
        $receiver = utilityManager::encodeToHASH($acId);

        wsocketManager::sendIMAsync($message,$receiver,$message_type);

        return;
    }

    public static function sendUpdateToBizDevices($bizID,$message_type,$message){
        $instance = new self();

        $activeDevices  = $instance->getActiveDevicesForBiz($bizID);

        foreach ($activeDevices as $activeDevice)
        {
        	$receiver = utilityManager::encodeToHASH($activeDevice["aad_ac_id"]);

            wsocketManager::sendIMAsync($message,$receiver,$message_type);
        }        

        return;
    }

    public static function sendUpdateToBizPositionDevices($bizID,$position,$message_type,$message){
        $instance = new self();

        $activeDevices  = $instance->getActiveDevicesForBizPosition($bizID,$position);

        foreach ($activeDevices as $activeDevice)
        {
            $accountActiveDeviceObj = accountActiveDeviceObject::withData($activeDevice);

        	$receiver = utilityManager::encodeToHASH($activeDevice["aad_ac_id"]);

            wsocketManager::sendIMAsync($message,$receiver,$message_type);
        }        

        return;
    }

    public static function getBizFleetName($deviceID,$bizID){
        try{
            $instance = new self();
            $name =  $instance->db->getVal("SELECT flt_name FROM tbl_admin_fleet 
                    WHERE flt_biz_id = $bizID
                    AND flt_device_id = '$deviceID'");

            if($name != ''){
                return resultObject::withData(1,'',$name);
            }
            else{
                return resultObject::withData(0,'no_name');
            }
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function setBizFleetName(adminRequestObject $request,$name){
        try{
            $instance = new self();
            $instance->db->execute("INSERT IGNORE INTO tbl_admin_fleet SET
                                        flt_biz_id = {$request->bizID},
                                        flt_device_id = '{$request->deviceID}',
                                        flt_device_type = '{$request->deviceType}',
                                        flt_name = '$name'");

            return resultObject::withData(1);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /************************************* */
    /*   BASIC ACCOUNTACTIVEDEVICE - PUBLIC           */
    /************************************* */

    /**
    * Insert new accountActiveDeviceObject to DB
    * Return Data = new accountActiveDeviceObject ID
    * @param accountActiveDeviceObject $accountActiveDeviceObj 
    * @return resultObject
    */
    public static function addAccountActiveDevice(accountActiveDeviceObject $accountActiveDeviceObj){       
        try{
            $instance = new self();
            
            $newId = $instance->addAccountActiveDeviceDB($accountActiveDeviceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($accountActiveDeviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get accountActiveDevice from DB for provided ID
    * * Return Data = accountActiveDeviceObject
    * @param int $accountActiveDeviceId 
    * @return resultObject
    */
    public static function getAccountActiveDeviceByID($accountActiveDeviceId){
        
        try {
            $instance = new self();
            $accountActiveDeviceData = $instance->loadAccountActiveDeviceFromDB($accountActiveDeviceId);
            
            $accountActiveDeviceObj = accountActiveDeviceObject::withData($accountActiveDeviceData);
            $result = resultObject::withData(1,'',$accountActiveDeviceObj);
            return $result;
          }
          //catch exception
          catch(Exception $e) {
              errorManager::addAPIErrorLog('API Model',$e->getMessage(),$accountActiveDeviceId);
              $result = resultObject::withData(0,$e->getMessage());
              return $result;
          }
    }

    /**
    * Update a1History in DB
    * @param accountActiveDeviceObject $accountActiveDeviceObj 
    * @return resultObject
    */
    public static function updateAccountActiveDevice(accountActiveDeviceObject $accountActiveDeviceObj){        
        try{
            $instance = new self();
            $instance->upateAccountActiveDeviceDB($accountActiveDeviceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($accountActiveDeviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete accountActiveDevice from DB
    * @param int $accountActiveDeviceID 
    * @return resultObject
    */
    public static function deleteAccountActiveDeviceById($accountActiveDeviceID){
        try{
            $instance = new self();
            $instance->deleteAccountActiveDeviceByIdDB($accountActiveDeviceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$accountActiveDeviceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC ACCOUNTACTIVEDEVICE - DB METHODS           */
    /************************************* */

    private function addAccountActiveDeviceDB(accountActiveDeviceObject $obj){

        if (!isset($obj)){
            throw new Exception("accountActiveDeviceObject value must be provided");             
        }

        $aad_logged_offDate = isset($obj->aad_logged_off) ? "'".$obj->aad_logged_off."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_account_active_devices SET 
        aad_ac_id = {$obj->aad_ac_id},
        aad_device_id = '".addslashes($obj->aad_device_id)."',
        aad_device_os = '{$obj->aad_device_os}',
        aad_active_biz_id = {$obj->aad_active_biz_id},
        aad_active_position = {$obj->aad_active_position},
        aad_logged_off = $aad_logged_offDate
                    ");
            return $newId;
    }

    private function loadAccountActiveDeviceFromDB($accountActiveDeviceID){

        if (!is_numeric($accountActiveDeviceID) || $accountActiveDeviceID <= 0){
            throw new Exception("Illegal value $accountActiveDeviceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_account_active_devices WHERE aad_id = $accountActiveDeviceID AND aad_logged_off IS NULL");
    }

    private function upateAccountActiveDeviceDB(accountActiveDeviceObject $obj){

        if (!isset($obj->aad_id) || !is_numeric($obj->aad_id) || $obj->aad_id <= 0){
            throw new Exception("accountActiveDeviceObject value must be provided");             
        }

        $aad_logged_onDate = isset($obj->aad_logged_on) ? "'".$obj->aad_logged_on."'" : "null";

        $aad_logged_offDate = isset($obj->aad_logged_off) ? "'".$obj->aad_logged_off."'" : "null";

        $this->db->execute("UPDATE tbl_account_active_devices SET 
        aad_ac_id = {$obj->aad_ac_id},
        aad_device_id = '".addslashes($obj->aad_device_id)."',
        aad_device_os = '{$obj->aad_device_os}',
        aad_logged_on = $aad_logged_onDate,
        aad_active_biz_id = {$obj->aad_active_biz_id},
        aad_active_position = {$obj->aad_active_position},
        aad_logged_off = $aad_logged_offDate
        WHERE aad_id = {$obj->aad_id} 
            ");
    }

    private function deleteAccountActiveDeviceByIdDB($accountActiveDeviceID){

        if (!is_numeric($accountActiveDeviceID) || $accountActiveDeviceID <= 0){
            throw new Exception("Illegal value $accountActiveDeviceID");             
        }

        $this->db->execute("DELETE FROM tbl_account_active_devices WHERE aad_id = $accountActiveDeviceID AND aad_logged_off IS NULL");
    }

    private function markAccountAsLoggedOffDevice($acID,$deviceID){
        $this->db->execute("UPDATE tbl_account_active_devices SET
                    aad_logged_off = NOW()
                WHERE aad_ac_id = $acID
                AND aad_device_id = '$deviceID'
                AND aad_logged_off IS NULL");

        return;
    }

    private function markAccountAsLoggedOffAllDevices($acID){
        $this->db->execute("UPDATE tbl_account_active_devices SET
                    aad_logged_off = NOW()
                WHERE aad_ac_id = $acID
                AND aad_logged_off IS NULL");

        return;
    }

    private function getActiveDeviceForAccountID_DB($accountID){
        return $this->db->getRow("SELECT * FROM tbl_account_active_devices 
                                    WHERE aad_ac_id = $accountID
                                    AND aad_logged_off IS NULL");
    }

    private function getAllActiveDeviceForAccountID_DB($accountID){
        return $this->db->getTable("SELECT * FROM tbl_account_active_devices 
                                    WHERE aad_ac_id = $accountID
                                    AND aad_logged_off IS NULL");
    }

    private function getActiveDevicesForBiz($bizID){
        return $this->db->getTable("SELECT * FROM tbl_account_active_devices
                                    WHERE aad_active_biz_id = $bizID
                                    AND aad_logged_off IS NULL");
    }

    private function getActiveDevicesForBizPosition($bizID,$position){
        return $this->db->getTable("SELECT * FROM tbl_account_active_devices
                                    WHERE aad_active_biz_id = $bizID
                                    AND aad_active_position = $position
                                    AND aad_logged_off IS NULL");
    }
}