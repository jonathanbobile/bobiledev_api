<?php
/**
 * Methods for Utility Functions
 *
 * @version 1.0
 * @author PapTap
 */

class bookingManager extends Manager
{
    private $currentRecordNo = 0;
    private $maxRecords = 30;
    private $index = 0;
    private $slotsList = array();

    function __construct(){
        parent::__construct();
    }

    /************************************* */
    /*   ADVANCED USAGE - PUBLIC           */
    /************************************* */

    public static function getAvailableClassesForBiz($biz_id){
        try{
            $instance = new self();
            $rowsList = $instance->getClassesForBiz_DB($biz_id);

            $classes = array();
            foreach ($rowsList as $entry)
            {
            	$classObj = classObject::withData($entry);

                $employeeObj = employeeObject::withData($entry);
                $classObj->setEmployee($employeeObj);

                if(isset($entry['bmt_id']) && $entry['bmt_id'] > 0){
                    $serviceObj = serviceObject::withData($entry);
                    $classObj->setService($serviceObj);
                }

                $classes[] = $classObj;
            }

            return resultObject::withData(1,'',$classes);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$biz_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeeMeetingsForCustomer(customerObject $customer,$start_date){
        try{
            $instance = new self();
            $meetings = array();
            $meetingsRows = $instance->getEmployeeMeetingForCustomerDB($customer,$start_date);

            foreach ($meetingsRows as $meetingRow)
            {
            	$meeting = employeeMeetingObject::withData($meetingRow);
                $meetings[] = $meeting;
            }

            return resultObject::withData(1,'',$meetings);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['start_date'] = $start_date;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerClassSignUps(customerObject $customer,$start_date){
        try{
            $instance = new self();
            $classesRows = $instance->getCustomerClassSignUpsDB($customer,$start_date);

            $classDatesList = array();
            foreach ($classesRows as $classRow)
            {

                $classDate = classDateObject::withData($classRow);


                $classDatesList[] = $classDate;
            }

            return resultObject::withData(1,'',$classDatesList);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['start_date'] = $start_date;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getClassDatesForClassByClassID($classID){
        try{
            $instance = new self();
            $datesRows = $instance->getClassDatesForClassByClassIDDB($classID);
            $dates = array();
            foreach ($datesRows as $dateRow)
            {

            	$date = classDateObject::withData($dateRow);

                $dates[] = $date;
            }
            return resultObject::withData(1,'',$dates);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getMeetingsList($empID,$meetTypeID,$startDate,$offset){
        try{
            $instance = new self();

            $startHourID = $instance->getStartHourIDFromDate($startDate,$offset);


            if($empID != 0){
                $employeeRow = $instance->loadEmployeeFromDB($empID);
                $employee = employeeObject::withData($employeeRow);
            }
            else{
                $employee = new employeeObject();
            }

            if($meetTypeID != 0){
                $meetTypeRow = $instance->loadServiceFromDB($meetTypeID);
                $meetType = serviceObject::withData($meetTypeRow);
            }
            else{
                $meetType = new serviceObject();
            }

            if($empID != 0 && $meetTypeID != 0){

                $instance->getMeetingListBoth($employee,$meetType,$startDate,$startHourID);
            }
            else if($empID == 0 && $meetTypeID != 0){

                $instance->getMeetingListOnlyMeetingType($meetType,$startDate,$startHourID);
            }
            else if($empID != 0 && $meetTypeID == 0){

                $instance->getMeetingListOnlyEmployee($employee,$startDate,$startHourID);
            }
            else{

                return resultObject::withData(0);
            }

            $meetings = array();
            foreach ($instance->slotsList as $slotData)
            {
            	$meetings[] = meetingSlotObject::withData($slotData);
            }

            return resultObject::withData(1,'',$meetings);
        }
        catch(Exception $e){
            $data = array();
            $data['empID'] = $empID;
            $data['meetType'] = $meetTypeID;
            $data['startDate'] = $startDate;
            $data['offset'] = $offset;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getServicesListForBizID($bizID){
        try{
            $instance = new self();

            $servicesRow = $instance->getServicesListForBizIDDB($bizID);

            $services = array();

            foreach ($servicesRow as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }

            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAvailableServicesListForBizID($bizID){
        try{
            $instance = new self();

            $servicesRow = $instance->getAvailableServicesListForBizIDDB($bizID);

            $services = array();

            foreach ($servicesRow as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }

            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getServicesListForEmployee(employeeObject $employee){
        try{
            $instance = new self();

            $servicesRow = $instance->getServicesForEmployee($employee);

            $services = array();

            foreach ($servicesRow as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }

            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employee));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeesForBizID($bizID){
        try{
            $instance = new self();

            $employeeRows = $instance->getEmployeeRowsForBizID($bizID);

            $employees = array();

            foreach ($employeeRows as $employeeRow)
            {
            	$employees[] = employeeObject::withData($employeeRow);
            }


            return resultObject::withData(1,'',$employees);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeesForService(serviceObject $service){
        try{
            $instance = new self();

            $employeeRows = $instance->getEmployeesRowsForService($service);

            $employees = array();

            foreach ($employeeRows as $employeeRow)
            {
            	$employees[] = employeeObject::withData($employeeRow);
            }

            return resultObject::withData(1,'',$employees);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($service));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeesIDsForService($serviceId){

        $instance = new self();
        return $instance->db->getTable("SELECT emt_emp_id FROM tbl_employee_meet_type
                                                        WHERE emt_bmt_id = $serviceId
                                                        AND emt_active = 1");
    }

    public static function setMeeting(employeeMeetingObject $meeting,customerObject $customer,$start_time_stamp,$end_time_stamp){
        try{
            $instance = new self();
            $meetingStartDate = date("Y-m-d",$start_time_stamp);
            $meetingEndDate = date("Y-m-d",$end_time_stamp);
            $meetingStartHour = date("H:i",$start_time_stamp);
            $meetingEndHour = date("H:i",$end_time_stamp);

            $startHourID = $instance->getHourIDFromStartHour($meetingStartHour);
            $endHourID = $instance->getHourIDFromEndHour($meetingEndHour);

            $start = $meetingStartDate." ".addslashes($meetingStartHour).":00";
            $end = $meetingEndDate." ".addslashes($meetingEndHour).":00";

            $isFuture = $instance->isFutureDate($start_time_stamp);

            if(!$isFuture){
                $responce = new stdClass();
                $responce->result["code"] = "9";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "222";
                $responce->result["internal_desc"] = "Past Dates Received";
                return resultObject::withData(1,'',$responce);
            }

            $isSlotAvailable = $instance->verifyAvailability($meeting->em_emp_id,$meetingStartDate,$meetingEndDate,$startHourID,$endHourID);

            if(!$isSlotAvailable){
                $responce = new stdClass();
                $responce->result["code"] = "8";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "221";
                $responce->result["internal_desc"] = "Slot is Taken";
                return resultObject::withData(1,'',$responce);
            }

            $serviceRow = $instance->loadServiceFromDB($meeting->em_meet_type);
            $service = serviceObject::withData($serviceRow);


            if($meeting->em_emp_id != 0){
                $employeeRow = $instance->loadEmployeeFromDB($meeting->em_emp_id);
                $employee = employeeObject::withData($employeeRow);
            }
            else{
                $employee = new employeeObject();
            }

            $meeting->setEmployee($employee);

            $country = bizManager::getBizCountryID($customer->cust_biz_id);

            $meetStart = ($country=="1") ? date( 'm/d/Y h:i A', $start_time_stamp ) : date( 'd/m/Y H:i', $start_time_stamp );
            $meetEnd = ($country=="1") ? date( 'm/d/Y h:i A', $end_time_stamp ) : date( 'd/m/Y H:i', $end_time_stamp );

            if($employee->be_cal_type == "IOS" || $employee->be_cal_type == "Outlook"){

                if($employee->be_cal_type == "Outlook"){
                    $meeting->em_cust_request = str_replace("\n","<br>",$meeting->em_cust_request);
                }

                $addSpace = ($employee->be_cal_type == "Outlook") ? "<br>" : "\n";

                if($customer->cust_first_name != ""){
                    $meeting->em_cust_request = "Client: {$customer->cust_first_name}".$addSpace.$meeting->em_cust_request;
                }

                $meeting->em_cust_request = "Employee: ".$employee->be_name.$addSpace.$meeting->em_cust_request;
            }

            if($employee->be_cal_type == "IOS"){
                $meeting->em_cust_request = str_replace("\r\n", "\n", $meeting->em_cust_request);
                $meeting->em_cust_request = str_replace("\\n", "\n", $meeting->em_cust_request);
                $meeting->em_cust_request = str_replace("\n", "\\n", $meeting->em_cust_request);
            }

            $meeting->em_start_date = $meetingStartDate;
            $meeting->em_start_time = $startHourID;
            $meeting->em_end_date = $meetingEndDate;
            $meeting->em_end_time = $endHourID;

            $meeting->em_color = $service->bmt_color;
            $meeting->em_cust_name = $customer->cust_first_name;

            $meeting->em_start = date('Y-m-d H:i:s',$start_time_stamp);
            $meeting->em_end = date('Y-m-d H:i:s',$end_time_stamp);

            $meetID = $instance->addEmployeeMeetingDB($meeting);

            if($meetID <= 0){
                return resultObject::withData(0,'could not add');
            }

            $meeting->em_id = $meetID;

            $meeting->setCustomer($customer);

            $paymentResult = self::processMeetingPayment($meeting);

            if($paymentResult->code != 1){
                return $paymentResult;
            }

            eventManager::actionTrigger(enumCustomerActions::bookedAppointment,$customer->cust_id, "appointment",'',$meeting->em_cust_device_id,$meeting->em_id);

            $instance->syncMeetingCalendars($meeting);

            $instance->setMeetingsRemindersMobile($meeting->em_biz_id,$meeting->em_cust_id,$meeting->em_name,$start_time_stamp,$meeting->em_id);

            $instance->sendMeetingEmails($meeting);

            $response = new stdClass();
            $response->result["code"] = 0;
            $response->result["meeting_id"] = $meeting->em_id;
            $response->result["localized_no"] = "225";
            $response->result["internal_desc"] = "Meeting is set";


            $appointments = array();
            $appointments[] = adminAppointmentObject::withMeeting($meeting);

            adminManager::sendUpdateToBizDevices($meeting->em_biz_id,"appointments.new",$appointments);

            customerManager::addCustomerLoyaltyStampToCustomerFromMeeting($customer,$meeting->em_meet_type,$meeting->em_id);

            return resultObject::withData(1,'',$response);
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            $data['start'] = $start_time_stamp;
            $data['end'] = $end_time_stamp;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function verifyMeetingTimeAvailable(employeeMeetingObject $meeting){
        try{

            $instance = new self();
            $meetingStartDate = date("Y-m-d",$meeting->em_start_time_stamp);
            $meetingEndDate = date("Y-m-d",$meeting->em_end_time_stamp);
            $meetingStartHour = date("H:i",$meeting->em_start_time_stamp);
            $meetingEndHour = date("H:i",$meeting->em_end_time_stamp);

            $startHourID = $instance->getHourIDFromStartHour($meetingStartHour);
            $endHourID = $instance->getHourIDFromEndHour($meetingEndHour);

            $isSlotAvailable = $instance->verifyAvailability($meeting->em_emp_id,$meetingStartDate,$meetingEndDate,$startHourID,$endHourID);

            if(!$isSlotAvailable){
                $responce = new stdClass();
                $responce->result["code"] = "8";
                $responce->result["meeting_id"] = 0;
                $responce->result["localized_no"] = "221";
                $responce->result["internal_desc"] = "Slot is Taken";
                $responce->result["slots"] = self::getMeetingsList($meeting->em_emp_id,$meeting->em_meet_type,$meeting->em_start_time_stamp,0);
                return resultObject::withData(-1,'slot_taken',$responce);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function setMeetingFromAdmin(employeeMeetingObject $meeting){
        try{
            $instance = new self();
            $meetingStartDate = date("Y-m-d",$meeting->em_start_time_stamp);
            $meetingEndDate = date("Y-m-d",$meeting->em_end_time_stamp);
            $meetingStartHour = date("H:i",$meeting->em_start_time_stamp);
            $meetingEndHour = date("H:i",$meeting->em_end_time_stamp);

            $startHourID = $instance->getHourIDFromStartHour($meetingStartHour);
            $endHourID = $instance->getHourIDFromEndHour($meetingEndHour);

            if($meeting->em_recurring_end_date_stamp > 0){
                $meeting->em_recurring_end_date = date("Y-m-d H:i:s",$meeting->em_recurring_end_date_stamp);
            }

            if($meeting->em_emp_id != 0){
                $isSlotAvailable = $instance->verifyAvailability($meeting->em_emp_id,$meetingStartDate,$meetingEndDate,$startHourID,$endHourID);

                if(!$isSlotAvailable){
                    $responce = new stdClass();
                    $responce->result["code"] = "8";
                    $responce->result["meeting_id"] = 0;
                    $responce->result["localized_no"] = "221";
                    $responce->result["internal_desc"] = "Slot is Taken";
                    $responce->result["slots"] = self::getMeetingsList($meeting->em_emp_id,$meeting->em_meet_type,$meeting->em_start_time_stamp,0);
                    return resultObject::withData(-1,'slot_taken',$responce);
                }
            }


            if($meeting->em_meet_type != 0){
                $serviceRow = $instance->loadServiceFromDB($meeting->em_meet_type);
                $service = serviceObject::withData($serviceRow);
                $meeting->em_color = $service->bmt_color;
            }

            if($meeting->em_emp_id != 0){
                $employeeRow = $instance->loadEmployeeFromDB($meeting->em_emp_id);
                $employee = employeeObject::withData($employeeRow);
            }
            else{
                $employee = new employeeObject();
            }

            $meeting->setEmployee($employee);

            if($employee->be_cal_type == "IOS" || $employee->be_cal_type == "Outlook"){

                if($employee->be_cal_type == "Outlook"){
                    $meeting->em_cust_request = str_replace("\n","<br>",$meeting->em_cust_request);
                }

                $addSpace = ($employee->be_cal_type == "Outlook") ? "<br>" : "\n";

                if($meeting->em_cust_id > 0 &&  $meeting->customer->cust_first_name != ""){
                    $meeting->em_cust_request = "Client: {$meeting->customer->cust_first_name}".$addSpace.$meeting->em_cust_request;
                }

                $meeting->em_cust_request = "Employee: ".$employee->be_name.$addSpace.$meeting->em_cust_request;
            }

            if($employee->be_cal_type == "IOS"){
                $meeting->em_cust_request = str_replace("\r\n", "\n", $meeting->em_cust_request);
                $meeting->em_cust_request = str_replace("\\n", "\n", $meeting->em_cust_request);
                $meeting->em_cust_request = str_replace("\n", "\\n", $meeting->em_cust_request);
            }

            $meeting->em_start_date = $meetingStartDate;
            $meeting->em_start_time = $startHourID;
            $meeting->em_end_date = $meetingEndDate;
            $meeting->em_end_time = $endHourID;

            $meeting->em_cust_name = $meeting->em_cust_id > 0 ? $meeting->customer->cust_first_name : "";

            $meeting->em_start = date('Y-m-d H:i:s',$meeting->em_start_time_stamp);
            $meeting->em_end = date('Y-m-d H:i:s',$meeting->em_end_time_stamp);

            $meetID = self::createMeetingInstance($meeting);

            if($meetID <= 0){
                return resultObject::withData(0,'could not add');
            }

            $meeting->em_id = $meetID;

            $paymentCollected = false;
            if($meeting->em_cust_id > 0 && $meeting->em_payment_source != "none"){
                $paymentResult = self::processMeetingPayment($meeting);

                if($paymentResult->code != 1){
                    return $paymentResult;
                }
                $paymentCollected = true;
                eventManager::actionTrigger(enumCustomerActions::bookedAppointment,$meeting->customer->cust_id, "appointment",'',$meeting->em_cust_device_id,$meeting->em_id);
            }

            $instance->sendMeetingEmails($meeting);

            if($meeting->em_is_recurring != "" && $meeting->em_is_recurring != "none"){
                $meeting->em_recurring_group = $meeting->em_id;
                self::createRecurringMeetingInstance($meeting);
            }
            else{
                $appointments = array();
                $appointments[] = adminAppointmentObject::withMeeting($meeting);

                adminManager::sendUpdateToBizDevices($meeting->em_biz_id,"appointments.new",$appointments);
            }

            $response = new stdClass();
            $response->result["code"] = 0;
            $response->result["meeting_id"] = $meeting->em_id;
            $response->result["localized_no"] = "225";
            $response->result["internal_desc"] = "Meeting is set";
            $response->result["paid"] = $paymentCollected ? 1 : 0;

            bizManager::addBizOfferingUsage($meeting->em_biz_id,'appointment',$meeting->em_meet_type);

            return resultObject::withData(1,'',$response);
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            $data['start'] = $meeting->em_start_time_stamp;
            $data['end'] = $meeting->em_end_time_stamp;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function updateMeetingFromAdmin(employeeMeetingObject $meeting,employeeMeetingObject $originalMeeting,$payChangeConfirmed = false){
        try{

            $instance = new self();
            $meetingStartDate = date("Y-m-d",$meeting->em_start_time_stamp);
            $meetingEndDate = date("Y-m-d",$meeting->em_end_time_stamp);
            $meetingStartHour = date("H:i",$meeting->em_start_time_stamp);
            $meetingEndHour = date("H:i",$meeting->em_end_time_stamp);

            $startHourID = $instance->getHourIDFromStartHour($meetingStartHour);
            $endHourID = $instance->getHourIDFromEndHour($meetingEndHour);

            $meeting->em_id = $originalMeeting->em_id;
            $meeting->em_scheduled_on = $originalMeeting->em_scheduled_on;

            $timeChanged = $startHourID != $meeting->em_start_time || $meeting->em_end_time != $endHourID || $meeting->em_start_date != $meetingStartDate || $meeting->em_end_date != $meetingEndDate;

            $employeeChanged = $meeting->em_emp_id != $originalMeeting->em_emp_id;

            $serviceChanged = $meeting->em_meet_type != $originalMeeting->em_meet_type;

            $notesChanged = $originalMeeting->em_cust_request != $meeting->em_cust_request;

            $locationChanged = $originalMeeting->em_location != $meeting->em_location;

            $paymentCollected = $meeting->em_payment_source != $originalMeeting->em_payment_source && $meeting->em_payment_source != "none";


            if($serviceChanged && $originalMeeting->em_payment_source == "order"){
                $originalService = $instance->loadServiceFromDB($originalMeeting->em_meet_type);
                $newService = $instance->loadServiceFromDB($meeting->em_meet_type);

                $orderResult = customerOrderManager::getCustomerOrderByID($originalMeeting->em_payment_source_id);

                if($orderResult->code != 1){
                    return resultObject::withData(0,'original_order_not_found');
                }

                $order = $orderResult->data;

                if(isset($order->cto_amended_on)){
                    return resultObject::withData(0,'order_was_already_amended');
                }

                $customerBilling = new customerBillingManager();
                if($originalService["bmt_price"] > $newService["bmt_price"]){//refund required
                    $amount = $originalService["bmt_price"] - $newService["bmt_price"];
                    if(!$payChangeConfirmed){
                        $data = array();
                        $data["pay_action"] = "refund";
                        $data["amount"] = $amount;
                        $data["pay_method"] = $order->getOrderPaymentMethod();
                        return resultObject::withData(-1,'pay_action_required',$data);
                    }
                    else{
                        $refundResult = $customerBilling->partialRefundCustomerOrder($order,$amount);

                        if($refundResult->code != 1){
                            return $refundResult;
                        }
                    }
                }

                if($originalService["bmt_price"] < $newService["bmt_price"]){//extra charge required
                    if(!$payChangeConfirmed){
                        $data = array();
                        $data["pay_action"] = "charge";
                        $data["amount"] = $newService["bmt_price"] - $originalService["bmt_price"];

                        return resultObject::withData(-1,'pay_action_required',$data);
                    }
                }
            }


            if($paymentCollected){
                $paymentResult = self::processMeetingPayment($meeting);

                if($paymentResult->code != 1){
                    return $paymentResult;
                }
            }

            if($timeChanged || $employeeChanged){
                $isSlotAvailable = $instance->verifyAvailabilityForEditingMeeting($meeting->em_emp_id,$originalMeeting->em_id,$meetingStartDate,$meetingEndDate,$startHourID,$endHourID);

                if(!$isSlotAvailable){
                    $responce = new stdClass();
                    $responce->result["code"] = "8";
                    $responce->result["meeting_id"] = 0;
                    $responce->result["localized_no"] = "221";
                    $responce->result["internal_desc"] = "Slot is Taken";
                    $responce->result["slots"] = self::getMeetingsList($meeting->em_emp_id,$meeting->em_meet_type,$meeting->em_start_time_stamp,0);
                    return resultObject::withData(-1,'slot_taken',$responce);
                }


            }

            if($timeChanged){
                $meeting->em_start_date = $meetingStartDate;
                $meeting->em_start_time = $startHourID;
                $meeting->em_end_date = $meetingEndDate;
                $meeting->em_end_time = $endHourID;

                $meeting->em_start = date('Y-m-d H:i:s',$meeting->em_start_time_stamp);
                $meeting->em_end = date('Y-m-d H:i:s',$meeting->em_end_time_stamp);
            }

            self::updateEmployeeMeeting($meeting);

            if($timeChanged || $employeeChanged || $serviceChanged){
                if($meeting->em_cust_id != 0){
                    $instance->sendCustomerMeetingUpdateEmail($originalMeeting,$meeting);
                }
                if(!$employeeChanged){
                    $instance->sendEmployeeMeetingUpdateEmail($originalMeeting,$meeting);
                }
                else{
                    if($originalMeeting->em_emp_id > 0){
                        $instance->sendEmployeeMeetingCancelledEmail($originalMeeting);
                    }
                    $instance->sendEmployeeMeetingSetEmail($meeting);
                }
            }



            $response = new stdClass();
            $response->result["code"] = 0;
            $response->result["meeting_id"] = $meeting->em_id;
            $response->result["localized_no"] = "225";
            $response->result["internal_desc"] = "Meeting updated";
            $response->result["paid"] = $paymentCollected || $meeting->em_payment_source != "none" ? 1 : 0;

            $appointments = array();
            $appointments[] = adminAppointmentObject::withMeeting($meeting);
            adminManager::sendUpdateToBizDevices($meeting->em_biz_id,"appointments.update",$appointments);

            return resultObject::withData(1,'',$response);
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function signUpCustomerToClassdate(customerObject $customer,classDateCustomerObject $customerDate){
        try{
            $instance = new self();

            $custDateID = $instance->addClassDateCustomerDB($customerDate);

            if($custDateID <= 0){
                return resultObject::withData(0,'add_failed');
            }

            $customerDate->cdc_id = $custDateID;

            self::processCustomerClassDatePayment($customerDate);

            $instance->sendCustomerClassDateEmails($customer,$customerDate);

            $params = array();
            $params['class_date_id'] = $customerDate->cdc_id;
            utilityManager::asyncTask(URL . 'autojobs/asyncUpdateClassAttendees',$params);

            bizManager::addBizOfferingUsage($customerDate->cdc_biz_id,'class',$customerDate->cdc_class_id);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['date'] = $customerDate;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function searchServicesInBiz($bizID,$skip = 0,$take = 5){
        try{
            $instance = new self();
            $serviceRows = $instance->searchServicesInBiz_DB($bizID,$skip,$take);

            $services = array();

            foreach ($serviceRows as $serviceData)
            {
            	$services[] = serviceObject::withData($serviceData);
            }

            return resultObject::withData(1,'',$services);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAdminSchedule($bizID,$from,$to,$filter = array()){
        try{
            $instance = new self();

            $appointmentsRows = $instance->getAdminAppointments($bizID,$from,$to,$filter);

            $result = array();

            foreach ($appointmentsRows as $appointment)
            {
            	$result[] = adminAppointmentObject::withdata($appointment);
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAdminSchedulerList($bizID,$from,$direction = "forward",$skip= 0,$filter = array()){
        try{
            $instance = new self();

            $appointmentsRows = $instance->getAdminAppointmentsList($bizID,$from,$direction,$skip,$filter);

            $result = array();

            foreach ($appointmentsRows as $appointment)
            {
            	$result[] = adminAppointmentObject::withdata($appointment);
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function searchAdminSchedulerList($bizID,$from,$skip,$key = ""){
        try{
            $instance = new self();

            $appointmentsRows = $instance->searchAdminAppointmentsList($bizID,$from,$skip,$key);

            $result = array();

            foreach ($appointmentsRows as $appointment)
            {
            	$result[] = adminAppointmentObject::withdata($appointment);
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function searchBizClassesList($bizID,$key){
        try{
            $instance = new self();

            $classRows = $instance->searchClassesListForBiz($bizID,$key);

            $result = array();

            foreach ($classRows as $classRow)
            {
            	$result[] = classObject::withdata($classRow);
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAdminEmployeesSchedule($bizID,$date,$filter = array()){
        try{
            $instance = new self();

            $appointmentsRows = $instance->getAdminAppointments($bizID,$date,$date,$filter);

            $result = array();

            foreach ($appointmentsRows as $appointment)
            {
                $appointObject = adminAppointmentObject::withdata($appointment);
                if(!isset($result[$appointObject->emp_id])){
                    $result[$appointObject->emp_id] = array();
                }
            	$result[$appointObject->emp_id][] = $appointObject;
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function deleteMeetingFromAdmin(employeeMeetingObject $meeting,$refundAction = "reset_claim"){
        try{

            $instance = new self();
            $calendarSincModel = new calendarSincManager();

            switch($refundAction){
                case "refund":
                    self::refundMeetingOrder($meeting);
                    break;
                case "reset_claim":
                    self::resetClaimForMeeting($meeting);
                    break;
            }

            if($meeting->em_emp_id != 0){
                $employeeRow = $instance->loadEmployeeFromDB($meeting->em_emp_id);
                $employee = employeeObject::withData($employeeRow);
            }
            else{
                $employee = new employeeObject();
            }
            $meeting->setEmployee($employee);

            $client = array();
            if($meeting->employee->be_cal_type == "Google"){
                $client = $calendarSincModel->getClient($meeting->em_emp_id);
                $calendarSincModel->deleteEvent($meeting->employee->be_cal_id,$meeting->em_external_id,$client);
            }

            if($meeting->employee->be_cal_type == "IOS"){
                $calendarSincModel->deleteEventIOS($meeting->employee,$meeting->em_external_id);
            }

            if($meeting->employee->be_cal_type == "Outlook"){
                $calendarSincModel->deleteEventOutlook($meeting->employee->be_id,$meeting->em_external_id);
            }

            $result = self::deleteEmployeeMeetingById($meeting->em_id);

            if($result->code == 1){
                self::deleteMeetingReminders($meeting);

                if($meeting->em_emp_id > 0){
                    $instance->sendEmployeeMeetingCancelledEmail($meeting);
                }

                if($meeting->em_cust_id > 0){
                    $instance->sendCustomerMeetingCancelledEmail($meeting);
                }
            }



            return $result;
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            $data['start'] = $meeting->em_start_time_stamp;
            $data['end'] = $meeting->em_end_time_stamp;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function deleteMeetingFromAdminRecurring(employeeMeetingObject $meeting,$refundAction = "none"){
        try{
            $instance = new self();
            $featuresMeetings = $instance->getMeetingsRecurringDB($meeting->em_id,$meeting->em_recurring_group);
            $appointments = array();
            if(count($featuresMeetings)>0){
                foreach ($featuresMeetings as $featuredMeeting)
                {
                	$featuredMeetingObject = employeeMeetingObject::withData($featuredMeeting);
                    self::deleteMeetingFromAdmin($featuredMeetingObject,$refundAction);
                    $appointments[] = adminAppointmentObject::withMeeting($featuredMeetingObject);
                }

            }

            adminManager::sendUpdateToBizDevices($meeting->em_biz_id,"appointments.delete",$appointments);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            $data = array();
            $data['meeting'] = $meeting;
            $data['start'] = $meeting->em_start_time_stamp;
            $data['end'] = $meeting->em_end_time_stamp;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCalculatedDuration($postData){

        $serviceId = $postData['service'] == "" ? "0" : $postData['service'];

        if($postData['dur_by_serv'] == 0){
            $calculatedDuration = self::getDurationFormat($postData['custom_days'], $postData['custom_hours'],$postData['custom_minutes']);
            $calculatedDuration["duration"] = $calculatedDuration['size'];
        }else{
            $calculatedDuration = array();
            $calculatedDuration['size'] = 0;
            $calculatedDuration['format'] = '0:0:0';
            if($serviceId > 0){
                $durattionResult = self::getDurationFormatForService($serviceId);
                $calculatedDuration["duration"] = $durattionResult['size'];
            }
        }

        return $calculatedDuration;
    }

    public static function getDurationFormat($days,$hours,$mins){
        $result = array();
        $result['size'] = ($days * 12 * 24) + ($hours * 12) + intval($mins / 5);

        $result['format'] = "$days:$hours:$mins";
        return $result;
    }

    public static function getDurationFormatForService($serviceId){

        $instance = new self();
        $serviceRow = $instance->loadServiceFromDB($serviceId);
        $durattionParts = explode(":",$serviceRow["bmt_duration"]);

        $result = array();
        $result['size'] = ($durattionParts[0] * 12 * 24) + ($durattionParts[1] * 12) + intval($durattionParts[2] / 5);

        $result['format'] = "{$durattionParts[0]}:{$durattionParts[1]}:{$durattionParts[2]}";
        return $result;
    }

    private static function createMeetingInstance(employeeMeetingObject $meeting){

        $instance = new self();
        $meetID = $instance->addEmployeeMeetingDB($meeting);

        $meeting->em_id = $meetID;

        $instance->syncMeetingCalendars($meeting);
        $instance->setMeetingsRemindersMobile($meeting->em_biz_id,$meeting->em_cust_id,$meeting->em_name,$meeting->em_start_time_stamp,$meeting->em_id);

        return $meetID;
    }

    private static function createRecurringMeetingInstance(employeeMeetingObject $meeting){

        $instance = new self();

        $startTime = new DateTime($meeting->em_start);
        $endTime = new DateTime($meeting->em_end);

        //Recurring meetings do not have payments yet
        $meeting->em_payment_source_id = 0;
        $meeting->em_payment_source = 'none';
        $meeting->em_payment_source_inner_id = 0;

        switch($meeting->em_is_recurring){
            case "weekly":
                $interval = new DateInterval('P1W');  // one week
                $endOfCalendar = new DateTime(date('Y-m-d', strtotime('+1 year')));
                break;
            case "monthly":
                $interval = new DateInterval('P1M');  // one month
                $endOfCalendar = new DateTime(date('Y-m-d', strtotime('+1 year')));
                break;
            default:
                $interval = new DateInterval('P1Y');  // one year
                $endOfCalendar = new DateTime(date('Y-m-d', strtotime('+5 year')));
                break;
        }

        if($meeting->em_recurring_limit_method == "end_date" && isset($meeting->em_recurring_end_date)){
            $endOfCalendar = new DateTime(date("Y-m-d",strtotime($meeting->em_recurring_end_date)));
        }


        $p = new DatePeriod($startTime, $interval, $endOfCalendar);
        $e = new DatePeriod($endTime, $interval, $endOfCalendar);

        $i = 0;
        $previousTime = $startTime;

        $appointments = array();
        foreach ($p as $w) {

            $et = self::getDateTimeAtIndex($e,$i);
            if ($i == 0){ //For the row we already inserted we only update the recurring data
                $instance->upateEmployeeMeetingDB($meeting);
            }
            else if($meeting->em_recurring_limit_method == "amount" && $i > $meeting->em_recurring_amount){
                break;
            }
            else{

                if($meeting->em_is_recurring == "monthly"){
                    /* IN MONTHS WE HAVE TO FIX THE UNEVEN MONTHS LIKE 28/02/XXXX */
                    $month_before  = (int) $previousTime->format("m") + 12 * (int) $previousTime->format("Y");
                    $month_after  = (int) $w->format("m") + 12 * $w->format("Y");

                    if ($month_after > 1 + $month_before){
                        $newCorrectedDateTime = new DateTime($w->format('Y-m-d H:i:s'));
                        $newCorrectedDateTime->modify('first day of this month');
                        $newCorrectedDateTime->modify('-1 day');
                        $newCorrectEndTime = new DateTime($newCorrectedDateTime->format('Y-m-d H:i:s'));

                        $offset = $w->diff($et);
                        $newCorrectEndTime->add($offset);
                        $w = $newCorrectedDateTime;
                        $et = $newCorrectEndTime;

                    }
                    /* END OF UNEVEN MONTHS */
                }

                $meeting->em_start = $w->format('Y-m-d H:i:s');
                $meeting->em_start_date = $w->format('Y-m-d');
                $meeting->em_end = $et->format('Y-m-d H:i:s');
                $meeting->em_end_date = $et->format('Y-m-d');

                self::createMeetingInstance($meeting);

                $appointments[] = adminAppointmentObject::withMeeting($meeting);
            }
            $i++;
            $previousTime = $w;
        }

        adminManager::sendUpdateToBizDevices($meeting->em_biz_id,"appointments.new",$appointments);
    }

    public static function setClassDate(classObject $class,classDateObject $classDate){
        $instance = new self();

        $conflicts = array();

        $firstResult = $instance->createClassdateInstance($class,$classDate);

        if($firstResult->code != 1){
            return $firstResult;
        }

        $classDateRow = $instance->loadClassDateFromDB($firstResult->data);

        $classDate = classDateObject::withData($classDateRow);

        $appointments = array();
        $appointments[] = adminAppointmentObject::withClassDate($class,$classDate);
        adminManager::sendUpdateToBizDevices($class->calc_biz_id,"appointments.new",$appointments);

        if($classDate->ccd_recurring != "none"){
            $classDate->ccd_recurring_group = $classDate->ccd_id;
            $instance->createRecurringClassDate($class,$classDate);
        }

        return resultObject::withData(1,'',$classDate);
    }

    public static function moveClassDate(classDateObject $classDate){
        $instance = new self();

        $classRow = $instance->loadClassByDateIdFromDB($classDate->ccd_id);

        $class = classObject::withData($classRow);

        if(isset($classRow['ce_employee_id']) && $classRow['ce_employee_id'] > 0){
            $employeeData = $instance->loadEmployeeFromDB($classRow['ce_employee_id']);
            $class->setEmployee(employeeObject::withData($employeeData));
            $meetingRow = self::getEmployeeMeetingsForClassDateAndEmployeeID($classDate->ccd_id,$class->employee->be_id);
            if(isset($meetingRow["em_id"])){
                if($instance->verifyAvailabilityForEditingMeeting($class->employee->be_id,$meetingRow["em_id"],$classDate->ccd_start_date,$classDate->ccd_end_date,$classDate->ccd_start_time,$classDate->ccd_end_time) == 0){
                    return resultObject::withData(0,'employee_slot_taken');
                }
            }
            else{
                if($instance->verifyAvailability($class->employee->be_id,$classDate->ccd_start_date,$classDate->ccd_end_date,$classDate->ccd_start_time,$classDate->ccd_end_time) == 0){
                    return resultObject::withData(0,'employee_slot_taken');
                }
            }
        }

        $instance->upateClassDateDB($classDate);



        if(isset($class->employee)){

            $meetingRow = self::getEmployeeMeetingsForClassDateAndEmployeeID($classDate->ccd_id,$class->employee->be_id);
            if(isset($meetingRow["em_id"])){
                $meeting = employeeMeetingObject::withData($meetingRow);

                $originalMeeting = $meeting;

                $meeting->em_start_time_stamp = strtotime($classDate->ccd_start_date." ".$instance->calculateStartHourFromHourID($classDate->ccd_start_time));
                $meeting->em_end_time_stamp = strtotime($classDate->ccd_end_date." ".$instance->calculateEndHourFromHourID($classDate->ccd_end_time));
                $meeting->em_start_date = $classDate->ccd_start_date;
                $meeting->em_end_date = $classDate->ccd_end_date;
                $meeting->em_start_time = $classDate->ccd_start_time;
                $meeting->em_end_time = $classDate->ccd_end_time;
                $meeting->em_start = $classDate->ccd_start_date." ".$instance->calculateStartHourFromHourID($classDate->ccd_start_time);
                $meeting->em_end = $classDate->ccd_end_date." ".$instance->calculateEndHourFromHourID($classDate->ccd_end_time);

                self::updateMeetingFromAdmin($meeting,$originalMeeting);
            }

            else{
                $meeting = new employeeMeetingObject();

                $meeting->em_emp_id = $class->employee->be_id;
                $meeting->em_meet_type = 0;
                $meeting->em_name = $class->calc_name;
                $meeting->em_start_time_stamp = strtotime($classDate->ccd_start_date." ".$instance->calculateStartHourFromHourID($classDate->ccd_start_time));
                $meeting->em_end_time_stamp = strtotime($classDate->ccd_end_date." ".$instance->calculateEndHourFromHourID($classDate->ccd_end_time));
                $meeting->em_start_date = $classDate->ccd_start_date;
                $meeting->em_end_date = $classDate->ccd_end_date;
                $meeting->em_start_time = $classDate->ccd_start_time;
                $meeting->em_end_time = $classDate->ccd_end_time;
                $meeting->em_start = $classDate->ccd_start_date." ".$instance->calculateStartHourFromHourID($classDate->ccd_start_time);
                $meeting->em_end = $classDate->ccd_end_date." ".$instance->calculateEndHourFromHourID($classDate->ccd_end_time);
                $meeting->em_cust_request = '';
                $meeting->em_location = '';
                $meeting->em_payment_source = 'none';
                $meeting->em_payment_source_id = 0;
                $meeting->em_biz_id = $class->calc_biz_id;
                $meeting->em_from_mobile = 0;
                $meeting->em_cust_id = 0;
                $meeting->em_is_recurring = "none";
                $meeting->em_class_id = $class->calc_id;
                $meeting->em_class_date_id = $classDate->ccd_id;
                $meeting->employee = $class->employee;

                self::createMeetingInstance($meeting);
            }


        }

        $classDate->start_hour = $instance->calculateStartHourFromHourID($classDate->ccd_start_time);
        $classDate->end_hour = $instance->calculateEndHourFromHourID($classDate->ccd_end_time);

        $classDate->ccd_start_unix = isset($classDate->start_hour) ? strtotime( $classDate->ccd_start_date . " " . $classDate->start_hour.":00") : 0;
        $classDate->ccd_end_unix = isset($classDate->end_hour) ? strtotime( $classDate->ccd_start_date . " " . $classDate->end_hour.":00") : 0;

        $appointments = array();
        $appointments[] = adminAppointmentObject::withClassDate($class,$classDate);
        adminManager::sendUpdateToBizDevices($class->calc_biz_id,"appointments.update",$appointments);

        return resultObject::withData(1,'',$classDate);
    }

    private function createRecurringClassDate(classObject $class,classDateObject $classDate){


        $startTime = new DateTime($classDate->ccd_start_date." ".$this->calculateStartHourFromHourID($classDate->start_hour));
        $endTime = new DateTime($classDate->ccd_end_date." ".$this->calculateEndHourFromHourID($classDate->end_hour));



        switch($classDate->ccd_recurring){
            case "weekly":
                $interval = new DateInterval('P1W');  // one week
                $endOfCalendar = new DateTime(date('Y-m-d', strtotime('+1 year')));
                break;
            case "monthly":
                $interval = new DateInterval('P1M');  // one month
                $endOfCalendar = new DateTime(date('Y-m-d', strtotime('+1 year')));
                break;
            default:
                $interval = new DateInterval('P1Y');  // one year
                $endOfCalendar = new DateTime(date('Y-m-d', strtotime('+5 year')));
                break;
        }

        if($classDate->ccd_recurring_limit_method == "end_date" && isset($classDate->ccd_recurring_end_date)){
            $endOfCalendar = new DateTime(date("Y-m-d",$classDate->ccd_recurring_end_date));
        }


        $p = new DatePeriod($startTime, $interval, $endOfCalendar);
        $e = new DatePeriod($endTime, $interval, $endOfCalendar);

        $i = 0;
        $previousTime = $startTime;
        foreach ($p as $w) {

            $et = self::getDateTimeAtIndex($e,$i);
            if ($i == 0){ //For the row we already inserted we only update the recurring data
                $this->upateClassDateDB($classDate);
            }
            else if($classDate->ccd_recurring_limit_method == "amount" && $i > $classDate->ccd_recurring_amount){
                break;
            }
            else{

                if($classDate->ccd_recurring == "monthly"){
                    /* IN MONTHS WE HAVE TO FIX THE UNEVEN MONTHS LIKE 28/02/XXXX */
                    $month_before  = (int) $previousTime->format("m") + 12 * (int) $previousTime->format("Y");
                    $month_after  = (int) $w->format("m") + 12 * $w->format("Y");

                    if ($month_after > 1 + $month_before){
                        $newCorrectedDateTime = new DateTime($w->format('Y-m-d H:i:s'));
                        $newCorrectedDateTime->modify('first day of this month');
                        $newCorrectedDateTime->modify('-1 day');
                        $newCorrectEndTime = new DateTime($newCorrectedDateTime->format('Y-m-d H:i:s'));

                        $offset = $w->diff($et);
                        $newCorrectEndTime->add($offset);
                        $w = $newCorrectedDateTime;
                        $et = $newCorrectEndTime;

                    }
                    /* END OF UNEVEN MONTHS */
                }


                $classDate->ccd_start_date = $w->format('Y-m-d');
                $classDate->ccd_end_date = $et->format('Y-m-d');

                $this->createClassdateInstance($class,$classDate);
            }
            $i++;
            $previousTime = $w;
        }
    }

    private function createClassdateInstance(classObject $class,classDateObject $classDate){

        if(isset($class->employee)){
            if($this->verifyAvailability($class->employee->be_id,$classDate->ccd_start_date,$classDate->ccd_end_date,$classDate->ccd_start_time,$classDate->ccd_end_time) == 0){
                return resultObject::withData(0,'employee_slot_taken');
            }
        }

        $new_date_id = $this->addClassDateDB($classDate);



        if(isset($class->employee)){
            $meeting = new employeeMeetingObject();

            $meeting->em_emp_id = $class->employee->be_id;
            $meeting->em_meet_type = 0;
            $meeting->em_name = $class->calc_name;
            $meeting->em_start_time_stamp = strtotime($classDate->ccd_start_date." ".$this->calculateStartHourFromHourID($classDate->ccd_start_time));
            $meeting->em_end_time_stamp = strtotime($classDate->ccd_end_date." ".$this->calculateEndHourFromHourID($classDate->ccd_end_time));
            $meeting->em_start_date = $classDate->ccd_start_date;
            $meeting->em_end_date = $classDate->ccd_end_date;
            $meeting->em_start_time = $classDate->ccd_start_time;
            $meeting->em_end_time = $classDate->ccd_end_time;
            $meeting->em_start = $classDate->ccd_start_date." ".$this->calculateStartHourFromHourID($classDate->ccd_start_time);
            $meeting->em_end = $classDate->ccd_end_date." ".$this->calculateEndHourFromHourID($classDate->ccd_end_time);
            $meeting->em_cust_request = '';
            $meeting->em_location = '';
            $meeting->em_payment_source = 'none';
            $meeting->em_payment_source_id = 0;
            $meeting->em_biz_id = $class->calc_biz_id;
            $meeting->em_from_mobile = 0;
            $meeting->em_cust_id = 0;
            $meeting->em_is_recurring = "none";
            $meeting->em_class_id = $class->calc_id;
            $meeting->em_class_date_id = $new_date_id;
            $meeting->employee = $class->employee;

            self::createMeetingInstance($meeting);
        }

        return resultObject::withData(1,'',$new_date_id);
    }

    private static function getDateTimeAtIndex($period,$index){
        $i=0;
        foreach ($period as $dt) {
            if ($i==$index){
                return $dt;
            }
            $i++;
        }
    }

    private static function deleteMeetingReminders(employeeMeetingObject $meeting){

        $instance = new self();
        $sql = "DELETE FROM tbl_reminders WHERE rem_entity_type = 'meeting' AND rem_entity_id = {$meeting->em_id} AND rem_biz_id = {$meeting->em_biz_id}";

        $instance->db->execute($sql);
    }

    public static function getHourDataByID($timeId){
        $instance = new self();
        return $instance->getHourDataFromHourID($timeId);
    }

    public static function isClassFutureDate($classDateId){

        $instance = new self();
        $sql = "SELECT ccd_start_date as date,hour_start FROM tbl_calendar_class_dates LEFT JOIN calendar_hours ON ccd_start_time = hour_id WHERE ccd_id = $classDateId";
        $classDate = $instance->db->getRow($sql);

        $dateTime = DateTime::createFromFormat('Y-m-d H:i',$classDate['date'].' '.$classDate['hour_start']);
        $today = date("Y-m-d H:i:s");

        return $dateTime > $today;
    }

    public static function processMeetingPayment(employeeMeetingObject $meeting){
        try{
            if($meeting->em_payment_source == 'order'){

                $orderItemResult = customerOrderManager::getAvailableItemtoConnectMeetingFromOrderByTypeAndServiceID($meeting->em_payment_source_id,$meeting->em_meet_type);
                if($orderItemResult->code == 1){
                    $orderItem = $orderItemResult->data;

                    $meeting->em_payment_source_inner_id = $orderItem->tri_id;

                    self::updateEmployeeMeeting($meeting);

                    $orderItem->tri_item_ext_type = "meeting";

                    customerOrderManager::updateCustomerOrderItem($orderItem);

                    customerManager::setCustomerClaimFlfilledByOrderIDAndItemID($meeting->em_payment_source_id,$orderItem->tri_id,$meeting->em_id);
                }
                customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$meeting->customer->cust_id);
            }

            if($meeting->em_payment_source == "subscription"){
                $usage = new customerSubscriptionUsageObject();
                $usage->csuse_biz_id = $meeting->em_biz_id;
                $usage->csuse_cust_id = $meeting->customer->cust_id;
                $usage->csuse_csu_id = $meeting->em_payment_source_id;
                $usage->csuse_item_type = 'service';
                $usage->csuse_item_id = $meeting->em_id;
                $usage->csuse_source = 'auto';
                $usage->csuse_type = 'usage';
                customerSubscriptionManager::useCustSubscription($usage);
            }

            if($meeting->em_payment_source == "punch_pass"){
                $usage = new customerPunchUsageObject();
                $usage->cpuse_biz_id = $meeting->em_biz_id;
                $usage->cpuse_cust_id = $meeting->customer->cust_id;
                $usage->cpuse_cpp_id = $meeting->em_payment_source_id;
                $usage->cpuse_item_type = 'service';
                $usage->cpuse_item_id = $meeting->em_id;
                $usage->cpuse_source = 'auto';
                $usage->cpuse_type = 'usage';
                customerSubscriptionManager::useCustPunchPass($usage);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($meeting));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function refundMeetingOrder(employeeMeetingObject $meeting){
        try{
            if($meeting->em_payment_source_id == 0){
                return resultObject::withData(1);
            }

            $orderResult = customerOrderManager::getCustomerOrderByID($meeting->em_payment_source_id);

            if($orderResult->code != 1){
                return resultObject::withData(0,'original_order_not_found');
            }

            $order = $orderResult->data;

            $customerBilling = new customerBillingManager();
            $refundResult = $customerBilling->partialRefundCustomerOrder($order);

            return $refundResult;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($meeting));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function resetClaimForMeeting(employeeMeetingObject $meeting){
        try{
            if($meeting->em_cust_id == 0){
                return resultObject::withData(1,'no_customer');
            }

            $claimResult = customerManager::getCustomerClaimBySourceAndFulfillRow("appointment",$meeting->em_id);

            if($claimResult->code == 1){//claim exists
                $claim = $claimResult->data;

                $claim->cc_was_fulfilled = 0;
                $claim->cc_fulfilled_on = null;
                $claim->cc_fulfillment_row = 0;

                customerManager::updateCustomerClaim($claim);
            }
            else{//create claim
                $claim = new customerClaimObject();

                $claim->cc_biz_id = $meeting->em_biz_id;
                $claim->cc_cust_id = $meeting->em_cust_id;
                $claim->cc_item_source = "appointment";
                $claim->cc_item_source_id = $meeting->em_meet_type;
                $claim->cc_item_type = 3;
                $claim->cc_order_id = $meeting->em_payment_source_id;
                $claim->cc_order_item_id = $meeting->em_payment_source_inner_id;

                customerManager::addCustomerClaim($claim);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($meeting));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function refundClassDateOrder(classDateCustomerObject $classDate){
        try{
            if($classDate->cdc_payment_source_id == 0){
                return resultObject::withData(1);
            }

            $orderResult = customerOrderManager::getCustomerOrderByID($classDate->cdc_payment_source_id);

            if($orderResult->code != 1){
                return resultObject::withData(0,'original_order_not_found');
            }

            $order = $orderResult->data;

            $customerBilling = new customerBillingManager();
            $refundResult = $customerBilling->partialRefundCustomerOrder($order);

            return $refundResult;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDate));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function resetClaimForClassDate(classDateCustomerObject $classDate){
        try{
            if($classDate->cdc_customer_id == 0){
                return resultObject::withData(1,'no_customer');
            }

            $claimResult = customerManager::getCustomerClaimBySourceAndFulfillRow("class",$classDate->cdc_id);

            if($claimResult->code == 1){//claim exists
                $claim = $claimResult->data;

                $claim->cc_was_fulfilled = 0;
                $claim->cc_fulfilled_on = null;
                $claim->cc_fulfillment_row = 0;

                customerManager::updateCustomerClaim($claim);
            }
            else{//create claim
                $claim = new customerClaimObject();

                $claim->cc_biz_id = $classDate->cdc_biz_id;
                $claim->cc_cust_id = $classDate->cdc_customer_id;
                $claim->cc_item_source = "class";
                $claim->cc_item_source_id = $classDate->cdc_class_id;
                $claim->cc_item_type = 3;
                $claim->cc_order_id = $classDate->cdc_payment_source_id;
                $claim->cc_order_item_id = $classDate->cdc_payment_source_inner_id;

                customerManager::addCustomerClaim($claim);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDate));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function processCustomerClassDatePayment(classDateCustomerObject $customerDate){
        try{
            if($customerDate->cdc_payment_source == 'order'){

                $orderItemResult = customerOrderManager::getAvailableItemtoConnectClassFromOrderByTypeAndClassID($customerDate->cdc_payment_source_id,$customerDate->cdc_class_id);
                if($orderItemResult->code == 1){
                    $orderItem = $orderItemResult->data;

                    $customerDate->cdc_payment_source_inner_id = $orderItem->tri_id;

                    self::updateClassDateCustomer($customerDate);

                    $orderItem->tri_item_ext_type = "class";

                    customerOrderManager::updateCustomerOrderItem($orderItem);

                    customerManager::setCustomerClaimFlfilledByOrderIDAndItemID($customerDate->cdc_payment_source_id,$orderItem->tri_id,$customerDate->cdc_id);
                }

                customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$customerDate->cdc_customer_id);
            }

            if($customerDate->cdc_payment_source == "subscription"){
                $usage = new customerSubscriptionUsageObject();
                $usage->csuse_biz_id = $customerDate->cdc_biz_id;
                $usage->csuse_cust_id = $customerDate->cdc_customer_id;
                $usage->csuse_csu_id = $customerDate->cdc_payment_source_id;
                $usage->csuse_item_type = 'class';
                $usage->csuse_item_id = $customerDate->cdc_id;
                $usage->csuse_source = 'auto';
                $usage->csuse_type = 'usage';
                customerSubscriptionManager::useCustSubscription($usage);
            }

            if($customerDate->cdc_payment_source == "punch_pass"){
                $usage = new customerPunchUsageObject();
                $usage->cpuse_biz_id = $customerDate->cdc_biz_id;
                $usage->cpuse_cust_id = $customerDate->cdc_customer_id;
                $usage->cpuse_cpp_id = $customerDate->cdc_payment_source_id;
                $usage->cpuse_item_type = 'class';
                $usage->cpuse_item_id = $customerDate->cdc_id;
                $usage->cpuse_source = 'auto';
                $usage->cpuse_type = 'usage';
                customerSubscriptionManager::useCustPunchPass($usage);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerDate));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   ADVANCED USAGE - PRIVATE          */
    /************************************* */


    private function getCustomerClassSignUpsDB(customerObject $customer,$start_date){
        $sql = "select * from tbl_calendar_class_dates,
                    (select * from calendar_hours) startTable,
                    (select * from calendar_hours) endTable,
					tbl_class_date_customers
                    where ccd_class_id=cdc_class_id
					and cdc_class_date_id = ccd_id
					and cdc_customer_id = {$customer->cust_id}
                    and startTable.hour_id = ccd_start_time
                    and endTable.hour_id = ccd_end_time
                    and concat(ccd_start_date,' ',startTable.hour_start) >= STR_TO_DATE('$start_date','%d/%m/%Y')";

        return $this->db->getTable($sql);
    }

    private function syncMeetingCalendars(employeeMeetingObject $meeting){
        $calendarSincModel = new calendarSincManager();

        $employee = $meeting->getEmployee();

        if($employee->be_cal_type == "Google" || $employee->be_cal_type == "IOS" || $employee->be_cal_type == "Outlook"){
            $client = array();
            if($employee->be_cal_type == "Google"){
                $client = $calendarSincModel->getClient($employee->be_id);
            }

            $externalID="";
            $startTime = strtotime( $meeting->em_start);
            $startdate = date( 'Y-m-d\TH:i:s', $startTime );

            $endTime = strtotime(  $meeting->em_end );
            $enddate = date( 'Y-m-d\TH:i:s', $endTime );

            $sincVals = array();
            $sincVals["calendarId"] = $employee->be_cal_id;
            $sincVals["eventName"] = $meeting->em_name;
            $sincVals["notes"] = $meeting->em_cust_request;
            $sincVals["startDate"] = $startdate;
            $sincVals["endDate"] = $enddate;
            $sincVals["client"] = $client;
            $attendees = array();

            if($employee->be_mail != ""){
                $employeeArray = array();
                $employeeArray["id"] = $employee->be_id;
                $employeeArray["email"] = $employee->be_mail;
                $employeeArray["displayName"] = $employee->be_name;
                $employeeArray["organizer"] = true;

                array_push($attendees,$employeeArray);
            }

            if($meeting->em_cust_id != 0){

                $custEmail = isset($meeting->customer) ? $meeting->customer->cust_email : "";
               
                if($custEmail != ""){
                    $employeeArray = array();
                    $employeeArray["id"] = $meeting->em_cust_id;

                    $employeeArray["email"] = $custEmail;

                    $employeeArray["displayName"] = $meeting->em_cust_name;
                    $employeeArray["organizer"] = false;
                    if($custEmail != ""){
                        array_push($attendees,$employeeArray);
                    }
                }
            }

            $sincVals["attendees"] = $attendees;

            if($employee->be_cal_type == "Google"){
                if(count($client) != 0){
                    $externalID = $calendarSincModel->addEventFromArray($sincVals);
                }
            }
            if($employee->be_cal_type == "IOS"){

                $appleUsername = utilityManager::decryptIt($employee->be_apple_user);
                $applePassword = utilityManager::decryptIt($employee->be_apple_pass);

                $externalID = $calendarSincModel->addEventToIOS($appleUsername,$applePassword,$employee->be_cal_id,$meeting->em_id,$meeting->em_name,$meeting->em_cust_request,$startdate, $enddate,$employee->be_event_color);
            }
            if($employee->be_cal_type == "Outlook"){
                $externalID = $calendarSincModel->addEventOutlook($employee->be_id,$meeting->em_name,$meeting->em_cust_request,$startdate,$enddate,$client);
            }

            $this->db->execute("update tbl_employee_meeting SET
                    em_external_id='$externalID'
                    where em_id={$meeting->em_id}");
        }



    }

    private function setMeetingsRemindersMobile($biz_id,$cust_id,$meet_name,$time,$meeting_id){
        $reminders = $this->checkCalendarReminders($biz_id);

        $sql = "SELECT biz_owner_id,biz_time_zone FROM tbl_biz WHERE biz_id = $biz_id";

        $bizRow = $this->db->getRow($sql);

        $owner_id = $bizRow['biz_owner_id'];
        $biz_time = $bizRow['biz_time_zone'];
        if($reminders['biz_cal_def_reminder_hour'] == 1 && $cust_id != 0){
            $sql = "SELECT CASE
                            WHEN ntpo_push_text IS NULL THEN ntp_push_text
                            WHEN ntpo_push_text = '' THEN ntp_push_text
                            ELSE ntpo_push_text
                           END as text
                    FROM tbl_biz_notif_params LEFT JOIN tbl_biz_notif_params_owner ON ntpo_notification_id = ntp_id AND ntpo_owner_id = $owner_id
                    WHERE ntp_id = 14";

            $text = $this->db->getVal($sql);

            $text = str_replace('#meeting_name#',$meet_name,$text);
            $correctTime = 1 + $biz_time;
            $sql = "INSERT INTO tbl_reminders SET
                                rem_biz_id = {$biz_id},
                                pu_cust_id = $cust_id,
                                rem_repeat = 'Once',
                                rem_text = '$text',
                                rem_time = TIME(DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR)),
                                rem_next_send = DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR),
                                rem_entity_type = 'meeting',
                                rem_entity_id = $meeting_id,
                                rem_active = 1";

            $this->db->execute($sql);
        }

        if($reminders['biz_cal_def_reminder_day'] == 1 && $cust_id != 0){
            $meetTime = DateTime::createFromFormat('U',$time);

            $dayAheadTime = new DateTime('+1 day');

            if($meetTime > $dayAheadTime){// check that the meeting is set for more than a day in the future - otherwise don't add day reminder
                $sql = "SELECT CASE
                                WHEN ntpo_push_text IS NULL THEN ntp_push_text
                                WHEN ntpo_push_text = '' THEN ntp_push_text
                                ELSE ntpo_push_text
                               END as text
                        FROM tbl_biz_notif_params LEFT JOIN tbl_biz_notif_params_owner ON ntpo_notification_id = ntp_id AND ntpo_owner_id = $owner_id
                        WHERE ntp_id = 15";

                $text = $this->db->getVal($sql);

                $text = str_replace('#meeting_name#',$meet_name,$text);
                $correctTime = 24 + $biz_time;
                $sql = "INSERT INTO tbl_reminders SET
                                    rem_biz_id = {$biz_id},
                                    pu_cust_id = $cust_id,
                                    rem_repeat = 'Once',
                                    rem_text = '$text',
                                    rem_time = TIME(DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR)),
                                    rem_next_send = DATE_SUB(from_unixtime('$time','%Y-%m-%d %H:%i:%s'),INTERVAL $correctTime HOUR),
                                    rem_entity_type = 'meeting',
                                    rem_entity_id = $meeting_id,
                                    rem_active = 1";

                $this->db->execute($sql);
            }
        }

        return 1;
    }

    /**
     * Check if biz sends reminders before meetings
     * @param mixed $biz_id
     * @return mixed
     */
    private function checkCalendarReminders($biz_id){
        $sql = "SELECT biz_cal_def_reminder_hour,biz_cal_def_reminder_day FROM tbl_biz_cal_settings WHERE bcs_biz_id = $biz_id";

        $result = $this->db->getRow($sql);

        return $result;
    }

    private function sendMeetingEmails(employeeMeetingObject $meeting){
        $extraParams = array();
        $extraParams["cust_name"] = $meeting->em_cust_name;
        $extraParams["meet_type"] = $meeting->em_name;
        $extraParams["meet_start"] = utilityManager::getFormattedTime($meeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($meeting->em_start_time).":00";
        $extraParams["meet_end"] = utilityManager::getFormattedTime($meeting->em_end_date)." ".utilityManager::getEndTimeFromHourID($meeting->em_end_time).":00";
        $extraParams["employee_name"] = $meeting->employee->be_name;

        //Email to Admin
        emailManager::sendSystemMailApp($meeting->em_biz_id,146,enumEmailType::SystemMailSGCare,$extraParams);

        //Email to Employee
        if($meeting->employee->be_mail != ""){
            $extraParams["to_name"] = $meeting->employee->be_name;
            $extraParams["to_email"] = $meeting->employee->be_mail;
            emailManager::sendSystemMailApp($meeting->em_biz_id,210,enumEmailType::SystemMailSGCare,$extraParams);
        }


        if($meeting->em_cust_id > 0){
            if($meeting->customer->cust_email != ""){
                $extraParams["to_name"] = $meeting->customer->cust_first_name;
                $extraParams["to_email"] = $meeting->customer->cust_email;
                //Email to customer
                emailManager::sendSystemMailApp($meeting->em_biz_id,150,enumEmailType::SystemMailSGCare,$extraParams);
            }

            //Push to Customer
            $message = pushManager::getSystemAlertPushMessageText(21,$meeting->em_biz_id,$extraParams);
            pushManager::sendPushBiz($meeting->em_biz_id,$meeting->em_cust_id,enumPushType::biz_personalBooking,$message);
        }

        $notifications = new notificationsManager();
        $params["cust_first_name"] = $meeting->em_cust_name;
        $params["customer_id"] = $meeting->em_cust_id;
        $notifications->addNotification($meeting->em_biz_id,enumActions::newMeeting,false,$params);



        return;
    }

    private function sendCustomerMeetingUpdateEmail(employeeMeetingObject $oldMeeting, employeeMeetingObject $newMeeting){
        $extraParams = array();
        $extraParams["cust_name"] = $oldMeeting->em_cust_name;
        $extraParams["meet_type"] = $newMeeting->em_name;
        $extraParams["meet_start"] = utilityManager::getFormattedTime($newMeeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($newMeeting->em_start_time).":00";
        $extraParams["meet_end"] = utilityManager::getFormattedTime($newMeeting->em_end_date)." ".utilityManager::getEndTimeFromHourID($newMeeting->em_end_time).":00";
        $extraParams["meet_old_start"] =  utilityManager::getFormattedTime($newMeeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($newMeeting->em_start_time).":00";
        $extraParams["meet_old_type"] = $oldMeeting->em_name;
        $extraParams["employee_name"] = $newMeeting->employee->be_name;

        $extraParams["to_name"] = $newMeeting->customer->cust_first_name;
        $extraParams["to_email"] = $newMeeting->customer->cust_email;
        //Email to customer
        emailManager::sendSystemMailApp($newMeeting->em_biz_id,227,enumEmailType::SystemMailSGCare,$extraParams);

        //Push to Customer
        $message = pushManager::getSystemAlertPushMessageText(23,$newMeeting->em_biz_id,$extraParams);
        pushManager::sendPushBiz($newMeeting->em_biz_id,$newMeeting->em_cust_id,enumPushType::biz_personalBooking,$message);
    }

    private function sendCustomerMeetingCancelledEmail(employeeMeetingObject $meeting){
        $extraParams = array();
        $extraParams["cust_name"] = $meeting->em_cust_name;
        $extraParams["meet_type"] = $meeting->em_name;
        $extraParams["meet_start"] = utilityManager::getFormattedTime($meeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($meeting->em_start_time).":00";
        $extraParams["meet_end"] = utilityManager::getFormattedTime($meeting->em_end_date)." ".utilityManager::getEndTimeFromHourID($meeting->em_end_time).":00";
        $extraParams["employee_name"] = $meeting->employee->be_name;

        $extraParams["to_name"] = $meeting->customer->cust_first_name;
        $extraParams["to_email"] = $meeting->customer->cust_email;
        //Email to customer
        emailManager::sendSystemMailApp($meeting->em_biz_id,228,enumEmailType::SystemMailSGCare,$extraParams);

        //Push to Customer
        $message = pushManager::getSystemAlertPushMessageText(25,$meeting->em_biz_id,$extraParams);
        pushManager::sendPushBiz($meeting->em_biz_id,$meeting->em_cust_id,enumPushType::biz_personalBooking,$message);
    }

    private function sendEmployeeMeetingUpdateEmail(employeeMeetingObject $oldMeeting, employeeMeetingObject $newMeeting){
        $extraParams = array();
        $extraParams["cust_name"] = $newMeeting->em_cust_name;
        $extraParams["cust_old_name"] = $newMeeting->em_cust_name;
        $extraParams["meet_type"] = $newMeeting->em_name;
        $extraParams["meet_start"] = utilityManager::getFormattedTime($newMeeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($newMeeting->em_start_time).":00";
        $extraParams["meet_end"] = utilityManager::getFormattedTime($newMeeting->em_end_date)." ".utilityManager::getEndTimeFromHourID($newMeeting->em_end_time).":00";
        $extraParams["meet_old_start"] =  utilityManager::getFormattedTime($oldMeeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($oldMeeting->em_start_time).":00";
        $extraParams["meet_old_type"] = $oldMeeting->em_name;
        $extraParams["employee_name"] = $newMeeting->employee->be_name;

        //Email to Employee
        if($newMeeting->employee->be_mail != ""){
            $extraParams["to_name"] = $newMeeting->employee->be_name;
            $extraParams["to_email"] = $newMeeting->employee->be_mail;
            emailManager::sendSystemMailApp($newMeeting->em_biz_id,235,enumEmailType::SystemMailSGCare,$extraParams);
        }
    }

    private function sendEmployeeMeetingCancelledEmail(employeeMeetingObject $meeting){
        $extraParams = array();
        $extraParams["cust_name"] = $meeting->em_cust_name;
        $extraParams["meet_type"] = $meeting->em_name;
        $extraParams["meet_start"] = utilityManager::getFormattedTime($meeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($meeting->em_start_time).":00";
        $extraParams["employee_name"] = $meeting->employee->be_name;

        //Email to Employee
        if($meeting->employee->be_mail != ""){
            $extraParams["to_name"] = $meeting->employee->be_name;
            $extraParams["to_email"] = $meeting->employee->be_mail;
            emailManager::sendSystemMailApp($meeting->em_biz_id,236,enumEmailType::SystemMailSGCare,$extraParams);
        }
    }

    private function sendEmployeeMeetingSetEmail(employeeMeetingObject $meeting){
        $extraParams = array();
        $extraParams["cust_name"] = $meeting->em_cust_name;
        $extraParams["meet_type"] = $meeting->em_name;
        $extraParams["meet_start"] = utilityManager::getFormattedTime($meeting->em_start_date,false,false)." ".utilityManager::getStartTimeFromHourID($meeting->em_start_time).":00";
        $extraParams["meet_end"] = utilityManager::getFormattedTime($meeting->em_end_date)." ".utilityManager::getEndTimeFromHourID($meeting->em_end_time).":00";
        $extraParams["employee_name"] = $meeting->employee->be_name;

        //Email to Employee
        if($meeting->employee->be_mail != ""){
            $extraParams["to_name"] = $meeting->employee->be_name;
            $extraParams["to_email"] = $meeting->employee->be_mail;
            emailManager::sendSystemMailApp($meeting->em_biz_id,210,enumEmailType::SystemMailSGCare,$extraParams);
        }
    }

    private function sendCustomerClassDateEmails(customerObject $customer,classDateCustomerObject $customerDate){
        $classRow = $this->loadClassFromDB($customerDate->cdc_class_id);
        $class = classObject::withData($classRow);



        $dateRow = $this->loadClassDateFromDB($customerDate->cdc_class_date_id);
        $date = classDateObject::withData($dateRow);

        $custName = $customer->cust_first_name;
        $custEmail = $customer->cust_email;

        if($custEmail != "" && $custEmail != "N/A"){
            $extraParams = array();
            if(isset($classRow['ce_employee_id']) && $classRow['ce_employee_id'] > 0){
                $employeeRow = $this->loadEmployeeFromDB($classRow['ce_employee_id']);

                $employeeName = $employeeRow["be_name"];
                $employeeEmail = $employeeRow["be_mail"];
            }
            else{
                $employeeName = "";
                $employeeEmail = "";
            }
            $extraParams["employee_name"] = $employeeName;

            $hour = $this->getHourDataFromHourID($date->ccd_start_time);

            $extraParams["cust_name"] = stripslashes($custName);
            $extraParams["class_name"] = stripslashes($class->calc_name);
            $extraParams["class_date"] = bizManager::getBizCountryID($class->calc_biz_id) ? date( 'm/d/Y', strtotime(($date->ccd_start_date))) : date( 'd/m/Y', strtotime(($date->ccd_start_date)));
            $extraParams["class_time"] = $hour['hour_start'];
            emailManager::sendSystemMailApp($class->calc_biz_id,230,enumEmailType::SystemMailSGCare,$extraParams);

            if($employeeEmail != ""){
                $extraParams["to_name"] = $employeeName;
                $extraParams["to_email"] = $employeeEmail;
                emailManager::sendSystemMailApp($class->calc_biz_id,229,enumEmailType::SystemMailSGCare,$extraParams);
            }

            $extraParams["to_name"] = $custName;
            $extraParams["to_email"] = $custEmail;
            emailManager::sendSystemMailApp($class->calc_biz_id,229,enumEmailType::SystemMailSGCare,$extraParams);

            $message = pushManager::getSystemAlertPushMessageText(35,$customer->cust_biz_id,$extraParams);
            pushManager::sendPushBiz($customer->cust_biz_id,$customer->cust_id,enumPushType::biz_personalBooking,$message);
        }
    }

    private function getAdminAppointments($bizID,$from,$to,$filter = array()){
        $start_date = date( "d/m/Y", $from);
        $end_date = date( "d/m/Y", $to);

        $meetingsSQL = $this->getAdminMeetingsSQL($bizID,$start_date,$end_date,$filter);
        $classesSQL = $this->getAdminClassesSQL($bizID,$start_date,$end_date,$filter);
        $sql = "SELECT * FROM(
                    $meetingsSQL
                    UNION
                    $classesSQL
                ) t1 order by start_time";

        return $this->db->getTable($sql);
    }

    private function getAdminMeetingsSQL($bizID,$start_date,$end_date,$filter = array()){
        $filterSQL = "";
        foreach ($filter as $key => $value)
        {
        	if($key == "employee"){
                $filterSQL .= " AND em_emp_id IN ($value)";
            }
            if($key == "service"){
                $filterSQL .= " AND em_meet_type IN ($value)";
            }
        }

        $sql = "SELECT em_id as appoint_id,em_meet_type as source_id,em_name label,em_start start_time,em_end end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'meeting' as type,bmt_color as color FROM tbl_employee_meeting
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        LEFT JOIN tbl_biz_cal_meet_type ON bmt_id = em_meet_type
                        WHERE em_biz_id = $bizID
                        AND em_class_id = 0
                        AND ((em_start_date >= STR_TO_DATE('$start_date','%d/%m/%Y') AND em_start_date <= STR_TO_DATE('$end_date','%d/%m/%Y'))
                              OR (em_end_date >= STR_TO_DATE('$start_date','%d/%m/%Y') AND em_end_date <= STR_TO_DATE('$end_date','%d/%m/%Y'))
                              OR (em_start_date < STR_TO_DATE('$start_date','%d/%m/%Y') AND em_end_date > STR_TO_DATE('$end_date','%d/%m/%Y')))
                        $filterSQL";

        return $sql;
    }

    private function getAdminClassesSQL($bizID,$start_date,$end_date,$filter = array()){
        $filterSQL = "";
        foreach ($filter as $key => $value)
        {
        	if($key == "employee"){
                $filterSQL .= " AND em_emp_id IN ($value)";
            }
            if($key == "service"){
                $filterSQL .= " AND calc_service IN ($value)";
            }
            if($key == "class"){
                $filterSQL .= " AND calc_id IN ($value)";
            }
        }

        $sql = "SELECT ccd_id as appoint_id,calc_id as source_id,calc_name as label,concat(ccd_start_date,' ',startTable.hour_start,':00') start_time,concat(ccd_end_date,' ',endTable.hour_start,':00') end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'class' as type,'' as color FROM tbl_calendar_classes,
                        (select * from calendar_hours) startTable,
                        (select * from calendar_hours) endTable,
                        tbl_calendar_class_dates
                        LEFT JOIN tbl_employee_meeting ON em_class_date_id = ccd_id
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        WHERE ccd_class_id=calc_id
					    AND calc_biz_id = $bizID
                        AND startTable.hour_id = ccd_start_time
                        AND endTable.hour_id = ccd_end_time
                        AND ((ccd_start_date >= STR_TO_DATE('$start_date','%d/%m/%Y') AND ccd_start_date <= STR_TO_DATE('$end_date','%d/%m/%Y'))
                              OR (ccd_end_date >= STR_TO_DATE('$start_date','%d/%m/%Y') AND ccd_end_date <= STR_TO_DATE('$end_date','%d/%m/%Y'))
                              OR (ccd_start_date < STR_TO_DATE('$start_date','%d/%m/%Y') AND ccd_end_date > STR_TO_DATE('$end_date','%d/%m/%Y')))
                        $filterSQL";

        return $sql;
    }

    private function getAdminAppointmentsList($bizID,$from,$direction,$skip,$filter = array(),$take = 25){
        $start_date = date( "d/m/Y", $from);

        $meetingsSQL = $this->getAdminMeetingsListSQL($bizID,$start_date,$direction,$skip,$filter);
        $classesSQL = $this->getAdminClassesListSQL($bizID,$start_date,$direction,$skip,$filter);
        $sql = "SELECT * FROM(
                    $meetingsSQL
                    UNION
                    $classesSQL
                ) t1 order by start_time
                LIMIT $skip,$take";

        return $this->db->getTable($sql);
    }

    private function searchAdminAppointmentsList($bizID,$from,$skip,$key,$take = 25){
        $start_date = date( "d/m/Y", $from);

        $meetingsSQL = $this->searchAdminMeetingsListSQL($bizID,$start_date,$skip,$key);
        $classesSQL = $this->searchAdminClassesListSQL($bizID,$start_date,$skip,$key);
        $sql = "$meetingsSQL
                    UNION
                $classesSQL";

        return $this->db->getTable($sql);
    }

    private function getAdminMeetingsListSQL($bizID,$start_date,$direction,$skip,$filter = array(),$take = 25){
        $filterSQL = "";
        foreach ($filter as $key => $value)
        {
        	if($key == "employee"){
                $filterSQL .= " AND em_emp_id = $value";
            }
            if($key == "service"){
                $filterSQL .= " AND em_meet_type = $value";
            }
        }

        $directionSQL = $direction == "forward" ? " AND em_start_date >= STR_TO_DATE('$start_date','%d/%m/%Y')" : " AND em_start_date <= STR_TO_DATE('$start_date','%d/%m/%Y')"; 

        $sql = "SELECT em_id as appoint_id,em_meet_type as source_id,em_name label,em_start start_time,em_end end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'meeting' as type,bmt_color as color FROM tbl_employee_meeting
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        LEFT JOIN tbl_biz_cal_meet_type ON bmt_id = em_meet_type
                        WHERE em_biz_id = $bizID
                        $directionSQL
                        $filterSQL";

        return $sql;
    }

    private function getAdminClassesListSQL($bizID,$start_date,$direction,$skip,$filter = array(),$take = 25){
        $filterSQL = "";
        foreach ($filter as $key => $value)
        {
        	if($key == "employee"){
                $filterSQL .= " AND em_emp_id = $value";
            }
            if($key == "service"){
                $filterSQL .= " AND calc_service = $value";
            }
            if($key == "class"){
                $filterSQL .= " AND calc_id = $value";
            }
        }

        $directionSQL = $direction == "forward" ? " AND ccd_start_date >= STR_TO_DATE('$start_date','%d/%m/%Y')" : " AND ccd_start_date <= STR_TO_DATE('$start_date','%d/%m/%Y')"; 

        $sql = "SELECT ccd_id as appoint_id,calc_id as source_id,calc_name as label,concat(ccd_start_date,' ',startTable.hour_start,':00') start_time,concat(ccd_end_date,' ',endTable.hour_start,':00') end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'class' as type,'' as color FROM tbl_calendar_classes,
                        (select * from calendar_hours) startTable,
                        (select * from calendar_hours) endTable,
                        tbl_calendar_class_dates
                        LEFT JOIN tbl_employee_meeting ON em_class_date_id = ccd_id
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        WHERE ccd_class_id=calc_id
					    AND calc_biz_id = $bizID
                        AND startTable.hour_id = ccd_start_time
                        AND endTable.hour_id = ccd_end_time
                        $directionSQL
                        $filterSQL";

        return $sql;
    }

    private function searchAdminMeetingsListSQL($bizID,$start_date,$skip,$key,$take = 25){


        $sql = "SELECT * FROM (SELECT em_id as appoint_id,em_meet_type as source_id,em_name label,em_start start_time,em_end end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'meeting' as type,bmt_color as color,cust_id FROM tbl_employee_meeting
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        LEFT JOIN tbl_biz_cal_meet_type ON bmt_id = em_meet_type
                        LEFT JOIN tbl_customers ON em_cust_id = cust_id
                        WHERE em_biz_id = $bizID
                        AND em_start_date >= STR_TO_DATE('$start_date','%d/%m/%Y')
                        AND (LOWER(be_name) LIKE LOWER('%$key%')
                            OR LOWER(be_mail) LIKE LOWER('%$key%')
                            OR LOWER(em_name) LIKE LOWER('%$key%')
                            OR LOWER(cust_email) LIKE LOWER('%$key%')
                            OR cust_phone1 LIKE '%$key%'
                            OR LOWER(cust_first_name) LIKE LOWER('%$key%')
                            OR LOWER(bmt_name) LIKE LOWER('%$key%'))
                        ORDER BY em_start_date ASC
                        LIMIT 0,20) forward
                    UNION
                    SELECT * FROM (SELECT em_id as appoint_id,em_meet_type as source_id,em_name label,em_start start_time,em_end end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'meeting' as type,bmt_color as color,cust_id FROM tbl_employee_meeting
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        LEFT JOIN tbl_biz_cal_meet_type ON bmt_id = em_meet_type
                        LEFT JOIN tbl_customers ON em_cust_id = cust_id
                        WHERE em_biz_id = $bizID
                        AND em_start_date < STR_TO_DATE('$start_date','%d/%m/%Y')
                        AND (LOWER(be_name) LIKE LOWER('%$key%')
                            OR LOWER(be_mail) LIKE LOWER('%$key%')
                            OR LOWER(em_name) LIKE LOWER('%$key%')
                            OR LOWER(cust_email) LIKE LOWER('%$key%')
                            OR cust_phone1 LIKE '%$key%'
                            OR LOWER(cust_first_name) LIKE LOWER('%$key%')
                            OR LOWER(bmt_name) LIKE LOWER('%$key%'))
                        ORDER BY em_start_date ASC
                        LIMIT 0,20) backward";

        return $sql;
    }

    private function searchAdminClassesListSQL($bizID,$start_date,$skip,$key,$take = 25){


        $sql = "SELECT * FROM (SELECT ccd_id as appoint_id,calc_id as source_id,calc_name as label,concat(ccd_start_date,' ',startTable.hour_start,':00') start_time,concat(ccd_end_date,' ',endTable.hour_start,':00') end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'class' as type,'' as color,cust_id FROM tbl_calendar_classes,
                        (select * from calendar_hours) startTable,
                        (select * from calendar_hours) endTable,
                        tbl_calendar_class_dates
                        LEFT JOIN tbl_employee_meeting ON em_class_date_id = ccd_id
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        LEFT JOIN tbl_class_date_customers ON cdc_class_date_id = ccd_id
                        LEFT JOIN tbl_customers ON cdc_customer_id = cust_id
                        WHERE ccd_class_id=calc_id
					    AND calc_biz_id = $bizID
                        AND startTable.hour_id = ccd_start_time
                        AND endTable.hour_id = ccd_end_time
                        AND ccd_start_date >= STR_TO_DATE('$start_date','%d/%m/%Y')
                        AND (LOWER(cust_email) LIKE LOWER('%$key%')
                            OR cust_phone1 LIKE '%$key%'
                            OR LOWER(cust_first_name) LIKE LOWER('%$key%'))
                        ORDER BY ccd_start_date ASC
                        LIMIT 0,20) class_forward
                        UNION
                        SELECT * FROM (SELECT ccd_id as appoint_id,calc_id as source_id,calc_name as label,concat(ccd_start_date,' ',startTable.hour_start,':00') start_time,concat(ccd_end_date,' ',endTable.hour_start,':00') end_time,IFNULL(be_id,0) as emp_id,IFNULL(be_name,'') as emp_name,IFNULL(be_pic,'') as emp_img,'class' as type,'' as color,cust_id FROM tbl_calendar_classes,
                        (select * from calendar_hours) startTable,
                        (select * from calendar_hours) endTable,
                        tbl_calendar_class_dates
                        LEFT JOIN tbl_employee_meeting ON em_class_date_id = ccd_id
                        LEFT JOIN tbl_employee ON be_id = em_emp_id
                        LEFT JOIN tbl_class_date_customers ON cdc_class_date_id = ccd_id
                        LEFT JOIN tbl_customers ON cdc_customer_id = cust_id
                        WHERE ccd_class_id=calc_id
					    AND calc_biz_id = $bizID
                        AND startTable.hour_id = ccd_start_time
                        AND endTable.hour_id = ccd_end_time
                        AND ccd_start_date < STR_TO_DATE('$start_date','%d/%m/%Y')
                        AND (LOWER(cust_email) LIKE LOWER('%$key%')
                            OR cust_phone1 LIKE '%$key%'
                            OR LOWER(cust_first_name) LIKE LOWER('%$key%'))
                        ORDER BY ccd_start_date ASC
                        LIMIT 0,20) class_back";

        return $sql;
    }

    private function searchClassesListForBiz($bizID,$key){
        return $this->db->getTable("SELECT * FROM tbl_calendar_classes
                        LEFT JOIN tbl_calendar_class_employee ON ce_class_id = calc_id
                        LEFT JOIN tbl_employee ON be_id = ce_employee_id
                        WHERE calc_biz_id = $bizID
                        AND (LOWER(be_name) LIKE LOWER('%$key%')
                            OR LOWER(be_mail) LIKE LOWER('%$key%')
                            OR LOWER(calc_name) LIKE LOWER('%$key%')
                        )
                        ");
    }


    /************************************* */
    /*   BASIC SERVICE - PUBLIC           */
    /************************************* */

    /**
     * Insert new serviceObject to DB
     * Return Data = new serviceObject ID
     * @param serviceObject $serviceObj
     * @return resultObject
     */
    public static function addService(serviceObject $serviceObj){
        try{
            $instance = new self();
            $newId = $instance->addServiceDB($serviceObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($serviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get service from DB for provided ID
     * * Return Data = serviceObject
     * @param int $serviceId
     * @return resultObject
     */
    public static function getServiceByID($serviceId){

        try {
            $instance = new self();
            $serviceData = $instance->loadServiceFromDB($serviceId);

            $serviceObj = serviceObject::withData($serviceData);
            $result = resultObject::withData(1,'',$serviceObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$serviceId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update service in DB
     * @param serviceObject $serviceObj
     * @return resultObject
     */
    public static function updateService(serviceObject $serviceObj){
        try{
            $instance = new self();
            $instance->upateServiceDB($serviceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($serviceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete service from DB
     * @param int $serviceID
     * @return resultObject
     */
    public static function deleteServiceById($serviceID){
        try{
            $instance = new self();
            $instance->deleteServiceByIdDB($serviceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$serviceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC SERVICE - DB METHODS           */
    /************************************* */

    private function addServiceDB(serviceObject $obj){

        if (!isset($obj)){
            throw new Exception("serviceObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_cal_meet_type SET
                                        bmt_biz_id = {$obj->bmt_biz_id},
                                        bmt_name = '".addslashes($obj->bmt_name)."',
                                        bmt_desc = '".addslashes($obj->bmt_desc)."',
                                        bmt_size = {$obj->bmt_size},
                                        bmt_duration = '".addslashes($obj->bmt_duration)."',
                                        bmt_active = {$obj->bmt_active},
                                        bmt_mobile_active = {$obj->bmt_mobile_active},
                                        bmt_capacity = {$obj->bmt_capacity},
                                        bmt_color = '".addslashes($obj->bmt_color)."',
                                        bmt_price = {$obj->bmt_price},
                                        bmt_pic = '".addslashes($obj->bmt_pic)."'
                                                 ");
        return $newId;
    }

    private function loadServiceFromDB($serviceID){

        if (!is_numeric($serviceID) || $serviceID <= 0){
            throw new Exception("Illegal value serviceID");
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_cal_meet_type WHERE bmt_id = $serviceID");
    }

    private function upateServiceDB(serviceObject $obj){

        if (!isset($obj->bmt_id) || !is_numeric($obj->bmt_id) || $obj->bmt_id <= 0){
            throw new Exception("serviceObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_biz_cal_meet_type SET
                                bmt_biz_id = {$obj->bmt_biz_id},
                                bmt_name = '".addslashes($obj->bmt_name)."',
                                bmt_desc = '".addslashes($obj->bmt_desc)."',
                                bmt_size = {$obj->bmt_size},
                                bmt_duration = '".addslashes($obj->bmt_duration)."',
                                bmt_active = {$obj->bmt_active},
                                bmt_mobile_active = {$obj->bmt_mobile_active},
                                bmt_capacity = {$obj->bmt_capacity},
                                bmt_color = '".addslashes($obj->bmt_color)."',
                                bmt_price = {$obj->bmt_price},
                                bmt_pic = '".addslashes($obj->bmt_pic)."'
                                WHERE bmt_id = {$obj->bmt_id}
                                         ");
    }

    private function deleteServiceByIdDB($serviceID){

        if (!is_numeric($serviceID) || $serviceID <= 0){
            throw new Exception("Illegal value serviceID");
        }

        $this->db->execute("DELETE FROM tbl_biz_cal_meet_type WHERE bmt_id = $serviceID");
    }

    private function getServicesForEmployee(employeeObject $employee){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type,tbl_employee_meet_type
                                WHERE bmt_biz_id = {$employee->be_biz_id}
                                AND emt_bmt_id = bmt_id
                                AND emt_emp_id = {$employee->be_id}
                                AND bmt_active = 1
                                AND bmt_mobile_active = 1
                                AND emt_active = 1
                                ORDER BY bmt_id ASC";

        return $this->db->getTable($sql);
    }

    private function getServicesListForBizIDDB($bizID){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type
                WHERE bmt_biz_id = $bizID
                AND bmt_active = 1
                AND bmt_mobile_active = 1 ";

        return $this->db->getTable($sql);
    }

    private function getAvailableServicesListForBizIDDB($bizID){
        $sql = "SELECT * FROM tbl_employee_meet_type,tbl_employee,tbl_biz_cal_meet_type
                WHERE bmt_biz_id = $bizID
                AND be_id=emt_emp_id
                AND emt_bmt_id = bmt_id
                AND be_active = 1
                AND emt_active = 1
                AND bmt_active = 1
                AND bmt_mobile_active = 1
                GROUP BY bmt_id";

        return $this->db->getTable($sql);
    }

    private function searchServicesInBiz_DB($bizID,$skip = 0,$take = 5){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type
                WHERE bmt_biz_id = $bizID
                AND bmt_active = 1
                AND bmt_mobile_active = 1
                AND ";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CLASS - PUBLIC           */
    /************************************* */

    /**
     * Insert new classObject to DB
     * Return Data = new classObject ID
     * @param classObject $classObj
     * @return resultObject
     */
    public static function addClass(classObject $classObj){
        try{
            $instance = new self();
            $newId = $instance->addClassDB($classObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get class from DB for provided ID
     * * Return Data = classObject
     * @param int $classId
     * @return resultObject
     */
    public static function getClassByID($classId){

        try {
            $instance = new self();
            $classData = $instance->loadClassFromDB($classId);

            $classObj = classObject::withData($classData);

            if(isset($classData['ce_employee_id']) && $classData['ce_employee_id'] > 0){
                $employeeData = $instance->loadEmployeeFromDB($classData['ce_employee_id']);
                $classObj->setEmployee(employeeObject::withData($employeeData));
            }

            if($classObj->calc_service > 0){
                $serviceData = $instance->loadServiceFromDB($classObj->calc_service);
                $classObj->setService(serviceObject::withData($serviceData));
            }

            $result = resultObject::withData(1,'',$classObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getClassByClassDateID($classDateId){

        try {
            $instance = new self();
            $classData = $instance->loadClassByDateIdFromDB($classDateId);

            $classObj = classObject::withData($classData);

            if(isset($classData['ce_employee_id']) && $classData['ce_employee_id'] > 0){
                $employeeData = $instance->loadEmployeeFromDB($classData['ce_employee_id']);
                $classObj->setEmployee(employeeObject::withData($employeeData));
            }

            if($classObj->calc_service > 0){
                $serviceData = $instance->loadServiceFromDB($classObj->calc_service);
                $classObj->setService(serviceObject::withData($serviceData));
            }

            $result = resultObject::withData(1,'',$classObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update class in DB
     * @param classObject $classObj
     * @return resultObject
     */
    public static function updateClass(classObject $classObj){
        try{
            $instance = new self();
            $instance->upateClassDB($classObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete class from DB
     * @param int $classID
     * @return resultObject
     */
    public static function deleteClassById($classID){
        try{
            $instance = new self();
            $instance->deleteClassByIdDB($classID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function connectEmployeeToClass($bizID,$classid,$employeeid){

        $instance = new self();

        $sql = "INSERT INTO tbl_calendar_class_employee
                    SET ce_biz_id = $bizID,
                    ce_class_id = $classid,
                    ce_employee_id = $employeeid ";
        $instance->db->execute($sql);

    }

    /************************************* */
    /*   BASIC CLASS - DB METHODS           */
    /************************************* */

    private function addClassDB(classObject $obj){

        if (!isset($obj)){
            throw new Exception("classObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_calendar_classes SET
                                        calc_biz_id = {$obj->calc_biz_id},
                                        calc_name = '".addslashes($obj->calc_name)."',
                                        calc_desc = '".addslashes($obj->calc_desc)."',
                                        calc_max_cust = {$obj->calc_max_cust},
                                        calc_min_cust = {$obj->calc_min_cust},
                                        calc_duration_by_service = {$obj->calc_duration_by_service},
                                        calc_service = {$obj->calc_service},
                                        calc_price = {$obj->calc_price},
                                        calc_duration = '".addslashes($obj->calc_duration)."',
                                        calc_duration_size = {$obj->calc_duration_size},
                                        calc_isvisible = {$obj->calc_isvisible},
                                        calc_isdeleted = {$obj->calc_isdeleted}
                                                 ");
        return $newId;
    }

    private function loadClassFromDB($classID){

        if (!is_numeric($classID) || $classID <= 0){
            throw new Exception("Illegal value classID");
        }

        return $this->db->getRow("SELECT * FROM tbl_calendar_classes
                                    LEFT JOIN tbl_calendar_class_employee ON calc_id = ce_class_id
                                    WHERE calc_id = $classID");
    }

    private function loadClassByDateIdFromDB($classDateID){

        if (!is_numeric($classDateID) || $classDateID <= 0){
            throw new Exception("Illegal value classDateID");
        }

        $classID = $this->db->getVal("SELECT ccd_class_id FROM tbl_calendar_class_dates WHERE ccd_id=$classDateID");

        return $this->db->getRow("SELECT * FROM tbl_calendar_classes
                                    LEFT JOIN tbl_calendar_class_employee ON calc_id = ce_class_id
                                    WHERE calc_id = $classID");
    }

    private function upateClassDB(classObject $obj){

        if (!isset($obj->calc_id) || !is_numeric($obj->calc_id) || $obj->calc_id <= 0){
            throw new Exception("classObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_calendar_classes SET
                                calc_biz_id = {$obj->calc_biz_id},
                                calc_name = '".addslashes($obj->calc_name)."',
                                calc_desc = '".addslashes($obj->calc_desc)."',
                                calc_max_cust = {$obj->calc_max_cust},
                                calc_min_cust = {$obj->calc_min_cust},
                                calc_duration_by_service = {$obj->calc_duration_by_service},
                                calc_service = {$obj->calc_service},
                                calc_price = {$obj->calc_price},
                                calc_duration = '".addslashes($obj->calc_duration)."',
                                calc_duration_size = {$obj->calc_duration_size},
                                calc_isvisible = {$obj->calc_isvisible},
                                calc_isdeleted = {$obj->calc_isdeleted}
                                WHERE calc_id = {$obj->calc_id}
                                         ");
    }

    private function deleteClassByIdDB($classID){

        if (!is_numeric($classID) || $classID <= 0){
            throw new Exception("Illegal value classID");
        }

        $this->db->execute("DELETE FROM tbl_calendar_classes WHERE calc_id = $classID");
    }

    private function getClassesForBiz_DB($biz_id){
        if (!is_numeric($biz_id) || $biz_id <= 0){
            throw new Exception("Illegal value bizID");
        }

        $sql = "select *
                from tbl_calendar_class_employee,tbl_employee,
                (SELECT ccd_class_id,min(concat(ccd_start_date,' ',hour_start)) as myDate
                FROM tbl_calendar_class_dates,calendar_hours
                where ccd_start_time=hour_id
                and concat(ccd_start_date,' ',hour_start) > now()
                and ccd_biz_id=$biz_id
                group by ccd_class_id) t1,tbl_calendar_classes
                LEFT JOIN tbl_biz_cal_meet_type ON calc_service = bmt_id
                WHERE ccd_class_id=calc_id
                and calc_id=ce_class_id
                and be_id=ce_employee_id
                and calc_isvisible = 1
                and calc_isdeleted = 0";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CLASSDATE - PUBLIC          */
    /************************************* */

    /**
     * Insert new classDateObject to DB
     * Return Data = new classDateObject ID
     * @param classDateObject $classDateObj
     * @return resultObject
     */
    public static function addClassDate(classDateObject $classDateObj){
        try{
            $instance = new self();
            $newId = $instance->addClassDateDB($classDateObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get classDate from DB for provided ID
     * * Return Data = classDateObject
     * @param int $classDateId
     * @return resultObject
     */
    public static function getClassDateByID($classDateId){

        try {
            $instance = new self();

            $classDateData = $instance->loadClassDateFromDB($classDateId);

            $classDateObj = classDateObject::withData($classDateData);
            $result = resultObject::withData(1,'',$classDateObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update classDate in DB
     * @param classDateObject $classDateObj
     * @return resultObject
     */
    public static function updateClassDate(classDateObject $classDateObj){
        try{
            $instance = new self();
            $instance->upateClassDateDB($classDateObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete classDate from DB
     * @param int $classDateID
     * @return resultObject
     */
    public static function deleteClassDateById($classDateID){
        try{
            $instance = new self();

            $instance->deleteClassDateByIdDB($classDateID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getClassDateBookedCount($classDateID){
        $instance = new self();

        return $instance->getClassDateBookedCountDB($classDateID);
    }

    public static function isCustomerInClassDate($cust_id,classDateObject $classDate){
        $instance = new self();
        return $instance->isCustomerInClassDateDB($cust_id,$classDate);
    }

    public static function getClassDates($classID,$skip = 0,$take = 10){
        try{
            $instance = new self();
            $datesRows = $instance->getClassDates_DB($classID,$skip,$take);

            $dates = array();

            if(count($datesRows) > 0){
                foreach ($datesRows as $oneDate)
                {
                    $classDateObject = classDateObject::withData($oneDate);
                    $dateCustomersResult = bookingManager::getAllCustomersForClassDate($classDateObject->ccd_id);
                    $classDateObject->setCustomers($dateCustomersResult->data);

                    $dates[] = $classDateObject;
                }
            }

            return resultObject::withData(1,'',$dates);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getStartHourID($startHour){

        $instance = new self();
        return $instance->getHourIDFromStartHour($startHour);
    }

    public static function getEndDateForClassDate($startDate,$startTimeId,$duration_size){

        $endID = (intval($startTimeId) + $duration_size - 1) % 288;
        if($endID == 0){
            $endID = 288;
        }

        $durationDays = intval($duration_size / 288);
        $endDate = $startDate;
        if($durationDays > 0){
            $endDate = date('Y-m-d',strtotime("+".$durationDays." days",strtotime($startDate)));
        }
        else{
            if($endID <= $startTimeId){
                $endDate = date('Y-m-d',strtotime("+1 days",strtotime($startDate)));
            }
        }

        return $endDate;
    }

    public static function getEndTimeIdForClassDate($startTimeId,$duration_size){

        $endID = (intval($startTimeId) + $duration_size - 1) % 288;
        if($endID == 0){
            $endID = 288;
        }

        return $endID;
    }

    /************************************* */
    /*   BASIC CLASSDATE - DB METHODS           */
    /************************************* */

    private function addClassDateDB(classDateObject $obj){

        if (!isset($obj)){
            throw new Exception("classDateObject value must be provided");
        }

        $ccd_start_dateDate = isset($obj->ccd_start_date) ? "'".$obj->ccd_start_date."'" : "null";

        $ccd_end_dateDate = isset($obj->ccd_end_date) ? "'".$obj->ccd_end_date."'" : "null";

        $ccd_recurring_end_dateDate = isset($obj->ccd_recurring_end_date) ? "'".$obj->ccd_recurring_end_date."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_calendar_class_dates SET
                                        ccd_biz_id = {$obj->ccd_biz_id},
                                        ccd_class_id = {$obj->ccd_class_id},
                                        ccd_start_date = $ccd_start_dateDate,
                                        ccd_start_time = {$obj->ccd_start_time},
                                        ccd_end_date = $ccd_end_dateDate,
                                        ccd_end_time = {$obj->ccd_end_time},
                                        ccd_recurring = '{$obj->ccd_recurring}',
                                        ccd_recurring_limit_method = '{$obj->ccd_recurring_limit_method}',
                                        ccd_recurring_end_date = $ccd_recurring_end_dateDate,
                                        ccd_recurring_amount = {$obj->ccd_recurring_amount},
                                        ccd_recurring_group = {$obj->ccd_recurring_group},
                                        ccd_iscanceled = {$obj->ccd_iscanceled},
                                        ccd_isvisible = {$obj->ccd_isvisible}
                                                 ");
        return $newId;
    }

    private function loadClassDateFromDB($classDateID){

        if (!is_numeric($classDateID) || $classDateID <= 0){
            throw new Exception("Illegal value classDateID");
        }

        return $this->db->getRow("SELECT tbl_calendar_class_dates.*,
                                         startTable.hour_start as start_hour,
                                         endTable.hour_end as end_hour
                                    FROM tbl_calendar_class_dates,
                                    (select * from calendar_hours) startTable,
                                    (select * from calendar_hours) endTable
                                    WHERE ccd_id = $classDateID
                                    AND startTable.hour_id = ccd_start_time
                                    AND endTable.hour_id = ccd_end_time");
    }

    private function upateClassDateDB(classDateObject $obj){

        if (!isset($obj->ccd_id) || !is_numeric($obj->ccd_id) || $obj->ccd_id <= 0){
            throw new Exception("classDateObject value must be provided");
        }

        $ccd_start_dateDate = isset($obj->ccd_start_date) ? "'".$obj->ccd_start_date."'" : "null";

        $ccd_end_dateDate = isset($obj->ccd_end_date) ? "'".$obj->ccd_end_date."'" : "null";

        $ccd_recurring_end_dateDate = isset($obj->ccd_recurring_end_date) ? "'".$obj->ccd_recurring_end_date."'" : "null";

        $this->db->execute("UPDATE tbl_calendar_class_dates SET
                                ccd_biz_id = {$obj->ccd_biz_id},
                                ccd_class_id = {$obj->ccd_class_id},
                                ccd_start_date = $ccd_start_dateDate,
                                ccd_start_time = {$obj->ccd_start_time},
                                ccd_end_date = $ccd_end_dateDate,
                                ccd_end_time = {$obj->ccd_end_time},
                                ccd_recurring = '{$obj->ccd_recurring}',
                                ccd_recurring_limit_method = '{$obj->ccd_recurring_limit_method}',
                                ccd_recurring_end_date = $ccd_recurring_end_dateDate,
                                ccd_recurring_amount = {$obj->ccd_recurring_amount},
                                ccd_recurring_group = {$obj->ccd_recurring_group},
                                ccd_iscanceled = {$obj->ccd_iscanceled},
                                ccd_isvisible = {$obj->ccd_isvisible}
                                WHERE ccd_id = {$obj->ccd_id}
                                         ");
    }

    private function deleteClassDateByIdDB($classDateID){

        if (!is_numeric($classDateID) || $classDateID <= 0){
            throw new Exception("Illegal value classDateID");
        }

        $this->db->execute("DELETE FROM tbl_calendar_class_dates WHERE ccd_id = $classDateID");
    }

    private function getClassDatesForClassByClassIDDB($classID){
        $sql = "select tbl_calendar_class_dates.*, startTable.hour_start start_hour,endTable.hour_end end_hour
                                        from tbl_calendar_class_dates,
                                        (select * from calendar_hours) startTable,
                                        (select * from calendar_hours) endTable
                                        where ccd_class_id=$classID
                                        and startTable.hour_id = ccd_start_time
                                        and endTable.hour_id = ccd_end_time
                                        and concat(ccd_start_date,' ',startTable.hour_start) > now()
                                        and ccd_iscanceled = 0
                                        and ccd_isvisible = 1
                                        order by ccd_start_date,ccd_start_time";

        return $this->db->getTable($sql);
    }

    private function getClassDateBookedCountDB($classDateID){
        $sql = "select count(cdc_id) from tbl_class_date_customers where cdc_class_date_id = $classDateID";

        return $this->db->getVal($sql);
    }

    private function isCustomerInClassDateDB($custID,classDateObject $classDate){
        $sql = "SELECT count(cdc_id) FROM tbl_class_date_customers
                WHERE cdc_biz_id={$classDate->ccd_biz_id}
                AND cdc_class_id={$classDate->ccd_class_id}
                AND cdc_class_date_id={$classDate->ccd_id}
                AND cdc_customer_id=$custID";

        return $this->db->getVal($sql) > 0;
    }

    private function getClassDates_DB($classID,$skip = 0,$take = 10){

        if (!is_numeric($classID) || $classID <= 0){
            throw new Exception("Illegal value classID");
        }

        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        return $this->db->getTable("SELECT tbl_calendar_class_dates.*,
                                         startTable.hour_start as start_hour,
                                         endTable.hour_end as end_hour
                                    FROM tbl_calendar_class_dates,
                                    (select * from calendar_hours) startTable,
                                    (select * from calendar_hours) endTable
                                    WHERE ccd_class_id = $classID
                                    AND startTable.hour_id = ccd_start_time
                                    AND endTable.hour_id = ccd_end_time
                                    ORDER BY ccd_start_date DESC
                                    $limit");

    }

    /************************************* */
    /*   BASIC CLASSDATECUSTOMER - PUBLIC           */
    /************************************* */

    /**
     * Insert new classDateCustomerObject to DB
     * Return Data = new classDateCustomerObject ID
     * @param classDateCustomerObject $classDateCustomerObj
     * @return resultObject
     */
    public static function addClassDateCustomer(classDateCustomerObject $classDateCustomerObj){
        try{
            $instance = new self();
            $newId = $instance->addClassDateCustomerDB($classDateCustomerObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateCustomerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get classDateCustomer from DB for provided ID
     * * Return Data = classDateCustomerObject
     * @param int $classDateCustomerId
     * @return resultObject
     */
    public static function getClassDateCustomerByID($classDateCustomerId){

        try {
            $instance = new self();
            $classDateCustomerData = $instance->loadClassDateCustomerFromDB($classDateCustomerId);

            $classDateCustomerObj = classDateCustomerObject::withData($classDateCustomerData);
            $result = resultObject::withData(1,'',$classDateCustomerObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateCustomerId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update classDateCustomer in DB
     * @param classDateCustomerObject $classDateCustomerObj
     * @return resultObject
     */
    public static function updateClassDateCustomer(classDateCustomerObject $classDateCustomerObj){
        try{
            $instance = new self();
            $instance->upateClassDateCustomerDB($classDateCustomerObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($classDateCustomerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete classDateCustomer from DB
     * @param int $classDateCustomerID
     * @return resultObject
     */
    public static function deleteClassDateCustomerById($classDateID,$classDateCustomerID,$refundAction = "none",$sendMail = true){
        try{

            $instance = new self();

            $classDateCustomerObj = classDateCustomerObject::withData($instance->loadClassDateCustomerFromDB($classDateCustomerID));

            if($classDateCustomerObj->cdc_payment_source == "order"){
                switch($refundAction){
                    case "refund":
                        self::refundClassDateOrder($classDateCustomerObj);
                        break;
                    case "reset_claim":
                        self::resetClaimForClassDate($classDateCustomerObj);
                        break;
                }
            }

            $instance->deleteClassDateCustomerByIdDB($classDateID,$classDateCustomerID);
            $result = resultObject::withData(1);

            if($sendMail){
                self::sendMailToCustomerAboutRemoveFromClass($classDateCustomerID,$classDateID);
            }

            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateCustomerID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function sendMailToCustomerAboutRemoveFromClass($customerID,$classDateID){

        $instance = new self();
        $extraParams = array();

        $customerModel = new customerModel();
        $customerRes = $customerModel->getCustomerWithID($customerID);

        if($customerRes->code != 1) return;

        $customerObject = $customerRes->data;

        $classResp = self::getClassByClassDateID($classDateID);
        $classObject = $classResp->data;

        $classDateResp =  self::getClassDateByID($classDateID);
        $classDateObject = $classDateResp->data;

        $custName = $customerObject->cust_first_name;
        $custEmail = $customerObject->cust_email;

        $hourRow = $instance->getHourDataFromHourID($classDateObject->ccd_start_time);

        $bizCountry = bizManager::getBizCountryID($classObject->calc_biz_id);

        if($custEmail != "" && $custEmail != "N/A"){
            $extraParams["to_name"] = $custName;
            $extraParams["to_email"] = $custEmail;
            $extraParams["cust_name"] = stripslashes($custName);
            $extraParams["class_name"] = stripslashes($classObject->calc_name);
            $dateDay = $bizCountry == "1" ? date( 'm/d/Y', strtotime(($classDateObject->ccd_start_date) )) : date( 'd/m/Y', strtotime(($classDateObject->ccd_start_date) ));
            $extraParams["class_date"] = $dateDay;
            $extraParams["class_time"] = $hourRow['hour_start'];

            emailManager::sendSystemMailApp($classObject->calc_biz_id,196,enumEmailType::SystemMailSGCare,$extraParams);

            $message = pushManager::getSystemAlertPushMessageText(37,$classObject->calc_biz_id,$extraParams);
            pushManager::sendPushBiz($classObject->calc_biz_id,$customerObject->cust_id,enumPushType::biz_personalBooking,$message);
        }
    }

    /**
     * Summary of getAllCustomersForClassDate
     * @param mixed $classDateId
     * @return resultObject
     */
    public static function getAllCustomersForClassDate($classDateId){

        try {
            $instance = new self();
            $classDateCustomersData = $instance->loadAllCustomersForClassDateFromDB($classDateId);
            $customers = array();


            if(count($classDateCustomersData) > 0){
                foreach ($classDateCustomersData as $customerData)
                {


                    $customerDate = classDateCustomerObject::withData($customerData);

                    $customerObj = $customerDate->adminAPIArray();
                    $customerObj["cust_pic"] = $customerData["cust_pic"];
                    $customerObj["cust_first_name"] = $customerData["cust_first_name"];
                    $customerObj["email"] = $customerData["cust_email"];
                    $customerObj["phone"] = $customerData["cust_phone1"];
                    array_push($customers,$customerObj);
                }
            }

            $result = resultObject::withData(1,'',$customers);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$classDateId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CLASSDATECUSTOMER - DB METHODS           */
    /************************************* */

    private function addClassDateCustomerDB(classDateCustomerObject $obj){

        if (!isset($obj)){
            throw new Exception("classDateCustomerObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_class_date_customers SET
                                        cdc_biz_id = {$obj->cdc_biz_id},
                                        cdc_class_id = {$obj->cdc_class_id},
                                        cdc_class_date_id = {$obj->cdc_class_date_id},
                                        cdc_customer_id = {$obj->cdc_customer_id},
                                        cdc_comments = '".addslashes($obj->cdc_comments)."',
                                        cdc_payment_source = '{$obj->cdc_payment_source}',
                                        cdc_payment_source_id = {$obj->cdc_payment_source_id},
                                        cdc_payment_source_inner_id = {$obj->cdc_payment_source_inner_id},
                                        cdc_scheduled_from = '{$obj->cdc_scheduled_from}'
                                                 ");
        return $newId;
    }

    private function loadClassDateCustomerFromDB($classDateCustomerID){

        if (!is_numeric($classDateCustomerID) || $classDateCustomerID <= 0){
            throw new Exception("Illegal value classDateCustomerID");
        }

        return $this->db->getRow("SELECT * FROM tbl_class_date_customers WHERE cdc_id = $classDateCustomerID");
    }

    private function upateClassDateCustomerDB(classDateCustomerObject $obj){

        if (!isset($obj->cdc_id) || !is_numeric($obj->cdc_id) || $obj->cdc_id <= 0){
            throw new Exception("classDateCustomerObject value must be provided");
        }

        $cdc_signupDate = isset($obj->cdc_signup) ? "'".$obj->cdc_signup."'" : "null";

        $this->db->execute("UPDATE tbl_class_date_customers SET
                                cdc_biz_id = {$obj->cdc_biz_id},
                                cdc_class_id = {$obj->cdc_class_id},
                                cdc_class_date_id = {$obj->cdc_class_date_id},
                                cdc_customer_id = {$obj->cdc_customer_id},
                                cdc_signup = $cdc_signupDate,
                                cdc_comments = '".addslashes($obj->cdc_comments)."',
                                cdc_payment_source = '{$obj->cdc_payment_source}',
                                cdc_payment_source_id = {$obj->cdc_payment_source_id},
                                cdc_payment_source_inner_id = {$obj->cdc_payment_source_inner_id},
                                cdc_scheduled_from = '{$obj->cdc_scheduled_from}'
                                WHERE cdc_id = {$obj->cdc_id}
                                         ");
    }

    private function deleteClassDateCustomerByIdDB($classDateID,$classDateCustomerID){

        if (!is_numeric($classDateCustomerID) || $classDateCustomerID <= 0){
            throw new Exception("Illegal value classDateCustomerID");
        }

        $this->db->execute("DELETE FROM tbl_class_date_customers
                                WHERE cdc_class_date_id = $classDateID
                                AND cdc_customer_id = $classDateCustomerID");
    }

    private function loadAllCustomersForClassDateFromDB($classDateID){

        if (!is_numeric($classDateID) || $classDateID <= 0){
            throw new Exception("Illegal value classDateCustomerID");
        }

        return $this->db->getTable("SELECT * FROM tbl_class_date_customers,tbl_customers
                                    WHERE cdc_customer_id = cust_id
                                    AND cdc_class_date_id = $classDateID");
    }

    /************************************* */
    /*   BASIC EMPLOYEEMEETING - PUBLIC           */
    /************************************* */

    /**
    * Insert new employeeMeetingObject to DB
    * Return Data = new employeeMeetingObject ID
    * @param employeeMeetingObject $employeeMeetingObj
    * @return resultObject
    */
    public static function addEmployeeMeeting(employeeMeetingObject $employeeMeetingObj){
            try{
                $instance = new self();
                $newId = $instance->addEmployeeMeetingDB($employeeMeetingObj);
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeMeetingObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get employeeMeeting from DB for provided ID
    * * Return Data = employeeMeetingObject
    * @param int $employeeMeetingId
    * @return resultObject
    */
    public static function getEmployeeMeetingByID($employeeMeetingId){

            try {
                $instance = new self();
                $employeeMeetingData = $instance->loadEmployeeMeetingFromDB($employeeMeetingId);

                $employeeMeetingObj = employeeMeetingObject::withData($employeeMeetingData);
                $result = resultObject::withData(1,'',$employeeMeetingObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeMeetingId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update a1History in DB
    * @param employeeMeetingObject $employeeMeetingObj
    * @return resultObject
    */
    public static function updateEmployeeMeeting(employeeMeetingObject $employeeMeetingObj){
            try{
                $instance = new self();
                $instance->upateEmployeeMeetingDB($employeeMeetingObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeMeetingObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete employeeMeeting from DB
    * @param int $employeeMeetingID
    * @return resultObject
    */
    public static function deleteEmployeeMeetingById($employeeMeetingID){
            try{
                $instance = new self();
                $instance->deleteEmployeeMeetingByIdDB($employeeMeetingID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeMeetingID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
     * Get the count of customers scheduled meetings for the last X days
     * Return Data = count of the meetings
     * @param mixed $cust_id
     * @param mixed $period
     * @return resultObject
     */
    public static function getCustMeetingCountForLastXDays($cust_id,$days){

        try {
            $instance = new self();
            $cnt = $instance->getCustMeetingCountForLastXDays_DB($cust_id,$days);

            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get the count of customers scheduled meetings
     * Return Data = count of the meetings
     * @param mixed $cust_id
     * @param mixed $period
     * @return resultObject
     */
    public static function getCustMeetingCount($cust_id){

        try {
            $instance = new self();
            $cnt = $instance->getCustMeetingCount_DB($cust_id);

            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeeMeetingsForClassDate($classDateId){

        $instance = new self();

        return $instance->db->getTable("SELECT * FROM tbl_employee_meeting WHERE em_class_date_id = $classDateId");
    }

    public static function getEmployeeMeetingsForClassDateAndEmployeeID($classDateId,$employeeID){

        $instance = new self();

        return $instance->db->getRow("SELECT * FROM tbl_employee_meeting
                                        WHERE em_class_date_id = $classDateId
                                        AND em_emp_id = $employeeID");
    }

    public static function getCustomersForClassDate($classDateId){

        $instance = new self();

        return $instance->db->getTable("SELECT tbl_customers.*,cdc_id,cdc_customer_id,cdc_biz_id
                                        FROM tbl_class_date_customers
                                        INNER JOIN tbl_customers ON cust_id = cdc_customer_id
                                        WHERE cdc_class_date_id = $classDateId");
    }

    public static function getEmployeeMeetingsForOrderItemID($itemID){
        try {
            $instance = new self();
            $employeeMeetingRows = $instance->getEmployeeMeetingsForOrderItemIDDB($itemID);

            $result = array();
            foreach ($employeeMeetingRows as $employeeMeetingData)
            {
                $result[] = employeeMeetingObject::withData($employeeMeetingData);
            }

            return resultObject::withData(1,'',$result);
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$itemID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC EMPLOYEEMEETING - DB METHODS           */
    /************************************* */

    private function addEmployeeMeetingDB(employeeMeetingObject $obj){

        if (!isset($obj)){
            throw new Exception("employeeMeetingObject value must be provided");
        }

        $em_start_dateDate = isset($obj->em_start_date) ? "'".$obj->em_start_date."'" : "null";

        $em_end_dateDate = isset($obj->em_end_date) ? "'".$obj->em_end_date."'" : "null";

        $em_recurring_end_dateDate = isset($obj->em_recurring_end_date) ? "'".$obj->em_recurring_end_date."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_employee_meeting SET
                                        em_emp_id = {$obj->em_emp_id},
                                        em_biz_id = {$obj->em_biz_id},
                                        em_name = '".addslashes($obj->em_name)."',
                                        em_class_id = {$obj->em_class_id},
                                        em_class_date_id = {$obj->em_class_date_id},
                                        em_start_date = $em_start_dateDate,
                                        em_start_time = {$obj->em_start_time},
                                        em_start = '{$obj->em_start}',
                                        em_end_date = $em_end_dateDate,
                                        em_end_time = {$obj->em_end_time},
                                        em_end = '{$obj->em_end}',
                                        em_color = '".addslashes($obj->em_color)."',
                                        em_from_mobile = {$obj->em_from_mobile},
                                        em_cust_device_id = '".addslashes($obj->em_cust_device_id)."',
                                        em_cust_id = {$obj->em_cust_id},
                                        em_cust_name = '".addslashes($obj->em_cust_name)."',
                                        em_cust_cancelled = {$obj->em_cust_cancelled},
                                        em_meet_type = {$obj->em_meet_type},
                                        em_cust_request = '".addslashes($obj->em_cust_request)."',
                                        em_location = '".addslashes($obj->em_location)."',
                                        em_is_recurring = '{$obj->em_is_recurring}',
                                        em_recurring_limit_method = '{$obj->em_recurring_limit_method}',
                                        em_recurring_amount = '{$obj->em_recurring_amount}',
                                        em_recurring_end_date = $em_recurring_end_dateDate,
                                        em_recurring_group = {$obj->em_recurring_group},
                                        em_external_id = '".addslashes($obj->em_external_id)."',
                                        em_source = '{$obj->em_source}',
                                        em_all_day_event = {$obj->em_all_day_event},
                                        em_payment_source = '{$obj->em_payment_source}',
                                        em_payment_source_id = {$obj->em_payment_source_id},
                                        em_payment_source_inner_id = {$obj->em_payment_source_inner_id}
                                                 ");
         return $newId;
    }

    private function loadEmployeeMeetingFromDB($employeeMeetingID){

        if (!is_numeric($employeeMeetingID) || $employeeMeetingID <= 0){
            throw new Exception("Illegal value employeeMeetingID");
        }

        return $this->db->getRow("SELECT * FROM tbl_employee_meeting WHERE em_id = $employeeMeetingID");
    }

    private function getEmployeeMeetingsForOrderItemIDDB($itemID){
        if (!is_numeric($itemID) || $itemID <= 0){
            throw new Exception("Illegal value itemID");
        }

        return $this->db->getTable("SELECT * FROM tbl_employee_meeting WHERE em_payment_source_inner_id = $itemID");
    }

    private function upateEmployeeMeetingDB(employeeMeetingObject $obj){

        if (!isset($obj->em_id) || !is_numeric($obj->em_id) || $obj->em_id <= 0){
            throw new Exception("employeeMeetingObject value must be provided");
        }

        $em_scheduled_onDate = isset($obj->em_scheduled_on) ? "'".$obj->em_scheduled_on."'" : "null";

        $em_start_dateDate = isset($obj->em_start_date) ? "'".$obj->em_start_date."'" : "null";

        $em_end_dateDate = isset($obj->em_end_date) ? "'".$obj->em_end_date."'" : "null";

        $em_recurring_end_dateDate = isset($obj->em_recurring_end_date) ? "'".$obj->em_recurring_end_date."'" : "null";

        $this->db->execute("UPDATE tbl_employee_meeting SET
        em_emp_id = {$obj->em_emp_id},
        em_biz_id = {$obj->em_biz_id},
        em_name = '".addslashes($obj->em_name)."',
        em_scheduled_on = $em_scheduled_onDate,
        em_class_id = {$obj->em_class_id},
        em_class_date_id = {$obj->em_class_date_id},
        em_start_date = $em_start_dateDate,
        em_start_time = {$obj->em_start_time},
        em_start = '{$obj->em_start}',
        em_end_date = $em_end_dateDate,
        em_end_time = {$obj->em_end_time},
        em_end = '{$obj->em_end}',
        em_color = '".addslashes($obj->em_color)."',
        em_from_mobile = {$obj->em_from_mobile},
        em_cust_device_id = '".addslashes($obj->em_cust_device_id)."',
        em_cust_id = {$obj->em_cust_id},
        em_cust_name = '".addslashes($obj->em_cust_name)."',
        em_cust_cancelled = {$obj->em_cust_cancelled},
        em_meet_type = {$obj->em_meet_type},
        em_cust_request = '".addslashes($obj->em_cust_request)."',
        em_location = '".addslashes($obj->em_location)."',
        em_is_recurring = '{$obj->em_is_recurring}',
        em_recurring_limit_method = '{$obj->em_recurring_limit_method}',
        em_recurring_amount = '{$obj->em_recurring_amount}',
        em_recurring_end_date = $em_recurring_end_dateDate,
        em_recurring_group = {$obj->em_recurring_group},
        em_external_id = '".addslashes($obj->em_external_id)."',
        em_source = '{$obj->em_source}',
        em_all_day_event = {$obj->em_all_day_event},
        em_payment_source = '{$obj->em_payment_source}',
        em_payment_source_id = {$obj->em_payment_source_id},
        em_payment_source_inner_id = {$obj->em_payment_source_inner_id}
        WHERE em_id = {$obj->em_id}
                 ");
    }

    private function deleteEmployeeMeetingByIdDB($employeeMeetingID){

        if (!is_numeric($employeeMeetingID) || $employeeMeetingID <= 0){
            throw new Exception("Illegal value employeeMeetingID");
        }

        $this->db->execute("DELETE FROM tbl_employee_meeting WHERE em_id = $employeeMeetingID");
    }

    private function getCustMeetingCountForLastXDays_DB($cust_id,$days = 0){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");
        }

        return $this->db->getVal("SELECT COUNT(*) FROM tbl_employee_meeting
                WHERE em_cust_id = $cust_id
                AND em_scheduled_on >= (NOW() - INTERVAL $days DAY)");
    }

    private function getCustMeetingCount_DB($cust_id){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");
        }

        return $this->db->getVal("SELECT COUNT(*)
                                    FROM tbl_employee_meeting
                                    WHERE em_cust_id = $cust_id");
    }

    private function getEmployeeMeetingForCustomerDB(customerObject $customer,$start_date){
        $sql = "SELECT * FROM tbl_employee_meeting,tbl_employee
                WHERE em_end_date >= STR_TO_DATE('$start_date','%d/%m/%Y')
                AND em_cust_id = {$customer->cust_id}
                AND em_biz_id = {$customer->cust_biz_id}
                AND em_emp_id = be_id ";

        return $this->db->getTable($sql);
    }

    private function getMeetingsRecurringDB($employeeMeetingID,$groupId){

        if (!is_numeric($employeeMeetingID) || $employeeMeetingID <= 0){
            throw new Exception("Illegal value employeeMeetingID");
        }

        return $this->db->getTable("SELECT * FROM tbl_employee_meeting WHERE em_id > $employeeMeetingID AND em_recurring_group = $groupId AND em_recurring_group > 0");
    }

    /************************************* */
    /*   BASIC EMPLOYEE - PUBLIC           */
    /************************************* */

    /**
     * Insert new employeeObject to DB
     * Return Data = new employeeObject ID
     * @param employeeObject $employeeObj
     * @return resultObject
     */
    public static function addEmployee(employeeObject $employeeObj){
        try{
            $instance = new self();
            $newId = $instance->addEmployeeDB($employeeObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get employee from DB for provided ID
     * * Return Data = employeeObject
     * @param int $employeeId
     * @return resultObject
     */
    public static function getEmployeeByID($employeeId){

        try {
            $instance = new self();
            $employeeData = $instance->loadEmployeeFromDB($employeeId);

            $employeeObj = employeeObject::withData($employeeData);
            $result = resultObject::withData(1,'',$employeeObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update employeeObject in DB
     * @param employeeObject $employeeObj
     * @return resultObject
     */
    public static function updateEmployee(employeeObject $employeeObj){
        try{
            $instance = new self();
            $instance->upateEmployeeDB($employeeObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($employeeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete employee from DB
     * @param int $employeeID
     * @return resultObject
     */
    public static function deleteEmployeeById($employeeID){
        try{
            $instance = new self();
            $instance->deleteEmployeeByIdDB($employeeID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$employeeID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getEmployeePic(employeeObject $employee){
        return isset($employee->be_pic) && $employee->be_pic == "" ? "https://storage.googleapis.com/paptap/system/no_employee_image.png" : utilityManager::getImageThumb($employee->be_pic);
    }

    /************************************* */
    /*   BASIC EMPLOYEE - DB METHODS           */
    /************************************* */

    private function addEmployeeDB(employeeObject $obj){

        if (!isset($obj)){
            throw new Exception("employeeObject value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_employee SET
                                    be_biz_id = {$obj->be_biz_id},
                                    be_account_id = {$obj->be_account_id},
                                    be_name = '".addslashes($obj->be_name)."',
                                    be_mail = '".addslashes($obj->be_mail)."',
                                    be_pass = '".addslashes($obj->be_pass)."',
                                    be_job = '".addslashes($obj->be_job)."',
                                    be_active = {$obj->be_active},
                                    be_pic = '".addslashes($obj->be_pic)."',
                                    be_default_color = {$obj->be_default_color},
                                    be_phone = '".addslashes($obj->be_phone)."',
                                    be_cal_type = '{$obj->be_cal_type}',
                                    be_cal_id = '".addslashes($obj->be_cal_id)."',
                                    be_cal_name = '".addslashes($obj->be_cal_name)."',
                                    be_token = '".addslashes($obj->be_token)."',
                                    be_event_color = '".addslashes($obj->be_event_color)."',
                                    be_reminder_time = {$obj->be_reminder_time},
                                    be_sync_token = '".addslashes($obj->be_sync_token)."',
                                    be_token_expiration = '{$obj->be_token_expiration}',
                                    be_ipn_push_id = '".addslashes($obj->be_ipn_push_id)."',
                                    be_ipn_expiration = '{$obj->be_ipn_expiration}',
                                    be_apple_user = '".addslashes($obj->be_apple_user)."',
                                    be_apple_pass = '".addslashes($obj->be_apple_pass)."',
                                    be_outlook_user = '".addslashes($obj->be_outlook_user)."'
                                             ");
        return $newId;
    }

    private function loadEmployeeFromDB($employeeID){

        if (!is_numeric($employeeID) || $employeeID <= 0){
            throw new Exception("Illegal value employeeID");
        }

        return $this->db->getRow("SELECT * FROM tbl_employee WHERE be_id = $employeeID");
    }

    private function upateEmployeeDB(employeeObject $obj){

        if (!isset($obj->be_id) || !is_numeric($obj->be_id) || $obj->be_id <= 0){
            throw new Exception("employeeObject value must be provided");
        }

        $this->db->execute("UPDATE tbl_employee SET
                            be_biz_id = {$obj->be_biz_id},
                            be_account_id = {$obj->be_account_id},
                            be_name = '".addslashes($obj->be_name)."',
                            be_mail = '".addslashes($obj->be_mail)."',
                            be_pass = '".addslashes($obj->be_pass)."',
                            be_job = '".addslashes($obj->be_job)."',
                            be_active = {$obj->be_active},
                            be_pic = '".addslashes($obj->be_pic)."',
                            be_default_color = {$obj->be_default_color},
                            be_phone = '".addslashes($obj->be_phone)."',
                            be_cal_type = '{$obj->be_cal_type}',
                            be_cal_id = '".addslashes($obj->be_cal_id)."',
                            be_cal_name = '".addslashes($obj->be_cal_name)."',
                            be_token = '".addslashes($obj->be_token)."',
                            be_event_color = '".addslashes($obj->be_event_color)."',
                            be_reminder_time = {$obj->be_reminder_time},
                            be_sync_token = '".addslashes($obj->be_sync_token)."',
                            be_token_expiration = '{$obj->be_token_expiration}',
                            be_ipn_push_id = '".addslashes($obj->be_ipn_push_id)."',
                            be_ipn_expiration = '{$obj->be_ipn_expiration}',
                            be_apple_user = '".addslashes($obj->be_apple_user)."',
                            be_apple_pass = '".addslashes($obj->be_apple_pass)."',
                            be_outlook_user = '".addslashes($obj->be_outlook_user)."'
                            WHERE be_id = {$obj->be_id}
                                     ");
    }

    private function deleteEmployeeByIdDB($employeeID){

        if (!is_numeric($employeeID) || $employeeID <= 0){
            throw new Exception("Illegal value employeeID");
        }

        $this->db->execute("DELETE FROM tbl_employee WHERE be_id = $employeeID");
    }

    private function getEmployeesRowsForService(serviceObject $service){
        $sql = "SELECT * FROM tbl_employee_meet_type,tbl_employee
                WHERE be_biz_id = {$service->bmt_biz_id}
                AND emt_bmt_id = {$service->bmt_id}
                AND emt_emp_id = be_id
                AND be_active = 1
                AND emt_active = 1
                ORDER BY be_id ASC";

        return $this->db->getTable($sql);
    }

    private function getEmployeeRowsForBizID($bizID){
        $sql = "SELECT * FROM tbl_biz_cal_meet_type,tbl_employee_meet_type,tbl_employee
                    WHERE be_biz_id = $bizID
		            AND emt_emp_id = be_id
                    AND emt_bmt_id = bmt_id
                    AND emt_active = 1
                    AND bmt_active = 1
                    AND be_active = 1
                    AND bmt_mobile_active = 1
                    GROUP BY be_id";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   Booking utilities                 */
    /************************************* */

    private function getStartHourIDFromDate($start_date,$offset){
        if ($start_date == ''){
            $start_date = mktime(0, 0, 0);
            $nowTimeStamp = time() + 60*60 + $offset;

        }else{


            $qure_date = mktime(0, 0, 0);

            $day = date('d',$start_date);
            $month = date('m',$start_date);
            $year = date('Y',$start_date);
            $start_date = mktime(0, 0, 0, $month, $day, $year);

            if($start_date <= $qure_date) {
                $start_date = $qure_date;
                $nowTimeStamp = time() + 60*60 + $offset;
            }else{

                $nowTimeStamp = mktime(0, 0, 0);
            }
        }

        $nowHour = date('H',$nowTimeStamp);
        $nowHour .= ":00";

        return $this->db->getVal("SELECT hour_id FROM calendar_hours WHERE hour_start = '$nowHour'");
    }

    private function getMeetingListBoth(employeeObject $employee,serviceObject $service,$start_date,$startHourID){
        $bizID = $service->bmt_biz_id;
        $meetSize = $service->bmt_size;
        if ($this->currentRecordNo >= $this->maxRecords){
            return;
        }

        $numLoops = 0;
        while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){
            $startDateDayOfWeek = date('w', $start_date) - 1;
            if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;

            $startDate = date( "Y-m-d",$start_date);

            $end_date = $start_date + 5*$meetSize*60;
            $endDate = date( "Y-m-d",$end_date);

            $allSlots = $this->getAllSlotsFromSpecificHour($bizID,$service,$employee,$startDateDayOfWeek,$startHourID);

            if (count($allSlots) > 0){
                $takenSlots = $this->getAllTakenBetween($employee,$startDate,$endDate);

                foreach ($allSlots as $oneSlot) {
                    $slotStartHour  = $oneSlot["hour_id"];

                    $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                    $slotEndTime = $slotStartTime + 5*60*$meetSize;


                    $isTaken = 0;
                    $takenEndHourId = 0;
                    if (count($takenSlots) > 0 ){


                        foreach ($takenSlots as $taken) {
                            $takenStartTime = strtotime($taken["em_start"]);
                            $takenEndTime =  strtotime($taken["em_end"]);
                            $takenEndHourId = $taken["em_end_time"];

                            if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                //It is not overlaping
                                $isTaken = 0;
                            }else{
                                $isTaken = 1;
                                break;
                            }
                        }
                    }

                    if ($this->currentRecordNo == $this->maxRecords){
                        return;
                    }

                    if ($isTaken == 0){
                        $slotData = array();
                        $slotData["meet_emp_id"] = $employee->be_id;
                        $slotData["meet_emp_name"] = $employee->be_name;
                        $slotData["meet_emp_pic"] = self::getEmployeePic($employee);
                        $slotData["meet_type_id"] = $service->bmt_id;
                        $slotData["meet_type_name"] = $service->bmt_name;
                        $slotData["meet_price"] = $service->bmt_price;
                        $slotData["meet_start_timestamp"] = $slotStartTime;
                        $slotData["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                        $slotData["meet_end_timestamp"] = $slotEndTime;
                        $slotData["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);

                        $this->slotsList[$this->index] = $slotData;
                        $this->index++;
                        $this->currentRecordNo++;
                    }else{
                        $day = date('d',$takenEndTime);
                        $month = date('m',$takenEndTime);
                        $year = date('Y',$takenEndTime);
                        $goTo = mktime(0, 0, 0, $month, $day, $year);

                        $this->getMeetingListBoth($employee,$service,$goTo,($takenEndHourId + 1));

                    }

                    if ($this->currentRecordNo == $this->maxRecords){
                        return;
                    }
                }
            }
            else{
                $numLoops = $numLoops + 1;
            }
            $startHourID = 0;
            $start_date = $start_date + 24*60*60;
        }

        return;
    }

    private function getMeetingListOnlyMeetingType(serviceObject $service,$start_date,$startHourID){
        $bizID = $service->bmt_biz_id;
        $meetSize = $service->bmt_size;
        if ($this->currentRecordNo >= $this->maxRecords){
            return;
        }

        $employees = $this->getEmployeesRowsForService($service);

        if (count($employees) > 0){
            $numLoops = 0;
            while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){

                $end_date = $start_date + 5*$meetSize*60;

                foreach ($employees as $employeeRow)
                {
                	$employee = employeeObject::withData($employeeRow);

                    $startDateDayOfWeek = date('w', $start_date) - 1;
                    if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;
                    $startDate = date( "Y-m-d",$start_date);

                    $endDate = date( "Y-m-d",$end_date);

                    $allSlots = $this->getAllSlotsFromSpecificHour($bizID,$service,$employee,$startDateDayOfWeek,$startHourID);

                    if (count($allSlots) > 0){
                        $takenSlots = $this->getAllTakenBetween($employee,$startDate,$endDate);

                        foreach ($allSlots as $oneSlot) {
                            $slotStartHour  = $oneSlot["hour_id"];

                            $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                            $slotEndTime = $slotStartTime + 5*60*$meetSize;


                            $isTaken = 0;
                            $takenEndHourId = 0;
                            if (count($takenSlots) > 0 ){


                                foreach ($takenSlots as $taken) {
                                    $takenStartTime = strtotime($taken["em_start"]);
                                    $takenEndTime =  strtotime($taken["em_end"]);
                                    $takenEndHourId = $taken["em_end_time"];

                                    if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                        //It is not overlaping
                                        $isTaken = 0;
                                    }else{
                                        $isTaken = 1;
                                        break;
                                    }
                                }
                            }

                            if ($this->currentRecordNo == $this->maxRecords){
                                return;
                            }

                            if ($isTaken == 0){
                                $slotData = array();
                                $slotData["meet_emp_id"] = $employee->be_id;
                                $slotData["meet_emp_name"] = $employee->be_name;
                                $slotData["meet_emp_pic"] = self::getEmployeePic($employee);
                                $slotData["meet_type_id"] = $service->bmt_id;
                                $slotData["meet_type_name"] = $service->bmt_name;
                                $slotData["meet_price"] = $service->bmt_price;
                                $slotData["meet_start_timestamp"] = $slotStartTime;
                                $slotData["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                                $slotData["meet_end_timestamp"] = $slotEndTime;
                                $slotData["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);

                                $this->slotsList[$this->index] = $slotData;
                                $this->index++;
                                $this->currentRecordNo++;
                            }else{
                                $day = date('d',$takenEndTime);
                                $month = date('m',$takenEndTime);
                                $year = date('Y',$takenEndTime);
                                $goTo = mktime(0, 0, 0, $month, $day, $year);

                                $this->getMeetingListBoth($employee,$service,$goTo,($takenEndHourId + 1));

                            }

                            if ($this->currentRecordNo == $this->maxRecords){
                                return;
                            }
                        }
                    }
                    else{
                        $numLoops = $numLoops + 1;
                    }
                    $startHourID = 0;
                    $start_date = $start_date + 24*60*60;
                }

            }
        }
    }

    private function getMeetingListOnlyEmployee(employeeObject $employee,$start_date,$startHourID){
        $bizID = $employee->be_biz_id;

        if ($this->currentRecordNo >= $this->maxRecords){
            return;
        }

        $numLoops = 0;
        while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){
            $services = $this->getServicesForEmployee($employee);

            if(count($services) > 0){
                $numLoops = 0;
                while ($this->currentRecordNo <=  $this->maxRecords && $numLoops < 100){
                    foreach ($services as $serviceRow)
                    {
                    	$service = serviceObject::withData($serviceRow);

                        $meetSize = $service->bmt_size;

                        $startDateDayOfWeek = date('w', $start_date) - 1;
                        if ($startDateDayOfWeek < 0) $startDateDayOfWeek = 6;

                        $startDate = date( "Y-m-d",$start_date);

                        $end_date = $start_date + 5*$meetSize*60;
                        $endDate = date( "Y-m-d",$end_date);

                        $allSlots = $this->getAllSlotsFromSpecificHour($bizID,$service,$employee,$startDateDayOfWeek,$startHourID);

                        if (count($allSlots) > 0){
                            $takenSlots = $this->getAllTakenBetween($employee,$startDate,$endDate);

                            foreach ($allSlots as $oneSlot) {
                                $slotStartHour  = $oneSlot["hour_id"];

                                $slotStartTime = $start_date + 5*60*($slotStartHour - 1);
                                $slotEndTime = $slotStartTime + 5*60*$meetSize;


                                $isTaken = 0;
                                $takenEndHourId = 0;
                                if (count($takenSlots) > 0 ){


                                    foreach ($takenSlots as $taken) {
                                        $takenStartTime = strtotime($taken["em_start"]);
                                        $takenEndTime =  strtotime($taken["em_end"]);
                                        $takenEndHourId = $taken["em_end_time"];

                                        if ($slotEndTime <= $takenStartTime || $slotStartTime >= $takenEndTime){
                                            //It is not overlaping
                                            $isTaken = 0;
                                        }else{
                                            $isTaken = 1;
                                            break;
                                        }
                                    }
                                }

                                if ($this->currentRecordNo == $this->maxRecords){
                                    return;
                                }

                                if ($isTaken == 0){
                                    $slotData = array();
                                    $slotData["meet_emp_id"] = $employee->be_id;
                                    $slotData["meet_emp_name"] = $employee->be_name;
                                    $slotData["meet_emp_pic"] = self::getEmployeePic($employee);
                                    $slotData["meet_type_id"] = $service->bmt_id;
                                    $slotData["meet_type_name"] = $service->bmt_name;
                                    $slotData["meet_price"] = $service->bmt_price;
                                    $slotData["meet_start_timestamp"] = $slotStartTime;
                                    $slotData["meet_start_time"] = date('Y-m-d H:i',$slotStartTime);
                                    $slotData["meet_end_timestamp"] = $slotEndTime;
                                    $slotData["meet_end_time"] = date('Y-m-d H:i',$slotEndTime);

                                    $this->slotsList[$this->index] = $slotData;
                                    $this->index++;
                                    $this->currentRecordNo++;
                                }else{
                                    $day = date('d',$takenEndTime);
                                    $month = date('m',$takenEndTime);
                                    $year = date('Y',$takenEndTime);
                                    $goTo = mktime(0, 0, 0, $month, $day, $year);

                                    $this->getMeetingListBoth($employee,$service,$goTo,($takenEndHourId + 1));

                                }

                                if ($this->currentRecordNo == $this->maxRecords){
                                    return;
                                }
                            }
                        }
                        else{
                            $numLoops = $numLoops + 1;
                        }
                        $startHourID = 0;
                        $start_date = $start_date + 24*60*60;
                    }

                }
            }
        }
    }

    private function getAllSlotsFromSpecificHour($bizID,serviceObject $service,employeeObject $employee,$startDateDayOfWeek,$startHourID){



        $size = $service->bmt_size;
        if ($size > 144){
            $size = 36;
        }

        $calSettings = bizManager::getBizCalendarSettingsByBizID($bizID);



        if(!isset($calSettings)){
            throw new Exception("bizCalendarSettingsObject is null");
        }



        $onlyBizHours = $calSettings->biz_cal_biz_hours_only;
        $bizSlotsGap = $calSettings->biz_cal_slot_gap;
        $canOverTime = $calSettings->biz_cal_allow_overtime == 1;

        if($bizSlotsGap < 144){
            $modulo = "AND MOD(hour_id - tbl_employee_hours.bch_begin_hour, $bizSlotsGap) = 0 ";
        }
        else{
            $modulo = " AND MOD(hour_id,$bizSlotsGap) = tbl_employee_hours.bch_begin_hour";
        }

        if($onlyBizHours == 1){
            if($canOverTime){
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_biz_cal_hours,tbl_employee_hours
                                    WHERE bch_biz_id = $bizID
                                    AND bch_emp_id = {$employee->be_id}
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_working_day = 1
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id between GREATEST($startHourID,tbl_biz_cal_hours.bch_begin_hour) and tbl_biz_cal_hours.bch_finish_hour
                                    AND hour_id >= GREATEST($startHourID,tbl_employee_hours.bch_begin_hour)
                                    $modulo
                                    ORDER BY hour_id";
            }
            else{
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_biz_cal_hours,tbl_employee_hours
                                    WHERE bch_biz_id = $bizID
                                    AND bch_emp_id = {$employee->be_id}
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_biz_cal_hours.bch_working_day = 1
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id between GREATEST($startHourID,tbl_biz_cal_hours.bch_begin_hour) and tbl_biz_cal_hours.bch_finish_hour
                                    AND hour_id between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND (hour_id + $size) between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and (tbl_employee_hours.bch_finish_hour + 1)
                                    $modulo
                                    ORDER BY hour_id";
            }
        }
        else{
            if($canOverTime){
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_employee_hours
                                    WHERE bch_emp_id = {$employee->be_id}
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id >= GREATEST($startHourID,tbl_employee_hours.bch_begin_hour)
                                    $modulo
                                    ORDER BY hour_id";
            }
            else{
                $sql = "SELECT hour_id, hour_start,hour_end FROM calendar_hours,tbl_employee_hours
                                    WHERE bch_emp_id = {$employee->be_id}
                                    AND tbl_employee_hours.bch_day_no = $startDateDayOfWeek
                                    AND tbl_employee_hours.bch_working_day = 1
                                    AND hour_id between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and tbl_employee_hours.bch_finish_hour
                                    AND (hour_id + $size) between GREATEST($startHourID,tbl_employee_hours.bch_begin_hour) and (tbl_employee_hours.bch_finish_hour + 1)
                                    $modulo
                                    ORDER BY hour_id";
            }
        }


        return $this->db->getTable($sql);
    }

    private function getAllTakenBetween(employeeObject $employee,$start,$end){


        return $this->db->getTable("SELECT * FROM tbl_employee_meeting
                                WHERE em_emp_id = {$employee->be_id}
                                AND (
                                    (
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$start','%Y-%m-%d')
                                    AND
                                    DATE_FORMAT(em_end_date , '%Y-%m-%d') >= STR_TO_DATE('$start','%Y-%m-%d')
                                   )
                                   OR
                                   (
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') >= STR_TO_DATE('$start','%Y-%m-%d')
                                    AND
                                    DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$end','%Y-%m-%d')
                                    )
                                )
                                ");

    }

    private function getHourIDFromStartHour($startHour){
        return $this->db->getVal("SELECT hour_id FROM calendar_hours WHERE hour_start like '$startHour'");
    }

    private function getHourIDFromEndHour($endHour){
        return $this->db->getVal("SELECT hour_id FROM calendar_hours WHERE hour_end like '$endHour'");
    }

    private function calculateStartHourFromHourID($hourID){
        $totalMinutes = ($hourID - 1) * 5;

        $hour = floor($totalMinutes / 60);
        $minutes = $totalMinutes % 60;

        $time = str_pad((string)$hour,2,"0",STR_PAD_LEFT).":".str_pad((string)$minutes,2,"0",STR_PAD_LEFT);

        return $time;
    }

    private function calculateEndHourFromHourID($hourID){
        $totalMinutes = $hourID * 5;

        $hour = floor($totalMinutes / 60);
        $minutes = $totalMinutes % 60;

        $time = str_pad((string)$hour,2,"0",STR_PAD_LEFT).":".str_pad((string)$minutes,2,"0",STR_PAD_LEFT);

        return $time;
    }

    private function getHourDataFromHourID($hourID){
        return $this->db->getRow("SELECT * FROM calendar_hours WHERE hour_id = $hourID");
    }

    private function isFutureDate($date){

        $currentTime = time();

        if ($date < $currentTime + 1*60*60){
            return false;
        }

        return true;
    }

    private function verifyAvailability($empID,$starting_date,$ending_date,$starting_time,$ending_time){

        $sql = "SELECT * FROM tbl_employee_meeting
                                    WHERE em_emp_id = $empID
                                    AND (
                                            (
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_start_time <= $starting_time
                                                AND
                                                DATE_FORMAT(em_end_date , '%Y-%m-%d') >= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_end_time >= $starting_time
                                            )
                                            OR
                                            (
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') >= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_start_time >= $starting_time
                                                AND
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$ending_date','%Y-%m-%d')
                                                AND em_start_time <= $ending_time
                                            )
                                        )
                                    ";
        $takenSlots = $this->db->getTable($sql);



        if (count($takenSlots) > 0){
            return 0;
        }

        return 1;

    }

    private function verifyAvailabilityForEditingMeeting($empID,$editedMeetingID,$starting_date,$ending_date,$starting_time,$ending_time){

        $takenSlots = $this->db->getTable("SELECT * FROM tbl_employee_meeting
                                    WHERE em_emp_id = $empID
                                    AND em_id != $editedMeetingID
                                    AND (
                                            (
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_start_time <= $starting_time
                                                AND
                                                DATE_FORMAT(em_end_date , '%Y-%m-%d') >= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_end_time >= $starting_time
                                            )
                                            OR
                                            (
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') >= STR_TO_DATE('$starting_date','%Y-%m-%d')
                                                AND
                                                em_start_time >= $starting_time
                                                AND
                                                DATE_FORMAT(em_start_date , '%Y-%m-%d') <= STR_TO_DATE('$ending_date','%Y-%m-%d')
                                                AND em_start_time <= $ending_time
                                            )
                                        )
                                    ");

        if (count($takenSlots) > 0){
            return 0;
        }

        return 1;

    }

}