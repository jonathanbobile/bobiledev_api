<?php
require_once('../admin/libs/Composer/vendor/autoload.php');

class customerBillingManager{
    protected $db;    
    private $stripeConnectAcount = "";
    private $bizID = 0;

    public function __construct()
    {
        \Stripe\Stripe::setApiVersion(getenv('STRIPE_API_VERSION'));
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.'.get_class($this));
        }       
    }

    public function setBillingBizID($bizId){
        $this->bizID = $bizId;
    }

    /* Invoice Management & Handling */

    /**
     * Summary of initiateCustomerInvoice
     * @param mixed $custID 
     * @return resultObject
     */
    public function initiateCustomerInvoice($custID){
        try{
            if ($custID == "" || $custID == 0 ){
                return resultObject::withData(0,"NO cust ID.");
            }
            $bizID = customerManager::getCustomerBizID($custID);

            if ($bizID == "" || $bizID == 0 ){
                return resultObject::withData(0,"NO biz ID.");
            }

            $invoiceUniqueID = $this->generateUniqueCustInvoiceId($custID,$bizID);
            $storeSettings = bizManager::getBizStoreObjectForBiz($bizID);
            $ownerCurrency = $storeSettings->ess_currency;

            $invoiceObj = new customerInvoiceObject();

            $invoiceObj->cin_biz_id = $bizID;
            $invoiceObj->cin_cust_id = $custID;
            $invoiceObj->cin_unique_id = $invoiceUniqueID;
            $invoiceObj->cin_currency = $ownerCurrency;

            
            $invoice_id = $this->addCustomerInvoiceDB($invoiceObj);

            $invoiceObj->cin_id = $invoice_id;

            $invoiceObj->cin_long_id = substr((string)time(), -8) . $invoice_id;
            
            $this->updateCustomerInvoiceDB($invoiceObj);

            $this->logInvoiceCreation($invoice_id);
            
            return resultObject::withData(1,"",$invoice_id);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$custID);
            return resultObject::withData(0,$e->getMessage());
        }
        
    }
   
    /**
     * create a 15-20 chars ugly unique invoice that takes the bizid, custid & timestamp (the last digits of it)
     * @param mixed $custID 
     * @param mixed $bizID 
     * @return mixed
     */
    public function generateUniqueCustInvoiceId($custID,$bizID){
        $timeLast5Digits = time() % 1000000;

        $timeLast5Digits += rand(1000,99999999);
        $toScramble = $bizID.$custID.$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 25){
            $scrambled .= rand();
        }
        
        $output = 'bbinv_'.substr($scrambled,0,25);

        return $output;        
    }

    public function connectCustInvoiceToCustOrder($orderID, $invoiceID){
        
        $finalized = $this->checkIfCustInvoiceWasFinalized($invoiceID);

        //1. if custInvoice was finalized - return
        if($finalized){//can't add order to finalized invoice
            return resultObject::withData(-1,'');
        }       

        $orderResult = customerOrderManager::getCustomerOrderByID($orderID);
        
        if($orderResult->code == 0){//No order exists
            return resultObject::withData(-2,'');
        }

        $order = $orderResult->data;

        $isConnected = $this->isCustOrderConnectedToInvoice($orderID,$invoiceID);

        if($isConnected){//connection already exists
            return resultObject::withData(1,'');
        }

        //2. make the connection in tbl_cust_invoice_items
        $sql = "INSERT IGNORE INTO tbl_cust_invoice_items SET
                    cini_invoice_id = $invoiceID,
                    cini_order_id = $orderID";

        $this->db->execute($sql);

        //3. ADD the sub total, tax & total of the order into the invoice - don't delete previous sub total, tax, total
        $this->addCustOrderTotalsToInvoice($invoiceID,$order->cto_without_vat,$order->cto_vat,$order->cto_amount);
        
        return resultObject::withData(1,'');
    }

    public function connectCustInvoiceToCustOrderDirect($orderID, $invoiceID){
        $orderResult = customerOrderManager::getCustomerOrderByID($orderID);
        
        if($orderResult->code == 0){//No order exists
            return resultObject::withData(-2,'');
        }

        $isConnected = $this->isCustOrderConnectedToInvoice($orderID,$invoiceID);

        if($isConnected){//connection already exists
            return resultObject::withData(1,'');
        }

        $sql = "INSERT IGNORE INTO tbl_cust_invoice_items SET
                    cini_invoice_id = $invoiceID,
                    cini_order_id = $orderID";

        $this->db->execute($sql);

        return resultObject::withData(1,'');
    }

    public function connectInvoiceToOrderDirectly($orderID, $invoiceID){
        $sql = "INSERT IGNORE INTO tbl_cust_invoice_items SET
                    cini_invoice_id = $invoiceID,
                    cini_order_id = $orderID";

        $this->db->execute($sql);

        return resultObject::withData(1,'');
    }

    public function disconnectCustInvoiceFromCustOrder($orderID, $invoiceID){
        $result = array();

        $finalized = $this->checkIfCustInvoiceWasFinalized($invoiceID);

        //1. if custInvoice was finalized - return
        if($finalized){//can't remove order from finalized invoice
            $result['code'] = -1;

            return $result;
        }

        $isConnected = $this->isCustOrderConnectedToInvoice($orderID,$invoiceID);

        if(!$isConnected){//connection doesn't exist
            $result['code'] = 0;

            return $result;
        }

        $order = captainOrders::getOrderDataFromOrderID($orderID);
        
        if(!isset($order['cto_id'])){// no order exists
            $result['code'] = -2;

            return $result;
        }

        //2. Remove the connection in tbl_cust_invoice_items
        $sql = "DELETE FROM tbl_cust_invoice_items WHERE
                    cini_invoice_id = $invoiceID,
                    cini_order_id = $orderID";

        $this->db->execute($sql);

        //3. SUBTRACT the sub total, tax & total of the order into the invoice - don't delete previous sub total, tax, total
        $this->addValuesToInvoice($invoiceID,-1 * $order['cto_without_vat'],-1 * $order['cto_vat'],-1 * $order['cto_amount']);
        

        $result['code'] = 1;

        return $result;
    }
    
    public function disconnectAllOrdersForCustInvoice($invoiceID){
        //remove all connected cust_invoice_items 
        $sql = "DELETE FROM tbl_cust_invoice_items WHERE cini_invoice_id = $invoiceID";
        $this->db->execute($sql);

        $this->setCustInvoiceTotals($invoiceID,0,0,0);
        return 1;
    }

    public function addCustOrderTotalsToInvoice($invoiceID,$order_sub_total,$order_tax,$order_total){
        $sql = "UPDATE tbl_cust_invoice SET
                    cin_sub_total = cin_sub_total + $order_sub_total,
                    cin_tax = cin_tax + $order_tax,
                    cin_total = cin_total + $order_total
                WHERE cin_id = $invoiceID";

        $this->db->execute($sql);
    }

    public function setCustInvoiceTotals($invoiceID,$sub_total,$tax,$total){
        $sql = "UPDATE tbl_cust_invoice SET
                    cin_sub_total = $sub_total,
                    cin_tax = $tax,
                    cin_total = $total
                WHERE cin_id = $invoiceID";

        $this->db->execute($sql);
    }

    public function isCustOrderConnectedToInvoice($orderID,$invoiceID){
        $sql = "SELECT COUNT(*) FROM tbl_cust_invoice_items
                WHERE cini_invoice_id = $invoiceID
                AND cini_order_id = $orderID";

        return $this->db->getVal($sql) > 0;
    }

    public function finalizeCustInvoice($invoiceID){

        //1. if custInvoice was finalized - return
        $finalized = $this->checkIfCustInvoiceWasFinalized($invoiceID);

        if($finalized){
            $result['code'] = -1;

            return $result;
        }
        
        $connectedOrders = $this->getConnectedOrdersForCustInvoice($invoiceID);

        $sub_total = 0;
        $tax = 0;
        $total = 0;

        //2. create stripe_item for every item (order) in tbl_cust_invoice_items.
        foreach ($connectedOrders as $order)
        {
        	$this->setStripeIdForConnectedOrder($invoiceID,$order);

            $sub_total += $order['cto_without_vat'];
            $tax += $order['cto_vat'];
            $total += $order['cto_amount'];

        }
        
        //3. create a stripe_invoice from the stripe_items created.
        $this->createStripeInvoice();

        //4. Update the invoice record with the cumulated sub-total, total, tax
        $this->setCustInvoiceTotals($invoiceID,$sub_total,$tax,$total);        

        $this->setCustInvoiceFinalized($invoiceID);

        //5. add finalized history event
        $this->logInvoiceFinalized($invoiceID);

        $result['code'] = 1;

        return $result;

    }

    public function setCustInvoiceFinalized($invoiceID){
        $sql = "UPDATE tbl_cust_invoice SET
                    cin_finalized = 1
                WHERE cin_id = $invoiceID";

        $this->db->execute($sql);
    }

    public function attemptChargeCustInvoice($invoiceID){
        $result = array();
        //1. if custInvoice was NOT finalized - return
        $finalized = $this->checkIfCustInvoiceWasFinalized($invoiceID);

        if(!$finalized){
            $result['code'] = -1;
            $result['message'] = 'not_finalized';
            return $result;
        }

        /**
        //2. stripe charge
        //3. if failed - handleChargeFailureForCustInvoice($invoiceID, $stripeChargeData)
        //4. if succeeded - handleChargeSuccessForCustInvoice($invoiceID, $stripeChargeData)
        */
        $result['code'] = 1;
        $result['message'] = '';
        return $result;
    }

    public function attemptChargeCustInvoiceDirect($invoiceID){
      
        
        $response = $this->stripe_createInvoice($invoiceID);

        if($response['responseStatus'] != 0){
            //created invoice failed
            return resultObject::withData(-1,$response['errorDescription']);
        }              
        
        $response = $this->stripe_chargeInvoice($invoiceID);
        
        if($response['responseStatus'] == 0){
            //charge success
            $this->handleChargeSuccessForCustInvoice($invoiceID);
            return resultObject::withData(1);
        }
        else{
            //charge failed
            $this->handleChargeFailureForCustInvoice($invoiceID);
            $this->stripe_closeInvoice($invoiceID);
            return resultObject::withData(0,$response['errorDescription']);
        }
        
    }

    public function finalizePaymnetMethodCustInvoiceDirect($invoiceID,$paymentMethodID){
        
        
        $response = $this->stripe_createInvoiceWithPaymentMethodID($invoiceID,$paymentMethodID);

        if($response['responseStatus'] != 0){
            //created invoice failed
            return resultObject::withData(-1,$response['errorDescription']);
        }              
        
        $response = $this->stripe_finalizeInvoice($invoiceID);

        $invoice = $response['stripeInvoice'];

        $response['stripePayIntent'] = $this->getStripePaymentIntent($invoice->payment_intent)->data;
        
        if($response['responseStatus'] == 0){
            //charge success
            $this->handleChargeSuccessForCustInvoice($invoiceID);
            return resultObject::withData(1,'',$response);
        }
        else{
            //charge failed
            $this->handleChargeFailureForCustInvoice($invoiceID);
            //$this->stripe_closeInvoice($invoiceID);
            return resultObject::withData(0,$response['errorDescription'],$response);
        }
        
    }

    public function handleChargeFailureForCustInvoice($invoiceID){

        //log failure with relevant payment source
      
    }

    public function handleChargeSuccessForCustInvoice($invoiceID){

        //create customer transaction record
        //log success with relevant customer transaction record
        //change invoice record status.

        $this->markInvoiceAsPaid($invoiceID);
        

        return;
    }

    public function logSuccesfullChargeForCustInvoice($invoiceID, $transactionID,$note = ''){

        //1. get transactionData
        //2. get Payment Source ID from transactionData
        $transactionData = $this->getCustTransactionDataWithTransactionId($transactionID);        
        //insert
        $sql = "INSERT INTO tbl_cust_invoice_history SET
                    cinh_invoice_id = $invoiceID,
                    cinh_event = 'paid',
                    cinh_cust_transaction_id = $transactionID,
                    cinh_cust_payment_source_id = {$transactionData["tr_cust_payment_source_id"]},
                    cinh_note = '$note'";

        $this->db->execute($sql);

        return 1;
    }

    public function logSuccesfullCashTransactionForCustInvoice($invoiceID, $transactionID,$note = ''){

        //1. get transactionData
        //2. get Payment Source ID from transactionData
        $transactionData = $this->getCustTransactionDataWithTransactionId($transactionID);
        //insert
        $sql = "INSERT INTO tbl_cust_invoice_history SET
                    cinh_invoice_id = $invoiceID,
                    cinh_event = 'paid_cash',
                    cinh_cust_transaction_id = $transactionID,
                    cinh_cust_payment_source_id = {$transactionData["tr_cust_payment_source_id"]},
                    cinh_note = '$note'";

        $this->db->execute($sql);

        return 1;
    }

    public function logFailedChargeForCustInvoice($invoiceID, $stripeChargeData){

        //get payment source record from chargeData
        $sql = "INSERT INTO tbl_cust_invoice_history SET
                    cinh_invoice_id = $invoiceID,
                    cinh_event = 'failed',
                    cinh_cust_payment_source_id = {$stripeChargeData['payment_source_id']}";

        $this->db->execute($sql);

        return 1;
    }

    public function logRefundForInvoice($invoiceID){
        $sql = "INSERT INTO tbl_cust_invoice_history SET
                    cinh_invoice_id = $invoiceID,
                    cinh_event = 'refunded'";

        $this->db->execute($sql);

        return 1;
    }

    public function deleteDraftCustInvoice($invoiceID){
        $result = array();
       
        $invoice = $this->getCustInvoiceDataWithInvoiceId($invoiceID);

        if(!isset($invoice['cin_status'])){//no invoice exists
            $result['code'] = -1;
            return $result;
        }

        if($invoice['cin_status'] != 'draft'){// invoice is not 'draft'
            $result['code'] = 0;
            return $result;
        }

        
        $this->disconnectAllOrdersForCustInvoice($invoiceID);

        $sql = "DELETE FROM tbl_cust_invoice WHERE cin_id = $invoiceID";
        $this->db->execute($sql);

        $result['code'] = 1;
        return $result;
        
    }

    public function checkIfCustInvoiceWasFinalized($invoiceID){
        $result = $this->getCustInvoiceDataWithInvoiceId($invoiceID);
        return $result["cin_finalized"] == 1;
    }

    public function logInvoiceCreation($invoiceID){

        $custInvHistory = new customerInvoiceHistoryObject();

        $custInvHistory->cinh_invoice_id = $invoiceID;
        $custInvHistory->cinh_event = 'created';
        
        $this->addCustomerInvoiceHistoryDB($custInvHistory);

        return 1;
    }

    public function logInvoiceFinalized($invoiceID){
        $sql = "INSERT INTO tbl_cust_invoice_history SET
                    cinh_invoice_id = $invoiceID,
                    cinh_event = 'finalized'";

        $this->db->execute($sql);

        return 1;
    }

    public function setStripeIdForConnectedOrder($invoiceID,$order){

        $stripe_item_id = $this->createStripeItem($order);

        $sql = "UPDATE tbl_cust_invoice_items SET
                    cini_stripe_item_id = '$stripe_item_id'
                WHERE cini_invoice_id = $invoiceID
                AND cini_order_id = {$order['cto_id']}";

        $this->db->execute($sql);
            
        return 1;
    }

    public function getCustInvoiceDataWithInvoiceId($invoiceID){
        return $this->db->getRow("SELECT * FROM tbl_cust_invoice WHERE cin_id = $invoiceID");
    }

    function getLocalInvoiceIdByStripeInvoiceId($stripeInvoiceId){

        return $this->db->getVal("SELECT cin_id FROM tbl_cust_invoice WHERE cin_stripe_invoice_id = '$stripeInvoiceId'");

    }

    function getLocalInvoiceIdByStripeChargeId($stripeChargeId){

        return $this->db->getVal("SELECT cin_id FROM tbl_cust_invoice WHERE cin_stripe_charge_id = '$stripeChargeId'");

    }

    public function getTransactionByInvoiceId($invoiceID){
        
        return $this->db->getVal("select tr_id from tbl_cust_transaction where tr_invoice_id=$invoiceID");
    }

    public static function getTransactionsByInvoiceId($invoiceID){
        
        $instance = new self();

        $transactionRows = $instance->db->getTable("select * from tbl_cust_transaction where tr_invoice_id=$invoiceID");

        $result = array();
        foreach ($transactionRows as $transactionRow)
        {
        	$result[] = customerTransactionObject::withData($transactionRow);
        }
        
        return $result;
    }

    public function updateRefundTransaction($transactionID){
        $this->db->execute("update tbl_cust_transaction set
                                tr_date=now(),
                                tr_type='REFUND',
                                tr_amount = 0 - tr_amount
                                where tr_id=$transactionID
                                ");
    }

    public function getCustInvoiceWithUniqueID($invoiceUID){
        $sql = "SELECT * FROM tbl_cust_invoice
                LEFT JOIN tbl_customers ON cust_id = cin_cust_id
                WHERE cin_unique_id = '$invoiceUID'";

        return $this->db->getRow($sql);
    }

    public function getCustomerInvoiceByLongID($longID){
        try{
            $invoiceRow = $this->loadCustomerInvoiceFromDBByLongID($longID);

            $invoice = customerInvoiceObject::withData($invoiceRow);

            return resultObject::withData(1,'',$invoice);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }
    
    public function getConnectedOrdersForCustInvoice($invoiceID){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_transaction_order
                WHERE cini_order_id = cto_id
                AND cini_invoice_id = $invoiceID";

        return $this->db->getTable($sql);
    }

    public function getConnectedOrderIDForCustInvoice($invoiceID){
        return $this->db->getVal("SELECT cini_order_id FROM tbl_cust_invoice_items
                WHERE cini_invoice_id = $invoiceID");
    }

    public function getCustInvoiceHistory($invoiceID){
        $sql = "SELECT * FROM tbl_cust_invoice_history
                WHERE cinh_invoice_id = $invoiceID
                ORDER BY cinh_time DESC";

        return $this->db->getTable($sql);
    }

    public function getCustInvoiceLastChargeAttempt($invoiceID){
        $sql = "SELECT * FROM tbl_cust_invoice_history
                WHERE cinh_invoice_id = $invoiceID
                AND (cinh_event = 'failed' OR cinh_event = 'paid')
                ORDER BY cinh_time DESC
                LIMIT 1";

        return $this->db->getRow($sql);
    }

    public function getCustInvoiceFinalizedHistoryEntry($invoiceID){
        $sql = "SELECT * FROM tbl_cust_invoice_history
                WHERE cinh_invoice_id = $invoiceID
                AND cinh_event = 'finalized'";

        return $this->db->getRow($sql);
    }

    public function setCustInvoiceStatusByUniqueID($invoiceUID,$status,$notes = ''){
        try{
            $sql = "UPDATE tbl_cust_invoice SET
                    cin_status = '$status'
                WHERE cin_unique_id = '$invoiceUID'";

            $this->db->execute($sql);

            $invoice = $this->getCustInvoiceWithUniqueID($invoiceUID);

            $this->addManualCustInvoiceHistoryEntry($invoice['cin_id'],$status,$notes);

            $result = array();

            $result['code'] = 1;
            return $result;
        }
        catch(Exception $ex){
            $result = array();

            $result['code'] = 0;
            return $result;
        }
    }

    public function setCustInvoiceStatusByInvoiceID($invoiceID,$status,$notes = ''){
        try{
            $sql = "UPDATE tbl_cust_invoice SET
                    cin_status = '$status'
                WHERE cin_id = $invoiceID";

            $this->db->execute($sql);

            $this->addManualCustInvoiceHistoryEntry($invoiceID,$status,$notes);

            $result = array();

            $result['code'] = 1;
            return $result;
        }
        catch(Exception $ex){
            $result = array();

            $result['code'] = 0;
            return $result;
        }
    }

    public function setCustInvoicePendingByInvoiceID($invoiceID){
        try{
            $sql = "UPDATE tbl_cust_invoice SET
                    cin_status = 'pending'
                WHERE cin_id = $invoiceID";

            $this->db->execute($sql);

            $this->addManualCustInvoiceHistoryEntry($invoiceID,'finalized');

            $result = array();

            $result['code'] = 1;
            return $result;
        }
        catch(Exception $ex){
            $result = array();

            $result['code'] = 0;
            return $result;
        }
    }

    public function addManualCustInvoiceHistoryEntry($invoiceID,$status,$notes = ''){
        $notes = addslashes($notes);
        $sql = "INSERT INTO tbl_cust_invoice_history SET
                    cinh_invoice_id = $invoiceID,
                    cinh_event = '$status',
                    cinh_note = '$notes'";

        $this->db->execute($sql);

        return 1;
    }

    public function attemptChargeCustInvoiceByUniqueID($invoiceUID){
        $invoice = $this->getCustInvoiceWithUniqueID($invoiceUID);

        $invoiceID = $invoice['cin_id'];

        $response = $this->stripe_chargeInvoice($invoiceID);

        if($response['responseStatus'] == 0){
            //charge success
            $this->handleChargeSuccessForCustInvoice($invoiceID,$response['data']);
            $result['code'] = 1;
            $result['data'] = '';
        }
        else{
            //charge failed
            $this->handleChargeFailureForCustInvoice($invoiceID, $response['data']);
            $result['code'] = 0;
            $result['data'] = $response['errorDescription'];

            $this->stripe_closeInvoice($invoiceID);
        }

        return $result;
    }

    public function refundChargeCustInvoiceByUniqueID($invoiceID){

        $response = $this->stripe_refundInvoice($invoiceID);

        $result = array();
        if($response['responseStatus'] == 0){
            $this->logRefundForInvoice($invoiceID);

            $result['code'] = 1;
            $result['message'] = '';
        }
        else{
            $result['code'] = 0;
            $result['message'] = $response['errorDescription'];
        }

        return $result;
    }

    public function partialRefundCustomerOrder(customerOrderObject $order,$amount = 0){
        try{
            $invoiceRow = $this->getCustomerInvoiceIDForOrderForPartialRefund($order->cto_id,$amount);

            $invoice = customerInvoiceObject::withData($invoiceRow);

            $refundResult =  $this->refundCustomerInvoice($invoice,$amount);

            if($refundResult->code == 1){
                $order->cto_amended_on = date("Y-m-d H:i:s",time());
                customerOrderManager::updateCustomerOrder($order);
            }

            return $refundResult;
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    
    
    public function refundCustomerInvoice(customerInvoiceObject $invoice,$amount = 0){
        //check if invoice wasn;t already refunded beyond the amount
        $refundableAmount = $this->getRefundableAmountForInvoice($invoice);

        if($refundableAmount <= 0 || $refundableAmount < $amount){
            return resultObject::withData(0,'not_enough_left');
        }

        $customerPaymentChargeData = $this->loadCustomerPaymentChargeByInvoiceIdFromDB($invoice->cin_id);        
        
        $transactionOrder = customerTransactionObject::withData($customerPaymentChargeData);   

        //create invoice
        $invoiceResult = $this->initiateCustomerInvoice($invoice->cin_cust_id);

        if($invoiceResult->code == 0){
            return resultObject::withData(-1,'no_invoice_created');
        }

        $invoiceID = $invoiceResult->data;

        $refundInvoiceRow = $this->loadCustomerInvoiceFromDB($invoiceID);

        $refundInvoice = customerInvoiceObject::withData($refundInvoiceRow);

        $refundInvoice->cin_is_credit_note = 1;
        $refundInvoice->cin_status = 'refunded';
        $refundInvoice->cin_parent_id = $invoice->cin_id;

        $transactionOrder->tr_invoice_id = $invoiceID;
        $transactionOrder->tr_type = "REFUND";

        if($amount > 0){//refund for given amount
            $transactionOrder->tr_amount = $amount;
            $refundInvoice->cin_sub_total = $amount;
            $refundInvoice->cin_total = $amount;

        }else{//refund for full amount of invoice
            $refundInvoice->cin_total = $invoice->cin_total;
            $refundInvoice->cin_sub_total = $invoice->cin_total;
            $transactionOrder->tr_amount = $invoice->cin_total;
            $amount = $invoice->cin_total;
        }

        //refund with stripe
        if($transactionOrder->tr_paymentMethodType == "CreditCard" && $transactionOrder->tr_paymentMethodToken == "stripe"){
            $striperefundResult = $this->stripe_partialRefundTransaction($transactionOrder,$amount);

            if($striperefundResult->code != 1){
                return resultObject::withData(0,"stripe_refund_rejected",$striperefundResult->data);
            }

            $refundData = $striperefundResult->data;

            $transactionOrder->tr_reconciliation_Id = $refundData["id"];
            $refundInvoice->cin_stripe_charge_id = $refundData["id"];
        }

        $this->updateCustomerInvoiceDB($refundInvoice);
        $this->addCustomerPaymentChargeDB($transactionOrder);

        $orderID = $this->getConnectedOrderIDForCustInvoice($invoice->cin_id);

        $this->connectCustInvoiceToCustOrderDirect($orderID,$refundInvoice->cin_id);

        return resultObject::withData(1);
    }

    function getRefundableAmountForInvoice(customerInvoiceObject $invoice){
        $refundedAmount = $this->db->getVal("SELECT SUM(cin_total) FROM tbl_cust_invoice
                WHERE cin_parent_id = {$invoice->cin_id}
                AND cin_is_credit_note = 1");

        return $invoice->cin_total - $refundedAmount;
    }


    function getCustomerOwnerStripeAccountId($bizId){
        
        $data = bizManager::getStripeAccountForBiz($bizId);

        if($data['code'] == 0){
            return "";
        }
        return $data["data"]["stripe_account_id"];
    }

    function markInvoiceAsPaid($invoiceId){
        $sql = "UPDATE tbl_cust_invoice SET cin_status='paid' WHERE cin_id=$invoiceId";
        $this->db->execute($sql);
    }

    function markCustomerInvoicePaidFromPaymentIntent($invoiceID,$payIntent,$deviceID = ''){
        try{
            $this->markInvoiceAsFinalized($invoiceID);

            $invoiceRow = $this->loadCustomerInvoiceFromDB($invoiceID);

            $invoice = customerInvoiceObject::withData($invoiceRow);

            $invoice->cin_stripe_payintent_id = $payIntent->id;
            if($payIntent->charges->total_count > 0){
                $invoice->cin_stripe_charge_id = $payIntent->charges->data[0]->id;
            }
            
            $this->updateCustomerInvoiceDB($invoice);

            $this->addTransactionCustomer($invoiceID,"","",$payIntent->charges->data[0]->id,"CHARGE",$deviceID);

            $this->markInvoiceAsPaid($invoiceID);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    function markCustomerInvoicePaidFromIsracard($invoiceID,$payIntent,$deviceID){
        try{
            $this->markInvoiceAsFinalized($invoiceID);

            $invoiceRow = $this->loadCustomerInvoiceFromDB($invoiceID);

            $invoice = customerInvoiceObject::withData($invoiceRow);

            $invoice->cin_stripe_payintent_id = $payIntent["payme_sale_id"];
            $invoice->cin_stripe_charge_id = $payIntent["payme_transaction_id"];
            
            $this->updateCustomerInvoiceDB($invoice);

            $this->addTransactionCustomerIsracard($invoiceID,"","",$payIntent,"CHARGE",$deviceID);

            $this->markInvoiceAsPaid($invoiceID);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage());
        }
    }

    function markInvoiceAsFinalized($invoiceId){
        $sql = "UPDATE tbl_cust_invoice SET cin_status='pending',cin_finalized=1 WHERE cin_id=$invoiceId";
        $this->db->execute($sql);

        $this->logInvoiceFinalized($invoiceId);
    }

    function markInvoiceAsRefunded($invoiceId){
        $sql = "UPDATE tbl_cust_invoice SET cin_status='refunded' WHERE cin_id=$invoiceId";
        $this->db->execute($sql);
    }

    function markInvoiceAsVoided($invoiceId){
        $sql = "UPDATE tbl_cust_invoice SET cin_status='voided' WHERE cin_id=$invoiceId";
        $this->db->execute($sql);
    }

    function addTransactionCustomer($invoiceId,$processorReferenceId,$captureCode,$reconciliationId,$type,$deviceID = ''){

        $chargeData = $this->stripe_retreiveChargeResultForInvoice($invoiceId);
        $last4 = (isset($chargeData["source"]["last4"])) ? $chargeData["source"]["last4"] : "";
        $vendor = (isset($chargeData["source"]["brand"])) ? $chargeData["source"]["brand"] : "";
        
        $this->executeTransactionCustomer($invoiceId,$processorReferenceId,$captureCode,$reconciliationId,$type,$deviceID,$last4,$vendor);
    }

    function addTransactionCustomerIsracard($invoiceId,$processorReferenceId,$captureCode,$payIntent,$type,$deviceID = ''){

        $last4 = substr($payIntent["buyer_card_mask"], -4);
        $vendor = $payIntent["payme_transaction_card_brand"];
        
        $this->executeTransactionCustomer($invoiceId,$processorReferenceId,$captureCode,$payIntent["payme_transaction_id"],$type,$deviceID,$last4,$vendor);
    }

    function executeTransactionCustomer($invoiceId,$processorReferenceId,$captureCode,$reconciliationId,$type,$deviceID = '',$last4 = '',$vendor = ''){

        $bizRow = $this->db->getRow("SELECT * FROM tbl_biz,tbl_cust_invoice
                                      WHERE biz_id = cin_biz_id
                                      AND cin_id=$invoiceId");

        $transID = $this->db->execute("insert into tbl_cust_transaction set
                           tr_invoice_id = $invoiceId,
                           tr_biz_id = {$bizRow["biz_id"]},
                           tr_device_id = '$deviceID',
                           tr_reference_Id = '$processorReferenceId',
                           tr_capture_code = '$captureCode',
                           tr_reconciliation_Id = '$reconciliationId',
                           tr_type = '$type',
                           tr_amount={$bizRow["cin_total"]},
                           tr_currency='{$bizRow["cin_currency"]}',
                           tr_paymentMethodToken='{$bizRow["biz_paymentMethodToken"]}',
                           tr_paymentMethodType = '{$bizRow["biz_paymentMethodType"]}',
                           tr_cvv = '{$bizRow["biz_cvv"]}',
                           tr_paymentMethodLastDigits = '$last4',
                           tr_vendor = '$vendor',
                           tr_country = '{$bizRow["biz_addr_country_id"]}',
                           tr_phone = '{$bizRow["biz_mobile_tele"]}'
                          ");
        
        $this->db->execute("UPDATE tbl_cust_invoice_items SET
                                cini_tr_id=$transID
                                WHERE cini_invoice_id=$invoiceId
                                ");
        
        $this->logSuccesfullChargeForCustInvoice($invoiceId,$transID);

    }

    function settleInvoiceWithCashTransaction($invoiceId,customerTransactionObject $transactionObject){
        try{
            
            $transID = $this->addCustomerPaymentChargeDB($transactionObject);
            
            $this->db->execute("UPDATE tbl_cust_invoice_items SET
                                cini_tr_id=$transID
                                WHERE cini_invoice_id={$transactionObject->tr_invoice_id}
                                ");

            $this->markInvoiceAsPaid($transactionObject->tr_invoice_id);
            
            $this->logSuccesfullCashTransactionForCustInvoice($transactionObject->tr_invoice_id,$transID);
            
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($transactionObject));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    function addDirectSuccesfulCustomerTransaction(customerTransactionObject $transactionObject){
        try{
            
            $transID = $this->addCustomerPaymentChargeDB($transactionObject);
            
            $this->db->execute("UPDATE tbl_cust_invoice_items SET
                                cini_tr_id=$transID
                                WHERE cini_invoice_id={$transactionObject->tr_invoice_id}
                                ");

            $this->markInvoiceAsPaid($transactionObject->tr_invoice_id);
            
            $this->logSuccesfullChargeForCustInvoice($transactionObject->tr_invoice_id,$transID);
            
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($transactionObject));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function checkIfCreditTokenExistsForCustomer($cust_id,$token){
        $sql = "SELECT * FROM tbl_customer_payment_sources 
                WHERE cps_token = '$token'
                AND cps_cust_id = $cust_id";
        
        $source = $this->db->getRow($sql);

      
        $code = isset($source['cps_id']) ? 1 : 0;
        $data = isset($source['cps_id']) ? $source['cps_id'] : 0;

        return resultObject::withData($code,"",$data);
    }

    public function addCreditCardToCustomerPaymentSources(customerObject $customer,$cardData,$token,$ipAddress,$phone){

        $sql = "INSERT INTO tbl_customer_payment_sources SET
                      cps_cust_id = {$customer->cust_id},
                      cps_type = '{$cardData["funding"]}',
                      cps_ip = '$ipAddress',
                      cps_token = '$token',
                      cps_stipe_object_id = '{$cardData["id"]}',
                      cps_card_brand = '{$cardData["brand"]}',
                      cps_country = '{$cardData["country"]}',
                      cps_exp_month = {$cardData["exp_month"]},
                      cps_exp_year = {$cardData["exp_year"]},
                      cps_funding = '{$cardData["funding"]}',
                      cps_last4 = '{$cardData["last4"]}',
                      cps_name = '{$cardData["name"]}',
                      cps_phone = '$phone'";

        $newPC = $this->db->execute($sql);

        $this->setCustomerDefaultPaymentSource($customer->cust_id,$newPC);
        $this->stripe_setCardAsDefaultForCustomer($customer,$cardData["id"]);
        return $newPC;
    }

    public function setCustomerDefaultPaymentSource($cust_id,$pay_source_id){
        $sql = "UPDATE tbl_customer_payment_sources SET
                    cps_default = CASE WHEN cps_id = $pay_source_id THEN 1 ELSE 0 END
                WHERE cps_cust_id = $cust_id";

        $this->db->execute($sql);

        return;
    }

    public function setCustomerDefaultPaymentEntryByID(customerObject $customer,$pay_source_id){
        $pay_source = $this->getCustomerPaymentSourceByID($customer->cust_id,$pay_source_id);

        $this->setCustomerDefaultPaymentSource($customer->cust_id,$pay_source_id);
        $this->stripe_setCardAsDefaultForCustomer($customer,$pay_source['cps_stipe_object_id']);

        return;
    }

    public function getCustomerPaymentSourceByID($cust_id,$pay_source_id){
        $sql = "SELECT * FROM tbl_customer_payment_sources
                WHERE cps_cust_id = $cust_id
                AND cps_id = $pay_source_id";

        return $this->db->getRow($sql);
    }

    public function getCustomerDefaultPaymentSource($cust_id){
        try{
            $paySourceRow = $this->getCustomerDefaultPaymentSourceRow($cust_id);
            if(!isset($paySourceRow['cps_id'])){
                return resultObject::withData(0,"no_pay_source");
            }
            $paySource = defaultPaySrcObject::withData($paySourceRow);

            return resultObject::withData(1,"",$paySource);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getCustomerPaymentSources($cust_id){
       try{
           $paySourceRows = $this->getCustomerPaymentSourcesRows($cust_id);

           $result = array();
           foreach ($paySourceRows as $paySourceRow)
           {
               $result[] = defaultPaySrcObject::withData($paySourceRow);
           }
           
           return resultObject::withData(1,"",$result);
       }
       catch(Exception $e){
           errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
           $result = resultObject::withData(0,$e->getMessage());
           return $result;
       }
    }

    public function getTransactionForOrderByOrderID($orderID){
        try{
            $transData = $this->getTransactionForOrderByOrderIDDB($orderID);

            $transaction = customerTransactionObject::withData($transData);

            return resultObject::withData(1,'',$transaction);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getTransactionForInvoiceByInvoiceID($invoiceID){
        try{
            $transData = $this->getTransactionByInvoiceId($invoiceID);

            $transaction = customerTransactionObject::withData($transData);

            return resultObject::withData(1,'',$transaction);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$invoiceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getInvoiceForOrderByOrderID($orderID){
        try{
            $invoiceRow = $this->loadCustomerInvoiceForOrder($orderID);

            return resultObject::withData(1,'',customerInvoiceObject::withData($invoiceRow));
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getInvoicesListForOrderByOrderID($orderID){
        try{
            $instance = new self();
            $invoicesRow = $instance->loadCustomerInvoicesListForOrder($orderID);

            $result = array();
            foreach ($invoicesRow as $invoiceRow)
            {
            	$result[] = customerInvoiceObject::withData($invoiceRow);
            }            

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    private function setStripeConnectAccountByInvoiceId($invoiceId){
        
        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($invoiceId);
        
        
        $this->stripeConnectAcount = $this->getCustomerOwnerStripeAccountId($invoiceRow["cin_biz_id"]);
    }

    private function getCustomerDefaultPaymentSourceRow($cust_id){
        $sql = "SELECT * FROM tbl_customer_payment_sources
                WHERE cps_cust_id = $cust_id
                AND cps_default = 1";

        return $this->db->getRow($sql);
    }

    private function getCustomerPaymentSourcesRows($cust_id){
        $sql = "SELECT * FROM tbl_customer_payment_sources
                WHERE cps_cust_id = $cust_id";

        return $this->db->getTable($sql);
    }

    public function sendCustomerInvoicePDFEmail($longInvoiceID,$email){
        try{
            $invoiceRow = $this->loadCustomerInvoiceFromDBByLongID($longInvoiceID);

            $invoice = customerInvoiceObject::withData($invoiceRow);

            $pathResult = $this->getInvoicePDFLink($invoice);

            return $pathResult;           
        }
        catch(Exception $e){
            $data = array();
            $data['invoiceID'] = $longInvoiceID;
            $data['email'] = $email;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    private function getInvoicePDFLink(customerInvoiceObject $invoice){
        $image_storage = imageStorage::getStorageAdapter();

        $fname = "invoice_".$invoice->cin_long_id.".pdf";

        if($image_storage->exists($invoice->cin_biz_id."/cust_invoices/".$fname)){
            return resultObject::withData(1,'',IMAGE_STORAGE_PATH.$invoice->cin_biz_id."/cust_invoices/".$fname);        
        }

        $orders = $this->getConnectedOrdersForCustInvoice($invoice->cin_id);

        $bizModel = new bizModel($invoice->cin_biz_id);
        $bizResult = $bizModel->getBiz();

        $biz = new bizObject();//remove this
        if($bizResult->code != 1){
            return resultObject::withData(0,"no_biz");
        }

        $customerModel = new customerModel();
        $customerResult = $customerModel->getCustomerWithID($invoice->cin_cust_id);

        $customer = new customerObject();//remove this
        if($customerResult->code != 1){
            return resultObject::withData(0,"no_customer");
        }

        //$biz = $bizResult->data;
        //$customer = $customerResult->data;

        $countryData = utilityManager::getCountry($biz->biz_addr_country_id);

        $country = $countryData['country_name_eng'];

        $state = '';

        if($biz->biz_addr_country_id == 1 && $biz->biz_addr_state_id != 0){
            $stateData = utilityManager::getState($biz->biz_addr_state_id);
            $state = $stateData['state_code'];
        }

        

        $storeSetting = bizManager::getBizStoreObjectForBiz($invoice->cin_biz_id);

        $currencySymbol = $storeSetting->ess_currency_symbol;

        $currencySymbol = '<span style="font-family:Arial;">'.$currencySymbol.'</span>';

        $ordersHtml = "";

        
        $orderCountry = '';
        $orderState = '';

        $issueDate = utilityManager::getFormattedTimeByCountryID($biz->biz_addr_country_id,$invoice->cin_create_date);
        $firstOrder = array();
        if(count($orders) > 0){
            $firstOrder = $orders[0];
            $orderCountryData = utilityManager::getCountry($firstOrder['cto_addr_country']);

            $orderCountry = $orderCountryData['country_name_eng'];

            
            $orderStateData = utilityManager::getCountry($firstOrder['cto_addr_state']);

            $orderState = $orderStateData['state_code'];
        }

        foreach ($orders as $order)
        {
            $subtotal = $order['cto_without_vat'] + $order['cto_sipping'] - $order['cto_membership_discount'] - $order['cto_benefit_value'];
            $orderRow = "<div class='invoice-order'>
                <div class='order-title'>
                    Order No. {$order['cto_long_order_id']}
                </div>
                <div class='invoice-data-row'>
                    <div class='order-row-label'>Subtotal:</div>
                    <div class='order-row-data'>$currencySymbol$subtotal</div>
                </div>
                <div class='invoice-data-row'>
                    <div class='order-row-label'>Tax:</div>
                    <div class='order-row-data'>$currencySymbol{$order['cto_vat']}</div>
                </div>
                <div class='invoice-data-row'>
                    <div class='order-row-label'>Order Total:</div>
                    <div class='order-row-data'>$currencySymbol{$order['cto_amount']}</div>
                </div>
            </div>";
            $ordersHtml .= $orderRow;
        }

        $invoiceHtml = "<html >
                         <head> 
                          <meta charset=\'UTF-8\'> 
                          
                          <meta name=\'x-apple-disable-message-reformatting\'> 
                          <meta http-equiv=\'X-UA-Compatible\' content=\'IE=edge\'> 
                          <meta content=\'telephone=no\' name=\'format-detection\'> 
                          <title>PDF Invoice</title> 
                          <!--[if (mso 16)]>
                            <style type=\'text/css\'>
                            a {text-decoration: none;}
                            </style>
                            <![endif]--> 
                          <!--[if gte mso 9]><style>sup { font-size: 100% !important; }</style><![endif]--> 
                          <!--[if !mso]><!-- --> 
                          <link href=\'https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700,700i\' rel=\'stylesheet\'> 
                          <!--<![endif]-->
                        <style>
                            body{
                                font-family: 'OpenSans';
                                width:1200px;
                            }
    
                            .invoice-header{
                                background-color: #f6f6f8;
                                padding:55px 40px 55px 50px;
                                display: inline-block;
                                width:1200px;
                            }
    
                            .biz-name{
                                float:left;
                                font-size: 36px;
                                font-weight: bold;
                                width: 50%;
                                line-height:36px;
                            }
    
                            .biz-info{
                                float:right;
                                text-align: right;
                                width: 50%;
                            }
    
                            .biz-info-row{
                                font-size: 18px;
                                line-height: 1.5;
                            }
    
                            .bold-text{
                                font-weight:bold;
                            }
    
                            .invoice-info{
                                width:100%;
                                margin-bottom: 113px;
                            }
    
                            .invoice-body{
                               padding:40px 40px 55px 50px; 
                               width:100%;    
                            }
    
                            .invoice-info-row{
                                width:100%;
                                display:inline-block;
                                margin-bottom: 24px;
                                text-align:left;
                                font-size: 16px;
                            }
    
                            .invoice-info-label{
                                font-weight: 600;
                                text-transform: uppercase;
                                width:155px;
                                float:left;
                            }

                            .invoice-info-text{
                                width:255px;
                                text-align:left;
                                float:left;
                            }
    
                            .invoice-data-row{
                                background-color: #f6f6f8;
                                display: inline-block;
                                width: 800px;
                                line-height: 38px;
                                margin-bottom: 5px;
                                padding: 0 21px 0px 15px;
                                box-sizing: border-box;
                            }

                            .invoice-orders{
                                width:100%;
                            }
    
                            .invoice-order{
                                font-size: 14px;
                                margin-bottom: 49px;
                                width:100%;
                            }
    
                            .order-title{
                                font-weight: bold;
                                margin-bottom: 14px;
                                width:100%;
                            }
    
                            .order-row-label{
                                float:left;
                                text-align:left;
                                width:50%;
                            }
    
                            .order-row-data{
                                float:left;
                                text-align:right;
                                width:50%;
                            }
    
                            .invoice-total{
                                margin-top: 7px;
        
                            }
    
                            .invoice-total .invoice-data-row{
                                line-height: 81px;
                                padding-left: 53%;
                            }
    
                            .invoice-total .order-row-label{
                                font-size: 24px;
                            }
    
                            .invoice-total .order-row-data{
                                font-size: 36px;
                            }
                        </style>
                        </head>
                        <body style=\"width:100%;font-family:'open sans';margin:0;background-color:#fff;\">
                            <div style=\"width:100%;margin:0\">
                                <div class=\"invoice-header\">
                                    <div  class=\"biz-name\">
                                        {$biz->biz_short_name}
                                    </div>  
                                    <div class=\"biz-info\">
                                        <div class='biz-info-row bold-text'>
                                            {$biz->biz_addr_no} {$biz->biz_addr_street}
                                        </div>
                                        <div class='biz-info-row bold-text'>
                                            {$biz->biz_addr_town} $state $country
                                        </div>
                                        <div class='biz-info-row'>
                                            {$biz->biz_e_mail}
                                        </div>
                                         <div class='biz-info-row'>
                                            {$biz->biz_office_tele}
                                        </div>
                                        <div class='biz-info-row'>
                                            {$biz->biz_website}
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class='invoice-body'>
                                    <div class='invoice-info'>
                                        <div class='invoice-info-row'>
                                            <div class='invoice-info-label'>Invoice #:</div>
                                            <div class='invoice-info-text'>{$invoice->cin_long_id}</div>
                                        </div>
                                        <div class='invoice-info-row'>
                                            <div class='invoice-info-label'>Billed to:</div>
                                            <div class='invoice-info-text'>
                                                <div>{$customer->cust_first_name} {$customer->cust_phone1} </div>
                                                <div>{$firstOrder['cto_addr_line1']} {$firstOrder['cto_addr_line2']}</div>
                                                <div>{$firstOrder['cto_addr_city']} $orderState $orderCountry </div>
                                                <div>{$firstOrder['cto_addr_postcode']}</div>
                                            </div>
                                        </div>
                                        <div class='invoice-info-row'>
                                            <div class='invoice-info-label'>Date of issue:</div>
                                            <div class='invoice-info-text'>$issueDate</div>
                                        </div>
                                    </div>
                                    <div class='invoice-orders'>
                                        $ordersHtml
                                    </div>
                                    <div class='invoice-total'>
                                        <div class='invoice-data-row'>
                                            <div class='order-row-label'>Total:</div>
                                            <div class='order-row-data'>$currencySymbol{$invoice->cin_total}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </body>
                        </html>";

        $mpdf = new mPDF();

        $mpdf->WriteHTML($invoiceHtml);
        $mpdf->Output($fname);
        
        


        $path = $image_storage->uploadFile(ROOT_PATH.$fname,$invoice->cin_biz_id."/cust_invoices/".$fname);

        unlink(ROOT_PATH.$fname);

        return resultObject::withData(1,'',$path);
    }

    /* ----------------------------             UTILITY METHODS                                   ------------------------------------------------ */

    public function getCustTransactionDataWithTransactionId($transactionID){
        return $this->db->getRow("SELECT * FROM tbl_cust_transaction WHERE tr_id = $transactionID");
    }

    public function getInvoicePDFByUniqueID($invoice_unique_id){
        $invoiceRow = $this->getCustInvoiceWithUniqueID($invoice_unique_id);

        $invoice = customerInvoiceObject::withData($invoiceRow);

        $pathResult = $this->getInvoicePDFLink($invoice);

        return $pathResult;
    }

    /* ----------------------------             ISRACARD METHODS FOR CUSTOMERS BILLING              ------------------------------------------------ */
    
    public function generateIsracardSale(customerObject $customer,customerOrderObject $orderObject, $paymentMethodId, $invoiceId){
        try{

            $data["seller_payme_id"] = isracardConnectManager::getApiKeyForBiz($customer->cust_biz_id);
            $data["sale_price"] = $orderObject->cto_amount * 100;
            $data["currency"] = $orderObject->cto_currency;
            $data["product_name"] = "Mobile Purchase";
            $data["transaction_id"] = $invoiceId;
            $data["installments"] = 1;
            $data["capture_buyer"] = 0;
            $data["buyer_key"] = $paymentMethodId;

            return isracardConnectManager::generateSale($data);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(-1,$e->getMessage());
            return $result;
        }
    }


    /* ----------------------------             STRIPE METHODS FOR CUSTOMERS BILLING              ------------------------------------------------ */

    /* ------ CUSTOMER ------ */
    public function addCustomerToStripe(customerObject $customer){
        try{
            if($customer->cust_stripe_cust_id != ""){
                return resultObject::withData(1,'',$customer->cust_stripe_cust_id);
            }

            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);
            
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                $data = array();
                if($customer->cust_email != ""){
                    $data["email"] = $customer->cust_email;
                }
                $data["description"] = $customer->cust_id;
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                    $responseData = \Stripe\Customer::create($data,["stripe_account" => $result["data"]["stripe_account_id"]]);
                    
                    return resultObject::withData(1,"",$responseData["id"]);
                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                   
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function stripe_createCustomer($customerId){


        $customerData = customersManager::getCustomerDetailsStatic($customerId);

        if($customerData["cust_stripe_cust_id"] == ""){
            
            if($customerData["cust_biz_id"] == ""){
                $response["code"] = -4;
                $response["message"] = "Wrong customer id";
                $response["custStripeId"] = "";
            }else{
                $result = bizManager::getStripeAccountForBiz($customerData["cust_biz_id"]);
               
                if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                    $data = array();
                    if($customerData["cust_email"] != ""){
                        $data["email"] = $customerData["cust_email"];
                    }
                    $data["description"] = $customerId;
                    
                    if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    try {

                        $responseData = \Stripe\Customer::create($data,["stripe_account" => $result["data"]["stripe_account_id"]]);
                        
                        $response["code"] = 1;
                        $response["message"] = "";
                        $response["custStripeId"] = $responseData["id"];
                        $this->db->execute("UPDATE tbl_customers SET cust_stripe_cust_id='{$responseData["id"]}' WHERE cust_id=$customerId");

                    }
                    catch (\Stripe\Error\Base $e) {
                        $response["code"] = -1;
                        $response["message"] =$e->getMessage();
                    }
                    catch (Exception $e) {
                        $response["code"] = -2;
                        $response["message"] =$e->getMessage();
                    }
                }else{
                    $response["code"] = -3;
                    $response["message"] = "Missing stripe account";
                    $response["custStripeId"] = "";
                }
            }

        }else{
            $response["code"] = 1;
            $response["message"] = "";
            $response["custStripeId"] = $customerData["cust_stripe_cust_id"];
        }
        return $response;
    }

    public function stripe_retreiveCustomer($customerId){

        $customerData = customersManager::getCustomerDetailsStatic($customerId);
        $stripeCustomerId = $customerData["cust_stripe_cust_id"];

        $response = array();
        $response["code"] = -3;
        $response["message"] = "Missing stripe account";
        $response["data"] = array();

        if($customerData["cust_stripe_cust_id"] != ""){
            
            $result = bizManager::getStripeAccountForBiz($customerData["cust_biz_id"]);
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){

                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                    $customerData = \Stripe\Customer::retrieve($stripeCustomerId,["stripe_account" => $result["data"]["stripe_account_id"]]);
                    $response["code"] = 1;
                    $response["message"] = "";
                    $response["data"] = $customerData;
                }
                catch (\Stripe\Error\Base $e) {
                    $response["code"] = -1;
                    $response["message"] =$e->getMessage();
                }
                catch (Exception $e) {
                    $response["code"] = -2;
                    $response["message"] =$e->getMessage();
                }
            }
        }

        return $response;
    }

    public function createPaymentIntent(customerObject $customer,customerOrderObject $orderObject, $paymentMethodId){
        try{
           
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);
            
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
               
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }
                \Stripe\Stripe::setApiVersion(getenv('STRIPE_API_VERSION'));
                try {

                    # Create the PaymentIntent
                    $intent = \Stripe\PaymentIntent::create([
                      'payment_method' => $paymentMethodId,
                      'amount' => $orderObject->cto_amount * 100,
                      'currency' => $orderObject->cto_currency,
                      'confirmation_method' => 'manual',
                      'confirm' => false,
                      'customer' => $customer->cust_stripe_cust_id
                    ],["stripe_account" => $result["data"]["stripe_account_id"]]);

                    $response["clientSecret"] = $intent->client_secret;
                    $response["intentId"] = $intent->id;

                    if (($intent->status == 'requires_source_action' || $intent->status == 'requires_action') && $intent->next_action->type == 'use_stripe_sdk') {
                        # Tell the client to handle the action
                        return resultObject::withData(0,"",$response);
                    } else if ($intent->status == 'succeeded' || $intent->status == 'requires_confirmation') {
                        # continue with shoping
                        return resultObject::withData(1,"",$response);
                    } else {
                        # Invalid status
                        return resultObject::withData(-4,"Invalid PaymentIntent status ".$intent->status);
                    }

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function creatOnlyPaymentIntent(customerObject $customer,customerOrderObject $orderObject, $paymentMethodId,$recurring = false){
        try{
            
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                $future_usage = $recurring ? 'off_session' : 'on_session';

                try {

                    # Create the PaymentIntent
                    $intent = \Stripe\PaymentIntent::create([
                      'payment_method' => $paymentMethodId,
                      'amount' => $orderObject->cto_amount * 100,
                      'currency' => $orderObject->cto_currency,
                      'confirmation_method' => 'manual',
                      'confirm' => false,
                      'customer' => $customer->cust_stripe_cust_id,
                      'setup_future_usage' => $future_usage
                    ],["stripe_account" => $result["data"]["stripe_account_id"]]);

                    

                    if (isset($intent->id)){
                        return resultObject::withData(1,"",$intent);
                    } else {
                        # Invalid status
                        return resultObject::withData(-4,"Invalid PaymentIntent status ".$intent->status);
                    }

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function validatePaymentIntent(customerObject $customer, $intentId){
        try{
            
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                    $intent = \Stripe\PaymentIntent::retrieve(
                        $intentId,["stripe_account" => $result["data"]["stripe_account_id"]]
                      );
                    $intent->confirm();

                    if ($intent->status == 'requires_source_action' && $intent->next_action->type == 'use_stripe_sdk') {
                        # Tell the client to handle the action
                        $response["clientSecret"] = $intent->client_secret;
                        $response["intentId"] = $intent->id;
                        return resultObject::withData(0,"",$response);
                    } else if ($intent->status == 'succeeded') {
                        return resultObject::withData(1,"");
                    } else {
                        # Invalid status
                        return resultObject::withData(-4,"Invalid PaymentIntent status");
                    }

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getStripePaymentIntent($intentId){
        try{
            
            $result = bizManager::getStripeAccountForBiz($this->bizID);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                   

                    $intent = \Stripe\PaymentIntent::retrieve(
                        $intentId,["stripe_account" => $result["data"]["stripe_account_id"]]
                      );



                    return resultObject::withData(1,"",$intent);

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function updateStripePaymentMethodToRecurring($intentID){
        try{

            $result = bizManager::getStripeAccountForBiz($this->bizID);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {
                    $intent = \Stripe\PaymentIntent::update(
                           $intentID,["setup_future_usage" => 'off_session'],["stripe_account" => $result["data"]["stripe_account_id"]]
                         );
                    
                    return resultObject::withData(1,"",$intent);

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($payIntent));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function confirmPaymentIntent($intent){
        try{
            try {

                if ($intent->status == 'succeeded') {
                    return resultObject::withData(1,"");
                }
                
                $intent->confirm();

                if (($intent->status == 'requires_source_action' || $intent->status == 'requires_action') && $intent->next_action->type == 'use_stripe_sdk') {
                    # Tell the client to handle the action
                    $response["clientSecret"] = $intent->client_secret;
                    $response["intentId"] = $intent->id;
                    return resultObject::withData(0,"",$response);
                } else if ($intent->status == 'succeeded') {
                    return resultObject::withData(1,"");
                } else {
                    # Invalid status
                    return resultObject::withData(-4,"Invalid PaymentIntent status");
                }

            }
            catch (\Stripe\Error\Base $e) {
            
                return resultObject::withData(-1,$e->getMessage());
            }
            catch (Exception $e) {
            
                return resultObject::withData(-2,$e->getMessage());
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($intent));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function getPaymentMethodDetails(customerObject $customer,$payMethodID){
        try{
            
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                    $payMethod = \Stripe\PaymentMethod::retrieve(
                        $payMethodID,["stripe_account" => $result["data"]["stripe_account_id"]]
                      );

                    return resultObject::withData(1,"",$payMethod);

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            $data = array();
            $data['pay_method_id'] =$payMethodID;
            $data['customer'] = $customer;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function attachIsracardPaymentMethodToCustomer(customerObject $customer,$postData){
        try{

            $custPaySource = customerPaymentSourceObject::fromIsracardPost($customer,$postData);
            
            $cpsResult = $this->addCustomerPaymentSource($custPaySource);

            if($cpsResult->code == 1){//set new customer payment source as default
                $this->setCustomerDefaultPaymentSource($customer->cust_id,$cpsResult->data);
            }


            return resultObject::withData(1);

        }
        catch(Exception $e){
            $data = array();
            
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function attachPaymentMethodToCustomer(customerObject $customer,$payMethod){
        try{
            
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){
                
                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {
                    if($payMethod->customer == $customer->cust_stripe_cust_id){
                        return resultObject::withData(1,"",$payMethod);
                    }

                    $payMethod->attach(['customer' => $customer->cust_stripe_cust_id]);

                    $custPaySource = customerPaymentSourceObject::fromStripePaymentMethod($customer,$payMethod);
                    
                    $cpsResult = $this->addCustomerPaymentSource($custPaySource);

                    if($cpsResult->code == 1){//set new customer payment source as default
                        $this->setCustomerDefaultPaymentSource($customer->cust_id,$cpsResult->data);
                    }


                    return resultObject::withData(1,"",$payMethod);

                }
                catch (\Stripe\Error\Base $e) {
                    
                    return resultObject::withData(-1,$e->getMessage());
                }
                catch (Exception $e) {
                    
                    return resultObject::withData(-2,$e->getMessage());
                }
            }else{                
                return resultObject::withData(-3,"Missing stripe account","");
            }
        }
        catch(Exception $e){
            $data = array();
            
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /* ------ CREDIT CARDS ------ */
    public function addCreditCardtoStripeCustomer(customerObject $customer,$token,$ipAddress,$phone){
        try{
            $existingSource = $this->checkIfCreditTokenExistsForCustomer($customer->cust_id,$token);

            if($existingSource->code == 1){
                $this->setCustomerDefaultPaymentEntryByID($customer,$existingSource->data);
                return resultObject::withData(1);
            }

            $cardAddResult = $this->stripe_addCardToCustomer($customer,$token);

            if($cardAddResult->code == 1){
                $this->addCreditCardToCustomerPaymentSources($customer,$cardAddResult->data,$token,$ipAddress,$phone);
                return resultObject::withData(1);
            }
            else{
                return resultObject::withData(0,$cardAddResult->message);
            }
        }
        catch(Exception $e){
            $data = array();
            $data['customer'] = $customer;
            $data['token'] = $token;
            $data['ip'] = $ipAddress;
            $data['phone'] = $phone;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public function stripe_addCardToCustomer(customerObject $customerObj,$token){
                
        $stripeCustomerId = $customerObj->cust_stripe_cust_id;

        $response = array();
        $response["code"] = -3;
        $response["message"] = "Missing stripe account";
        $response["data"] = array();
        
        if($stripeCustomerId != ""){
            
            $result = bizManager::getStripeAccountForBiz($customerObj->cust_biz_id);
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){

                try{

                    if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $customer = \Stripe\Customer::retrieve($stripeCustomerId,["stripe_account" => $result["data"]["stripe_account_id"]]);
                    
                    $data = $customer->sources->create(["source" => $token]);
                    return resultObject::withData(1,"success",$data);
                }
                catch(Exception $e){
                    $data = array();
                    $data['customer'] = $customerObj;
                    $data['token'] = $token;
                    errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
                    $result = resultObject::withData(-1,$e->getMessage());
                    return $result;
                } 
            }
            else{
                return resultObject::withData(-3,"Missing stripe account",array());
            }
        }
        else{
            return resultObject::withData(-3,"Missing stripe account",array());
        }

    }

    public function stripe_removeCardFromCustomer($customerId,$cardId){

        $customerData = customersManager::getCustomerDetailsStatic($customerId);
        $stripeCustomerId = $customerData["cust_stripe_cust_id"];

        $response = array();
        $response["code"] = -3;
        $response["message"] = "Missing stripe account";

        if($customerData["cust_stripe_cust_id"] != ""){
            
            $result = bizManager::getStripeAccountForBiz($customerData["cust_biz_id"]);
 
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){

                try{

                    if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $customer = \Stripe\Customer::retrieve($stripeCustomerId,["stripe_account" => $result["data"]["stripe_account_id"]]);
                    $customer->sources->retrieve($cardId)->delete();

                    $response["code"] = 1;
                    $response["message"] = "success";

                }
                catch(Exception $e){
                    $response["code"] = -1;
                    $response["message"] = $e->getMessage();
                } 
            }
        }

        return $response;
    }

    public function stripe_retreiveCardsForCustomer($customerId){

        $customerData = customersManager::getCustomerDetailsStatic($customerId);
        $stripeCustomerId = $customerData["cust_stripe_cust_id"];

        $response = array();
        $response["code"] = -3;
        $response["message"] = "Missing stripe account";
        $response["data"] = array();

        if($customerData["cust_stripe_cust_id"] != ""){
            
            $result = bizManager::getStripeAccountForBiz($customerData["cust_biz_id"]);
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){

                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                    $customer = \Stripe\Customer::retrieve($stripeCustomerId,["stripe_account" => $result["data"]["stripe_account_id"]]);

                    $response["code"] = 1;
                    $response["message"] = "";
                    $response["data"] = $customer->sources->all()["data"];
                }
                catch (\Stripe\Error\Base $e) {
                    $response["code"] = -1;
                    $response["message"] =$e->getMessage();
                }
                catch (Exception $e) {
                    $response["code"] = -2;
                    $response["message"] =$e->getMessage();
                }
            }
        }

        return $response;

    }

    public function stripe_setCardAsDefaultForCustomer(customerObject $customer,$stripeCardId){

        
        $stripeCustomerId = $customer->cust_stripe_cust_id;

        $response = array();
        $response["code"] = -3;
        $response["message"] = "Missing stripe account";

        if($customer->cust_stripe_cust_id != ""){
            
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);
          
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){

                try{

                    if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $customer = \Stripe\Customer::retrieve($stripeCustomerId,["stripe_account" => $result["data"]["stripe_account_id"]]);
                    $customer->default_source = $stripeCardId;
                    $customer->save();

                    $response["code"] = 1;
                    $response["message"] = "success";

                }
                catch(Exception $e){
                    $response["code"] = -1;
                    $response["message"] = $e->getMessage();
                } 
            }
        }

        return $response;
    }

    public function stripe_retreiveSpecificCardForCustomer($customerId,$stripeCardId){

        $customerData = customersManager::getCustomerDetailsStatic($customerId);
        $stripeCustomerId = $customerData["cust_stripe_cust_id"];

        $response = array();
        $response["code"] = -3;
        $response["message"] = "Missing stripe account";
        $response["data"] = array();

        if($customerData["cust_stripe_cust_id"] != ""){
            
            $result = bizManager::getStripeAccountForBiz($customerData["cust_biz_id"]);
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != ""){

                if(isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                }else{
                    \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                }

                try {

                    $customer = \Stripe\Customer::retrieve($stripeCustomerId,["stripe_account" => $result["data"]["stripe_account_id"]]);

                    $response["code"] = 1;
                    $response["message"] = "";
                    $response["data"] = $customer->sources->retrieve($stripeCardId);
                }
                catch (\Stripe\Error\Base $e) {
                    $response["code"] = -1;
                    $response["message"] =$e->getMessage();
                }
                catch (Exception $e) {
                    $response["code"] = -2;
                    $response["message"] =$e->getMessage();
                }
            }
        }

        return $response;

    }


    /* ------ INVOICE ITEMS (ORDERS) ------ */
    public function stripe_createInvoiceItem($stripeCustomerId,$ordersArray,$bizId){

        $result = bizManager::getStripeAccountForBiz($bizId);

        if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
            \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
        }else{
            \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        }

        try{

            $data = array();
            $data["customer"] = $stripeCustomerId;
            $data["unit_amount"] = $ordersArray["amount"];
            $data["currency"] = $ordersArray["currency"];
            $data["description"] = $ordersArray["description"];
            $data["quantity"] = $ordersArray["quantity"];

            $invoiceItem = \Stripe\InvoiceItem::create(
                $data,
                ["stripe_account" => $this->stripeConnectAcount]
            );

            return $invoiceItem;
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();
            return $response;
        }
    }

    public function stripe_deleteInvoiceItem($stripeItemId,$bizId){

        $result = bizManager::getStripeAccountForBiz($bizId);

        if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
            \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
        }else{
            \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        }
        try{

            $invoiceItem = \Stripe\InvoiceItem::retrieve($stripeItemId,["stripe_account" => $this->stripeConnectAcount]);
            $response = $invoiceItem->delete();

            return $response;
        }
        catch(Exception $e){
            return array();
        }

    }

    public function stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$bizId){

        $allCustItems = $this->stripe_retreiveAllItemsForCustomer($stripeCustomerId,$bizId);
        
        if (count($allCustItems["data"]) > 0){
            foreach ($allCustItems["data"] as $item){
                if( !isset($item["invoice"]) || $item["invoice"] == null || $item["invoice"] == "" ){
                    $this->stripe_deleteInvoiceItem($item["id"],$bizId);
                }
            }
        }
    }

    public function stripe_retreiveAllItemsForCustomer($stripeCustomerId,$bizId){

        $result = bizManager::getStripeAccountForBiz($bizId);

        if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
            \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
        }else{
            \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
        }
        try{

            $data = array();
            $data["customer"] = $stripeCustomerId;

            $response = \Stripe\InvoiceItem::all(
                $data,
                ["stripe_account" => $this->stripeConnectAcount]
            );
            return $response;
        }
        catch(Exception $e){
            return array();
        }

    }


    /* ------ INVOICES ------ */
    public function stripe_createInvoice($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);


        
        $custStripeID = customerManager::getCustomerstripeID($invoiceRow["cin_cust_id"]);
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($custStripeID != ""){
            
            $stripeCustomerId = $custStripeID;

            if($this->stripeConnectAcount != ""){

                $invoiceItems = $this->getConnectedOrdersForCustInvoice($cutomerInvoieID);
                $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$invoiceRow["cin_biz_id"]);
                $subTotal = 0;
                $taxAmmount = 0;

                if(count($invoiceItems) > 0){
                    
                    foreach ($invoiceItems as $item)
                    {
                        $ammount = $item["cto_without_vat"] + $item["cto_sipping"] - $item["cto_membership_discount"] - $item["cto_benefit_value"];
                        $ammount = number_format((float)$ammount, 2, '', '');
                        $subTotal = $subTotal + $ammount;
                        $taxAmmount = $taxAmmount + $item["cto_vat"];

                        $itemArray = array();
                        $itemArray["amount"] = $ammount;
                        $itemArray["currency"] = $invoiceRow["cin_currency"];
                        $itemArray["description"] = "Order No: {$item["cto_id"]}";
                        $itemArray["quantity"] = 1;

                        $resultItem = $this->stripe_createInvoiceItem($stripeCustomerId,$itemArray,$invoiceRow["cin_biz_id"]);

                        if (isset($resultItem["responseStatus"])){
                            return $resultItem;
                        }
                    }            
                }else{

                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = "No Items Created on Stripe";
                    return $response;
                }

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);
                   
                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $metadata = array();
                    $metadata["Invoice ID"] = $cutomerInvoieID;
                    $metadata["Biz ID"] = $invoiceRow["cin_biz_id"];
                    $metadata["Entity"] = "customer";

                    $data = array();
                    $data["customer"] = $stripeCustomerId;
                    $data["billing"] = $invoiceRow["cin_stripe_billing_type"];
                    $data["metadata"] = $metadata;
                    $data["auto_advance"] = false;

                    $tax = ($taxAmmount / $subTotal) * 100;
                    if($tax > 0){
                        $data["tax_percent"] = number_format((float)$tax, 2, '', '');
                    }

                    $invoice = \Stripe\Invoice::create(
                        $data,
                        ["stripe_account" => $this->stripeConnectAcount]
                    );
                    
                    $stripeInvoiceId = $invoice['id'];        
                    $this->db->execute("UPDATE tbl_cust_invoice SET cin_stripe_invoice_id='$stripeInvoiceId' WHERE cin_id=$cutomerInvoieID");

                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Created Successfully";
                }
                catch(Exception $e){

                    //If the invoice creation was failed - it means that there are floating items in Stripe.
                    //It is required to clean them. otherwise they will be added to the next invoice
                    $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$invoiceRow["cin_biz_id"]);

                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;
    }

    public function stripe_createInvoiceWithPaymentMethodID($cutomerInvoieID,$payMethodID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);
        
        $custStripeID = customerManager::getCustomerstripeID($invoiceRow["cin_cust_id"]);
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($custStripeID != ""){
            
            $stripeCustomerId = $custStripeID;

            if($this->stripeConnectAcount != ""){

                $invoiceItems = $this->getConnectedOrdersForCustInvoice($cutomerInvoieID);
                $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$invoiceRow["cin_biz_id"]);
                $subTotal = 0;
                $taxAmmount = 0;

                if(count($invoiceItems) > 0){
                    
                    foreach ($invoiceItems as $item)
                    {
                        $ammount = $item["cto_without_vat"] + $item["cto_sipping"] - $item["cto_membership_discount"] - $item["cto_benefit_value"];
                        $ammount = number_format((float)$ammount, 2, '', '');
                        $subTotal = $subTotal + $ammount;
                        $taxAmmount = $taxAmmount + $item["cto_vat"];

                        $itemArray = array();
                        $itemArray["amount"] = $ammount;
                        $itemArray["currency"] = $invoiceRow["cin_currency"];
                        $itemArray["description"] = "Order No: {$item["cto_id"]}";
                        $itemArray["quantity"] = 1;

                        $resultItem = $this->stripe_createInvoiceItem($stripeCustomerId,$itemArray,$invoiceRow["cin_biz_id"]);

                        if (isset($resultItem["responseStatus"])){
                            return $resultItem;
                        }
                    }            
                }else{

                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = "No Items Created on Stripe";
                    return $response;
                }

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $metadata = array();
                    $metadata["Invoice ID"] = $cutomerInvoieID;
                    $metadata["Biz ID"] = $invoiceRow["cin_biz_id"];
                    $metadata["Entity"] = "customer";

                    $data = array();
                    $data["customer"] = $stripeCustomerId;
                    $data["billing"] = $invoiceRow["cin_stripe_billing_type"];
                    $data["metadata"] = $metadata;
                    $data["auto_advance"] = false;
                    $data['default_payment_method'] = $payMethodID;

                    $tax = ($taxAmmount / $subTotal) * 100;
                    if($tax > 0){
                        $data["tax_percent"] = number_format((float)$tax, 2, '', '');
                    }

                    $invoice = \Stripe\Invoice::create(
                        $data,
                        ["stripe_account" => $this->stripeConnectAcount]
                    );
                    
                    $stripeInvoiceId = $invoice['id'];        
                    $this->db->execute("UPDATE tbl_cust_invoice SET cin_stripe_invoice_id='$stripeInvoiceId' WHERE cin_id=$cutomerInvoieID");

                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Created Successfully";
                }
                catch(Exception $e){

                    //If the invoice creation was failed - it means that there are floating items in Stripe.
                    //It is required to clean them. otherwise they will be added to the next invoice
                    $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$invoiceRow["cin_biz_id"]);

                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;
    }

    public function stripe_chargeInvoice($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);
        
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($invoiceRow["cin_stripe_invoice_id"] != ""){
            
            if($this->stripeConnectAcount != ""){

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $invoice = \Stripe\Invoice::retrieve($invoiceRow["cin_stripe_invoice_id"],["stripe_account" => $this->stripeConnectAcount]);
                    try{
                        $invoice->pay();
                    }
                    catch(Exception $e){
                        $response["responseStatus"] = -1;
                        $response["errorDescription"] = $e->getMessage();
                        $response['stripeInvoice'] = $invoice;
                    }

                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Payd Successfully";
                    $response['stripeInvoice'] = $invoice;
                }
                catch(Exception $e){
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                    $response['stripeInvoice'] = "";
                }
            }
        }
        

        return $response;
    }

    public function stripe_refundInvoice($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);

        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($invoiceRow["cin_stripe_charge_id"] != ""){

            if($this->stripeConnectAcount != ""){

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $metadata = array();
                    $metadata["Entity"] = "customer";

                    $data = array();
                    $data["charge"] = $invoiceRow["cin_stripe_charge_id"];
                    $data["metadata"] = $metadata;

                    \Stripe\Refund::create(
                        $data,
                        ["stripe_account" => $this->stripeConnectAcount]
                    );
                   
                    $this->markInvoiceAsRefunded($cutomerInvoieID);
                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Refunded Successfully";
                }
                catch(Exception $e){
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;
    }

    public function stripe_partialRefundTransaction(customerTransactionObject $transaction,$amount){
        try{
            $this->setStripeConnectAccountByInvoiceId($transaction->tr_invoice_id);
            $response = array();
            if($transaction->tr_reconciliation_Id != ""){
                if($this->stripeConnectAcount != ""){

                    $result = bizManager::getStripeAccountForBiz($transaction->tr_biz_id);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $metadata = array();
                    $metadata["Entity"] = "customer";

                    $data = array();
                    $data["charge"] = $transaction->tr_reconciliation_Id;
                    $data["metadata"] = $metadata;
                    $data["amount"] = $transaction->tr_amount * 100;

                    
                    try{
                        $response = \Stripe\Refund::create(
                                $data,
                                ["stripe_account" => $this->stripeConnectAcount]
                            );

                        return resultObject::withData(1,'',$response);
                    }
                    catch(Exception $e){
                        return resultObject::withData(0,$e->getMessage);
                    }

                   
                }
            }

            return resultObject::withData(0,'failed');
        }
        catch(Exception $e){
            return resultObject::withData(0,$e->getMessage);
        }
    }

    public function stripe_retreiveChargeResultForInvoice($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);
        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);

        if($invoiceRow["cin_stripe_charge_id"] != ""){
            
            $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
            }else{
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
            }
            try{
                $response = \Stripe\Charge::retrieve($invoiceRow["cin_stripe_charge_id"],["stripe_account" => $this->stripeConnectAcount]);
                return $response;
            }
            catch(Exception $e){
                return array();
            }
        }

        return array();

    }

    public function stripe_closeInvoice($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);
        
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($invoiceRow["cin_stripe_invoice_id"] != ""){
            
            if($this->stripeConnectAcount != ""){

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $invoice = \Stripe\Invoice::retrieve($invoiceRow["cin_stripe_invoice_id"],["stripe_account" => $this->stripeConnectAcount]);
                    $invoice->voidInvoice();

                    $this->markInvoiceAsVoided($cutomerInvoieID);
                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Voided Successfully";
                }
                catch(Exception $e){
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;

    }

    public function stripe_finalizeInvoice($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);
        
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($invoiceRow["cin_stripe_invoice_id"] != ""){
            
            if($this->stripeConnectAcount != ""){

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $invoice = \Stripe\Invoice::retrieve($invoiceRow["cin_stripe_invoice_id"],["stripe_account" => $this->stripeConnectAcount]);
                    $invoice->finalizeInvoice();
                    $this->markInvoiceAsFinalized($cutomerInvoieID);

                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Finalized Successfully";
                    $response["stripeInvoice"] = $invoice;
                }
                catch(Exception $e){
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;

    }

    public function stripe_stopInvoiceCollection($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);
      
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($invoiceRow["cin_stripe_invoice_id"] != ""){
           
            if($this->stripeConnectAcount != ""){

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $invoice = \Stripe\Invoice::retrieve($invoiceRow["cin_stripe_invoice_id"],["stripe_account" => $this->stripeConnectAcount]);
                    $invoice->auto_advance = false;
                    $invoice->save();

                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Collection Stoped Successfully";
                }
                catch(Exception $e){
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;
    }

    public function stripe_markInvoiceAsPAyd($cutomerInvoieID){

        $this->setStripeConnectAccountByInvoiceId($cutomerInvoieID);

        $invoiceRow = $this->getCustInvoiceDataWithInvoiceId($cutomerInvoieID);
        
        $response = array();
        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($invoiceRow["cin_stripe_invoice_id"] != ""){
            
            if($this->stripeConnectAcount != ""){

                try{
                    $result = bizManager::getStripeAccountForBiz($invoiceRow["cin_biz_id"]);

                    if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
                    }else{
                        \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
                    }

                    $invoice = \Stripe\Invoice::retrieve($invoiceRow["cin_stripe_invoice_id"],["stripe_account" => $this->stripeConnectAcount]);
                    $invoice->paid = true;
                    $invoice->save();

                    $response["responseStatus"] = 0;
                    $response["errorDescription"] = "Invoice Payd Manualy Successfully";
                }
                catch(Exception $e){
                    $response["responseStatus"] = -1;
                    $response["errorDescription"] = $e->getMessage();
                }
            }
        }
        return $response;
    }

    public function createStripeInvoiceForCustomerOrder(customerObject $customer,customerOrderObject $order,$payMethodID){
        $stripeCustomerId = $customer->cust_stripe_cust_id;

        $response = array();
        
        $this->stripeConnectAcount = $this->getCustomerOwnerStripeAccountId($customer->cust_biz_id);

        $response["responseStatus"] = -1;
        $response["errorDescription"] = "Missing stripe account";

        if($this->stripeConnectAcount == "" || $stripeCustomerId == ""){
            return $response;
        }

        $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$customer->cust_biz_id);

        $subTotal = 0;
        $taxAmmount = 0;

        $ammount = $order->cto_without_vat + $order->cto_sipping - $order->cto_membership_discount - $order->cto_benefit_value;
        $ammount = number_format((float)$ammount, 2, '', '');
        $subTotal = $subTotal + $ammount;
        $taxAmmount = $taxAmmount + $order->cto_vat;

        $itemArray = array();
        $itemArray["amount"] = $ammount;
        $itemArray["currency"] = $order->cto_currency;
        $itemArray["description"] = "Order for sum: $ammount".$order->cto_currency;
        $itemArray["quantity"] = 1;

        $resultItem = $this->stripe_createInvoiceItem($stripeCustomerId,$itemArray,$customer->cust_biz_id);

        if (isset($resultItem["responseStatus"])){
            return $resultItem;
        }

        try{
            $result = bizManager::getStripeAccountForBiz($customer->cust_biz_id);
           
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
            }else{
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
            }

            $metadata = array();
            
            $metadata["Biz ID"] = $customer->cust_biz_id;
            $metadata["Entity"] = "customer";

            $data = array();
            $data["customer"] = $stripeCustomerId;
            $data["billing"] = 'charge_automatically';
            $data["metadata"] = $metadata;
            $data["auto_advance"] = false;
            $data['default_payment_method'] = $payMethodID;

            $tax = ($taxAmmount / $subTotal) * 100;
            if($tax > 0){
                $data["tax_percent"] = number_format((float)$tax, 2, '', '');
            }

            $invoice = \Stripe\Invoice::create(
                $data,
                ["stripe_account" => $this->stripeConnectAcount]
            );            
            

            return resultObject::withData(1,"Invoice Created Successfully",$invoice);
        }
        catch(Exception $e){

            //If the invoice creation was failed - it means that there are floating items in Stripe.
            //It is required to clean them. otherwise they will be added to the next invoice
            $this->stripe_deleteFloatingItemsForCustomer($stripeCustomerId,$customer->cust_biz_id);

            return resultObject::withData(0,$e->getMessage());
        }

    }

    public function getStripeInvoice($stripeInvoiceID){
        //To do
    }

    public function finalizeStripeInvoiceForCustomer($stripeInvoiceObject){
        try{
            $result = bizManager::getStripeAccountForBiz($this->bizID);

            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
            }else{
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
            }
          
            $stripeInvoiceObject->finalizeInvoice();            

            return resultObject::withData(1,"Invoice Finalized Successfully",$stripeInvoiceObject);
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();

            return resultObject::withData(0,$e->getMessage());
        }
    }
    
    public function getAllCustomerCards($stripeCustomerID){
        try{
            $result = bizManager::getStripeAccountForBiz($this->bizID);
            
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
            }else{
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
            }
            
            
            $cards = \Stripe\PaymentMethod::all(["customer" => $stripeCustomerID, "type" => "card"],["stripe_account" => $result["data"]["stripe_account_id"]]);       

            return resultObject::withData(1,"Invoice Finalized Successfully",$cards);
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();

            return resultObject::withData(0,$e->getMessage());
        }
    }


    //Test
    public function checkCardToken(customerObject $customer){
        $sql = "SELECT * FROM tbl_customer_payment_sources 
                    WHERE cps_cust_id = {$customer->cust_id}
                    AND cps_default = 1";

        $paySource = $this->db->getRow($sql);

        $token = $paySource['cps_stipe_object_id'];

        try{
            $result = bizManager::getStripeAccountForBiz($this->bizID);
            
            if($result['code'] != 0 && $result["data"]["stripe_account_id"] != "" && isset($result["data"]["ENVIRONMENT"]) && $result["data"]["ENVIRONMENT"] == "SANDBOX"){
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY_SANDBOX'));
            }else{
                \Stripe\Stripe::setApiKey(getenv('STRIPE_API_KEY'));
            }
            
            $payM = \Stripe\PaymentMethod::create(["type" => "card","card" => ["id" => $token]]);     

            return resultObject::withData(1,"Invoice Finalized Successfully",$payM);
        }
        catch(Exception $e){
            $response["responseStatus"] = -1;
            $response["errorDescription"] = $e->getMessage();

            return resultObject::withData(0,$e->getMessage());
        }

        

        
        
    }

    /* ------ WEB HOOKS ------ */
    public function stripe_webhookPaymentSucceeded($receivedData){

        $invoiceId = $this->getLocalInvoiceIdByStripeInvoiceId($receivedData->data->object->id);

        if($invoiceId != ""){
            $this->db->execute("UPDATE tbl_cust_invoice SET cin_stripe_charge_id='{$receivedData->data->object->charge}' WHERE cin_id=$invoiceId");
            $this->markInvoiceAsPaid($invoiceId);

            $this->addTransactionCustomer($invoiceId,"","",$receivedData->data->object->charge,"CHARGE");
        }
    }

    public function stripe_webhookChargeRefunded($receivedData){

        $invoiceId = $this->getLocalInvoiceIdByStripeChargeId($receivedData->data->object->id);

        $app_creator = new appCreatorManager();
        $transactionID =  $this->getTransactionByInvoiceId($invoiceId);
        if($transactionID != "" && $transactionID  > 0){
            $trans_id = $app_creator->DuplicateSQLRecord('tbl_cust_transaction', 'tr_id', $transactionID);
            $this->updateRefundTransaction($trans_id);
        } 

    }

    /* ------ STATISTICS ------ */

    public function getCustomerPaymentStats($biz_id,$start_date,$end_date,$interval){
        $result = array();

        $result['trends'] = array();
        $result['trends']['sum'] = $this->getSumInvoiceTrendsBetweenDates($biz_id,$start_date,$end_date,$interval);
        $result['trends']['data'] = $this->getInvoiceTrendsBetweenDates($biz_id,$start_date,$end_date,$interval);

        $result['gross'] = array();
        $result['gross']['sum'] = $this->getSumTotalGrossPaid($biz_id,$start_date,$end_date,$interval);
        $result['gross']['data'] = $this->getTotalGrossPaid($biz_id,$start_date,$end_date,$interval);

        $result['net'] = array();
        $result['net']['sum'] = $this->getSumTotalNetPaid($biz_id,$start_date,$end_date,$interval);
        $result['net']['data'] = $this->getTotalNetPaid($biz_id,$start_date,$end_date,$interval);

        $result['success'] = array();
        $result['success']['sum'] = $this->getSumTotalSuccessfulPayments($biz_id,$start_date,$end_date,$interval);
        $result['success']['data'] = $this->getTotalSuccessfulPayments($biz_id,$start_date,$end_date,$interval);

        return $result;
    }

    public function getInvoiceTrendsBetweenDates($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT * FROM (
                    SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total 
                    FROM calendar
                    LEFT JOIN (SELECT ROUND(SUM(cin_total),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice
                               WHERE cin_status NOT IN ('draft')
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield = inv_curr.day_date
                    LEFT JOIN (SELECT ROUND(SUM(cin_total),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice
                               WHERE cin_status NOT IN ('draft')
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        
        return $this->db->getTable($sql);

    }

    public function getSumInvoiceTrendsBetweenDates($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT SUM(curr_total) as now,
                        SUM(prev_total) as prev,
                       IF(SUM(prev_total) <> 0,(SUM(curr_total) / SUM(prev_total)) - 1,'NA') as percent
                    FROM (
                   SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total 
                    FROM calendar
                    LEFT JOIN (SELECT ROUND(SUM(cin_total),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice
                               WHERE cin_status NOT IN ('draft')
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield = inv_curr.day_date
                    LEFT JOIN (SELECT ROUND(SUM(cin_total),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice
                               WHERE cin_status NOT IN ('draft')
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        return $this->db->getRow($sql);

    }

    public function getTotalGrossPaid($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT * FROM (
                    SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total  
                    FROM calendar
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield =inv_curr.day_date
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        return $this->db->getTable($sql);

    }

    public function getSumTotalGrossPaid($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT SUM(curr_total) as now,
                        SUM(prev_total) as prev,
                       IF(SUM(prev_total) <> 0,(SUM(curr_total) / SUM(prev_total)) - 1,'NA') as percent
                    FROM (
                    SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total  
                    FROM calendar
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield =inv_curr.day_date
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        return $this->db->getRow($sql);

    }

    public function getTotalNetPaid($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT * FROM (
                    SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total 
                    FROM calendar
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount - cin_tax),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield = inv_curr.day_date
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount - cin_tax),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        return $this->db->getTable($sql);

    }

    public function getSumTotalNetPaid($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT SUM(curr_total) as now,
                        SUM(prev_total) as prev,
                       IF(SUM(prev_total) <> 0,(SUM(curr_total) / SUM(prev_total)) - 1,'NA') as percent
                    FROM (
                     SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total 
                    FROM calendar
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount - cin_tax),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield = inv_curr.day_date
                    LEFT JOIN (SELECT ROUND(SUM(tr_amount - cin_tax),2) day_total,DATE(cin_create_date) as day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";


        return $this->db->getRow($sql);

    }

    public function getTotalSuccessfulPayments($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT * FROM (
                    SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total 
                    FROM calendar
                    LEFT JOIN (SELECT COUNT(*) as day_total,DATE(cin_create_date) day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield = inv_curr.day_date
                    LEFT JOIN (SELECT COUNT(*) as day_total,DATE(cin_create_date) day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        

        return $this->db->getTable($sql);

    }

    public function getSumTotalSuccessfulPayments($biz_id,$start_date,$end_date,$interval){

        $sql = "SELECT SUM(curr_total) as now,
                        SUM(prev_total) as prev,
                       IF(SUM(prev_total) <> 0,(SUM(curr_total) / SUM(prev_total)) - 1,'NA') as percent
                    FROM (
                    SELECT datefield curr_date, 
    	                    IFNULL(inv_curr.day_total,0) curr_total ,
    	                    DATE_SUB(datefield,INTERVAL 1 {$interval}) previous_date ,  
    	                    IFNULL(prev_inv.day_total,0) prev_total 
                    FROM calendar
                    LEFT JOIN (SELECT COUNT(*) as day_total,DATE(cin_create_date) day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )inv_curr
                    ON datefield = inv_curr.day_date
                    LEFT JOIN (SELECT COUNT(*) as day_total,DATE(cin_create_date) day_date FROM tbl_cust_invoice,tbl_cust_transaction
                               WHERE tr_invoice_id = cin_id
                               AND tr_amount > 0
                               AND cin_biz_id = $biz_id
                                GROUP BY day_date
                               )prev_inv
                    ON DATE_SUB(datefield,INTERVAL 1 {$interval}) = prev_inv.day_date
                    WHERE datefield BETWEEN DATE('{$start_date}') AND DATE('{$end_date}')
                    GROUP BY datefield
                        ) CUBE
                    ORDER BY curr_date ASC";

        return $this->db->getRow($sql);

    }

    /************************************* */
    /*   BASIC CUSTOMERINVOICE - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerInvoiceObject to DB
     * Return Data = new customerInvoiceObject ID
     * @param customerInvoiceObject $customerInvoiceObj 
     * @return resultObject
     */
    public function addCustomerInvoice(customerInvoiceObject $customerInvoiceObj){       
        try{
            
            $newId = $this->addCustomerInvoiceDB($customerInvoiceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerInvoiceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerInvoice from DB for provided ID
     * * Return Data = customerInvoiceObject
     * @param int $customerInvoiceId 
     * @return resultObject
     */
    public function getCustomerInvoiceByID($customerInvoiceId){
        
        try {
            
            $customerInvoiceData = $this->loadCustomerInvoiceFromDB($customerInvoiceId);
            
            $customerInvoiceObj = customerInvoiceObject::withData($customerInvoiceData);
            $result = resultObject::withData(1,'',$customerInvoiceObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerInvoiceId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerInvoice in DB
     * @param customerInvoiceObject $customerInvoiceObj 
     * @return resultObject
     */
    public function updateCustomerInvoice(customerInvoiceObject $customerInvoiceObj){        
        try{
            
            $this->updateCustomerInvoiceDB($customerInvoiceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerInvoiceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerInvoice from DB
     * @param int $customerInvoiceID 
     * @return resultObject
     */
    public function deleteCustomerInvoiceById($customerInvoiceID){
        try{
            
            $this->deleteCustomerInvoiceByIdDB($customerInvoiceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerInvoiceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERINVOICE - DB METHODS           */
    /************************************* */

    private function addCustomerInvoiceDB(customerInvoiceObject $obj){

        if (!isset($obj)){
            throw new Exception("customerInvoiceObject value must be provided");             
        }

        
       

        $newId = $this->db->execute("INSERT INTO tbl_cust_invoice SET 
                                        cin_long_id = '".addslashes($obj->cin_long_id)."',
                                        cin_biz_id = {$obj->cin_biz_id},
                                        cin_cust_id = {$obj->cin_cust_id},
                                        cin_unique_id = '".addslashes($obj->cin_unique_id)."',
                                        cin_is_credit_note = {$obj->cin_is_credit_note},
                                        cin_stripe_invoice_id = '".addslashes($obj->cin_stripe_invoice_id)."',
                                        cin_stripe_payintent_id = '".addslashes($obj->cin_stripe_payintent_id)."',
                                        cin_stripe_charge_id = '".addslashes($obj->cin_stripe_charge_id)."',
                                        cin_sub_total = {$obj->cin_sub_total},
                                        cin_tax = {$obj->cin_tax},
                                        cin_total = {$obj->cin_total},
                                        cin_currency = '".addslashes($obj->cin_currency)."',
                                        cin_status = '{$obj->cin_status}',
                                        cin_finalized = {$obj->cin_finalized},
                                        cin_stripe_billing_type = '{$obj->cin_stripe_billing_type}',
                                        cin_due_date = DATE_ADD(NOW(),INTERVAL 30 DAY),
                                        cin_parent_id = {$obj->cin_parent_id}
                                                 ");
        return $newId;
    }

    private function loadCustomerInvoiceFromDB($customerInvoiceID){

        if (!is_numeric($customerInvoiceID) || $customerInvoiceID <= 0){
            throw new Exception("Illegal value customerInvoiceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_invoice WHERE cin_id = $customerInvoiceID");
    }

    private function loadCustomerInvoiceFromDBByLongID($customerInvoiceLongID){
       

        return $this->db->getRow("SELECT * FROM tbl_cust_invoice WHERE cin_long_id = '$customerInvoiceLongID'");
    }

    private function updateCustomerInvoiceDB(customerInvoiceObject $obj){

        if (!isset($obj->cin_id) || !is_numeric($obj->cin_id) || $obj->cin_id <= 0){
            throw new Exception("customerInvoiceObject value must be provided");             
        }

        $createDateSQL = "";
        if(isset($obj->cin_create_date)){
            $createDateSQL = "cin_create_date = '{$obj->cin_create_date}',";
        }

        $dueDateSQL = "";
        if(isset($obj->cin_due_date)){
            $dueDateSQL = "cin_due_date = '{$obj->cin_due_date}',";
        }
        

        $this->db->execute("UPDATE tbl_cust_invoice SET 
                                cin_long_id = '".addslashes($obj->cin_long_id)."',
                                cin_biz_id = {$obj->cin_biz_id},
                                cin_cust_id = {$obj->cin_cust_id},
                                cin_unique_id = '".addslashes($obj->cin_unique_id)."',
                                cin_is_credit_note = {$obj->cin_is_credit_note},
                                cin_stripe_invoice_id = '".addslashes($obj->cin_stripe_invoice_id)."',
                                cin_stripe_payintent_id = '".addslashes($obj->cin_stripe_payintent_id)."',
                                cin_stripe_charge_id = '".addslashes($obj->cin_stripe_charge_id)."',
                                $createDateSQL
                                $dueDateSQL
                                cin_sub_total = {$obj->cin_sub_total},
                                cin_tax = {$obj->cin_tax},
                                cin_total = {$obj->cin_total},
                                cin_currency = '".addslashes($obj->cin_currency)."',
                                cin_status = '{$obj->cin_status}',
                                cin_finalized = {$obj->cin_finalized},
                                cin_stripe_billing_type = '{$obj->cin_stripe_billing_type}',
                                cin_parent_id = {$obj->cin_parent_id}
                                WHERE cin_id = {$obj->cin_id} 
                                         ");
    }

    private function deleteCustomerInvoiceByIdDB($customerInvoiceID){

        if (!is_numeric($customerInvoiceID) || $customerInvoiceID <= 0){
            throw new Exception("Illegal value customerInvoiceID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_invoice WHERE cin_id = $customerInvoiceID");
    }

    private function loadCustomerInvoiceForOrder($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_invoice 
                WHERE cini_order_id = $orderID
                AND cini_invoice_id = cin_id";

        return $this->db->getRow($sql);
    }

    private function loadCustomerInvoicesListForOrder($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_invoice 
                WHERE cini_order_id = $orderID
                AND cini_invoice_id = cin_id";

        return $this->db->getTable($sql);
    }

    private function getCustomerInvoiceIDForOrderForPartialRefund($orderID,$amount){

        $amountSql = "";

        if($amount > 0){
            $amountSql = " AND cin_total >= $amount";
        }

        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_invoice 
                WHERE cini_order_id = $orderID
                AND cini_invoice_id = cin_id
                AND cin_is_credit_note = 0
                $amountSql
                LIMIT 1";

        return $this->db->getRow($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERINVOICEHISTORY - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerInvoiceHistoryObject to DB
     * Return Data = new customerInvoiceHistoryObject ID
     * @param customerInvoiceHistoryObject $customerInvoiceHistoryObj 
     * @return resultObject
     */
    public function addCustomerInvoiceHistory(customerInvoiceHistoryObject $customerInvoiceHistoryObj){       
        try{
            
            $newId = $this->addCustomerInvoiceHistoryDB($customerInvoiceHistoryObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerInvoiceHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerInvoiceHistory from DB for provided ID
     * * Return Data = customerInvoiceHistoryObject
     * @param int $customerInvoiceHistoryId 
     * @return resultObject
     */
    public function getCustomerInvoiceHistoryByID($customerInvoiceHistoryId){
        
        try {
            
            $customerInvoiceHistoryData = $this->loadCustomerInvoiceHistoryFromDB($customerInvoiceHistoryId);
            
            $customerInvoiceHistoryObj = customerInvoiceHistoryObject::withData($customerInvoiceHistoryData);
            $result = resultObject::withData(1,'',$customerInvoiceHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerInvoiceHistoryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerInvoiceHistory in DB
     * @param customerInvoiceHistoryObject $customerInvoiceHistoryObj 
     * @return resultObject
     */
    public function updateCustomerInvoiceHistory(customerInvoiceHistoryObject $customerInvoiceHistoryObj){        
        try{
            
            $this->upateCustomerInvoiceHistoryDB($customerInvoiceHistoryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerInvoiceHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerInvoiceHistory from DB
     * @param int $customerInvoiceHistoryID 
     * @return resultObject
     */
    public function deleteCustomerInvoiceHistoryById($customerInvoiceHistoryID){
        try{
            
            $this->deleteCustomerInvoiceHistoryByIdDB($customerInvoiceHistoryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerInvoiceHistoryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERINVOICEHISTORY - DB METHODS           */
    /************************************* */

    private function addCustomerInvoiceHistoryDB(customerInvoiceHistoryObject $obj){

        if (!isset($obj)){
            throw new Exception("customerInvoiceHistoryObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_invoice_history SET 
                                        cinh_invoice_id = {$obj->cinh_invoice_id},
                                        cinh_event = '{$obj->cinh_event}',
                                        cinh_cust_transaction_id = {$obj->cinh_cust_transaction_id},
                                        cinh_cust_payment_source_id = {$obj->cinh_cust_payment_source_id},
                                        cinh_note = '".addslashes($obj->cinh_note)."'
                                                 ");
        return $newId;
    }

    private function loadCustomerInvoiceHistoryFromDB($customerInvoiceHistoryID){

        if (!is_numeric($customerInvoiceHistoryID) || $customerInvoiceHistoryID <= 0){
            throw new Exception("Illegal value customerInvoiceHistoryID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_invoice_history WHERE cinh_id = $customerInvoiceHistoryID");
    }

    private function upateCustomerInvoiceHistoryDB(customerInvoiceHistoryObject $obj){

        if (!isset($obj->cinh_id) || !is_numeric($obj->cinh_id) || $obj->cinh_id <= 0){
            throw new Exception("customerInvoiceHistoryObject value must be provided");             
        }

        $cinh_timeDate = isset($obj->cinh_time) ? "'".$obj->cinh_time."'" : "null";

        $this->db->execute("UPDATE tbl_cust_invoice_history SET 
                                cinh_invoice_id = {$obj->cinh_invoice_id},
                                cinh_event = '{$obj->cinh_event}',
                                cinh_time = $cinh_timeDate,
                                cinh_cust_transaction_id = {$obj->cinh_cust_transaction_id},
                                cinh_cust_payment_source_id = {$obj->cinh_cust_payment_source_id},
                                cinh_note = '".addslashes($obj->cinh_note)."'
                                WHERE cinh_id = {$obj->cinh_id} 
                                         ");
    }

    private function deleteCustomerInvoiceHistoryByIdDB($customerInvoiceHistoryID){

        if (!is_numeric($customerInvoiceHistoryID) || $customerInvoiceHistoryID <= 0){
            throw new Exception("Illegal value customerInvoiceHistoryID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_invoice_history WHERE cinh_id = $customerInvoiceHistoryID");
    }

    /************************************* */
    /*   BASIC CUSTOMERTRANSACTION - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerTransaction to DB
     * Return Data = new customerPaymentChargeObject ID
     * @param customerTransactionObject $customerPaymentChargeObj 
     * @return resultObject
     */
    public function addCustomerPaymentCharge(customerTransactionObject $customerPaymentChargeObj){       
        try{
            
            $newId = $this->addCustomerPaymentChargeDB($customerPaymentChargeObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPaymentChargeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerTransaction from DB for provided ID
     * * Return Data = customerPaymentChargeObject
     * @param int $customerPaymentChargeId 
     * @return resultObject
     */
    public function getCustomerPaymentChargeByID($customerPaymentChargeId){
        
        try {
            
            $customerPaymentChargeData = $this->loadCustomerPaymentChargeFromDB($customerPaymentChargeId);
            
            $customerPaymentChargeObj = customerTransactionObject::withData($customerPaymentChargeData);
            $result = resultObject::withData(1,'',$customerPaymentChargeObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPaymentChargeId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerTransaction in DB
     * @param customerTransactionObject $customerPaymentChargeObj 
     * @return resultObject
     */
    public function updateCustomerPaymentCharge(customerTransactionObject $customerPaymentChargeObj){        
        try{
            
            $this->upateCustomerPaymentChargeDB($customerPaymentChargeObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPaymentChargeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerTransaction from DB
     * @param int $customerPaymentChargeID 
     * @return resultObject
     */
    public function deleteCustomerPaymentChargeById($customerPaymentChargeID){
        try{
            
            $this->deleteCustomerPaymentChargeByIdDB($customerPaymentChargeID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPaymentChargeID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Summary of getCustomerPaymentChargeByInvoiceID
     * @param mixed $customerInvoiceId 
     * @return resultObject
     */
    public function getCustomerPaymentChargeByInvoiceID($customerInvoiceId){
        
        try {
            
            $customerPaymentChargeData = $this->loadCustomerPaymentChargeByInvoiceIdFromDB($customerInvoiceId);
            
            $customerPaymentChargeObj = customerTransactionObject::withData($customerPaymentChargeData);
            $result = resultObject::withData(1,'',$customerPaymentChargeObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerInvoiceId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }
    /************************************* */
    /*   BASIC CUSTOMERTRANSACTION - DB METHODS           */
    /************************************* */

    private function addCustomerPaymentChargeDB(customerTransactionObject $obj){

        if (!isset($obj)){
            throw new Exception("customerPaymentChargeObject value must be provided");             
        }

        $obj->tr_paymentMethodLastDigits = $obj->tr_paymentMethodLastDigits == "" ? 0 : $obj->tr_paymentMethodLastDigits;

        $newId = $this->db->execute("INSERT INTO tbl_cust_transaction SET 
                                        tr_biz_id = {$obj->tr_biz_id},
                                        tr_device_id = '".addslashes($obj->tr_device_id)."',
                                        tr_cust_id = {$obj->tr_cust_id},
                                        tr_order_id = {$obj->tr_order_id},
                                        tr_invoice_id = {$obj->tr_invoice_id},
                                        tr_cust_payment_source_id = {$obj->tr_cust_payment_source_id},
                                        tr_reference_Id = '".addslashes($obj->tr_reference_Id)."',
                                        tr_capture_code = '".addslashes($obj->tr_capture_code)."',
                                        tr_reconciliation_Id = '".addslashes($obj->tr_reconciliation_Id)."',
                                        tr_type = '".addslashes($obj->tr_type)."',
                                        tr_amount = {$obj->tr_amount},
                                        tr_currency = '".addslashes($obj->tr_currency)."',
                                        tr_paymentMethodToken = '".addslashes($obj->tr_paymentMethodToken)."',
                                        tr_paymentMethodType = '".addslashes($obj->tr_paymentMethodType)."',
                                        tr_paymentMethodLastDigits = {$obj->tr_paymentMethodLastDigits},
                                        tr_cvv = '".addslashes($obj->tr_cvv)."',
                                        tr_vendor = '".addslashes($obj->tr_vendor)."',
                                        tr_ip = '".addslashes($obj->tr_ip)."',
                                        tr_country = {$obj->tr_country},
                                        tr_phone = '".addslashes($obj->tr_phone)."'
                                                 ");
        return $newId;
    }

    private function loadCustomerPaymentChargeFromDB($customerPaymentChargeID){

        if (!is_numeric($customerPaymentChargeID) || $customerPaymentChargeID <= 0){
            throw new Exception("Illegal value customerPaymentChargeID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction WHERE tr_id = $customerPaymentChargeID");
    }

    private function upateCustomerPaymentChargeDB(customerTransactionObject $obj){

        if (!isset($obj->tr_id) || !is_numeric($obj->tr_id) || $obj->tr_id <= 0){
            throw new Exception("customerPaymentChargeObject value must be provided");             
        }

        $tr_dateDate = isset($obj->tr_date) ? "'".$obj->tr_date."'" : "null";

        $this->db->execute("UPDATE tbl_cust_transaction SET 
                                tr_date = $tr_dateDate,
                                tr_biz_id = {$obj->tr_biz_id},
                                tr_device_id = '".addslashes($obj->tr_device_id)."',
                                tr_cust_id = {$obj->tr_cust_id},
                                tr_order_id = {$obj->tr_order_id},
                                tr_invoice_id = {$obj->tr_invoice_id},
                                tr_cust_payment_source_id = {$obj->tr_cust_payment_source_id},
                                tr_reference_Id = '".addslashes($obj->tr_reference_Id)."',
                                tr_capture_code = '".addslashes($obj->tr_capture_code)."',
                                tr_reconciliation_Id = '".addslashes($obj->tr_reconciliation_Id)."',
                                tr_type = '".addslashes($obj->tr_type)."',
                                tr_amount = {$obj->tr_amount},
                                tr_currency = '".addslashes($obj->tr_currency)."',
                                tr_paymentMethodToken = '".addslashes($obj->tr_paymentMethodToken)."',
                                tr_paymentMethodType = '".addslashes($obj->tr_paymentMethodType)."',
                                tr_paymentMethodLastDigits = {$obj->tr_paymentMethodLastDigits},
                                tr_cvv = '".addslashes($obj->tr_cvv)."',
                                tr_vendor = '".addslashes($obj->tr_vendor)."',
                                tr_ip = '".addslashes($obj->tr_ip)."',
                                tr_country = {$obj->tr_country},
                                tr_phone = '".addslashes($obj->tr_phone)."'
                                WHERE tr_id = {$obj->tr_id} 
                                         ");
    }

    private function deleteCustomerPaymentChargeByIdDB($customerPaymentChargeID){

        if (!is_numeric($customerPaymentChargeID) || $customerPaymentChargeID <= 0){
            throw new Exception("Illegal value customerPaymentChargeID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_transaction WHERE tr_id = $customerPaymentChargeID");
    }

    private function getTransactionForOrderByOrderIDDB($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items WHERE cini_order_id = $orderID";

        $invoiceItem = $this->db->getRow($sql);

        if(isset($invoiceItem['cini_invoice_id']) && $invoiceItem['cini_invoice_id'] > 0){
            $sql = "SELECT * FROM tbl_cust_transaction WHERE tr_invoice_id = {$invoiceItem['cini_invoice_id']}";
        }
        else{
            $sql = "SELECT * FROM tbl_cust_transaction WHERE tr_order_id = $orderID";
        }

        return $this->db->getRow($sql);
    }

    private function getChargeTransactionForOrderByOrderIDDB($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items WHERE cini_order_id = $orderID";

        $invoiceItem = $this->db->getRow($sql);

        if(isset($invoiceItem['cini_invoice_id']) && $invoiceItem['cini_invoice_id'] > 0){
            $sql = "SELECT * FROM tbl_cust_transaction WHERE tr_invoice_id = {$invoiceItem['cini_invoice_id']} AND tr_type='CHARGE'";
        }
        else{
            $sql = "SELECT * FROM tbl_cust_transaction WHERE tr_order_id = $orderID AND tr_type='CHARGE'";
        }

        return $this->db->getRow($sql);
    }

    private function loadCustomerPaymentChargeByInvoiceIdFromDB($customerInvoiceID){

        if (!is_numeric($customerInvoiceID) || $customerInvoiceID <= 0){
            throw new Exception("Illegal value customerInvoiceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction WHERE tr_invoice_id = $customerInvoiceID AND tr_type='CHARGE'");
    }
    /************************************* */
    /*   BASIC CUSTOMERTRANSACTIONHISTORY - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerTransactionHistory to DB
     * Return Data = new customerTransactionHistory ID
     * @param customerTransactionHistory $customerTransactionHistoryObj 
     * @return resultObject
     */
    public function addCustomerTransactionHistory(customerTransactionHistory $customerTransactionHistoryObj){       
        try{
            
            $newId = $this->addCustomerTransactionHistoryDB($customerTransactionHistoryObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerTransactionHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerTransactionHistory from DB for provided ID
     * * Return Data = customerTransactionHistory
     * @param int $customerTransactionHistoryId 
     * @return resultObject
     */
    public function getCustomerTransactionHistoryByID($customerTransactionHistoryId){
        
        try {
            
            $customerTransactionHistoryData = $this->loadCustomerTransactionHistoryFromDB($customerTransactionHistoryId);
            
            $customerTransactionHistoryObj = customerTransactionHistory::withData($customerTransactionHistoryData);
            $result = resultObject::withData(1,'',$customerTransactionHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerTransactionHistoryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerTransactionHistory in DB
     * @param customerTransactionHistory $customerTransactionHistoryObj 
     * @return resultObject
     */
    public function updateCustomerTransactionHistory(customerTransactionHistory $customerTransactionHistoryObj){        
        try{
            
            $this->upateCustomerTransactionHistoryDB($customerTransactionHistoryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerTransactionHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerTransactionHistory from DB
     * @param int $customerTransactionHistoryID 
     * @return resultObject
     */
    public function deleteCustomerTransactionHistoryById($customerTransactionHistoryID){
        try{
            
            $this->deleteCustomerTransactionHistoryByIdDB($customerTransactionHistoryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerTransactionHistoryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERTRANSACTIONHISTORY - DB METHODS           */
    /************************************* */

    private function addCustomerTransactionHistoryDB(customerTransactionHistory $obj){

        if (!isset($obj)){
            throw new Exception("customerTransactionHistory value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_transaction_order_history SET 
                                        ctoh_order_id = {$obj->ctoh_order_id},
                                        ctoh_status_id = {$obj->ctoh_status_id}
                                                 ");
        return $newId;
    }

    private function loadCustomerTransactionHistoryFromDB($customerTransactionHistoryID){

        if (!is_numeric($customerTransactionHistoryID) || $customerTransactionHistoryID <= 0){
            throw new Exception("Illegal value customerTransactionHistoryID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction_order_history,tbl_cust_transaction_order_status
                                    WHERE ctoh_status_id = ctos_id
                                    AND ctoh_id = $customerTransactionHistoryID");
    }

    private function upateCustomerTransactionHistoryDB(customerTransactionHistory $obj){

        if (!isset($obj->ctoh_id) || !is_numeric($obj->ctoh_id) || $obj->ctoh_id <= 0){
            throw new Exception("customerTransactionHistory value must be provided");             
        }

        $this->db->execute("UPDATE tbl_cust_transaction_order_history SET 
                                ctoh_order_id = {$obj->ctoh_order_id},
                                ctoh_status_id = {$obj->ctoh_status_id},
                                ctoh_status_date = '{$obj->ctoh_status_date}'
                                WHERE ctoh_id = {$obj->ctoh_id} 
                                         ");
    }

    private function deleteCustomerTransactionHistoryByIdDB($customerTransactionHistoryID){

        if (!is_numeric($customerTransactionHistoryID) || $customerTransactionHistoryID <= 0){
            throw new Exception("Illegal value customerTransactionHistoryID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_transaction_order_history WHERE ctoh_id = $customerTransactionHistoryID");
    }

    /************************************* */
    /*   BASIC CUSTOMERPAYMENTSOURCE - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerPaymentSourceObject to DB
     * Return Data = new customerPaymentSourceObject ID
     * @param customerPaymentSourceObject $customerPaymentSourceObj 
     * @return resultObject
     */
    public function addCustomerPaymentSource(customerPaymentSourceObject $customerPaymentSourceObj){       
        try{
            $existingSource = $this->checkIfCreditTokenExistsForCustomer($customerPaymentSourceObj->cps_cust_id,$customerPaymentSourceObj->cps_token);

            if($existingSource->code == 1){
                
                return resultObject::withData(1,'',$existingSource->data);
            }

            $newId = $this->addCustomerPaymentSourceDB($customerPaymentSourceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPaymentSourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerPaymentSource from DB for provided ID
     * * Return Data = customerPaymentSourceObject
     * @param int $customerPaymentSourceId 
     * @return resultObject
     */
    public function getCustomerPaymentSourceByPaySourceID($customerPaymentSourceId){
        
        try {
            $customerPaymentSourceData = $this->loadCustomerPaymentSourceFromDB($customerPaymentSourceId);
            
            $customerPaymentSourceObj = customerPaymentSourceObject::withData($customerPaymentSourceData);
            $result = resultObject::withData(1,'',$customerPaymentSourceObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPaymentSourceId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param customerPaymentSourceObject $customerPaymentSourceObj 
     * @return resultObject
     */
    public function updateCustomerPaymentSource(customerPaymentSourceObject $customerPaymentSourceObj){        
        try{
            $this->upateCustomerPaymentSourceDB($customerPaymentSourceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPaymentSourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerPaymentSource from DB
     * @param int $customerPaymentSourceID 
     * @return resultObject
     */
    public function deleteCustomerPaymentSourceById($customerPaymentSourceID){
        try{
            $this->deleteCustomerPaymentSourceByIdDB($customerPaymentSourceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPaymentSourceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERPAYMENTSOURCE - DB METHODS           */
    /************************************* */

    private function addCustomerPaymentSourceDB(customerPaymentSourceObject $obj){

        if (!isset($obj)){
            throw new Exception("customerPaymentSourceObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_customer_payment_sources SET 
                cps_cust_id = {$obj->cps_cust_id},
                cps_type = '".addslashes($obj->cps_type)."',
                cps_ip = '".addslashes($obj->cps_ip)."',
                cps_token = '".addslashes($obj->cps_token)."',
                cps_stipe_object_id = '".addslashes($obj->cps_stipe_object_id)."',
                cps_card_brand = '".addslashes($obj->cps_card_brand)."',
                cps_country = '".addslashes($obj->cps_country)."',
                cps_exp_month = {$obj->cps_exp_month},
                cps_exp_year = {$obj->cps_exp_year},
                cps_funding = '".addslashes($obj->cps_funding)."',
                cps_last4 = '".addslashes($obj->cps_last4)."',
                cps_name = '".addslashes($obj->cps_name)."',
                cps_phone = '".addslashes($obj->cps_phone)."',
                cps_valid = {$obj->cps_valid},
                cps_default = {$obj->cps_default}
                            ");
        return $newId;
    }

    private function loadCustomerPaymentSourceFromDB($customerPaymentSourceID){

        if (!is_numeric($customerPaymentSourceID) || $customerPaymentSourceID <= 0){
            throw new Exception("Illegal value $customerPaymentSourceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_payment_sources WHERE cps_id = $customerPaymentSourceID");
    }

    private function upateCustomerPaymentSourceDB(customerPaymentSourceObject $obj){

        if (!isset($obj->cps_id) || !is_numeric($obj->cps_id) || $obj->cps_id <= 0){
            throw new Exception("customerPaymentSourceObject value must be provided");             
        }

        $cps_createdDate = isset($obj->cps_created) ? "'".$obj->cps_created."'" : "null";

        $this->db->execute("UPDATE tbl_customer_payment_sources SET 
                cps_cust_id = {$obj->cps_cust_id},
                cps_type = '".addslashes($obj->cps_type)."',
                cps_created = $cps_createdDate,
                cps_ip = '".addslashes($obj->cps_ip)."',
                cps_token = '".addslashes($obj->cps_token)."',
                cps_stipe_object_id = '".addslashes($obj->cps_stipe_object_id)."',
                cps_card_brand = '".addslashes($obj->cps_card_brand)."',
                cps_country = '".addslashes($obj->cps_country)."',
                cps_exp_month = {$obj->cps_exp_month},
                cps_exp_year = {$obj->cps_exp_year},
                cps_funding = '".addslashes($obj->cps_funding)."',
                cps_last4 = '".addslashes($obj->cps_last4)."',
                cps_name = '".addslashes($obj->cps_name)."',
                cps_phone = '".addslashes($obj->cps_phone)."',
                cps_valid = {$obj->cps_valid},
                cps_default = {$obj->cps_default}
                WHERE cps_id = {$obj->cps_id} 
                         ");
    }

    private function deleteCustomerPaymentSourceByIdDB($customerPaymentSourceID){

        if (!is_numeric($customerPaymentSourceID) || $customerPaymentSourceID <= 0){
            throw new Exception("Illegal value $customerPaymentSourceID");             
        }

        $this->db->execute("DELETE FROM tbl_customer_payment_sources WHERE cps_id = $customerPaymentSourceID");
    }
    
}


