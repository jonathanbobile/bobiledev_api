<?php
/**
 * customerOrderManager short summary.
 *
 * customerOrderManager description.
 *
 * @version 1.0
 * @author JonathanM
 */

class customerOrderManager extends Manager
{
    function __construct()
    {
        parent::__construct();
    }

    /************************************* */
    /*   ORDER FLOWS - PUBLIC              */
    /************************************* */

    public static function initiateEmptyOrderForCustomer($custID,customerOrderObject $orderObject){
        try{
            $instance = new self();
            $orderObject->cto_biz_id = customerManager::getCustomerBizID($custID);
            $orderObject->cto_unique_id = $instance->generateUniuqueCustOrderId($custID,$orderObject->cto_biz_id);

            $orderObject->cto_id = $instance->addCustomerOrderDB($orderObject);

            $long_order_id = substr((string)time(), -8) . $orderObject->cto_id;

            $orderObject->cto_long_order_id = $long_order_id;

            $instance->upateCustomerOrderDB($orderObject);

            return resultObject::withData(1,'',$orderObject);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($orderObject));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function addCustItemsToOrder($orderID,$items){
        try{
            $instance = new self();
            foreach ($items as $item)
            {
            	$item->tri_order_id = $orderID;
                $item->tri_unique_id = $instance->generateUniqueCustItemId($orderID);
                $instance->addCustomerOrderItemDB($item);

            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function isOrderConnectedCustInvoiceFinalized($orderID){
        $instance = new self();
        $invoices = $instance->getConnectedCustInvoiceForCustOrderID($orderID);

        foreach ($invoices as $invoice)
        {
            if($invoice['cin_finalized'] == 1){
                return true;
            }
        }

        return false;
    }

    public static function processCustOrder($orderID){
        try{
            $instance = new self();
            $usedReward = false;
            $orderRow = $instance->loadCustomerOrderFromDB($orderID);

            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($orderRow['cto_cust_id']);

            $customer = $customerResult->data;

            $bizModel = new bizModel($orderRow['cto_biz_id']);
            $storeRow = $bizModel->getBizStoreSettings();

            $shopManager = new shopManager();

            if($orderRow['cto_benefit_id'] != 0 && $orderRow['cto_benefit_id'] != ""){//cart level discount was used - setting the reward to redeem

                $cartBenefitResult = customerManager::getCustomerBenefit($orderRow['cto_benefit_id']);
                if($cartBenefitResult->code == 1){
                    $cartRedeemBenefitResult = customerManager::redeemBenefitByID($orderRow['cto_benefit_id']);
                    $orderRow['benefit_label'] = customerManager::getBenefitLabel($orderRow['cto_benefit_id']);
                    $usedReward = true;
                    eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderRow['benefit_label'],$orderRow['cto_device_id'],$cartBenefitResult->data->cb_external_id);
                }
            }


            $orderItems = $instance->getOrderItemsForOrderByOrderIDDB($orderID);


            $isNonDelevarbleItem = false;

            foreach ($orderItems as $key =>$orderItem)
            {
                if($orderItem['tri_reward_redeem_code'] != ''){//a Reward was used for this item - setting the reward as redeemd.

                    $redeemCodeResult = customerManager::getBenefitByCode($orderItem['tri_reward_redeem_code']);
                    if($redeemCodeResult->code == 1){
                        $itemRedeemResult = customerManager::redeemBenefitWithCode($orderItem['tri_reward_redeem_code']);
                        $orderItems[$key]['benefit_label'] = $itemRedeemResult->data;
                        $usedReward = true;
                        eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderItems[$key]['benefit_label'],$orderRow['cto_device_id'],$redeemCodeResult->data->cb_external_id);
                    }
                }

                if(isset($orderItem['tri_multiuse_src_type']) && $orderItem['tri_multiuse_src_type'] != ''){//a subscription or punch pass was used for this item


                    if($orderItem['tri_multiuse_src_type'] == 'subscription'){
                        $subscriptionResult  = customerSubscriptionManager::getCustomerSubscriptionByUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($subscriptionResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $subUsed = $subscriptionResult->data;
                            $orderItem['multiuse_label'] = $subUsed->subscription->md_head;
                        }
                    }

                    if($orderItem['tri_multiuse_src_type'] == 'punch_pass'){
                        $punchpassResult = customerSubscriptionManager::getCustomerPunchpassFromUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($punchpassResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $ppassUsed = $punchpassResult->data;
                            $orderItem['multiuse_label'] = $ppassUsed->punchpass->md_head;
                        }
                    }

                }


                if($orderItem['tri_item_type'] == 1){//a coupon was purchased
                    $redeemCode = utilityManager::generateRedeemCode($orderRow['cto_cust_id']."1");

                    $orderItemObj = customerOrderItemObject::withData($orderItem);

                    $orderItemObj->tri_redeem_code = $redeemCode;

                    $instance->upateCustomerOrderItemDB($orderItemObj);

                    $benefitObj = new customerBenefitObject();

                    $benefitObj->cb_biz_id = $customer->cust_biz_id;
                    $benefitObj->cb_cust_id = $customer->cust_id;
                    $benefitObj->cb_type = "coupon";
                    $benefitObj->cb_external_id = $orderItem['tri_id'];
                    $benefitObj->cb_source_row_id = $orderItemObj->tri_itm_row_id;
                    $benefitObj->cb_granted_by = "purchase";
                    $benefitObj->cb_granted_by_id = $orderItemObj->tri_order_id;

                    $benefitObj->cb_code = $redeemCode;

                    $benefitResult = customerManager::addCustomerBenefit($benefitObj);
                    
                    if($benefitResult->code == 1){ 
                        couponsManager::addClaimToCoupon($orderItemObj->tri_itm_row_id,1);
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'coupon',$orderItemObj->tri_itm_row_id);
                        customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::benefits);
                        $instance->setItemExtTypeAndID($orderItem['tri_id'],'coupon',$benefitResult->data);
                    }
                    $isNonDelevarbleItem = true;
                }

                if($orderItem['tri_item_type'] == 2){//Retail Product
                    bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'product',$orderItem['tri_itm_row_id']);
                    if($storeRow->data->ess_track_stock == 1){
                        $shopManager->reduceProductStock($orderItem['tri_itm_row_id'],$orderItem['tri_quantity']);
                    }
                    customerManager::addCustomerLoyaltyStampToCustomerFromProductPurchase($customer,$orderItem['tri_itm_row_id'],$orderItem['tri_quantity'],$orderID);
                }


                if ( $orderItem['tri_item_type'] == 3 || $orderItem['tri_item_type'] == 0){//Paid Booking Service OR 'other' from payment request
                    $isNonDelevarbleItem = true;
                    continue;
                }


                if($orderItem['tri_item_type'] == 4){//a subscription was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $subscriptionDataResult = $bizM->getSubscriptionBySubscriptionID($orderItem['tri_itm_row_id']);
                    if($subscriptionDataResult->code == 1){
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'subscription',$orderItem['tri_itm_row_id']);
                        $custSubResult = customerSubscriptionManager::addSubscriptionToCustomerByCustomerID($orderRow['cto_cust_id'],$subscriptionDataResult->data);
                        if($custSubResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'subscription',$custSubResult->data->csu_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedSubscription,$orderRow['cto_cust_id'],'purchased_subscription',$subscriptionDataResult->data->md_head,$orderRow['cto_device_id'],$custSubResult->data->csu_id,'order',$orderRow['cto_id']);

                            $orderObj = customerOrderObject::withData($orderRow);

                            $orderObj->cto_charge_status = 'ongoing';

                            $instance->upateCustomerOrderDB($orderObj);
                        }
                    }
                    $isNonDelevarbleItem = true;
                }

                if($orderItem['tri_item_type'] == 5){//a punchpass was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $punchpassDataResult = $bizM->getPunchpassByPunchpassID($orderItem['tri_itm_row_id']);
                    if($punchpassDataResult->code == 1){
                        $custPunchResult = customerSubscriptionManager::addPunchpassToCustomerByCustomerID($orderRow['cto_cust_id'],$punchpassDataResult->data);
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'punchpass',$orderItem['tri_itm_row_id']);
                        if($custPunchResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'punch_pass',$custPunchResult->data->cpp_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedPunchPass,$orderRow['cto_cust_id'],'purchased_punch_pass',$punchpassDataResult->data->md_head,$orderRow['cto_device_id'],$custPunchResult->data->cpp_id,'order',$orderRow['cto_id']);
                        }
                    }
                    $isNonDelevarbleItem = true;
                }


            }


            if($isNonDelevarbleItem){
                $instance->updateOrderStatus($orderID,enumOrderStatus::NA);
            }
            else{
                $instance->updateOrderStatus($orderID,enumOrderStatus::received);
            }

            if($usedReward){
                customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$orderRow['cto_cust_id']);
            }
            customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$orderRow['cto_cust_id']);
            
            if($orderRow["cto_amount"] > 0){
                eventManager::checkAccumulated($customer,$orderRow["cto_amount"]);

                $customer->cust_total_spend += $orderRow["cto_amount"];

                $customerModel->updateCustomer($customer);
            }

            //Update badges and notifiactions + push to admin
            customerManager::addOneUnattendedOrderForCust($orderRow['cto_cust_id']);
            customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::orders);

            utilityManager::notifyBizOfAction($orderRow['cto_biz_id'],enumActions::newOrder);

            $notificationsManager = new notificationsManager();
            $notificationsManager->addNotification($orderRow['cto_biz_id'],enumActions::newOrder);

            //Grant inviter points
            $inviterPoints = $orderRow['cto_without_vat'] / 2;
            customerManager::grantInviterPoints($orderRow['cto_cust_id'],$inviterPoints);

            //Send emails
            $instance->sendOrderEmailToAdmin($orderRow,$orderItems,$customer);
            $instance->sendOrderEmailToCustomer($orderRow,$orderItems,$customer);

            return resultObject::withData(1,'',$orderID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

    }

    public static function processAdminOrder($orderID){
        try{
            $instance = new self();
            $usedReward = false;
            $orderRow = $instance->loadCustomerOrderFromDB($orderID);

            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($orderRow['cto_cust_id']);

            $customer = $customerResult->data;

            $bizModel = new bizModel($orderRow['cto_biz_id']);
            $storeRow = $bizModel->getBizStoreSettings();

            $shopManager = new shopManager();

            if($orderRow['cto_benefit_id'] != 0 && $orderRow['cto_benefit_id'] != ""){//cart level discount was used - setting the reward to redeem

                $cartBenefitResult = customerManager::getCustomerBenefit($orderRow['cto_benefit_id']);
                if($cartBenefitResult->code == 1){
                    $cartRedeemBenefitResult = customerManager::redeemBenefitByID($orderRow['cto_benefit_id']);
                    $orderRow['benefit_label'] = customerManager::getBenefitLabel($orderRow['cto_benefit_id']);
                    $usedReward = true;
                    eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderRow['benefit_label'],$orderRow['cto_device_id'],$cartBenefitResult->data->cb_external_id);
                }
            }


            $orderItems = $instance->getOrderItemsForOrderByOrderIDDB($orderID);

            foreach ($orderItems as $key =>$orderItem)
            {
                if($orderItem['tri_reward_redeem_code'] != ''){//a Reward was used for this item - setting the reward as redeemd.

                    $redeemCodeResult = customerManager::getBenefitByCode($orderItem['tri_reward_redeem_code']);
                    if($redeemCodeResult->code == 1){
                        $itemRedeemResult = customerManager::redeemBenefitWithCode($orderItem['tri_reward_redeem_code']);
                        $orderItems[$key]['benefit_label'] = $itemRedeemResult->data;
                        $usedReward = true;
                        eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderItems[$key]['benefit_label'],$orderRow['cto_device_id'],$redeemCodeResult->data->cb_external_id);
                    }
                }

                if(isset($orderItem['tri_multiuse_src_type']) && $orderItem['tri_multiuse_src_type'] != ''){//a subscription or punch pass was used for this item


                    if($orderItem['tri_multiuse_src_type'] == 'subscription'){
                        $subscriptionResult  = customerSubscriptionManager::getCustomerSubscriptionByUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($subscriptionResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $subUsed = $subscriptionResult->data;
                            $orderItem['multiuse_label'] = $subUsed->subscription->md_head;
                        }
                    }

                    if($orderItem['tri_multiuse_src_type'] == 'punch_pass'){
                        $punchpassResult = customerSubscriptionManager::getCustomerPunchpassFromUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($punchpassResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $ppassUsed = $punchpassResult->data;
                            $orderItem['multiuse_label'] = $ppassUsed->punchpass->md_head;
                        }
                    }

                }


                if($orderItem['tri_item_type'] == 1){//a coupon was purchased
                    $redeemCode = utilityManager::generateRedeemCode($orderRow['cto_cust_id']."1");

                    $orderItemObj = customerOrderItemObject::withData($orderItem);

                    $orderItemObj->tri_redeem_code = $redeemCode;

                    $instance->upateCustomerOrderItemDB($orderItemObj);

                    $benefitResult = customerManager::addRewardToCustomer($orderRow['cto_cust_id'],'coupon',$redeemCode,$orderItem['tri_id']);

                    if($benefitResult->code == 1){
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'coupon',$orderItemObj->tri_itm_row_id);
                        couponsManager::addClaimToCoupon($orderItemObj->tri_itm_row_id,1);
                        customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::benefits);
                        $instance->setItemExtTypeAndID($orderItem['tri_id'],'coupon',$benefitResult->data);
                    }

                }

                if($orderItem['tri_item_type'] == 2){//Retail Product
                    bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'product',$orderItem['tri_itm_row_id']);
                    if($storeRow->data->ess_track_stock == 1){
                        $shopManager->reduceProductStock($orderItem['tri_itm_row_id'],$orderItem['tri_quantity']);
                    }
                    customerManager::addCustomerLoyaltyStampToCustomerFromProductPurchase($customer,$orderItem['tri_itm_row_id'],$orderItem['tri_quantity'],$orderID);
                }


                if ($orderItem['tri_item_type'] == 3){//Paid Booking - add to customer's claims
                    //for ($i = 0; $i < $orderItem['tri_quantity']; $i++)
                    //{
                    //    $customerClaim = new customerClaimObject();

                    //    $customerClaim->cc_cust_id = $orderRow["cto_cust_id"];
                    //    $customerClaim->cc_biz_id = $orderRow["cto_biz_id"];
                    //    $customerClaim->cc_order_id = $orderRow["cto_id"];
                    //    $customerClaim->cc_order_item_id = $orderItem['tri_id'];
                    //    $customerClaim->cc_item_type = $orderItem['tri_item_type'];
                    //    $customerClaim->cc_item_source = "appointment";
                    //    $customerClaim->cc_item_source_id = $orderItem["tri_itm_row_id"];

                    //    customerManager::addCustomerClaim($customerClaim);
                    //}
                }

                if ($orderItem['tri_item_type'] == 0){//Paid 'other'

                    continue;
                }


                if($orderItem['tri_item_type'] == 4){//a subscription was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $subscriptionDataResult = $bizM->getSubscriptionBySubscriptionID($orderItem['tri_itm_row_id']);
                    if($subscriptionDataResult->code == 1){
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'subscription',$orderItem['tri_itm_row_id']);
                        $custSubResult = customerSubscriptionManager::addSubscriptionToCustomerByCustomerID($orderRow['cto_cust_id'],$subscriptionDataResult->data);
                        if($custSubResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'subscription',$custSubResult->data->csu_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedSubscription,$orderRow['cto_cust_id'],'purchased_subscription',$subscriptionDataResult->data->md_head,$orderRow['cto_device_id'],$custSubResult->data->csu_id);

                            $orderObj = customerOrderObject::withData($orderRow);

                            $orderObj->cto_charge_status = 'ongoing';

                            $instance->upateCustomerOrderDB($orderObj);
                        }
                    }

                }

                if($orderItem['tri_item_type'] == 5){//a punchpass was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $punchpassDataResult = $bizM->getPunchpassByPunchpassID($orderItem['tri_itm_row_id']);
                    if($punchpassDataResult->code == 1){
                        $custPunchResult = customerSubscriptionManager::addPunchpassToCustomerByCustomerID($orderRow['cto_cust_id'],$punchpassDataResult->data);
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'punchpass',$orderItem['tri_itm_row_id']);
                        if($custPunchResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'punch_pass',$custPunchResult->data->cpp_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedPunchPass,$orderRow['cto_cust_id'],'purchased_punch_pass',$punchpassDataResult->data->md_head,$orderRow['cto_device_id'],$custPunchResult->data->cpp_id);
                        }
                    }

                }


            }

            if($orderRow["cto_shipping_type_id"] == 0){
                $instance->updateOrderStatus($orderID,enumOrderStatus::NA,$orderRow['cto_account_id']);
            }
            else{
                $instance->updateOrderStatus($orderID,enumOrderStatus::received,$orderRow['cto_account_id']);
            }


            if($usedReward){
                customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$orderRow['cto_cust_id']);
            }
            customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$orderRow['cto_cust_id']);

            if($orderRow["cto_amount"] > 0){
                eventManager::checkAccumulated($customer,$orderRow["cto_amount"]);

                $customer->cust_total_spend += $orderRow["cto_amount"];

                $customerModel->updateCustomer($customer);
            }


            //Update badges and notifiactions + push to admin
            customerManager::addOneUnattendedOrderForCust($orderRow['cto_cust_id']);
            customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::orders);

            $notificationsManager = new notificationsManager();
            $notificationsManager->addNotification($orderRow['cto_biz_id'],enumActions::newOrder);


            //Grant inviter points
            $inviterPoints = $orderRow['cto_without_vat'] / 2;
            customerManager::grantInviterPoints($orderRow['cto_cust_id'],$inviterPoints);

            //Send emails
            $instance->sendOrderEmailToAdmin($orderRow,$orderItems,$customer);


            return resultObject::withData(1,'',$orderID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }

    }

    public static function processAdminPaymentRequest($orderID){
        try{
            $instance = new self();
            $usedReward = false;
            $orderRow = $instance->loadCustomerOrderFromDB($orderID);

            $orderObject = customerOrderObject::withData($orderRow);

            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($orderRow['cto_cust_id']);

            $customer = $customerResult->data;

            $bizModel = new bizModel($orderRow['cto_biz_id']);
            $storeRow = $bizModel->getBizStoreSettings();

            $shopManager = new shopManager();

            if($orderRow['cto_benefit_id'] != 0 && $orderRow['cto_benefit_id'] != ""){//cart level discount was used - setting the reward to redeem

                $cartBenefitResult = customerManager::getCustomerBenefit($orderRow['cto_benefit_id']);
                if($cartBenefitResult->code == 1){
                    $cartRedeemBenefitResult = customerManager::redeemBenefitByID($orderRow['cto_benefit_id']);
                    $orderRow['benefit_label'] = customerManager::getBenefitLabel($orderRow['cto_benefit_id']);
                    $usedReward = true;
                    eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderRow['benefit_label'],$orderRow['cto_device_id'],$cartBenefitResult->data->cb_external_id);
                }
            }


            $orderItems = $instance->getOrderItemsForOrderByOrderIDDB($orderID);

            foreach ($orderItems as $key =>$orderItem)
            {
                if($orderItem['tri_reward_redeem_code'] != ''){//a Reward was used for this item - setting the reward as redeemd.

                    $redeemCodeResult = customerManager::getBenefitByCode($orderItem['tri_reward_redeem_code']);
                    if($redeemCodeResult->code == 1){
                        $itemRedeemResult = customerManager::redeemBenefitWithCode($orderItem['tri_reward_redeem_code']);
                        $orderItems[$key]['benefit_label'] = $itemRedeemResult->data;
                        $usedReward = true;
                        eventManager::actionTrigger(enumCustomerActions::redeemedCoupon,$orderRow['cto_cust_id'],'coupon_redeemed',$orderItems[$key]['benefit_label'],$orderRow['cto_device_id'],$redeemCodeResult->data->cb_external_id);
                    }
                }

                if(isset($orderItem['tri_multiuse_src_type']) && $orderItem['tri_multiuse_src_type'] != ''){//a subscription or punch pass was used for this item


                    if($orderItem['tri_multiuse_src_type'] == 'subscription'){
                        $subscriptionResult  = customerSubscriptionManager::getCustomerSubscriptionByUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($subscriptionResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $subUsed = $subscriptionResult->data;
                            $orderItem['multiuse_label'] = $subUsed->subscription->md_head;
                        }
                    }

                    if($orderItem['tri_multiuse_src_type'] == 'punch_pass'){
                        $punchpassResult = customerSubscriptionManager::getCustomerPunchpassFromUsageID($orderItem['tri_multiuse_src_usage_id']);
                        if($punchpassResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $ppassUsed = $punchpassResult->data;
                            $orderItem['multiuse_label'] = $ppassUsed->punchpass->md_head;
                        }
                    }

                }
            }

            if($usedReward){
                customerManager::sendAsyncCustomerDataUpdateToDevice('benefits',$orderRow['cto_cust_id']);
            }

            $orderObject->cto_order_type = "eCommerce";

            self::updateCustomerOrder($orderObject);

            customerManager::sendAsyncCustomerDataUpdateToDevice('orders',$orderRow['cto_cust_id']);


            //Update badges
            customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::orders);

            return resultObject::withData(1,'',$orderID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function handlePaymentRequestOrder($orderID){
        try{
            $instance = new self();
            $orderRow = $instance->loadCustomerOrderFromDB($orderID);

            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($orderRow['cto_cust_id']);

            $customer = $customerResult->data;

            $bizModel = new bizModel($orderRow['cto_biz_id']);
            $storeRow = $bizModel->getBizStoreSettings();

            $shopManager = new shopManager();

            $orderItems = $instance->getOrderItemsForOrderByOrderIDDB($orderID);

            foreach ($orderItems as $key =>$orderItem)
            {
                if($orderItem['tri_item_type'] == 1){//a coupon was purchased
                    $redeemCode = utilityManager::generateRedeemCode($orderRow['cto_cust_id']."1");

                    $orderItemObj = customerOrderItemObject::withData($orderItem);

                    $orderItemObj->tri_redeem_code = $redeemCode;

                    $instance->upateCustomerOrderItemDB($orderItemObj);

                    $benefitResult = customerManager::addRewardToCustomer($orderRow['cto_cust_id'],'coupon',$redeemCode,$orderItem['tri_id']);

                    if($benefitResult->code == 1){
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'coupon',$orderItemObj->tri_itm_row_id);
                        couponsManager::addClaimToCoupon($orderItemObj->tri_itm_row_id,1);
                        customerManager::addBadgeToCustomer($orderRow['cto_cust_id'],enumBadges::benefits);
                        $instance->setItemExtTypeAndID($orderItem['tri_id'],'coupon',$benefitResult->data);
                    }

                }

                if($orderItem['tri_item_type'] == 2){//Retail Product
                    bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'product',$orderItem['tri_itm_row_id']);
                    if($storeRow->data->ess_track_stock == 1){
                        $shopManager->reduceProductStock($orderItem['tri_itm_row_id'],$orderItem['tri_quantity']);
                    }
                    customerManager::addCustomerLoyaltyStampToCustomerFromProductPurchase($customer,$orderItem['tri_itm_row_id'],$orderItem['tri_quantity'],$orderID);
                }


                if ($orderItem['tri_item_type'] == 3){//Paid Booking - add to customer's claims

                }

                if ($orderItem['tri_item_type'] == 0){//Paid 'other'

                    continue;
                }


                if($orderItem['tri_item_type'] == 4){//a subscription was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $subscriptionDataResult = $bizM->getSubscriptionBySubscriptionID($orderItem['tri_itm_row_id']);
                    if($subscriptionDataResult->code == 1){
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'subscription',$orderItem['tri_itm_row_id']);
                        $custSubResult = customerSubscriptionManager::addSubscriptionToCustomerByCustomerID($orderRow['cto_cust_id'],$subscriptionDataResult->data);
                        if($custSubResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'subscription',$custSubResult->data->csu_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedSubscription,$orderRow['cto_cust_id'],'purchased_subscription',$subscriptionDataResult->data->md_head,$orderRow['cto_device_id'],$custSubResult->data->csu_id);

                            $orderObj = customerOrderObject::withData($orderRow);

                            $orderObj->cto_charge_status = 'ongoing';

                            $instance->upateCustomerOrderDB($orderObj);
                        }
                    }

                }

                if($orderItem['tri_item_type'] == 5){//a punchpass was purchased
                    $bizM = new bizModel($orderRow['cto_biz_id']);
                    $punchpassDataResult = $bizM->getPunchpassByPunchpassID($orderItem['tri_itm_row_id']);
                    if($punchpassDataResult->code == 1){
                        $custPunchResult = customerSubscriptionManager::addPunchpassToCustomerByCustomerID($orderRow['cto_cust_id'],$punchpassDataResult->data);
                        bizManager::addBizOfferingUsage($orderRow['cto_biz_id'],'punchpass',$orderItem['tri_itm_row_id']);
                        if($custPunchResult->code == 1){
                            customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$orderRow['cto_cust_id']);
                            $instance->setItemExtTypeAndID($orderItem['tri_id'],'punch_pass',$custPunchResult->data->cpp_id);
                            eventManager::actionTrigger(enumCustomerActions::purchasedPunchPass,$orderRow['cto_cust_id'],'purchased_punch_pass',$punchpassDataResult->data->md_head,$orderRow['cto_device_id'],$custPunchResult->data->cpp_id);
                        }
                    }

                }


            }

            if($orderRow["cto_shipping_type_id"] == 0){
                $instance->updateOrderStatus($orderID,enumOrderStatus::NA,$orderRow['cto_account_id']);
            }
            else{
                $instance->updateOrderStatus($orderID,enumOrderStatus::received,$orderRow['cto_account_id']);
            }


            $notificationsManager = new notificationsManager();
            $notificationsManager->addNotification($orderRow['cto_biz_id'],enumActions::newOrder);

            //Send emails
            $instance->sendOrderEmailToAdmin($orderRow,$orderItems,$customer);


            return resultObject::withData(1,'',$orderID);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update the items of the order to the external type and external ID
     * If the item_id is not 0, only the specified item will be updated
     * @param mixed $order_id
     * @param mixed $ext_type
     * @param mixed $ext_id
     * @param mixed $item_id
     * @return resultObject
     */
    public static function updateOrderitemsExternalTypeAndID($order_id,$ext_type,$ext_id,$item_id = 0){
        try{
            $instance = new self();

            $instance->setOrderItemExtTypeAndIDDB($order_id,$ext_type,$ext_id,$item_id);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['orderID'] = $order_id;
            $data['extType'] = $ext_type;
            $data['extID'] = $ext_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAvailableItemtoConnectMeetingFromOrderByTypeAndServiceID($orderID,$serviceID){
        try{
            $instance = new self();
            $sql = "SELECT * FROM tbl_cust_transaction_items
                        LEFT JOIN (SELECT COUNT(*) as cnt,em_payment_source_inner_id as connected_item FROM tbl_employee_meeting
                                    WHERE em_payment_source = 'order'
                                    AND em_payment_source_id = $orderID
                                    AND em_meet_type = $serviceID
                                    GROUP BY em_payment_source_inner_id) s ON s.connected_item = tri_id
                        WHERE tri_order_id = $orderID
                        AND tri_item_type = 3
                        AND tri_itm_row_id = $serviceID
                        AND (cnt IS NULL OR cnt < tri_quantity)
                        ORDER BY tri_id ASC
                        LIMIT 1";

            $item = $instance->db->getRow($sql);

            if(isset($item["tri_id"])){
                $itemObject = customerOrderItemObject::withData($item);

                return resultObject::withData(1,'',$itemObject);
            }

            return resultObject::withData(0,'no_item');
        }
        catch(Exception $e){
            $data = array();
            $data['orderID'] = $orderID;
            $data['source'] = $item_source;
            $data['source_row_id'] = $item_source_row_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getAvailableItemtoConnectClassFromOrderByTypeAndClassID($orderID,$classID){
        try{
            $instance = new self();
            $sql = "SELECT * FROM tbl_cust_transaction_items
                        LEFT JOIN (SELECT COUNT(*) as cnt,cdc_payment_source_inner_id as connected_item FROM tbl_class_date_customers
                                    WHERE cdc_payment_source = 'order'
                                    AND cdc_payment_source_id = $orderID
                                    AND cdc_class_id = $classID
                                    GROUP BY cdc_payment_source_inner_id) s ON s.connected_item = tri_id
                        WHERE tri_order_id = $orderID
                        AND tri_item_type = 3
                        AND tri_itm_row_id = $classID
                        AND (cnt IS NULL OR cnt < tri_quantity)
                        ORDER BY tri_id ASC
                        LIMIT 1";

            $item = $instance->db->getRow($sql);

            if(isset($item["tri_id"])){
                $itemObject = customerOrderItemObject::withData($item);

                return resultObject::withData(1,'',$itemObject);
            }

            return resultObject::withData(0,'no_item');
        }
        catch(Exception $e){
            $data = array();
            $data['orderID'] = $orderID;
            $data['source'] = $item_source;
            $data['source_row_id'] = $item_source_row_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function sendCustomerOrderEmailByOrderLongID($orderLongID,$email,$invoiceLongID = ""){
        try{
            $instance = new self();
            $order = $instance->loadCustomerOrderFromDBByLongID($orderLongID);
            $items = $instance->getOrderItemsForOrderByOrderIDDB($order["cto_id"]);

            $customerModel = new customerModel();

            $customerResult = $customerModel->getCustomerWithID($order['cto_cust_id']);

            $customer = $customerResult->data;

            $bizCountryID = bizManager::getBizCountryID($order['cto_biz_id']);

            $orderDate = utilityManager::getFormattedTimeByCountryID($bizCountryID,$order['cto_order_tyme']);

            $itemsToMail = $instance->createOrderMailBody($order,$items);

            $extraParams["cust_name"] = $customer->cust_first_name;
            $extraParams["order_id"] = $order["cto_long_order_id"];;
            $extraParams["tax"] = $order["cto_vat"];
            $extraParams["currency"] = $order["cto_currency"];
            $extraParams["total"] = $order["cto_amount"];
            $extraParams["order_date"] = $orderDate;
            $extraParams["items"] = $itemsToMail;
            $extraParams["ship_country"] = $order["cto_addr_country"];
            $extraParams["ship_state"] = $order["cto_addr_state"];
            $extraParams["ship_city"] = $order["cto_addr_city"];
            $extraParams["ship_address1"] = $order["cto_addr_line1"];
            $extraParams["ship_address2"] = $order["cto_addr_line2"];
            $extraParams["ship_zip"] = $order["cto_addr_postcode"];
            $extraParams["ship_name"] = $order["cto_addr_name"];
            $extraParams["ship_phone"] = $order["cto_addr_phone"];
            $extraParams["cust_email"] = $email;
            $extraParams["cust_phone"] = $customer->cust_phone2;

            $extraParams["to_name"] = $customer->cust_first_name;
            $extraParams["to_email"] = $email;



            emailManager::sendSystemMailApp($order['cto_biz_id'],151,enumEmailType::SystemMailSGCare,$extraParams);

            return resultObject::withData(1);
        }
        catch(Exception $ex){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderLongID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function sendPaymentRequestToCustomer(customerOrderObject $payRequest){
        try{
            eventManager::actionTrigger(enumCustomerActions::receivedPaymentRequest,$payRequest->cto_cust_id,'received_pay_request',$payRequest->cto_amount.' '.$payRequest->cto_currency,'',$payRequest->cto_id);
            $message = pushManager::getSystemAlertPushMessageText(28,$payRequest->cto_biz_id);

            pushManager::sendPushBiz($payRequest->cto_biz_id,$payRequest->cto_cust_id,enumPushType::biz_personalPRequest,$message);

            return resultObject::withData(1);
        }
        catch(Exception $ex){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($payRequest));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getLastCustomerOrder(bizMemberObject $customer){
        try{
            $instance = new self();

            $ordersList = $instance->getCustomerOriginalOrdersDB($customer,0,1);

            if(count($ordersList) == 0){
                return resultObject::withData(0,'no_orders');
            }

            $order = customerOrderObject::withData($ordersList[0]);

            return resultObject::withData(1,'',$order);
        }
        catch(Exception $ex){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   ORDER FLOWS - PRIVATE              */
    /************************************* */

    private function generateUniuqueCustOrderId($custID,$bizID){
        $timeLast5Digits = time() % 10000;
        $toScramble = $bizID.$custID.$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 15){
            $scrambled .= rand();
        }

        $output = 'bbord_'.substr($scrambled,0,15);

        return $output;
    }

    private function generateUniqueCustItemId($orderID){

        $order = self::loadCustomerOrderFromDB($orderID);

        $timeLast5Digits = time() % 10000;
        $toScramble = $order['cto_biz_id'].$order['cto_cust_id'].$timeLast5Digits;
        $scrambled = utilityManager::encryptIt($toScramble,true);
        while(strlen($scrambled) < 15){
            $scrambled .= rand();
        }

        $output = 'bbitem_'.substr($scrambled,0,15);

        return $output;
    }

    private function getConnectedCustInvoiceForCustOrderID($orderID){
        $sql = "SELECT * FROM tbl_cust_invoice_items,tbl_cust_invoice
                WHERE cini_invoice_id = cin_id
                AND cini_order_id = $orderID";

        return $this->db->getTable($sql);
    }

    private function setItemExtTypeAndID($item_id,$ext_type,$ext_id){
        $sql = "UPDATE tbl_cust_transaction_items SET
                                tri_item_ext_type = '$ext_type',
                                tri_item_ext_id = $ext_id
                            WHERE tri_id = $item_id";

        $this->db->execute($sql);
    }

    private function setOrderItemExtTypeAndIDDB($order_id,$ext_type,$ext_id,$item_id = 0){
        $itemSql = "";
        if($item_id > 0){
            $itemSql = " AND tri_id = $item_id";
        }

        $sql = "UPDATE tbl_cust_transaction_items SET
                                tri_item_ext_type = '$ext_type',
                                tri_item_ext_id = $ext_id
                            WHERE tri_order_id = $order_id
                            $itemSql";

        $this->db->execute($sql);
    }

    private function updateOrderStatus($orderID,$status,$accountID = 0,$notes = ""){

        $this->db->execute("INSERT INTO tbl_cust_transaction_order_history SET
                                        ctoh_order_id=$orderID,
                                        ctoh_status_id=$status,
                                        ctoh_status_account_id = $accountID,
                                        ctoh_status_notes = '$notes'");

        $this->db->execute("UPDATE tbl_cust_transaction_order SET
                                cto_status = $status
                            WHERE cto_id=$orderID");

        return;
    }

    private function sendOrderEmailToAdmin($order,$items,$customer){


        $itemsToMail = $this->createOrderMailBody($order,$items);
        $bizCountryID = bizManager::getBizCountryID($order['cto_biz_id']);

        $orderDate = utilityManager::getFormattedTimeByCountryID($bizCountryID,$order['cto_order_tyme']);

        $extraParams["cust_name"] = $customer->cust_first_name;
        $extraParams["order_id"] = $order["cto_long_order_id"];;
        $extraParams["tax"] = $order["cto_vat"];
        $extraParams["currency"] = $order["cto_currency"];
        $extraParams["total"] = $order["cto_amount"];
        $extraParams["order_date"] = $orderDate;
        $extraParams["items"] = $itemsToMail;
        $extraParams["ship_country"] = $order["cto_addr_country"];
        $extraParams["ship_state"] = $order["cto_addr_state"];
        $extraParams["ship_city"] = $order["cto_addr_city"];
        $extraParams["ship_address1"] = $order["cto_addr_line1"];
        $extraParams["ship_address2"] = $order["cto_addr_line2"];
        $extraParams["ship_zip"] = $order["cto_addr_postcode"];
        $extraParams["ship_name"] = $order["cto_addr_name"];
        $extraParams["ship_phone"] = $order["cto_addr_phone"];
        $extraParams["cust_email"] = $customer->cust_email;
        $extraParams["cust_phone"] = $customer->cust_phone2;

        //biz owner
        emailManager::sendSystemMailApp($order['cto_biz_id'],147,enumEmailType::SystemMailSGCare,$extraParams);

        return;
    }

    private function sendOrderEmailToCustomer($order,$items,$customer){


        $itemsToMail = $this->createOrderMailBody($order,$items);
        $bizCountryID = bizManager::getBizCountryID($order['cto_biz_id']);

        $orderDate = utilityManager::getFormattedTimeByCountryID($bizCountryID,$order['cto_order_tyme']);

        $extraParams["cust_name"] = $customer->cust_first_name;
        $extraParams["order_id"] = $order["cto_long_order_id"];;
        $extraParams["tax"] = $order["cto_vat"];
        $extraParams["currency"] = $order["cto_currency"];
        $extraParams["total"] = $order["cto_amount"];
        $extraParams["order_date"] = $orderDate;
        $extraParams["items"] = $itemsToMail;
        $extraParams["ship_country"] = $order["cto_addr_country"];
        $extraParams["ship_state"] = $order["cto_addr_state"];
        $extraParams["ship_city"] = $order["cto_addr_city"];
        $extraParams["ship_address1"] = $order["cto_addr_line1"];
        $extraParams["ship_address2"] = $order["cto_addr_line2"];
        $extraParams["ship_zip"] = $order["cto_addr_postcode"];
        $extraParams["ship_name"] = $order["cto_addr_name"];
        $extraParams["ship_phone"] = $order["cto_addr_phone"];
        $extraParams["cust_email"] = $customer->cust_email;
        $extraParams["cust_phone"] = $customer->cust_phone2;

        //customer
        $extraParams["to_name"] = $customer->cust_first_name;
        $extraParams["to_email"] = $customer->cust_email;
        emailManager::sendSystemMailApp($order['cto_biz_id'],151,enumEmailType::SystemMailSGCare,$extraParams);
        return;
    }

    private function createOrderMailBody($order,$items){

        $orderMailBody = "<table width=\"100%\" cellspacing=\"0\">
                            <tr>
                                <td width=\"45%\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Item</td>
                                <td width=\"15%\" style=\"text-align:center; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Qty</td>
                                <td width=\"20%\" style=\"text-align:center; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Price <span style=\"font-size:9pt;\">per unit</span></td>
                                <td width=\"20%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-bottom:1px solid #a8a8a8; font-size:16px;\">Total</td>
                            </tr>";

        $orderTotal = 0;

        foreach ($items as $item)
        {

            $totalRow = $item['tri_quantity'] * $item['tri_price'];
            $orderTotal += $totalRow;
        	$totalLine = "$totalRow <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            $paddingBottom = "padding-bottom:5px;";

            $priceLine = $item['tri_price'];
            if($item['tri_orig_price'] != $item['tri_price']){

                $priceLine = $item['tri_orig_price'];
            }

            $rewardRow = '';
            if(isset($item['benefit_label'])){

                $rewardLine = '';
                if($item['tri_reward_type'] == 'discount'){
                    $rewardLine = "({$item['tri_reward_value']}% discount)";
                }
                if($item['tri_reward_type'] == 'item'){
                    $rewardLine = "({$item['tri_reward_value']} free)";
                }

                $rewardRow = "<br><span style=\"font-size: 9pt; color: #555555;\"><span style=\"font-weight:600;\">Reward used</span>: {$item['benefit_label']} $rewardLine</span>";
                $paddingBottom = "padding-bottom:0px;";
                $totalLine = "<span style=\"text-decoration:line-through;\">{$item['tri_orig_price']}</span> <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span><br>{$item['tri_price']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            }

            if(isset($item['multiuse_label'])){

                $rewardLine = "Used {$item['multiuse_label']}";


                $rewardRow = "<br><span style=\"font-size: 9pt; color: #555555;\"><span style=\"font-weight:600;\">Reward used</span>: {$item['benefit_label']} $rewardLine</span>";
                $paddingBottom = "padding-bottom:0px;";
                $totalLine = "<span style=\"text-decoration:line-through;\">{$item['tri_orig_price']}</span> <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span><br>{$item['tri_price']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span>";
            }


            $orderMailBody .= " <tr>
                                <td width=\"45%\" height=\"60px\" style=\"text-align:left; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; vertical-align:top; padding-top:20px; $paddingBottom \">{$item['tri_name']}<br><span style=\"font-size: 9pt; color: #555555;\">SKU: {$item['tri_item_sku']}</span><br><span style=\"font-size: 9pt; color: #555555; line-height:10pt\">{$item['tri_variation_values']}</span>$rewardRow</td>
                                <td width=\"15%\" height=\"60px\" style=\"text-align:center; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif;  font-size:16px; vertical-align:top; padding-top:20px;\">{$item['tri_quantity']}</td>
                                <td width=\"20%\" height=\"60px\" style=\"text-align:center; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; vertical-align:top; padding-top:20px;\">$priceLine <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
                                <td width=\"20%\" height=\"60px\" style=\"text-align:right; color: #000; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif;  font-size:16px; vertical-align:top; padding-top:20px; padding-right:5px;\">$totalLine</td>

                            </tr>
                            ";
        }

        $subtotal = $orderTotal;
        $orderTotal = $orderTotal + $order['cto_vat'] + $order['cto_sipping'] - $order['cto_membership_discount'] - $order['cto_benefit_value'];

        $orderMailBody .= "
            <tr>
                <td colspan=\"4\" width=\"100%\" height=\"15px\"></td>
            </tr>
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px; padding-top:15px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px; padding-top:15px;\">Subtotal</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; border-top:1px solid #a8a8a8; font-size:16px;padding-right:5px; padding-top:15px;\">$subtotal <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>    ";

        if($order['cto_membership_discount'] > 0){
            $orderMailBody .= "
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; line-height:18px;\">Membership Discount</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">-{$order['cto_membership_discount']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>";
        }


        if($order['cto_benefit_value'] > 0 && $order['cto_benefit_id'] > 0){
            $orderMailBody .= "
            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; line-height:18px;\">{$order['benefit_label']}</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">-{$order['cto_benefit_value']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>";
        }

        $orderMailBody .= "<tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\">Shipping</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">{$order['cto_sipping']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;\">Tax</td>
                <td width=\"25%\" style=\"text-align:right; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px;\">{$order['cto_vat']} <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

            <tr>
                <td colspan=\"2\" width=\"60%\" height=\"30px\" style=\"text-align:left; color: #a8a8a8; font-size:18px; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; padding-top:10px;\"></td>
                <td width=\"15%\" style=\"text-align:right; color: #1875d2; font-size:18px; font-weight:600; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px; padding-top:10px;\">Order Total</td>
                <td width=\"25%\" style=\"text-align:right; color: #1875d2; font-size:18px; font-weight:600; font-family: 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', 'DejaVu Sans', Verdana, sans-serif; font-size:16px;padding-right:5px; padding-top:10px;\">$orderTotal <span style=\"font-size: 9pt;\">{$order["cto_currency"]}</span></td>
            </tr>

        </table>";

        return $orderMailBody;

    }

    /************************************* */
    /*   BASIC CUSTOMERORDERITEM - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerOrderItemObject to DB
    * Return Data = new customerOrderItemObject ID
    * @param customerOrderItemObject $customerOrderItemObj
    * @return resultObject
    */
    public static function addCustomerOrderItem(customerOrderItemObject $customerOrderItemObj){
            try{
                $instance = new self();
                $newId = $instance->addCustomerOrderItemDB($customerOrderItemObj);
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderItemObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerOrderItem from DB for provided ID
    * * Return Data = customerOrderItemObject
    * @param int $customerOrderItemId
    * @return resultObject
    */
    public static function getCustomerOrderItemByID($customerOrderItemId){

            try {
                $instance = new self();
                $customerOrderItemData = $instance->loadCustomerOrderItemFromDB($customerOrderItemId);

                $customerOrderItemObj = customerOrderItemObject::withData($customerOrderItemData);
                $result = resultObject::withData(1,'',$customerOrderItemObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderItemId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update customerOrderItem in DB
    * @param customerOrderItemObject $customerOrderItemObj
    * @return resultObject
    */
    public static function updateCustomerOrderItem(customerOrderItemObject $customerOrderItemObj){
            try{
                $instance = new self();
                $instance->upateCustomerOrderItemDB($customerOrderItemObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderItemObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerOrderItem from DB
    * @param int $customerOrderItemID
    * @return resultObject
    */
    public static function deleteCustomerOrderItemById($customerOrderItemID){
            try{
                $instance = new self();
                $instance->deleteCustomerOrderItemByIdDB($customerOrderItemID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderItemID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC CUSTOMERORDERITEM - DB METHODS           */
    /************************************* */

    private function addCustomerOrderItemDB(customerOrderItemObject $obj){

    if (!isset($obj)){
                throw new Exception("customerOrderItemObject value must be provided");
            }

    $tri_redeem_dateDate = isset($obj->tri_redeem_date) ? "'".$obj->tri_redeem_date."'" : "null";

    $newId = $this->db->execute("INSERT INTO tbl_cust_transaction_items SET
                                    tri_order_id = {$obj->tri_order_id},
                                    tri_name = '".addslashes($obj->tri_name)."',
                                    tri_unique_id = '".addslashes($obj->tri_unique_id)."',
                                    tri_attribute_values = '".addslashes($obj->tri_attribute_values)."',
                                    tri_quantity = '".addslashes($obj->tri_quantity)."',
                                    tri_price = {$obj->tri_price},
                                    tri_orig_price = {$obj->tri_orig_price},
                                    tri_without_vat = {$obj->tri_without_vat},
                                    tri_vat = {$obj->tri_vat},
                                    tri_itm_row_id = {$obj->tri_itm_row_id},
                                    tri_item_id = {$obj->tri_item_id},
                                    tri_item_type = '{$obj->tri_item_type}',
                                    tri_redeem_code = '".addslashes($obj->tri_redeem_code)."',
                                    tri_redeem_date = $tri_redeem_dateDate,
                                    tri_variation_id = {$obj->tri_variation_id},
                                    tri_variation_values = '".addslashes($obj->tri_variation_values)."',
                                    tri_item_sku = '".addslashes($obj->tri_item_sku)."',
                                    tri_reward_redeem_code = '".addslashes($obj->tri_reward_redeem_code)."',
                                    tri_reward_Id = {$obj->tri_reward_Id},
                                    tri_reward_type = '".addslashes($obj->tri_reward_type)."',
                                    tri_reward_value = '".addslashes($obj->tri_reward_value)."',
                                    tri_item_ext_type = '{$obj->tri_item_ext_type}',
                                    tri_item_source_row_id = {$obj->tri_item_source_row_id},
                                    tri_item_ext_id = {$obj->tri_item_ext_id},
                                    tri_multiuse_src_type = '{$obj->tri_multiuse_src_type}',
                                    tri_multiuse_src_usage_id = {$obj->tri_multiuse_src_usage_id}
                                             ");
             return $newId;
        }

    private function loadCustomerOrderItemFromDB($customerOrderItemID){

        if (!is_numeric($customerOrderItemID) || $customerOrderItemID <= 0){
            throw new Exception("Illegal value customerOrderItemID");
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction_items WHERE tri_id = $customerOrderItemID");
    }

    private function upateCustomerOrderItemDB(customerOrderItemObject $obj){

    if (!isset($obj->tri_id) || !is_numeric($obj->tri_id) || $obj->tri_id <= 0){
                throw new Exception("customerOrderItemObject value must be provided");
            }

    $tri_redeem_dateDate = isset($obj->tri_redeem_date) ? "'".$obj->tri_redeem_date."'" : "null";

    $this->db->execute("UPDATE tbl_cust_transaction_items SET
                            tri_order_id = {$obj->tri_order_id},
                            tri_name = '".addslashes($obj->tri_name)."',
                            tri_unique_id = '".addslashes($obj->tri_unique_id)."',
                            tri_attribute_values = '".addslashes($obj->tri_attribute_values)."',
                            tri_quantity = '".addslashes($obj->tri_quantity)."',
                            tri_price = {$obj->tri_price},
                            tri_orig_price = {$obj->tri_orig_price},
                            tri_without_vat = {$obj->tri_without_vat},
                            tri_vat = {$obj->tri_vat},
                            tri_itm_row_id = {$obj->tri_itm_row_id},
                            tri_item_id = {$obj->tri_item_id},
                            tri_item_type = {$obj->tri_item_type},
                            tri_redeem_code = '".addslashes($obj->tri_redeem_code)."',
                            tri_redeem_date = $tri_redeem_dateDate,
                            tri_variation_id = {$obj->tri_variation_id},
                            tri_variation_values = '".addslashes($obj->tri_variation_values)."',
                            tri_item_sku = '".addslashes($obj->tri_item_sku)."',
                            tri_reward_redeem_code = '".addslashes($obj->tri_reward_redeem_code)."',
                            tri_reward_Id = {$obj->tri_reward_Id},
                            tri_reward_type = '".addslashes($obj->tri_reward_type)."',
                            tri_reward_value = '".addslashes($obj->tri_reward_value)."',
                            tri_item_ext_type = '{$obj->tri_item_ext_type}',
                            tri_item_source_row_id = {$obj->tri_item_source_row_id},
                            tri_item_ext_id = {$obj->tri_item_ext_id},
                            tri_multiuse_src_type = '{$obj->tri_multiuse_src_type}',
                            tri_multiuse_src_usage_id = {$obj->tri_multiuse_src_usage_id}
                            WHERE tri_id = {$obj->tri_id}
                                     ");
        }

    private function deleteCustomerOrderItemByIdDB($customerOrderItemID){

            if (!is_numeric($customerOrderItemID) || $customerOrderItemID <= 0){
                throw new Exception("Illegal value customerOrderItemID");
            }

            $this->db->execute("DELETE FROM tbl_cust_transaction_items WHERE tri_id = $customerOrderItemID");
        }

    private function getOrderItemsForOrderByOrderIDDB($orderID){
        if (!is_numeric($orderID) || $orderID <= 0){
            throw new Exception("Illegal value orderID");
        }

        return $this->db->getTable("SELECT * FROM tbl_cust_transaction_items WHERE tri_order_id = $orderID");
    }

    private function deleteOrderItemsForOrderID($orderID){
        $this->db->execute("DELETE FROM tbl_cust_transaction_items where tri_order_id = $orderID");
    }

    /************************************* */
    /*   BASIC CUSTOMERORDER - PUBLIC           */
    /************************************* */

    /**
    * Insert new customerOrderObject to DB
    * Return Data = new customerOrderObject ID
    * @param customerOrderObject $customerOrderObj
    * @return resultObject
    */
    public static function addCustomerOrder(customerOrderObject $customerOrderObj){
            try{
                $instance = new self();
                $newId = $instance->addCustomerOrderDB($customerOrderObj);
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get customerOrder from DB for provided ID
    * * Return Data = customerOrderObject
    * @param int $customerOrderId
    * @return resultObject
    */
    public static function getCustomerOrderByID($customerOrderId){

            try {
                $instance = new self();
                $customerOrderData = $instance->loadCustomerOrderFromDB($customerOrderId);

                $customerOrderObj = customerOrderObject::withData($customerOrderData);

                $itemRows = $instance->getOrderItemsForOrderByOrderIDDB($customerOrderId);

                $itemsList = array();

                foreach ($itemRows as $itemData)
                {
                	$item = customerOrderItemObject::withData($itemData);
                    $itemsList[] = $item;
                }

                $customerOrderObj->setItems($itemsList);

                $result = resultObject::withData(1,'',$customerOrderObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    public static function getCustomerOrderByLongID($customerOrderLongId){

        try {
            $instance = new self();
            $customerOrderData = $instance->loadCustomerOrderFromDBByLongID($customerOrderLongId);

            $customerOrderObj = customerOrderObject::withData($customerOrderData);

            $itemRows = $instance->getOrderItemsForOrderByOrderIDDB($customerOrderObj->cto_id);

            $itemsList = array();

            foreach ($itemRows as $itemData)
            {
                $item = customerOrderItemObject::withData($itemData);
                $itemsList[] = $item;
            }

            $customerOrderObj->setItems($itemsList);

            $result = resultObject::withData(1,'',$customerOrderObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderLongId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Update customerOrder in DB
    * @param customerOrderObject $customerOrderObj
    * @return resultObject
    */
    public static function updateCustomerOrder(customerOrderObject $customerOrderObj){
            try{
                $instance = new self();
                $instance->upateCustomerOrderDB($customerOrderObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete customerOrder from DB
    * @param int $customerOrderID
    * @return resultObject
    */
    public static function deleteCustomerOrderById($customerOrderID){
        try{
            $instance = new self();
            $instance->deleteOrderItemsForOrderID($customerOrderID);
            $instance->deleteCustomerOrderByIdDB($customerOrderID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function emptyCustomerOrderFromItems($customerOrderID){
        try{
            $instance = new self();
            $instance->deleteOrderItemsForOrderID($customerOrderID);

            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get the count of customers orders for the last X days
     * Return Data = count of the orders
     * @param mixed $cust_id
     * @param mixed $period
     * @return resultObject
     */
    public static function getCustTransactionsOrderCountForLastXDays($cust_id,$days){

        try {
            $instance = new self();
            $cnt = $instance->getCustTransactionsOrderCountForLastXDays_DB($cust_id,$days);

            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get the count of customers orders
     * Return Data = count of the orders
     * @param mixed $cust_id
     * @param mixed $period
     * @return resultObject
     */
    public static function getCustTransactionsOrderCount($cust_id){

        try {
            $instance = new self();
            $cnt = $instance->getCustTransactionsOrderCount_DB($cust_id);

            $result = resultObject::withData(1,'',$cnt);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getOrdersForAdminForBiz($bizID,$from,$to,$filter = "",$skip = 0,$take = 50){
        try{
            $instance = new self();

            $start_date = date( "Y-m-d 00:00:00", $from);
            $end_date = date( "Y-m-d 23:59:00", $to);

            $ordersRows = $instance->getBizOrdersDB($bizID,$start_date,$end_date,$filter,$skip,$take);

            $result = array();
            foreach ($ordersRows as $customerOrderData)
            {
            	$orderObject = customerOrderObject::withData($customerOrderData);
                $result[] = $orderObject->AdminAPIArray();
            }

            return resultObject::withData(1,'',$result);
        }
        //catch exception
        catch(Exception $e) {
            $data = array();
            $data['biz_id'] = $bizID;
            $data['skip'] = $skip;
            $data['take'] = $take;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getOrdersSearchForAdminForBiz($bizID,$from,$to,$key = "",$skip = 0,$take = 50){
        try{
            $instance = new self();

            $start_date = date( "Y-m-d 00:00:00", $from);
            $end_date = date( "Y-m-d 23:59:00", $to);

            $ordersRows = $instance->getBizOrdersSearchDB($bizID,$start_date,$end_date,$key,$skip,$take);

            $result = array();
            foreach ($ordersRows as $customerOrderData)
            {
            	$orderObject = customerOrderObject::withData($customerOrderData);
                $result[] = $orderObject->AdminAPIArray();
            }

            return resultObject::withData(1,'',$result);
        }
        //catch exception
        catch(Exception $e) {
            $data = array();
            $data['biz_id'] = $bizID;
            $data['skip'] = $skip;
            $data['take'] = $take;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getOrdersForAdminHandlingForBiz($bizID,$from,$to,$filter = "",$skip = 0,$take = 20){
        try{
            $instance = new self();

            $start_date = date( "Y-m-d 00:00:00", $from);
            $end_date = date( "Y-m-d 23:59:00", $to);

            $ordersRows = $instance->getBizOrdersDBForHandling($bizID,$start_date,$end_date,$filter,$skip,$take);

            $result = array();
            foreach ($ordersRows as $customerOrderData)
            {
            	$orderObject = customerOrderObject::withData($customerOrderData);

                $orderObject->setConnectedAccountIDToCurrentHandlerAccountID();

                $itemRows = $instance->getOrderItemsForOrderByOrderIDDB($orderObject->cto_id);

                foreach ($itemRows as $itemRow)
                {
                	$orderObject->items[] = customerOrderItemObject::withData($itemRow);
                }

                $result[] = $orderObject->AdminAPIOrderHandlingArray();
            }

            return resultObject::withData(1,'',$result);
        }
        //catch exception
        catch(Exception $e) {
            $data = array();
            $data['biz_id'] = $bizID;
            $data['skip'] = $skip;
            $data['take'] = $take;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerPersonalZoneOrders(customerObject $customer,$filterType = '',$skip = 0,$take = 0){
        try{
            $instance = new self();
            $orderRows = $instance->getCustomerOrdersDB($customer,$filterType,$skip,$take);
            $ordersList = array();

            $bizModel = new bizModel($customer->cust_biz_id);

            $bizObjResult = $bizModel->getBiz();

            if($bizObjResult->code == 0){
                return resultObject::withData(0,'no_biz');
            }
            $bizobj = $bizObjResult->data;
            $storeSettingsResult = $bizModel->getBizStoreSettings();
            $storeSettings = $storeSettingsResult->data;

            foreach ($orderRows as $orderRow)
            {
            	$order = customerOrderObject::withData($orderRow);

                if($filterType != "paymentRequest" && $order->cto_due_date == ""){
                    $orderTime = new DateTime($order->cto_order_tyme);
                    $addHours = ($storeSettings->ess_shipping_time * 24) + $bizobj->biz_time_zone;
                    $orderTime->add(new DateInterval("PT{$addHours}H"));
                    $order->cto_due_date = $orderTime->format('Y-m-d H:i:s');
                }
                $itemsRows = $instance->getOrderItemsForOrderByOrderIDDB($order->cto_id);

                $order->items = array();
                foreach ($itemsRows as $itemRow)
                {
                	$item = customerOrderItemObject::withData($itemRow);
                    $order->items[] = $item;
                }


                $ordersList[] = $order;
            }

            return resultObject::withData(1,'',$ordersList);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerAdminOrders(customerObject $customer,$filterType = '',$skip = 0,$take = 0){
        try{
            $instance = new self();
            $orderRows = $instance->getCustomerOrdersDB($customer,$filterType,$skip,$take);
            $ordersList = array();

            foreach ($orderRows as $orderRow)
            {
                $order = customerOrderObject::withData($orderRow);
                $ordersList[] = $order->AdminAPIArray();
            }

            return resultObject::withData(1,'',$ordersList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerAdminpaymentRequests(customerObject $customer,$skip = 0,$take = 0){
        try{
            $instance = new self();
            $ordersList = array();

            if($skip == 0){
                $draft = $instance->getCustomerOpenDraft($customer);

                if(isset($draft['cto_id'])){
                    $order = customerOrderObject::withData($draft);
                    $ordersList[] = $order->AdminPayRequestAPIArray();
                }
            }

            $orderRows = $instance->getCustomerFinalizedPayRequests($customer,$skip,$take);


            foreach ($orderRows as $orderRow)
            {
                $order = customerOrderObject::withData($orderRow);
                $ordersList[] = $order->AdminPayRequestAPIArray();
            }

            return resultObject::withData(1,'',$ordersList);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function customerHasOpenDraft(customerObject $customer){
        try{
            $instance = new self();

            $draftCount = $instance->getCustomerOpenDraftCount($customer);

            $hasOpenDraft = $draftCount > 0;

            $code = $hasOpenDraft ? 1 : 0;

            return resultObject::withData($code,'',$hasOpenDraft);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerOpenDraftForAdmin(customerObject $customer){
        try{
            $instance = new self();

            $draftRow = $instance->getCustomerOpenDraft($customer);

            if(isset($draftRow['cto_id'])){
                $order = customerOrderObject::withData($draftRow);

                return resultObject::withData(1,'',$order->AdminSingleOrderAPIArray());
            }
            else{
                return resultObject::withData(0,'no_draft');
            }
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customer));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getOrderStatusLog(customerOrderObject $order){
        try{
            $instance = new self();

            $statusLogs = $instance->getStatusLogsForOrderID($order->cto_id);

            $result = array();

            foreach ($statusLogs as $statusLog)
            {
            	$result[] = customerOrderStatusHistory::withData($statusLog);
            }

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($order));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getOrdersDistribuationByStatusForBizID($bizID){
        try{
            $instance = new self();

            $result = $instance->getOrdersDistribuationByStatusForBizID_DB($bizID);

            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERORDER - DB METHODS           */
    /************************************* */

    private function addCustomerOrderDB(customerOrderObject $obj){

    if (!isset($obj)){
        throw new Exception("customerOrderObject value must be provided");
    }

    $cto_due_dateDate = isset($obj->cto_due_date) ? "'".$obj->cto_due_date."'" : "null";

    $cto_next_reminderDate = isset($obj->cto_next_reminder) ? "'".$obj->cto_next_reminder."'" : "null";

    $cto_seen_onDate = isset($obj->cto_seen_on) ? "'".$obj->cto_seen_on."'" : "null";

    $cto_amended_onDate = isset($obj->cto_amended_on) ? "'".$obj->cto_amended_on."'" : "null";

    $cto_finalized_onDate = isset($obj->cto_finalized_on) ? "'".$obj->cto_finalized_on."'" : "null";
    $cto_request_sent_onDate = isset($obj->cto_request_sent_on) ? "'".$obj->cto_request_sent_on."'" : "null";

    $obj->cto_charge_status = $obj->cto_charge_status != '' ? $obj->cto_charge_status : 'one_time';

    $newId = $this->db->execute("INSERT INTO tbl_cust_transaction_order SET
                                    cto_amount = {$obj->cto_amount},
                                    cto_without_vat = {$obj->cto_without_vat},
                                    cto_vat = {$obj->cto_vat},
                                    cto_sipping = {$obj->cto_sipping},
                                    cto_membership_discount = {$obj->cto_membership_discount},
                                    cto_benefit_id = {$obj->cto_benefit_id},
                                    cto_benefit_value = {$obj->cto_benefit_value},
                                    cto_shipping_type_id = {$obj->cto_shipping_type_id},
                                    cto_currency = '".addslashes($obj->cto_currency)."',
                                    cto_device_id = '".addslashes($obj->cto_device_id)."',
                                    cto_cust_id = {$obj->cto_cust_id},
                                    cto_biz_id = {$obj->cto_biz_id},
                                    cto_cancelled = {$obj->cto_cancelled},
                                    cto_status = {$obj->cto_status},
                                    cto_charge_status = '{$obj->cto_charge_status}',
                                    cto_request_status = '{$obj->cto_request_status}',
                                    cto_request_sent = '{$obj->cto_request_sent}',
                                    cto_finalized_on = $cto_finalized_onDate,
                                    cto_request_sent_on = $cto_request_sent_onDate,
                                    cto_note = '".addslashes($obj->cto_note)."',
                                    cto_addr_name = '".addslashes($obj->cto_addr_name)."',
                                    cto_addr_country = '".addslashes($obj->cto_addr_country)."',
                                    cto_addr_state = '".addslashes($obj->cto_addr_state)."',
                                    cto_addr_city = '".addslashes($obj->cto_addr_city)."',
                                    cto_addr_line1 = '".addslashes($obj->cto_addr_line1)."',
                                    cto_addr_line2 = '".addslashes($obj->cto_addr_line2)."',
                                    cto_addr_pob = {$obj->cto_addr_pob},
                                    cto_addr_postcode = '".addslashes($obj->cto_addr_postcode)."',
                                    cto_addr_phone = '".addslashes($obj->cto_addr_phone)."',
                                    cto_delivery_details = '".addslashes($obj->cto_delivery_details)."',
                                    cto_delivery_tracking_id = '".addslashes($obj->cto_delivery_tracking_id)."',
                                    cto_buyer_email = '".addslashes($obj->cto_buyer_email)."',
                                    cto_long_order_id = '".addslashes($obj->cto_long_order_id)."',
                                    cto_paid_manual = {$obj->cto_paid_manual},
                                    cto_order_type = '{$obj->cto_order_type}',
                                    cto_original_type = '{$obj->cto_original_type}',
                                    cto_due_date = $cto_due_dateDate,
                                    cto_title = '".addslashes($obj->cto_title)."',
                                    cto_unique_id = '".addslashes($obj->cto_unique_id)."',
                                    cto_reminder_type = '{$obj->cto_reminder_type}',
                                    cto_reminder_time = '{$obj->cto_reminder_time}',
                                    cto_next_reminder = $cto_next_reminderDate,
                                    cto_reminder_text = '".addslashes($obj->cto_reminder_text)."',
                                    cto_discount = {$obj->cto_discount},
                                    cto_account_id = {$obj->cto_account_id},
                                    cto_seen_on = $cto_seen_onDate,
                                    cto_amended_on = $cto_amended_onDate
                                             ");
             return $newId;
        }

    private function loadCustomerOrderFromDB($customerOrderID){

        if (!is_numeric($customerOrderID) || $customerOrderID <= 0){
            throw new Exception("Illegal value customerOrderID");
        }

        return $this->db->getRow("SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                                    LEFT JOIN tbl_ship_method ON cto_shipping_type_id = ship_method_id
                                    WHERE cto_status=ors_id
                                    AND cto_id = $customerOrderID");
    }

    private function loadCustomerOrderFromDBByLongID($customerOrderLongID){

        return $this->db->getRow("SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                                    LEFT JOIN tbl_ship_method ON cto_shipping_type_id = ship_method_id
                                    WHERE cto_status=ors_id
                                    AND cto_long_order_id = '$customerOrderLongID'");
    }

    private function upateCustomerOrderDB(customerOrderObject $obj){

    if (!isset($obj->cto_id) || !is_numeric($obj->cto_id) || $obj->cto_id <= 0){
                throw new Exception("customerOrderObject value must be provided");
            }

    $cto_order_tymeDate = isset($obj->cto_order_tyme) ? "'".$obj->cto_order_tyme."'" : "null";

    $cto_status_dateDate = isset($obj->cto_status_date) ? "'".$obj->cto_status_date."'" : "null";

    $cto_due_dateDate = isset($obj->cto_due_date) ? "'".$obj->cto_due_date."'" : "null";

    $cto_next_reminderDate = isset($obj->cto_next_reminder) ? "'".$obj->cto_next_reminder."'" : "null";

    $cto_seen_onDate = isset($obj->cto_seen_on) ? "'".$obj->cto_seen_on."'" : "null";

    $cto_amended_onDate = isset($obj->cto_amended_on) ? "'".$obj->cto_amended_on."'" : "null";

    $cto_finalized_onDate = isset($obj->cto_finalized_on) ? "'".$obj->cto_finalized_on."'" : "null";
    $cto_request_sent_onDate = isset($obj->cto_request_sent_on) ? "'".$obj->cto_request_sent_on."'" : "null";

    $obj->cto_charge_status = $obj->cto_charge_status != '' ? $obj->cto_charge_status : 'one_time';

    $this->db->execute("UPDATE tbl_cust_transaction_order SET
                            cto_amount = {$obj->cto_amount},
                            cto_without_vat = {$obj->cto_without_vat},
                            cto_original_amount = {$obj->cto_original_amount},
                            cto_vat = {$obj->cto_vat},
                            cto_sipping = {$obj->cto_sipping},
                            cto_membership_discount = {$obj->cto_membership_discount},
                            cto_benefit_id = {$obj->cto_benefit_id},
                            cto_benefit_value = {$obj->cto_benefit_value},
                            cto_shipping_type_id = {$obj->cto_shipping_type_id},
                            cto_currency = '".addslashes($obj->cto_currency)."',
                            cto_order_tyme = $cto_order_tymeDate,
                            cto_device_id = '".addslashes($obj->cto_device_id)."',
                            cto_cust_id = {$obj->cto_cust_id},
                            cto_biz_id = {$obj->cto_biz_id},
                            cto_cancelled = {$obj->cto_cancelled},
                            cto_status = {$obj->cto_status},
                            cto_charge_status = '{$obj->cto_charge_status}',
                            cto_request_status = '{$obj->cto_request_status}',
                            cto_request_sent = '{$obj->cto_request_sent}',
                            cto_finalized_on = $cto_finalized_onDate,
                            cto_request_sent_on = $cto_request_sent_onDate,
                            cto_note = '".addslashes($obj->cto_note)."',
                            cto_status_date = $cto_status_dateDate,
                            cto_addr_name = '".addslashes($obj->cto_addr_name)."',
                            cto_addr_country = '".addslashes($obj->cto_addr_country)."',
                            cto_addr_state = '".addslashes($obj->cto_addr_state)."',
                            cto_addr_city = '".addslashes($obj->cto_addr_city)."',
                            cto_addr_line1 = '".addslashes($obj->cto_addr_line1)."',
                            cto_addr_line2 = '".addslashes($obj->cto_addr_line2)."',
                            cto_addr_pob = {$obj->cto_addr_pob},
                            cto_addr_postcode = '".addslashes($obj->cto_addr_postcode)."',
                            cto_addr_phone = '".addslashes($obj->cto_addr_phone)."',
                            cto_delivery_details = '".addslashes($obj->cto_delivery_details)."',
                            cto_delivery_tracking_id = '".addslashes($obj->cto_delivery_tracking_id)."',
                            cto_buyer_email = '".addslashes($obj->cto_buyer_email)."',
                            cto_long_order_id = '".addslashes($obj->cto_long_order_id)."',
                            cto_paid_manual = {$obj->cto_paid_manual},
                            cto_order_type = '{$obj->cto_order_type}',
                            cto_original_type = '{$obj->cto_original_type}',
                            cto_due_date = $cto_due_dateDate,
                            cto_title = '".addslashes($obj->cto_title)."',
                            cto_unique_id = '".addslashes($obj->cto_unique_id)."',
                            cto_reminder_type = '{$obj->cto_reminder_type}',
                            cto_reminder_time = '{$obj->cto_reminder_time}',
                            cto_next_reminder = $cto_next_reminderDate,
                            cto_reminder_text = '".addslashes($obj->cto_reminder_text)."',
                            cto_discount = {$obj->cto_discount},
                            cto_seen_on = $cto_seen_onDate,
                            cto_amended_on = $cto_amended_onDate
                            WHERE cto_id = {$obj->cto_id}
                                     ");
        }

    private function deleteCustomerOrderByIdDB($customerOrderID){

            if (!is_numeric($customerOrderID) || $customerOrderID <= 0){
                throw new Exception("Illegal value customerOrderID");
            }

            $this->db->execute("DELETE FROM tbl_cust_transaction_order WHERE cto_id = $customerOrderID");
        }

    private function getCustTransactionsOrderCountForLastXDays_DB($cust_id,$days = 0){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");
        }

        return $this->db->getVal("SELECT COUNT(*)
                                    FROM tbl_cust_transaction_order
                                    WHERE cto_cust_id = $cust_id
                                    AND cto_order_tyme >= (NOW() - INTERVAL $days DAY)");
    }

    private function getCustTransactionsOrderCount_DB($cust_id){

        if (!is_numeric($cust_id) || $cust_id <= 0){
            throw new Exception("Illegal value customer ID");
        }

        return $this->db->getVal("SELECT COUNT(*)
                                    FROM tbl_cust_transaction_order
                                    WHERE cto_cust_id = $cust_id");
    }

    private function getCustomerOrdersDB(customerObject $customer,$filterType = '',$skip = 0,$take = 0){
        $filter = "";
        if($filterType != ""){
            $filter = " AND cto_order_type='$filterType' ";
        }

        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_status=ors_id
                AND cto_biz_id={$customer->cust_biz_id}
                AND cto_cust_id={$customer->cust_id}
                $filter
                ORDER BY cto_order_tyme DESC
                $limit";

        return $this->db->getTable($sql);
    }

    private function getCustomerOriginalOrdersDB(bizMemberObject $customer,$skip = 0,$take = 0){

        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_status=ors_id
                AND cto_biz_id={$customer->cust_biz_id}
                AND cto_cust_id={$customer->cust_id}
                AND cto_original_type='eCommerce'
                ORDER BY cto_order_tyme DESC
                $limit";

        return $this->db->getTable($sql);
    }

    private function getCustomerFinalizedPayRequests(customerObject $customer,$skip = 0,$take = 0){

        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }


        $sql = "SELECT * FROM tbl_cust_transaction_order
                WHERE cto_cust_id = {$customer->cust_id}
                AND cto_biz_id = {$customer->cust_biz_id}
                AND cto_original_type = 'paymentRequest'
                AND cto_request_status = 'finalized'
                ORDER BY cto_order_tyme ASC
                $limit";



        return $this->db->getTable($sql);
    }

    private function getCustomerOpenDraftCount(customerObject $customer){
        $sql = "SELECT COUNT(*) FROM tbl_cust_transaction_order
                WHERE cto_cust_id = {$customer->cust_id}
                AND cto_biz_id = {$customer->cust_biz_id}
                AND cto_order_type = 'paymentRequest'
                AND cto_request_status = 'draft'";

        return $this->db->getVal($sql);
    }

    private function getCustomerOpenDraft(customerObject $customer){
        $sql = "SELECT * FROM tbl_cust_transaction_order
                WHERE cto_cust_id = {$customer->cust_id}
                AND cto_biz_id = {$customer->cust_biz_id}
                AND cto_order_type = 'paymentRequest'
                AND cto_request_status = 'draft'";


        return $this->db->getRow($sql);
    }

    private function getBizOrdersDB($bizID,$from,$to,$filter = "",$skip = 0,$take = 50){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $filterSQL = "";
        if($filter != ""){
            $filterSQL = " AND cto_status IN ($filter)";
        }

        $sql = "SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_status=ors_id
                AND cto_biz_id={$bizID}
                AND cto_order_type = 'eCommerce'
                AND cto_order_tyme >= '$from'
                AND cto_order_tyme <= '$to'
                $filterSQL
                ORDER BY cto_order_tyme DESC
                $limit";



        return $this->db->getTable($sql);
    }

    private function getBizOrdersSearchDB($bizID,$from,$to,$key = "",$skip = 0,$take = 50){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }



        $sql = "SELECT * FROM tbl_order_status,tbl_customers,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_cust_id = cust_id
                AND cto_status=ors_id
                AND cto_biz_id={$bizID}
                AND cto_order_type = 'eCommerce'
                AND cto_order_tyme >= '$from'
                AND cto_order_tyme <= '$to'
                AND (LOWER(cust_email) LIKE LOWER('%$key%') OR cust_phone1 LIKE '%$key%' OR LOWER(cust_first_name) LIKE LOWER('%$key%') OR cto_long_order_id LIKE '%$key%')
                ORDER BY cto_order_tyme DESC
                $limit";



        return $this->db->getTable($sql);
    }

    private function getBizOrdersDBForHandling($bizID,$from,$to,$filter = "",$skip = 0,$take = 50){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $filterSQL = "";
        if($filter != ""){
            $filterSQL = " AND cto_status IN ($filter)";
        }

        $sql = "SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_status=ors_id
                AND cto_biz_id={$bizID}
                AND cto_order_type = 'eCommerce'
                AND cto_order_tyme >= '$from'
                AND cto_order_tyme <= '$to'
                AND cto_cancelled = 0
                $filterSQL
                ORDER BY cto_order_tyme DESC
                $limit";



        return $this->db->getTable($sql);
    }

    private function getBizPaymentRequestsDB($bizID,$skip = 0,$take = 50){
        $limit = "";
        if($take > 0){
            $limit = " LIMIT $skip,$take";
        }

        $sql = "SELECT * FROM tbl_order_status,tbl_cust_transaction_order
                LEFT JOIN tbl_ship_method on cto_shipping_type_id = ship_method_id
                WHERE cto_status=ors_id
                AND cto_biz_id={$bizID}
                AND cto_order_type = 'paymentRequest'
                ORDER BY cto_order_tyme DESC
                $limit";

        return $this->db->getTable($sql);
    }

    private function getStatusLogsForOrderID($orderID){
        return $this->db->getTable("SELECT * FROM tbl_cust_transaction_order_history,tbl_order_status
                WHERE ctoh_status_id = ors_id
                AND ctoh_order_id = $orderID
                ORDER BY ctoh_status_date DESC");
    }

    private function getOrdersDistribuationByStatusForBizID_DB($bizID){
        return $this->db->getTable("SELECT ors_id as status_id, IF(cnt IS NULL,0,cnt) as count FROM tbl_order_status LEFT JOIN (
                                        SELECT COUNT(*) as cnt,cto_status FROM tbl_cust_transaction_order
                                        WHERE cto_biz_id = $bizID
                                        GROUP BY cto_status) as s on s.cto_status = ors_id");
    }

    /************************************* */
    /*   BASIC CUSTOMERORDERSTATUSHISTORY - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerOrderStatusHistory to DB
     * Return Data = new customerOrderStatusHistory ID
     * @param customerOrderStatusHistory $customerOrderStatusHistoryObj
     * @return resultObject
     */
    public static function addCustomerOrderStatusHistory(customerOrderStatusHistory $customerOrderStatusHistoryObj){
        try{
            $instance = new self();
            $newId = $instance->addCustomerOrderStatusHistoryDB($customerOrderStatusHistoryObj);
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderStatusHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerOrderStatusHistory from DB for provided ID
     * * Return Data = customerOrderStatusHistory
     * @param int $customerOrderStatusHistoryId
     * @return resultObject
     */
    public static function getCustomerOrderStatusHistoryByID($customerOrderStatusHistoryId){

        try {
            $instance = new self();
            $customerOrderStatusHistoryData = $instance->loadCustomerOrderStatusHistoryFromDB($customerOrderStatusHistoryId);

            $customerOrderStatusHistoryObj = customerOrderStatusHistory::withData($customerOrderStatusHistoryData);
            $result = resultObject::withData(1,'',$customerOrderStatusHistoryObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderStatusHistoryId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param customerOrderStatusHistory $customerOrderStatusHistoryObj
     * @return resultObject
     */
    public static function updateCustomerOrderStatusHistory(customerOrderStatusHistory $customerOrderStatusHistoryObj){
        try{
            $instance = new self();
            $instance->upateCustomerOrderStatusHistoryDB($customerOrderStatusHistoryObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerOrderStatusHistoryObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerOrderStatusHistory from DB
     * @param int $customerOrderStatusHistoryID
     * @return resultObject
     */
    public static function deleteCustomerOrderStatusHistoryById($customerOrderStatusHistoryID){
        try{
            $instance = new self();
            $instance->deleteCustomerOrderStatusHistoryByIdDB($customerOrderStatusHistoryID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerOrderStatusHistoryID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getLatestStatusHistoryForOrderID($orderID){
        try{
            $instance = new self();
            $statusHistoryRow = $instance->getLatestStatusHistoryForOrderIDDB($orderID);

            if(isset($statusHistoryRow['ctoh_id']) && $statusHistoryRow['ctoh_id'] != ""){
                $statusHistory = customerOrderStatusHistory::withData($statusHistoryRow);
                $result = resultObject::withData(1,'',$statusHistory);
            }
            else{
                $result = resultObject::withData(0);
            }
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$orderID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /*********************************************************** */
    /*   BASIC CUSTOMERORDERSTATUSHISTORY - DB METHODS           */
    /*********************************************************** */

    private function addCustomerOrderStatusHistoryDB(customerOrderStatusHistory $obj){

        if (!isset($obj)){
            throw new Exception("customerOrderStatusHistory value must be provided");
        }

        $newId = $this->db->execute("INSERT INTO tbl_cust_transaction_order_history SET
                                        ctoh_order_id = {$obj->ctoh_order_id},
                                        ctoh_status_id = {$obj->ctoh_status_id},
                                        ctoh_status_account_id = {$obj->ctoh_status_account_id},
                                        ctoh_status_notes = '".addslashes($obj->ctoh_status_notes)."'");
        return $newId;
    }

    private function loadCustomerOrderStatusHistoryFromDB($customerOrderStatusHistoryID){

        if (!is_numeric($customerOrderStatusHistoryID) || $customerOrderStatusHistoryID <= 0){
            throw new Exception("Illegal value $customerOrderStatusHistoryID");
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction_order_history WHERE ctoh_id = $customerOrderStatusHistoryID");
    }

    private function upateCustomerOrderStatusHistoryDB(customerOrderStatusHistory $obj){

        if (!isset($obj->ctoh_id) || !is_numeric($obj->ctoh_id) || $obj->ctoh_id <= 0){
            throw new Exception("customerOrderStatusHistory value must be provided");
        }

        $this->db->execute("UPDATE tbl_cust_transaction_order_history SET
                                ctoh_order_id = {$obj->ctoh_order_id},
                                ctoh_status_id = {$obj->ctoh_status_id},
                                ctoh_status_date = '{$obj->ctoh_status_date}',
                                ctoh_status_account_id = {$obj->ctoh_status_account_id},
                                ctoh_status_notes = '".addslashes($obj->ctoh_status_notes)."'
                                WHERE ctoh_id = {$obj->ctoh_id}");
    }

    private function deleteCustomerOrderStatusHistoryByIdDB($customerOrderStatusHistoryID){

        if (!is_numeric($customerOrderStatusHistoryID) || $customerOrderStatusHistoryID <= 0){
            throw new Exception("Illegal value $customerOrderStatusHistoryID");
        }

        $this->db->execute("DELETE FROM tbl_cust_transaction_order_history WHERE ctoh_id = $customerOrderStatusHistoryID");
    }

    private function getLatestStatusHistoryForOrderIDDB($orderID){

        return $this->db->getRow("SELECT * FROM tbl_cust_transaction_order_history
                            WHERE ctoh_order_id = $orderID
                            ORDER BY ctoh_status_date DESC
                            LIMIT 1");
    }

    /************************************* */
    /*     HELPERS CUSTOMERORDER           */
    /************************************* */

    public static function getClaimedCouponsByItem($itemId){

        $instance = new self();

        return $instance->db->getVal("SELECT count(tri_id)
                                        FROM tbl_cust_transaction_items
                                        WHERE tri_item_type=1
                                        AND tri_itm_row_id=$itemId");
    }

    public static function getOrderAndItemByCouponAndRedeemCode($custId,$bizId,$couponId,$redeemCode){

        $instance = new self();

        return $instance->db->getRow("SELECT * FROM tbl_cust_transaction_order,tbl_cust_transaction_items
                                        WHERE tri_order_id=cto_id
                                        AND cto_cust_id=$custId
                                        AND cto_biz_id=$bizId
                                        AND tri_itm_row_id = $couponId
                                        AND tri_redeem_code = '$redeemCode'");

    }
}
