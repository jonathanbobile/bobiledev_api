<?php

/**
 * subscriptionManager short summary.
 *
 * subscriptionManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerSubscriptionManager extends Manager
{
    /************************************* */
    /*   CUSTOMER SUBSCRIPTION PUBLIC      */
    /************************************* */

    public static function getCustomerPrimaryMultiuseEntry($cust_id){
        try{
            $instance = new self();
            $primaryRow = $instance->getCustomerPrimaryMultiuseEntry_DB($cust_id);

            if(!isset($primaryRow['type'])){
                return resultObject::withData(0,"no_primary");
            }

            if($primaryRow['type'] == 'subscription'){
                $custSubRow = $instance->getCustomerSubscriptionRowByCustSubID($primaryRow['entry_id']);

                $custSubObj = subscriptionPZObject::withData($custSubRow);
            }
            else{
                $custPpassRow = $instance->getCustomerPuncPassRowByCustPpassID($primaryRow['entry_id']);

                $custSubObj = punchpassPZObject::withData($custPpassRow);
            }

            return resultObject::withData(1,"",$custSubObj);

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cust_id);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getCustomerSubscriptionByCustSubID($csubID,$withInnerSubscription = true){
        try{
            $instance = new self();
            $custSubRow = $instance->getCustomerSubscriptionRowByCustSubID($csubID);

            $custSubObj = customerSubsciptionObject::withData($custSubRow,$withInnerSubscription);

            return resultObject::withData(1,'',$custSubObj);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$csubID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getCustomerSubscriptionByUsageID($usageID){
        try{
            $instance = new self();
            $custSubRow = $instance->getSubscriptionFromUsageID($usageID);

            $custSubObj = customerSubsciptionObject::withData($custSubRow);

            return resultObject::withData(1,'',$custSubObj);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$usageID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getCustomerPunchpassByCustPpassID($cppassID){
        try{
            $instance = new self();
            $custPpassRow = $instance->getCustomerPuncPassRowByCustPpassID($cppassID);

            $custPpassObj = customerPunchpassObject::withData($custPpassRow);

            return resultObject::withData(1,'',$custPpassObj);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$cppassID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getCustomerPunchpassFromUsageID($usageID){
        try{
            $instance = new self();
            $custPpassRow = $instance->getPunchPassFromUsageID($usageID);

            $custPpassObj = customerPunchpassObject::withData($custPpassRow);

            return resultObject::withData(1,'',$custPpassObj);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$usageID);
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function updateCustomerSubscriptionNextBillingDate(customerSubsciptionObject $custSubObj){
        try{
            $instance = new self();
            $instance->updateCustomerSubscriptionRowNextBillingDate($custSubObj);
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custSubObj));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function updateCustomerSubcriptionNextUsageRenew(customerSubsciptionObject $custSubObj){
        try{
            $instance = new self();
            $instance->updateCustomerSubscriptionRowNextUsageRenew($custSubObj);
            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($custSubObj));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function addSubscriptionToCustomerByCustomerID($custID,levelData10Object $subscription){
        try{
            $instance = new self();
            $csubID = $instance->addCustomerSubscriptionRowForCustomerByCustomerID($custID,$subscription);            

            

            if($csubID <= 0){
                return resultObject::withData(0,'Failed to add subscription to customer');
            }

            $csubsData = $instance->getCustomerSubscriptionRowByCustSubID($csubID);
            $custSubscription = customerSubsciptionObject::withData($csubsData);

            $instance->updateCustomerSubcriptionNextUsageRenew($custSubscription);

            $instance->updateCustomerSubscriptionNextBillingDate($custSubscription);

            return resultObject::withData(1,'',$custSubscription);
        }
        catch(Exception $e){
            $data = array();
            $data['customer_id'] = $custID;
            $data['subscription'] = $subscription;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function addPunchpassToCustomerByCustomerID($custID,levelData11Object $punchpass){
        try{
            $instance = new self();
            $cppassID = $instance->addCustomerPunchpassRowForCustomerByCustomerID($custID,$punchpass);   

            if($cppassID <= 0){
                return resultObject::withData(0,'Failed to add subscription to customer');
            }
           
            $cpassData = $instance->getCustomerPuncPassRowByCustPpassID($cppassID);
            
            $custPunchpass = customerPunchpassObject::withData($cpassData);
           
            $instance->updateCustomerPunchpassRowNextUsageRenew($custPunchpass);

            return resultObject::withData(1,'',$custPunchpass);
        }
        catch(Exception $e){
            $data = array();
            $data['customer_id'] = $custID;
            $data['punchpass'] = $punchpass;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function isItemInCustomerSubscription($cust_id,$item_type,$item_id){
        $instance = new self();

        $sql = "SELECT COUNT(*) as cnt FROM (SELECT sitem_ext_id as item_id FROM tbl_customer_subscription,tbl_subscription_items,tbl_subscription_usage_mech
                WHERE csu_sub_id = sitem_sub_id
                    AND sum_sub_id = csu_sub_id
                    AND sum_auto = 1
                    AND csu_cust_id = $cust_id
                    AND sitem_type = '$item_type'
                    AND csu_status = 'active'
                    AND csu_usage_start_time < NOW()
                    AND (csu_usage_rule = 'unlimited' OR csu_usage_uses_left > 0)
                    AND sitem_ext_id = $item_id
                UNION
                SELECT ppitem_ext_id as item_id FROM tbl_customers_punchpass,tbl_punchpass_items,tbl_punchpass_usage_mech
                WHERE cpp_pass_id = ppitem_pass_id
                    AND pum_sub_id = cpp_pass_id
                    AND cpp_cust_id = $cust_id
                    AND ppitem_type = '$item_type'
                    AND cpp_status = 'active'
                    AND cpp_usage_start_time < NOW()
                    AND cpp_uses_left > 0
                    AND pum_auto = 1
                    AND (cpp_usage_rule = 'unlimited' OR cpp_usage_rule_uses_left > 0)
                    AND ppitem_ext_id = $item_id) s";

        $cnt = $instance->db->getVal($sql);

        return $cnt > 0;
    }

    public static function getAllActiveCustomerSubscriptionsForItem($custID,$item_type,$item_id){
        try{
            $instance = new self();
            $result = array();

            $custSubs = $instance->getAllActiveCustomerSubscriptionsIDForItemDB($custID,$item_type,$item_id);
            $result['subscriptions'] = array();
            foreach ($custSubs as $custSub)
            {
            	$result['subscriptions'][] = subscriptionPZObject::withData($custSub);
            }
            
            $custPpasses = $instance->getAllActiveCustomerPunchPassesIDForItemDB($custID,$item_type,$item_id);
            $result['punchpasses'] = array();
            foreach ($custPpasses as $custPpass)
            {
            	$result['punchpasses'][] = punchpassPZObject::withData($custPpass);
            }

            return resultObject::withData(1,'',$result);            
        }
        catch(Exception $e){
            $data = array();
            $data['customer_id'] = $custID;
            $data['item_type'] = $item_type;
            $data['item_id'] = $item_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getAllActiveCustomerMultiuseItems($custID){
        try{
            $instance = new self();
            $result = array();

            $custSubs = $instance->getAllActiveCustomerSubscriptionsIDDB($custID);
            
            foreach ($custSubs as $custSub)
            {
            	$result[] = subscriptionPZObject::withData($custSub);
            }
            
            $custPpasses = $instance->getAllActiveCustomerPunchPassesIDFDB($custID);
            
            foreach ($custPpasses as $custPpass)
            {
            	$result[] = punchpassPZObject::withData($custPpass);
            }

            return resultObject::withData(1,'',$result);            
        }
        catch(Exception $e){
            $data = array();
            $data['customer_id'] = $custID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getFirstActiveCustomerSubscriptionsIDForItem($cust_id,$item_type,$item_id){
        try{
            $instance = new self();
            $result = array();

            $csub_id = $instance->getFirstActiveCustomerSubscriptionsIDForItemDB($cust_id,$item_type,$item_id);

            if($csub_id > 0){
                $result['code'] = 1;
                $result['type'] = 'subscription';
                $result['multi_id'] = $csub_id;
                return resultObject::withData(1,'',$result);
            }

            $cpass_id = $instance->getFirstActiveCustomerPunchPassesIDForItemDB($cust_id,$item_type,$item_id);

            if($cpass_id > 0){
                $result['code'] = 1;
                $result['type'] = 'punch_pass';
                $result['multi_id'] = $cpass_id;
                return resultObject::withData(1,'',$result);
            }
            $result['code'] = 0;
            return resultObject::withData(1,'',$result);
        }
        catch(Exception $e){
            $data = array();
            $data['custID'] = $cust_id;
            $data['ItemType'] = $item_type;
            $data['itemID'] = $item_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function getSubscriptionItems($subscriptionId){
        $instance = new self();
        $sql = "SELECT * FROM tbl_subscription_items WHERE sitem_sub_id = $subscriptionId";

        return $instance->db->getTable($sql);
    }

    public static function getPunchPassItems($punchPassId){
        $instance = new self();
        $sql = "SELECT * FROM tbl_punchpass_items WHERE ppitem_pass_id = $punchPassId";

        return $instance->db->getTable($sql);
    }

    public static function useCustSubscription(customerSubscriptionUsageObject $usageObject){
        try{
            $instance = new self();

            $cust_sub_row = $instance->getCustomerSubscriptionRowByCustSubID($usageObject->csuse_csu_id);

            if(!$instance->subscriptionMechAllowed($usageObject->csuse_csu_id,$usageObject->csuse_source)){
                return resultObject::withData(0,'mech_not_allowed');
            }

            if($cust_sub_row['csu_usage_rule'] == 'n_times_period' && $cust_sub_row['csu_usage_uses_left'] <= 0){
                return resultObject::withData(0,'no_more_uses');
            }

            $usageID = $instance->addCustomerSubscriptionUsageDB($usageObject);

            if($usageID > 0){
                if($cust_sub_row['csu_usage_rule'] == 'n_times_period'){
                    $instance->decreaseCustSubRemainingCapacity($usageObject->csuse_csu_id);
                }

                eventManager::actionTrigger(enumCustomerActions::usedSubscription,$cust_sub_row['csu_cust_id'],'used_subscription',$cust_sub_row['md_head'],'',$cust_sub_row['csu_id']);
                customerManager::sendAsyncCustomerDataUpdateToDevice('subscriptions',$usageObject->csuse_cust_id);
                return resultObject::withData(1,"",$usageID);
            }           

            return resultObject::withData(0,'usage_failed');
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($usageObject));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function checkCustSubscriptionUsageIsValid(customerSubscriptionUsageObject $usageObject){
        try{
            $instance = new self();

            $cust_sub_row = $instance->getCustomerSubscriptionRowByCustSubID($usageObject->csuse_csu_id);

            if(!$instance->subscriptionMechAllowed($usageObject->csuse_csu_id,$usageObject->csuse_source)){
                return resultObject::withData(0,'mech_not_allowed');
            }

            if($cust_sub_row['csu_usage_rule'] == 'n_times_period' && $cust_sub_row['csu_usage_uses_left'] <= 0){
                return resultObject::withData(0,'no_more_uses');
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($usageObject));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function useCustPunchPass(customerPunchUsageObject $usageObject){
        try{
            $instance = new self();
            $cust_pass_row = $instance->getCustomerPuncPassRowByCustPpassID($usageObject->cpuse_cpp_id);

            if(!$instance->punchpassMechAllowed($usageObject->cpuse_cpp_id,$usageObject->cpuse_source)){
                return resultObject::withData(0,'mech_not_allowed');
            }

            if($cust_pass_row['cpp_uses_left'] <= 0){
                return resultObject::withData(0,'no_more_uses');
            }

            if($cust_pass_row['cpp_usage_rule'] == 'n_times_period' && $cust_pass_row['cpp_usage_rule_uses_left'] <= 0){
                return resultObject::withData(0,'no_more_uses_period_'.$cust_pass_row['cpp_usage_rule_period']);
            }

            $usageID = $instance->addCustomerPunchUsageDB($usageObject);

            if($usageID > 0){
                $has_usage_period_limit = $cust_pass_row['cpp_usage_rule'] == 'n_times_period';
                $instance->decreaseCustPunchPassRemainingCapacity($usageObject->cpuse_cpp_id,$has_usage_period_limit);

                eventManager::actionTrigger(enumCustomerActions::usedPunchPass,$cust_pass_row['cpp_cust_id'],'used_punch_pass',$cust_pass_row['md_head'],'',$cust_pass_row['cpp_id']);
                customerManager::sendAsyncCustomerDataUpdateToDevice('punchpasses',$usageObject->cpuse_cust_id);
                if($cust_pass_row['cpp_uses_left'] - 1 <= 0){
                    $punchPassObject = customerPunchpassObject::withData($cust_pass_row);

                    $instance->endCustomerPunchPass_inner($punchPassObject);
                }
                return resultObject::withData(1,'',$usageID);
            }

            return resultObject::withData(0,'usage_failed');

        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($usageObject));
            return resultObject::withData(0,$e->getMessage());
        }

    }

    public static function checkCustPunchpassusageIsvalid(customerPunchUsageObject $usageObject){
        try{
            $instance = new self();
            $cust_pass_row = $instance->getCustomerPuncPassRowByCustPpassID($usageObject->cpuse_cpp_id);

            if(!$instance->punchpassMechAllowed($usageObject->cpuse_cpp_id,$usageObject->cpuse_source)){
                return resultObject::withData(0,'mech_not_allowed');
            }

            if($cust_pass_row['cpp_uses_left'] <= 0){
                return resultObject::withData(0,'no_more_uses');
            }

            if($cust_pass_row['cpp_usage_rule'] == 'n_times_period' && $cust_pass_row['cpp_usage_rule_uses_left'] <= 0){
                return resultObject::withData(0,'no_more_uses_period_'.$cust_pass_row['cpp_usage_rule_period']);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($usageObject));
            return resultObject::withData(0,$e->getMessage());
        }

    }

    public static function endCustomerPunchPass(customerPunchpassObject $punchPassObject){
        try{
            $instance = new self();

            $instance->endCustomerPunchPass_inner($punchPassObject);

            return resultObject::withData(1);
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($punchPassObject));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function setCustomerSubscriptionAsPrimary($cust_id,$entry_id){
        try{
            $instance = new self();
            $instance->setPrimaryCustomerSubscription($cust_id,$entry_id);
            $instance->setPrimaryCustomerPunchpass($cust_id,0);//reset any punch pass primary mark

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['custID'] = $cust_id;
            $data['entry'] = $entry_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function setCustomerPunchPassAsPrimary($cust_id,$entry_id){
        try{
            $instance = new self();

            $instance->setPrimaryCustomerPunchpass($cust_id,$entry_id);
            $instance->setPrimaryCustomerSubscription($cust_id,0);//reset any subscription primary mark

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['custID'] = $cust_id;
            $data['entry'] = $entry_id;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function checkCustSubscriptionCartValidity($csub_id,$quantity){
        try{
            $instance = new self();
            $cust_sub = $instance->loadCustomerSubsciptionFromDB($csub_id);
            $result = array();
            $result['id'] = $csub_id;
            $result['type'] = 'subscription';
            if(isset($cust_sub['csu_usage_end_time'])){
                $dateTime = $cust_sub['csu_usage_end_time'];
                $today = date("Y-m-d H:i:s");
                
                if($today > $dateTime){
                    $result['code'] = 0;
                    $result['message'] = 'expired';
                    return resultObject::withData(0,'expired',$result);
                }
            }

            if($cust_sub['csu_usage_rule'] == 'n_times_period' && $quantity > $cust_sub['csu_usage_uses_left']){
                $result['code'] = 0;
                $result['message'] = 'no_uses_left';
                return resultObject::withData(0,'no_uses_left',$result);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['csubID'] = $csub_id;
            $data['quantity'] = $quantity;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function checkCustPunchpassCartValidity($cpass_id,$quantity){
        try{
            $instance = new self();
            $cpass = $instance->loadCustomerPunchpassFromDB($cpass_id);
            $result = array();
            $result['id'] = $cpass_id;
            $result['type'] = 'punch_pass';
            if(isset($cust_sub['cpp_usage_end_time'])){
                $dateTime = DateTime::createFromFormat('Y-m-d H:i',$cpass['cpp_usage_end_time']);
                $today = date("Y-m-d H:i:s");
                
                if($today > $dateTime){
                    $result['code'] = 0;
                    $result['message'] = 'expired';
                    return resultObject::withData(0,'expired',$result);
                }
            }

            if($quantity > $cpass['cpp_uses_left']){
                $result['code'] = 0;
                $result['name'] = 'punchpass_uses';
                $result['message'] = 'no_uses_left';
                return resultObject::withData(0,'no_uses_left',$result);
            }

            if($cpass['cpp_usage_rule'] == 'n_times_period' && $quantity > $cpass['cpp_usage_rule_uses_left']){
                $result['code'] = 0;
                $result['name'] = $cpass['cpp_usage_rule'];
                $result['message'] = 'no_uses_left';
                return resultObject::withData(0,'no_uses_left',$result);
            }

            return resultObject::withData(1);
        }
        catch(Exception $e){
            $data = array();
            $data['cppassID'] = $cpass_id;
            $data['quantity'] = $quantity;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public static function isSubscriptionMechAllowed($csub_id,$mech){
        $instance = new self();

        return $instance->subscriptionMechAllowed($csub_id,$mech);
    }

    public static function isPunchpassMechAllowed($cpass_id,$mech){
        $instance = new self();

        return $instance->punchpassMechAllowed($cpass_id,$mech);
    }

    public static function getCustomerActiveSubscriptionsCount($custID){
        $instance = new self();

        $sql = "SELECT COUNT(*) FROM tbl_customer_subscription
                WHERE csu_cust_id = $custID
                AND csu_status = 'active'
                AND csu_usage_start_time < NOW()";

        return $instance->db->getVal($sql);
    }

    public static function getCustomerActivePunchpassCount($custID){
        $instance = new self();

        $sql = "SELECT COUNT(*) FROM tbl_customers_punchpass
                WHERE cpp_cust_id = $custID
                AND cpp_status = 'active'
                AND cpp_usage_start_time < NOW()";

        return $instance->db->getVal($sql);
    }

    /************************************* */
    /*   CUSTOMER SUBSCRIPTION PRIVATE     */
    /************************************* */

    private function getSubscriptionFromUsageID($usageID){
        $sql = "SELECT * FROM tbl_cust_sub_usage,tbl_customer_subscription,tbl_mod_data10
                WHERE csuse_csu_id = csu_id
                AND csu_sub_id = md_row_id
                AND csuse_id = $usageID";
        
        return $this->db->getRow($sql);
    }

    private function getPunchPassFromUsageID($usageID){
        $sql = "SELECT * FROM tbl_cust_punch_usage,tbl_customers_punchpass,tbl_mod_data11
                WHERE cpuse_cpp_id = cpp_id
                AND cpp_pass_id = md_row_id
                AND cpuse_id = $usageID";

        
        return $this->db->getRow($sql);
    }

    private function getCustomerSubscriptionRowByCustSubID($csubID){
        $sql = "SELECT * FROM tbl_customer_subscription,tbl_mod_data10
                WHERE csu_sub_id = md_row_id
                AND csu_id = $csubID";

        

        return $this->db->getRow($sql);
    }

    private function getCustomerPuncPassRowByCustPpassID($cppassID){
        $sql = "SELECT * FROM tbl_customers_punchpass,tbl_mod_data11
                WHERE cpp_pass_id = md_row_id
                AND cpp_id = $cppassID";

        return $this->db->getRow($sql);
    }

    private function updateCustomerSubscriptionRowNextBillingDate(customerSubsciptionObject $custSubObj){
        if(!$custSubObj->_isValid()){
            throw new Exception('Invalid customer subscription');
        }

        if($custSubObj->csu_billing_type != 'auto_renew'){
            return;
        }

        $sql = "UPDATE tbl_customer_subscription SET
                    csu_billing_next_date = IF(csu_usage_end_time IS NULL OR csu_usage_end_time > DATE_ADD(NOW(), INTERVAL 1 {$custSubObj->csu_billing_period_unit}),DATE_ADD(NOW(), INTERVAL 1 {$custSubObj->csu_billing_period_unit}), NULL)
                WHERE csu_id = {$custSubObj->csu_id}";

        $this->db->execute($sql);
    }

    private function updateCustomerSubscriptionRowNextUsageRenew(customerSubsciptionObject $custSubObj){
        if(!$custSubObj->_isValid()){
            throw new Exception('Invalid customer subscription');
        }

        if($custSubObj->csu_usage_rule != 'n_times_period'){
            return;
        }

        $sql = "UPDATE tbl_customer_subscription SET
                    csu_usage_next_renew = IF(csu_usage_end_time IS NULL OR csu_usage_end_time > DATE_ADD(NOW(), INTERVAL 1 {$custSubObj->csu_usage_period_unit}),DATE_ADD(NOW(), INTERVAL 1 {$custSubObj->csu_usage_period_unit}), NULL)
                WHERE csu_id = {$custSubObj->csu_id}";

        $this->db->execute($sql);
    }

    private function addCustomerSubscriptionRowForCustomerByCustomerID($custID,levelData10Object $subscription){
        if(!is_numeric($custID) || $custID <= 0){
            throw new Exception('Illegal customer ID');
        }

        if(!$subscription->_isValid()){
            throw new Exception('Invalid subscription');
        }

        
        $startTime = 'NOW()';

        if($subscription->md_usage_period == 'period' && isset($subscription->md_usage_period_start) && $subscription->md_usage_period_start != ''){
            $startDateTime = strtotime($subscription->md_usage_period_start);
            if($startDateTime > time()){//starts in future date
                $startTime = "'{$subscription->md_usage_period_start}'";
            }
        }

        $endTime = 'NULL';

        if($subscription->md_usage_period == 'period' && isset($subscription->md_usage_period_end) && $subscription->md_usage_period_end != ''){
            $endTime = "'{$subscription->md_usage_period_end}'";
        }

        if($subscription->md_usage_period == 'limited'){
            $endTime = "DATE_ADD($startTime, INTERVAL {$subscription->md_usage_period_number} {$subscription->md_usage_period_unit})";
        }        

        $sql = "INSERT INTO tbl_customer_subscription SET
                    csu_cust_id = $custID,
                    csu_sub_id = {$subscription->md_row_id},
                    csu_biz_id = {$subscription->md_biz_id},
                    csu_price = {$subscription->md_price},
                    csu_usage_start_time = $startTime,
                    csu_usage_end_time = $endTime,
                    csu_billing_type = '{$subscription->md_billing_type}',
                    csu_billing_period_unit = '{$subscription->md_billing_period_unit}',
                    csu_usage_rule = '{$subscription->md_usage_rule_type}',
                    csu_usage_period_unit = '{$subscription->md_usage_rule_period_unit}',
                    csu_usage_capacity = {$subscription->md_usage_rule_capacity},
                    csu_usage_uses_left = {$subscription->md_usage_rule_capacity}";        

        return $this->db->execute($sql);
    } 

    private function addCustomerPunchpassRowForCustomerByCustomerID($custID,levelData11Object $punchpassObj){
        if(!is_numeric($custID) || $custID <= 0){
            throw new Exception('Illegal customer ID');
        }

        if(!$punchpassObj->_isValid()){
            throw new Exception('Invalid punchpass');
        }

        $startTime = 'NOW()';

        if($punchpassObj->md_usage_period == 'period' && isset($punchpassObj->md_usage_period_start) && $punchpassObj->md_usage_period_start != ''){
            $startDateTime = strtotime($punchpassObj->md_usage_period_start);
            if($startDateTime > time()){//starts in future date
                $startTime = "'{$punchpassObj->md_usage_period_start}'";
            }
        }

        $endTime = 'NULL';

        if($punchpassObj->md_usage_period == 'period' && isset($punchpassObj->md_usage_period_end) && $punchpassObj->md_usage_period_end != ''){
            $endTime = "'{$punchpassObj->md_usage_period_end}'";
        }

        if($punchpassObj->md_usage_period == 'limited'){
            $endTime = "DATE_ADD($startTime, INTERVAL {$punchpassObj->md_usage_period_number} {$punchpassObj->md_usage_period_unit})";
        }     

        $sql = "INSERT INTO tbl_customers_punchpass SET
                    cpp_cust_id = $custID,
                    cpp_pass_id = {$punchpassObj->md_row_id},
                    cpp_biz_id = {$punchpassObj->md_biz_id},
                    cpp_price = {$punchpassObj->md_price},
                    cpp_usage_start_time = $startTime,
                    cpp_usage_end_time = $endTime,   
                    cpp_uses_left = {$punchpassObj->md_usage_total_capacity},
                    cpp_usage_capacity = {$punchpassObj->md_usage_total_capacity},
                    cpp_usage_rule = '{$punchpassObj->md_usage_rule_type}',
                    cpp_usage_rule_period = '{$punchpassObj->md_usage_rule_period_unit}',
                    cpp_usage_rule_capacity = {$punchpassObj->md_usage_rule_capacity},
                    cpp_usage_rule_uses_left = {$punchpassObj->md_usage_rule_capacity}";

        return $this->db->execute($sql);
    }

    private function updateCustomerPunchpassRowNextUsageRenew(customerPunchpassObject $punchpassObj){
        if(!$punchpassObj->_isValid()){
            throw new Exception('Invalid customer punchpass');
        }

        if($punchpassObj->cpp_usage_rule != 'n_times_period'){
            return;
        }

        $sql = "UPDATE tbl_customers_punchpass SET
                    cpp_usage_rule_next_renew = IF(cpp_usage_end_time IS NULL OR cpp_usage_end_time > DATE_ADD(NOW(), INTERVAL 1 {$punchpassObj->cpp_usage_rule_period}),DATE_ADD(NOW(), INTERVAL 1 {$punchpassObj->cpp_usage_rule_period}), NULL)
                WHERE cpp_id = {$punchpassObj->cpp_id}";

        $this->db->execute($sql);
    }
    
    private function getCustomerPrimaryMultiuseEntry_DB($cust_id){
        $sql = "SELECT * FROM (SELECT 'subscription' as type,csu_id as entry_id FROM tbl_customer_subscription,tbl_mod_data10
                    WHERE  csu_sub_id = md_row_id
                    AND csu_cust_id = $cust_id
                    AND csu_is_primary = 1
                    AND csu_status = 'active'
                    AND md_row_id IS NOT NULL
                UNION
                SELECT 'punch_pass' as type,cpp_id as entry_id FROM tbl_customers_punchpass,tbl_mod_data11
                    WHERE cpp_pass_id = md_row_id
                    AND cpp_cust_id = $cust_id
                    AND cpp_is_primary = 1
                    AND cpp_status = 'active'
                    AND md_row_id IS NOT NULL) s
                LIMIT 1";

        return $this->db->getRow($sql);
    }

    private function subscriptionMechAllowed($csub_id,$mech){
        if($mech == 'auto'){
            return true;
        }

        $sql = "SELECT tbl_subscription_usage_mech.* FROM tbl_customer_subscription,tbl_subscription_usage_mech
                WHERE sum_sub_id = csu_sub_id
                AND csu_id = $csub_id";

        $mechanisms = $this->db->getRow($sql);

        if($mech == 'scan'){
            return $mechanisms['sum_scan'] == 1;
        }

        if($mech == 'manual'){
            return $mechanisms['sum_manual'] == 1;
        }

        if($mech == 'geolocation'){
            return $mechanisms['sum_geo'] == 1;
        }

        return false;
    }

    private function punchpassMechAllowed($pass_id,$mech){
        if($mech == 'auto'){
            return true;
        }

        $sql = "SELECT tbl_punchpass_usage_mech.* FROM tbl_customers_punchpass,tbl_punchpass_usage_mech
                WHERE pum_sub_id = cpp_pass_id
                AND cpp_id = $pass_id";

        $mechanisms = $this->db->getRow($sql);

        if($mech == 'scan'){
            return $mechanisms['pum_scan'] == 1;
        }

        if($mech == 'manual'){
            return $mechanisms['pum_manual'] == 1;
        }

        return false;
    }

    /**
     * lower value of cust_subscription capacity_left by 1
     * @param mixed $csub 
     */
    private function decreaseCustSubRemainingCapacity($csub_id){
        $sql = "UPDATE tbl_customer_subscription 
                        SET csu_usage_uses_left = csu_usage_uses_left - 1
                        WHERE csu_id = $csub_id";

        $this->db->execute($sql);
    }

    private function decreaseCustPunchPassRemainingCapacity($cpass_id,$has_period_limit = false){
        $reducePeriodUses = "";
        if($has_period_limit){
            $reducePeriodUses = ",cpp_usage_rule_uses_left = cpp_usage_rule_uses_left - 1";
        }

        $sql = "UPDATE tbl_customers_punchpass SET
                        cpp_uses_left = cpp_uses_left - 1
                        $reducePeriodUses
                        
                    WHERE cpp_id = $cpass_id";

        $this->db->execute($sql);
    }

    private function endCustomerPunchPass_inner(customerPunchpassObject $punchPassObject){
        $punchPassObject->cpp_status = 'ended';
        $this->upateCustomerPunchpassDB($punchPassObject);

        $usageObject = new customerPunchUsageObject();

        $usageObject->cpuse_cpp_id = $punchPassObject->cpp_id;
        $usageObject->cpuse_cust_id = $punchPassObject->cpp_cust_id;
        $usageObject->cpuse_biz_id = $punchPassObject->cpp_biz_id;
        $usageObject->cpuse_type = 'ended';

        $this->addCustomerPunchUsageDB($usageObject);

        return;
    }

    private function getFirstActiveCustomerSubscriptionsIDForItemDB($cust_id,$item_type,$item_id){
        $sql = "SELECT csu_id FROM tbl_customer_subscription,tbl_subscription_items 
                    WHERE csu_sub_id = sitem_sub_id
                    AND csu_cust_id = $cust_id
                    AND sitem_type = '$item_type'
                    AND csu_status = 'active'
                    AND csu_usage_start_time < NOW()
                    AND (csu_usage_rule = 'unlimited' OR csu_usage_uses_left > 0)
                    AND sitem_ext_id = $item_id
                    ORDER BY -csu_usage_end_time DESC
                    LIMIT 1";

        return $this->db->getVal($sql);
    }

    private function getFirstActiveCustomerPunchPassesIDForItemDB($cust_id,$item_type,$item_id){
        $sql = "SELECT cpp_id FROM tbl_customers_punchpass,tbl_punchpass_items 
                    WHERE cpp_pass_id = ppitem_pass_id
                    AND cpp_cust_id = $cust_id
                    AND ppitem_type = '$item_type'
                    AND cpp_usage_start_time < NOW()
                    AND cpp_uses_left > 0
                    AND (cpp_usage_rule = 'unlimited' OR cpp_usage_rule_uses_left > 0)
                    AND cpp_status = 'active'
                    AND ppitem_ext_id = $item_id
                    ORDER BY -cpp_usage_end_time DESC
                    LIMIT 1";

        return $this->db->getVal($sql);
    }

    private function getAllActiveCustomerSubscriptionsIDDB($cust_id){
        $sql = "SELECT * FROM tbl_mod_data10,tbl_customer_subscription
                    WHERE csu_sub_id = md_row_id 
                    AND csu_cust_id = $cust_id
                    AND csu_status = 'active'
                    AND csu_usage_start_time < NOW()
                    AND (csu_usage_rule = 'unlimited' OR csu_usage_uses_left > 0)
                    ORDER BY -csu_usage_end_time DESC";
        
        return $this->db->getTable($sql);
    }

    private function getAllActiveCustomerPunchPassesIDFDB($cust_id){
        $sql = "SELECT * FROM tbl_mod_data11,tbl_customers_punchpass
                    WHERE  cpp_pass_id = md_row_id
                    AND cpp_cust_id = $cust_id
                    AND cpp_usage_start_time < NOW()
                    AND cpp_uses_left > 0
                    AND (cpp_usage_rule = 'unlimited' OR cpp_usage_rule_uses_left > 0)
                    AND cpp_status = 'active'
                    ORDER BY -cpp_usage_end_time DESC";
        
        return $this->db->getTable($sql);
    }

    private function getAllActiveCustomerSubscriptionsIDForItemDB($cust_id,$item_type,$item_id){
        $sql = "SELECT * FROM tbl_mod_data10,tbl_customer_subscription,tbl_subscription_items 
                    WHERE csu_sub_id = md_row_id 
                    AND csu_sub_id = sitem_sub_id
                    AND csu_cust_id = $cust_id
                    AND sitem_type = '$item_type'
                    AND csu_status = 'active'
                    AND csu_usage_start_time < NOW()
                    AND (csu_usage_rule = 'unlimited' OR csu_usage_uses_left > 0)
                    AND sitem_ext_id = $item_id
                    ORDER BY -csu_usage_end_time DESC";
        
        return $this->db->getTable($sql);
    }

    private function getAllActiveCustomerPunchPassesIDForItemDB($cust_id,$item_type,$item_id){
        $sql = "SELECT * FROM tbl_mod_data11,tbl_customers_punchpass,tbl_punchpass_items 
                    WHERE  cpp_pass_id = md_row_id
                    AND cpp_pass_id = ppitem_pass_id
                    AND cpp_cust_id = $cust_id
                    AND ppitem_type = '$item_type'
                    AND cpp_usage_start_time < NOW()
                    AND cpp_uses_left > 0
                    AND (cpp_usage_rule = 'unlimited' OR cpp_usage_rule_uses_left > 0)
                    AND cpp_status = 'active'
                    AND ppitem_ext_id = $item_id
                    ORDER BY -cpp_usage_end_time DESC";

        return $this->db->getTable($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERSUBSCIPTION - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerSubsciptionObject to DB
     * Return Data = new customerSubsciptionObject ID
     * @param customerSubsciptionObject $customerSubsciptionObj 
     * @return resultObject
     */
    public static function addCustomerSubsciption(customerSubsciptionObject $customerSubsciptionObj){       
        try{
            $instance = new self();
            $newId = $instance->addCustomerSubsciptionDB($customerSubsciptionObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerSubsciptionObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerSubsciption from DB for provided ID
     * * Return Data = customerSubsciptionObject
     * @param int $customerSubsciptionId 
     * @return resultObject
     */
    public static function getCustomerSubsciptionByID($customerSubsciptionId, $withInnerSubscription = true){
        
        try {
            $instance = new self();
            $customerSubsciptionData = $instance->loadCustomerSubsciptionFromDB($customerSubsciptionId);
            
            $customerSubsciptionObj = customerSubsciptionObject::withData($customerSubsciptionData,$withInnerSubscription);
            $result = resultObject::withData(1,'',$customerSubsciptionObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerSubsciptionId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerSubsciption in DB
     * @param customerSubsciptionObject $customerSubsciptionObj 
     * @return resultObject
     */
    public static function updateCustomerSubsciption(customerSubsciptionObject $customerSubsciptionObj){        
        try{
            $instance = new self();
            $instance->upateCustomerSubsciptionDB($customerSubsciptionObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerSubsciptionObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerSubsciption from DB
     * @param int $customerSubsciptionID 
     * @return resultObject
     */
    public static function deleteCustomerSubsciptionById($customerSubsciptionID){
        try{
            $instance = new self();
            $instance->deleteCustomerSubsciptionByIdDB($customerSubsciptionID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerSubsciptionID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERSUBSCIPTION - DB METHODS           */
    /************************************* */

    private function addCustomerSubsciptionDB(customerSubsciptionObject $obj){

        if (!isset($obj)){
            throw new Exception("customerSubsciptionObject value must be provided");             
        }

        $csu_usage_start_timeDate = isset($obj->csu_usage_start_time) ? "'".$obj->csu_usage_start_time."'" : "null";

        $csu_usage_end_timeDate = isset($obj->csu_usage_end_time) ? "'".$obj->csu_usage_end_time."'" : "null";

        $csu_billing_next_dateDate = isset($obj->csu_billing_next_date) ? "'".$obj->csu_billing_next_date."'" : "null";

        $csu_usage_next_renewDate = isset($obj->csu_usage_next_renew) ? "'".$obj->csu_usage_next_renew."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_customer_subscription SET 
                                        csu_cust_id = {$obj->csu_cust_id},
                                        csu_sub_id = {$obj->csu_sub_id},
                                        csu_biz_id = {$obj->csu_biz_id},
                                        csu_price = {$obj->csu_price},
                                        csu_status = '{$obj->csu_status}',
                                        csu_is_primary = {$obj->csu_is_primary},
                                        csu_usage_start_time = $csu_usage_start_timeDate,
                                        csu_usage_end_time = $csu_usage_end_timeDate,
                                        csu_billing_type = '{$obj->csu_billing_type}',
                                        csu_billing_period_unit = '{$obj->csu_billing_period_unit}',
                                        csu_biling_payments_total = {$obj->csu_biling_payments_total},
                                        csu_billing_payments_left = {$obj->csu_billing_payments_left},
                                        csu_billing_next_date = $csu_billing_next_dateDate,
                                        csu_usage_rule = '{$obj->csu_usage_rule}',
                                        csu_usage_period_unit = '{$obj->csu_usage_period_unit}',
                                        csu_usage_capacity = {$obj->csu_usage_capacity},
                                        csu_usage_uses_left = {$obj->csu_usage_uses_left},
                                        csu_usage_next_renew = $csu_usage_next_renewDate
                                                 ");
        return $newId;
    }

    private function loadCustomerSubsciptionFromDB($customerSubsciptionID){

        if (!is_numeric($customerSubsciptionID) || $customerSubsciptionID <= 0){
            throw new Exception("Illegal value customerSubsciptionID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customer_subscription,tbl_mod_data10
                                    WHERE csu_sub_id = md_row_id
                                    AND csu_id = $customerSubsciptionID");
    }

    private function upateCustomerSubsciptionDB(customerSubsciptionObject $obj){

        if (!isset($obj->csu_id) || !is_numeric($obj->csu_id) || $obj->csu_id <= 0){
            throw new Exception("customerSubsciptionObject value must be provided");             
        }

        $csu_purchase_timeDate = isset($obj->csu_purchase_time) ? "'".$obj->csu_purchase_time."'" : "null";

        $csu_usage_start_timeDate = isset($obj->csu_usage_start_time) ? "'".$obj->csu_usage_start_time."'" : "null";

        $csu_usage_end_timeDate = isset($obj->csu_usage_end_time) ? "'".$obj->csu_usage_end_time."'" : "null";

        $csu_billing_next_dateDate = isset($obj->csu_billing_next_date) ? "'".$obj->csu_billing_next_date."'" : "null";

        $csu_usage_next_renewDate = isset($obj->csu_usage_next_renew) ? "'".$obj->csu_usage_next_renew."'" : "null";        

        $this->db->execute("UPDATE tbl_customer_subscription SET 
                                csu_cust_id = {$obj->csu_cust_id},
                                csu_sub_id = {$obj->csu_sub_id},
                                csu_biz_id = {$obj->csu_biz_id},
                                csu_price = {$obj->csu_price},
                                csu_status = '{$obj->csu_status}',
                                csu_is_primary = {$obj->csu_is_primary},
                                csu_purchase_time = $csu_purchase_timeDate,
                                csu_usage_start_time = $csu_usage_start_timeDate,
                                csu_usage_end_time = $csu_usage_end_timeDate,
                                csu_billing_type = '{$obj->csu_billing_type}',
                                csu_billing_period_unit = '{$obj->csu_billing_period_unit}',
                                csu_biling_payments_total = {$obj->csu_biling_payments_total},
                                csu_billing_payments_left = {$obj->csu_billing_payments_left},
                                csu_billing_next_date = $csu_billing_next_dateDate,
                                csu_usage_rule = '{$obj->csu_usage_rule}',
                                csu_usage_period_unit = '{$obj->csu_usage_period_unit}',
                                csu_usage_capacity = {$obj->csu_usage_capacity},
                                csu_usage_uses_left = {$obj->csu_usage_uses_left},
                                csu_usage_next_renew = $csu_usage_next_renewDate
                                WHERE csu_id = {$obj->csu_id} 
         ");
    }

    private function deleteCustomerSubsciptionByIdDB($customerSubsciptionID){

        if (!is_numeric($customerSubsciptionID) || $customerSubsciptionID <= 0){
            throw new Exception("Illegal value customerSubsciptionID");             
        }

        $this->db->execute("DELETE FROM tbl_customer_subscription WHERE csu_id = $customerSubsciptionID");
    }

    private function setPrimaryCustomerSubscription($cust_id,$csub_id){
        $sql = "UPDATE tbl_customer_subscription SET
                    csu_is_primary = CASE WHEN csu_id = $csub_id THEN 1 ELSE 0 END
                WHERE csu_cust_id = $cust_id";

        $this->db->execute($sql);
    }
    
    /************************************* */
    /*   BASIC CUSTOMERPUNCHPASS - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerPunchpassObject to DB
     * Return Data = new customerPunchpassObject ID
     * @param customerPunchpassObject $customerPunchpassObj 
     * @return resultObject
     */
    public static function addCustomerPunchpass(customerPunchpassObject $customerPunchpassObj){       
        try{
            $instance = new self();
            $newId = $instance->addCustomerPunchpassDB($customerPunchpassObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPunchpassObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerPunchpass from DB for provided ID
     * * Return Data = customerPunchpassObject
     * @param int $customerPunchpassId 
     * @return resultObject
     */
    public static function getCustomerPunchpassByID($customerPunchpassId,$withInnerPunchPass = true){
        
        try {
            $instance = new self();
            $customerPunchpassData = $instance->loadCustomerPunchpassFromDB($customerPunchpassId);
            
            $customerPunchpassObj = customerPunchpassObject::withData($customerPunchpassData,$withInnerPunchPass);
            $result = resultObject::withData(1,'',$customerPunchpassObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPunchpassId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerPunchpass in DB
     * @param customerPunchpassObject $customerPunchpassObj 
     * @return resultObject
     */
    public static function updateCustomerPunchpass(customerPunchpassObject $customerPunchpassObj){        
        try{
            $instance = new self();
            $instance->upateCustomerPunchpassDB($customerPunchpassObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPunchpassObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerPunchpass from DB
     * @param int $customerPunchpassID 
     * @return resultObject
     */
    public static function deleteCustomerPunchpassById($customerPunchpassID){
        try{
            $instance = new self();
            $instance->deleteCustomerPunchpassByIdDB($customerPunchpassID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPunchpassID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERPUNCHPASS - DB METHODS           */
    /************************************* */

    private function addCustomerPunchpassDB(customerPunchpassObject $obj){

        if (!isset($obj)){
            throw new Exception("customerPunchpassObject value must be provided");             
        }

        $cpp_usage_start_timeDate = isset($obj->cpp_usage_start_time) ? "'".$obj->cpp_usage_start_time."'" : "null";

        $cpp_usage_end_timeDate = isset($obj->cpp_usage_end_time) ? "'".$obj->cpp_usage_end_time."'" : "null";

        $cpp_usage_rule_next_renewDate = isset($obj->cpp_usage_rule_next_renew) ? "'".$obj->cpp_usage_rule_next_renew."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_customers_punchpass SET 
                                        cpp_cust_id = {$obj->cpp_cust_id},
                                        cpp_pass_id = {$obj->cpp_pass_id},
                                        cpp_biz_id = {$obj->cpp_biz_id},
                                        cpp_price = {$obj->cpp_price},
                                        cpp_status = '{$obj->cpp_status}',
                                        cpp_is_primary = {$obj->cpp_is_primary},
                                        cpp_usage_start_time = $cpp_usage_start_timeDate,
                                        cpp_usage_end_time = $cpp_usage_end_timeDate,
                                        cpp_uses_left = {$obj->cpp_uses_left},
                                        cpp_usage_capacity = {$obj->cpp_usage_capacity},
                                        cpp_usage_rule = '{$obj->cpp_usage_rule}',
                                        cpp_usage_rule_period = '{$obj->cpp_usage_rule_period}',
                                        cpp_usage_rule_capacity = {$obj->cpp_usage_rule_capacity},
                                        cpp_usage_rule_uses_left = {$obj->cpp_usage_rule_uses_left},
                                        cpp_usage_rule_next_renew = $cpp_usage_rule_next_renewDate
                                                 ");
        return $newId;
    }

    private function loadCustomerPunchpassFromDB($customerPunchpassID){

        if (!is_numeric($customerPunchpassID) || $customerPunchpassID <= 0){
            throw new Exception("Illegal value customerPunchpassID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_customers_punchpass,tbl_mod_data11 
                                    WHERE cpp_pass_id = md_row_id
                                    AND cpp_id = $customerPunchpassID");
    }

    private function upateCustomerPunchpassDB(customerPunchpassObject $obj){

        if (!isset($obj->cpp_id) || !is_numeric($obj->cpp_id) || $obj->cpp_id <= 0){
            throw new Exception("customerPunchpassObject value must be provided");             
        }

        $cpp_purchase_timeDate = isset($obj->cpp_purchase_time) ? "'".$obj->cpp_purchase_time."'" : "null";

        $cpp_usage_start_timeDate = isset($obj->cpp_usage_start_time) ? "'".$obj->cpp_usage_start_time."'" : "null";

        $cpp_usage_end_timeDate = isset($obj->cpp_usage_end_time) ? "'".$obj->cpp_usage_end_time."'" : "null";

        $cpp_usage_rule_next_renewDate = isset($obj->cpp_usage_rule_next_renew) ? "'".$obj->cpp_usage_rule_next_renew."'" : "null";

        $this->db->execute("UPDATE tbl_customers_punchpass SET 
                                cpp_cust_id = {$obj->cpp_cust_id},
                                cpp_pass_id = {$obj->cpp_pass_id},
                                cpp_biz_id = {$obj->cpp_biz_id},
                                cpp_price = {$obj->cpp_price},
                                cpp_status = '{$obj->cpp_status}',
                                cpp_is_primary = {$obj->cpp_is_primary},
                                cpp_purchase_time = $cpp_purchase_timeDate,
                                cpp_usage_start_time = $cpp_usage_start_timeDate,
                                cpp_usage_end_time = $cpp_usage_end_timeDate,
                                cpp_uses_left = {$obj->cpp_uses_left},
                                cpp_usage_capacity = {$obj->cpp_usage_capacity},
                                cpp_usage_rule = '{$obj->cpp_usage_rule}',
                                cpp_usage_rule_period = '{$obj->cpp_usage_rule_period}',
                                cpp_usage_rule_capacity = {$obj->cpp_usage_rule_capacity},
                                cpp_usage_rule_uses_left = {$obj->cpp_usage_rule_uses_left},
                                cpp_usage_rule_next_renew = $cpp_usage_rule_next_renewDate
                                WHERE cpp_id = {$obj->cpp_id} 
                                         ");
    }

    private function deleteCustomerPunchpassByIdDB($customerPunchpassID){

        if (!is_numeric($customerPunchpassID) || $customerPunchpassID <= 0){
            throw new Exception("Illegal value customerPunchpassID");             
        }

        $this->db->execute("DELETE FROM tbl_customers_punchpass WHERE cpp_id = $customerPunchpassID");
    }   
    
    private function setPrimaryCustomerPunchpass($cust_id,$cpass_id){
        $sql = "UPDATE tbl_customers_punchpass SET
                    cpp_is_primary = CASE WHEN cpp_id = $cpass_id THEN 1 ELSE 0 END
                WHERE cpp_cust_id = $cust_id";

        $this->db->execute($sql);
    }

    /************************************* */
    /*   BASIC CUSTOMERSUBSCRIPTIONUSAGE - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerSubscriptionUsageObject to DB
     * Return Data = new customerSubscriptionUsageObject ID
     * @param customerSubscriptionUsageObject $customerSubscriptionUsageObj 
     * @return resultObject
     */
    public static function addCustomerSubscriptionUsage(customerSubscriptionUsageObject $customerSubscriptionUsageObj){       
        try{
            $instance = new self();
            $newId = $instance->addCustomerSubscriptionUsageDB($customerSubscriptionUsageObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerSubscriptionUsageObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerSubscriptionUsage from DB for provided ID
     * * Return Data = customerSubscriptionUsageObject
     * @param int $customerSubscriptionUsageId 
     * @return resultObject
     */
    public static function getCustomerSubscriptionUsageByID($customerSubscriptionUsageId){
        
        try {
            $instance = new self();
            $customerSubscriptionUsageData = $instance->loadCustomerSubscriptionUsageFromDB($customerSubscriptionUsageId);
            
            $customerSubscriptionUsageObj = customerSubscriptionUsageObject::withData($customerSubscriptionUsageData);
            $result = resultObject::withData(1,'',$customerSubscriptionUsageObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerSubscriptionUsageId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update customerSubscriptionUsage in DB
     * @param customerSubscriptionUsageObject $customerSubscriptionUsageObj 
     * @return resultObject
     */
    public static function updateCustomerSubscriptionUsage(customerSubscriptionUsageObject $customerSubscriptionUsageObj){        
        try{
            $instance = new self();
            $instance->upateCustomerSubscriptionUsageDB($customerSubscriptionUsageObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerSubscriptionUsageObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerSubscriptionUsage from DB
     * @param int $customerSubscriptionUsageID 
     * @return resultObject
     */
    public static function deleteCustomerSubscriptionUsageById($customerSubscriptionUsageID){
        try{
            $instance = new self();
            $instance->deleteCustomerSubscriptionUsageByIdDB($customerSubscriptionUsageID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerSubscriptionUsageID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERSUBSCRIPTIONUSAGE - DB METHODS           */
    /************************************* */

    private function addCustomerSubscriptionUsageDB(customerSubscriptionUsageObject $obj){

        if (!isset($obj)){
            throw new Exception("customerSubscriptionUsageObject value must be provided");             
        }

        $csuse_status_change_timeDate = isset($obj->csuse_status_change_time) ? "'".$obj->csuse_status_change_time."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_cust_sub_usage SET 
                                        csuse_cust_id = {$obj->csuse_cust_id},
                                        csuse_biz_id = {$obj->csuse_biz_id},
                                        csuse_csu_id = {$obj->csuse_csu_id},
                                        csuse_type = '{$obj->csuse_type}',
                                        csuse_item_type = '{$obj->csuse_item_type}',
                                        csuse_item_id = {$obj->csuse_item_id},
                                        csuse_source = '{$obj->csuse_source}',
                                        csuse_source_id = {$obj->csuse_source_id},
                                        csuse_note = '".addslashes($obj->csuse_note)."',
                                        csuse_status = '{$obj->csuse_status}',
                                        csuse_status_change_time = $csuse_status_change_timeDate,
                                        csuse_status_changer_id = {$obj->csuse_status_changer_id},
                                        csuse_status_note = '".addslashes($obj->csuse_status_note)."'
                                                 ");
        return $newId;
    }

    private function loadCustomerSubscriptionUsageFromDB($customerSubscriptionUsageID){

        if (!is_numeric($customerSubscriptionUsageID) || $customerSubscriptionUsageID <= 0){
            throw new Exception("Illegal value customerSubscriptionUsageID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_sub_usage WHERE csuse_id = $customerSubscriptionUsageID");
    }

    private function upateCustomerSubscriptionUsageDB(customerSubscriptionUsageObject $obj){

        if (!isset($obj->csuse_id) || !is_numeric($obj->csuse_id) || $obj->csuse_id <= 0){
            throw new Exception("customerSubscriptionUsageObject value must be provided");             
        }

        $csuse_timeDate = isset($obj->csuse_time) ? "'".$obj->csuse_time."'" : "null";

        $csuse_status_change_timeDate = isset($obj->csuse_status_change_time) ? "'".$obj->csuse_status_change_time."'" : "null";

        $this->db->execute("UPDATE tbl_cust_sub_usage SET 
                                csuse_cust_id = {$obj->csuse_cust_id},
                                csuse_biz_id = {$obj->csuse_biz_id},
                                csuse_csu_id = {$obj->csuse_csu_id},
                                csuse_type = '{$obj->csuse_type}',
                                csuse_time = $csuse_timeDate,
                                csuse_item_type = '{$obj->csuse_item_type}',
                                csuse_item_id = {$obj->csuse_item_id},
                                csuse_source = '{$obj->csuse_source}',
                                csuse_source_id = {$obj->csuse_source_id},
                                csuse_note = '".addslashes($obj->csuse_note)."',
                                csuse_status = '{$obj->csuse_status}',
                                csuse_status_change_time = $csuse_status_change_timeDate,
                                csuse_status_changer_id = {$obj->csuse_status_changer_id},
                                csuse_status_note = '".addslashes($obj->csuse_status_note)."'
                                WHERE csuse_id = {$obj->csuse_id} 
                                         ");
    }

    private function deleteCustomerSubscriptionUsageByIdDB($customerSubscriptionUsageID){

        if (!is_numeric($customerSubscriptionUsageID) || $customerSubscriptionUsageID <= 0){
            throw new Exception("Illegal value customerSubscriptionUsageID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_sub_usage WHERE csuse_id = $customerSubscriptionUsageID");
    }

    /************************************* */
    /*   BASIC CUSTOMERPUNCHUSAGE - PUBLIC           */
    /************************************* */

    /**
     * Insert new customerPunchUsgaeObject to DB
     * Return Data = new customerPunchUsgaeObject ID
     * @param customerPunchUsageObject $customerPunchUsgaeObj 
     * @return resultObject
     */
    public static function addCustomerPunchUsage(customerPunchUsageObject $customerPunchUsgaeObj){       
        try{
            $instance = new self();
            $newId = $instance->addCustomerPunchUsageDB($customerPunchUsgaeObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPunchUsgaeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get customerPunchUsgae from DB for provided ID
     * * Return Data = customerPunchUsgaeObject
     * @param int $customerPunchUsgaeId 
     * @return resultObject
     */
    public static function getCustomerPunchUsageByID($customerPunchUsgaeId){
        
        try {
            $instance = new self();
            $customerPunchUsgaeData = $instance->loadCustomerPunchUsageFromDB($customerPunchUsgaeId);
            
            $customerPunchUsgaeObj = customerPunchUsageObject::withData($customerPunchUsgaeData);
            $result = resultObject::withData(1,'',$customerPunchUsgaeObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPunchUsgaeId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update a1History in DB
     * @param customerPunchUsageObject $customerPunchUsgaeObj 
     * @return resultObject
     */
    public static function updateCustomerPunchUsage(customerPunchUsageObject $customerPunchUsgaeObj){        
        try{
            $instance = new self();
            $instance->upateCustomerPunchUsageDB($customerPunchUsgaeObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($customerPunchUsgaeObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete customerPunchUsgae from DB
     * @param int $customerPunchUsgaeID 
     * @return resultObject
     */
    public static function deleteCustomerPunchUsageById($customerPunchUsgaeID){
        try{
            $instance = new self();
            $instance->deleteCustomerPunchUsageByIdDB($customerPunchUsgaeID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$customerPunchUsgaeID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC CUSTOMERPUNCHUSAGE - DB METHODS           */
    /************************************* */

    private function addCustomerPunchUsageDB(customerPunchUsageObject $obj){

        if (!isset($obj)){
            throw new Exception("customerPunchUsgaeObject value must be provided");             
        }

        $cpuse_status_change_timeDate = isset($obj->cpuse_status_change_time) ? "'".$obj->cpuse_status_change_time."'" : "null";

        $newId = $this->db->execute("INSERT INTO tbl_cust_punch_usage SET 
                                        cpuse_cust_id = {$obj->cpuse_cust_id},
                                        cpuse_biz_id = {$obj->cpuse_biz_id},
                                        cpuse_cpp_id = {$obj->cpuse_cpp_id},
                                        cpuse_type = '{$obj->cpuse_type}',
                                        cpuse_item_type = '{$obj->cpuse_item_type}',
                                        cpuse_item_id = {$obj->cpuse_item_id},
                                        cpuse_source = '{$obj->cpuse_source}',
                                        cpuse_source_id = {$obj->cpuse_source_id},
                                        cpuse_note = '".addslashes($obj->cpuse_note)."',
                                        cpuse_status = '{$obj->cpuse_status}',
                                        cpuse_status_change_time = $cpuse_status_change_timeDate,
                                        cpuse_status_changer_id = {$obj->cpuse_status_changer_id},
                                        cpuse_status_note = '".addslashes($obj->cpuse_status_note)."'
                                                 ");
        return $newId;
    }

    private function loadCustomerPunchUsageFromDB($customerPunchUsgaeID){

        if (!is_numeric($customerPunchUsgaeID) || $customerPunchUsgaeID <= 0){
            throw new Exception("Illegal value customerPunchUsgaeID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_cust_punch_usage WHERE cpuse_id = $customerPunchUsgaeID");
    }

    private function upateCustomerPunchUsageDB(customerPunchUsageObject $obj){

        if (!isset($obj->cpuse_id) || !is_numeric($obj->cpuse_id) || $obj->cpuse_id <= 0){
            throw new Exception("customerPunchUsgaeObject value must be provided");             
        }

        $cpuse_timeDate = isset($obj->cpuse_time) ? "'".$obj->cpuse_time."'" : "null";

        $cpuse_status_change_timeDate = isset($obj->cpuse_status_change_time) ? "'".$obj->cpuse_status_change_time."'" : "null";

        $this->db->execute("UPDATE tbl_cust_punch_usage SET 
                                cpuse_cust_id = {$obj->cpuse_cust_id},
                                cpuse_biz_id = {$obj->cpuse_biz_id},
                                cpuse_cpp_id = {$obj->cpuse_cpp_id},
                                cpuse_type = '{$obj->cpuse_type}',
                                cpuse_time = $cpuse_timeDate,
                                cpuse_item_type = '{$obj->cpuse_item_type}',
                                cpuse_item_id = {$obj->cpuse_item_id},
                                cpuse_source = '{$obj->cpuse_source}',
                                cpuse_source_id = {$obj->cpuse_source_id},
                                cpuse_note = '".addslashes($obj->cpuse_note)."',
                                cpuse_status = '{$obj->cpuse_status}',
                                cpuse_status_change_time = $cpuse_status_change_timeDate,
                                cpuse_status_changer_id = {$obj->cpuse_status_changer_id},
                                cpuse_status_note = '".addslashes($obj->cpuse_status_note)."'
                                WHERE cpuse_id = {$obj->cpuse_id} 
                                         ");
    }

    private function deleteCustomerPunchUsageByIdDB($customerPunchUsgaeID){

        if (!is_numeric($customerPunchUsgaeID) || $customerPunchUsgaeID <= 0){
            throw new Exception("Illegal value customerPunchUsgaeID");             
        }

        $this->db->execute("DELETE FROM tbl_cust_punch_usage WHERE cpuse_id = $customerPunchUsgaeID");
    }

}
