<?php

/**
 * enumOrderStatus - the order's statuses
 * 
 *
 * @version 1.0
 * @author JonathanM
 */
class enumAdminEntities extends Enum{

    const offering = "offering";

    const meeting = "meeting";
    
    const class_date = "class_date";
    
    const member = "member";

    const employee = "employee";
    
}

    

