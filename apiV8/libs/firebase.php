<?php 

require_once(dirname(__FILE__) . '/../MVC/config.php');
require_once(dirname(__FILE__) . '/utilityManager.php');
require_once(dirname(__FILE__) . '/google-api-client/src/Google/autoload.php');
require_once ROOT_PATH.'libs/Composer/vendor/autoload.php';
use Google\Cloud\Firestore\FirestoreClient;
use Google\Cloud\Firestore\FieldValue;

class firebase {
   
    private $db;

    function __construct(){
        $this->db = new FirestoreClient();
    }

    public function getBaseRecordRef($record_name,$collection = 'db'){
        $docRef = $this->db->collection($collection)->document($record_name);
        $snapShot = $docRef->snapshot();
        $result = array();

        if($snapShot->exists()){
            $result['code'] = 1;
            $result['data'] = $docRef;
        }
        else{
            $result['code'] = 0;
        }
        
        return $result;
    }

    public function setBaseRecord($record_data,$record_name = '',$collection = 'db'){
        $ref = $this->db->collection($collection);
        $result = array();
        $result['data'] = array();
        if($record_name != ''){
            $ref->document($record_name)->set($record_data);

            $record = $ref->document($record_name);
            $snap = $record->snapshot();
            $result['data']['inner_data'] = $snap->data();
            $result['data']['record_id'] = $snap->id();
        }
        else{
            
            $record = $ref->add($record_data);
            $snap = $record->snapshot();
            $result['data']['inner_data'] = $snap->data();
            $result['data']['record_id'] = $snap->id();
        }

        

        $result['code'] = 1;

        return $result;
    }

    public function updateBaseRecord($record_data,$record_name = '',$collection = 'db'){
        $ref = $this->db->collection($collection);
        $result = array();
        $result['data'] = array();
        $ref->document($record_name)->update($record_data);

        $record = $ref->document($record_name);
        $snap = $record->snapshot();
        $result['data']['inner_data'] = $snap->data();
        $result['data']['record_id'] = $snap->id();
        $result['code'] = 1;

        return $result;
    }

    public function getCollectionFromRef($ref,$collection){
        $colRef = $ref->collection($collection);
        
        $result = array();

        $result['code'] = 1;
        $result['data'] = $colRef;
        
        return $result;
    }

    public function getRecordsFromCollection($colRef){
        $docs = $colRef->documents();
        $result = array();

        $data = array();

        $result['code'] = 0;
        foreach ($docs as $doc)
        {
        	if($doc->exists()){
                $result['code'] = 1;
                $data[$doc->id()] = $doc->data();
            }
        }
        

        $result['code'] = 1;
        $result['data'] = $data;
        
        return $result;
    }

    public function getRecordFromRef($record_name,$ref){
        $recordRef = $ref->document($record_name);
        $snapShot = $recordRef->snapshot();
        $result = array();

        if($snapShot->exists()){
            $result['code'] = 1;
            $result['data'] = $snapShot->data();
        }
        else{
            $result['code'] = 0;
        }
        
        return $result;
    }

    public function setRecord($ref,$record_data,$record_name = ''){
        $result = array();
        $result['data'] = array();
        if($record_name != ''){
            $ref->document($record_name)->set($record_data);

            $record = $ref->document($record_name);
            $snap = $record->snapshot();
            $result['data']['inner_data'] = $snap->data();
            $result['data']['record_id'] = $snap->id();
        }
        else{
            
            $record = $ref->add($record_data);
            $snap = $record->snapshot();
            $result['data']['inner_data'] = $snap->data();
            $result['data']['record_id'] = $snap->id();
        }

        

        $result['code'] = 1;

        return $result;
    }

    public function getDocument($collection,$document){
        $docRef = $this->db->collection($collection)->document($document);
        $snapshot = $docRef->snapshot();
        if($snapshot->exists()){
            $result['code'] = 1;
            $result['data'] = $snapshot->data();
        }
        else{
            $result['code'] = 0;
        }
        return $result;
    }

    public function deleteDocument($collection,$document){
        $this->db->collection($collection)->document($document)->delete();
    }

}

?>
