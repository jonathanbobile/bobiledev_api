<?php


class imageManager
{
    private $bizID = 0;

    function __construct()
    {
        try {
            $this->db = new Database();
        }
        catch (PDOException $e) {
            die('Database connection could not be established.');
        }       
    }

    public static function createThumb($src_r, $extension, $th_width = 122, $th_height = 91)
    {
        $instance = new self();
        $src_width = imagesx($src_r);
        $src_height = imagesy($src_r);

        if($src_width < $th_width && $src_height < $th_height){
            $th_width = $src_width;
            $th_height = $src_height;
        }


        if ($src_width / $src_height > $th_width / $th_height) {
            $width = round($src_height * $th_width / $th_height);
            $height = $src_height;
            $x = ($src_width - $width) / 2;
            $y = 0;
        } else {
            $height = round($src_width * $th_height / $th_width);
            $width = $src_width;
            $x = 0;
            $y = ($src_height - $height) / 2;
        }

        $dst_r = $instance->getBlankImage($th_width, $th_height, $extension);

        imagecopyresampled($dst_r, $src_r, 0, 0, $x, $y, $th_width, $th_height, $width, $height);

        return $dst_r;
    }


    public static function getBlankImage($w, $h, $extension)
    {
        
        $dst_r = ImageCreateTrueColor($w, $h);
        switch (strtolower($extension))
        {
            case 'png':
                $background = imagecolorallocate($dst_r, 255, 255, 255);
                imagecolortransparent($dst_r, $background);
                imagealphablending($dst_r, false);
                imagesavealpha($dst_r, true);
                break;
            case 'gif':
                $background = imagecolorallocate($dst_r, 255, 255, 255);
                imagecolortransparent($dst_r, $background);
                break;
        }
        return $dst_r;
    }


    public static function createSplash($bizID,$app_name,$templateID) {
        $instance = new self();

        $themeRow = $instance->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $templateID");
        $originalImage = imagecreatefromjpeg(IMAGE_STORAGE_PATH . "templates/{$themeRow["tmpl_name"]}/magicmenu_background.jpg");

        $img_back = imagecreatetruecolor( 1536, 2048 );
        imagecopyresampled( $img_back, $originalImage, 0, 0, 0, 0, 1536, 2048, imagesx($originalImage), imagesy($originalImage) );

        imagefilter($img_back, IMG_FILTER_GRAYSCALE);

        $imgText = stripcslashes($app_name);
        $font = dirname(dirname(__FILE__))."/styles/fonts/arialbd.ttf";
        $fontsize =  $instance->getFontSize(imagesx($img_back) - 200,$font,$imgText);
        if($fontsize > 80) $fontsize=80;

        $bbox = imagettfbbox($fontsize, 0, $font, $imgText);
        $x = $bbox[0] + (imagesx($img_back) / 2) - ($bbox[4] / 2);
        $y = 350;
        $textColor = $instance->hexColorAlloc($img_back, $themeRow["tmpl_color_6"]);
        $borderColor = $instance->hexColorAlloc($img_back, $themeRow["tmpl_color_1"]);


        $instance->imagettfstroketext($img_back,$fontsize,0, $x, $y,$textColor,$borderColor,$font,$imgText,3);

        ob_start();
        imagepng($img_back);
        $image_string = ob_get_contents();
        ob_end_clean();

        $image_storage = imageStorage::getStorageAdapter();
        $amazon_path = "$bizID/" . uniqid($bizID) . '.png';
        $image = $image_storage->uploadFileData($image_string, $amazon_path,200,266,"",1536,2048);

        $data = array('image_url' => $image, 'uid' => $bizID);
        return $instance->update_splash_screen($data);
    }

    public static function resizeImage($image,$width,$height,$dest,$type = 'jpg',$thumbWidth = 0, $thumbHeight = 0){
       
        $img_back = imagecreatetruecolor( $width, $height );
        
        if(strpos(strtolower(strtolower($image)), '.jpg') !== false || strpos(strtolower(strtolower($image)), '.jpeg') !== false){
            $originalImage = imagecreatefromjpeg($image);
            $type = 'jpg';
        }
        if(strpos($image, '.png') !== false){
            $originalImage = imagecreatefrompng($image);
            $type = 'png';
        }
        imagecopyresampled( $img_back, $originalImage, 0, 0, 0, 0, $width, $height, imagesx($originalImage), imagesy($originalImage) );
        

        ob_start();
        if($type == 'jpg'){
            imagejpeg($img_back);
        }
        else{
            imagepng($img_back);
        }
        $image_string = ob_get_contents();
        ob_end_clean();

         $image_storage = imageStorage::getStorageAdapter();
         
         $image = $image_storage->uploadFileData($image_string, $dest,$width,$height);
         return $image;
    }

    public function updateImage($data){

        $image_type = isset($data['image_type']) ? $data['image_type'] : '';
        $forceSize = false;
        switch($image_type){
            case "product_img":
                $thumbWidth = 240;
                $thumbHeight = 160; 
                break;
            case "app_logo":
                $thumbWidth = 400;
                $thumbHeight = 200; 
                $forceSize = true;
                $forceSizeWidth = 500;
                $forceSizeHeight = 250;
                break;
            case "content_img":   
            case "slider_img": 
                $thumbWidth = 300;
                $thumbHeight = 166; 
                break;
            case "news_img":
            case "video_img":
                $thumbWidth = 300;
                $thumbHeight = 190; 
                break;
            case "coupon_img":   
            case "program_img":
                $thumbWidth = 373;
                $thumbHeight = 257; 
                break;
            case "splash_screen":
                $thumbWidth = 200;
                $thumbHeight = 266; 
                $forceSize = true;
                $forceSizeWidth = 1536;
                $forceSizeHeight = 2048;
                break;
            case "biz_module_img":
            case "module_img":  
                $thumbWidth = 290;
                $thumbHeight = 290;
                $forceSize = true;
                $forceSizeWidth = 290;
                $forceSizeHeight = 290;
                break;    
            case "push_img":  
                $thumbWidth = 280;
                $thumbHeight = 146;                
                break; 
            case "gallery_img":  
                $thumbWidth = 124;
                $thumbHeight = 124;                
                break;
            case "custom_img_back":
            case "custom_img_magic":
                $thumbWidth = 165;
                $thumbHeight = 294; 
                $forceSize = true;
                $forceSizeWidth = 828;
                $forceSizeHeight = 1470;
                break;
            case "app_icon":
            case "market_icon":
                $thumbWidth = 290;
                $thumbHeight = 290;
                $forceSize = true;
                $forceSizeWidth = 1024;
                $forceSizeHeight = 1024;
                break;
            default:
                $thumbWidth = 108;
                $thumbHeight = 108;
        }
        if($image_type == "custom_img_back" || $image_type == "custom_img_magic"){
            $theme_name = isset($data['targetFolder']) ? $data['targetFolder'] : "custom_".$data['uid'];
            $jpg_name = $image_type == "custom_img_back" ? "new_background.jpg" : "magicmenu_background.jpg";

            $image_storage = imageStorage::getStorageAdapter();
           
            $cleanFile = substr($data['image_url'],strlen(IMAGE_STORAGE_PATH));
            $data['file_name'] = $image_storage->copyFile($cleanFile,'templates/' . $theme_name . '/' . $jpg_name);
            $data['image_url'] = IMAGE_STORAGE_PATH.$data['file_name'];
            
            $rand = rand(100000,9999999);
            $data['image_url'] .= '?temp='.$rand;
        }

        if($forceSize){
            $data['image_url'] = $this->resizeImageFix($data['image_url'],$data['file_name'],$forceSizeWidth,$forceSizeHeight);
        }


        $method = 'update_' . $image_type;
        if(method_exists($this,$method)){
            $this->$method($data);
        }

        $result = array();
        $result['data'] = array();

        $result['data']['image_url'] = $data['image_url'];
        if($image_type != 'reseller_logo' && $image_type != "custom_img_back" && $image_type != "custom_img_magic"){

            $result['data']['thumb_url'] = $this->createThumbImage($data['image_url'],$data['file_name'], $thumbWidth, $thumbHeight);
        }
        else{
            $result['data']['thumb_url'] = $data['image_url'];
        }
        $result['code'] = 1;

        return $result;
    }

    public function createImageCopy($imageUrl){
        $image_storage = imageStorage::getStorageAdapter();

        $image_name = uniqid($this->bizID);
        $file_name = $this->bizID. '/' .$image_name.substr($imageUrl, strrpos($imageUrl, '.'));
        $cleanFile = substr($imageUrl,strlen(IMAGE_STORAGE_PATH));
        $data['file_name'] = $file_name;
        $data['image_url'] = IMAGE_STORAGE_PATH.$image_storage->copyFile($cleanFile,  $file_name);

        $result = array();

        $result['code'] = 1;
        $result['data'] = $data;

        return $result;
    }

    public function createThumbImage($image,$file_name, $thumbWidth, $thumbHeight){
        
       
        $type= '';
        
        $fileMimeType = utilityManager::get_url_mime_type($image);
        $imageData = file_get_contents($image);
        
        
        $fileData = getimagesizefromstring($imageData);
        $fileMimeType = $fileData['mime'];

        if($fileMimeType == 'image/gif'){
            $originalImage = imagecreatefromgif($image);
            $type = 'gif';
        }else if($fileMimeType == 'image/png'){
            $originalImage = imagecreatefrompng($image);
            $type = 'png';
        }else{
            $originalImage = imagecreatefromjpeg($image);
            $type = 'jpg';
        }

        $thumb = $this->createThumb($originalImage, $type, $thumbWidth, $thumbHeight);
        ob_start();
        if($type == 'jpg'){
            imagejpeg($thumb);
        }
        else{
            imagepng($thumb);
        }
        $thumb_result =  ob_get_contents();
        ob_end_clean();

        $image_storage = imageStorage::getStorageAdapter();

        return $image_storage->uploadFileContent($thumb_result, $file_name,BUCKET_THUMBS);
    }

    public function getBase64Thumb($image,$thumbWidth, $thumbHeight){
        
        $type= '';
        
        $fileMimeType = utilityManager::get_url_mime_type($image);
        $imageData = file_get_contents($image);
        
        
        $fileData = getimagesizefromstring($imageData);
        $fileMimeType = $fileData['mime'];

        if($fileMimeType == 'image/gif'){
            $originalImage = imagecreatefromgif($image);
            $type = 'gif';
        }else if($fileMimeType == 'image/png'){
            $originalImage = imagecreatefrompng($image);
            $type = 'png';
        }else{
            $originalImage = imagecreatefromjpeg($image);
            $type = 'jpg';
        }

        $thumb = $this->createThumb($originalImage, $type, $thumbWidth, $thumbHeight);
        ob_start();
        if($type == 'jpg'){
            imagejpeg($thumb);
        }
        else{
            imagepng($thumb);
        }
        $thumb_result =  ob_get_contents();
        ob_end_clean();

        $dataUri = "data:$fileMimeType;base64," . base64_encode($thumb_result);

        return $dataUri;
    }

    public function resizeImageFix($image,$file_name, $thumbWidth, $thumbHeight){

        
        $type= '';
        $fileMimeType = utilityManager::get_url_mime_type($image);
        
        if($fileMimeType == 'image/gif'){
            $type = 'gif';
            $originalImage = imagecreatefromgif($image);
        }else if($fileMimeType == 'image/png'){
            $type = 'png';
            $originalImage = imagecreatefrompng($image);
        }else{
            $type = 'jpg';
            $originalImage = imagecreatefromjpeg($image);
        }

        $resized = imagescale($originalImage,$thumbWidth,$thumbHeight,IMG_BICUBIC_FIXED);

        ob_start();
        if($type == 'png'){
            imagepng($resized);
        }
        else if($type == 'gif'){
            imagegif($resized);
        }else{
            imagejpeg($resized);
        }
        $resize_result =  ob_get_contents();
        ob_end_clean();

        $image_storage = imageStorage::getStorageAdapter();

        return $image_storage->uploadFileContent($resize_result,$file_name,BUCKET);
    }

    public static function createMarketIcon($icon)
    {
        $instance = new self();
        $image_storage = imageStorage::getStorageAdapter();
        $amazon_path = $_SESSION['id'] . '/' . uniqid($this->bizID) . '.png';
        $image = $image_storage->uploadFile("../../graphics/icons/big/" . $icon . ".png", $amazon_path);

        $data = array('image_url' => $image, 'uid' => $_SESSION['id']);
        return $instance->update_market_icon($data);
    }


    public static function createLogo($bizID,$shortName,$templateID)
    {
        $instance = new self();

        $themeRow = $instance->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $templateID");

        //$originalImage = URL . "img/emptyLogo.png";
        $colors = array();
        $colors[0] = $themeRow["tmpl_color1_freestyle"];
        $colors[1] = $themeRow["tmpl_color1_freestyle"];
        $colors[2] = $themeRow["tmpl_color4_freestyle"];
        $colors[3] = $themeRow["tmpl_color4_freestyle"];

        $im = $instance->gradient(500,250,$colors);

        imagesavealpha($im, true); // important to keep the png's transparency
        $digit = stripcslashes($shortName);
        $font = dirname(dirname(__FILE__))."/styles/fonts/arialbd.ttf";
        $fontsize =  $instance->getFontSize(imagesx($im) - 50,$font,$digit);
        if($fontsize > 60) $fontsize=60;
        $outputImage = str_replace(".","",microtime(true)).".png";

        $bbox = imagettfbbox($fontsize, 0, $font, $digit);
        $x = $bbox[0] + (imagesx($im) / 2) - ($bbox[4] / 2);
        $y = (imagesy($im) / 2) - ($bbox[5] / 2);

        $textColor = $instance->hexColorAlloc($im, $themeRow["tmpl_color2_freestyle"]);
        imagettftext($im, $fontsize, 0, $x, $y, $textColor, $font, $digit);

        $newImgFile  = dirname(dirname(__FILE__))."/img/bizImages/".$outputImage;
        imagepng($im, $newImgFile, 9);  //to file
        imagedestroy($im);
        return $newImgFile;
    }

    public static function createIcon($bizID,$shortName,$templateID)
    {
        $instance = new self();
        $themeRow = $instance->db->getRow("SELECT * FROM tbl_template WHERE tmpl_id = $templateID");

        $instance = new self();
        //$originalImage = URL . "img/emptyIcon.png";
        //$im = imagecreatefrompng($originalImage);
        $colors = array();
        $colors[0] = $themeRow["tmpl_color4_freestyle"];
        $colors[1] = $themeRow["tmpl_color4_freestyle"];
        $colors[2] = $themeRow["tmpl_color1_freestyle"];
        $colors[3] = $themeRow["tmpl_color1_freestyle"];

        $im = $instance->gradient(1024,1024,$colors);
        $digit = stripcslashes($shortName);
        $nameArray = explode(" ",$digit);
        $digit = "";
        foreach ($nameArray as $oneWord)
        {
            if($oneWord != ""){
        	    $digit .= strtoupper($oneWord[0]);
            }
        }

        $font = dirname(dirname(__FILE__))."/styles/fonts/arialbd.ttf";
        $fontsize =  $instance->getFontSize(imagesx($im) - 50,$font,$digit);
        if($fontsize > 400) $fontsize=400;
        $outputImage = str_replace(".","",microtime(true)).".png";

        $bbox = imagettfbbox($fontsize, 0, $font, $digit);
        $x = $bbox[0] + (imagesx($im) / 2) - ($bbox[4] / 2);
        $y = (imagesy($im) / 2) - ($bbox[5] / 2);

        $textColor = $instance->hexColorAlloc($im, $themeRow["tmpl_color2_freestyle"]);
        //$lime = imagecolorallocate($im, 0, 0, 0); // font color

        imagettftext($im, $fontsize, 0, $x, $y, $textColor, $font, $digit);

        $newImgFile  = dirname(dirname(__FILE__))."/img/bizImages/".$outputImage;
        imagepng($im, $newImgFile, 9);  //to file
        imagedestroy($im);
        return $newImgFile;
    }


    public static function update_app_icon($data)
    {
        $instance = new self();
        $instance->db->execute("UPDATE tbl_biz SET biz_icon = '" . $data['image_url'] . "', biz_submit_icon = '" . $data['image_url'] . "' WHERE biz_id = " . intval($data['uid']));
        $_SESSION["appData"]["biz_icon"] = $data['image_url'];
        $_SESSION["appData"]["biz_submit_icon"] = $data['image_url'];
        return $data['image_url'];
    }


    public function update_market_icon($data)
    {
        $this->db->execute("UPDATE tbl_biz SET biz_icon = '" . $data['image_url'] . "', biz_submit_icon = '" . $data['image_url'] . "' WHERE biz_id = " . intval($data['uid']));
        $_SESSION["appData"]["biz_icon"] = $data['image_url'];
        $_SESSION["appData"]["biz_submit_icon"] = $data['image_url'];
        return $data['image_url'];
    }


    public static function update_app_logo($data, $autoGenerated=false)
    {
        $instance = new self();
        $addLogoToUpdate = ($autoGenerated) ? ",biz_custom_logo=0" : "";

        $instance->db->execute("UPDATE tbl_biz SET biz_logo = '{$data['image_url']}' $addLogoToUpdate WHERE biz_id = " . intval($data['uid']));
        $_SESSION["appData"]["biz_logo"] = $data['image_url'];

        $instance->db->execute("UPDATE tbl_mod_data0 SET md_pic = '" . $data['image_url'] . "' WHERE md_mod_id = 0 AND md_belong_to_structure=0 AND md_biz_id = " . intval($data['uid']));
        return $data['image_url'];
    }


    public function update_splash_screen($data)
    {
        $this->db->execute("UPDATE tbl_biz SET biz_submit_splash = '{$data['image_url']}' WHERE biz_id = {$data['uid']}");
        $_SESSION["appData"]["biz_submit_splash"] = $data['image_url'];
        return $data['image_url'];
    }


    public function update_module_img($data)
    {
        $this->db->execute("UPDATE tbl_biz_mod SET biz_mod_mod_pic = '" . $data['image_url'] . "' WHERE biz_mod_biz_id = " . intval($data['uid']) . " AND biz_mod_mod_id = " . intval($data['mod_id']));
        $_SESSION["moduleData"][$data['mod_id']]["biz_mod_mod_pic"] = $data['image_url'];
        return $data['image_url'];
    }

    public function update_biz_module_img($data){
        //$data['mod_id'] - it is now biz_mod_id
        $this->db->execute("UPDATE tbl_biz_mod SET biz_mod_mod_pic = '" . $data['image_url'] . "' WHERE biz_mod_biz_id = " . intval($data['uid']) . " AND biz_mod_id = " . intval($data['mod_id']));
        $sql = "SELECT biz_mod_mod_id FROM tbl_biz_mod WHERE biz_mod_id = " . intval($data['mod_id']). " AND biz_mod_biz_id = " . intval($data['uid']);

        $modid = $this->db->getVal($sql);
        if(isset($_SESSION["moduleData"][$modid])){
            $_SESSION["moduleData"][$modid]["biz_mod_mod_pic"] = $data['image_url'];
        }
        return $data['image_url'];
    }


    public function update_content_img($data)
    {
        $row = $this->db->getRow("SELECT * FROM tbl_mod_data{$data['mod_id']}  WHERE md_biz_id  = ".intval($data['uid'])." AND md_row_id = " . intval($data['row_id']));
        if($row['md_parent'] != 0 || $data['mod_id'] == 1){
            $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_pic = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        }
       
        return $data['image_url'];
    }

    public function insert_gallery_img($data)
    {
        $sql = "SELECT IFNULL(MAX(md_index) + 1,1) FROM tbl_mod_data{$data['mod_id']} WHERE md_biz_id={$this->bizID}
                        AND md_mod_id= {$data['mod_id']}
                        AND md_level_no={$data['level']}
                        AND md_parent={$data['mdidparent']}";

        $max_index = $this->db->getVal($sql);
        if($max_index == '' || $max_index == null){
            $max_index = 1;
        }
        $id = $this->db->execute("insert into tbl_mod_data" . $data['mod_id'] . " SET
                        md_biz_id={$this->bizID},
                        md_mod_id= {$data['mod_id']},
                        md_level_no={$data['level']},
                        md_parent={$data['mdidparent']},
                        md_icon='{$data['image_url']}',
                        md_index = $max_index,
                        md_element=12");

        return $this->db->getRow("SELECT * from tbl_mod_data{$data['mod_id']},tbl_mod_elements
                                    where me_id=md_element
                                    and md_biz_id={$_SESSION["appData"]["biz_id"]}
                                    and md_mod_id={$data['mod_id']}
                                    and md_row_id = $id
                                    order by md_index");
    }

    public function update_gallery_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));

        return $data['image_url'];
    }


    public function update_slider_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_pic" . $data['img_index'] . " = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }


    public function update_list_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data" . $data['mod_id'] . " SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }


    public function update_news_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data3 SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }

    public function update_video_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data24 SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }

    public function update_coupon_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data26 SET md_icon = '" . $data['image_url'] . "' WHERE md_biz_id  = " . intval($data['uid']) . " AND md_row_id = " . intval($data['row_id']));
        return $data['image_url'];
    }

    public function update_program_img($data)
    {
        //the image saved on program save
        return $data['image_url'];
    }

    public function update_product_img($data)
    {
        $this->db->execute("UPDATE tbl_mod_data9 SET md_icon = '" . addslashes($data['image_url']) . "' WHERE md_row_id = " . intval($data['row_id']));

        return $data['image_url'];
    }


    public function update_employee_img($data)
    {
        $this->db->execute("UPDATE tbl_employee SET be_pic = '" . $data['image_url'] . "' WHERE be_id = " . intval($data['be_id']) . " AND be_biz_id = " . intval($data['uid']));

        return $data['image_url'];
    }

    public function update_push_img($data)
    {
        $this->db->execute("UPDATE tbl_push SET pu_img = '" . $data['image_url'] . "' WHERE pu_id = " . intval($data['pu_id']) . " AND pu_biz_id = " . intval($data['uid']));

        return $data['image_url'];
    }

    public function multiUploadFiles($files,$data)
    {
        $image_storage = imageStorage::getStorageAdapter();

        $jsona = json_decode($data);
        $modID = $jsona->mid;
        $level = $jsona->level;
        $mdidparent = $jsona->mdidparent;
        $elid = $jsona->elid;
       
        $urlsToAdd = array();
        $originals = array();
        foreach($files['files']['tmp_name'] as $key => $file) {

            $file_content = utilityManager::file_get_contents_pt($file);
            $image_name = uniqid($this->bizID);
            $image_name .= substr($files['files']['name'][$key], strrpos($files['files']['name'][$key], '.'));
            $image_url = $image_storage->uploadFileData($file_content, $this->bizID . '/' . $image_name,124,124,'',1,1);

            $original = array();
            $original['image_url'] = $image_storage->uploadFile($file, $this->bizID . '/original/' . $image_name,124,124,1,1);
            $original['thumb_url'] = utilityManager::getImageThumb($original['image_url']);
            $original['image_name'] = $image_name;
            $original['file_name'] = $this->bizID . '/original/' . $image_name;
            $originals[] = $original;

            $urlsToAdd[] = $image_url;

            
        }

        $result = array();
        $result['added_ids'] = $this->insertMultiGalleryImages($urlsToAdd,$modID,$level,$mdidparent,$elid);
        $result['originals'] = $originals;
        return $result;
    }

    public function insertMultiGalleryImages($images,$modID,$level,$mdidparent,$elid){
        $added = "";
        foreach ($images as $image)
        {
        	$sql = "SELECT IFNULL(MAX(md_index) + 1,1) FROM tbl_mod_data$modID WHERE md_biz_id={$this->bizID}
                        AND md_mod_id= $modID
                        AND md_level_no={$level}
                        AND md_parent={$mdidparent}";

            $max_index = $this->db->getVal($sql);
            if($max_index == '' || $max_index == null){
                $max_index = 1;
            }
            $sql = "INSERT INTO tbl_mod_data$modID
                    SET md_biz_id={$this->bizID},
                        md_mod_id= $modID,
                        md_level_no={$level},
                        md_parent={$mdidparent},
                        md_index=$max_index,
                        md_icon='$image',
                        md_element={$elid}";

            

            $row_id = $this->db->execute($sql);

            if($added != ""){
                $added .= ",";
            }
            $added .= $row_id;
        }
        return $added;
    }

    public function directUploadImageFiles($files){
        $image_storage = imageStorage::getStorageAdapter();
        $response = array();
        $response['success'] = array();
        $response['failed'] = array();
        foreach($files['files']['tmp_name'] as $key => $file) {
            $mimeType= '';
            $fileMimeType = mime_content_type($file);

            if($fileMimeType == 'image/gif'){
                $mimeType = 'gif';
            }else if($fileMimeType == 'image/png'){
                $mimeType = 'png';
            }else{
                $mimeType = 'jpg';
            }

            if($mimeType != ''){
                $image_name = uniqid($this->bizID);

                $file_name = $this->bizID . '/original/' .$image_name.'.'.$mimeType;
                
                $image_url = $image_storage->directUpload($file, $file_name);
                $thumb_url = $this->createThumbImage($image_url,$file_name,132,132);
                
                

                $result = array();
                $result['image_name'] = $image_name;
                $result['file_name'] = $file_name;
                $result['image_url'] = $image_url;
                $result['thumb_url'] = $thumb_url;

                $response['success'][] = $result;
            }
            else{
                $response['failed'][] = $files['files']['name'][$key];
            }
            
        };

        return $response;
    }

    public function directUploadToOriginal($imageUrl){
        $image_storage = imageStorage::getStorageAdapter();
        $image_name = uniqid($this->bizID);
        $file_name = $this->bizID . '/original/' .$image_name.substr($imageUrl, strrpos($imageUrl, '.'));
        
        $image_url = $image_storage->directUpload($imageUrl, $file_name);
        $thumb_url = $this->createThumbImage($image_url,$file_name,132,132);

        $result = array();
        $result['image_name'] = $image_name;
        $result['file_name'] = $file_name;
        $result['image_url'] = $image_url;
        $result['thumb_url'] = $thumb_url;

        return $result;
    }

    public function getCroppedImage($image,$x,$y,$width,$height,$change_dest = '',$force_size = ''){
        
        $targetWidth = $width;
        $targetHeight = $height;
        if($force_size != ''){
            $targetWidth = $force_size['width'];
            $targetHeight = $force_size['height'];
        }
        $img_back = imagecreatetruecolor( $targetWidth, $targetHeight );

        $extension = '';
        $image = str_replace(' ','%20',$image);
        $imageData = file_get_contents($image);
        
        
        $fileData = getimagesizefromstring($imageData);
        $fileMimeType = $fileData['mime'];

        if($fileMimeType == 'image/gif'){
            $extension = 'gif';
            $originalImage = imagecreatefromgif($image);
        }else if($fileMimeType == 'image/png'){
            $extension = 'png';
            $originalImage = imagecreatefrompng($image);
        }else{
            $extension = 'jpg';
            $originalImage = imagecreatefromjpeg($image);
        }

        $img_back = imageManager::getBlankImage($targetWidth, $targetHeight, $extension);

        
        imagecopyresampled( $img_back, $originalImage, 0, 0,$x, $y, $targetWidth,$targetHeight, $width, $height );
        

        ob_start();
        if($extension == 'jpg'){
            imagejpeg($img_back);
        }
        else{
            imagepng($img_back);
        }
        $image_result = ob_get_contents();
        ob_end_clean();


        $image_storage = imageStorage::getStorageAdapter();
        $bucket = BUCKET;
        if($change_dest == ''){
            $image_name = uniqid($this->bizID);
            $file_name = $this->bizID . '/' .$image_name.'.'.$extension;
        }
        else{
            $image_name = uniqid("crop");
            $dest_parts = explode('|',$change_dest);
            if(count($dest_parts) == 1){
                if(strpos($change_dest,'bobile/') !== false){
                    $bucket = 'bobile';
                    $change_dest = str_replace('bobile/','',$change_dest);
                }
            }
            else{
                $bucket = $dest_parts[0];
                $change_dest = $dest_parts[1];
            }
             
            $file_name = $change_dest . '/' .$image_name.'.'.$extension;
            
        }
        $result = array();
        $result['data'] = array();

        $result['data']['file_name'] = $file_name;
        $result['data']['image_url'] = $image_storage->uploadFileContent($image_result,$file_name,$bucket);
        
        $result['code'] = 1;

        return $result;
    }

    public function uploadImageFromBase64($imageBase64Content,$mimeType,$change_dest = ''){
        $imageContent = base64_decode($imageBase64Content);

        $bucket = BUCKET;
        if($change_dest == ''){
            $image_name = uniqid($this->bizID);
            $file_name = $this->bizID . '/' .$image_name.'.'.$mimeType;
        }
        else{
            $image_name = uniqid("filter");
            $dest_parts = explode('|',$change_dest);
            if(count($dest_parts) == 1){
                if(strpos($change_dest,'bobile/') !== false){
                    $bucket = 'bobile';
                    $change_dest = str_replace('bobile/','',$change_dest);
                }
            }
            else{
                $bucket = $dest_parts[0];
                $change_dest = $dest_parts[1];
            }
            
            $file_name = $change_dest . '/' .$image_name.'.'.$mimeType;
            
        }

        $image_storage = imageStorage::getStorageAdapter();

        $result = array();
        $result['data'] = array();
        $result['data']['image_name'] = $image_name;
        $result['data']['file_name'] = $file_name;
        $result['data']['image_url'] = $image_storage->uploadFileContent($imageContent, $file_name,$bucket);

      
        $result['code'] = 1;

        return $result;
    }

    public static function customUploadFile($bizID,$imgURL,$width=0,$height=0,$format="png",$fileName="",$crop_width = 0, $crop_height = 0)
    {
        $image_storage = imageStorage::getStorageAdapter();
        $amazon_path = ($fileName == "") ? "$bizID/". uniqid($bizID) . ".$format" : "$bizID/$fileName";

        if($width != 0){
            return $image_storage->uploadFile($imgURL, $amazon_path, $width, $height,$crop_width,$crop_height);
        }
        else{
            return $image_storage->uploadFile($imgURL, $amazon_path);
        }
    }

    public function deleteImageFile($file_name){
        $image_storage = imageStorage::getStorageAdapter();
        $response = array();

        $image_storage->deleteFile($this->bizID . '/original/' . $file_name);
    }

    public static function getFontSize($targetWidth,$font,$digit)
    {
        $fontsize = 27;
        $bbox = imagettfbbox($fontsize, 0, $font, $digit);
        $scale = $targetWidth / $bbox[4];
        $fontSize = $fontsize * $scale;
        return floor($fontSize);
    }

    public static function hexColorAlloc($im,$hex){
        $hex = ltrim($hex,'#');
        $a = hexdec(substr($hex,0,2));
        $b = hexdec(substr($hex,2,2));
        $c = hexdec(substr($hex,4,2));
        return imagecolorallocate($im, $a, $b, $c);
    }

    public function imagettfstroketext(&$image, $fontsize, $angle, $x, $y, &$textcolor, &$strokecolor, $fontfile, $text, $px) {
        for($c1 = ($x-abs($px)); $c1 <= ($x+abs($px)); $c1++){

            for($c2 = ($y-abs($px)); $c2 <= ($y+abs($px)); $c2++){
                imagettftext($image, $fontsize, $angle, $c1, $c2, $strokecolor, $fontfile, $text);
            }
        }
        return imagettftext($image, $fontsize, $angle, $x, $y, $textcolor, $fontfile, $text);
    }

    public function gradient($w=100, $h=100, $c=array('#FFFFFF','#FF0000','#00FF00','#0000FF'), $hex=true) {

        /*
        Generates a gradient image

        Author: Christopher Kramer

        Parameters:
        w: width in px
        h: height in px
        c: color-array with 4 elements:
        $c[0]:   top left color
        $c[1]:   top right color
        $c[2]:   bottom left color
        $c[3]:   bottom right color

        if $hex is true (default), colors are hex-strings like '#FFFFFF' (NOT '#FFF')
        if $hex is false, a color is an array of 3 elements which are the rgb-values, e.g.:
        $c[0]=array(0,255,255);

         */

        $im=imagecreatetruecolor($w,$h);

        if($hex) {  // convert hex-values to rgb
            for($i=0;$i<=3;$i++) {
                $c[$i]=$this->hex2rgb($c[$i]);
            }
        }

        $rgb=$c[0]; // start with top left color
        for($x=0;$x<=$w;$x++) { // loop columns
            for($y=0;$y<=$h;$y++) { // loop rows
                // set pixel color
                $col=imagecolorallocate($im,$rgb[0],$rgb[1],$rgb[2]);
                imagesetpixel($im,$x-1,$y-1,$col);
                // calculate new color
                for($i=0;$i<=2;$i++) {
                    $rgb[$i]=
                      $c[0][$i]*(($w-$x)*($h-$y)/($w*$h)) +
                      $c[1][$i]*($x     *($h-$y)/($w*$h)) +
                      $c[2][$i]*(($w-$x)*$y     /($w*$h)) +
                      $c[3][$i]*($x     *$y     /($w*$h));
                }
            }
        }
        return $im;
    }

    public function hex2rgb($hex)
    {
        $rgb[0]=hexdec(substr($hex,1,2));
        $rgb[1]=hexdec(substr($hex,3,2));
        $rgb[2]=hexdec(substr($hex,5,2));
        return($rgb);
    }

    public function checkIfBizHasShutterstock($bizId)
    {
        $existActiveAccount = $this->db->getVal("SELECT COUNT(sst_id) FROM tbl_shutterstock WHERE sst_biz_id=$bizId AND sst_status='active'");
        return $existActiveAccount > 0;
    }

    public function getBizPurchasedShutterstockImages($bizId)
    {
        $data = $this->db->getTable("SELECT bsst_image_id FROM tbl_biz_shutterstock WHERE bsst_biz_id=$bizId");

        $existing_images = array();
        if(count($data)>0){
            foreach($data as $image) {
                $existing_images[] = $image['bsst_image_id'];
            }
        }

        return $existing_images;
    }

    public function addShutterstockImageToBiz($bizId,$imageId,$previewUrl,$originalUrl)
    {
        $this->db->execute("INSERT IGNORE INTO tbl_biz_shutterstock 
                                SET bsst_biz_id=$bizId, 
                                bsst_image_id='$imageId',
                                bsst_preview_url='$previewUrl',
                                bsst_original_url='$originalUrl'
                                ");
    }

    public function addShutterstockImageToBizDuplcate($bizId,$imageId,$previewUrl,$originalUrl)
    {
        $this->db->execute("INSERT IGNORE INTO tbl_biz_shutterstock 
                                SET bsst_biz_id=$bizId, 
                                bsst_image_id='$imageId',
                                bsst_preview_url='$previewUrl',
                                bsst_original_url='$originalUrl',
                                bsst_first_time=0
                                ");
    }

    public function getShuterstockImageRow($imageId)
    {
        return $this->db->getRow("SELECT * FROM tbl_biz_shutterstock WHERE bsst_image_id='$imageId'");
    }

    public static function colorizeBasedOnAplhaChannnel( $im_src, $color) {

        $width = imagesx($im_src);
        $height = imagesy($im_src);
        $rgb = explode(",",utilityManager::hex2rgb($color));

        $im_dst = $im_src;

        for( $x=0; $x<$width; $x++ ) {
            for( $y=0; $y<$height; $y++ ) {

                $alpha = ((imagecolorat( $im_src, $x, $y ) >> 24) & 0x7F);

                $col = imagecolorallocatealpha( $im_dst,
                    $rgb[0],
                    $rgb[1],
                    $rgb[2],
                    $alpha
                    );

                imagesetpixel( $im_dst, $x, $y, $col );
            }
        }
        return $im_dst;
    }

    public static function colorizePng( $url, $color) {

        $im_src = imagecreatefrompng($url);
        imagealphablending($im_src, false);
        imagesavealpha($im_src, true);

        $width = imagesx($im_src);
        $height = imagesy($im_src);
        $rgb = explode(",",utilityManager::hex2rgb("#".$color));

        $im_dst = $im_src;

        for( $x=0; $x<$width; $x++ ) {
            for( $y=0; $y<$height; $y++ ) {

                $alpha = ((imagecolorat( $im_src, $x, $y ) >> 24) & 0x7F);

                $col = imagecolorallocatealpha( $im_dst,
                    $rgb[0],
                    $rgb[1],
                    $rgb[2],
                    $alpha
                    );

                imagesetpixel( $im_dst, $x, $y, $col );
            }
        }
        
        imagepng($im_dst);
        imagedestroy($im_dst);
    }
}
