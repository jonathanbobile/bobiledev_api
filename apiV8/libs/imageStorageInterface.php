<?php

/**
 * An interface, containing methods that storage adapters must implement
 *
 *
 * @version 1.0
 * @author Alex
 */
interface ImageStorageInterface
{
    public function uploadFile($file_path, $storage_path, $thumb_width = 122, $thumb_height = 91);
    public function directUpload($file_path, $storage_path);
    public function uploadFileData($file_content, $storage_path, $thumb_width = 122, $thumb_height = 91, $file_path = '');
    public function createAndUploadThumb($file_data, $storage_path, $thumb_width, $thumb_height);
    public function deleteFiles($path);
    public function deleteFile($path);
    public function createFolder($folder);
    public function getFilesInFolder($biz_id, $folder = '');
    public function copyFiles($source_path, $dest_path);
    public function moveFiles($source_path, $dest_path);
    public function exists($file_path);
}
