<?php

/**
 * membershipsManager short summary.
 *
 * membershipsManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class membershipsManager extends Manager
{
   
    function __construct()
    {
        parent::__construct();        
    }

    /************************************* */
    /*   BASIC BIZMEMBERSHIP - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizMembershipObject to DB
     * Return Data = new bizMembershipObject ID
     * @param bizMembershipObject $bizMembershipObj 
     * @return resultObject
     */
    public static function addBizMembership(bizMembershipObject $bizMembershipObj){       
        try{
            $instance = new self();
            $newId = $instance->addBizMembershipDB($bizMembershipObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizMembershipObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizMembership from DB for provided ID
     * * Return Data = bizMembershipObject
     * @param int $bizMembershipId 
     * @return resultObject
     */
    public static function getBizMembershipByID($bizMembershipId){
        
        try {
            $instance = new self();
            $bizMembershipData = $instance->loadBizMembershipFromDB($bizMembershipId);
            
            $bizMembershipObj = bizMembershipObject::withData($bizMembershipData);
            $result = resultObject::withData(1,'',$bizMembershipObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizMembershipId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizMembership in DB
     * @param bizMembershipObject $bizMembershipObj 
     * @return resultObject
     */
    public static function updateBizMembership(bizMembershipObject $bizMembershipObj){        
        try{
            $instance = new self();
            $instance->upateBizMembershipDB($bizMembershipObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizMembershipObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizMembership from DB
     * @param int $bizMembershipID 
     * @return resultObject
     */
    public static function deleteBizMembershipById($bizMembershipID){
        try{
            $instance = new self();
            $instance->deleteBizMembershipByIdDB($bizMembershipID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizMembershipID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZMEMBERSHIP - DB METHODS           */
    /************************************* */

    private function addBizMembershipDB(bizMembershipObject $obj){

        if (!isset($obj)){
            throw new Exception("bizMembershipObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_biz_memberships SET 
                                        bm_biz_id = {$obj->bm_biz_id},
                                        bm_mship_level = {$obj->bm_mship_level},
                                        bm_name = '".addslashes($obj->bm_name)."',
                                        bm_notes = '".addslashes($obj->bm_notes)."',
                                        bm_back_color = '".addslashes($obj->bm_back_color)."',
                                        bm_text_color = '".addslashes($obj->bm_text_color)."',
                                        bm_img = '".addslashes($obj->bm_img)."',
                                        bm_min_points = {$obj->bm_min_points},
                                        bm_active = {$obj->bm_active}
                                                 ");
        return $newId;
    }

    private function loadBizMembershipFromDB($bizMembershipID){

        if (!is_numeric($bizMembershipID) || $bizMembershipID <= 0){
            throw new Exception("Illegal value bizMembershipID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_biz_memberships WHERE bm_id = $bizMembershipID");
    }

    private function upateBizMembershipDB(bizMembershipObject $obj){

        if (!isset($obj->bm_id) || !is_numeric($obj->bm_id) || $obj->bm_id <= 0){
            throw new Exception("bizMembershipObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_biz_memberships SET 
                                bm_biz_id = {$obj->bm_biz_id},
                                bm_mship_level = {$obj->bm_mship_level},
                                bm_name = '".addslashes($obj->bm_name)."',
                                bm_notes = '".addslashes($obj->bm_notes)."',
                                bm_back_color = '".addslashes($obj->bm_back_color)."',
                                bm_text_color = '".addslashes($obj->bm_text_color)."',
                                bm_img = '".addslashes($obj->bm_img)."',
                                bm_min_points = {$obj->bm_min_points},
                                bm_active = {$obj->bm_active}
                                WHERE bm_id = {$obj->bm_id} 
                                         ");
    }

    private function deleteBizMembershipByIdDB($bizMembershipID){

        if (!is_numeric($bizMembershipID) || $bizMembershipID <= 0){
            throw new Exception("Illegal value bizMembershipID");             
        }

        $this->db->execute("DELETE FROM tbl_biz_memberships WHERE bm_id = $bizMembershipID");
    }

    /************************************* */
    /*   BASIC BIZMEMBERSHIPBENEFIT - PUBLIC           */
    /************************************* */

    /**
     * Insert new bizMembershipBenefitObject to DB
     * Return Data = new bizMembershipBenefitObject ID
     * @param bizMembershipBenefitObject $bizMembershipBenefitObj 
     * @return resultObject
     */
    public static function addBizMembershipBenefit(bizMembershipBenefitObject $bizMembershipBenefitObj){       
        try{
            $instance = new self();
            $newId = $instance->addBizMembershipBenefitDB($bizMembershipBenefitObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizMembershipBenefitObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Get bizMembershipBenefit from DB for provided ID
     * * Return Data = bizMembershipBenefitObject
     * @param int $bizMembershipBenefitId 
     * @return resultObject
     */
    public static function getBizMembershipBenefitByID($bizMembershipBenefitId){
        
        try {
            $instance = new self();
            $bizMembershipBenefitData = $instance->loadBizMembershipBenefitFromDB($bizMembershipBenefitId);
            
            $bizMembershipBenefitObj = bizMembershipBenefitObject::withData($bizMembershipBenefitData);
            $result = resultObject::withData(1,'',$bizMembershipBenefitObj);
            return $result;
        }
        //catch exception
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizMembershipBenefitId);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Update bizMembershipBenefit in DB
     * @param bizMembershipBenefitObject $bizMembershipBenefitObj 
     * @return resultObject
     */
    public static function updateBizMembershipBenefit(bizMembershipBenefitObject $bizMembershipBenefitObj){        
        try{
            $instance = new self();
            $instance->upateBizMembershipBenefitDB($bizMembershipBenefitObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($bizMembershipBenefitObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
     * Delete bizMembershipBenefit from DB
     * @param int $bizMembershipBenefitID 
     * @return resultObject
     */
    public static function deleteBizMembershipBenefitById($bizMembershipBenefitID){
        try{
            $instance = new self();
            $instance->deleteBizMembershipBenefitByIdDB($bizMembershipBenefitID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$bizMembershipBenefitID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC BIZMEMBERSHIPBENEFIT - DB METHODS           */
    /************************************* */

    private function addBizMembershipBenefitDB(bizMembershipBenefitObject $obj){

        if (!isset($obj)){
            throw new Exception("bizMembershipBenefitObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_memberships_benefits SET 
                                        mben_mb_id = {$obj->mben_mb_id},
                                        mben_biz_id = {$obj->mben_biz_id},
                                        mben_text = '".addslashes($obj->mben_text)."',
                                        mben_type = '{$obj->mben_type}',
                                        mben_entity_id = {$obj->mben_entity_id},
                                        mben_image = '".addslashes($obj->mben_image)."',
                                        mben_title = '".addslashes($obj->mben_title)."',
                                        mben_amount = {$obj->mben_amount}
                                                 ");
        return $newId;
    }

    private function loadBizMembershipBenefitFromDB($bizMembershipBenefitID){

        if (!is_numeric($bizMembershipBenefitID) || $bizMembershipBenefitID <= 0){
            throw new Exception("Illegal value bizMembershipBenefitID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_memberships_benefits WHERE mben_id = $bizMembershipBenefitID");
    }

    private function upateBizMembershipBenefitDB(bizMembershipBenefitObject $obj){

        if (!isset($obj->mben_id) || !is_numeric($obj->mben_id) || $obj->mben_id <= 0){
            throw new Exception("bizMembershipBenefitObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_memberships_benefits SET 
                                mben_mb_id = {$obj->mben_mb_id},
                                mben_biz_id = {$obj->mben_biz_id},
                                mben_text = '".addslashes($obj->mben_text)."',
                                mben_type = '{$obj->mben_type}',
                                mben_entity_id = {$obj->mben_entity_id},
                                mben_image = '".addslashes($obj->mben_image)."',
                                mben_title = '".addslashes($obj->mben_title)."',
                                mben_amount = {$obj->mben_amount}
                                WHERE mben_id = {$obj->mben_id} 
                                         ");
    }

    private function deleteBizMembershipBenefitByIdDB($bizMembershipBenefitID){

        if (!is_numeric($bizMembershipBenefitID) || $bizMembershipBenefitID <= 0){
            throw new Exception("Illegal value bizMembershipBenefitID");             
        }

        $this->db->execute("DELETE FROM tbl_memberships_benefits WHERE mben_id = $bizMembershipBenefitID");
    }

    /********************************************** */
    /* Membership additional functions - PUBLIC     */
    /********************************************** */

    public static function getCustomerMembershipByMembershipID($memberShipID){
        $result = array();
        try{
            $instance = new self();
            $membershipData = $instance->loadBizMembershipFromDB($memberShipID);
            $custMembershipObj = customerMembershipObject::withData($membershipData); 

            $nextMembershipResult = $instance->getNextMemberShipLevel($custMembershipObj);
            
            if($nextMembershipResult->code == 1){
                $custMembershipObj->setNextMembershipLevelData($nextMembershipResult->data);
            }

            $membershipBenefits = $instance->getBizMembershipBenefitsForMembership($custMembershipObj);
            $custMembershipObj->setBenefits($membershipBenefits);

            $result = resultObject::withData(1,'',$custMembershipObj);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$memberShipID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getCustomerMembershipByLevelAndBizID($bizID,$level){
        $result = array();
        try{
            if($level == 0){
                return resultObject::withData(0,'No membership found');
            }
            $instance = new self();
            $membershipData = $instance->getMembershipRowByLevelAndBizID($bizID,$level);
            if(isset($membershipData['bm_id']) && $membershipData['bm_id'] > 0){
                $custMembershipObj = customerMembershipObject::withData($membershipData);
                
               
                $membershipBenefits = $instance->getBizMembershipBenefitsForMembership($custMembershipObj);
                $nextMembershipResult = $instance->getNextMemberShipLevel($custMembershipObj);
                
                if($nextMembershipResult->code == 1){
                    $custMembershipObj->setNextMembershipLevelData($nextMembershipResult->data);
                }
                $custMembershipObj->setBenefits($membershipBenefits);
               
                $result = resultObject::withData(1,'',$custMembershipObj);
            }
            else{
                
                $result = resultObject::withData(0,'No membership found');
            }
            return $result;
        }
        catch(Exception $e){
            $dataArray = array();
            $dataArray['biz_id'] = $bizID;
            $dataArray['level'] = $level;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($dataArray));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Summary of getBizMembershipByLevelAndBizID
     * Return Data = bizMembershipObject
     * @param mixed $bizID 
     * @param mixed $level 
     * @return resultObject
     */
    public static function getBizMembershipByLevelAndBizID($bizID,$level){
        $result = array();
        try{
            $instance = new self();
            $membershipData = $instance->getMembershipRowByLevelAndBizID($bizID,$level);
            if(isset($membershipData['bm_id']) && $membershipData['bm_id'] > 0){
                $membershipObj = bizMembershipObject::withData($membershipData);
                $membershipBenefits = $instance->getBizMembershipBenefitsForMembership($membershipObj);
                $membershipObj->setBenefits($membershipBenefits);
                $result = resultObject::withData(1,'',$membershipObj);
            }
            else{
                $result = resultObject::withData(0,'No membership found');;
            }
            return $result;
        }
        catch(Exception $e){
            $dataArray = array();
            $dataArray['biz_id'] = $bizID;
            $dataArray['level'] = $level;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($dataArray));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    /**
     * Set the customer's membership
     * @param mixed $membership_level 
     * @param mixed $customer_id 
     * @param mixed $biz_id 
     */
    public static function setCustomerMemberShip(bizMembershipObject $membershipObject, customerObject $customerObject){
       
        $customerModel = new customerModel();

        $customerObject->cust_membership_id = $membershipObject->bm_id;
        $customerObject->cust_membership_level = $membershipObject->bm_mship_level;
        $customerObject->cust_membership_since = utilityManager::getSQLDateFormatTimeCurrentTimestamp();
        $customerModel->updateCustomer($customerObject);

        $benefits = $membershipObject->getBenefits();

        foreach ($benefits as $benefit)
        {
        	if($benefit->mben_type != 'text' && $benefit->mben_type != 'discount'){
                $hasItem = true;
                
                if($benefit->mben_type == 'product'){
                    $levelManager = new levelDataManager(enumModuleNumber::mobile_shop);
                    $result = $levelManager->getLevelDataByID($benefit->mben_entity_id);
                    if($result->code == 1){
                        if(isset($result->data->md_head) && $result->data->md_head != ""){
                            $benefit->mben_title = $result->data->md_head;
                            
                            if($benefit->mben_text == ''){
                                $benefit->mben_text = $result->data->md_desc;
                            }
                        }else{
                            $hasItem = false;
                        }
                    }else{
                        $hasItem = false;
                    }
                }
                if($benefit->mben_type == 'service'){
                    $bookManager = new bookingManager();
                    $result = $bookManager->getServiceByID($benefit->mben_entity_id);
                    if($result->code == 1){
                        if(isset($result->data->bmt_name) && $result->data->bmt_name != ""){
                            $benefit->mben_title = $result->data->bmt_name;
                            if($benefit->mben_text == ''){
                                $benefit->mben_text = $result->data->bmt_desc;
                            }
                        }
                        else{
                            $hasItem = false;
                        }
                    }else{
                        $hasItem = false;
                    }
                }

                if($hasItem){

                    $custBenefitObj = new customerBenefitObject ();
                    $custBenefitObj->cb_biz_id = $customerObject->cust_biz_id;
                    $custBenefitObj->cb_cust_id = $customerObject->cust_id;
                    $custBenefitObj->cb_type = 'membership';
                    $custBenefitObj->cb_granted_by = 'membership';
                    $custBenefitObj->cb_granted_by_id = $benefit->mben_mb_id;
                    $custBenefitObj->cb_title = $benefit->mben_title;
                    $custBenefitObj->cb_item_type = $benefit->mben_type;
                    $custBenefitObj->cb_external_id = $benefit->mben_entity_id;
                    $custBenefitObj->cb_value = 0;
                    $custBenefitObj->cb_description = $benefit->mben_text;
                    $custBenefitObj->cb_code = utilityManager::generateRedeemCode();
                    $custBenefitObj->cb_image = $benefit->mben_image;

                    $result = customerManager::addCustomerBenefit($custBenefitObj);
                    if($result->code == 1){
                        customerManager::addBadgeToCustomer($customerObject->cust_id,enumBadges::benefits);
                    }                    
                }
            }
        }

        return resultObject::withData(1,"ok");        
    }

    public static function getNextMemberShipLevel(bizMembershipObject $membershipObj){
        try{
            $instance = new self();
            $nextMemberShip = $instance->getMembershipRowByLevelAndBizID($membershipObj->bm_biz_id,$membershipObj->bm_mship_level + 1);

            if(isset($nextMemberShip['bm_id']) && $nextMemberShip['bm_id'] > 0){
                
                $nextMembershipData = $instance->loadBizMembershipFromDB($nextMemberShip['bm_id']);
                $membershipObj = bizMembershipObject::withData($nextMembershipData);
                
                $result = resultObject::withData(1,'',$membershipObj);
                return $result;
            }
            else{
                $result = resultObject::withData(0,'No membership found');
            }

            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($membershipObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    public static function getMembershipDiscount($membershipId){
        $instance = new self();
        $membershipDiscount = $instance->db->getVal("SELECT mben_amount FROM tbl_memberships_benefits
                                                            WHERE mben_mb_id=$membershipId
                                                            AND mben_type='discount'");

        $discount = $membershipDiscount != "" ? $membershipDiscount : 0;

        return $discount;
    }

    public static function getMembershipBenefitsListByMembershipID($membershipId){
        $result = array();
        try{
            $instance = new self();

            $benefitsRows = $instance->getMembershipBenefitsRowsByMembershipID($membershipId);

            foreach ($benefitsRows as $benefitRow)
            {
            	$bizMembershipBenefitObj = bizMembershipBenefitObject::withData($benefitRow);
                $result[] = $bizMembershipBenefitObj;
            }
            

            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$membershipId);
            return $result;
        }
    }

    /********************************************** */
    /* Membership additional functions - PRIVATE    */
    /********************************************** */

    private function getBizMembershipBenefitsForMembership(bizMembershipObject $bizMembershipObj){
        $sql = "SELECT * FROM tbl_memberships_benefits
                WHERE mben_mb_id = $bizMembershipObj->bm_id";

        $benefitsEntries = $this->db->getTable($sql);

        $benefitsList = array();

        foreach ($benefitsEntries as $entry)
        {
        	$benefitsList[] = bizMembershipBenefitObject::withData($entry);
        }
        
        return $benefitsList;
    }   

    private function getMembershipRowByLevelAndBizID($bizID,$level){
        if(!is_numeric($bizID) || $bizID <= 0){
            throw new Exception("Illegal value bizID");
        }

        if(!is_numeric($level) || $level <= 0){
            throw new Exception("Illegal value level");
        }

        $sql = "SELECT * FROM tbl_biz_memberships
                    WHERE bm_biz_id = $bizID
                    AND bm_mship_level = $level";        

        return $this->db->getRow($sql);
    }

    private function getMembershipBenefitsRowsByMembershipID($membershipId){
        $sql = "SELECT * FROM tbl_memberships_benefits WHERE mben_mb_id = $membershipId";

        return $this->db->getTable($sql);
    }
}
