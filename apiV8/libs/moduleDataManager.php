<?php

/**
 * moduleManager short summary.
 *
 * moduleManager description.
 *
 * @version 1.0
 * @author JonathanM
 */
class moduleDataManager extends Manager
{
    public function getModuleByModIdAndRowID($modID,$rowID){
        try{
            $levelData = $this->getLevelRowByModIDAndRowID($modID,$rowID);
            $levelObj = levelDataObject::withData($levelData);
            return resultObject::withData(1,'',$levelObj);
        }
        catch(Exception $e){
            $data = array();
            $data['modID'] = $modID;
            $data['rowID'] = $rowID;
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($data));
            return resultObject::withData(0,$e->getMessage());
        }
    }

    public function getModuleRowByModIDAndRowID($modID,$rowID){
        if(!is_numeric($modID) || $modID < 0){
            throw new Exception('Illegal module ID');
        }

        if(!is_numeric($rowID) || $rowID <= 0){
            throw new Exception('Illegal row ID');
        }

        $sql = "SELECT * FROM tbl_mod_data$modID
                WHERE md_row_id = $rowID"; 

        return $this->db->getRow($sql);
    }

    public function getModuleChildRowsByModIDAndParentID($modID,$parentID){
        if(!is_numeric($modID) || $modID < 0){
            throw new Exception('Illegal module ID');
        }

        if(!is_numeric($parentID) || $parentID <= 0){
            throw new Exception('Illegal parent ID');
        }

        $sql = "SELECT * FROM tbl_mod_data$modID
                WHERE md_parent = $parentID";

        return $this->db->getTable($sql);
    }

    public function getModuleLevelRowsForBizByModIDAndBizIDAndLevel($modID,$bizID,$level){
        if(!is_numeric($modID) || $modID < 0){
            throw new Exception('Illegal module ID');
        }

        if(!is_numeric($bizID) || $bizID <= 0){
            throw new Exception('Illegal biz ID');
        }

        if(!is_numeric($level) || $level <= 0){
            throw new Exception('Illegal level');
        }

        $sql = "SELECT * FROM tbl_mod_data$modID
                WHERE md_biz_id = $bizID
                AND md_level_no = $level";

        return $this->db->getTable($sql);
    }

    public function loadModulesForMobileFromDB(bizModObject $bizModObject){

        if(!is_numeric($bizModObject->biz_mod_biz_id) || $bizModObject->biz_mod_biz_id <= 0){
            throw new Exception('Illegal biz ID');
        }

        $excludeModules = "16";
        if($bizModObject->biz_mod_stuct == "0" || $bizModObject->biz_mod_stuct == "91"){
            $excludeModules = "0,16";
        }

        $modSQL = "SELECT 0 as row_id,biz_mod_id as biz_mod_instance_id, biz_mod_mod_id,REPLACE(biz_mod_mobile_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,
                    IFNULL(biz_mod_mod_pic, mod_pic) biz_mod_mod_pic,biz_mod_mod_large_pic,
                    biz_mod_stuct,biz_mod_has_reviews,
                    mod_reg_type,biz_mod_index orderCollumn
                                        FROM tbl_biz_mod,tbl_modules  
                                        WHERE biz_mod_active=1 
                                        AND biz_mod_mod_id=mod_id 
                                        AND biz_mod_biz_id={$bizModObject->biz_mod_biz_id} 
                                        AND mod_id not in ($excludeModules)
                                        AND (mod_type=1 OR (mod_id = 32 AND biz_mod_can_delete=1))
                                        ORDER BY orderCollumn";
        return $this->db->getTable( $modSQL );
    }

    public function loadModulesForAdminForBiz($bizID){
        return $this->db->getTable("SELECT 0 as row_id, biz_mod_mod_id,REPLACE(biz_mod_mod_name, 'Your Club', 'Join Our Club') biz_mod_mod_name,
                                        IFNULL(biz_mod_mod_pic, mod_pic) biz_mod_mod_pic,biz_mod_active,biz_mod_mobile_name,
                                        biz_mod_stuct,
                                        mod_reg_type,biz_mod_index orderCollumn
                                        FROM tbl_biz_mod,tbl_modules 
                                        WHERE biz_mod_mod_id=mod_id 
                                        and biz_mod_biz_id=$bizID 
                                        and mod_id not in (0,16,31)
                                        and mod_type=1
                                        order by orderCollumn");
    }

    /************************************* */
    /*   BASIC MODULE - PUBLIC           */
    /************************************* */

    /**
    * Insert new moduleObject to DB
    * Return Data = new moduleObject ID
    * @param moduleObject $moduleObj 
    * @return resultObject
    */
    public function addModule(moduleObject $moduleObj){       
            try{
                $newId = $this->addModuleDB($moduleObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($moduleObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get module from DB for provided ID
    * * Return Data = moduleObject
    * @param int $moduleId 
    * @return resultObject
    */
    public function getModuleByID($moduleId){
        
            try {
                $moduleData = $this->loadModuleFromDB($moduleId);
            
                $moduleObj = moduleObject::withData($moduleData);
                $result = resultObject::withData(1,'',$moduleObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$moduleId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update module in DB
    * @param moduleObject $moduleObj 
    * @return resultObject
    */
    public function updateModule(moduleObject $moduleObj){        
            try{
                $this->upateModuleDB($moduleObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($moduleObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete module from DB
    * @param int $moduleID 
    * @return resultObject
    */
    public function deleteModuleById($moduleID){
            try{
                $this->deleteModuleByIdDB($moduleID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$moduleID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC MODULE - DB METHODS           */
    /************************************* */

    private function addModuleDB(moduleObject $obj){

    if (!isset($obj)){
                throw new Exception("moduleObject value must be provided");             
            }

    $newId = $this->db->execute("INSERT INTO tbl_modules SET 
    mod_name = '".addslashes($obj->mod_name)."',
    mod_pic = '".addslashes($obj->mod_pic)."',
    mod_active = {$obj->mod_active},
    mod_index = {$obj->mod_index},
    mod_description = '".addslashes($obj->mod_description)."',
    mod_short_desc = '".addslashes($obj->mod_short_desc)."',
    mod_payd = {$obj->mod_payd},
    mod_reg_type = {$obj->mod_reg_type},
    mod_have_ui = {$obj->mod_have_ui},
    mod_have_layout = {$obj->mod_have_layout},
    mod_line_color = '".addslashes($obj->mod_line_color)."',
    mod_struct = {$obj->mod_struct},
    mod_type = {$obj->mod_type},
    mod_feature = {$obj->mod_feature},
    mod_menuid = {$obj->mod_menuid},
    mod_sell_text = '".addslashes($obj->mod_sell_text)."',
    mod_price = {$obj->mod_price},
    mod_group = {$obj->mod_group},
    mod_store = {$obj->mod_store},
    mod_plan_group = {$obj->mod_plan_group},
    mod_set_as_home_screen = {$obj->mod_set_as_home_screen},
    mod_can_be_hidden = {$obj->mod_can_be_hidden},
    mod_ui_type = '{$obj->mod_ui_type}',
    mod_element_based = '{$obj->mod_element_based}',
    mod_turn_to_page_element = {$obj->mod_turn_to_page_element},
    mod_svg = '".addslashes($obj->mod_svg)."',
    mod_url = '".addslashes($obj->mod_url)."',
    mod_tooltip_id = {$obj->mod_tooltip_id},
    mod_plan_only = {$obj->mod_plan_only},
    mod_can_link = {$obj->mod_can_link},
    mod_can_review = {$obj->mod_can_review},
    mod_min_instances = {$obj->mod_min_instances}
             ");
             return $newId;
        }

    private function loadModuleFromDB($moduleID){

    if (!is_numeric($moduleID) || $moduleID <= 0){
                throw new Exception("Illegal value moduleID");             
            }

            return $this->db->getRow("SELECT * FROM tbl_modules WHERE mod_id = $moduleID");
        }

    public function loadAllModulesForBizFromDb($bizID){

        if (!is_numeric($bizID) || $bizID <= 0){
            throw new Exception("Illegal value bizID");             
        }

        return $this->db->getTable("SELECT * FROM tbl_modules 
                                    LEFT JOIN tbl_biz_mod ON biz_mod_mod_id=mod_id 
                                    AND biz_mod_biz_id=$bizID 
                                    WHERE mod_active = 1 
                                    AND mod_id NOT IN(0,31)");
    }

    private function upateModuleDB(moduleObject $obj){

    if (!isset($obj->mod_id) || !is_numeric($obj->mod_id) || $obj->mod_id <= 0){
                throw new Exception("moduleObject value must be provided");             
            }

    $this->db->execute("UPDATE tbl_modules SET 
    mod_name = '".addslashes($obj->mod_name)."',
    mod_pic = '".addslashes($obj->mod_pic)."',
    mod_active = {$obj->mod_active},
    mod_index = {$obj->mod_index},
    mod_description = '".addslashes($obj->mod_description)."',
    mod_short_desc = '".addslashes($obj->mod_short_desc)."',
    mod_payd = {$obj->mod_payd},
    mod_reg_type = {$obj->mod_reg_type},
    mod_have_ui = {$obj->mod_have_ui},
    mod_have_layout = {$obj->mod_have_layout},
    mod_line_color = '".addslashes($obj->mod_line_color)."',
    mod_struct = {$obj->mod_struct},
    mod_type = {$obj->mod_type},
    mod_feature = {$obj->mod_feature},
    mod_menuid = {$obj->mod_menuid},
    mod_sell_text = '".addslashes($obj->mod_sell_text)."',
    mod_price = {$obj->mod_price},
    mod_group = {$obj->mod_group},
    mod_store = {$obj->mod_store},
    mod_plan_group = {$obj->mod_plan_group},
    mod_set_as_home_screen = {$obj->mod_set_as_home_screen},
    mod_can_be_hidden = {$obj->mod_can_be_hidden},
    mod_ui_type = '{$obj->mod_ui_type}',
    mod_element_based = '{$obj->mod_element_based}',
    mod_turn_to_page_element = {$obj->mod_turn_to_page_element},
    mod_svg = '".addslashes($obj->mod_svg)."',
    mod_url = '".addslashes($obj->mod_url)."',
    mod_tooltip_id = {$obj->mod_tooltip_id},
    mod_plan_only = {$obj->mod_plan_only},
    mod_can_link = {$obj->mod_can_link},
    mod_can_review = {$obj->mod_can_review},
    mod_min_instances = {$obj->mod_min_instances}
    WHERE mod_id = {$obj->mod_id} 
             ");
        }

    private function deleteModuleByIdDB($moduleID){

            if (!is_numeric($moduleID) || $moduleID <= 0){
                throw new Exception("Illegal value moduleID");             
            }

            $this->db->execute("DELETE FROM tbl_modules WHERE mod_id = $moduleID");
        }

    /************************************* */
    /*   BASIC MODULESTRUCTURE - PUBLIC           */
    /************************************* */

    /**
    * Insert new moduleStructureObject to DB
    * Return Data = new moduleStructureObject ID
    * @param moduleStructureObject $moduleStructureObj 
    * @return resultObject
    */
    public function addModuleStructure(moduleStructureObject $moduleStructureObj){       
            try{
                $newId = $this->addModuleStructureDB($moduleStructureObj);         
                $result = resultObject::withData(1,'',$newId);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($moduleStructureObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Get moduleStructure from DB for provided ID
    * * Return Data = moduleStructureObject
    * @param int $moduleStructureId 
    * @return resultObject
    */
    public function getModuleStructureByID($moduleStructureId){
        
            try {
                $moduleStructureData = $this->loadModuleStructureFromDB($moduleStructureId);
            
                $moduleStructureObj = moduleStructureObject::withData($moduleStructureData);
                $result = resultObject::withData(1,'',$moduleStructureObj);
                return $result;
              }
              //catch exception
              catch(Exception $e) {
                  errorManager::addAPIErrorLog('API Model',$e->getMessage(),$moduleStructureId);
                  $result = resultObject::withData(0,$e->getMessage());
                  return $result;
              }
        }

    /**
    * Update moduleStructure in DB
    * @param moduleStructureObject $moduleStructureObj 
    * @return resultObject
    */
    public function updateModuleStructure(moduleStructureObject $moduleStructureObj){        
            try{
                $this->upateModuleStructureDB($moduleStructureObj);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($moduleStructureObj));
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /**
    * Delete moduleStructure from DB
    * @param int $moduleStructureID 
    * @return resultObject
    */
    public function deleteModuleStructureById($moduleStructureID){
            try{
                $this->deleteModuleStructureByIdDB($moduleStructureID);
                $result = resultObject::withData(1);
                return $result;
            }
            catch(Exception $e){
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$moduleStructureID);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
        }

    /************************************* */
    /*   BASIC MODULESTRUCTURE - DB METHODS           */
    /************************************* */

    private function addModuleStructureDB(moduleStructureObject $obj){

    if (!isset($obj)){
                throw new Exception("moduleStructureObject value must be provided");             
            }

    $newId = $this->db->execute("INSERT INTO tbl_mod_struct SET 
                                    ms_level_no = {$obj->ms_level_no},
                                    ms_friendly_name = '".addslashes($obj->ms_friendly_name)."',
                                    ms_view_type = '".addslashes($obj->ms_view_type)."',
                                    ms_view_type_android = '".addslashes($obj->ms_view_type_android)."',
                                    ms_view_end = '".addslashes($obj->ms_view_end)."',
                                    ms_view_end_android = '".addslashes($obj->ms_view_end_android)."',
                                    ms_nib_name = '".addslashes($obj->ms_nib_name)."',
                                    ms_nib_name_android = '".addslashes($obj->ms_nib_name_android)."',
                                    ms_template = {$obj->ms_template},
                                    ms_font = '".addslashes($obj->ms_font)."',
                                    ms_font_color = '".addslashes($obj->ms_font_color)."',
                                    ms_btn_image = '".addslashes($obj->ms_btn_image)."',
                                    ms_html_site = '".addslashes($obj->ms_html_site)."',
                                    ms_html_element = '".addslashes($obj->ms_html_element)."',
                                    ms_icon = '".addslashes($obj->ms_icon)."',
                                    ms_img1_w = {$obj->ms_img1_w},
                                    ms_img2_w = {$obj->ms_img2_w},
                                    ms_img3_w = {$obj->ms_img3_w},
                                    ms_img4_w = {$obj->ms_img4_w},
                                    ms_img5_w = {$obj->ms_img5_w},
                                    ms_img6_w = {$obj->ms_img6_w},
                                    ms_img7_w = {$obj->ms_img7_w},
                                    ms_img8_w = {$obj->ms_img8_w},
                                    ms_img9_w = {$obj->ms_img9_w},
                                    ms_img10_w = {$obj->ms_img10_w},
                                    ms_img1_default = '".addslashes($obj->ms_img1_default)."',
                                    ms_html = '".addslashes($obj->ms_html)."',
                                    ms_tooltip_id = {$obj->ms_tooltip_id},
                                    ms_good_for = '{$obj->ms_good_for}',
                                    ms_req_mods = '".addslashes($obj->ms_req_mods)."',
                                    ms_mainpage_cat = {$obj->ms_mainpage_cat}
                                             ");
             return $newId;
        }

    private function loadModuleStructureFromDB($moduleStructureID){

    if (!is_numeric($moduleStructureID) || $moduleStructureID <= 0){
                throw new Exception("Illegal value moduleStructureID");             
            }

            return $this->db->getRow("SELECT * FROM tbl_mod_struct WHERE ms_mod_id = $moduleStructureID");
        }

    private function upateModuleStructureDB(moduleStructureObject $obj){

    if (!isset($obj->ms_mod_id) || !is_numeric($obj->ms_mod_id) || $obj->ms_mod_id <= 0){
                throw new Exception("moduleStructureObject value must be provided");             
            }

    $this->db->execute("UPDATE tbl_mod_struct SET 
                            ms_level_no = {$obj->ms_level_no},
                            ms_friendly_name = '".addslashes($obj->ms_friendly_name)."',
                            ms_view_type = '".addslashes($obj->ms_view_type)."',
                            ms_view_type_android = '".addslashes($obj->ms_view_type_android)."',
                            ms_view_end = '".addslashes($obj->ms_view_end)."',
                            ms_view_end_android = '".addslashes($obj->ms_view_end_android)."',
                            ms_nib_name = '".addslashes($obj->ms_nib_name)."',
                            ms_nib_name_android = '".addslashes($obj->ms_nib_name_android)."',
                            ms_template = {$obj->ms_template},
                            ms_font = '".addslashes($obj->ms_font)."',
                            ms_font_color = '".addslashes($obj->ms_font_color)."',
                            ms_btn_image = '".addslashes($obj->ms_btn_image)."',
                            ms_html_site = '".addslashes($obj->ms_html_site)."',
                            ms_html_element = '".addslashes($obj->ms_html_element)."',
                            ms_icon = '".addslashes($obj->ms_icon)."',
                            ms_img1_w = {$obj->ms_img1_w},
                            ms_img2_w = {$obj->ms_img2_w},
                            ms_img3_w = {$obj->ms_img3_w},
                            ms_img4_w = {$obj->ms_img4_w},
                            ms_img5_w = {$obj->ms_img5_w},
                            ms_img6_w = {$obj->ms_img6_w},
                            ms_img7_w = {$obj->ms_img7_w},
                            ms_img8_w = {$obj->ms_img8_w},
                            ms_img9_w = {$obj->ms_img9_w},
                            ms_img10_w = {$obj->ms_img10_w},
                            ms_img1_default = '".addslashes($obj->ms_img1_default)."',
                            ms_html = '".addslashes($obj->ms_html)."',
                            ms_tooltip_id = {$obj->ms_tooltip_id},
                            ms_good_for = '{$obj->ms_good_for}',
                            ms_req_mods = '".addslashes($obj->ms_req_mods)."',
                            ms_mainpage_cat = {$obj->ms_mainpage_cat}
                            WHERE ms_mod_id = {$obj->ms_mod_id} 
                                     ");
        }

    private function deleteModuleStructureByIdDB($moduleStructureID){

            if (!is_numeric($moduleStructureID) || $moduleStructureID <= 0){
                throw new Exception("Illegal value moduleStructureID");             
            }

            $this->db->execute("DELETE FROM tbl_mod_struct WHERE ms_mod_id = $moduleStructureID");
        }

    /************************************* */
    /*   GENERSL STATIC FUNCTIONS          */
    /************************************* */

    public static function addDocumentsModuleIfMissing($bizId){

        $instance = new self();

        $sql = "SELECT COUNT(*) FROM tbl_biz_mod 
                    WHERE biz_mod_biz_id=$bizId
                    AND biz_mod_mod_id=12";

        $exists = $instance->db->getVal($sql);
        if($exists == 0){
            $bizModel = new bizModel($bizId);
            
            $bizModObject = new bizModObject();
            $bizModObject->biz_mod_biz_id = $bizId;
            $bizModObject->biz_mod_mod_id = 12;
            $bizModObject->biz_mod_mod_name = 'Documents';
            $bizModObject->biz_mod_mobile_name = 'Documents';
            $bizModObject->biz_mod_mod_pic = 'mod12.png';
            $bizModObject->biz_mod_active = 1;
            $bizModObject->biz_mod_stuct = 106;

            $bizModel->addBizMod($bizModObject);
        }
    }
}
