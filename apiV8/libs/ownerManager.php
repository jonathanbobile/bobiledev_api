<?php

/**
 * ownerManager short summary.
 *
 * ownerManager description.
 *
 * @version 1.0
 * @author Dany
 */
class ownerManager extends Manager
{
    public $ownerID;

    function __construct($ownerID)
    {
        parent::__construct();  
        $this->ownerID = $ownerID;
    }

    //************************* Public Functions ***************************//

    /**
     * Get all bizes belonges to provided owner id
     * ownerId - provided in the constructor
     * Return Data = array of bizObject's
     * @return resultObject
     */
    public function getBizListByOwnerId(){

        try{

            $bizArray = array();
            $responseData = $this->getBizListByOwnerIdDB($this->ownerID);
            if(count($responseData) > 0){
                foreach ($responseData as $oneBiz)
                {
                	$bizObject = bizObject::withData($oneBiz);
                    array_push($bizArray,$bizObject);
                }
                
            }
            return resultObject::withData(1,"Ok",$bizArray);
        }
        catch(Exception $e) {
            errorManager::addAPIErrorLog('API Model',$e->getMessage());
            return resultObject::withData(0,$e->getMessage());
        }
    }

    //************************* Private Functions ***************************//

    private function getBizListByOwnerIdDB(){

        if (!is_numeric($this->ownerID) || $this->ownerID <= 0){
            throw new Exception("Illegal value {$this->ownerID}");             
        }
        
        return $this->db->getTable("SELECT * FROM tbl_biz WHERE biz_owner_id={$this->ownerID} ORDER BY LOWER(biz_short_name)");
    }

    private function getOwnerByResellerId($resellerId){

        if (!is_numeric($resellerId) || $resellerId <= 0){
            throw new Exception("Illegal value resellerId");             
        }

        return $this->db->getRow("SELECT * FROM tbl_owners WHERE owner_reseller_id=$resellerId");
    }

    /************************************* */
    /*   BASIC OWNER - PUBLIC           */
    /************************************* */

    /**
    * Insert new ownerObject to DB
    * Return Data = new ownerObject ID
    * @param ownerObject $ownerObj 
    * @return resultObject
    */
    public function addOwner(ownerObject $ownerObj){       
        try{
            $newId = $this->addOwnerDB($ownerObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($ownerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get owner from DB for provided ID
    * * Return Data = ownerObject
    * @param int $ownerId 
    * @return resultObject
    */
    public function getOwnerByID($ownerId){
        
        try {
            $ownerData = $this->loadOwnerFromDB($ownerId);
            
            $ownerObj = ownerObject::withData($ownerData);
            $result = resultObject::withData(1,'',$ownerObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$ownerId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update owner in DB
    * @param ownerObject $ownerObj 
    * @return resultObject
    */
    public function updateOwner(ownerObject $ownerObj){        
        try{
            $this->upateOwnerDB($ownerObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($ownerObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete owner from DB
    * @param int $ownerID 
    * @return resultObject
    */
    public function deleteOwnerById($ownerID){
        try{
            $this->deleteOwnerByIdDB($ownerID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$ownerID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC OWNER - DB METHODS           */
    /************************************* */

    private function addOwnerDB(ownerObject $obj){

        if (!isset($obj)){
            throw new Exception("ownerObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_owners SET 
                                    owner_ip = '".addslashes($obj->owner_ip)."',
                                    owner_name = '".addslashes($obj->owner_name)."',
                                    owner_username = '".addslashes($obj->owner_username)."',
                                    owner_password = '".addslashes($obj->owner_password)."',
                                    owner_system_lang = '".addslashes($obj->owner_system_lang)."',
                                    owner_short_lang = '".addslashes($obj->owner_short_lang)."',
                                    owner_conf_code = '".addslashes($obj->owner_conf_code)."',
                                    owner_conf_mail = {$obj->owner_conf_mail},
                                    owner_last_app_id = {$obj->owner_last_app_id},
                                    owner_bad_email = {$obj->owner_bad_email},
                                    owner_hide_hint = {$obj->owner_hide_hint},
                                    owner_unsub = {$obj->owner_unsub},
                                    owner_aff_id = {$obj->owner_aff_id},
                                    owner_aff_group = {$obj->owner_aff_group},
                                    owner_zooz_merchant_id = '".addslashes($obj->owner_zooz_merchant_id)."',
                                    owner_facebook_id = '".addslashes($obj->owner_facebook_id)."',
                                    owner_facebook_token = '".addslashes($obj->owner_facebook_token)."',
                                    owner_reseller_id = {$obj->owner_reseller_id},
                                    owner_visitor_id = {$obj->owner_visitor_id},
                                    owner_country = {$obj->owner_country},
                                    owner_device_id = '".addslashes($obj->owner_device_id)."',
                                    owner_device_type = '{$obj->owner_device_type}',
                                    owner_isreal = {$obj->owner_isreal},
                                    owner_account_id = {$obj->owner_account_id},
                                    owner_currency_code = '".addslashes($obj->owner_currency_code)."',
                                    owner_currency_symbol = '".addslashes($obj->owner_currency_symbol)."',
                                    owner_environment = '{$obj->owner_environment}',
                                    owner_stripe_cust_id = '".addslashes($obj->owner_stripe_cust_id)."',
                                    owner_gclid = '".addslashes($obj->owner_gclid)."'
                                             ");
             return $newId;
    }

    private function loadOwnerFromDB($ownerID){

        if (!is_numeric($ownerID) || $ownerID <= 0){
            throw new Exception("Illegal value ownerID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_owners WHERE owner_id = $ownerID");
    }

    private function upateOwnerDB(ownerObject $obj){

        if (!isset($obj->owner_id) || !is_numeric($obj->owner_id) || $obj->owner_id <= 0){
            throw new Exception("ownerObject value must be provided");             
        }

        $this->db->execute("UPDATE tbl_owners SET 
                                owner_ip = '".addslashes($obj->owner_ip)."',
                                owner_name = '".addslashes($obj->owner_name)."',
                                owner_username = '".addslashes($obj->owner_username)."',
                                owner_password = '".addslashes($obj->owner_password)."',
                                owner_system_lang = '".addslashes($obj->owner_system_lang)."',
                                owner_short_lang = '".addslashes($obj->owner_short_lang)."',
                                owner_conf_code = '".addslashes($obj->owner_conf_code)."',
                                owner_conf_mail = {$obj->owner_conf_mail},
                                owner_last_app_id = {$obj->owner_last_app_id},
                                owner_bad_email = {$obj->owner_bad_email},
                                owner_hide_hint = {$obj->owner_hide_hint},
                                owner_unsub = {$obj->owner_unsub},
                                owner_aff_id = {$obj->owner_aff_id},
                                owner_aff_group = {$obj->owner_aff_group},
                                owner_start_date = '{$obj->owner_start_date}',
                                owner_zooz_merchant_id = '".addslashes($obj->owner_zooz_merchant_id)."',
                                owner_facebook_id = '".addslashes($obj->owner_facebook_id)."',
                                owner_facebook_token = '".addslashes($obj->owner_facebook_token)."',
                                owner_reseller_id = {$obj->owner_reseller_id},
                                owner_visitor_id = {$obj->owner_visitor_id},
                                owner_country = {$obj->owner_country},
                                owner_device_id = '".addslashes($obj->owner_device_id)."',
                                owner_device_type = '{$obj->owner_device_type}',
                                owner_isreal = {$obj->owner_isreal},
                                owner_account_id = {$obj->owner_account_id},
                                owner_currency_code = '".addslashes($obj->owner_currency_code)."',
                                owner_currency_symbol = '".addslashes($obj->owner_currency_symbol)."',
                                owner_environment = '{$obj->owner_environment}',
                                owner_stripe_cust_id = '".addslashes($obj->owner_stripe_cust_id)."',
                                owner_gclid = '".addslashes($obj->owner_gclid)."'
                                WHERE owner_id = {$obj->owner_id} 
                                         ");
        }

    private function deleteOwnerByIdDB($ownerID){

        if (!is_numeric($ownerID) || $ownerID <= 0){
            throw new Exception("Illegal value ownerID");             
        }

        $this->db->execute("DELETE FROM tbl_owners WHERE owner_id = $ownerID");
    }

    /************************************* */
    /*   BASIC OWNERPAYMENTSOURCE - PUBLIC           */
    /************************************* */

    /**
    * Insert new ownerPaymentSourceObject to DB
    * Return Data = new ownerPaymentSourceObject ID
    * @param ownerPaymentSourceObject $ownerPaymentSourceObj 
    * @return resultObject
    */
    public function addOwnerPaymentSource(ownerPaymentSourceObject $ownerPaymentSourceObj){       
        try{
            $newId = $this->addOwnerPaymentSourceDB($ownerPaymentSourceObj);         
            $result = resultObject::withData(1,'',$newId);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($ownerPaymentSourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Get ownerPaymentSource from DB for provided ID
    * * Return Data = ownerPaymentSourceObject
    * @param int $ownerPaymentSourceId 
    * @return resultObject
    */
    public function getOwnerPaymentSourceByID($ownerPaymentSourceId){
        
        try {
            $ownerPaymentSourceData = $this->loadOwnerPaymentSourceFromDB($ownerPaymentSourceId);
            
            $ownerPaymentSourceObj = ownerPaymentSourceObject::withData($ownerPaymentSourceData);
            $result = resultObject::withData(1,'',$ownerPaymentSourceObj);
            return $result;
            }
            //catch exception
            catch(Exception $e) {
                errorManager::addAPIErrorLog('API Model',$e->getMessage(),$ownerPaymentSourceId);
                $result = resultObject::withData(0,$e->getMessage());
                return $result;
            }
    }

    /**
    * Update ownerPaymentSourceObject in DB
    * @param ownerPaymentSourceObject $ownerPaymentSourceObj 
    * @return resultObject
    */
    public function updateOwnerPaymentSource(ownerPaymentSourceObject $ownerPaymentSourceObj){        
        try{
            $this->upateOwnerPaymentSourceDB($ownerPaymentSourceObj);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),json_encode($ownerPaymentSourceObj));
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /**
    * Delete ownerPaymentSource from DB
    * @param int $ownerPaymentSourceID 
    * @return resultObject
    */
    public function deleteOwnerPaymentSourceById($ownerPaymentSourceID){
        try{
            $this->deleteOwnerPaymentSourceByIdDB($ownerPaymentSourceID);
            $result = resultObject::withData(1);
            return $result;
        }
        catch(Exception $e){
            errorManager::addAPIErrorLog('API Model',$e->getMessage(),$ownerPaymentSourceID);
            $result = resultObject::withData(0,$e->getMessage());
            return $result;
        }
    }

    /************************************* */
    /*   BASIC OWNERPAYMENTSOURCE - DB METHODS           */
    /************************************* */

    private function addOwnerPaymentSourceDB(ownerPaymentSourceObject $obj){

        if (!isset($obj)){
            throw new Exception("ownerPaymentSourceObject value must be provided");             
        }

        $newId = $this->db->execute("INSERT INTO tbl_owner_payment_sources SET 
                                        ops_owner_id = {$obj->ops_owner_id},
                                        ops_type = '".addslashes($obj->ops_type)."',
                                        ops_ip = '".addslashes($obj->ops_ip)."',
                                        ops_token = '".addslashes($obj->ops_token)."',
                                        ops_stipe_object_id = '".addslashes($obj->ops_stipe_object_id)."',
                                        ops_card_brand = '".addslashes($obj->ops_card_brand)."',
                                        ops_country = '".addslashes($obj->ops_country)."',
                                        ops_exp_month = {$obj->ops_exp_month},
                                        ops_exp_year = {$obj->ops_exp_year},
                                        ops_funding = '".addslashes($obj->ops_funding)."',
                                        ops_last4 = '".addslashes($obj->ops_last4)."',
                                        ops_name = '".addslashes($obj->ops_name)."',
                                        ops_phone = '".addslashes($obj->ops_phone)."',
                                        ops_valid = {$obj->ops_valid},
                                        ops_default = {$obj->ops_default},
                                        ops_suspect = {$obj->ops_suspect}
                                                 ");
        return $newId;
    }

    private function loadOwnerPaymentSourceFromDB($ownerPaymentSourceID){

        if (!is_numeric($ownerPaymentSourceID) || $ownerPaymentSourceID <= 0){
            throw new Exception("Illegal value ownerPaymentSourceID");             
        }

        return $this->db->getRow("SELECT * FROM tbl_owner_payment_sources WHERE ops_id = $ownerPaymentSourceID");
    }

    private function upateOwnerPaymentSourceDB(ownerPaymentSourceObject $obj){

        if (!isset($obj->ops_id) || !is_numeric($obj->ops_id) || $obj->ops_id <= 0){
            throw new Exception("ownerPaymentSourceObject value must be provided");             
        }

        $ops_createdDate = isset($obj->ops_created) ? "'".$obj->ops_created."'" : "null";

        $this->db->execute("UPDATE tbl_owner_payment_sources SET 
        ops_owner_id = {$obj->ops_owner_id},
        ops_type = '".addslashes($obj->ops_type)."',
        ops_created = $ops_createdDate,
        ops_ip = '".addslashes($obj->ops_ip)."',
        ops_token = '".addslashes($obj->ops_token)."',
        ops_stipe_object_id = '".addslashes($obj->ops_stipe_object_id)."',
        ops_card_brand = '".addslashes($obj->ops_card_brand)."',
        ops_country = '".addslashes($obj->ops_country)."',
        ops_exp_month = {$obj->ops_exp_month},
        ops_exp_year = {$obj->ops_exp_year},
        ops_funding = '".addslashes($obj->ops_funding)."',
        ops_last4 = '".addslashes($obj->ops_last4)."',
        ops_name = '".addslashes($obj->ops_name)."',
        ops_phone = '".addslashes($obj->ops_phone)."',
        ops_valid = {$obj->ops_valid},
        ops_default = {$obj->ops_default},
        ops_suspect = {$obj->ops_suspect}
        WHERE ops_id = {$obj->ops_id} 
                 ");
    }

    private function deleteOwnerPaymentSourceByIdDB($ownerPaymentSourceID){

        if (!is_numeric($ownerPaymentSourceID) || $ownerPaymentSourceID <= 0){
            throw new Exception("Illegal value ownerPaymentSourceID");             
        }

        $this->db->execute("DELETE FROM tbl_owner_payment_sources WHERE ops_id = $ownerPaymentSourceID");
    }

}
