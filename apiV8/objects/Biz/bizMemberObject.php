<?php

/**
 * bizMemberObject short summary.
 *
 * bizMemberObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class bizMemberObject extends bobileObject
{

    /* Identifiers */
    public $cust_id = 0;
    public $cust_biz_id = 0;
    public $cust_reseller_id = -1;
    public $cust_market_id = -1;
    public $cust_status = 'guest'; //enum('invited', 'guest', 'member', 'tester', 'preview')
    public $cust_appid = 0;
    public $cust_mobile_serial;
    public $cust_last_mobile_serial;
    public $cust_inviter = 0;
    public $cust_inviter_points = 0;
    public $cust_stripe_cust_id = '';
    public $cust_active = 0;
    public $cust_membership_expiration;

    /* Customer Details */
    public $cust_ip = '';
    public $cust_reg_date;
    public $cust_first_name = '';
    public $cust_pic;
    public $cust_big_pic = '';
    public $cust_email = '';
    public $cust_birth_date;
    public $cust_weding_date;
    public $cust_address;
    public $cust_city = '';
    public $cust_country = 0;
    public $cust_state = 0;
    public $cust_zip;
    public $cust_comments;
    public $cust_gender;
    public $cust_phone1 = '';
    public $cust_phone2;
    public $cust_last_seen;
    public $cust_device = ''; //enum('', 'Android', 'IOS', 'Amazon')
    public $cust_last_device = ''; //enum('', 'Android', 'IOS', 'Amazon')
    public $cust_valid_code = 0;
    public $cust_valid_phone = '';
    public $cust_validation_date;
    public $cust_longt = 0.0;
    public $cust_lati = 0.0;
    public $cust_location_updated;
    public $cust_privacy_time;


    /* Counters | Actions | Gained | Achieved */
    public $cust_current_points = 0;
    public $cust_total_points = 0;
    public $cust_membership_id = 0;
    public $cust_membership_level = 0;
    public $cust_membership_since;
    public $cust_membership_discount=0;
    public $cust_current_stage = 1; //Relationship funnel
    public $cust_unread_chats = 0;
    public $cust_unattended_orders = 0;
    public $cust_unseen_files = 0;
    public $cust_paymentreq_paid = 0;
    public $cust_total_spend = 0;

    /* Idicators - Boolians */
    public $cust_returning = 0;
    public $cust_online = 0;
    public $cust_send_mail = 1;
    public $cust_need_photo_update = 0;
    public $cust_islead = 0;
    public $cust_is_admin = 0;
    public $cust_send_geo_push = 1;

    /* External data */
    public $cust_membership_label = "";
    public $cust_membership_color = "#ffffff";

    function __construct(){}

    public static function withData($data){

        if (!isset($data["cust_id"])){
            throw new Exception("bizMemberObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cust_id = $data["cust_id"];
        $instance->cust_biz_id = $data["cust_biz_id"];
        $instance->cust_reseller_id = $data["cust_reseller_id"];
        $instance->cust_market_id = $data["cust_market_id"];
        $instance->cust_status = $data["cust_status"];
        $instance->cust_returning = $data["cust_returning"];
        $instance->cust_ip = $data["cust_ip"];
        $instance->cust_reg_date = $data["cust_reg_date"];
        $instance->cust_first_name = $data["cust_first_name"];
        $instance->cust_online = $data["cust_online"];

        if(isset($data["cust_pic"]) && $data["cust_pic"] != ""){
            $custPic = $data["cust_pic"];
        }
        else{
            $avatarIndex = ($instance->cust_id % 28) + 1;
            $custPic = "https://storage.googleapis.com/bbassets/avatars/avatar_$avatarIndex.png";
        }

        $instance->cust_pic = $custPic;
        $instance->cust_email = $data["cust_email"];
        $instance->cust_big_pic = $data["cust_big_pic"];
        $instance->cust_birth_date = $data["cust_birth_date"];
        $instance->cust_weding_date = $data["cust_weding_date"];
        $instance->cust_address = $data["cust_address"];
        $instance->cust_city = $data["cust_city"];
        $instance->cust_country = $data["cust_country"];
        $instance->cust_state = $data["cust_state"];
        $instance->cust_zip = $data["cust_zip"];
        $instance->cust_comments = $data["cust_comments"];
        $instance->cust_gender = $data["cust_gender"];
        $instance->cust_send_mail = $data["cust_send_mail"];
        $instance->cust_phone1 = $data["cust_phone1"];
        $instance->cust_phone2 = $data["cust_phone2"];
        $instance->cust_mobile_serial = $data["cust_mobile_serial"];
        $instance->cust_last_mobile_serial = $data["cust_last_mobile_serial"];
        $instance->cust_need_photo_update = $data["cust_need_photo_update"];
        $instance->cust_appid = $data["cust_appid"];
        $instance->cust_last_seen = $data["cust_last_seen"];
        $instance->cust_device = $data["cust_device"];
        $instance->cust_last_device = $data["cust_last_device"];
        $instance->cust_valid_code = $data["cust_valid_code"];
        $instance->cust_valid_phone = $data["cust_valid_phone"];
        $instance->cust_validation_date = $data["cust_validation_date"];
        $instance->cust_islead = $data["cust_islead"];
        $instance->cust_longt = $data["cust_longt"];
        $instance->cust_lati = $data["cust_lati"];
        $instance->cust_location_updated = $data["cust_location_updated"];
        $instance->cust_is_admin = $data["cust_is_admin"];
        $instance->cust_current_points = $data["cust_current_points"];
        $instance->cust_total_points = $data["cust_total_points"];
        $instance->cust_membership_id = $data["cust_membership_id"];
        $instance->cust_membership_level = $data["cust_membership_level"];
        $instance->cust_membership_since = $data["cust_membership_since"];
        $instance->cust_membership_discount = membershipsManager::getMembershipDiscount($data["cust_membership_id"]);
        $instance->cust_current_stage = $data["cust_current_stage"];
        $instance->cust_inviter = $data["cust_inviter"];
        $instance->cust_inviter_points = $data["cust_inviter_points"];
        $instance->cust_unread_chats = $data["cust_unread_chats"];
        $instance->cust_unattended_orders = $data["cust_unattended_orders"];
        $instance->cust_paymentreq_paid = $data["cust_paymentreq_paid"];
        $instance->cust_unseen_files = $data["cust_unseen_files"];
        $instance->cust_privacy_time = $data["cust_privacy_time"];
        $instance->cust_stripe_cust_id = $data["cust_stripe_cust_id"];
        $instance->cust_active = $data["cust_active"];
        $instance->cust_is_fraud = $data["cust_is_fraud"];
        $instance->cust_total_spend = $data["cust_total_spend"];
        $instance->cust_send_geo_push = $data["cust_send_geo_push"];

        $instance->cust_membership_label = isset($data["bm_name"]) ? $data["bm_name"] : "";
        $instance->cust_membership_color = isset($data["bm_back_color"]) ? $data["bm_back_color"] : "#ffffff";

        return $instance;
    }

    public function AdminAPIArray(){
        $result = (array)$this;
        $customerBillng = new customerBillingManager();


        if(bizManager::isBizExclusive($this->cust_biz_id)){
            $result['member_since'] = isset($this->cust_validation_date) ? strtotime($this->cust_validation_date) : 0;
        }
        else{
            $result['member_since'] = strtotime($this->cust_reg_date);
        }

        $result["pay_sources"] = array();
        $paySources = $customerBillng->getCustomerPaymentSources($this->cust_id);
        if($paySources->code == 1){
            $result["pay_sources"] = $paySources->data;
        }

        $result["multi_use"] = array();
        $customerMultiUseResult = customerSubscriptionManager::getAllActiveCustomerMultiuseItems($this->cust_id);

        if($customerMultiUseResult->code == 1){
            $result["multi_use"] = $customerMultiUseResult->data;
        }

        $lastOrderResult = customerOrderManager::getLastCustomerOrder($this);


        $result['shipName'] = "";
        $result['shipCountry'] = "";
        $result['shipCity'] = "";
        $result['shipLine1'] = "";
        $result['shipLine2'] = "";
        $result['shipZip'] = "";
        $result['shipState'] = "";
        if($lastOrderResult->code == 1){
            $lastOrder = $lastOrderResult->data;
            $result['shipName'] = $lastOrder->cto_addr_name;
            $result['shipCountry'] = $lastOrder->cto_addr_country;
            $result['shipCity'] = $lastOrder->cto_addr_city;
            $result['shipLine1'] = $lastOrder->cto_addr_line1;
            $result['shipLine2'] = $lastOrder->cto_addr_line2;
            $result['shipZip'] = $lastOrder->cto_addr_postcode;
            $result['shipState'] = $lastOrder->cto_addr_state;
        }

        return $result;
    }

    public function fillData($data){
        $this->cust_id = $data["cust_id"];
        $this->cust_biz_id = $data["cust_biz_id"];
        $this->cust_reseller_id = $data["cust_reseller_id"];
        $this->cust_market_id = $data["cust_market_id"];
        $this->cust_status = $data["cust_status"];
        $this->cust_returning = $data["cust_returning"];
        $this->cust_ip = $data["cust_ip"];
        $this->cust_reg_date = $data["cust_reg_date"];
        $this->cust_first_name = $data["cust_first_name"];
        $this->cust_online = $data["cust_online"];

        if(isset($custData["cust_pic"]) && $data["cust_pic"] != ""){
            $custPic = $data["cust_pic"];
        }
        else{
            $avatarIndex = ($this->cust_id % 28) + 1;
            $custPic = "https://storage.googleapis.com/bbassets/avatars/avatar_$avatarIndex.png";
        }

        $this->cust_pic = $custPic;
        $this->cust_email = $data["cust_email"];
        $this->cust_big_pic = $data["cust_big_pic"];
        $this->cust_birth_date = $data["cust_birth_date"];
        $this->cust_weding_date = $data["cust_weding_date"];
        $this->cust_address = $data["cust_address"];
        $this->cust_city = $data["cust_city"];
        $this->cust_country = $data["cust_country"];
        $this->cust_state = $data["cust_state"];
        $this->cust_zip = $data["cust_zip"];
        $this->cust_comments = $data["cust_comments"];
        $this->cust_gender = $data["cust_gender"];
        $this->cust_send_mail = $data["cust_send_mail"];
        $this->cust_phone1 = $data["cust_phone1"];
        $this->cust_phone2 = $data["cust_phone2"];
        $this->cust_mobile_serial = $data["cust_mobile_serial"];
        $this->cust_last_mobile_serial = $data["cust_last_mobile_serial"];
        $this->cust_need_photo_update = $data["cust_need_photo_update"];
        $this->cust_appid = $data["cust_appid"];
        $this->cust_last_seen = $data["cust_last_seen"];
        $this->cust_device = $data["cust_device"];
        $this->cust_last_device = $data["cust_last_device"];
        $this->cust_valid_code = $data["cust_valid_code"];
        $this->cust_valid_phone = $data["cust_valid_phone"];
        $this->cust_validation_date = $data["cust_validation_date"];
        $this->cust_islead = $data["cust_islead"];
        $this->cust_longt = $data["cust_longt"];
        $this->cust_lati = $data["cust_lati"];
        $this->cust_location_updated = $data["cust_location_updated"];
        $this->cust_is_admin = $data["cust_is_admin"];
        $this->cust_current_points = $data["cust_current_points"];
        $this->cust_total_points = $data["cust_total_points"];
        $this->cust_membership_id = $data["cust_membership_id"];
        $this->cust_membership_level = $data["cust_membership_level"];
        $this->cust_membership_since = $data["cust_membership_since"];
        $this->cust_current_stage = $data["cust_current_stage"];
        $this->cust_inviter = $data["cust_inviter"];
        $this->cust_inviter_points = $data["cust_inviter_points"];
        $this->cust_unread_chats = $data["cust_unread_chats"];
        $this->cust_unattended_orders = $data["cust_unattended_orders"];
        $this->cust_paymentreq_paid = $data["cust_paymentreq_paid"];
        $this->cust_unseen_files = $data["cust_unseen_files"];
        $this->cust_privacy_time = $data["cust_privacy_time"];
        $this->cust_stripe_cust_id = $data["cust_stripe_cust_id"];
        $this->cust_active = $data["cust_active"];
        $this->cust_is_fraud = $data["cust_is_fraud"];

        $this->cust_membership_label = isset($data["bm_name"]) ? $data["bm_name"] : "";
        $this->cust_membership_color = isset($data["bm_back_color"]) ? $data["bm_back_color"] : "#ffffff";
    }

    function _isValid(){
        return true;
    }
}