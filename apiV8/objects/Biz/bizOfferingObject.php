<?php 

class bizOfferingObject extends bobileObject { 

    public $bao_id = "0";
    public $bao_biz_id = "0";
    public $bao_source;//enum('product','appointment','class','subscription','punchpass','workshop','event','other','coupon','giftcard')
    public $bao_source_mod_id = "0";
    public $bao_source_row_id = "0";
    public $bao_entity;
    public $bao_header;
    public $bao_description;
    public $bao_long_description;
    public $bao_image;
    public $bao_color;
    public $bao_sku;
    public $bao_price = "0";
    public $bao_original_price = "0";
    public $bao_charge_repetition = "once";//enum('once','auto_renew')
    public $bao_charge_freq = "one_time";//enum('one_time','month','year')
    public $bao_coupon_disclaimer;
    public $bao_visible = "1";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["bao_id"])){
              throw new Exception("bizOfferingObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->bao_id = $data["bao_id"];
        $instance->bao_biz_id = $data["bao_biz_id"];
        $instance->bao_source = $data["bao_source"];
        $instance->bao_source_mod_id = $data["bao_source_mod_id"];
        $instance->bao_source_row_id = $data["bao_source_row_id"];        
        $instance->bao_entity = utilityManager::getItemTypeIntFromItemTypeString($data["bao_source"]);
        $instance->bao_header = $data["bao_header"];
        $instance->bao_description = $data["bao_description"];
        $instance->bao_long_description = $data["bao_long_description"];
        $instance->bao_image = $data["bao_image"];
        $instance->bao_color = $data["bao_color"];
        $instance->bao_sku = $data["bao_sku"];
        $instance->bao_price = $data["bao_price"];
        $instance->bao_original_price = $data["bao_original_price"];
        $instance->bao_charge_repetition = $data["bao_charge_repetition"];
        $instance->bao_charge_freq = $data["bao_charge_freq"];
        $instance->bao_coupon_disclaimer = $data["bao_coupon_disclaimer"];
        $instance->bao_visible = $data["bao_visible"];

        return $instance;
    }

    public function AdminAPIArray(){
        $data = array();

         $data["bao_id"] = $this->bao_id;
         $data["bao_biz_id"] = $this->bao_biz_id;
         $data["bao_source"] = $this->bao_source;
         $data["bao_source_mod_id"] = $this->bao_source_mod_id;
         $data["bao_source_row_id"] = $this->bao_source_row_id;        
         $data["bao_entity"] = $this->bao_entity;
         $data["bao_header"] = $this->bao_header ;
         $data["bao_description"] = $this->bao_description;
         $data["bao_long_description"] = $this->bao_long_description;
         $data["bao_image"] = $this->bao_image;
         $data["bao_color"] = $this->bao_color;
         $data["bao_sku"] = $this->bao_sku;
         $data["bao_price"] = $this->bao_price;
         $data["bao_original_price"] = $this->bao_original_price;
         $data["bao_charge_repetition"] = $this->bao_charge_repetition;
         $data["bao_charge_freq"] = $this->bao_charge_freq;
         $data["bao_coupon_disclaimer"] = $this->bao_coupon_disclaimer;
         $data["bao_visible"] = $this->bao_visible;
         $data["cateogries"] = array();
         if($this->bao_source == 'product'){
             $data["cateogries"] = shopManager::getCategoriesForProduct($this->bao_source_row_id);
         }

         return $data;
    }

    function _isValid(){
        return true;
    }
}
?>


