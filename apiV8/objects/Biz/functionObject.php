<?php 

class functionObject extends bobileObject { 

    public $fu_id = "0";
    public $fu_default_label;
    public $fu_icon_name;
    public $fu_image;
    public $fu_ios_selector;
    public $fu_and_selector;
    public $fu_web_icon;
    public $fu_html;
    public $fu_required = "0";
    public $fu_multy = "0";
    public $fu_payd = "0";
    public $fu_description;
    public $fu_price = "0";
    public $fu_url;
    public $fu_full_desc;
    public $fu_req_mod = "0";
    public $fu_biz_type = "all";//enum('all','bobilex','no_bobilex')

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["fu_id"])){
              throw new Exception("functionObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fu_id = $data["fu_id"];
        $instance->fu_default_label = $data["fu_default_label"];
        $instance->fu_icon_name = $data["fu_icon_name"];
        $instance->fu_image = $data["fu_image"];
        $instance->fu_ios_selector = $data["fu_ios_selector"];
        $instance->fu_and_selector = $data["fu_and_selector"];
        $instance->fu_web_icon = $data["fu_web_icon"];
        $instance->fu_html = $data["fu_html"];
        $instance->fu_required = $data["fu_required"];
        $instance->fu_multy = $data["fu_multy"];
        $instance->fu_payd = $data["fu_payd"];
        $instance->fu_description = $data["fu_description"];
        $instance->fu_price = $data["fu_price"];
        $instance->fu_url = $data["fu_url"];
        $instance->fu_full_desc = $data["fu_full_desc"];
        $instance->fu_req_mod = $data["fu_req_mod"];
        $instance->fu_biz_type = $data["fu_biz_type"];

        return $instance;
    }

    public function fillFunctionData($data){
        if (!isset($data["fu_id"])){
            throw new Exception("functionObject requies data array provided!");
        }             

        $this->fu_id = $data["fu_id"];
        $this->fu_default_label = $data["fu_default_label"];
        $this->fu_icon_name = $data["fu_icon_name"];
        $this->fu_image = $data["fu_image"];
        $this->fu_ios_selector = $data["fu_ios_selector"];
        $this->fu_and_selector = $data["fu_and_selector"];
        $this->fu_web_icon = $data["fu_web_icon"];
        $this->fu_html = $data["fu_html"];
        $this->fu_required = $data["fu_required"];
        $this->fu_multy = $data["fu_multy"];
        $this->fu_payd = $data["fu_payd"];
        $this->fu_description = $data["fu_description"];
        $this->fu_price = $data["fu_price"];
        $this->fu_url = $data["fu_url"];
        $this->fu_full_desc = $data["fu_full_desc"];
        $this->fu_req_mod = $data["fu_req_mod"];
        $this->fu_biz_type = $data["fu_biz_type"];
    }

    function _isValid(){
          return true;
    }
}
?>


