<?php 

class pushEventStructureObject extends bobileObject { 

public $pbg_id = "0";
public $pbg_event_id = "0";
public $pbg_name;
public $pbg_img;
public $pbg_img_blank;
public $pbg_font_1 = "opensansregular.ttf";
public $pbg_font_2 = "opensansregular.ttf";
public $pbg_font_3 = "OpenSansItalic.ttf";
public $pbg_font_1_ios = "OpenSans";
public $pbg_font_2_ios = "OpenSans";
public $pbg_font_3_ios = "OpenSans-Italic";
public $pbg_color_1 = "#6b6b6b";
public $pbg_color_2 = "#6b6b6b";
public $pbg_color_3 = "#6b6b6b";
public $pbg_view = "struct1";
public $pbg_view_android = "struct1";
public $pbg_font_name_1;
public $pbg_font_name_2;
public $pbg_font_name_3;

 function __construct(){} 

public static function withData($data){

if (!isset($data["pbg_id"])){
      throw new Exception("pushEventStructureObject constructor requies data array provided!");
}

$instance = new self();

$instance->pbg_id = $data["pbg_id"];
$instance->pbg_event_id = $data["pbg_event_id"];
$instance->pbg_name = $data["pbg_name"];
$instance->pbg_img = $data["pbg_img"];
$instance->pbg_img_blank = $data["pbg_img_blank"];
$instance->pbg_font_1 = $data["pbg_font_1"];
$instance->pbg_font_2 = $data["pbg_font_2"];
$instance->pbg_font_3 = $data["pbg_font_3"];
$instance->pbg_font_1_ios = $data["pbg_font_1_ios"];
$instance->pbg_font_2_ios = $data["pbg_font_2_ios"];
$instance->pbg_font_3_ios = $data["pbg_font_3_ios"];
$instance->pbg_color_1 = $data["pbg_color_1"];
$instance->pbg_color_2 = $data["pbg_color_2"];
$instance->pbg_color_3 = $data["pbg_color_3"];
$instance->pbg_view = $data["pbg_view"];
$instance->pbg_view_android = $data["pbg_view_android"];
$instance->pbg_font_name_1 = $data["pbg_font_name_1"];
$instance->pbg_font_name_2 = $data["pbg_font_name_2"];
$instance->pbg_font_name_3 = $data["pbg_font_name_3"];

return $instance;
}

function _isValid(){
      return true;
}
}
?>


