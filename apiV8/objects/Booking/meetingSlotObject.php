<?php

/**
 * meetingSlotObject short summary.
 *
 * meetingSlotObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class meetingSlotObject extends bobileObject
{ 
    public $meet_emp_id;
    public $meet_emp_name;
    public $meet_emp_pic;
    public $meet_type_id;
    public $meet_type_name;
    public $meet_price;
    public $meet_start_timestamp;
    public $meet_start_time;
    public $meet_end_timestamp;
    public $meet_end_time;

    public static function withData($slotData){
        $instance = new self();

        $instance->meet_emp_id = $slotData["meet_emp_id"];
        $instance->meet_emp_name = $slotData["meet_emp_name"];
        $instance->meet_emp_pic = $slotData["meet_emp_pic"];
        $instance->meet_type_id = $slotData["meet_type_id"];
        $instance->meet_type_name = $slotData["meet_type_name"];
        $instance->meet_price = $slotData["meet_price"];
        $instance->meet_start_timestamp = $slotData["meet_start_timestamp"];
        $instance->meet_start_time = $slotData["meet_start_time"];
        $instance->meet_end_timestamp = $slotData["meet_end_timestamp"];
        $instance->meet_end_time = $slotData["meet_end_time"];

        return $instance;
    }
}