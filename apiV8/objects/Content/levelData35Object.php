<?php 

class levelData35Object extends levelDataObject { 

    public $md_mod_id = "35";

    public $md_display_location = "menu";//enum('module','menu')

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData35Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);

        if($instance->md_external_type == "product"){
            $instance->fillConnectedProduct($data);
        }

        $instance->md_display_location = $data["md_display_location"];


        return $instance;
    }
}
?>


