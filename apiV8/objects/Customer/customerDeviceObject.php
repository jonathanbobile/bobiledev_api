<?php

/**
 * custDeviceObject short summary.
 *
 * custDeviceObject description.
 *
 * @version 1.0
 * @author JonathanM
 */
class customerDeviceObject extends bobileObject
{
    /* Identifiers */
    public $cd_id;
    public $cd_cust_id;
    public $cd_biz_id;

    /* Settings */
    public $cd_device;
    public $cd_app_id;
    public $cd_market_id = -1;
    public $cd_device_type = '';

    function __construct(){
    }

    public static function withData($custDeviceData){
        if(!isset($custDeviceData["cd_id"])){
            throw new Exception("Data incorrect");
        }

        $instance = new self();

        /* Identifiers */
        $instance->cd_id = isset($custDeviceData["cd_id"]) && $custDeviceData["cd_id"] > 0 ? $custDeviceData["cd_id"] : 0;
        $instance->cd_cust_id = isset($custDeviceData["cd_cust_id"]) && $custDeviceData["cd_cust_id"] > 0 ? $custDeviceData["cd_cust_id"] : 0;
        $instance->cd_biz_id = isset($custDeviceData["cd_biz_id"]) && $custDeviceData["cd_biz_id"] > 0 ? $custDeviceData["cd_biz_id"] : 0;

        /* Settings */
        $instance->cd_device = isset($custDeviceData["cd_device"]) ? $custDeviceData["cd_device"] : '';
        $instance->cd_app_id = isset($custDeviceData["cd_app_id"]) ? $custDeviceData["cd_app_id"] : 0;
        $instance->cd_market_id = isset($custDeviceData["cd_market_id"]) && $custDeviceData["cd_market_id"] != '' ? $custDeviceData["cd_market_id"] : -1;
        $instance->cd_device_type = isset($custDeviceData["cd_device_type"]) ? $custDeviceData["cd_device_type"] : "";
        return $instance;
    }
}