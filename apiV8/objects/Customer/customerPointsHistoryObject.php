<?php 

class customerPointsHistoryObject extends bobileObject { 

    public $cph_id = "0";
    public $cph_biz_id = "0";
    public $cph_cust_id = "0";
    public $cph_date = "";
    public $cph_expire_date;
    public $cph_source;//enum('event','scratch_win','scratch_lose','redeemd_points','other','personal_grant','friend','web_register','complete_onboarding','manual_change')
    public $cph_value = "0";
    public $cph_reason_text;
    public $cph_external_id = "0";

    public $description = "";

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cph_id"])){
              throw new Exception("customerPointsHistoryObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cph_id = $data["cph_id"];
        $instance->cph_biz_id = $data["cph_biz_id"];
        $instance->cph_cust_id = $data["cph_cust_id"];
        $instance->cph_date = $data["cph_date"];
        $instance->cph_expire_date = $data["cph_expire_date"];
        $instance->cph_source = $data["cph_source"];
        $instance->cph_value = $data["cph_value"];
        $instance->cph_reason_text = $data["cph_reason_text"];
        $instance->cph_external_id = $data["cph_external_id"];
        $instance->description = isset($data["description"]) ? $data["description"] : "";

        return $instance;
    }

    public function getAPIFormattedArray(){
        $result = array();

        $result["entry_id"] = $this->cph_id;
        $result["source"] = $this->cph_source;
        $result["description"] = stripslashes($this->description);
        $result["points"] = $this->cph_value;
        $result["date"] = $this->cph_date;

        return $result;
    }

    function _isValid(){
          return true;
    }
}
?>


