<?php 

class wishListObject extends bobileObject { 

    public $wl_id = "0";
    public $wl_biz_id = "0";
    public $wl_customer_id = "0";
    public $wl_mod_id = "0";
    public $wl_row_id = "0";
    public $wl_level_id = "0";
    public $title = "";
    public $img = "";
    public $label = "";
    public $md_review_score = 0;
    public $allow_reviews = 0;
    public $ext_type = "";
    public $ext_id = 0;
    public $has_ext_action = 0;
    public $ext_action_title = "";
    public $currency = "";
    function __construct(){} 

    public static function withData($data){

        if (!isset($data["wl_id"])){
              throw new Exception("wishListObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->wl_id = $data["wl_id"];
        $instance->wl_biz_id = $data["wl_biz_id"];
        $instance->wl_customer_id = $data["wl_customer_id"];
        $instance->wl_mod_id = $data["wl_mod_id"];
        $instance->wl_row_id = $data["wl_row_id"];
        $instance->wl_level_id = $data["wl_level_id"];

        return $instance;
    }

    public function PersonalZoneAPIArray(){
        $result = array();
        $levelManager = new levelDataManager($this->wl_mod_id);

        $levelDataResult = $levelManager->getLevelDataByID($this->wl_row_id);
        if($levelDataResult->code == 1){
            $levelData = $levelDataResult->data;
            $result["wl_biz_id"] = $this->wl_biz_id;
            $result["wl_mod_id"] = $this->wl_mod_id;
            $result["wl_row_id"] = $this->wl_row_id;
            $result["wl_level_id"] = $this->wl_level_id;
            $result["ext_type"] = $this->ext_type;
            $result["ext_id"] = $this->ext_id;
            $result["has_ext_action"] = $this->has_ext_action;
            $result["ext_action_title"] = $this->ext_action_title;
            $result["title"] = $levelData->md_head;
            $result["img"] = $levelData->md_icon;
            $result["label"] = $this->wl_mod_id == 3 || $this->wl_mod_id == 26 ? $this->label : $levelData->md_price.$this->currency;
            $result["price"] = $levelData->md_price;                        
            $result["orig_price"] = isset($levelData->md_original_price) ? $levelData->md_original_price : 0;
            $result["needs_shipping"] = $levelData->md_bool2;
            if($this->currency != ""){
                $result["currency"] = $levelData->currency;
            }
        }

        return $result;
    }

    function _isValid(){
          return true;
    }
}
?>


