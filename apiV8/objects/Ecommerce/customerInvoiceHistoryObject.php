<?php 

class customerInvoiceHistoryObject extends bobileObject { 

    public $cinh_id = "0";
    public $cinh_invoice_id = "0";
    public $cinh_event;//enum('created','finalized','failed','paid','closed','voided')
    public $cinh_time = "";
    public $cinh_cust_transaction_id = "0";
    public $cinh_cust_payment_source_id = "0";
    public $cinh_note;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["cinh_id"])){
              throw new Exception("customerInvoiceHistoryObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->cinh_id = $data["cinh_id"];
        $instance->cinh_invoice_id = $data["cinh_invoice_id"];
        $instance->cinh_event = $data["cinh_event"];
        $instance->cinh_time = $data["cinh_time"];
        $instance->cinh_cust_transaction_id = $data["cinh_cust_transaction_id"];
        $instance->cinh_cust_payment_source_id = $data["cinh_cust_payment_source_id"];
        $instance->cinh_note = $data["cinh_note"];

        return $instance;
    }

    function _isValid(){
          return true;
    }
}
?>


