<?php

/**
 * customerOrderStatusHistory short summary.
 *
 * customerOrderStatusHistory description.
 *
 * @version 1.0
 * @author Jonathan
 */
class customerOrderStatusHistory extends bobileObject
{
    public $ctoh_id;
    public $ctoh_order_id;
    public $ctoh_status_id;
    public $ctoh_status_label = "";
    public $ctoh_status_date;
    public $ctoh_status_account_id;
    public $ctoh_status_notes;

    public static function withData($data){
        $instance = new self();

        $instance->ctoh_id = $data["ctoh_id"];
        $instance->ctoh_order_id = $data["ctoh_order_id"];
        $instance->ctoh_status_id = $data["ctoh_status_id"];
        $instance->ctoh_status_date = $data["ctoh_status_date"];
        $instance->ctoh_status_account_id = $data["ctoh_status_account_id"];
        $instance->ctoh_status_notes = $data["ctoh_status_notes"];

        if(isset($data["ors_name"])){
            $instance->ctoh_status_label = $data["ors_name"];
        }

        return $instance;
    }

    public function AdminAPIArray(){
        $result = (array) $this;

        $result['ctoh_status_date'] = strtotime($this->ctoh_status_date);
        $result['ctoh_status_account_name'] = "";
        $result['ctoh_status_account_image'] = "";
        $result['ctoh_status_account_id'] = utilityManager::encodeToHASH($this->ctoh_status_account_id);
        if($this->ctoh_status_account_id > 0){
            
            $accountManager = new accountManager();
            $accountresult = $accountManager->getAccountById($this->ctoh_status_account_id);
            if($accountresult->code == 1){
                $result['ctoh_status_account_name'] = $accountresult->data->ac_name;
                $result['ctoh_status_account_image'] = $accountresult->data->ac_image;
            }
            
        }

        return $result;
    }
}