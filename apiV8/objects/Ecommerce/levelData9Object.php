<?php 

class levelData9Object extends levelDataObject { 

    public $md_mod_id = "9";

    public $md_featured = "0";
    public $md_has_discount = "0";
    public $md_discount_type;//enum('currency','percent','','')
    public $md_discount_amount = "0";
    public $md_adjusted_price = "0";
    public $md_weight = "0";
    public $attributeValues = array();
    public $md_qty_instock = 0;
    public $md_safety_stock = 0;
    public $attributePosition = 0;

    function __construct(){} 

    public static function withData($data){

        if (!isset($data["md_row_id"])){
            throw new Exception("levelData9Object constructor requies data array provided!");
        }

        $instance = new self();

        $instance->fillLevelData($data);
        $instance->fillConnectedProduct($data);

        $instance->md_featured = $data["md_featured"];
        $instance->md_has_discount = $data["md_has_discount"];
        $instance->md_discount_type = $data["md_discount_type"];
        $instance->md_discount_amount = $data["md_discount_amount"];
        $instance->md_adjusted_price = $data["md_adjusted_price"];
        $instance->md_original_price = $data["md_original_price"];
        $instance->md_weight = $data["md_weight"];
        $instance->md_qty_instock = $data["md_qty_instock"];
        $instance->md_safety_stock = $data["md_safety_stock"];
        $instance->variations_count=count($data["variants"]);
        $instance->attributePosition = $data["attrPosition"];

        if(($data["me_name"] == "color_attr" || $data["me_name"] == "list_attr") && count($data["variants"]) == 0 ){
            return $instance;
        }

        if($data["me_name"] == "color_attr" || $data["me_name"] == "list_attr"){
            $attributeResponse = self::fillProductAttribute($data);
            if($attributeResponse["code"] == 1){
                $instance->attributeValues = $attributeResponse["value"];
                $instance->attributePosition = $attributeResponse["attrPosition"];
            }
        }
        return $instance;
    }

    public function fillProductAttribute($levelData){

        $shopManager = new shopManager();
        $atributeValues = $shopManager->getAttributeValues($levelData["md_row_id"],$levelData["attrPosition"]);
        
        $response["code"] = 0;
        $response["attrPosition"] = $levelData["attrPosition"];

        if(count($atributeValues) > 0){
            $response["code"] = 1;
            $response["value"] = $atributeValues;
            $response["attrPosition"] = $levelData["attrPosition"]++;
        }
        return $response;
    }
}
?>


