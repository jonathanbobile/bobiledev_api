<?php

/**
 * productVariationObject short summary.
 *
 * productVariationObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class productVariationObject extends bobileObject
{
    public $id;
    public $product_id;
    public $attributes_values;
    public $attributes_values_ids;
    public $attributes_ids;
    public $surcharge;
    public $sku;
    public $active;

    public static function withData($variation){
        $instance= new self();

        $instance->id = $variation['id'];
        $instance->surcharge = $variation['surcharge'];
        $instance->sku = $variation['sku'];
        $instance->active = $variation['active'];
        $instance->values = $variation['attributes_values'];
        $instance->values_ids = $variation['attributes_values_ids'];
        $instance->attributes = $variation['attributes_ids'];
        $instance->product_id = $variation['product_id'];

        return $instance;
    }

    public function AdminAPIArray(){
        $result = array();

        $result['id'] = $this->id;
        $result['sku'] = $this->sku;
        $result['active'] = $this->active;
        $result['values'] = $this->attributes_values;
        $result['values_ids'] = $this->attributes_values_ids;
        $result['product_id'] = $this->product_id;

        return $result;
    }
}