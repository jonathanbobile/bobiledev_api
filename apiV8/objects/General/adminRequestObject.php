<?php

/**
 * adminRequestObject short summary.
 *
 * adminRequestObject description.
 *
 * @version 1.0
 * @author Jonathan
 */
class adminRequestObject
{
    public $acID;
    public $bizID;
    public $resellerId;
    public $idemKey;
    public $deviceID;
    public $deviceType;
	public $deviceModel;
    public $deviceOrient;
    public $OS;
    public $OSVersion;
    public $lang;
    public $bobileVersionNumber;
    public $long;
    public $lati;
    public $appid;
    public $bundleId;
    public $authToken = "";
    public $caller = "";
    public $fleet_device_id = "";

    function __construct(){}

    public static function withData($GET){
        $instance = new self();

        if(isset($GET["re_id"]) && $GET["re_id"] != "" && $GET["re_id"] != "0"){
            $resellerData = utilityManager::decryptIt($GET["re_id"],true);
            $resellerArray = explode("^",$resellerData);
            $resellerId = $resellerArray[0];
        }else{
            $resellerId = 0;
        }
        $instance->acID = isset($GET["ac_id"]) ? utilityManager::decodeFromHASH($GET["ac_id"]): 0;
        $instance->bizID = isset($GET["bizID"]) && $GET["bizID"] != "" ?  utilityManager::decodeFromHASH($GET["bizID"]) : 0;
        $instance->resellerId = $resellerId;
        $instance->idemKey = isset($GET["idem_key"]) ? $GET["idem_key"] : "";
        $instance->deviceID = $GET["deviceID"];
        $instance->lang = isset($GET["lang"]) ? $GET["lang"] : "en";
        $instance->deviceType = isset($GET["deviceType"]) ? $GET["deviceType"] : "";
        $instance->deviceModel = isset($GET["deviceModel"]) ? $GET["deviceModel"] : "";
        $instance->deviceOrient = isset($GET["deviceOrient"]) ? $GET["deviceOrient"] : "";
        $instance->OS = isset($GET["OS"]) ? $GET["OS"] : "Android";
        $instance->OSVersion = isset($GET["OSVersion"]) ? $GET["OSVersion"] : "";
        $instance->bobileVersionNumber = isset($GET["bobileVersionNumber"]) ? $GET["bobileVersionNumber"] : 0;
        $instance->long = isset($GET["long"]) ? $GET["long"] : "";
        $instance->lati = isset($GET["lati"]) ? $GET["lati"] : "";
        $instance->appid = (!isset($GET["appid"]) || $GET["appid"] == 'null' || $GET["appid"] == '') ? 0 : $GET["appid"];
        $instance->bundleId = isset($GET["bundle_id"]) ? $GET["bundle_id"] : "";
        $instance->caller = isset($GET["caller"]) ? $GET["caller"] : "";
        $instance->fleet_device_id = isset($GET["fleet_device"]) ? $GET["fleet_device"] : "";

        $header = apache_request_headers();
        
        if(isset($header['Authorization'])){
            $instance->authToken = $header['Authorization'];
        }
        else if(isset($header['authorization'])){
            $instance->authToken = $header['authorization'];
        }

        return $instance;
    }

    public function isAuthorizedRequest(){
        
        if($this->authToken == "" || !encryptionManager::isTokenSignatureValid($this->authToken)){
            
            return false;
        }
        
        $tokenPayload = encryptionManager::getJWTPayload($this->authToken);
        $payloadAcID = utilityManager::decodeFromHASH($tokenPayload->ac);
        
        return $tokenPayload->iss == JWT_ISSUER && $payloadAcID == $this->acID;
    }
}