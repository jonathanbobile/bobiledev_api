<?php

/**
 * enumApkUpdateLocation - the types of messages that the socket sends to the clients.
 * 
 *
 * @version 1.0
 * @author DanyG
 */
class enumApkUpdateLocation extends Enum{

    const google_play = 'google_play';
   
    const bobile_market = 'bobile_market';  

    const apple_store = 'apple_store';
}

    

