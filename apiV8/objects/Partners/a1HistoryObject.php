<?php

class a1HistoryObject extends bobileObject {

    public $a1h_id = "0";
    public $a1h_license_id = "0";
    public $a1h_request_id;
    public $a1h_request_type;//enum('purchase','cancel','change','suspend','resume')
    public $a1h_plan_id = "0";
    public $a1h_date = "";

    function __construct(){}

    public static function withData($data){

        if (!isset($data["a1h_id"])){
            throw new Exception("a1HistoryObject constructor requies data array provided!");
        }

        $instance = new self();

        $instance->a1h_id = $data["a1h_id"];
        $instance->a1h_license_id = $data["a1h_license_id"];
        $instance->a1h_request_id = $data["a1h_request_id"];
        $instance->a1h_request_type = $data["a1h_request_type"];
        $instance->a1h_plan_id = $data["a1h_plan_id"];
        $instance->a1h_date = $data["a1h_date"];
      
        return $instance;
    }

    function _isValid(){       
        return true;
    }
}
?>