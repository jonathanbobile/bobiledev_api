<?php

/**
 * adminBizLoyaltyCard short summary.
 *
 * adminBizLoyaltyCard description.
 *
 * @version 1.0
 * @author Jonathan
 */
class adminBizLoyaltyCard extends levelData6Object{

    public $customer_card;

    public static function withData($data){
        $instance = new self();

        $instance->fillLoyaltyData($data);

        if(isset($data['customer_card_id'])){
            
            $instance->customer_card = customerLoyaltyCardObject::withData($data);
        }

        return $instance;
    }
}
