<?php


use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class websocketHandler implements MessageComponentInterface {
    protected $clients;
    protected $wsManager;
    protected $subscribers;
    protected $customersConnections;
    protected $socket;
    public function __construct($socket) {
       
        $this->socket = $socket;
       
        $this->customersConnections = array();
        $this->subscribers = array();
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        
        $this->subscribers[$conn->resourceId] = $conn;
        $result = array();
        $result['code'] = 1;
        $result['type'] = 'connected';
        $result['message'] = '';
        $answer = json_encode($result);
        $conn->send($answer);
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        
        try{
            
            $message = json_decode($msg);
            $function = (string)$message->type;
            
            if(method_exists($this,$function)){
                $answer = $this->{$function}($from,$message->data);
            }
            else{
                $answer = array();

                $result['code'] = -1;
                $result['type'] = 'failure';
                $result['message'] = 'no_function';
            }
            
            $from->send(json_encode($answer));
            
            return $answer;
        }
        catch(Exception $e){
            $result = array();
            $result['code'] = -1;
            $result['type'] = 'failure';
            $result['message'] = 'bad_message';
            $result['data'] = $msg;            

            $from->send(json_encode($result));
            return $result;
        }
    }

    public function onServerMessage($msg){
        $message = json_decode($msg);
        try{
            

            $function = (string)$message->type;
            
            
            $answer = $this->{$function}(null,$message->data);            
            
            $this->socket->send($answer);
            
            return $answer;
        }
        catch(Exception $e){
            $result = array();
            $result['code'] = -1;
            $result['type'] = 'failure';
            $result['message'] = 'bad_message';
            $result['data'] = $message;
            $answer = json_encode($result);

            $this->socket->send($answer);
            return $answer;
        }
    }

    public function heartBeat($conn,$msg){
        $answer = array();

        $answer['type'] = 'heartBeat';
        $answer['data'] = 1;
        $answer['caller'] = isset($msg->caller) ? $msg->caller : '';
        
        return $answer;
    }

    public function serverRequest($conn,$msg){
        $body = "";
        if($msg->data != ""){
            $body = json_decode($msg->data);
        }

        $result = $this->sendCurl('http://127.0.0.1/'.$msg->url,$body);
        
        $answer = array();
        $answer['type'] = 'serverRequest';
        $answer['server_data'] = json_decode($result);
        $answer['caller'] = isset($msg->caller) ? $msg->caller : '';
        
        return $answer;
    }

    private function forgetMe($conn){
        $conn_id = $conn->resourceId;
        $sub = $this->subscribers[$conn_id];

        $identifier = $sub->cust_ident;
        $sconn_id = $sub->con_log_id;

        $this->subscribers[$conn_id]->cust_ident = "";
        $this->subscribers[$conn_id]->con_log_id = 0;

        unset($this->customersConnections[$identifier]);        
        
        $this->sendCurl('http://127.0.0.1/admin/eventAsync/deleteSocketConnection/'.$sconn_id,array());

        $result = array();
        $result['code'] = 1;
        $result['type'] = 'forgot';
        $result['message'] = '';
        
        
        return $result;
    }

    private function handShake($conn,$data){
        
        $conn_id = $conn->resourceId;
        $this->subscribers[$conn_id]->cust_ident = $data;
        $this->customersConnections[$data] = $conn_id;

        $conLogData = array();
        $conLogData['identity'] = $data;
        $conLogData['connection_id'] = $conn_id;
       
        $response = $this->sendCurl('http://127.0.0.1/admin/eventAsync/logSocketConnection',$conLogData);

        $result = json_decode($response);
        if($result->responseCode == 1){
            $this->subscribers[$conn_id]->con_log_id = $result->responseData;
        }

        $result = array();
        $result['code'] = 1;
        $result['type'] = 'handshake_complete';
        $result['message'] = '';
        
        
        return $result;
    }

    private function sendCurl($url,$data){
        $post_params = array();

        foreach ($data as $key => $val) 
        {
            $post_params[] = $key.'='.$val;
        }
        
        $post_string = implode('&',$post_params);

        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, 1 ); 
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
        
        $result = curl_exec($ch);

        return $result;
    }

    private function sendMessage($conn,$data){
        
        $result = array();
        $logData = array();
        $result['code'] = 0;
        $result['type'] = 'message';
        $result['message'] = 'no_user';
        $logData = $result;
        if(isset($this->subscribers[$this->customersConnections[$data->target]])){
           
            $body = json_encode($data->body);
            $this->subscribers[$this->customersConnections[$data->target]]->send($body);
            $result['code'] = 1;
            $result['type'] = 'message';
            $result['message'] = 'message_sent';
            $logData = $result;
            $logData['target'] = $this->subscribers[$this->customersConnections[$data->target]];
        }

      
       
        return json_encode($result);
    }

    private function getConnections($conn,$data){
        $data = array();
        $data['subs'] = $this->subscribers;
        $data['connections'] = $this->customersConnections;
        return json_encode($data);
    }

    public function onClose(ConnectionInterface $conn) {
        // The connection is closed, remove it, as we can no longer send it messages
        $sub = $this->subscribers[$conn->resourceId];
        $cust_ident = $sub->cust_ident;
        $sconn_id = $sub->con_log_id;
        $this->sendCurl('http://127.0.0.1/admin/eventAsync/deleteSocketConnection/'.$sconn_id,array());

        unset($this->subscribers[$conn->resourceId]);
        unset($this->customersConnections[$cust_ident]);
       
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        

        $conn->close();
    }
}

