<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

require dirname(__DIR__).'/wsServer/libs/Ratchet/vendor/autoload.php';
require dirname(__DIR__).'/admin/libs/zeroMQ/vendor/autoload.php';
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
require_once 'websocketHandler.php';

$loop   = React\EventLoop\Factory::create();


$context = new React\ZMQ\Context($loop);

$pull = $context->getSocket(ZMQ::SOCKET_REP);
$pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
$handler = new websocketHandler($pull); 
$pull->on('message', array($handler, 'onServerMessage'));


$wsServer = new WsServer($handler);
$webSock = new React\Socket\Server('0.0.0.0:8080', $loop);
$server = new IoServer(
        new HttpServer(
            $wsServer
        ),
        $webSock,
        $loop
    );

$wsServer->enableKeepAlive($loop,30);

$server->run();

